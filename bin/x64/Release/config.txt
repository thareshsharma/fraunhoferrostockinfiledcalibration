#interval between images [s]
interval=0.7		

#application name can be given one the following string 
#	InfieldCompensation 
#	FactoryCalibration 
#	LensTestingApp
applicationname=InfieldCompensation
 
#Which parameters needs to be kept fixed
# these are parameters number 15,16,17,18,19
ddexp1=true 
ddexp2=true
ddexp3=true
ddscalar1=true
ddscalar2=false
distortion=false
extrinsic=false
focallength=false

# Camera Calibration with depth dependent parameters
#ddexp1=false 
#ddexp2=true
#ddexp3=true
#ddscalar1=true
#ddscalar2=false
#distortion=false
#extrinsic=false
#focallength=false

#maximum time between LED and Laser image
tolerance=0.2		

#number of dots to recognize a laser image
laserimage=3000		

#store all recorded images
allimages=false		

#path and filename of used dmvs setting file
dmvssettingsfile=dmvs_infield_compensation_settings.json  

# Record images or process images offline
record=true

#Path from where the images are to be read or stored
storagepath=C:\Users\SharmaT\Downloads\Ergebnisse\Recording1

#Name of calibration file name
calibrationfilename=\Factory_FREESTYLE_DMVS011900029_S1184115_S1181801.xml

#Number of image to record
imagestorecord=40

#Path to read the board files
#boardfilespath=D:\repos\BitBucket\farolabs\infield-compensation\Data\Boards
boardfilespath=C:\Users\SharmaT\Downloads\Ergebnisse\Boards

#depth depdendent params scalar
depthedependentscalar1 = 0.4
depthedependentscalar2 = 0.0065

#depth depdendent params exp
depthedependentexp1 = 0.25
depthedependentexp2 = 3.0
depthedependentexp3 = 5.0

minledfraction=0.1
maxledsaturation=0.4