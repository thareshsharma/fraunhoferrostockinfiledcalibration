#pragma once

template <int SMOOTH_LENGTH>
class Smoother final
{
public:
	Smoother()
	{
		history_.clear();
	}

	~Smoother()
	{
	}

	/**
	 * \brief Forcing a copy constructor to be deleted by the compiler.
	 */
	Smoother(const Smoother& rhs) = delete;

	/**
	 * \brief Forcing a assignment operator to be deleted by the compiler.
	 */
	Smoother& operator=(const Smoother& rhs) = delete;

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	Smoother(Smoother&& rhs) = delete;

	/**
	 * \brief Forcing a move assignment operator to be deleted by the compiler.
	 */
	Smoother& operator=(Smoother&& rhs) = delete;

	void Add(const double value)
	{
		if(history_.size() >= LENGTH)
			history_.pop_front();

		history_.push_back(value);
	}

	double Current() const
	{
		return history_.size() ? history_.back() : 0.0;
	}

	double Min() const
	{
		return history_.size() ? *std::min_element(history_.begin(), history_.end()) : 0.0;
	}

	double Max() const
	{
		return history_.size() ? *std::max_element(history_.begin(), history_.end()) : 0.0;
	}

	double Avg() const
	{
		return history_.size() ? std::accumulate(history_.begin(), history_.end(), 0.0) / history_.size() : 0.0;
	}

	double Median() const
	{
		if(history_.size())
		{
			std::vector<double> values(history_.begin(), history_.end());

			std::sort(values.begin(), values.end());

			return values[values.size() / 2];
		}

		return 0.0;
	}

	void ClearHistory()
	{
		history_.clear();
	}

protected:
	static const size_t LENGTH = SMOOTH_LENGTH;
	std::deque<double> history_;
};
