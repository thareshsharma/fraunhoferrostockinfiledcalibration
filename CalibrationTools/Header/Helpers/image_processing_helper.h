#pragma once

#include "dmvs_sdk.h"
#include "typedefs.h"
#include "ikinematics.h"
#include <opencv2/core/types_c.h>

namespace DMVS
{
namespace CameraCalibration
{
using namespace dmvs_sdk;
using namespace LABS::MotionControl;

class ImageProcessingHelper
{
public:
	static void ToCVMat(cv::Mat m,
	                    CvMat& self);

	static void CopyImages(ChannelImageMap& source_images,
	                       Frame& frame,
	                       size_t frame_count);

	static void CreateCollage(const cv::Mat& pattern_img0,
	                          const cv::Mat& pattern_img1,
	                          cv::Mat& collage);

	static cv::Mat AverageImages(const ImageVector& images);

};
}
}
