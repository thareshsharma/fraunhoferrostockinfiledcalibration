#pragma once

struct Version
{
	int major;
	int minor;
	int patch;
	int revision;

	// Constructor
	Version() :
		major(0),
		minor(0),
		patch(0),
		revision(0)
	{
	}

	const std::string& GetId() const
	{
		return id_;
	}

	void SetId(const std::string& in)
	{
		id_ = in;
	}

	bool SetVersionNumbersFrom(std::istream& ist)
	{
		char c;
		ist >> major >> c;
		if(c != '.')
		{
			ist.putback(c);
			return false;
		}
		ist >> minor >> c;
		if(c != '.')
		{
			ist.putback(c);
			return false;
		}
		ist >> patch >> c;
		if(c != '.')
		{
			ist.putback(c);
			return false;
		}
		ist >> revision;

		///* SST: It is not garanteed that a SCENE without Beta (Old SCENE) can open a File which is
		//created with a SCENE with Beta != 0. (The communication and file-format is the same regardless
		//what Beta is set) Therefore the Beta-Mark is only set for a display to the User.
		//*/
		//// Try to read the Beta-Version. On Versions without a beta-Version it should work too
		//beta = 0;
		//if(!ist.eof() && '.' == ist.peek())
		//{
		//	// Read in .
		//	ist >> c;
		//	// Read in beta.
		//	ist >> beta;
		//}

		return true;
	}



private:
	std::string id_;
};
