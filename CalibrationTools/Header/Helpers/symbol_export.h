#pragma once

#ifdef DMVS_CAMERA_CALIBRATION
#define DMVS_CAMERA_CALIBRATION_DLLIO __declspec(dllexport)
#else
#define DMVS_CAMERA_CALIBRATION_DLLIO __declspec(dllimport)
#endif
