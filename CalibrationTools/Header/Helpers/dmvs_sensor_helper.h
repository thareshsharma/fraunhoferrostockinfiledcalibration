#pragma once

#include "dmvs_sdk.h"
#include "ikinematics.h"
#include "constants.h"

namespace DMVS
{
namespace CameraCalibration
{
using namespace dmvs_sdk;
using namespace LABS::MotionControl;

class DMVSSensorHelper
{
public:
	static bool IsDeviceSafe(DMVSDevice* device);

	static std::string GetErrorString(error::DMVSErrorCode error);

	static void ThrowExceptionOnError(error::DMVSErrorCode error_code);

	static float PositionFactor(size_t frame_count,
	                            size_t positions_count);

	static unsigned char CalculateLedPower(size_t frame_count);

	static double GetLinearStagePosition(std::shared_ptr<IKinematics>& linear_stage);

	static void JogForward(std::shared_ptr<IKinematics>& linear_stage,
	                       double step_size = Constants::LINEAR_STAGE_STEPPING);

	static void JogForward(int type,
	                       void* data);

	static void JogBackward(std::shared_ptr<IKinematics>& linear_stage,
	                        double step_size = Constants::LINEAR_STAGE_STEPPING);

	static void JogBackward(int type,
	                        void* data);

	static std::string GetCaptureDeviceIdent();

	static std::string GetSerialNumber();

	static std::string GetDeviceType();

	static std::string GetCaptureDeviceFile();
};
}
}
