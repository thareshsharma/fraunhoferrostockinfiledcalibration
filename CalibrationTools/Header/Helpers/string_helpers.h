#pragma once

namespace DMVS
{
namespace CameraCalibration
{

class StringHelper
{
public:

	static unsigned int CharacterCount(const std::wstring& data);

	static wchar_t At(std::wstring& data,
	                  unsigned int position);

	static int FindNonWS(std::wstring data,
	                     unsigned int position);

	static int FindWS(std::wstring data,
	                  unsigned int position);

	static const wchar_t* StringNeverNull(const std::wstring& data);

	static std::wstring Substr(std::wstring data,
	                           int position,
	                           int last);

	static std::wstring Trim(std::wstring data,
	                         bool front,
	                         bool back);

	/**
	 * \brief Converts the char* into wchar_t*.
	 * \param source char array that need to be converted to wchar_t array.
	 * \return Converted to wchar_t.
	 */
	static const wchar_t* Widen(const char* source);

	static const char* Narrow(const wchar_t* source);

	/**
	 * slices the string by cutting it at positions defined by a delimiter and writes
	 * the resulting substrings in a container.
	 * \param result output iterator to write the resulting substrings in the destination range.
	 * \param delimiter user-defined character used to determine slicing points.
	 * \param keep_empty set to false to strip empty substrings.
	 * \return output iterator addressing one past the final element in the destination range.
	 */
	template <class OutputIterator>
	OutputIterator SliceC(OutputIterator result,
	                      const wchar_t delimiter,
	                      const bool keep_empty = false) const
	{
		return slice(result, std::bind1st(std::equal_to<wchar_t>(), delimiter), keep_empty);
	}
};
}
}
