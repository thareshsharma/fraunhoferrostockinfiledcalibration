#pragma once

#include "dmvs_sdk.h"
#include "ikinematics.h"
#include "constants.h"

namespace DMVS
{
namespace CameraCalibration
{
using namespace dmvs_sdk;
using namespace LABS::MotionControl;

class Helper
{
public:
	static double GetLinearStagePosition(std::shared_ptr<IKinematics>& linear_stage);

	static void JogForward(std::shared_ptr<IKinematics>& linear_stage,
	                       double step_size = Constants::LINEAR_STAGE_STEPPING);

	static void JogForward(int type,
	                       void* data);

	static void JogBackward(std::shared_ptr<IKinematics>& linear_stage,
	                        double step_size = Constants::LINEAR_STAGE_STEPPING);

	static void JogBackward(int type,
	                        void* data);
};
}
}
