#pragma once

#include "dmvs_sdk.h"
#include "observation.h"
#include "blob_detector.h"
#include "typedefs.h"

namespace DMVS
{
namespace CameraCalibration
{
class CircularMarkerHelper
{
public:

	static void DrawMarker(cv::Mat& image,
	                       const CircularMarkerVector& markers,
	                       bool if_draw_id,
	                       const cv::Scalar& board_color);

	static void DetectMarkers(ChannelImageMap& channel_images,
	                          BlobDetector& blob_detector,
	                          ImageChannelCircularMarkerVectorMap& markers,
	                          size_t frames_count);

	static void CalculateMarkerSurrounding(ImageChannelCircularMarkerVectorMap& markers,
	                                       ChannelImageMap& channel_type);

	static void AddUncodedMarker(std::map<ImageChannel, CircularMarkerVector>& circular_markers,
	                             std::vector<std::shared_ptr<Observation>>& observations,
	                             size_t frame_count);
};
}
}
