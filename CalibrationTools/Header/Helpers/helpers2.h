#pragma once

namespace DMVS
{
namespace CameraCalibration
{
class Helper2
{
public:

	static void Sleeping(size_t sleep_time_seconds);

	static unsigned char CalculateLedPower(size_t frame_count);

	static void CopyReportData(const std::string& dest_path);
};
}
}
