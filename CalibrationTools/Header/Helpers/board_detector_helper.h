#pragma once

#include "dmvs_sdk.h"
#include "BoardDetector/board_detector.h"
#include "observation.h"
#include "blob_detector.h"
#include "typedefs.h"

namespace DMVS
{
namespace CameraCalibration
{
class BlobDetectionHelper
{
public:

	static size_t LoadDetectorBoards(std::string file_name,
	                                 std::vector<std::shared_ptr<DMVS::CameraCalibration::BoardDetector>>& boards);

	static void DetectAllBoards(const std::vector<std::shared_ptr<DMVS::CameraCalibration::BoardDetector>>& boards,
	                            std::map<ImageChannel, CircularMarkerVector>& markers_map,
	                            size_t frame_count,
	                            std::vector<std::shared_ptr<Observation>>& observations,
	                            ChannelImageMap& channel);

	static void DrawMarker(cv::Mat& image,
	                       const CircularMarkerVector& markers,
	                       bool if_draw_id,
	                       const cv::Scalar& board_color);

	static void DetectMarkers(ChannelImageMap& channel_images,
	                          BlobDetector& blob_detector,
	                          ImageChannelCircularMarkerVectorMap& markers,
	                          size_t frames_count);

	static cv::Scalar PredefinedBoardColor(size_t board_id);
};
}
}
