#pragma once

#include "dmvs_sdk.h"
#include "BoardDetector/board_detector.h"
#include "ikinematics.h"
#include "calibration_file_base.h"
#include "shlobj_core.h"
#include "enum_default_directory.h"
#include "read_context_base.h"
#include "read_from_scope.h"
#include "version.h"

namespace DMVS
{
namespace CameraCalibration
{
using namespace dmvs_sdk;
using namespace LABS::MotionControl;

class FileSystemHelper
{
public:
	static int ReplaceFileExtension(const wchar_t* to_replace,
	                                const wchar_t* replacement);

	static int ComparePathComponents(const std::string& a,
	                                 const std::string& b);

	static fs::path GetDataFolder(const DefaultDirectory directory)
	{
		TCHAR program_data_folder[1024];
		program_data_folder[0] = '\0';

#ifdef _WINDOWS
		if(FAILED(SHGetFolderPath(nullptr, CSIDL_COMMON_APPDATA, nullptr, 0, program_data_folder)))
		{
			return "";
		}
#else
				program_data_folder[0] = '.';
				program_data_folder[1] = '\0';

#endif

		fs::path data_folder(program_data_folder);

		switch(directory)
		{
		case SCENE_DIRECTORY:
			return data_folder.append("FARO");

		case FREESTYLE_DIRECTORY:
			return data_folder.append("FARO").append("Freestyle");

		case CAMERAS_DIRECTORY:
			return data_folder.append("FARO").append("Freestyle").append("Cameras");

		case CUSTOMIZE_DIRECTORY:
			return data_folder.append("FARO").append("Freestyle").append("Customize");

		case CALIBRATIONS_DIRECTORY:
			return data_folder.append("FARO").append("Freestyle").append("Calibrations");

		case PROGRAM_DATA_DIRECTORY:
			return data_folder;

		default:
			throw std::exception("Directory folder not available");
		}
	}

	static fs::path GetCalibrationsDirectory()
	{
		return GetDataFolder(CALIBRATIONS_DIRECTORY);
	}

	static fs::path GetCamerasDirectory()
	{
		return GetDataFolder(CAMERAS_DIRECTORY);
	}

	static fs::path GetSceneDirectory()
	{
		return GetDataFolder(SCENE_DIRECTORY);
	}

	static fs::path GetFreeStyleDirectory()
	{
		return GetDataFolder(FREESTYLE_DIRECTORY);
	}

	static fs::path GetCustomizedDirectory()
	{
		return GetDataFolder(CUSTOMIZE_DIRECTORY);
	}

	static fs::path GetProgramDataDirectory()
	{
		return GetDataFolder(PROGRAM_DATA_DIRECTORY);
	}

	static bool IsValidPathComponent(const std::string& s);

	static int Read(const fs::path& file,
	                std::shared_ptr<ConfigurationContainer> container = nullptr)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Reading file -------- START [" << file.string() << "]";
		//if(!container)
		//	return IOMessages::IO_FALSE;

		if(file.empty())
			return IOMessages::IO_FALSE;

		if(!exists(file))
			return IOMessages::IO_FALSE;

		// Normalize file name
		//fs::path file_path(file.());

		std::ifstream file_stream(file, std::ios::in);

		if(!file_stream.is_open())
			return IOMessages::IO_OPEN_READ;

		const auto context = std::make_shared<ReadContextBase>();
		ReadFromScope scope(&file_stream, context.get());
		auto& model_in_format = scope.GetModelInFormat();
		scope.GetReadContext()->SetFormat(model_in_format.DetermineFormat());

		std::string head;
		model_in_format.ReadHeader(head);

		if(head.empty())
			return IOMessages::IO_UNKNOWN_FORMAT;

		Version ver;
		model_in_format.ReadVersion(ver);

		/*imf.readFaroStart();
		int res = container->readFrom(in, context);
		imf.readFaroEnd();*/

		file_stream.close();
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Reading file -------- END [" << file.string() << "]";
		return 0;
	}
};
}
}
