#pragma once

#include "enum_image_channel_type.h"

namespace DMVS
{
namespace CameraCalibration
{
class CircularMarker;
}
}

// Board data of one board contains two correlating vectors of equal size with IDs and measurements
typedef size_t BoardID;

typedef size_t CameraID;

typedef size_t FrameID;

/**
 * \brief type used for hierarchy levels.
 */
typedef unsigned int LevelType;

/**
 * \brief unsigned type to use for object counts and indices such as number of
 * children of an object, etc.
 * \note a 32-bit unsigned integral type on Win32 and Win64 platforms.
 */
typedef unsigned int TypeCount;

/**
 * \brief unsigned type to use for tokens.
 */
typedef TypeCount TypeToken;

/**
 * \brief
 */
typedef std::vector<cv::Mat> ImageVector;

/**
 * \brief
 */
typedef std::map<ImageChannel, ImageVector> ChannelImageVectorMap;

/**
 * \brief
 */
typedef struct
{
	std::map<ImageChannel, cv::Mat> channel_image_map;
} ChannelImageMap;

/**
 * \brief
 */
typedef struct
{
	std::map<FrameID, ChannelImageMap> frame_images;
} Frame;

/**
 * \brief Represents a vector of type CircularMarker
 */
typedef std::vector<DMVS::CameraCalibration::CircularMarker> CircularMarkerVector;

/**
 * \brief
 */
typedef std::pair<size_t, DMVS::CameraCalibration::CircularMarker> CircularMarkerPair;

/**
 * \brief
 */
typedef std::map<ImageChannel, CircularMarkerVector> ImageChannelCircularMarkerVectorMap;

/**
 * \brief Type of values in the table. also type of counter
 * value stored as exposed to clients.
 */
typedef unsigned int ValueType;

/**
 * \brief type used for string sizes and indices.
 * \note must be signed for historical reasons.
 * \note for 32/64bit compatibility, it is only a 32bit type
 */
typedef int size_type;

typedef std::vector<cv::Point2f> Dots;

typedef std::vector<cv::Vec6f> DotInfos;