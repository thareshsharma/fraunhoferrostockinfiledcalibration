#pragma once

namespace DMVS
{
namespace CameraCalibration
{
class MathHelper
{
public:

	static int RoundToInt(double value);

	/**
	 * \brief Calculates the radian angle to vector vec2. Returns 0 in the case that
	 * one of the 2 vectors are smaller than the threshold 1e-5.
	 */
	static double AngleTo(const cv::Vec3d& vec1,
	                      const cv::Vec3d& vec2);

	static double AngleTo(const cv::Vec2d& vec1,
	                      const cv::Vec2d& vec2);

	static double VectorLength(cv::Vec3d vec);

	static double VectorLength(cv::Vec2d vec);

	static double VectorLength(cv::Vec3d vec1,
	                           cv::Vec3d vec2);

	//Template methods
	template <typename T>
	static bool InRange(const T& value,
	                    const T& low,
	                    const T& upper)
	{
		return (value > low) && (value < upper);
	}

	template <typename T>
	static T AngleInRadians(const T degree)
	{
		return static_cast<float>(M_PI * degree / 180.0f);
	}

	template <typename T>
	static T AngleInDegrees(const T radians)
	{
		return static_cast<T>(radians * (180.0f / M_PI));
	}

	template <class T>
	static std::vector<T> ApplyTransformation(const cv::Mat& rotation_vector,
	                                          const cv::Mat& translational_vector,
	                                          std::vector<T>& points)
	{
		std::vector<T> out(points.size());

		if(points.empty())
			return out;

		cv::Mat rotation_matrix;

		if(rotation_vector.cols == 1)
			cv::Rodrigues(rotation_vector, rotation_matrix);
		else
			rotation_matrix = rotation_vector;

		Eigen::Matrix3d rotation;
		cv2eigen(rotation_matrix, rotation);
		const auto x = translational_vector.at<double>(0, 0);
		const auto y = translational_vector.at<double>(1, 0);
		const auto z = translational_vector.at<double>(2, 0);
		Eigen::Vector3d location(x, y, z);

		std::transform(points.begin(),
		               points.end(),
		               out.begin(),
		               [&rotation, &location](const T& pt) -> T
		               {
			               const Eigen::Vector3d point(pt.x, pt.y, pt.z);
			               Eigen::Vector3d point_out = rotation * point + location;
			               return T(point_out[0], point_out[1], point_out[2]);
		               });

		return out;
	}
};
}
}
