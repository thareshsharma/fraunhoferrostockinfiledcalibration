#pragma once

#include "dmvs.h"
#include "BoardDetector/board_detector.h"
#include "observation.h"
#include "blob_detector.h"
#include "typedefs.h"
#include "ikinematics.h"
#include "constants.h"
#include "enum_default_directory.h"
#include "io_messages.h"

namespace DMVS
{
namespace CameraCalibration
{
using namespace dmvs_sdk;
using namespace LABS::MotionControl;

class Helper
{
public :
	static bool IsDeviceSafe(DMVSDevice* device);

	static void Sleeping(size_t sleep_time_seconds,
	                     const std::string& message);

	static unsigned int CharacterCount(const std::wstring& data);

	static wchar_t At(std::wstring& data,
	                  unsigned int position);

	static int FindNonWS(std::wstring data,
	                     unsigned int position);

	static int FindWS(std::wstring data,
	                  unsigned int position);

	static const wchar_t* StringNeverNull(const std::wstring& data);

	static std::wstring Substr(std::wstring data,
	                           int position,
	                           int last);

	static std::wstring Trim(std::wstring data,
	                         bool front,
	                         bool back);

	static int ReplaceFileExtension(const wchar_t* to_replace,
	                                const wchar_t* replacement);

	static std::string GetErrorString(error::DMVSErrorCode error);

	static void ThrowExceptionOnError(error::DMVSErrorCode error_code);

	static int RoundToInt(double value);

	static size_t LoadDetectorBoards(std::string file_name,
	                                 std::vector<std::shared_ptr<DMVS::CameraCalibration::BoardDetector>>& boards);

	static size_t AddCodedMarkers(const std::vector<std::shared_ptr<DMVS::CameraCalibration::BoardDetector>>& boards,
	                              std::map<ImageChannel, CircularMarkerVector>& markers_map,
	                              size_t frame_id,
	                              std::vector<std::shared_ptr<Observation>>& observations,
	                              ChannelImageMap& channel);

	static void DrawMarker(cv::Mat& image,
	                       const CircularMarkerVector& markers,
	                       bool if_draw_id              = true,
	                       const cv::Scalar& text_color = Constants::WHITE);

	static void DetectMarkers(ChannelImageMap& channel_images,
	                          BlobDetector& blob_detector,
	                          ImageChannelCircularMarkerVectorMap& markers);

	static cv::Scalar PredefinedBoardColor(size_t board_id);

	static void RemoveLowQualityMarker(ImageChannelCircularMarkerVectorMap& markers,
	                                   ChannelImageMap& channel_type);

	static void AddUncodedMarkers(std::map<ImageChannel, CircularMarkerVector>& circular_markers,
	                              std::vector<std::shared_ptr<Observation>>& observations,
	                              size_t frame_count);

	static void CopyImages(ChannelImageMap& source_images,
	                       Frame& frame,
	                       size_t index);

	static void CreateCollage(cv::Mat& collage,
	                          const cv::Mat& pattern_image_left,
	                          const cv::Mat& pattern_image_right,
	                          const cv::Mat& projector_image);

	static void CreateCollageImage3And1(cv::Mat& collage,
	                                    const cv::Mat& color_image,
	                                    const cv::Mat& pattern_image_left,
	                                    const cv::Mat& pattern_image_right,
	                                    const cv::Mat& extra_image);

	static double PositionFactor(size_t frame_count,
	                             size_t positions_count);

	static void CreateCollage(const cv::Mat& pattern_img0,
	                          const cv::Mat& pattern_img1,
	                          cv::Mat& collage);

	static unsigned char CalculateLedPower(size_t frame_count);

	static double GetLinearStagePosition(IKinematics* linear_stage);

	static void JogForward(IKinematics* linear_stage,
	                       double step_size = Constants::LINEAR_STAGE_STEPPING);

	static void JogForward(int type,
	                       void* data);

	static void JogBackward(IKinematics* linear_stage,
	                        double step_size = Constants::LINEAR_STAGE_STEPPING);

	static void JogBackward(int type,
	                        void* data);

	static std::string GetCaptureDeviceIdent();

	static std::string GetSerialNumber();

	static std::string GetDeviceType();

	static std::string GetCaptureDeviceFile();

	static cv::Mat AverageImages(const ImageVector& images);

	static void CopyReportData(const std::string& dest_path);

	/**
	 * \brief Calculates the radian angle to vector vec2. Returns 0 in the case that
	 * one of the 2 vectors are smaller than the threshold 1e-5.
	 */
	static double AngleTo(const cv::Vec3d& vec1,
	                      const cv::Vec3d& vec2);

	static double AngleTo(const cv::Vec2d& vec1,
	                      const cv::Vec2d& vec2);

	static double VectorLength(cv::Vec3d vec);

	static double VectorLength(cv::Vec2d vec);

	static double VectorLength(cv::Vec3d vec1,
	                           cv::Vec3d vec2);

	/**
	 * \brief Converts the char* into wchar_t*.
	 * \param source char array that need to be converted to wchar_t array.
	 * \return Converted to wchar_t.
	 */
	static const wchar_t* Widen(const char* source);

	static const char* Narrow(const wchar_t* source);

	//static void ReportErrorMessage(const std::string& error_msg,
	//                               Messages::RESULT ls_result);

	static int ComparePathComponents(const std::string& a,
	                                 const std::string& b);

	static fs::path GetDataFolder(const DefaultDirectory directory);

	static fs::path GetCalibrationsDirectory();

	static fs::path GetCalibrationImagesDirectory();

	static fs::path GetCalibrationImagesAverageDirectory();

	static fs::path GetCalibrationImagesCollageDirectory();

	static fs::path GetCalibrationImagesAllDirectory();

	static fs::path GetCamerasDirectory();

	static fs::path GetSceneDirectory();

	static fs::path GetFreeStyleDirectory();

	static fs::path GetCustomizedDirectory();

	static fs::path GetProgramDataDirectory();

	static std::string GetWindowsError(DWORD last_error);

	static std::string GetWindowsError();

	static bool IsValidPathComponent(const std::string& s);

	static bool IsValidSearchString(const std::string& s);

	static IOMessages::IOResult Read(const fs::path& file,
	                                 std::shared_ptr<ConfigurationContainer> container = nullptr);

	static std::string GetNowTime();

	static void ToVec2D(std::vector<double> vec,
	                    cv::Vec2d& vec2d);

	static void ToVec3D(std::vector<double> vec,
	                    cv::Vec3d& vec3d);

	static void ToMatrix3x3(std::vector<double> vec,
	                        cv::Mat& mat3x3);

	static void ToDistortionMatrix(std::vector<double> vec,
	                               cv::Mat& mat5x1);

	static void ToMatrix4x4(std::vector<double> vec,
	                        cv::Mat& mat4x4);

	static void ToExtrinsicsParameters(const std::vector<double>& vec,
	                                   cv::Mat& rotation_matrix,
	                                   cv::Mat& translation_vector);

	static void ToLaserBeamCustomMatrix(const std::vector<double>& laser_beams,
	                                    cv::Mat& laser_beams_matrix);

	static void ToVector(const cv::Vec2d& vec2d,
	                     std::vector<double>& vec);

	static void ToVector(const cv::Vec3d& vec3d,
	                     std::vector<double>& vec);

	static void ToVector(const cv::Mat& mat,
	                     std::vector<double>& vec);

	/*static  std::string::size_type Replace(const wchar_t* to_replace, const wchar_t* replacement)
	{
		if(!to_replace)
			return 0;
		assert(wcslen(to_replace) < INT_MAX);
		const int to_replace_len = (int)wcslen(to_replace);
		if(to_replace_len <= 0)
			return 0;

		std::string result;
		int found;
		int index = 0;
		size_type count = 0;
		while((found = Find(to_replace, index)) >= 0) {
			const int n = found - index;
			if(n > 0)
				result += Substr(index, found);
			index += n;
			if(replacement)
				result += replacement;
			index += to_replace_len;
			++count;
		}
		result += substr(index);
		std::swap(result);
		return count;
	}*/

	static std::string GetTimeStamp()
	{
		char time_buffer[100];
		time_t raw_time;
		struct tm time_info{};
		time(&raw_time);
		localtime_s(&time_info, &raw_time);
		strftime(time_buffer, 100, "%Y%m%d-%H%M%S", &time_info);

		return std::string(time_buffer);
	}

	//Template methods
	template <typename T>
	static bool InRange(const T& value,
	                    const T& low,
	                    const T& upper)
	{
		return (value > low) && (value < upper);
	}

	template <typename T>
	static T AngleInRadians(const T degree)
	{
		return static_cast<float>(M_PI * degree / 180.0f);
	}

	template <typename T>
	static T AngleInDegrees(const T radians)
	{
		return static_cast<T>(radians * (180.0f / M_PI));
	}

	template <class T>
	static std::vector<T> ApplyTransformation(const cv::Mat& rotation_vector,
	                                          const cv::Mat& translational_vector,
	                                          std::vector<T>& points)
	{
		std::vector<T> out(points.size());

		if(points.empty())
			return out;

		cv::Mat rotation_matrix;

		if(rotation_vector.cols == 1)
			cv::Rodrigues(rotation_vector, rotation_matrix);
		else
			rotation_matrix = rotation_vector;

		Eigen::Matrix3d rotation;
		cv2eigen(rotation_matrix, rotation);
		const auto x = translational_vector.at<double>(0, 0);
		const auto y = translational_vector.at<double>(1, 0);
		const auto z = translational_vector.at<double>(2, 0);
		Eigen::Vector3d location(x, y, z);

		std::transform(points.begin(),
		               points.end(),
		               out.begin(),
		               [&rotation, &location](const T& pt) -> T
		               {
			               const Eigen::Vector3d point(pt.x, pt.y, pt.z);
			               Eigen::Vector3d point_out = rotation * point + location;
			               return T(point_out[0], point_out[1], point_out[2]);
		               });

		return out;
	}

	template <class T>
	static std::vector<cv::Point_<T>> GetMarkersCenter(const CircularMarkerVector& markers)
	{
		std::vector<cv::Point_<T>> out(markers.size());

		std::transform(markers.begin(),
		               markers.end(),
		               out.begin(),
		               [](const CircularMarker& in) -> cv::Point_<T>
		               {
			               const auto center = in.CorrectedCenter();
			               return cv::Point_<T>(center.x, center.y);
		               });
		return out;
	}

	/**
	 * slices the string by cutting it at positions defined by a delimiter and writes
	 * the resulting substrings in a container.
	 * \param result output iterator to write the resulting substrings in the destination range.
	 * \param delimiter user-defined character used to determine slicing points.
	 * \param keep_empty set to false to strip empty substrings.
	 * \return output iterator addressing one past the final element in the destination range.
	 */
	template <class OutputIterator>
	OutputIterator SliceC(OutputIterator result,
	                      const wchar_t delimiter,
	                      const bool keep_empty = false) const
	{
		return slice(result, std::bind1st(std::equal_to<wchar_t>(), delimiter), keep_empty);
	}

	template <class T>
	static std::vector<cv::Point_<T>> GetRawEllipseCenters(const CircularMarkerVector& markers)
	{
		std::vector<cv::Point_<T>> out(markers.size());
		std::transform(markers.begin(),
		               markers.end(),
		               out.begin(),
		               [](const CircularMarker& in) -> cv::Point_<T>
		               {
			               return cv::Point_<T>(in.raw_ellipse.center.x, in.raw_ellipse.center.y);
		               });
		return out;
	}
};
}
}
