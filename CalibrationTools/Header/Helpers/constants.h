#pragma once

#include "typedefs.h"

namespace DMVS
{
namespace CameraCalibration
{
namespace Constants
{
static const unsigned char DEFAULT_LED_POWER           = 90;
static const unsigned char LED_ZERO_POWER              = 0;
static const unsigned char BOUNDING_RECTANGLE_MIN_AREA = 25;
static const unsigned char PIXEL_COLOR_WHITE           = 0xFF;
static const unsigned char PIXEL_COLOR_BLACK           = 0x00;

static const bool STABLE_ONLY = true;

static const int PIXEL_PER_BIT       = 20;
static const int RING_DECODER_ROWS   = 50;
static const int MIN_THRESHOLD       = 80;
static const int MAX_THRESHOLD       = 160;
static const int STEP_THRESHOLD      = 20;
static const int MAX_ID              = 1023;
static const int MIN_HITS            = 2;
static const int MAX_THRESHOLD_VALUE = 255;
static const int NUMBER_OF_BITS      = 12;
static const int RANGE_UPPER_LIMIT   = 1000;
static const int RANGE_LOWER_LIMIT   = 0;

static const float MARKER_DIAMETER_FACTOR   = 0.2f;
static const float REASONABLE_SCALING_RATIO = 4.0f;
static const float OUTER_SCALING_FACTOR     = 1.1f;
static const float TOLERANCE_LIMIT          = 0.4f;
static const float DEFAULT_SCALING_FACTOR   = 1.0f;
static const float ZERO_ANGLE               = 0.0f;
static const float OUTER_IMAGE_RING_FACTOR  = 6.0f;
static const float SCALING_CONSTANT         = 100.0f;

//IMAGE PROCESSING
static const size_t BLOCK_SIZE_CONSTANT       = 30;
static const int ADAPTIVE_THRESHOLD_OFFSET    = 1;
static const int DEFAULT_CONTOUR_SIZE         = 6;
static const int FILTER_BY_CIRCULARITY_FACTOR = 6;
static const int ADAPTIVE_THRESHOLD_MAX_VALUE = 1;
static const int MAX_PARENT_SIZE              = 36;
static const int BORDER                       = 1;
static const int DRAW_CONTOURS_THICKNESS      = 1;
static const int DRAW_CONTOURS_INDEX          = -1;
static const int DRAW_CONTOURS_COLOR          = 1;

static const float DEFAULT_SCALE               = 1.0f;
static const float DEFAULT_CENTER_CONFIDENCE   = 1.0f;
static const float FILTER_BY_INERTIA_EPS       = 1e-2f;
static const float BLOB_CENTER_TOLERANCE       = 1.0f;
static const float SCALED_CONTOUR_CONSTANT     = 3.2f;
static const float SCALED_POINT_CONSTANT       = 2.2f;
static const float BLOB_BLACK_RING_RATIO       = 0.6f;
static const float ELLIPSE_IMAGE_CENTER_OFFSET = 1.0f;
static const float ALLOWED_DIFFERENCE_OFFSET   = 1.0f;
static const float ELLIPSE_MIN_ROTATION_ANGLE  = 0.0f;
static const float ELLIPSE_MAX_ROTATION_ANGLE  = 180.0f;

//DETECTOR BOARDS
static const size_t MAX_COUNT_DETECTOR_BOARDS = 99;

//Ring code decoder
const size_t DEFAULT_SIZE_T_VALUE = 1000000;

//COLORS
static const cv::Scalar WHITE(255, 255, 255);
static const cv::Scalar BLACK(0, 0, 0);
static const cv::Scalar RED(37, 29, 255);
static const cv::Scalar BRIGHT_RED(255);
static const cv::Scalar LIGHT_RED(20, 20, 180);
static const cv::Scalar GREEN(67, 201, 122);
static const cv::Scalar BRIGHT_GREEN(0, 255, 0);
static const cv::Scalar PURE_BLUE(255, 0, 0);
static const cv::Scalar BRIGHT_BLUE(0, 255, 255);
static const cv::Scalar DARK_BLUE(0, 0, 255);
static const cv::Scalar BLUE(136, 80, 0);
static const cv::Scalar LIGHT_BLUE(128, 20, 20);
static const cv::Scalar YELLOW(59, 176, 255);
static const cv::Scalar GRAY(128, 128, 128);
static const cv::Scalar LIGHT_GRAY(200, 200, 200);
static const cv::Scalar CYAN(255, 255, 0);
static const cv::Scalar MAGENTA(255, 0, 255);
static const cv::Scalar ORANGE(17, 153, 255);
static const cv::Scalar COLOR1(0, 200, 200);
static const cv::Scalar COLOR2(100, 100, 100);
static const cv::Scalar COLOR3(100, 100, 180);

//PROCESSING
static const size_t FRAMES_PER_POSITION = 9;
static const size_t NUMBER_OF_CHANNELS  = 3;

// CIRCULAR MARKER
//const cv::Vec2f CIRCULAR_MARKER_DEFAULT_CENTER_CORRECTION = cv::Vec2f(0.0f, 0.0f);
//const size_t CIRCULAR_MARKER_DEFAULT_CLASS_ID = DEFAULT_SIZE_T_VALUE;
//const double CIRCULAR_MARKER_DEFAULT_OUTER_RADIUS_SCALE = 1.0;
//const double CIRCULAR_MARKER_DEFAULT_INNER_RADIUS_SCALE = 0.0;
//const double CIRCULAR_MARKER_DEFAULT_RADIUS = 0.0;
//const double CIRCULAR_MARKER_DEFAULT_ANGLE = 0.0;
//const double CIRCULAR_MARKER_DEFAULT_CONFIDENCE = -1.0;
//const double CIRCULAR_MARKER_DEFAULT_WEIGHT = 1.0;
//const double CIRCULAR_MARKER_DEFAULT_RAW_DISTANCE = 1.0;
//const double CIRCULAR_MARKER_DEFAULT_AVERAGE_SURROUNDING_AREA = 255.0;
//const double CIRCULAR_MARKER_DEFAULT_AVERAGE_INNER_AREA = 1.0;

//MARKER QUALITY ESTIMATOR
static const float SCALING_FACTOR_EDGE_PARTS = 1.2f;
static const float SCALING_FACTOR_MASK       = 1.3f;
static const int MAXIMUM_AOI_SIZE            = 300;
static const cv::Scalar OUTER_ELLIPSE_COLOR  = PURE_BLUE;

//WORK FLOW
static const size_t SLEEP_TIME_IN_MILLISECONDS                   = 30;
static const size_t NUMBER_OF_IMAGES_IN_SERIES                   = 4;
static const size_t SLEEP_LOOP_CONSTANT                          = 1000;
static const double LINEAR_STAGE_LENGTH                          = 0.6;
//static const double LINEAR_STAGE_LENGTH_LTS                      = 0.3;
static const double LINEAR_STAGE_PLATE_DETECTION_POSITION_START  = 0.00;
static const double LINEAR_STAGE_PLATE_DETECTION_MIDDLE_POSITION = 0.05;
static const double LINEAR_STAGE_PLATE_DETECTION_POSITION_END    = 0.10;
static const double LINEAR_STAGE_STEPPING                        = 0.03;
//static const double LINEAR_STAGE_STEPPING_LTS                    = 0.02;
static const double LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION  = 0.05;

//BLOB DETECTION PARAMS
static const float BLOB_DETECTION_MIN_AREA           = 25.00f;
static const float BLOB_DETECTION_MAX_AREA           = 2500.00f;
static const size_t BLOB_DETECTION_THRESHOLD_STEP    = 5;
static const size_t BLOB_DETECTION_MIN_THRESHOLD     = 20;
static const size_t BLOB_DETECTION_MAX_THRESHOLD     = 200;
static const size_t BLOB_DETECTION_MIN_REPEATABILITY = 2;
static const size_t BLOB_DETECTION_BLOB_COLOR        = 0;
static const size_t BLOB_DETECTION_WHITE_CIRCLE_ID   = 0;
static const size_t BLOB_DETECTION_BLACK_CIRCLE_ID   = 1;
static const size_t BLOB_DETECTION_REFINE_ELLIPSES   = 2;

static const float BLOB_DETECTION_MIN_DISTANCE_BETWEEN_BLOBS = 1.0f;
static const float BLOB_DETECTION_MIN_VIEWING_ANGLE          = 65.0f;

static const bool BLOB_DETECTION_SET_ID_BY_COLOR    = true;
static const bool BLOB_DETECTION_DECODE_RING_MARKER = true;

//LED POWER
static const unsigned char LED_BASE_POWER     = 20U;
static const unsigned char LED_VARIABLE_POWER = 80U;
static const unsigned char LED_MAX_POWER      = 100U;

//CAMERA
static const double CAMERA_MIN_EXPOSURE_TIME_SEC = 0.07;
static const double CAMERA_MAX_EXPOSURE_TIME_SEC = 2.4;

//WORK FLOW
static const size_t NUMBER_CALIBRATION_POSITIONS = size_t(LINEAR_STAGE_LENGTH / LINEAR_STAGE_STEPPING);
//static const size_t NUMBER_CALIBRATION_POSITIONS_LTS = size_t(LINEAR_STAGE_LENGTH_LTS / LINEAR_STAGE_STEPPING_LTS);
static const size_t MAX_AVERAGE_SURROUNDING_AREA     = 240;
static const size_t CIRCLE_RADIUS                    = 2;
static const size_t MIN_COUNT_CODED_MARKER           = 4;
static const double TOLERANCE_CORRECTED_CENTER       = 3.0;

//DMVS class
static const double DMVS_DEFAULT_FRAME_RATE              = 10.0;
static const double DMVS_DEFAULT_EXPOSURE_TIME           = 1000.0;
static const DWORD DMVS_GRAB_WAIT_TIME_MILLISECONDS      = 5;
static const size_t DMVS_DEFAULT_MODE                    = 0;
static const size_t DMVS_IMAGE_WIDTH                     = 1280;
static const size_t DMVS_IMAGE_HEIGHT                    = 1024;
static const size_t DMVS_DOUBLE_CAMERA_IMAGE_HEIGHT      = 1024;
static const size_t DMVS_DOUBLE_CAMERA_IMAGE_WIDTH       = 2560;
static const bool DMVS_DEFAULT_AUTO_EXPOSURE             = false;
static const bool DMVS_DEFAULT_POSITION_INTERFACE_ACTIVE = false;
static const bool DMVS_DEFAULT_SET_LASER                 = false;
static const bool DMVS_DEFAULT_SET_LED_FLASH_CONSTANT    = false;
static const unsigned char DMVS_DEFAULT_LED_POWER        = 100; //URANGE 0 to 100 (%)

const std::string DMVS_SENSOR      = "sensor";
const std::string DMVS_DEVICE_TYPE = "devicetype";
const std::string DMVS_SERIAL_NO   = "serialNo";
const std::string DMVS_RGB_WIDTH   = "rgbWidth";
const std::string DMVS_RGB_HEIGHT  = "rgbHeight";
const std::string DMVS_WIDTH       = "width";
const std::string DMVS_HEIGHT      = "height";
const std::string DMVS_SKIPPED     = "skipped";
const std::string DMVS_OPTIONS     = "options";

// DEVICE DESCRIPTION
const double DEVICE_DESCRIPTION_MINIMUM_Z_VALUE = 0.3;
const double DEVICE_DESCRIPTION_MAXIMUM_Z_VALUE = 10.0;
const double DEVICE_DESCRIPTION_DEFAULT_Z_VALUE = 0.8;

// GENERAL
const double DOUBLE_EPSILON                      = 1e-12;
const float FLOAT_EPSILON                        = 1e-6f;
const double CALIBRATION_MAX_PERMITTED_RMS_ERROR = 0.06;
static const cv::Mat EMPTY_MATRIX                = cv::Mat(0, 0, CV_8U);

/**
 * \brief constant to use for invalid indices for collections based on size_t.
 */
static const size_t INVALID_SIZE = size_t(-1);

/**
 * \brief constant to use for invalid indices for collections based on count_t.
 */
static const TypeCount INVALID_COUNT = TypeCount(-1);

/**
 * \brief minimum possible hierarchy level constant.
 */
static const LevelType MIN_LEVEL = LevelType(0u);

/**
 * \brief unlimited hierarchy level constant.
 */
static const LevelType MAX_LEVEL = LevelType(-1);

// Default paths and folder name
const std::string ROOT_DEFAULT_PATH                 = "C:\\CalibrationDataFS";
const std::string NETWORK_DEFAULT_PATH              = "C:\\CalibrationDataNetwork";
const std::string INTERMEDIATE_FOLDER               = "50 intermediate";
const std::string INPUT_FOLDER                      = "10 input";
const std::string OUTPUT_FOLDER                     = "90 results";
const std::string ATTRIBUTE_ROOT_FOLDER             = "CalibrationDataRoot";
const std::string FUNCTION_NOT_IMPLEMENTED          = "FUNCTION NOT IMPLEMENTED";
const std::string FUNCTION_NOT_IMPLEMENTED_IN_SCENE = "FUNCTION IS NOT IMPLEMENTED IN SCENE";
const std::string CHECK_ME                          = " ?? CHECK ME ??";

// Images
const std::string GREY0        = "grey0";
const std::string GREY1        = "grey1";
const std::string RAW_BAYER_BG = "rawBayerBG";
const std::string POINT_CLOUD  = "pc";

//  File Name
const std::string FILE_FRAME                                = "Frame_";
const std::string FILE_CHANNEL                              = "_Channel";
const std::string FILE_PNG                                  = ".png";
const std::string FILE_JSON                                 = ".json";
const std::string FILE_CALIBRATION_PARAMS                   = "calibParams.yml";
const std::string FILE_REF_BOARDS                           = "refboards";
const std::string FILE_STATISTICS                           = "Statistics";
const std::string FILE_IN_HOUSE_BOARD_TEMPLATE              = "InHouseBoardTemplate.yml";
const std::string FILE_CALIBRATION_BOARD_TEMPLATE           = "CalibBoardTemplate.cpd";
const std::string FILE_LINEAR_TABLE_POSITIONS               = "LinearTablePositions.yml";
const std::string FILE_CAMERA_CALIBRATION_FSV               = "_CameraCalibration.fsv";
const std::string FILE_CAMERA_CALIBRATION_PDF               = "_CameraCalibration.pdf";
const std::string FILE_CAMERA_CALIBRATION_SEQ               = "_CameraCalibration.seq";
const std::string FILE_CAMERA_CALIBRATION_LOG               = "_CameraCalibration.log";
const std::string FILE_CAMERA_CALIBRATION_XML               = "_CameraCalibration.xml";
const std::string FILE_CAMERA_CALIBRATION_REPORT_XML        = "_CameraCalibrationReport.xml";
const std::string FILE_TMP                                  = "tmp";
const std::string FILE_FQC_REPORT_XML                       = "FQCReport.xml";
const std::string FILE_FQC_LOG                              = "FQCLog.log";
const std::string FILE_INFIELD_CALIBRATION_FSV              = "InfieldCalibration.fsv";
const std::string FILE_INFIELD_CALIBRATION_ALL_LOG          = "InfieldCalibrationAll.log";
const std::string FILE_INFIELD_CALIBRATION_BRIGHT_LOG       = "InfieldCalibrationBright.log";
const std::string FILE_INFIELD_CALIBRATION_PROJECTION_LOG   = "InfieldCalibrationProj.log";
const std::string FILE_INFIELD_CALIBRATION_BRIGHT_XML       = "InfieldCalibrationBright.xml";
const std::string FILE_INFIELD_CALIBRATION_PROJECTION_XML   = "InfieldCalibrationProj.xml";
const std::string FILE_INFIELD_CALIBRATION_TRANS_SHIFT_LOG  = "InfieldCalibrationTransShift.log";
const std::string FILE_INFIELD_CALIBRATION_TRANS_SHIFT_XML  = "InfieldCalibrationTransShift.xml";
const std::string FILE_INFIELD_CALIBRATION_ALL_XML          = "InfieldCalibrationAll.xml";
const std::string FILE_INFIELD_CALIBRATION_ORIGINAL_XML     = "InfieldCalibrationOrig.xml";
const std::string FILE_INFIELD_CALIBRATION_COMPLETE_LOG_XML = "InfieldCalibrationCompleteLog.xml";
const std::string FILE_CALIBRATION_REPORT_PDF               = "CalibrationReport.pdf";
const std::string FILE_DOE_YML                              = "doe.yml";
const std::string FILE_PROJECTOR_CALIBRATION_FSV            = "ProjectorCalibration.fsv";
const std::string FILE_PROJECTOR_CALIBRATION_LOG            = "ProjectorCalibration.log";
const std::string FILE_PROJECTOR_CALIBRATION_XML            = "_ProjectorCalibration.xml";
const std::string FILE_PROJ_CALIBRATION_REPORT_XML          = "ProjectorCalibrationReport.xml";

// LOG
const std::string LOG_GRABBER_FRAME      = "() : Grabber Frame Count : ";
const std::string LOG_GRAB_IMAGE         = "() : Grab Image ";
const std::string LOG_RETRIEVE_IMAGE     = "() : Retrieve Image from Calibration Cache";
const std::string LOG_NO_IMAGE_RETRIEVED = "() : No Image Retrieved";
const std::string LOG_CACHE_ERROR        = "() : Cache Error : ";
const std::string LOG_ERROR_IO_MESSAGE1  = "IOMessage1 : ";
const std::string LOG_ERROR_IO_MESSAGE2  = "IOMessage2 : ";
const std::string LOG_START              = " --------START";
const std::string LOG_END                = " --------END";
const std::string LOG_OK                 = "OK";
const std::string LOG_FAILED             = "FAILED";

// JSON
const std::string JSON_SERIAL_NUMBER = "serial_number";

// Boards Detector
const size_t MIN_REQUIRED_MARKERS     = 4;
const size_t PARAMS_COUNT             = 15;
const size_t DEFAULT_SIZE_T_MAX_VALUE = size_t(-1);

// Calibration Calculator
const size_t MARKER_ID_START                                  = 1000;
const std::string CALIBRATION_CALC_WITH_EXISTING_OBSERVATIONS = "Calculate with existing observations ";
const std::string CALIBRATION_CALC_REASSIGNED_ALL             = "Reassigned All ";
const std::string CALIBRATION_CALC_REASSIGNED_OUTLIER_REMOVED = "Reassigned outlier removed ";
const std::string CALIBRATION_CALC_WITH_ELLIPSE_CORRECTION    = "First all - with Ellipse Correction ";
const std::string CALIBRATION_CALC_WITHOUT_ELLIPSE_CORRECTION = "First all - without Ellipse Correction ";

//Depth Dependent Model
const size_t PARAMS_SIZE              = 20;
const size_t DISTORTION_VECTOR_LENGTH = 5;

//PROJECTOR CALIBRATION
const float PROJECTOR_CALIBRATION_MINIMUM_BEAM_LENGTH               = 0.20f;
const double PROJECTOR_CALIBRATION_DEFAULT_FORCE_BEAM_X              = 311.0;
const double PROJECTOR_CALIBRATION_DEFAULT_FORCE_BEAM_Y              = 11.0;
const double PROJECTOR_CALIBRATION_DEFAULT_IR_EXPOSURE_TIME_AT_1M_MS = 0.6;
const size_t PROJECTOR_CALIBRATION_INITIAL_EXPOSURE_TEST_FRAMES      = 10;
const size_t PROJECTOR_CALIBRATION_MIN_NUMBER_OF_DOTS                = 1400;

// LASER DOTS
const size_t MAX_LASER_DOTS = 20000;
const float BASE            = 0.05f;
const float STEP            = 0.95f / 220.0f;
const cv::Mat_<float> RING5 = (cv::Mat_<float>(5, 5) <<
	2, 2, 2, 2, 2,
	2, 1, 1, 0, 1,
	2, 1, 0, 0, 1,
	2, 1, 0, 0, 1,
	2, 1, 1, 1, 1);


const size_t MAX_VALID_IMAGES       = 33;
const size_t MIN_LED_BRIGHTNESS     = 10;
const size_t BRIGHT_PIXEL_THRESHOLD = 70;
const float MAX_LED_SATURATION      = 0.2f;
const float MIN_LED_FRACTION        = 0.4f;

// Projector
const int NEIGHBOR_POINTS_IN_EACH_DIRECTION = 7;
const int XY_NEIGHBOR_RADIUS_SEARCH = 25;
const int DIAGONAL_NEIGHBOR_RADIUS_SEARCH = 35;
//number of cameras in total : 2 IR camera and 1 Projector.
const size_t NUMBER_OF_CAMERAS = 3;

// Used to Access the perpendicular/parallel coordinate of the epipolar lines
// after rectification. Rectification takes care that it is correct
static const size_t PARALLEL_COORDINATE = 0;
static const size_t PERPENDICULAR_COORDINATE = 1;
}
}
}
