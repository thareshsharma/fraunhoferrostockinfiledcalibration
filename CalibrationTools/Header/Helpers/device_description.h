#pragma once

// List of components a capture device can have
enum Component
{
	FLASH,
	LEDS,
	BUTTON_LEFT,
	BUTTON,
	BUTTON_RIGHT,
	IMU,
	SENSOR,
};

// Device types
enum DeviceType
{
	NO_CAPTURE = -100,
	DETECT_CAPTURE = -1,

	// Use ID > 10000 to avoid conflicts with OpenCV IDs
	SFM_CAPTURE = 10000,
	FREESTYLE_CAPTURE = SFM_CAPTURE + 300,
	XTION_CAPTURE = SFM_CAPTURE + 400,
	ASTRA_CAPTURE = SFM_CAPTURE + 401,
	KINECT_CAPTURE = SFM_CAPTURE + 500,
	KINECTONE_CAPTURE = SFM_CAPTURE + 501,
	R200_CAPTURE = SFM_CAPTURE + 600,
	F200_CAPTURE = SFM_CAPTURE + 601,
	SR300_CAPTURE = SFM_CAPTURE + 602,
	ZR300_CAPTURE = SFM_CAPTURE + 603,
	FREESTYLE_FPGA_CAPTURE = SFM_CAPTURE + 700,
	VIDEO_CAPTURE = SFM_CAPTURE + 900,
	DMVS_CAPTURE = SFM_CAPTURE + 1000,
};

enum CapabilityType
{
	NO_CAL = -1,

	//No calibration function available
	FS_CAMERA_FACTORY_CAL = 0,
	FS_PROJECTOR_FACTORY_CAL = 1,
	FS_CAMERA_INFIELD_CAL = 2,
	FS_PROJECTOR_INFIELD_CAL = 3,
	FS_FOCUS_ADJUSTMENT_CAL = 5,
	FS_LASER_POWER_CAL = 7,
	FS_WHITE_BALANCE_HARDWARE = 10,
	FS_WHITE_BALANCE_NO_BOARD = 11,
	SINGLE_CHANNEL_CV = 100,
	SINGLE_CHANNEL_FARO = 200,

	//Calibration data stored on device
	FS_STORED_ON_DEVICE = 998,
	FS_TESTLOOP_CAL = 999,

	//Support for turntable scanning
	TURNTABLE_SUPPORT = 5000,

	//Sequential frame access only when scanning
	FRAME_ACCESS_SEQUENTIAL = 10000,

	//Random frame access when scanning
	FRAME_ACCESS_RANDOM = 10001
};

//  Device state
enum DeviceState
{
	UNKNOWN = -1,
	NOT_CONNECTED = 0,
	CONNECTED = 1,
	DEFAULT_CALIBRATED = 2,
	CALIBRATED = 3,
};

struct DeviceDescription
{
	DeviceDescription();

	DeviceDescription(enum DeviceType device_type,
	                  enum DeviceState device_state,
	                  const std::string& serial_number = "");

	~DeviceDescription();

	/**
	 * \brief Forcing a copy constructor to be deleted by the compiler.
	 */
	DeviceDescription(const DeviceDescription& rhs) = default;

	/**
	 * \brief Forcing a assignment operator to be deleted by the compiler.
	 */
	DeviceDescription& operator=(const DeviceDescription& rhs) = delete;

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	DeviceDescription(DeviceDescription&& rhs) = default;

	/**
	 * \brief Forcing a move assignment operator to be deleted by the compiler.
	 */
	DeviceDescription& operator=(DeviceDescription&& rhs) = delete;

	bool Equals(const DeviceDescription& other) const;

	bool HasComponent(Component component) const;

	bool HasCapability(CapabilityType capability_type) const;

	DeviceType type;
	DeviceState state;

	// Color image information
	int width;
	int height;

	// Range limits and recommended Z range. (in meter)
	double minimum_z_value;
	double maximum_z_value;
	double default_z_value;

	// Components and calibration methods of the device
	std::vector<Component> components;
	std::vector<CapabilityType> capabilities;

	std::string serial;
	std::string identifier;
	const std::string& serial_number;
};
