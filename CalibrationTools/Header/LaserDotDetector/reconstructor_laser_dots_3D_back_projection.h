#pragma once

#include "reconstructor_laser_dots_3D.h"

namespace Reconstruct
{
/**
 * \brief Subclass of ReconstructorLaserDots3D which uses a different algorithm for
 * the actual correspondence search which is based on the back projection of
 * points into the third camera
 */
class ReconstructorLaserDots3DBackProjection : public ReconstructorLaserDots3D
{
public:

	ReconstructorLaserDots3DBackProjection();

	~ReconstructorLaserDots3DBackProjection();

	/**
	 * \brief Do some pre-calculation after setting the calibration values
	 * Pre-calculates matrices for back projection
	 */
	bool Preparation() override;

	/**
	 * \brief Set the Uncertainties for all cameras and for the back projection used in correspondence search
	 * \param ir0
	 * \param ir1
	 * \param proj
	 * \param back_proj
	 */
	void SetUncertaintiesBackProjection(double ir0,
	                                    double ir1,
	                                    double proj,
	                                    double back_proj);

protected:

	/**
	 * \brief Overrides the correspondence search and uses the back projection algorithm
	 */
	void FindTripleCorrespondences() override;

private:

	/**
	 * \brief Finds an intersection of the epipolar lines and additionally uses the back projection
	 * of the point into the projector to determine correspondences
	 * \param cam0_index
	 * \param corr_idx1
	 * \param corr_idx2
	 * \return
	 */
	std::vector<TripleCorrespondence> FindIntersectionByBackProjection(int cam0_index,
	                                                                   int corr_idx1,
	                                                                   int corr_idx2) const;

	/**
	 * Uncertainty used to determine whether the back projected point is good enough
	 */
	double projector_back_projection_uncertainty_;
};
}
