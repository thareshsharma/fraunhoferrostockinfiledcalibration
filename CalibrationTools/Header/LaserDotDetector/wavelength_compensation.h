#pragma once

#include <Eigen/Core>
#include "typedefs.h"

namespace Reconstruct
{
// Base class for WavelengthCompensationFit that implements interface as expected by Eigen.
//see: http://eigen.tuxfamily.org/dox-devel/unsupported/group__NonLinearOptimization__Module.html
//and the examples in NonLinearOpimization.cpp

template <typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
class FunctorForWavelengthCompensation
{
protected:

	/**
	 *\brief < which data format to use, e.g. double, float...
	 */
	typedef _Scalar Scalar;

	enum
	{
		InputsAtCompileTime = NX,
		ValuesAtCompileTime = NY
	};

	// the number of parameters (inputs) and function values (values)
	const int inputs_, values_;

	/**
	 *\brief  constructor with dimensions of parameters and values known at compile time (faster)
	 *\param [in] inputs_ number of parameters
	 *\param [in] values_ number of function values
	 */
	FunctorForWavelengthCompensation() :
		inputs_(4), values_(ValuesAtCompileTime)
	{
	}

	/**
	 *\brief constructor with dimensions of parameters and values not known at compile time (slower)
	 *\param [in] inputs number of parameters
	 *\param [in] values number of function values
	*/
	FunctorForWavelengthCompensation(const int inputs,
	                                 const int values) :
		inputs_(inputs), values_(values)
	{
	}

public:
	typedef Eigen::Matrix<Scalar, InputsAtCompileTime, 1> InputType;
	typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
	typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

	/**
	 *\brief access to the number of parameters
	 *\return the number of parameters
	*/
	int inputs() const
	{
		return inputs_;
	}

	/**
	 *\brief access to the number of function values
	 *\return the number of function values
	*/
	int values() const
	{
		return values_;
	}
};

// WavelengthCompensationFit
// Applies transformations to original coordinates to obtain new coordinates.
// Error of new coordinates to the measured coordinates (=currentCoordsIn) is minimized.
class WavelengthCompensationFit : public FunctorForWavelengthCompensation<double>
{
public:

	struct WavelengthCompInfo
	{
		Eigen::Vector2d translation;
		Eigen::Matrix2d rotation_matrix;

		WavelengthCompInfo() :
			translation(Eigen::Vector2d(0., 0.)),
			rotation(0.),
			scale(1.0)
		{
		}

		void Accumulate(const WavelengthCompInfo& other)
		{
			rotation += other.rotation;
			translation += other.translation;
			scale *= other.scale;
		}

		double rotation;
		double scale;
	};

	/**
	 *\brief constructorWavelengthCompensationFit
	 *\param [in] orig_coords_in the original coordinates, e.g. from calibration
	 *\param [in] current_coords_in the measured coordinates
	 *\param [in] central_beam_in the pixels of the central beam
	*/
	WavelengthCompensationFit(const std::vector<Eigen::Vector2d>& orig_coords_in,
	                          const std::vector<Eigen::Vector2d>& current_coords_in,
	                          const Eigen::Vector2d& central_beam_in);

	static WavelengthCompInfo ToCompInfo(const Eigen::VectorXd& x);


	static Eigen::VectorXd ToEigenVector(const WavelengthCompInfo& in);

	int operator()(const Eigen::VectorXd& x,
	               Eigen::VectorXd& fvec,
	               JacobianType* jacobi_type = nullptr) const;

	static double GetTangentsOfBeam(const Eigen::Vector2d& beam);

	template <class T, typename V>
	std::vector<T> ApplyToPoints(const WavelengthCompInfo& info,
	                             const std::vector<T>& points) const
	{
		//transformation from orig to current coordinates:
		//1. translation (so that it possible that rotation is always done about 0,0
		//2. rotation
		//3. calculate scale based on angle of transformed central beam (except for central beam.... as for this 0deg is assumed)
		//4. scale the difference vector to the central beam
		//5. add central beam to difference vector to obtain final point

		std::vector<T> out(points.size());

		for(auto i = 0; i < points.size(); ++i)
		{
			Eigen::Vector2d point(points[i][0], points[i][1]);
			Eigen::Vector2d delta = info.rotation_matrix * (point - central_beam_);

			// See http://www.rapidtables.com/math/trigonometry/arcsin/tan-of-arcsin.htm
			const auto value               = orig_sin_angles_.at(i) * info.scale;
			const auto& new_tangents_angle = value / sqrt(1 - value * value) /*tan(asin(sinAngles->at(i) * info.scale))*/;

			if(fabs(orig_tan_angles_.at(i)) > 0.5 / 1e5)
			{
				delta *= new_tangents_angle / orig_tan_angles_.at(i);
			}
			delta.noalias() += central_beam_ + info.translation;

			out[i] = T((V)delta[0], (V)delta[1]);
		}

		return out;
	}

	Dots ApplyToPoints(const WavelengthCompInfo& info,
	                   const Dots& points) const
	{
		Dots out(points.size());

		for(unsigned int i = 0; i < points.size(); ++i)
		{
			Eigen::Vector2d point(points[i].x, points[i].y);
			Eigen::Vector2d delta = (point - central_beam_);

			// See http://www.rapidtables.com/math/trigonometry/arcsin/tan-of-arcsin.htm
			const auto val                = orig_sin_angles_.at(i) * info.scale;
			const auto& new_tangent_angle = val / sqrt(1 - val * val) /*tan(asin(sinAngles->at(i) * info.scale))*/;

			if(fabs(orig_tan_angles_.at(i)) > 0.5 / 1e5)
			{
				delta *= new_tangent_angle / orig_tan_angles_.at(i);
			}

			delta.noalias() += central_beam_;
			out[i] = cv::Point2f((float)delta[0], (float)delta[1]);
		}

		return out;
	}

private:
	const std::vector<Eigen::Vector2d>& orig_coords_;
	const std::vector<Eigen::Vector2d>& current_coords_;
	const Eigen::Vector2d central_beam_;
	mutable std::vector<double> orig_sin_angles_;
	mutable std::vector<double> orig_tan_angles_;
};

class WavelengthCompensationFitScaleOnly : public FunctorForWavelengthCompensation<double>
{
public:

	/**
	 *\brief constructorWavelengthCompensationFit
	 *\param [in] orig_coords_in the original coordinates, e.g. from calibration
	 *\param [in] current_coords_in the measured coordinates
	 *\param [in] central_beam_in the pixels of the central beam
	*/
	WavelengthCompensationFitScaleOnly(const std::vector<Eigen::Vector2d>& orig_coords_in,
	                                   const std::vector<Eigen::Vector2d>& current_coords_in,
	                                   Eigen::Vector2d central_beam_in);

	virtual ~WavelengthCompensationFitScaleOnly();

	static WavelengthCompensationFit::WavelengthCompInfo ToCompInfo(const Eigen::VectorXd& x);

	static Eigen::VectorXd ToEigenVector(const WavelengthCompensationFit::WavelengthCompInfo& in);

	virtual int operator()(const Eigen::VectorXd& x,
	                       Eigen::VectorXd& fvec,
	                       JacobianType* jacobi = nullptr) const;

	static double GetTangentsOfBeam(const Eigen::Vector2d& beam);

	template <class T, typename V>
	std::vector<T> ApplyToPoints(const WavelengthCompensationFit::WavelengthCompInfo& info,
	                             const std::vector<T>& points) const
	{
		//transformation from orig to current coordinates:
		//1. translation (so that it possible that rotation is always done about 0,0
		//2. rotation
		//3. calculate scale based on angle of transformed central beam (except for central beam.... as for this 0deg is assumed)
		//4. scale the difference vector to the central beam
		//5. add central beam to difference vector to obtain final point

		std::vector<T> out(points.size());

		for(unsigned int i = 0; i < points.size(); ++i)
		{
			Eigen::Vector2d point(points[i][0], points[i][1]);
			Eigen::Vector2d delta = (point - central_beam_);

			// See http://www.rapidtables.com/math/trigonometry/arcsin/tan-of-arcsin.htm
			const auto val                = orig_sin_angles_.at(i) * info.scale;
			const auto& new_tangent_angle = val / sqrt(1 - val * val) /*tan(asin(sinAngles->at(i) * info.scale))*/;

			if(fabs(orig_tan_angles_.at(i)) > 0.5 / 1e5)
			{
				delta *= new_tangent_angle / orig_tan_angles_.at(i);
			}

			delta.noalias() += central_beam_;
			out[i] = T((V)delta[0], (V)delta[1]);
		}

		return out;
	}

protected:
	const std::vector<Eigen::Vector2d>& orig_coords_;
	const std::vector<Eigen::Vector2d>& current_coords_;
	const Eigen::Vector2d central_beam_;
	mutable std::vector<double> orig_sin_angles_;
	mutable std::vector<double> orig_tan_angles_;
};

class WavelengthCompensationFitScaleOnlyWeighted : public WavelengthCompensationFitScaleOnly
{
public:
	WavelengthCompensationFitScaleOnlyWeighted(const std::vector<Eigen::Vector2d>& orig_coords_in,
	                                           const std::vector<Eigen::Vector2d>& current_coords_in,
	                                           const Eigen::Vector2d& central_beam_in,
	                                           const std::vector<double>& weights_in);

	int operator()(const Eigen::VectorXd& x,
	               Eigen::VectorXd& fvec,
	               JacobianType* jacobi_type = nullptr) const override;

private:
	const std::vector<double>& weights_;
};
}
