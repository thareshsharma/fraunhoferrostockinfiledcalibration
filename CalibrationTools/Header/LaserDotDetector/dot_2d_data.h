#pragma once

#include "constants.h"
#include "camera_sensor.h"

/**
 * \brief Holds the calibration data of one camera and the derived data
 * (rectification etc.) and the coordinates of dots (original and rectified)
 */
namespace Reconstruct
{

class Dot2DData
{
public:

	Dot2DData();

	explicit Dot2DData(int index);

	virtual ~Dot2DData();

	cv::Point2f GetDot(size_t idx) const;

	size_t DotsCount() const;

	bool IfDotsEmpty() const;

	Dots& GetDots();

	cv::Point2f GetUndistortedDot(size_t idx);

	std::shared_ptr<const CameraSensor> GetCalibration() const;

	const cv::Mat& GetR2(int sensor_index) const;

	const cv::Mat& GetQ(int sensor_index) const;

	//Checks whether cam is rotated
	bool IsCameraRotated(int sensor_index) const;

	/**
	 * \brief get the bucket index for the specified coordinate in the second camera
	 * \param camera_b the second camera
	 * \param camera_a_coordinate the coordinate in this camera
	*/
	int GetEpipolarLineIndex(Dot2DData& camera_b,
	                         double camera_a_coordinate);

	/**
	 * \brief Set the calibration to use for this camera
	 * \param sensor_calibration smart pointer to the camera calibration container
	 * \return success
	 */
	bool SetCalibration(std::shared_ptr<const CameraSensor> sensor_calibration);

	/**
	 * \brief set the saved coordinates in this class
	 * \param laser_dots the 2D coordinates of the laser_dots to save to the class
	 */
	void SetDotCoords(const Dots& laser_dots);

	/**
	 * \brief rectify the actual points relative to the camB.
	 * \param camera_b the second camera to compensate to
	 */
	void RectifyPoints(Dot2DData& camera_b);

	/**
	 * \brief calculate the stereo rectification parameters to the second camera
	 * \param camera_2 the second camera to compensate to
	 */
	void StereoRectifyCams(Dot2DData& camera_2);

	/**
	 * \brief reset all counter of this camera for the correspondences
	 * \param camera_b is the camera view containing the dots that shall be pre-sorted
	 * \param camera_c is the cam according to which epipolar relation the dots shall be sorted into bins
	 * \param use_projector
	 */

	void GenerateLookupCorrespondences(Dot2DData& camera_b,
	                                   Dot2DData& camera_c,
	                                   bool use_projector = true);
	/**
	 * \brief reset all counter of this camera for the correspondences
	 */
	void ResetDotUseCounter();

	/**
	* \brief Projects a possible point based on its disparity and position to a third camera
	* \param x0 x coordinate in first cam
	* \param y_avg y coordinate in first cam
	* \param disparity_cam_b disparity to camB
	* \param cam_b_index index of the second camera
	* \param cam_c_index index of the third camera which w project into
	* \param point2d_cam_c output 2D point in the third cam
	*/
	void ProjectPointToThirdCamera(double x0,
	                               double y_avg,
	                               double disparity_cam_b,
	                               int cam_b_index,
	                               int cam_c_index,
	                               cv::Point2f& point2d_cam_c) const;
	/**
	* \brief Pre-calculates relative transformations between this and the cameras
	* CamB and CamC which can be used to accelerate the back projection in the third cam
	* \param camera_b second camera to use
	* \param camera_c third camera to use
	*/
	void PreCalculateRelativeTransformation(Dot2DData& camera_b,
	                                        Dot2DData& camera_c);

	// camera-intrinsic values
	// index of camera;
	int camera_index;

	// uncertainty of dot-detection (depending also on mechanical stability)
	double uncertainty;

	// undistorted coordinates
	Dots undistorted_coordinates;

	//original measured coordinates of dots in unrectified / distorted view
	Dots original_coordinates;

	// calibration of this camera in coordinate system of device
	std::shared_ptr<const CameraSensor> calibration;

	//Projection matrix for triangulation (in respective stereo-pairs)
	cv::Mat matrix_q[3];

	bool camera_rotated[3];

	// counting the number of triple correspondences using a specific dot
	std::vector<int> counter_used;

	// Values depending on relation to other camera (the index in the following
	// arrays is given by the specific relation) rectified coordinates in
	// the respective camera relation
	Dots rectified_coordinates[3];

	// index of coordinate of dots that is perpendicular to epipolar lines
	int perpendicular_coordinates[3];

	// limit of epipolar lines, used to calculate indices in lookup table
	double min_epipolar_line[3];

	// limit of epipolar lines, used to calculate indices in lookup table
	double max_epipolar_line[3];

	// distance of epipolar lines, used to calculate indices in lookup table
	double bin_center_dist[3];

	// lookup table for double correspondence search
	std::vector<std::vector<int>> lookup_correspondences;

private:
	int StereoRectify2(cv::InputArray camera_matrix1,
	                   cv::InputArray dist_coeffs1,
	                   cv::InputArray camera_matrix2,
	                   cv::InputArray dist_coeffs2,
	                   cv::Size image_size,
	                   cv::InputArray rotation_matrix,
	                   cv::InputArray translation_vector,
	                   cv::OutputArray rot_mat1,
	                   cv::OutputArray rot_mat2,
	                   cv::OutputArray p_mat1,
	                   cv::OutputArray p_mat2,
	                   cv::OutputArray q_mat,
	                   int flags,
	                   double alpha,
	                   cv::Size new_image_size,
	                   cv::Rect& valid_pix_roi1,
	                   cv::Rect& valid_pix_roi2) const;

	int StereoRectifyInternal(const cv::Mat& camera_matrix1,
	                          const cv::Mat& camera_matrix2,
	                          const cv::Mat& dist_coeffs1,
	                          const cv::Mat& dist_coeffs2,
	                          cv::Size image_size,
	                          const cv::Mat& rot_mat,
	                          const cv::Mat& translate_mat,
	                          cv::Mat& r1,
	                          cv::Mat& r2,
	                          cv::Mat& p1,
	                          cv::Mat& p2,
	                          cv::Mat& q_mat,
	                          int flags,
	                          double alpha,
	                          cv::Size new_img_size,
	                          cv::Rect& roi1,
	                          cv::Rect& roi2) const;

	void IcvGetRectangles(const cv::Mat& camera_matrix,
	                      const cv::Mat& distance_coefficients,
	                      const cv::Mat& r,
	                      const cv::Mat& new_camera_matrix,
	                      CvSize image_size,
	                      cv::Rect_<float>& inner,
	                      cv::Rect_<float>& outer) const;

	/*void InitRotatedCams()*/;

	std::vector<cv::Vec4f> dot_parameter_;

	// Values depending on relation to other camera (the index in the following
	// arrays is given by the specific relation)

	//2D rotation matrix for rectification (in respective stereo-pairs)
	cv::Mat rectification_2D_rotation_matrix_[3];

	//2D projection matrix for rectification (in respective stereo-pairs)
	cv::Mat projection_2D_rectification_matrix_[3];

	// Intersect two sorted lists of dots
	// Pre-calculated matrices for back projection
	cv::Mat relative_transformation_[3];
	Eigen::Matrix4d eigen_q_[3];
	Eigen::Matrix4d projection_matrices_[3][3];
};

// sort every bin with respect to rectified coordinates with camera 3
class Comparator
{
public:
	Dot2DData* camera;
	int camera_idx;

	Comparator(Dot2DData* cam,
	           const int idx) : camera(cam)
	{
		camera_idx = idx;
	}

	bool operator()(const int i,
	                const int j) const
	{
		return camera->rectified_coordinates[camera_idx][i].y < camera->rectified_coordinates[camera_idx][j].y;
	}
};
}
