#pragma once
#include "typedefs.h"

class LaserDotDetector2D
{
public:

	struct Params
	{
		int peak_neighbor_step;
		int bg_limit;
		double mean_peak_factor;     // value how much brighter the a center pixel must be
		double neighbor_peak_factor; //  = SFMPlugin::getOption(L"DotNeighb", 1.0);
		bool edge_dot_detection;     // advanced dot detection for dots on texture edges
		int sub_pixel_patch_size;    // the patch size in which sub-pixel fit is calculated
		int dot_blur;                // Kernel size of gaussian blurr
		double sigma_dot_blur;       // Sigma of gaussian blurr
		int outer_ring;
		int inner_ring;
		bool use_all_candidates;

		Params() :
			peak_neighbor_step(1),
			bg_limit(1),
			mean_peak_factor(2.5),
			neighbor_peak_factor(1.0),
			edge_dot_detection(true),
			sub_pixel_patch_size(5),
			dot_blur(5),
			sigma_dot_blur(0.5),
			outer_ring(4),
			inner_ring(3),
			use_all_candidates(false)
		{
		}
	};

	// Constructor with default parameter set
	LaserDotDetector2D(const cv::Mat& source_image);

	// Constructor with given parameter set
	LaserDotDetector2D(const cv::Mat& source_image,
	                   const Params& params);

	// main interface for dot detection
	double FindLaserDots(Dots& dots,
	                     DotInfos* dot_infos = nullptr);

	// main interface for dot detection
	double FindLaserDots(const cv::Mat& source_image,
	                     cv::Mat& img_32,
	                     cv::Mat& img_dil,
	                     Dots& dots,
	                     DotInfos* dot_infos = nullptr);

	// initialize the ring mask
	void SetRingMask(int outer,
	                 int inner);

	//convert image to float32 and apply dilation
	void FilterImages(const cv::Mat& source_image,
	                  cv::Mat& image_cv_32f,
	                  cv::Mat& image_dilated) const;

private:
	// experimental: do not filter at all - return all candidates
	bool UseAllCandidates(cv::Mat& img32,
	                      cv::Mat& img_dil,
	                      Dots& dots,
	                      DotInfos* dot_infos) const;

	// internal selection of candidate pixels, to be verified and fitted later
	std::vector<cv::Vec2i> SelectCandidates2(cv::Mat& img32,
	                                         cv::Mat& img_dil);

	// create a black/white mask of a ring in which neighborhood- analysis is done
	void InitRingMask(int outer_radius,
	                  int inner_radius);

	std::vector<cv::Vec2i> SelectCandidates(cv::Mat& image_cv_32f,
	                                        cv::Mat& image_dilate) const;

	// image in which dots are detected
	const cv::Mat& source_image_;

	// mask image for candidate search
	cv::Mat ring_;

	// dot detection parameters, default values used if not passed in constructor
	const Params params_;
};
