#pragma once

#include <vector>

/**
 * \brief Algorithmus fuer Union/Find Problem - Baum mit Pfadkomprimierung
 */
namespace SFMAlgorithms
{
/**
* Union Find Datenstruktur zur Bestimmung von Zusammenhangskomponenten in einem Graphen
* Implementierung: Baum mit Pfadkomprimierung
*/
class UnionFind
{
public:

	/**
	* Konstruktor mit Anzahl der initialen Partitionenzahl
	* \param n Anzahl der Knoten, jeder eine Partition
	*/
	UnionFind(int n);

	/**
	* Suchen des Repraesentanten der Partition, zu der Element i gehoert
	* @param i Knoten, dessen Representant gesucht wird

	* @return Repraesentanten-Knoten
	*/
	int findPartition(int i);

	/**
	* Suchen des Repraesentanten der Partition, zu der Element i gehoert.
	* Rueckgabe ist Index der Partition 0...num_partitions-1
	*
	* @param i Knoten, dessen Representanten Index gesucht wird
	* @return Index der Partition zu der i gehoert
	*/
	int findPartitionIndex(int i);

	/**
	* Zwei Partitionen vereinen, macht nichts, wenn beide Knoten bereits
	* in derselben Partition sind.
	*
	* @param j ein Knoten, der zu vereinenden Partition
	* @param k ein Knoten der anderen zu vereinenden Partition
	*/
	void unionPartitions(int j,
	                     int k);

	/**
	* Anzahl der Partitionen abfragen
	*
	* @return Anzahl der noch vorhandenen Partitionen
	*/
	int getNumPartitions();

	/**
	* Abfrage der Groesse der Partition, zu der ein Knoten gehoert
	*
	* @param node Knoten zu dem die Partitionsgroesse abgefragt wird
	* @return Groesse der Partition, die den Knoten enthaelt
	*/
	int getCardinality(int node);

	/**
	* Abfrage der Groesse der Partition, zu der ein Knoten gehoert
	*
	* @param partIdx Partition, deren Groesse abgefragt wird
	* @return Groesse der Partition mit Index partIdx
	*/
	int getCardinalityIndex(int partIdx);

	/**
	* Abfrage des Repräsentanten-Knoten einer Partition,
	*
	* @param partIdx Partition, deren Repräsentant abgefragt wird
	* @return Repräsentant der Partition
	*/
	int getPartitionRepresentative(int partitionIdx);

	~UnionFind();

private:

	/**
	* Union Find Datenstruktur zur Bestimmung der Zusammenhangskomponenten
	* Baum mit Pfadkomprimierung, auch zur Partitionierung grosser Graphen geeignet
	* Knoten-Element fuer Baum
	*/
	struct node
	{
		int id;              ///< Id des Knotens
		struct node* parent; ///< Vorgaenger im Baum
		int card;            ///< Groesse der Partition, nur gueltig fuer Repraesentanten
		int size;            ///< Tiefe des UnterBaumes diese Knotens

		struct node* pred; ///< Vorgaenger, doppelt verkettete Liste der Repraesentanten-Knoten
		struct node* succ; ///< Nachfolger, doppelt verkettete Liste der Repraesentanten-Knoten

		int partition_idx; ///< fuer Umbenennung der Partitionen auf 0..num_partitions-1
	};

	/**
	* Aktualisieren der Partitions-Index Rueckgaben auf 0...num_partitions-1
	* So kann man die findPartitionIndex() Rueckgabe gleich als Index verwenden
	*/
	void renameRoots();

	int m_rootsDirty;

	/// Liste der Repraesentanten einer Partition
	node m_rootList;

	/// alle Knoten
	node* m_nodes;

	/// Aktuelle Anzahl von Partitionen
	int m_numPartitions;

	/// Vektor aller Repraesentanten der Partitionen
	std::vector<node*> m_roots;
};
}


