#pragma once

#include "dot_2d_data.h"
#include "capture_calibration.h"
#include "wavelength_compensation.h"

class ConfigurationContainer;

namespace Reconstruct
{
using namespace DMVS::CameraCalibration;

class ReconstructorLaserDots3D
{
public:

	/**
	 * \brief holds the indices of triple-corresponding dots in the 3 views
	 */
	struct TripleCorrespondence
	{
		int camera_index[3];
		int used;
		int credibility;
		double pt3d[3];
		double e[3];
		double back_projection_uncertainty_parallel;
		double back_projection_uncertainty_perpendicular;

		TripleCorrespondence()
		{
			memset(this, 0, sizeof(TripleCorrespondence));
		}

		TripleCorrespondence(const int cam_index0,
		                     const int cam_index1,
		                     const int cam_index2,
		                     const int triple_credibility) :
			camera_index{cam_index0, cam_index1, cam_index2},
			used(0),
			credibility(triple_credibility),
			pt3d{0.0, 0.0, 0.0},
			e{0.0, 0.0, 0.0},
			back_projection_uncertainty_parallel(0),
			back_projection_uncertainty_perpendicular(0)
		{
		}
	};

	enum CorrespondenceInfo
	{
		BEAM_INDEX_16BIT = 0x0000ffff,
		BEAM_FLAGS = 0xffff0000,
		IS_BRIGHT = 0x00010000,
		IS_UNSAFE = 0x00020000,
		IS_PLANAR = 0x00040000,
		HAS_NO_PROJ_NEIGHBOR = 0x00080000,
		NOT_MOVED = 0x00100000,
	};

	ReconstructorLaserDots3D(std::shared_ptr<ConfigurationContainer> options = nullptr);

	virtual ~ReconstructorLaserDots3D();

	/**
	 * \brief Initialize all reconstructor parameters using the
	 * options attribute container. Use the defaults if no options are given.
	 * \param[in] options Scene attribute container with options
	 */
	bool Initialize(std::shared_ptr<ConfigurationContainer> options);

	/**
	 * \brief Update camera and projector calibrations
	 * \param[in] camera_calibration0 ir0 calibration
	 * \param[in] camera_calibration1 ir1 calibration
	 * \param[in] projector_calibration projector calibration
	 * \return success
	 */
	bool SetCalibrations(std::shared_ptr<const CameraSensor> camera_calibration0,
	                     std::shared_ptr<const CameraSensor> camera_calibration1,
	                     std::shared_ptr<const CameraSensor> projector_calibration);

	/**
	 * \brief Update calibration and calculate all pre-computations
	 * \param[in] calibration The device calibration
	 * \return success
	 */
	bool SetCalibrationAndPrepare(const CaptureCalibration& calibration);

	/**
	 * \brief
	 * \param calibration
	 */
	void GetUpdatedCalibration(CaptureCalibration& calibration) const;

	/**
	 * \brief Update 2d coordinates of IR pattern camera 0 and camera 1
	 * Set detected 2d coordinates of both ir cameras. Coordinates are raw image coordinates.
	 * Usually called every frame with new coordinates.
	 * \param[in] dot_coords0 Vector of raw image 2d coordinates detected in camera 0
	 * \param[in] dot_coords1 Vector of raw image 2d coordinates detected in camera 1
	 */
	void SetCoordinates(const Dots& dot_coords0,
	                    const Dots& dot_coords1);

	/**
	 * \brief Sets the beam coordinates as given in the calibration, i.e. they represent
	 * the beam directions with default pattern compensation.
	 * \param[in] mat the laser beams as given in the calibration.
	 */
	void SetLaserBeamsFromCalibration(const cv::Mat& mat);

	/**
	 * \brief
	 * \param dot_coords
	 */
	void GetCoordinatesPro0(Dots& dot_coords);

	/**
	 * \brief
	 * \return
	 */
	int GetNumBeams() const;

	/**
	 * \brief
	 * \param idx
	 * \return
	 */
	cv::Point2f GetProjectorCoordinate(int idx) const;

	/**
	 * \brief
	 * \param idx
	 * \return
	 */
	cv::Point2f GetCam0Coordinate(int idx) const;

	/**
	 * \brief
	 * \param idx
	 * \return
	 */
	cv::Point2f GetCam1Coordinate(int idx) const;

	/**
	 * \brief Do some pre-calculation after setting the calibration values
	 * Updates correspondence look up tables for current calibration settings
	 */
	virtual bool Preparation();

	/**
	 * \brief Enable and disable triangulation with projector beam from calibration
	 * If enabled the 3d point is calculated from 3 rays IR0 IR1 PROJ. Currently this causes a
	 * recalculation of undistortion and thus decreases calculation speed.
	 * \param[in] val true enables usage of projector beam
	 */
	void SetTriangulateWithProjector(bool val);

	/**
	 * \brief
	 * \return
	 */
	virtual std::vector<cv::Point3f> Calculate3DData();

	/**
	 * \brief
	 * \return
	 */
	std::vector<cv::Point3f> Calculate3DDataCorrespondencesGiven();

	/**
	 * \brief
	 * \param point_idx
	 * \return
	 */
	TripleCorrespondence GetUsedCorrespondence(int point_idx);

	/**
	 * \brief
	 * \param errors
	 * \param channel
	 * \return
	 */
	double GetProjectionError(std::vector<cv::Vec4f>& errors,
	                          int channel);

	/**
	 * \brief
	 * \param errors
	 * \param channel
	 */
	void GetProjectionError(Dots& errors,
	                        int channel);

	/**
	 * \brief
	 * \param errors
	 * \param channel
	 */
	void GetProjectionError(DotInfos& errors,
	                        int channel);

	/**
	 * \brief
	 * \param channel
	 * \param angular_x
	 * \param angular_y
	 * \return
	 */
	bool CalculateDeviationAngleOfCamera(int channel,
	                                     double& angular_x,
	                                     double& angular_y);

	/**
	 * \brief
	 * \return
	 */
	double CompensatePatternErrors();

	/**
	 * \brief
	 * \return
	 */
	double CompensatePatternErrorsOnlyScale();

	/**
	 * \brief
	 * \return
	 */
	double CompensatePatternErrorsOnlyScaleWeighted();

	/**
	 * \brief
	 * \param ir0
	 * \param ir1
	 * \param proj
	 */
	void SetUncertainties(double ir0,
	                      double ir1,
	                      double proj);

	///**
	// * \brief
	// * \param ir0
	// * \param ir1
	// * \param proj
	// * \param back_proj
	// */
	//void SetUncertaintiesBackProjection(double ir0,
	//                                    double ir1,
	//                                    double proj,
	//                                    double back_proj);

	/**
	 * \brief
	 * \return
	 */
	std::vector<int>& GetBrightPoints();

	/**
	 * \brief
	 * \param ir0_img
	 * \param ir1_img
	 * \param point_flags
	 * \return
	 */
	int MarkUnsafeByPatchComparison(const cv::Mat& ir0_img,
	                                const cv::Mat& ir1_img,
	                                std::vector<int>& point_flags);

	/**
	 * \brief
	 * \param point_flags
	 * \return
	 */
	int MarkPlanarByNeighborAnalysis(std::vector<int>& point_flags);

	/**
	 * \brief
	 * \param ir0_dot_params
	 * \param ir1_dot_params
	 * \param point_flags
	 * \return
	 */
	int MarkUnsafeByPeakValue(DotInfos ir0_dot_params,
	                          DotInfos ir1_dot_params,
	                          std::vector<int>& point_flags);

	/**
	 * \brief
	 * \param point_flags
	 * \return
	 */
	int MarkUnsafeByRadius(std::vector<int>& point_flags);

	/**
	 * \brief
	 * \param point_flags
	 * \return
	 */
	int MarkUnsafeByCredibility(std::vector<int>& point_flags);

	/**
	 * \brief
	 * \param point_flags
	 * \return
	 */
	int MarkUnsafeByAggressiveBrightOnly(std::vector<int>& point_flags);

	/**
	 * \brief
	 * \param point_flags
	 * \return
	 */
	int MarkUnsafeByProjectionError(std::vector<int>& point_flags);

	/**
	 * \brief
	 * \param point_flags
	 * \return
	 */
	int MarkUnsafeByDepth(std::vector<int>& point_flags);

	/**
	 * \brief
	 * \return
	 */
	std::vector<TripleCorrespondence> GetTripleCorrespondences() const;

	/**
	 * \brief Analyze depth matrices (nx9) and marks points that didn't move
	 * with the NOT_MOVED flag. The caller is responsible to provide the
	 * depth matrices.
	 * \param left
	 * \param right
	 * \return Returns -1 if failed. Otherwise the number of marked points.
	 */
	static int MarkNoMotion(cv::Mat& left,
	                        cv::Mat& right);

	/**
	 * \brief
	 * \return
	 */
	void UseBrightOnly(bool bright_only);

	/**
	 * \brief Resets the pattern compensation to default values, i.e. translation
	 * and rotation 0, scale 1. Sets the dot coordinates in the projector accordingly
	 * (back to the original coordinates from the calibration.)
	 */
	void ResetPattern();

	/**
	 * \brief Triangulates dots0 and dots1 and stores found 3d points in triangulatedPts
	 * Tries some wavelength compensation pattern values to find more points if set to true
	 * \param dots0
	 * \param dots1
	 * \param triangulated_points
	 * \param point_flags
	 * \param search_if_not_sufficient_pts
	 * \param search_every_n_frame
	 * \param necessary_amount_of_points_divider
	 * \return
	 */
	int TriangulatePoints(const Dots& dots0,
	                      const Dots& dots1,
	                      std::vector<cv::Point3f>& triangulated_points,
	                      std::vector<int>& point_flags,
	                      bool search_if_not_sufficient_pts               = false,
	                      unsigned int search_every_n_frame               = 10,
	                      unsigned int necessary_amount_of_points_divider = 4);


	/**
	 * \brief Triangulates dots0 and dots1 and stores found 3d points in triangulatedPts
	 * Tries some wavelength compensation pattern values to find more points if set to true
	 * \param dots0
	 * \param dots1
	 * \param triangulated_points
	 * \param point_flags
	 * \param search_if_not_sufficient_pts
	 * \return
	 */
	int TriangulatePoints(const Dots& dots0,
	                      const Dots& dots1,
	                      std::vector<cv::Point3f>& triangulated_points,
	                      std::vector<int>& point_flags,
	                      bool search_if_not_sufficient_pts = false);

	/**
	 * \brief
	 * \param dots0
	 * \param dots1
	 * \param triangulated_points
	 * \param point_flags
	 * \param use_back_projection_correspondence_search
	 * \param search_if_not_sufficient_pts
	 * \param search_every_n_frame
	 * \param necessary_amount_of_points_divider
	 * \return
	 */
	int TriangulatePointsAlgoChoice(const Dots& dots0,
	                                const Dots& dots1,
	                                std::vector<cv::Point3f>& triangulated_points,
	                                std::vector<int>& point_flags,
	                                bool use_back_projection_correspondence_search,
	                                bool search_if_not_sufficient_pts               = false,
	                                int search_every_n_frame                        = 10,
	                                unsigned int necessary_amount_of_points_divider = 10);

	/**
	 * \brief Other implementation of wave length correction. Gives reproducible results.
	 * \return void
	 * \param dots0
	 * \param dots1
	 * \param triangulated_points
	 * \param deep_search
	 */
	void TriangulateWithAutoWaveLengthCorrection(const Dots& dots0,
	                                             const Dots& dots1,
	                                             std::vector<cv::Point3f>& triangulated_points,
	                                             bool deep_search);

	/**
	 * \brief Return a measure for the laser wavelength. Accessors for member.
	 * \return
	 */
	double AccumulatedScaleCorrection() const;

	/**
	 * \brief
	 * \return
	 */
	double AccumulatedRotationCorrection() const;

	/**
	 * \brief
	 * \return
	 */
	Eigen::Vector2d AccumulatedTranslationCorrection() const;

	/**
	 * \brief
	 * \return
	 */
	WavelengthCompensationFit::WavelengthCompInfo AccumulatedWavelengthCompInfo() const;

	/**
	 * \brief
	 * \return
	 */
	bool GetBrightOnly() const;

	/**
	 * \brief
	 * \param iterations
	 */
	void SetAutoCompensation(int iterations);

	/**
	 * \brief
	 * \return
	 */
	int GetAutoCompensation() const;

	/**
	 * \brief
	 * \param limit
	 */
	void SetProjectorAutoCompensationLimit(double limit);

	/**
	 * \brief
	 * \return
	 */
	double GetProjectorAutoCompensationLimit() const;

	/**
	 * \brief
	 * \return
	 */
	int GetWaveLengthCompensationMinPts() const;

	/**
	 * \brief Temperature/scale pair used for wave length correction
	 * \return
	 */
	using TemperatureOfScale = std::pair<float, float>;

	/**
	 * \brief Test if Temperature/scale pair is valid
	 * \return bool
	 * \param scale
	 */
	static bool IsValid(const TemperatureOfScale& scale);

	/**
	 * \brief Adjust wave length correction using a new temperature value.
	 * \param last_value
	 * \param current_temperature
	 * \return TemperatureOfScale Returns the new scale.
	 */
	TemperatureOfScale AdjustScale(const TemperatureOfScale& last_value,
	                               float current_temperature);

	/**
	 * \brief Access to wavelength compensation accumulated values.
	 * \return
	 */
	double GetLastWavelengthScale() const;

	/**
	 * \brief
	 * \return
	 */
	Eigen::Vector2d GetLastTranslation() const;

	/**
	 * \brief
	 * \return
	 */
	double GetLastRotation() const;

	/**
	 * \brief Updates the current projector coordinates to the scale given by scale.
	 * Reference to this scale are the original projector points, i.e. scale = 1.
	 * If the current state of the reconstructor has a scale of 1.3 and this function
	 * is called with scale = 1.5, the state after the function is scale = 1.5, meaning
	 * the current state of the reconstructor is considered.
	 * \param info
	 */
	void UpdateProjectorPtsToPatternCompensationState(const WavelengthCompensationFit::WavelengthCompInfo& info);

	/**
	 * \brief
	 * \param in
	 */
	void SetNextWavelengthComp(const WavelengthCompensationFit::WavelengthCompInfo& in);

	/**
	 * \brief
	 * \return
	 */
	bool GetForceDeepProjectorCompensation() const;

	/**
	 * \brief
	 * \return
	 */
	void SetForceDeepProjectorCompensation(bool in);

	/**
	 * \brief Checks if the last correction is good. Used to control
	 *	- the frequency of calculation
	 *	- storing of the correction on the device
	 * \return bool
	 */
	bool HasGoodAccumulatedScaleCorrection() const;

	/**
	 * \brief Calculate 3d points out of valid triples
	 * Loops through all triples, checks whether they are valid and calculates a
	 * 3d point for it.
	 * @param[out] points vector for 3d points output
	 */
	void TriangulateTripleCorrespondences(std::vector<cv::Point3f>& points);

	/**
	 * \brief
	 * \param triples
	 */
	void SetTripleCorrespondences(const std::vector<TripleCorrespondence>& triples);

	bool use_projector = true;

	bool triangulate_with_projector;

	// All data of IR pattern camera 0, currently cam0 has a master status
	Dot2DData camera_sensor0;

	// All data of IR pattern camera 1
	Dot2DData camera_sensor1;

	// All data of IR pattern projector
	Dot2DData projector;

	bool use_online_correspondence_search;

protected:
	/**
	 * \brief All consistent triples
	 */
	std::vector<TripleCorrespondence> triples_;

	/**
	 * \brief
	 * \param point_flags
	 * \param number_of_triangulated_points
	 */
	void MarkPointIndices(std::vector<int>& point_flags,
	                      size_t number_of_triangulated_points);

	/**
	 * \brief Find consistent triples for all cam0 points
	 * Loops through all cam0 points and finds the triples containing them.
	 * Fills the valid triple vector of the class
	 * Degenerates to pair matching (based on epipolar constraint) if
	 * if projector usage is switched of (m_use_projector)
	 */
	virtual void FindTripleCorrespondences();

	/**
	 * \brief
	 */
	void FindTripleCorrespondencesOnLine();

	/**
	 * \brief Updated the depth for each triple
	 * Uses fast depth calculation based on disparity of ir cam coordinates
	 * result is used for depth dependent lens distortion correction
	 */
	void UpdateTripleCorrespondencesDepth();

	/**
	 * \brief
	 * \param cam0_index
	 * \param corr_idx1
	 * \param corr_idx2
	 * \return
	 */
	std::vector<TripleCorrespondence> FindPossiblePoints(int cam0_index,
	                                                     int corr_idx1,
	                                                     int corr_idx2);

	/**
	 * \brief
	 */
	void UpdateTripleCorrespondencesDepthTriangulation();

private:
	/*!
	 * \brief Find and resolve ambiguities in triple vector of potential correspondences
	 * Sets the valid flags of the triples
	 */
	void ResolveAmbiguities();

	/**
	 * \brief
	 * \return
	 */
	void AnalyzeAmbiguities();

	/**
	 * \brief
	 * \return
	 */
	double CompensatePatternErrorsImpl01();

	/**
	 * \brief
	 * \return
	 */
	double CompensatePatternErrorsImpl02();

	/**
	 * \brief
	 * \param info
	 */
	void UpdateProjectorPtsToPatternCompensationStateWithoutReset(const WavelengthCompensationFit::WavelengthCompInfo& info);

	/**
	 * \brief
	 * \param laser_dots
	 */
	void GetLaserDotMatrix(cv::Mat& laser_dots) const;

	/**
	 * \brief Update 2d coordinates of IR pattern projector
	 * Together with the projector calibration, these points define the
	 * projector rays.  Usually called once with values from pattern calibration
	 * or whenever the pattern is corrected (auto calibration)
	 * \param[in] dot_coords Vector of ray-defining projector coordinates
	 */
	void SetCoordinatesPro0(const Dots& dot_coords);

	/**
	 * \brief Find triples out of the two candidate lists from cam1 and proj0 generated for cam0
	 * There are two correspondence candidate lists for a point in cam0. We try to find all consistent
	 * triples containing the cam0 point and one point out of each candidate list
	 * \param[in] cam0_index Index of the currently considered point in cam0
	 * \param[in] corr_idx1 Index of LUT entry for potentially corresponding cam1 points
	 * \param[in] corr_idx2 Index of LUT entry for potentially corresponding projector points
	 * \return A vector of all consistent triples containing point cam0_index of cam0
	 */
	std::vector<TripleCorrespondence> FindIntersection(int cam0_index,
	                                                   int corr_idx1,
	                                                   int corr_idx2);

	void ResetToLastProjectorState();

	void InitBeamNeighbors(const cv::Mat& beams);

	void CalculateWeights(std::vector<cv::Point3f> points_3d,
	                      std::vector<double>& weights);

	void CalculateWeightsSingleCamera(std::vector<cv::Point3f> points_3d,
	                                  std::vector<double>& distortion_vector,
	                                  std::shared_ptr<const CameraSensor> calibration) const;

	static void ProjectWithoutDistortion(std::vector<cv::Point3f>& points_3d,
	                                     Dots& points_2d,
	                                     double fx,
	                                     double fy,
	                                     double cx,
	                                     double cy,
	                                     cv::Mat& R,
	                                     cv::Mat& t);

	// related to Mark Brenner new compensation
	// Save the last state of the Wavelength Compensation
	Dots all_beams_from_last_wavelength_compensation_;
	WavelengthCompensationFit::WavelengthCompInfo last_info_;
	std::vector<cv::Point3f> triangulated_points_;

	// Wavelength compensation accumulated values:
	bool use_stored_wavelength_comp_for_next_triangulation_;
	WavelengthCompensationFit::WavelengthCompInfo accumulated_wavelength_compensation_;
	WavelengthCompensationFit::WavelengthCompInfo next_wavelength_compensation_;

	//Not every detected triple leads to a valid 3d point. Credibility of triples may be too low
	// or some filters marked triples invalid. This vector stores triple index for each calculated 3d point
	std::vector<int> point2_triple_;

	// For laser patter neighborhood analysis we need fast access to the 3d point
	//that was created by a certain beam index.
	std::vector<int> beam_idx2_point_idx_;

	// all neighbor beams of a certain beam index
	// calculated once at initialization of laser pattern
	std::vector<std::vector<int>> beam_neighbours_;

	std::vector<int> bright_points_;

	//actually obsolete, to be removed
	cv::Mat laser_beam_matrix_;

	Dots bright_beams_from_calibration_;

	//the bright beams, in case of default pattern compensation
	Dots all_beams_from_calibration_;

	// all beams, in case of default pattern compensation
	static const double IMAGE_SIZE_X;
	static const double IMAGE_SIZE_Y;

	double general_uncertainty_;
	double uncertainty_proj_fac_;
	double projector_back_projection_uncertainty_;
	double auto_compensation_limit_;
	double max_reprojection_error_;
	double wavelength_cnum_diff_delta_;
	double wavelength_cx_tolerance_;
	double projector_radius_factor_;
	double max_err_proj_;
	double max_err_cams_;

	float max_pt_dist_;

	int auto_compensation_iterations_;
	int compensation_min_pts_;

	// Controls the frequency of the wave length compensation updates
	int auto_compensation_calls_;
	int wavelength_cmax_fev_;

	bool bright_only_;
	bool resolve_ambiguities_;
	bool force_deep_projector_compensation_;
	bool wavelength_clevmar_;
	bool search_bended_projector_;
	bool search_wave_length_;
	bool wavelength_compensation_on_;
};

/**
 * \brief reconstructor management
 */
struct ReconstructorScope
{
	ReconstructorScope(ReconstructorLaserDots3D* reconstructor,
	                   CaptureCalibration* calibration) :
		reconstructor_(reconstructor)
	{
		reconstructor_->use_projector = true;
		reconstructor_->SetCalibrationAndPrepare(*calibration);
		reconstructor_->SetLaserBeamsFromCalibration(calibration->GetCustomMatrix("laser_beams"));
		reconstructor_->UseBrightOnly(true);
		reconstructor_->Preparation();

		auto_compensation_before_ = reconstructor_->GetAutoCompensation();
	}

	virtual ~ReconstructorScope()
	{
		if(reconstructor_->GetBrightOnly())
		{
			reconstructor_->UseBrightOnly(false);
			reconstructor_->SetAutoCompensation(auto_compensation_before_);
			reconstructor_->Preparation();
		}
		else
		{
			reconstructor_->SetAutoCompensation(auto_compensation_before_);
		}
	}

private:
	ReconstructorLaserDots3D* reconstructor_;
	int auto_compensation_before_;
};
}
