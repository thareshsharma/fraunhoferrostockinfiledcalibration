#pragma once

/**
 * \brief FromFile should be smaller than the live modes.
 * Use values suitable for disjunction of the modes.
 */
enum class DeviceCurrentMode
{
	//Closed
	CLOSED = 0x0,

	//Read data from file
	FROM_FILE = 0x1,

	//The device is connected
	CONNECTED = 0x10,

	//Listen (to button) without grabbing
	LISTENING = 0x20,

	//Active grabbing. E.g. from USB
	GRABBING = 0x40,

	//Streaming. E.g from NUC via network
	STREAMING = 0x80,

	//All modes
	MODE_MASK = 0xFF,
};
