#pragma once

/**
 * \brief Class to fit a polynomial model to a set of x- and y-values
 * by default, a quadratic fit is used otherwise, the degree can be set
*/
class PolynomialLeastSquares
{
public:
	PolynomialLeastSquares(std::vector<double>& x_values,
	                       std::vector<double>& y_values,
	                       int degree = 2);

	/**
	 * \brief in case of degree = 2, we get the result of the fit as
	 * in f(x) = quadratic * (x-position)� + offset if degree != 2 the
	 * return value is -1 and no value is returned
	*/
	int GetQuadraticComponents(double& quadratic,
	                           double& position,
	                           double& offset) const;

private:

	/**
	 * \brief
	 * \return
	 */
	int CalculateLeastSquares();

	/**
	 * \brief
	 * \return
	 */
	int CalculateResiduals();

	std::vector<double> x_;
	std::vector<double> y_;
	std::vector<double> estimated_parameters_;
	std::vector<double> calculated_values_;
	std::vector<double> residuals_;

	double offset_;
	double minimum_position_;
	double quadratic_component_;
	double residual_rms_;

	int degree_;
};
