#pragma once

/**
 * \brief Class to calculate a linear regression (single pass only; i.e.
 * no iterative optimization)
 */
class LinearRegression
{
public:
	LinearRegression(std::vector<double>& x_values,
	                 std::vector<double>& y_values);

	/**
	 * \brief Returns the slope
	 * \return
	 */
	double GetSlope() const;

	/**
	 * \brief return the offset
	 * \return
	 */
	double GetOffset() const;

	/**
	 * \brief return the R� value
	 * \return
	 */
	double GetRSquared() const;

	/**
	 * \brief return the RMS of the residuals
	 * \return
	 */
	double GetResidualsRMS() const;

private:

	int CalculateRegression();

	double slope_;
	double offset_;
	double residual_rms_;
	double r_squared_;

	std::vector<double> x_;
	std::vector<double> y_;
	std::vector<double> y_fit_;
	std::vector<double> residuals_;
};
