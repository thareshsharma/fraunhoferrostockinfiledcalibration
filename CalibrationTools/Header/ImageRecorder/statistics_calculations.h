#pragma once

/**
 * \brief Class for some basic statistic calculations on vectors of double input data
 */
class StatisticsCalculations
{
public:
	/**
	 * \brief copies the input to \ref data_no calculation is done here
	 */
	StatisticsCalculations(std::vector<double>& input);

	/**
	 * \brief calculates the average value of either \ref data_ or
	 * the values provided via the pointer
	 * \param values
	 * \return
	 */
	double CalculateAverage(std::vector<double>& values);

	/**
	 * \brief calculates the median value of either \ref data_ or
	 * the values provided via the pointer
	 * \param values
	 * \return
	 */
	double CalculateMedian(std::vector<double>& values);

	/**
	 * \brief calculates the Root-Mean-Square value of either \ref data_
	 * or the values provided via the pointer
	 * \param values
	 * \return
	 */
	double RMS(std::vector<double>& values);

	/**
	 * \brief 	calculates the distribution values for either \ref data_
	 * or the values provided via the pointer. The return value
	 * contains 1-sigma, 2-sigma, 3-sigma, 4-sigma
	 * \param values
	 * \param offset
	 * \return
	 */
	std::array<double, 4> CalculateSigma(std::vector<double>& values,
	                                     double offset = 0);

	/**
	 * \brief
	 * \param sigma
	 * \param values
	 * \return
	 */
	double CalculateSigma(int sigma,
	                      std::vector<double>& values);

	/**
	 * \brief
	 * \return
	 */
	std::array<double, 4> CalculateSigmaToMedian();

private:

	std::vector<double> data_;
	double rms_;
	double average_;
	double median_;
	std::array<double, 4> sigma_;
};
