#pragma once

namespace ProcessRegions
{
std::vector<int> vertical_tilings = {300, 600, 1000};
int vertical_boarder              = 15;
int horizontal_split_factor       = 15;
}
