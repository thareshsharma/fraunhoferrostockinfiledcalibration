#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>

namespace CameraCalibration
{
class SubPixel
{
public:
	/**
	 * \brief Sub-pixel position for the maximal correlation around pix.
	 * First two elements of the result is the interpolated pixel.
	 * \param image
	 * \param pixel
 	 * \param patch_size
	 * \return
	*/
	static cv::Vec<float, 7> subPixel(const cv::Mat& image,
	                                  const cv::Point& pixel,
	                                  int patch_size);

	/**
	 * \brief
	 * \param pyramids
	 * \param pyr_level
	 * \param corners
	 */
	static void subPixel(const std::vector<cv::Mat>& pyramids,
	                     int pyr_level,
	                     std::vector<cv::Point2f>& corners);

	/**
	 * \brief
	 * \param pyramids
	 * \param pyr_level
	 * \param keys
	 */
	static void subPixel(const std::vector<cv::Mat>& pyramids,
	                     int pyr_level,
	                     std::vector<cv::KeyPoint>& keys);
};
}
