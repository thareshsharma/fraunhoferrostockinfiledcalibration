#pragma once

class ImageClass
{
public:
	explicit ImageClass(int bright_pixel_threshold = 100);

	void AddImage(unsigned char* image,
	              int rows,
	              int cols,
	              size_t counter);

	void PopImage();

	float ReturnLastDuration();

	float ReturnLastBrightFraction(int total_number_of_pixels = 2 * 1280 * 1024);

	float ReturnLastSaturation();

	size_t ReturnCounter();

	cv::Mat GrabImage(bool pop = true);

	void GrabCurrentImage(cv::Mat* output);

	bool IsLaserImage(int threshold = 2000);

	bool IsLedImage();

	size_t GetSize() const;

private:

	struct ImageContainer
	{
		std::chrono::duration<float> time{};
		cv::Mat image;
		size_t counter{};
		float saturation{};
		int is_laser{};
		int bright_count{};
	};

	float GetImageSaturation(int threshold = 50);

	static bool FindDots(const cv::Mat& image,
	                     int threshold);

	std::queue<ImageContainer> images_;
	std::mutex mutex_;
	std::chrono::steady_clock::time_point time_reference_;
	int bright_pixel_threshold_;
};
