#pragma once

class MeasurementAnalyzer
{
public:
	void AddMainValues(long position,
	                   std::vector<double> values);

private:
	std::map<long, std::vector<double>> main_measurements_;

	struct MinimumContainer
	{
		double value;
		std::vector<long> positions_with_minimum;
	};

	//keep track of the minimum and its position (shaky implementation!!)
	double minimum_tolerance_{0.05};

	std::vector<MinimumContainer> minimum_radius_;
	//std::map<int , std::map<long, double>>
};
