#pragma once

/**
 * \brief
 */
class ApplicationSettings
{
public:
	ApplicationSettings(int ac,
	                    char* av[]);

	~ApplicationSettings();

	char time_buffer[100];

	bool record;
	bool store_every_image;
	bool dd_exp1;
	bool dd_exp2;
	bool dd_exp3;
	bool dd_scalar1;
	bool dd_scalar2;
	bool distortion;
	bool extrinsic;
	bool focal_length;

	std::string application_name;
	std::string path_to_setting_file;
	std::string calibration_files_folder;
	std::string calibration_filename;
	std::string serial_no;
	std::string storage_path;
	std::string board_files_path;
	std::string config_file_path;

	double depth_dependent_exp1;
	double depth_dependent_exp2;
	double depth_dependent_exp3;
	double depth_dependent_scalar1;
	double depth_dependent_scalar2;
	double min_led_fraction;
	double max_led_saturation;

	int laser_dot_threshold;
	int images_to_record;
	int valid_image_count;

	float image_interval;
	float doublet_tolerance;
};
