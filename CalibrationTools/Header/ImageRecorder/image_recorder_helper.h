#pragma once

#include "dmvs_sdk.h"

namespace ImageRecorder
{
/**
* \brief
* \param device
* \param prefix
* \return
*/
std::string GetConfigFileName(dmvs_sdk::DMVSDevice* device,
                              const std::string prefix);

/**
* \brief
* \param device
* \param prefix
* \return
*/
bool SaveSettings(dmvs_sdk::DMVSDevice* device,
                  const std::string prefix);

/**
* \brief
* \param device
* \param prefix
* \return
*/
bool LoadSettings(dmvs_sdk::DMVSDevice* device,
                  const std::string prefix);
}
