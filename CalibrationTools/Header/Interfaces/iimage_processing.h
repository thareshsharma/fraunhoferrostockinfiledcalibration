#pragma once

#include "circular_marker.h"

namespace DMVS
{
namespace CameraCalibration
{
class IImageProcessing
{
public:
	virtual ~IImageProcessing() = default;

	virtual void DetectBlob(const cv::Mat& image,
	                        const cv::Mat& contour_search_image,
	                        int threshold,
	                        CircularMarkerVector& centers) = 0;

	virtual void DetectBlob(const cv::Mat& image,
	                        CircularMarkerVector& key_points) = 0;
	virtual void DrawRings() = 0;
	virtual void DrawCenters() = 0;
	virtual void Reset() = 0;
	virtual void DetectMarkers() = 0;
};
}
}
