#pragma once

namespace DMVS
{
namespace CameraCalibration
{
class ICameraCalibration
{
	//Add methods for different steps for doing camera calibration
public:
	virtual ~ICameraCalibration() = default;
	virtual void Initialization() = 0;
	virtual void PreProcessing() = 0;
	virtual void Processing() = 0;
	virtual void PostProcessing() = 0;
	virtual void CalculateStatistics() = 0;
	virtual void WriteOutputFiles() = 0;
	virtual void SaveResults() = 0;
	virtual void CreatePDFReport() = 0;
	virtual void CreateJSON() = 0;
	virtual void UpdateCalibration() = 0;
};
}
}
