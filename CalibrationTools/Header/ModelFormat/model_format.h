#pragma once

/**
 * \brief Class for holding different formats
 */
class ModelFormat
{
public:
	static const wchar_t END_OF_BLOCK;
	static const wchar_t START_OF_BLOCK;
	static const std::string END_OF_BLOCK_STR;
	static const std::string START_OF_BLOCK_STR;
	static const std::string XML_HEADER;
	static const std::string XML_V2_COMMENT;
	static const std::string XML_NEXT_ROOT;
	static const std::string XML_START_COMMENT;
	static const std::string XML_END_COMMENT;
	static const std::string XML_NEXT_VERSION;
	static const std::string XML_NEXT_ROOT_MODEL;
	static const std::string XML_ROOT_START_TAG;
	static const std::string XML_ROOT_END_TAG;
	static const std::string XML_FOOTER;

	enum Format
	{
		FORMAT_FARO = 0,
		FORMAT_XML_V1 = 1,
		FORMAT_XML_V2 = 2
	};
};