#pragma once

#include "read_context_base.h"
#include "model_in_format.h"

/**
 * \brief Template to define input_ of arbitrary formats
 */
template <class T>
class DataInFormat
{
	typedef T value_type;

public:
	explicit DataInFormat(std::istream* in,
	                      const ModelFormat::Format format = ModelFormat::FORMAT_FARO) :
		input_(*in),
		format_(format)
	{
		assert(in);
	}

	/**
	 * \brief Return decoded value
	 */
	const value_type& Get()
	{
		return storage_;
	}

private:
	std::istream& input_;
	value_type storage_;
	ModelFormat::Format format_;
};

template <>
inline const std::string& DataInFormat<std::string>::Get()
{
	// this method must behave differently in XML v.2 input_ mode:
	if(format_ == ModelFormat::FORMAT_XML_V2)
	{
		storage_ = "";
		std::string cpp_str;

		// remove the whitespaces before the start of a string
		// (just like in the old version, see below)
		input_ >> std::ws;
		char ch(input_.get());

		// for example at the start of a |<OctVisFilterField value="'OctVisFilter'" />
		// element, a < can't appear in a value="..." attribute since it's always &lt;
		// in this case
		if(ch == '<')
		{
			// get the first char of the tag name
			input_.get(ch);

			while(input_.good() && ch != ' ')
			{
				// don't loop forever if there is an error condition
				// is std::string::EMPTY
				if(ch == -1 && !input_.good())
					return storage_;

				cpp_str.append(1, ch);
				input_.get(ch);
			}

			// now skip the value=" until the ':
			// also discards the ' therefore...
			input_.ignore(INT_MAX, '\'');

			// ... put it back
			input_.putback('\'');
		}
		else
		{
			assert(ch == '\'');
			// something is wrong...
			if(ch != '\'')
			{
				input_.putback(ch);

				// nothing got written
				return nullptr;
			}

			// input_ is now e.g. at <Name value="'|Position'" />
			// get the first char of the tag name
			input_.get(ch);

			while(input_.good() && ch != '\'')
			{
				// don't loop forever if there is an error condition
				// is std::string::EMPTY
				if(ch == -1 && !input_.good())
					return storage_;

				cpp_str.append(1, ch);
				input_.get(ch);
			}
			// the ' should not be put back
			// input_ is now e.g. at <Name value="'Position'|" />
		}

		if(cpp_str.empty() == false)
		{
			storage_ = std::string(cpp_str.c_str());
			ModelInFormat::DecodeXMLEntities(storage_);
			// this includes special handling for non-ASCII Unicode strings.
			//const int unknown = storage_.EscToControls();
		}
	}
	else
	{
		char ch;
		input_ >> std::ws >> ch;
		assert(ch == '\"');

		if(ch != '\"')
		{
			input_.putback(ch);
			// nothing got written
			return nullptr;
		}

		// Second character, first '\"' already was read
		ch = input_.get();
		// Using an std::basic_string<char> here to route the input_ character string
		// through wide string conversion later on.
		std::string cstr;

		// the loop is needed since \" might represent " and we want to recognize the end of the string
		bool backslash = false;

		while(input_.good() && (ch != '\"' || backslash))
		{
			if(ch == -1 && !input_.good())
			{
				// Don't loop for ever if there is an error condition
				storage_ = "";
				return storage_;
			}

			if(ch == '\\')
				backslash = !backslash;
			else
				backslash = false;

			cstr.append(1, ch);
			ch = input_.get();
		}

		// for an empty string we want to reuse the static std::string::EMPTY instance to save memory
		storage_ = "";
		if(!cstr.empty())
		{
			// we have a content
			storage_ = std::string(cstr.c_str()); // conversion to wide string

			//commented out, old files contain locale-specific non-ascii characters.
			// dynamic_assert(storage_.isAscii() || !input_.good());
			//const int unknown = storage_.EscToControls(); // this includes special handling for non-ASCII Unicode strings.
			// if unknown is != -1, we might have a legacy pre-Unicode file
			// but we don't need a dynamic_assert(unknown == -1) because of this,
			// as this will hinder us in debugging real problems!
		}
	}

	return storage_;
}

template<>
inline const Version& DataInFormat<Version>::Get()
{
	DataInFormat<std::string> fs(&input_, format_);

	if(format_ == ModelFormat::FORMAT_XML_V2)
	{
		// is looks like:
		// <!-- 'iQLib' 4.8.1001.0 -->
		// <FARO>
		input_ >> std::ws;
		input_.ignore(4); // skip <!--

		// set id, for example "iQLib":
		std::string id(fs.Get());
		storage_.SetId(id);
		// set version number, for example 4.8.1001.0:

		storage_.SetVersionNumbersFrom(input_);

		input_.ignore(INT_MAX, '\n'); // skip the rest of the line
	}
	else
	{
		// is looks like:
		// "iQLib" 4.8.1001.23823
		std::string header = fs.Get();
		storage_.SetId(header);
		storage_.SetVersionNumbersFrom(input_);
	}

	return storage_;
}


/**
 * \brief Specialization for vectors
 */
template <class ValueT, class AllocT>
class DataInFormat<std::vector<ValueT, AllocT>>
{
	typedef std::vector<ValueT, AllocT> value_type;

public:
	explicit DataInFormat(std::istream* in,
	                      const ModelFormat::Format format = ModelFormat::FORMAT_FARO) :
		input_(*in),
		format_(format)
	{
		assert(in);
	}

	/**
	 * \brief Return decoded value
	 */
	const value_type& Get()
	{
		return storage_;
	}

private:
	std::istream& input_;
	value_type storage_;
	ModelFormat::Format format_;
};

/**
 *\brief Specialization for sets
 */
template <class ValueT, class TraitsT, class AllocT>
class DataInFormat<std::set<ValueT, TraitsT, AllocT>>
{
	typedef std::set<ValueT, TraitsT, AllocT> value_type;

public:
	explicit DataInFormat(std::istream* in,
	                      const ModelFormat::Format format = ModelFormat::FORMAT_FARO) :
		input_(*in),
		format_(format)
	{
		assert(in);
	}

	/**
	 * \brief Return decoded value
	 */
	const value_type& Get()
	{
		return storage_;
	}

private:
	std::istream& input_;
	value_type storage_;
	ModelFormat::Format format_;
};
