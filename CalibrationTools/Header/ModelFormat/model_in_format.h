#pragma once

#include "read_context_base.h"
#include "version.h"

/**
 * \brief
 */
class ModelInFormat : public ModelFormat
{
public:
	ModelInFormat(std::istream* stream);

	~ModelInFormat();

	ModelInFormat(const ModelInFormat& rhs) = delete;

	ModelInFormat& operator=(const ModelInFormat&) = delete;

	/**
	 * \brief Before a load method is allowed to call readHeader
	 * it has to determine the format of the document with this
	 * method which sets the format attribute of the ModelInFormat
	 * and return the format value
	*/
	Format DetermineFormat();

	/**
	 * \brief getKeyword: If a keyword is present, returns keyword,
	 * otherwise returns the next keyword. Returns 0 in the case,
	 * that there is no further keyword.
	 */
	std::string GetKeyword();

	/**
	 * \brief Static assistant method for nextKeyword()
	 * \param c
	 * \return
	 */
	wchar_t CharToWchar_t(const char& c) const;
	/**
	 *\brief Go on to find the next keyword.
	 */
	void NextKeyword();

	/**
	 * \brief
	 * \return
	 */
	bool SkipRestOfLine();

	/**
	 * \brief
	 * \return
	 */
	bool SkipToEndOfBlock();

	/**
		Resets the propagateKeyword to NIL. Has to be called after
		reading a file containing serialization information that is
		not a complete object contained in <FARO></FARO> tags, for
		example when reading an object exchange document. Example usage:

		result = obj->readFrom(is, readContext);
		ModelInFormat::cleanPropagateKeyword();

		and the input stream is looks like this:

		<AttrContainer>
		<Attr type="double">
		<Name value="'ScanSymbolHeight'" />
		<Value value="2.5" />
		</Attr>
		</AttrContainer>
	*/
	static void CleanPropagateKeyword();

	/**
	 * \brief
	 * \param in
	 */
	void SetPropagateKeyword(const std::string& in);

	/**
	 * \brief Checks for the end of block identifier.
	 * \return
	 */
	bool IsEndOfBlock();

	/**
	 * \brief Peeks or extracts a single character from the input sequence.
	 * \param no_peek Set to false to peek for the character, meaning the
	 * read character is not removed from the input sequence.
	 * \return A read character of type char, or std::char_traits<char>::eof()
	 * in case a wide character conversion was needed but not possible.
	 * In this error state the stream is set to std::ios::failbit state .
	 */
	int RawRead(const bool no_peek = true);

	/**
	 * \brief Reads the header of a file.
	 * \param header An empty String
	 */
	void ReadHeader(std::string& header) const;

	/**
	 * \briefReads the version of a file.
	 * \param version An empty VersionNum (subclass of String)
	*/
	void ReadVersion(Version& version) const;

	/**
	 * \briefReads the root model version of a file
	 * \param version An empty VersionNum
	*/
	void ReadRootModelVersion(Version& version);

	/**
	 * \brief\brief	Reads the Faro start tag (Only XML).
	 * \return	True if Faro start tag was existing.
	*/
	bool ReadFaroStart();

	/**
	 * \brief\brief	Reads the Faro end tag (Only XML).
	 * \return	True if Faro end tag was existing.
	*/
	bool ReadFaroEnd();

	/**
	 * \brief Replaces &lt; --> < ### &gt; --> > ### &quot --> " ### &apos; --> ' ### &amp; --> &
	 * \param to_decode String to decode
	*/
	static void DecodeXMLEntities(std::string& to_decode);

	/**
	 * \brief Replaces &amp;lt; --> &lt; ### &amp;gt; --> &gt; ### &amp;quot --> &quot ### &amp;apos; --> &apos; ### &amp;amp; --> &amp;
	 * \param to_decode String to decode
	*/
	static void DecodeEscapedXMLEntities(std::string& to_decode);

	/**
	 * \brief
	*/
	Format GetFormat() const;

	/**
	 * \brief
	 */
	void SetFormat(Format format);

	/**
	 * \briefSpecial handling of reading a single whitespace before blocks of byte data in the old Faro format
	 */
	void ReadBeforeBuf();

private:
	/**
	 * Assistant method for nextKeyword() that reads the next key (depending
	 * on what relative position of the XML file it is) and puts it in the keyword
	 * attribute.
	 * A portion of the XML file could look like this, for example:

		<Child>
			<MeasureObject value="MeasureObject1" />
			<InstanceID value="33062016" />
			<MeasureLine>
				<Polygon3d value="3 -1.065286636 -0.5554655194 0.5585538745 ..." />
				...
			</MeasureLine>
			<AttrContainer>
				<Attr type="String">
					<Name value="Name" />
					<Value value="MeasureObject1" />
				</Attr>
				...
			</AttrContainer>
		</Child>

		with the "MeasureObject" of <MeasureObject ... being a String
		while the original structured storage format looks like this:

		Child
		{
			"MeasureObject" "MeasureObject1"
			InstanceID 33062016
			MeasureLine
			{
				Polygon3d 3 -1.065286636 -0.5554655194 0.5585538745 ...
				...
			}
			AttrContainer
			{
				Attr<String>
				{
					Name "Name"
					Value "MeasureObject1"
				}
				...
			}
		}
	*/
	void NextKeywordFromXMLV2();

	/**
	 * \brief reference to the stream the object operates on.
	 */
	std::istream& input_;

	/**
	 * \briefcurrently stored keyword read by the instance.
	 */
	std::string keyword_;

	/**
	 * \brief an offset into keyword from where the keyword is valid, or -1 when no keyword was read so far.
	 */
	int keyword_index_;

	Format format_;
};
