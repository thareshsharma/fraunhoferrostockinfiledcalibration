#pragma once

/**
 * \brief
 */
enum ImageChannel : size_t
{
	//Data given from depth generator.
	CHANNEL_POINT_CLOUD = 0,

	//XYZ in meters (CV_32FC3)
	CHANNEL_DEPTH_MAP = 1,

	//Depth values in mm (CV_16UC1)
	//rc in pixel, XYZ in meter, RGB in [0,1]^3
	//matrix 8xn. One row per colored 3D value
	//Data given from RGB image generator.
	CHANNEL_SPARSE_CLOUD = 2,

	//for use as SFMCamera, as sfm camera does not yet support table based correction.
	CHANNEL_BGR0 = 10,

	//same image as BGR0 but raw bayer pattern, may not exist in files !
	CHANNEL_BGR1 = 11,

	CHANNEL_BAYBG0 = 12,

	CHANNEL_GREY0 = 20,

	CHANNEL_GREY1 = 21,

	CHANNEL_PROJECTOR0 = 31,

	//Gray scale version of RGB image (returns Y channel if YUV image)
	CHANNEL_RGB_GRAY = 40,

	//Features coordinates, <float x,float y>
	CHANNEL_FEATURES = 41,

	//Features descriptors,  16 bytes per descriptor, same number and order as features
	CHANNEL_DESCRIPTORS = 42,

	//Mainly for debugging reasons: access to dots data of IR camera 1
	CHANNEL_DOTS_0 = 43,

	//Mainly for debugging reasons: access to dots data of IR camera 2
	CHANNEL_DOTS_1 = 44,

	//JPEG compressed RGB image
	CHANNEL_RGB_JPEG = 45,

	//IMU Data if the sensor has one. First three values are gravity direction always.
	//Rest is device dependent. Transformation from IMU sensor system to
	//Freestyle sensor system in calibration
	CHANNEL_IMU = 50,

	//Transformation for IMU world system to Freestyle system. Provided
	//by the NGIMU for example.
	CHANNEL_IMU_WORLD = 51,

	//Intrinsic calibration data. Used for information only.
	CHANNEL_IMU_INTRINSIC = 52,

	//Second IMU - device depended
	CHANNEL_IMU2 = 53,

	//Third IMU - device depended
	CHANNEL_IMU3 = 54,

	//Exposure information in an attribute container
	CHANNEL_INFO = 60,

	//Tracking information in an attribute container
	CHANNEL_PROTOCOL = 61,

	CHANNEL_DEFAULT = 70
};
