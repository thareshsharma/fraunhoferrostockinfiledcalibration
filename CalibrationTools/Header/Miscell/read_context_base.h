#pragma once

#include "model_format.h"

/**
 * \brief ReadContextBase provides the basis for a collection of parameterization
 * of the Observer::ReadFrom algorithm
 */
class ReadContextBase
{
public:
	ReadContextBase(ModelFormat::Format format = ModelFormat::FORMAT_FARO);

	/**
	 * \brief Disallows copy-construction
	 * \param rhs
	 */
	ReadContextBase(const ReadContextBase& rhs) = delete;

	virtual ~ReadContextBase() noexcept;

	/**
	 * \brief Disallows assignment
	 * \param rhs
	 * \return
	 */
	ReadContextBase& operator=(const ReadContextBase& rhs) = delete;

	/**
	 * \brief Disallows move-construction
	 * \param rhs
	 */
	ReadContextBase(const ReadContextBase&& rhs) = delete;

	/**
	 * \brief Disallows move assignment
	 * \param rhs
	 * \return
	 */
	ReadContextBase& operator=(const ReadContextBase&& rhs) = delete;

	/**
	 * \brief
	 * \return
	 */
	bool ReadDemandData() const;

	/**
	 * \brief
	 * \param read_demand_data
	 */
	void SetReadDemandData(bool read_demand_data);

	/**
	 * \brief
	 * \return
	 */
	bool ReadMarked() const;

	/**
	 * \brief
	 * \param read_marked
	 */
	void SetReadMarked(bool read_marked);

	/**
	 * \brief
	 * \return
	 */
	bool ReadEncrypted() const;

	/**
	 * \brief
	 * \param read_encrypted
	 */
	void SetReadEncrypted(bool read_encrypted);

	/**
	 * \brief
	 * \return
	 */
	bool UpdateObject() const;

	/**
	 * \brief
	 * \param update_object
	 */
	void SetUpdateObject(bool update_object);

	// Format
	/**
	 * \brief
	 * \return
	 */
	ModelFormat::Format GetFormat() const;

	/**
	 * \brief
	 * \param format
	 */
	void SetFormat(ModelFormat::Format format);

protected:
	/**
	 * \brief Defines, if demand data are read from demand load pool.
	 */
	bool flag_read_demand_data_ : 1;

	/**
	 * \brief Defines, if only marked attrs are read
	 */
	bool flag_read_marked_ : 1;

	/**
	 * \brief Defines, if the reading is done to update an already existing object.
	 */
	bool flag_update_object_ : 1;

	/**
	 * \brief Defines, if the reading is done from an encrypted file.
	 */
	bool flag_read_encrypted_ : 1;

private:
	/**
	 * \brief FormatXMLv2 is default format
	 */
	ModelFormat::Format format_;
};
