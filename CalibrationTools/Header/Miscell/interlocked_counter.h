#pragma once

#include "typedefs.h"

class InterlockedCounter
{
public:
	/**
	 * \brief default construct sets the counter to 0.
	 */
	InterlockedCounter() noexcept : counter_(0u)
	{
	}

	/**
	 * \brief  constructs and initializes the counter to a given value.
	 * \param value of counter initially.
	 */
	explicit InterlockedCounter(const LONG value) noexcept : counter_(value)
	{
	}

	/**
	 * \brief Disallows copy-construction
	 */
	InterlockedCounter(const InterlockedCounter& rhs) = default;

	/**
	 * \brief Disallows assignment
	 * \return
	 */
	InterlockedCounter& operator=(const InterlockedCounter& rhs) = default;

	/**
	 * \brief Disallows move copy construction
	 */
	InterlockedCounter(InterlockedCounter&& rhs) = delete;

	/**
	 * \brief Disallows move assignment
	 * \return
	 */
	InterlockedCounter& operator=(InterlockedCounter&& rhs) = delete;

	/**
	 * \brief returns the current counter value.
	 */
	operator ValueType() const noexcept
	{
		return counter_;
	}

	/**
	 * \brief assignment resets the counter to a given value.
	 * \param value
	 * \return
	 */
	ValueType operator=(const ValueType value) noexcept
	{
		InterlockedExchange(&counter_, value);
		return value;
	}

	/**
	 * \brief prefix-increment of counter by 1.
	 * \return
	 */
	ValueType operator++() noexcept
	{
		return InterlockedIncrement(&counter_);
	}

	/**
	 * \brief prefix-increment of counter by 1.
	 * \return
	 */
	ValueType operator++(int) noexcept
	{
		return InterlockedIncrement(&counter_) - 1;
	}

	/**
	 * \brief prefix-decrement of counter by 1.
	 * \return
	 */
	ValueType operator--() noexcept
	{
		return InterlockedDecrement(&counter_);
	}

	/**
	 * \brief postfix-decrement of counter by 1.
	 * \return
	 */
	ValueType operator--(int) noexcept
	{
		return InterlockedDecrement(&counter_) + 1;
	}

	/**
	 * \brief increment counter by value
	 * \param value
	 * \return
	 */
	ValueType operator+=(const ValueType value) noexcept
	{
		return InterlockedExchangeAdd(&counter_, value);
	}

private:
	/**
	 * \brief  current counter value.
	 */
	volatile LONG counter_;
};
