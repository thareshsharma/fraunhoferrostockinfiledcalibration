#pragma once

#include "calibration_data.h"

namespace fs = boost::filesystem;
using namespace DMVS::CameraCalibration;

class CalibrationXMLParser
{
public:
	explicit CalibrationXMLParser(fs::path xml_file_path);

	~CalibrationXMLParser();

	/**
	 * \brief Forcing a copy constructor to be deleted by the compiler.
	 */
	CalibrationXMLParser(const CalibrationXMLParser& rhs) = delete;

	/**
	 * \brief Forcing a assignment operator to be deleted by the compiler.
	 */
	CalibrationXMLParser& operator=(const CalibrationXMLParser& rhs) = delete;

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	CalibrationXMLParser(CalibrationXMLParser&& rhs) = delete;

	/**
	 * \brief Forcing a move assignment operator to be deleted by the compiler.
	 */
	CalibrationXMLParser& operator=(CalibrationXMLParser&& rhs) = delete;

	/**
	 * \brief Gets serial number from the calibration file
	 * \return
	 */
	std::string GetSerialNumber() const;

	/**
	 * \brief Gets calibration time stamp from the calibration file
	 * \return
	 */
	double GetCalibrationTimestamp() const;

	/**
	 * \brief
	 */
	void SaveCalibrationFile(bool append_header      = true,
	                         const std::string& name = "",
	                         bool if_create_new      = false);

	///**
	// * \brief Gets single camera calibration from the calibration file
	// * \return
	// */
	//cv::Mat GetSingleCameraCalibration() const;

	/**
	 * \brief Gets custom matrices from the calibration file
	 * \return
	 */
	cv::Mat GetCustomMatrices() const;

	/**
	 * \brief Sets custom matrices from the calibration file
	 * \return
	 */
	void SetCustomMatrices(const cv::Mat& pattern_definition) const;

	/**
	 * \brief
	 * \param channel
	 * \param distortion_vector
	 */
	void GetDistortionVector(int channel,
	                         cv::Mat& distortion_vector) const;

	/**
	 * \brief
	 * \param channel
	 * \param distortion_vector
	 */
	void SetDistortionVector(int channel,
	                         const cv::Mat& distortion_vector) const;

	/**
	 * \brief
	 * \param channel
	 * \param camera_matrix
	 */
	void GetCameraMatrix(int channel,
	                     cv::Mat& camera_matrix) const;

	/**
	 * \brief
	 * \param channel
	 * \param camera_matrix
	 */
	void SetCameraMatrix(int channel,
	                     const cv::Mat& camera_matrix) const;

	/**
	 * \brief
	 * \param channel
	 * \param rotation_matrix
	 */
	void GetRotationMatrix(int channel,
	                       cv::Mat& rotation_matrix) const;

	/**
	 * \brief
	 * \param channel
	 * \param rotation_matrix
	 */
	void SetRotationMatrix(int channel,
	                       const cv::Mat& rotation_matrix) const;

	/**
	 * \brief
	 * \param channel
	 * \param translation_vector
	 */
	void GetTranslationVector(int channel,
	                          cv::Mat& translation_vector) const;

	/**
	 * \brief
	 * \param channel
	 * \param translation_vector
	 */
	void SetTranslationVector(int channel,
	                          const cv::Mat& translation_vector) const;

	/**
	 * \brief
	 * \param channel
	 * \param depth_params
	 */
	void GetDepthDependentScalarParameters(int channel,
	                                       cv::Vec2d& depth_params) const;

	/**
	 * \brief
	 * \param channel
	 * \param depth_params
	 */
	void SetDepthDependentScalarParameters(int channel,
	                                       const cv::Vec2d& depth_params);

	/**
	 * \brief
	 * \param channel
	 * \param depth_params
	 */
	void GetDepthDependentExponentialParameters(int channel,
	                                            cv::Vec3d& depth_params) const;

	/**
	 * \brief
	 * \param channel
	 * \param depth_params
	 */
	void SetDepthDependentExponentialParameters(int channel,
	                                            const cv::Vec3d& depth_params);

	/**
	 * \brief
	 * \param calibration_data
	 */
	void GetCalibrationData(CalibrationData& calibration_data) const;

private:
	/**
	 * \brief Gets value attribute of the Attr tag
	 * \param attribute the attribute for which the value has to be extracted
	 * \return
	 */
	std::string GetValueAttributeOfAttrTag(const std::string& attribute) const;
	fs::path calibration_file_path_;
	pugi::xml_document document_;
};
