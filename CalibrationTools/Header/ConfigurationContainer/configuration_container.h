#pragma once

#include "helper.h"
namespace fs = boost::filesystem;

class ConfigurationContainer
{
public:
	ConfigurationContainer();

	~ConfigurationContainer();

	/**
	 * \brief Forcing a copy constructor to be deleted by the compiler.
	 */
	ConfigurationContainer(const ConfigurationContainer& rhs) = delete;

	/**
	 * \brief Forcing a assignment operator to be deleted by the compiler.
	 */
	ConfigurationContainer& operator=(const ConfigurationContainer& rhs) = delete;

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	ConfigurationContainer(ConfigurationContainer&& rhs) = delete;

	/**
	 * \brief Forcing a move assignment operator to be deleted by the compiler.
	 */
	ConfigurationContainer& operator=(ConfigurationContainer&& rhs) = delete;

	void Initialize();

	// Board Id
	int GetBoardId() const;
	void SetBoardId(int board_id);

	// First UncodedId
	int GetFirstUncodedId() const;
	void SetFirstUncodedId(int uncoded_id);

	//Marker Index 3d
	int GetMarkerIndex3d() const;
	void SetMarkerIndex3d(int marker_index3d);

	// Coordinate System Definition
	int GetCoordinateSystemDefinition() const;
	void SetCoordinateSystemDefinition(int coordinate_system_definition);

	// Calibration Board Count
	int GetCalibrationBoardsCount() const;
	void SetCalibrationBoardsCount(int calibration_boards_count);

	// Calibration Configuration Data File
	fs::path GetConfigurationDataFile() const;
	void SetConfigurationDataFile(const std::string& configuration_data_file);

	// Calibration Data Root
	std::string GetCalibrationDataRoot() const;
	void SetCalibrationDataRoot(const std::string& data_root);

	//PDF Report File Name
	std::string GetPDFReportFileName() const;
	void SetPDFReportFileName(const std::string& file_name);

	// Infield Report Logo path
	std::string GetInfieldReportLogoPath() const;
	void SetInfieldReportLogoPath(const std::string& logo_path);

	// Color Camera RMS
	double GetColorCameraRMS() const;
	void SetColorCameraRMS(double camera_rms);

	// Left Camera RMS
	double GetLeftCameraRMS() const;
	void SetLeftCameraRMS(double left_camera_rms);

	// Right Camera RMS
	double GetRightCameraRMS() const;
	void SetRightCameraRMS(double right_camera_rms);

	// Factory Calibration Max RMS
	double GetFactoryCalibrationMaxRMS() const;
	void SetFactoryCalibrationMaxRMS(double factory_calibration_max_rms);

	//// Factory Calibration Successful
	//bool GetFactoryCalibrationSuccessful() const;
	//void SetFactoryCalibrationSuccessful(bool if_successful);

	// Content
	std::string& GetContent();
	void SetContent(const std::string& content);

	float GetFactoryCalibrationDistance() const;
	void SetFactoryCalibrationDistance(float factory_calibration_distance);

	// On-site calibration
	float GetOnsiteCalibrationMinDistance() const;
	void SetOnsiteCalibrationMinDistance(float onsite_calibration_min_distance);

	float GetOnsiteCalibrationStepDistance() const;
	void SetOnsiteCalibrationStepDistance(float onsite_calibration_step_distance);

	int GetOnsiteCalibrationNumSteps() const;
	void SetOnsiteCalibrationNumSteps(int onsite_calibration_num_steps);

	// Projector Calibration
	float GetProjectorCalibrationMinBeamLength() const;
	void SetProjectorCalibrationMinBeamLength(float projector_calibration_min_beam_length);

	int GetProjectorCalibrationMinBrightBeams() const;
	void SetProjectorCalibrationMinBrightBeams(int projector_calibration_min_bright_beams);

	float GetProjectorCalibrationCenterBeamWindowPositionX() const;
	void SetProjectorCalibrationCenterBeamWindowPositionX(float projector_calibration_center_beam_window_position_x);

	float GetProjectorCalibrationCenterBeamWindowPositionY() const;
	void SetProjectorCalibrationCenterBeamWindowPositionY(float projector_calibration_center_beam_window_position_y);

	int GetProjectorCalibrationCenterBeamWindowWidth() const;
	void SetProjectorCalibrationCenterBeamWindowWidth(int projector_calibration_center_beam_window_width);

	int GetProjectorCalibrationCenterBeamWindowHeight() const;
	void SetProjectorCalibrationCenterBeamWindowHeight(int projector_calibration_center_beam_window_height);

	int GetProjectorCalibrationMinPoints() const;
	void SetProjectorCalibrationMinPoints(int projector_calibration_min_points);

	bool GetProjectorCalibrationCenterBeamUseWindowCenter() const;
	void SetProjectorCalibrationCenterBeamUseWindowCenter(bool projector_calibration_center_beam_use_window_center);

	float GetProjectorCalibrationIRExposureAt1m() const;
	void SetProjectorCalibrationIRExposureAt1m(float projector_calibration_ir_exposure_at_1m);

	bool GetUseDMVSProjectorCalibration() const;
	void SetUseDMVSProjectorCalibration(bool use_dmvs_projector_calibration);

	// Scanning
	double GetFreestyleMinDistance() const;
	void SetFreestyleMinDistance(double freestyle_min_distance);

	float GetProjectorCalMinBeamLength() const;
	void SetProjectorCalMinBeamLength(float min_beam_length);

	float GetForceBeamX() const;
	void SetForceBeamX(float force_beam_x);

	float GetForceBeamY() const;
	void SetForceBeamY(float force_beam_y);

	bool GetForceBeam() const;
	void SetForceBeam(bool if_force_beam);

	int GetMinProjectorCalibrationPointsToFinish() const;
	void SetMinProjectorCalibrationPointsToFinish(int min_projector_calibration_points);

	float GetMaxDistSymmetryTest() const;
	void SetMaxDistSymmetryTest(float max_distance_symmetry_test);

	bool GetShowSymmetryProjection() const;
	void SetShowSymmetryProjection(bool show_symmetry_projection);

private:
	int board_id_;
	int first_uncoded_id_;
	int marker_index3d_;
	int coordinate_system_definition_;
	int calibration_boards_count_;

	std::string calibration_data_root_;
	fs::path configuration_data_file_;
	std::string pdf_report_file_name_;
	std::string infield_report_logo_path_;
	std::string content_;

	double color_camera_rms_;
	double left_camera_rms_;
	double right_camera_rms_;
	double factory_calibration_max_rms_;
	float factory_calibration_distance_;

	// On-site calibration
	float on_site_calibration_min_distance_;
	float on_site_calibration_step_distance_;
	int on_site_calibration_num_steps_;

	// Projector Calibration
	int projector_calibration_min_bright_beams_;
	int projector_calibration_center_beam_window_width_;
	int projector_calibration_center_beam_window_height_;
	int projector_calibration_min_points_;

	float projector_calibration_center_beam_window_position_x_;
	float projector_calibration_min_beam_length_;
	float projector_calibration_center_beam_window_position_y_;
	float projector_calibration_ir_exposure_at_1m_;

	bool projector_calibration_center_beam_use_window_center_;
	bool use_dmvs_projector_calibration_;

	// Scanning
	double freestyle_min_distance_;

	// Laser Projection
	bool if_force_beam_;
	bool show_symmetry_projection_;

	int min_projector_calibration_points_;

	float min_beam_length_;
	float force_beam_x_;
	float force_beam_y_;
	float max_distance_symmetry_test_;
};
