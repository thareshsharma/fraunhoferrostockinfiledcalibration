#pragma once

#include "single_camera_calibration.h"

using namespace DMVS::CameraCalibration;

class CodedRingCenterDetector
{
public:
	CodedRingCenterDetector(const cv::Mat& image);

	/**
	 *\brief writes the outer ellipse into out.
	 *\param inner_circle the ellipse describing the inner circle
	 *\param coding_angle
	 *\param out
	 *\param outer_scale the factor by which width and height need to be
	 * multiplied to lie approximately in the middle of the outer ring
	 *\param min_val the minimum grey value of the ellipse
	 *\param max_val the maximum grey value of the ellipse
	 *\return whether successful or not
	*/
	bool GetOuterEllipse(const cv::RotatedRect& inner_circle,
	                     double coding_angle,
	                     cv::RotatedRect& out,
	                     double outer_scale,
	                     double& min_val,
	                     double& max_val);

	//so far not used
	void UseDistortionCorrection(const SingleCameraCalibration& camera);

private:
	template <class T>
	bool GetOuterEllipseIntern(const cv::RotatedRect& in,
	                           double coding_angle,
	                           cv::RotatedRect& out,
	                           double outer_scale,
	                           double& min_val,
	                           double& max_val);

	// the original image
	cv::Mat image_;

	// the image with the filled ellipses
	cv::Mat filled_;

	// the ratio of the outer circle over the inner circle, approx. 5
	const double ratio_outer_vs_inner_radius_;
};
