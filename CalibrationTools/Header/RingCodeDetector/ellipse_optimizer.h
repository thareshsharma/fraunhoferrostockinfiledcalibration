#pragma once

/**
 * Ellipse fit based on "Precise ellipse estimation without contour point extraction",
 * Machine Vision and Applications
 * November 2009, Volume 21, Issue 1, pp 59-67
*/
class EllipseFitOptimizer
{
public:

	EllipseFitOptimizer(const cv::Mat& img);

	//angle exclude is defined in coordinate system: 0deg --> +col, 90deg --> +row
	cv::RotatedRect optimize(const cv::RotatedRect& in,
	                         double distance                                            = 0.0,
	                         const std::vector<std::pair<double, double>>& angleExclude = std::vector<std::pair<double, double>>(0),
	                         double* minVal                                             = 0,
	                         double* maxVal                                             = 0);
	bool success();

private:
	template <class T>
	cv::RotatedRect optimizeInternal(const cv::RotatedRect& in,
	                                 double distance                                            = 0.0,
	                                 const std::vector<std::pair<double, double>>& angleExclude = std::vector<std::pair<double, double>>(0),
	                                 double* minVal                                             = 0,
	                                 double* maxVal                                             = 0);

	struct Gradient
	{
		cv::Point2i coord;
		double gradientX;
		double gradientY;
	};

	const cv::Mat& m_img;

	// precalculated gradient matrices for entire image
	cv::Mat grad_x, grad_y;

	const double maxDistSq;
	static const size_t minNumGradients;
	bool m_success;
};