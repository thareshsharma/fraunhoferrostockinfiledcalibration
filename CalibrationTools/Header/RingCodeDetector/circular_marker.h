#pragma once

#include "constants.h"

namespace DMVS
{
namespace CameraCalibration
{
class CircularMarker
{
public:
	CircularMarker() :
		outer_radius_scale(1.0),
		inner_radius_scale(0.0),
		radius(0.0),
		angle(0.0),
		confidence(-1.0),
		id(Constants::DEFAULT_SIZE_T_VALUE),
		weight(1.0),
		raw_distance(0.0),
		center_correction(cv::Point2f(0.0f, 0.0f)),
		average_surrounding_area(255.0),
		average_inner_area(1.0)
	{
	}

	cv::RotatedRect CorrectedEllipse() const
	{
		auto corrected = raw_ellipse;
		corrected.center += center_correction;
		return corrected;
	}

	cv::Point2f CorrectedCenter() const
	{
		return raw_ellipse.center + center_correction;
	}

	//Canonical strict weak ordering such that this class can be used e.g. in std::set or std::multiset.
	friend bool operator <(const CircularMarker& lhs,
	                       const CircularMarker& rhs)
	{
		return std::tie(lhs.raw_ellipse.center.x,
		                lhs.raw_ellipse.center.y,
		                lhs.raw_ellipse.size.height,
		                lhs.raw_ellipse.size.width,
		                lhs.raw_ellipse.angle,
		                lhs.outer_radius_scale,
		                lhs.inner_radius_scale,
		                lhs.radius,
		                lhs.angle,
		                lhs.confidence,
		                lhs.id,
		                lhs.center_correction.x,
		                lhs.center_correction.y) <
			std::tie(rhs.raw_ellipse.center.x,
			         rhs.raw_ellipse.center.y,
			         rhs.raw_ellipse.size.height,
			         rhs.raw_ellipse.size.width,
			         rhs.raw_ellipse.angle,
			         rhs.outer_radius_scale,
			         rhs.inner_radius_scale,
			         rhs.radius,
			         rhs.angle,
			         rhs.confidence,
			         rhs.id,
			         rhs.center_correction.x,
			         rhs.center_correction.y
			);
	}

	/**
	 * \brief Ellipse as detected in image - raw ellipse detection result
	 */
	cv::RotatedRect raw_ellipse;

	/**
	 * \brief Radius of outer ring if outer ring has been detected and ellipse
	 * is inner circle (e.g. coded ring marker )
	 */
	double outer_radius_scale;

	/**
	 * \brief Radius of inner ring if outer ring has been detected and ellipse
	 * is outer ring ( e.g. coded ring marker )
	 */
	double inner_radius_scale;

	/**
	 * \brief Real world radius of circle ( in meter )
	 */
	double radius;

	/**
	 * \brief Angle of zero mark of outer ring (coded ring marker) [degree]
	 * (up- direction: un-rotated)
	 */
	double angle;

	/**
	 * \brief Quality value
	 */
	double confidence;

	/**
	 * \brief Detected marker ID, can distinct general black/white marker or
	 * IDs for coded marker
	 */
	size_t id;

	/**
	 * \brief Weight of this marker
	 */
	double weight;

	/**
	 * \brief Measured distance from point of observation, available for
	 * laser scanner only
	 */
	double raw_distance;

	/**
	 * \brief Correction of difference between CenterOfProjection and
	 * ProjectionOfCenter of projected circle.  Compensates errors caused by
	 * perspective projection and lens distortion
	 */
	cv::Point2f center_correction;

	/**
	 * \brief Pixel intensity average of the surrounding area (15 pixel)
	 */
	double average_surrounding_area;

	/**
	 * \brief Pixel intensity average of the marker
	 */
	double average_inner_area;
};
}
}
