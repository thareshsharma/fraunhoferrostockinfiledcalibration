#pragma once

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief For outer ring detection used for verification
 */
class RingCodeDecoder
{
public:
	RingCodeDecoder(const cv::Mat& image,
	                float outer_code_ring_scale,
	                int num_bits,
	                bool img_mirrored_in = false);

	/**
	 * \brief Decode circular coded marker
	 * \param ellipse for getting marker id
	 * \param outer_ring_scale : factor which connects outermost line of the marker with inner circle width/height
	 * \param middle_outer_ring_scale : factor which connects middle of outer ring with inner circle width/height
	 * \param angle : the angle where the marker code start mark is located
	 */
	int GetMarkerId(const cv::RotatedRect& ellipse,
	                double& outer_ring_scale,
	                double& middle_outer_ring_scale,
	                double& angle) const;

	/**
	 * \brief Decode circular coded marker
	 * \param ellipse : for getting pixel offset
	 * \param angle : factor which connects outermost line of the marker with inner circle width/height
	 * \param ring_scale factor which connects outermost line of the marker with inner circle width/height
	 */
	cv::Point2f GetEllipsePixelOffset(const cv::RotatedRect& ellipse,
	                                  float angle,
	                                  float ring_scale = 1.0f) const;

private:
	/**
	 * \brief decodes the img internally
	 * \param coded_image_full_size
	 * \param outer_ring_scale factor which connects outermost line of the marker with inner circle width/height
	 * \param angle the angle where the marker code start mark is located
	 * \param middle_outer_ring_scale factor which connects middle of outer ring with inner circle width/height
	 */
	int DecodeImage(cv::Mat& coded_image_full_size,
	                double& outer_ring_scale,
	                double& angle,
	                double& middle_outer_ring_scale) const;

	/**
	 * \brief Internal decode circular coded marker
	 * \param ellipse : for getting pixel offset
	 * \param is_outer
	 * \param angle rotation in rad
	 * \param outer_ring_scale factor which connects outermost line of the marker with inner circle width/height
	 * \param middle_outer_ring_scale factor which connects middle of outer ring with inner circle width/height
	 * \param angle the angle where the marker code start mark is located
	 */
	int GetIdInternal(const cv::RotatedRect& ellipse,
	                  bool is_outer,
	                  double& outer_ring_scale,
	                  double& middle_outer_ring_scale,
	                  double& angle) const;

	size_t FindMarkerIdFirst(const cv::Mat& coded_image,
	                         double& outer_ring_scale,
	                         double& middle_outer_ring_scale,
	                         double& deskewed_angle) const;

	static size_t MarkerIdsSanityCheck(const std::vector<int>& marker_ids);

	static bool IsScalingReasonable(const cv::Mat& patch,
	                                float vertical_scale,
	                                float horizontal_scale);

	void CalculateCodedImage(const cv::RotatedRect& ellipse,
	                         const cv::Mat& patch,
	                         float code_ring_scale,
	                         cv::Mat& coded_image) const;

	double CalculateMarkerAngle(const cv::RotatedRect& local_ellipse,
	                            const cv::RotatedRect& patch_ellipse,
	                            double rotation,
	                            double outer_ring_scale,
	                            float vertical_scale,
	                            float horizontal_scale,
	                            double deskewed_angle) const;

	bool GetBoundingRectangle(const cv::RotatedRect& ellipse,
	                          float outer_code_ring_scale,
	                          cv::Rect_<int>& bounding_rectangle) const;

	static void ScaleEllipse(cv::RotatedRect& local_ellipse,
	                         float vertical_scale,
	                         float horizontal_scale);

	void CalculatePatchRotated(const cv::Mat& patch,
	                           const cv::Point2f& center,
	                           float angle,
	                           cv::Mat& patch_rotated) const;

	bool GetEllipseAndScales(const cv::RotatedRect& ellipse,
	                         cv::RotatedRect& local_ellipse,
	                         const cv::Mat& patch,
	                         float& vertical_scale,
	                         float& horizontal_scale,
	                         cv::Mat& patch_rotated_resized) const;

	cv::Mat image_;

	float outer_code_ring_scale_;

	bool invert_bit_order_;
	bool stable_only_;

	int num_bits_;
	int step_threshold_;
	int min_threshold_;
	int max_threshold_;
	int max_id_;
	int min_hits_;
	int pixel_per_bit_;
	int num_rows_;
};
}
}
