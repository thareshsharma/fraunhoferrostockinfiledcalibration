#pragma once

#include "critical_section.h"

class ThreadDataType;

#define _WINDOWS

class ThreadLocalStorage
{
public:
	/**
	 * \brief returns the singleton instance
	 * \return
	 */
	static ThreadLocalStorage* The();

	/**
	 * \brief  call this method from inside the thread, to get access
	 * to the thread local data
	 * \return
	 */
	ThreadDataType* GetData();

private:
	// Default Constructor
	ThreadLocalStorage();

	// non-copyable
	ThreadLocalStorage(const ThreadLocalStorage& rhs) =delete;

	// Destructor. Typically this destructor should not be called from within
	// the code.
	~ThreadLocalStorage() noexcept;

	/**
	 * \brief // critical section will be used for synchronization methods altering tls_map.
	 * This is done by ScopeSyncGuard
	 */
	CRITICAL_SECTION critical_section_{};

	/**
	 * \brief type of key for the std::map (corresponds to Thread id type)
	 */
	typedef DWORD KeyType;

	/**
	 * \brief std::map for assigning ThreadData pointers to thread ids
	 */
	typedef std::map<KeyType, ThreadDataType*> MapType;

	/**
	 * \brief The map
	 */
	MapType tls_map_;

	/**
	 * \brief dynamically allocated tls index for the variable used with the Win32 TlsXxx() API.
	 */
	const DWORD tls_index_;

#ifdef _LINUX
	bool is_index_valid_;
#endif

	// registers given thread
	void RegisterCurrentThread() noexcept;

	// deletes the data associated with current thread
	void UnregisterCurrentThread() noexcept;

	// called whenever a thread terminates.
#ifdef _WINDOWS
	static void ThreadTerminating() noexcept;
#elif defined(_LINUX)
	static void ThreadTerminating(void* key) noexcept;
#endif

	/**
	 * \brief initialize the singleton instance. This method is not synchronized so it
	 * should be called from main thread
	 */
	static void Init();

	/**
	 * \brief clean up the singleton instance. This method is not synchronized so it
	 * should be called from main thread
	 */
	static void Cleanup() noexcept;

	/**
	 * \brief used by callData() inline function to take the slower path using the map lookup in
	 * cases where there was no Tls index available.
	 * \return
	 */
	ThreadDataType* GetDataFallback();

	// singleton pointer
	static ThreadLocalStorage* singleton_;

	/**
	 * \brief Internal lock for access to the global object.
	 */
	class LockInternal
	{
	public:
		// Constructor: enters lock.
		LockInternal()
		{
			critical_section_.enter();
		}

		// Destructor: Releases lock.
		~LockInternal()
		{
			critical_section_.leave();
		}

		static bool IsThreadLocked()
		{
			return critical_section_.isThreadLocked();
		}

	private:
		// Static critical section to lock the singleton.
		static CriticalSection critical_section_;
	};
};

inline ThreadLocalStorage* ThreadLocalStorage::The()
{
	if(!singleton_)
		Init();

	return singleton_;
}

inline ThreadDataType* ThreadLocalStorage::GetData()
{
	ThreadDataType* result = nullptr;
#ifdef _WINDOWS
	if(tls_index_ != TLS_OUT_OF_INDEXES)
	{
		// read value from Tls slot
		result = static_cast<ThreadDataType*>(TlsGetValue(tls_index_));
	}
#elif defined(_LINUX)
	if(isIndexValid) {
		// read value from Tls slot
		result = static_cast<ThreadDataType*>(pthread_getspecific(tls_index));
	}
#endif

	if(!result)
	{
		// on allocation there was no Tls slot free,
		// or thread not registered yet
		result = GetDataFallback();
	}

	assert(result); // user must have called register beforehand
	return result;
}
