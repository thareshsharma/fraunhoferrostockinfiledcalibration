#pragma once

#include "critical_section.h"

/**
 * \brief ThreadBase is a base class for threads.
 * Derive from this class to implement a custom thread.
 * Implement the abstract runThreadProc function to define the thread behavior.
 * Optionally override initThreadProc and exitThreadProc to define
 * custom initialization and finalization within the thread.
 * CAUTION:
 *     -> Call initThread() immediately after construction!
 *     -> Call terminateThread() immediately before destruction!
 * This class can optionally continuously repeat runThreadProc() until someone calls
 * setToSleep() or terminateThread().
 * If runThreadProc() returns true, it will be run again. If it returns false, the thread
 * will terminate.
 * To wait for run one of runThreadProc() to finish, call waitForFinish().
 * To wait for the thread to really terminate, call terminateThread().
 */

class ThreadBase
{
public :
	typedef CriticalSection LockType;

	explicit ThreadBase();

	/** \brief initThread: Initializes thread*/
	virtual void InitThread();

	/**
	 * \brief Waits until the thread completes. Also triggers a signal to
	 * tell the thread quit the loop calling runThreadProc iteratively.
	 */
	void TerminateThread();

	/// setToSleep: Set running thread to sleep, wait for sleep state
	/// depending on wait.
	bool SetToSleep(bool wait = true);

	/// waitForFinish: Waits until the thread finishes the call runThreadProc or the thread terminates.
	///			If runThreadProc returns true, the thread is not terminated but waits
	///			for calling activate() again to do another run of runThreadProc.
	///			If runThreadProc returns false, the thread finishes. One still has to
	///			call terminateThread()!
	void WaitForFinish();

	/// activate: Activates the thread: Returns false, if the thread
	/// doesn't need to be activated
	bool Activate(bool wait = false);

	/// getPriority: Returns priority of running thread
	int GetPriority() const;

	/// setPriority: Sets priority of running thread
	bool SetPriority(int in);

	/// allocData: Synchronize data access: Called to checkout data
	void AllocData();

	/// releaseData: Synchronize data access: Called to checkin data
	void ReleaseData();

	/// getThreadID: retrieve the threadID of this thread (only valid after initThread has been called!!)
	DWORD GetThreadId();

#ifdef _DEBUG
	/// Debug variable, defines, if thread should be blocked. For finding synchronization failures.
	bool thread_blocked;
#endif

protected:
	virtual ~ThreadBase();

	/// Scope class for internal lock.
	class LockIntern : public LockType::Access
	{
	public:
		LockIntern(const ThreadBase& thread) :
			Access(thread.critical_section_, LockType::write)
		{
		}
	};

	/// Implement this function to define the thread behaviour. return true to
	/// continue with another loop, false to stop execution.
	/// Default implementation: Returns false and set thread to sleep.
	virtual bool RunThreadProc();

	/// Override this function to implement special initialization behaviour.
	virtual void InitThreadProc()
	{
	};

	/// Override this function to implement special termination behaviour.
	virtual void ExitThreadProc()
	{
	};

	/// To be called before going to sleep, to be overwritten by derived class,
	/// if different behavior is wanted.
	virtual bool IsReadyForSleep() const;

	/// reset: Resets thread
	void Reset(bool cleanup = true);

	/// Returns handle of this thread.
	HANDLE GetThreadHandle() const;

	/// threadTerminate: --> true, used to terminate thread from outside
	volatile bool thread_terminate_;

	/// threadRunning: --> Reflects state of thread
	volatile bool thread_running_;

	/// threadSleep: --> Thread is running but waits to be waked up
	volatile bool thread_sleep_;

	/// setThreadToSleep: --> Stop current activity
	volatile bool set_thread_to_sleep_;

	/// threadActivated: --> Set to 1 if thread had been activated
	volatile bool thread_activated_;

	/// callsDestructorInThread: --> Set to true if no one will wait for the thread to finish
	/// Dangerous! Should be used only by FunctorThread as it calls its destructor
	/// from within its thread if initialized with FunctorThread(true).
	volatile bool calls_destructor_in_thread_ = false;

	// Lockable element to synchronize concurrent access to a thread instance
	mutable CriticalSection lock_internal_access_;

	/// set your own thread name in constructor (works good in linux)
	std::string thread_name_;

private:
	/// criticalSection: Used to synchronize full access to ScanOp
	mutable LockType critical_section_;

	/// handle of thread
	HANDLE thread_handle_;

	/// threadID: id of running thread
	DWORD thread_id_;

	/// initializeThread: Event used to initialize the thread
	HANDLE initialize_thread_;

	/// activateThread: Event used to activate the thread
	HANDLE activate_thread_;

	/// terminatedThreadSignal: Event used to signal
	/// that the the thread has finished
	HANDLE terminated_thread_signal_;

	/// terminatedThreadSignal: Event used to signal
	/// that the the thread is running now in its own thread.
	HANDLE started_thread_signal_;

	/// staticThreadProc is called as a starting point for a new thread.
	static unsigned int WINAPI StaticThreadProc(void* user_data);

	/// threadProcBase is called by staticThreadProc starts the basic thread functionality
	unsigned int ThreadProcBase();

	/// Wait for the thread to terminate and clean everything up
	void Join();
};
