#pragma once

//	Lock with Windows Critical Section
//
//
//	This locking class will lock section for exclusive access. (no shared read).
//	It behaves like windows critical section, especially it will grant
//	the r/w access for the same thread if this thread already has access.
//
//	Supports Shared Read: NO
//	Supports multiple locks from the same process/thread: YES
class CriticalSection
{
	typedef unsigned int count_t;
private:
	CRITICAL_SECTION cs;
	/// Counter of number of lockings.
	count_t lockCounter;

	/// No Copy Constructor
	CriticalSection(const CriticalSection&);

	/// No assignment operator.
	CriticalSection& operator=(const CriticalSection&);

public:

	void enter()
	{
		EnterCriticalSection(&cs);
		++lockCounter;
	}

	void leave()
	{
		--lockCounter;
		LeaveCriticalSection(&cs);
	}


	typedef enum { read, write } AccessMode;

	CriticalSection(const DWORD SpinCount = 4000);

	~CriticalSection()
	{
		DeleteCriticalSection(&cs);
	}

	/// typical for critical sections : try enter a cs and returns true if successful
	BOOL tryEnter()
	{
		BOOL result = TryEnterCriticalSection(&cs);
		if(result != FALSE)
			++lockCounter;
		return result;
	}

	/// Gets the lock counter.
	count_t getLockCounter() const
	{
		return lockCounter;
	}

	bool isThreadLocked() const
	{
		TryAccess lock(*const_cast<CriticalSection*>(this));
		bool result = (lock.hasAccess() && (getLockCounter() != 1u));
		return result;
	}

	class Access
	{
		CriticalSection& cs;
		const AccessMode mode;
		bool hasLockFlg; // flag, that indicates if this object has the lock.

		Access(const Access&);            // non-copy-constructable
		Access& operator=(const Access&); // non-assignable
	public:
		Access(CriticalSection& csIn,
		       const AccessMode& modeIn);
		~Access();
		void aquire();
		void release();
		static void aquire(CriticalSection& csIn,
		                   const AccessMode& modeIn);
		static void release(CriticalSection& csIn,
		                    const AccessMode& modeIn);
	};

	class TryAccess
	{
	protected:
		// critial section to try to get.
		CriticalSection& cs;
		// Access mode for critical section.
		const AccessMode mode;
		// If accessing lock worked.
		bool gotLock;

	private:
		TryAccess(const TryAccess&);            // non-copy-constructable
		TryAccess& operator=(const TryAccess&); // non-assignable

	public:
		/// Constructor.
		TryAccess(CriticalSection& csIn,
		          const AccessMode& modeIn = write);
		/// Destructor.
		~TryAccess();

		/// Returns, if getting access has worked.
		bool hasAccess() const;
		bool tryAquire();
		void release();

	protected:
		static bool tryAquire(CriticalSection& csIn,
		                      const AccessMode& modeIn);
		static void release(CriticalSection& csIn,
		                    const AccessMode& modeIn);
	};

	friend class Access;
	friend class TryAccess;
};
