#pragma once

#include "typedefs.h"

class ModelInFormat;

/**
	\brief Thread local data struct. Put one member for each method AND type.
	There must be one declaration in the struct for
	each method, otherwise the data will be shared
*/
class ThreadDataType
{
public:

#ifdef MULTI_MAIN_LOCK_CHECK
	// type for map for counting AllocMains.
	typedef std::map<ModelContext*, int> context_lock_counter_type;
#endif // MULTI_MAIN_LOCK_CHECK

	// default constructor
	ThreadDataType() :
		model_out_format_flag_(0),
		attribute_temp_flags_(0),
		located_to_memory_block_(false),
		alloc_scene_count_(0),
		alloc_scene_all_count_(0),
		alloc_main_all_count_(0)
	{
	}

	cv::Vec3f& GetVec3dVec3df()
	{
		return get_vec3d_vec3df_;
	}

	std::stack<ModelInFormat*>& GetModelInFormatInList()
	{
		return model_in_format_in_list_;
	}

	int& GetModelOutFormatFlag()
	{
		return model_out_format_flag_;
	}

	std::string& GetModelInFormatPropagateKeyword()
	{
		return model_in_format_propagate_keyword_;
	}

	std::string& GetAttributeTempString()
	{
		return attribute_temp_string_;
	}

	//std::shared_ptr<BaseNode> GetCurrentListExceptionManager() const
	//{
	//	return current_list_exception_manager_;
	//}

	//std::shared_ptr<BaseNode> GetCurrentPathRootObserver() const
	//{
	//	return current_path_root_observer_;
	//}

	//std::shared_ptr<BaseNode> GetNotifyInfosObserver() const
	//{
	//	return notify_infos_observer_;
	//}

	//std::shared_ptr<BaseNode> GetSourceToCopyTableObserver() const
	//{
	//	return source_to_copy_table_observer_;
	//}

	//std::shared_ptr<BaseNode> GetInfosAttrContainer() const
	//{
	//	return info_map_attr_container_;
	//}

	int& GetAttributeTempFlags()
	{
		return attribute_temp_flags_;
	}

	TypeToken& GetAllocSceneCount()
	{
		return alloc_scene_count_;
	}

	TypeToken& GetAllocSceneAllCount()
	{
		return alloc_scene_all_count_;
	}

	TypeToken& GetAllocMainAllCount()
	{
		return alloc_main_all_count_;
	}

	bool& GetLocatedToMemoryBlock()
	{
		return located_to_memory_block_;
	}

	void SetLocatedToMemoryBlock(const bool located)
	{
		located_to_memory_block_ = located;
	}

#ifdef MULTI_MAIN_LOCK_CHECK
	context_lock_counter_type& getContextLockCounter() { return m_ContextLockCounter; }
#endif // MULTI_MAIN_LOCK_CHECK
protected:

	// for transformations of Vec3df.
	cv::Vec3f get_vec3d_vec3df_;

	// data used by ModelInFormat
	// for ModelInFormat::inList
	std::stack<ModelInFormat*> model_in_format_in_list_;

	// for ModelInFormat::propagateKeyword
	std::string model_in_format_propagate_keyword_;

	// Model out format for iQModelFormat
	int model_out_format_flag_;

	// Data used by ExceptionManager
	//std::shared_ptr<BaseNode> current_list_exception_manager_;

	//// static data used by Observer implementation.
	//std::shared_ptr<BaseNode> current_path_root_observer_;
	//std::shared_ptr<BaseNode> notify_infos_observer_;
	//std::shared_ptr<BaseNode> source_to_copy_table_observer_;

	//// static infos used by AttrContainer
	//std::shared_ptr<BaseNode> info_map_attr_container_;

	// for unique name generation within AttrContainer
	std::string attribute_temp_string_;

	// Special temporary flags for all attributes.
	int attribute_temp_flags_;
	bool located_to_memory_block_;

	/// @name Lock Counter

	TypeToken alloc_scene_count_;     // protect 3d view tree counter
	TypeToken alloc_scene_all_count_; // number of instances of AllocSceneAll in this thread
	TypeToken alloc_main_all_count_;  // number of instances of AllocMainAll in this thread

#ifdef MULTI_MAIN_LOCK_CHECK
	context_lock_counter_type m_ContextLockCounter;	// Counter for number of AllocMains for each model context in this thread.
#endif // MULTI_MAIN_LOCK_CHECK
};

/**
   Guard for entering and leaving critical sections.
   Enters critical section at creation and leaves in destructor.

*/
class ScopeSyncGuard
{
public:

	/// enters given critical section
	ScopeSyncGuard(CRITICAL_SECTION* a_critical_section)
	{
		EnterCriticalSection(a_critical_section);
		cs_ = a_critical_section;
	}

	/// leaves critical section that was given in constructor
	~ScopeSyncGuard()
	{
		LeaveCriticalSection(cs_);
	}

private:

	/// local pointer to critical section
	CRITICAL_SECTION* cs_;

	/// no default constructor
	ScopeSyncGuard()
	{
	}
};
