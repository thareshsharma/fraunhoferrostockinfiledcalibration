#pragma once

#include "camera_sensor.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief Class representing Transformation
 */
class Transformation
{
public:

	Transformation();

	Transformation(const cv::Mat& rotation_matrix,
	               const cv::Mat& translation);

	Transformation(const Transformation& rhs);

	Transformation& operator=(const Transformation& rhs);

	/*Transformation(Transformation&& rhs) noexcept;

	Transformation& operator=(Transformation&& rhs) noexcept;*/

	explicit Transformation(const cv::Affine3d& transformation);

	//explicit Transformation(std::shared_ptr<const CameraSensor> sensor);

	explicit Transformation(std::shared_ptr<const CameraSensor> sensor);

	explicit Transformation(const std::vector<double>& vector_of_doubles);

	explicit Transformation(const cv::Mat& matrix);

	explicit Transformation(const cv::Mat4d& matrix_4d);

	/**
	 * \brief Rotates p along q
	 * \param p vector 1
	 * \param q vector 1
	 */
	void Rotate(const cv::Vec3d& p,
	            const cv::Vec3d& q);

	/**
	 * \brief Sets the translation part of the 4x4 matrix
	 * \param translation translation vector
	 */
	void Translate(const cv::Vec3d& translation);

	/**
	 * \brief Transforms the points
	 * \tparam T type
	 * \param point point which will be transformed
	 * \return Transformed point3d
	 */
	template <class T>
	cv::Point3_<T> Multiply(const cv::Point3_<T>& point) const
	{
		return transformation_.cast<T>() * point;
	}

	/**
	 * \brief
	 * \tparam T
	 * \param points
	 * \return
	 */
	template <class T>
	std::vector<T> Multiply(const std::vector<T>& points) const
	{
		std::vector<T> out(points.size());

		std::transform(points.begin(),
		               points.end(),
		               out.begin(),
		               [this](const T& point) -> T
		               {
			               return Multiply(point);
		               });
		return out;
	}

	/**
	 * \brief
	 * \tparam T
	 * \param point
	 * \return
	 */
	template <class T>
	cv::Point3_<T> MultiplyInverse(const cv::Point3_<T>& point) const
	{
		return transformation_.inv().cast<T>() * point;
	}

	/**
	 * \brief
	 * \tparam T
	 * \param point
	 * \return
	 */
	template <class T>
	cv::Point3_<T> MultiplyWithRotation(const cv::Point3_<T>& point) const
	{
		return transformation_.rotation() * point;
	}

	///**
	// * \brief
	// * \return
	// */
	//cv::Mat4d GetMat4d() const;

	/**
	 * \brief
	 * \return
	 */
	std::vector<double> Serialized() const;

	/**
	 * \brief
	 * \param transformation
	 * \return
	 */
	Transformation Multiply(const Transformation& transformation) const;

	/**
	 * \brief Returns Transformation so that return * this = in
	 * \param in transformation
	 * \return relative transformation
	 */
	Transformation GetRelTransformation(const Transformation& in) const;

	/**
	 * \brief Gets inverse
	 * \return
	 */
	Transformation GetInverse() const;

	/**
	 * \brief
	 * \return
	 */
	cv::Affine3d GetInverseAffine() const;

	/**
	 * \brief
	 * \param sensor
	 */
	void SetToCalibration(std::shared_ptr<CameraSensor> sensor) const;

	/**
	 * \brief
	 * \return
	 */
	cv::Mat GetRotationalTransformation() const;

	/**
	 * \brief
	 * \return
	 */
	cv::Mat GetTranslationalTransformation() const;

	/**
	 * \brief
	 * \return
	 */
	const cv::Affine3d& GetMat4() const;

	/**
	 * \brief
	 * \return
	 */
	bool QueryMirrorMat() const;

private:
	cv::Affine3d transformation_;
	cv::Mat matrix_;
};
}
}
