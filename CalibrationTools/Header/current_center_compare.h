#pragma once

#include "circular_marker.h"

namespace DMVS
{
namespace CameraCalibration
{
struct CircularMarkerComparator
{
	bool operator ()(const CircularMarkerVector& lhs,
	                 const CircularMarkerVector& rhs) const
	{
		return lhs < rhs;
	}
};
}
}
