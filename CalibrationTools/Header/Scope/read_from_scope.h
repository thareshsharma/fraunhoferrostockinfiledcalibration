#pragma once

#include "read_context_base.h"
#include "model_in_format.h"

class ReadFromScope
{
public:
	ReadFromScope(std::istream* input_stream,
	              ReadContextBase* read_context);

	virtual ~ReadFromScope();

	/**
	 * \brief Disallows copy-construction
	 * \return
	 */
	ReadFromScope(const ReadFromScope&) = delete;

	/**
	 * \brief Disallows assignment
	 * \return
	 */
	ReadFromScope& operator=(const ReadFromScope&) = delete;

	/**
	 * \brief Returns the ReadContext(Base/SharedData)
	 * \return
	 */
	ReadContextBase* GetReadContext() const;

	/**
	 * \brief Returns the ModelInFormat
	 * \return
	 */
	ModelInFormat& GetModelInFormat();

protected:
	ModelInFormat model_in_format_;
	ReadContextBase* read_context_;
};
