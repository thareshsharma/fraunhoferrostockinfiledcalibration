#pragma once

#include "blob_detector.h"
#include "BoardDetector/board_detector.h"
#include "helper.h"
#include "dmvs.h"
#include "enum_calibration_exit_code.h"
#include "calibration_file_base.h"
#include "calibration_cache.h"
#include "calibration_calculator.h"
#include "configuration_container.h"
#include "ikinematics.h"
#include "kinematics_helpers.h"
#include "enum_image_channel_type.h"
#include "KdTree/kdtree.h"
#include "application_settings.h"

bool SetUpLinearAxis(IKinematics*& linear_axis);

void SetUpCalibrationCache(const std::shared_ptr<ConfigurationContainer>& configuration_container,
                           const std::string& serial_number,
                           const std::string& file_name,
                           std::shared_ptr<CalibrationCache>& calibration_cache);

void PushImagesToCalibrationCache(std::shared_ptr<CalibrationCache>& calibration_cache,
                                  ChannelImageMap& channel_image_map,
                                  int index);

void RemoveSaturatedMarkers(const std::vector<ImageChannelCircularMarkerVectorMap>& circular_markers_maps,
                            std::map<ImageChannel, std::vector<CircularMarkerPair>>& unsaturated_markers_map);

void DrawMarkers(ChannelImageVectorMap& image_vector_channel_map,
                 std::map<ImageChannel, std::vector<CircularMarkerPair>>& markers_combined_map);

void DrawObservations(ChannelImageVectorMap& image_vector_channel_map,
                      std::vector<Observation> observations);

void CombineMarkers(std::map<ImageChannel, std::vector<CircularMarkerPair>>& markers_combined_map,
                    std::map<ImageChannel, CircularMarkerVector>& combined_markers);

void ChangeLedPower(DMVSSensor& dmvs,
                    int frame_count);

double FixExposureTime(double exposure_time);

void ChangeExposureTime(DMVSSensor& dmvs,
                        size_t frame_index);

void SetPositionBasedExposureTime(DMVSSensor& dmvs,
                                  int frame_count);

void GrabImageSeries(DMVSSensor& dmvs,
                     const ChannelImageMap& channel_image_map,
                     ChannelImageVectorMap& image_series);

void GrabImageSeries2(DMVSSensor& dmvs,
                      const ChannelImageMap& channel_image_map,
                      ChannelImageVectorMap& image_series);

void WriteAverageImage(const ApplicationSettings& settings,
                       int position_index,
                       size_t frame_index_at_position,
                       const cv::Mat& average_image);

void AverageImageSeries(ChannelImageMap& channel_image_map,
                        ChannelImageVectorMap& image_vector_channel_map,
                        int position_index,
                        size_t frame_index_at_position,
                        ChannelImageVectorMap& image_series,
                        const ApplicationSettings& settings);

void FrameProcessing(DMVSSensor& dmvs,
                     ChannelImageMap& channel_image_map,
                     BlobDetector& blob_detector,
                     std::vector<ImageChannelCircularMarkerVectorMap>& found_circular_markers_map,
                     ChannelImageVectorMap& image_vector_channel_map,
                     int current_frame_index,
                     int recorded_position_count,
                     const ApplicationSettings& settings);

void FrameProcessing2(DMVSSensor& dmvs,
                      ChannelImageMap& channel_image_map,
                      BlobDetector& blob_detector,
                      std::vector<ImageChannelCircularMarkerVectorMap>& found_circular_markers_map,
                      ChannelImageVectorMap& channel_image_vector_map,
                      int recorded_position_count,
                      const ApplicationSettings& settings);

void CopyChannelImageMap(const ChannelImageMap& source,
                         ChannelImageMap& destination);

CalibrationExitCode RetrieveChannelImages(DMVSSensor& dmvs,
                                          ChannelImageMap& channel_image_map,
                                          int frame_index);

void WriteCollage(int position_idx,
                  const cv::Mat& collage);

void CreateAndShowCollage(ChannelImageMap image_channel,
                          int position_idx);

void CalculateStatistics(const std::shared_ptr<CalibrationFileBase>& file_base,
                         Frame& all_frame_images,
                         CalibrationCalculator& calibration_calculator,
                         ApplicationSettings* settings = nullptr);

int FactoryCameraCalibration(int ac,
                             char* av[]);

int ProjectorCalibration();

int FactoryCalibration();

void LoadRecordedImages(const std::string& images_folder,
                        std::map<size_t, std::vector<cv::Mat>>& images_map);

void LoadRecordedImages(const std::string& images_folder,
                        std::vector<cv::Mat>& led_images,
                        std::vector<cv::Mat>& laser_images);

void JoinImages(const std::string& images_folder,
                std::vector<cv::Mat>& recorded_joined_images);

void FilterRecordedImages(std::vector<std::shared_ptr<BoardDetector>>& boards,
                          const BlobDetector& blob_detector,
                          std::vector<cv::Mat>& led_images,
                          std::vector<cv::Mat>& laser_images);

void FilterRecordedImages(const std::vector<cv::Mat>& images,
                          std::vector<cv::Mat>& led_on_images,
                          std::vector<cv::Mat>& laser_on_images);

bool IfImageIsSharp(cv::Mat image);

double ModifiedLaplacian(const cv::Mat& src);

double VarianceOfLaplacian(const cv::Mat& src);

double Tenengrad(const cv::Mat& src,
                 int kernel_size);

double NormalizedGrayLevelVariance(const cv::Mat& src);
