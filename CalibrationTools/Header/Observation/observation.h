#pragma once

#include "circular_marker.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief Structure to store observation
 */
class Observation
{
public:

	Observation();

	Observation(std::shared_ptr<CircularMarker> marker);

	~Observation() noexcept;

	Observation& operator=(const Observation& rhs);

	Observation(const Observation& rhs);

	/**
	 * \brief
	 */
	void GetAsCircularMarker(CircularMarker& marker) const;

	/**
	 * \brief
	 */
	std::shared_ptr<Observation> CreateFromCircularMarker(const CircularMarker& marker) const;

	/**
	 * \brief
	 */
	void SetFromCircularMarker(const CircularMarker& circular_marker);

	//Attribute accessors: marker_id
	size_t GetMarkerId() const;
	void SetMarkerId(size_t marker_id);

	//Attribute accessors: board_id
	size_t GetBoardId() const;
	void SetBoardId(size_t board_id);

	//Attribute accessors: camera_id
	size_t GetImageChannel() const;
	void SetImageChannel(size_t channel);

	//Attribute accessors: frame_id
	size_t GetFrameId() const;
	void SetFrameId(size_t frame_id);

	// Attribute accessors: RawEllipseCenter
	cv::Point2f GetRawEllipseCenter() const;
	void SetRawEllipseCenter(const cv::Point2f& ellipse_center);

	// Attribute accessors: RawEllipseWidth
	double GetRawEllipseWidth() const;
	void SetRawEllipseWidth(const double& ellipse_width);

	// Attribute accessors: RawEllipseHeight
	double GetRawEllipseHeight() const;
	void SetRawEllipseHeight(const double& ellipse_height);

	// Attribute accessors: RawEllipseAngle
	double GetRawEllipseAngle() const;
	void SetRawEllipseAngle(const double& ellipse_angle);

	// Attribute accessors: RawEllipseRadius
	double GetRawEllipseRadius() const;
	void SetRawEllipseRadius(const double& ellipse_radius);

	// Attribute accessors: RawEllipseDistortionCorrection
	cv::Point2f GetRawEllipseDistortionCorrection() const;
	void SetRawEllipseDistortionCorrection(const cv::Point2f& distortion_correction);

	// Attribute accessors: CamCoords3d
	cv::Vec3d GetCameraCoordinates3d() const;
	void SetCameraCoordinates3d(const cv::Vec3d& camera_coordinates_3d);

	//Attribute accessors: scanner_position_id
	size_t GetScannerPositionId() const;
	void SetScannerPositionId(size_t scanner_id);

	//Attribute accessors: deactivated
	bool GetDeactivated() const;
	void SetDeactivated(bool is_deactivated);

	//Attribute accessors: deactivated
	bool GetIsProcessed() const;
	void SetIsProcessed(bool is_processed);

	//Attribute accessors: raw_distance
	double GetRawDistance() const;
	void SetRawDistance(double raw_distance);

	/// Attribute accessors: EncoderAngles
	cv::Point2f GetEncoderAngles() const;
	void SetEncoderAngles(const cv::Point2f& encoder_angles);

	//Attribute accessors: scan_half
	bool GetScanHalf() const;
	void SetScanHalf(bool is_scan_half);

	//Attribute accessors: weight
	double GetWeight() const;
	void SetWeight(double weight);

protected:
	/**
	 * \brief Initializes the object
	 */
	void Init();

private:
	size_t marker_id_;
	size_t image_channel_;
	size_t board_id_;
	size_t frame_id_;
	size_t scanner_position_id_;

	cv::Vec3d camera_coordinates3d_;

	cv::Point2f raw_ellipse_center_;
	cv::Point2f raw_ellipse_distortion_correction_;
	cv::Point2f encoder_angles_;

	double raw_ellipse_width_;
	double raw_ellipse_height_;
	double raw_ellipse_angle_;
	double raw_ellipse_radius_;
	double raw_distance_;
	double weight_;

	bool scan_half_;
	bool is_deactivated_;
	bool is_processed_ = false;
};
}
}
