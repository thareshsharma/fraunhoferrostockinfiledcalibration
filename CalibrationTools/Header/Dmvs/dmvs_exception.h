#pragma once

namespace DMVS
{
class DMVSException : std::exception
{
public:
	~DMVSException() noexcept override;
	DMVSException(const char* msg,
	              const char* file,
	              size_t line,
	              const char* func,
	              const char* info = "");

	char const* what() const override;
	const char* GetFile() const;
	size_t GetLineNumber() const;
	const char* GetFunction() const;
	const char* GetInfo() const;

private:
	const char* file_     = nullptr;
	size_t line_number_   = 0;
	const char* function_ = nullptr;
	const char* info_     = nullptr;
};
}
