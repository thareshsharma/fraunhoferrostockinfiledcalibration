#pragma once

#include "capture_calibration.h"
#include "dmvs_sdk.h"
#include "ikinematics.h"
#include "device_description.h"
#include "enum_camera_model.h"

class CalibrationCache;
class ConfigurationContainer;

namespace fs = boost::filesystem;

namespace DMVS
{
namespace CameraCalibration
{
class DMVSCameraCalibration : std::enable_shared_from_this<DMVSCameraCalibration>
{
public:

	/**
	 * Modes for manageCalibration
	 * Note: Override operator | for easy combining of flags
	*/
	enum ManageCalibrationMode
	{
		STORE_CALIBRATION = 0x1,
		RESTORE_CALIBRATION = 0x2,
		DELETE_CALIBRATION = 0x4,
		FIND_CALIBRATION = 0x8,
		FACTORY = 0x10,
		USER = 0x20,
		FORMAT_DEVICE = 0x100000,
	};

	DMVSCameraCalibration();

	virtual ~DMVSCameraCalibration();

	DeviceDescription DeviceState();

	virtual DeviceType DeviceType() const;

	bool Open();

	void GetCalibration(CaptureCalibration& calibration) const;

	void SetCalibration(CaptureCalibration& calibration);

	bool GetDefaultCalibration(CaptureCalibration& calibration);

	bool GetDeviceCalibration(CaptureCalibration& calibration);

	bool ManageCalibration(ManageCalibrationMode calibration_mode);

	bool PushChannelImageToCache(bool synced = true);

	bool RetrieveImageFromCalibrationCache(size_t frame,
	                                       size_t channel,
	                                       cv::Mat& image) const;

	static std::string GetExtension();

	bool IsOpened() const;

	bool Init(const std::string& input_filename,
	          const std::string& output_filename,
	          std::shared_ptr<ConfigurationContainer> options);

	/**
	 * \brief
	 * \return
	 */
	std::string GetCaptureDeviceIdent() const;

	static std::shared_ptr<const CameraSensor> GetDMVSCameraCameraCalibration(std::string name,
	                                                                          ImageChannel channel,
	                                                                          CameraModel camera_model,
	                                                                          cv::Vec2d depth_params,
	                                                                          cv::Vec3d depth_params_exp,
	                                                                          const cv::Mat& camera_matrix      = cv::Mat::eye(3, 3, CV_64F),
	                                                                          const cv::Mat& distortion_vector  = cv::Mat::zeros(5, 1, CV_64F),
	                                                                          const cv::Mat& rotation_matrix    = cv::Mat::eye(3, 3, CV_64F),
	                                                                          const cv::Mat& translation_vector = cv::Mat::zeros(3, 1, CV_64F));

	/**
	 * \brief
	 */
	CaptureCalibration capture_calibration;

	/**
	 * \brief
	 */
	std::shared_ptr<CalibrationCache> cache;

	/**
	 * \brief
	 */
	std::shared_ptr<ConfigurationContainer> configuration_options;

	/**
	 * \brief Serial number of the connected
	 */
	std::string serial_number;

	/**
	 * \brief
	 */
	bool m_use_frame = false;

private:

	bool InitCapture();

	std::string GetDeviceType() const;

	std::string GetSerialNumber() const;

	double timestamp_;

	std::string calibration_filename_;
	fs::path output_file_;
	fs::path input_file_;

	/**
	 * \brief contains the device type string e.g. 'DMVS', 'Freestyle'
	 */
	std::string device_type_;
};
}
}
