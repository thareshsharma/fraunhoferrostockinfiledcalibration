#pragma once

#include "dmvs_sdk.h"
#include "critical_section.h"
#include "ikinematics.h"
#include "capture_calibration.h"
#include "device_description.h"
#include "enum_camera_model.h"
#include "enum_device_current_mode.h"
#include "enum_calibration_exit_code.h"
#include "interlocked_counter.h"
#include "reconstructor_laser_dots_3d.h"
#include "projector_board_detector.h"

class ApplicationSettings;
class CalibrationCache;
class ConfigurationContainer;
class CaptureGrabber;

using namespace dmvs_sdk;
using namespace Reconstruct;
using namespace LABS::MotionControl;

namespace fs = boost::filesystem;

namespace DMVS
{
namespace CameraCalibration
{
class DMVSSensor : std::enable_shared_from_this<DMVSSensor>
{
public:

	/**
	 * Modes for manageCalibration
	 * Note: Override operator | for easy combining of flags
	*/
	enum ManageCalibrationMode
	{
		STORE_CALIBRATION = 0x1,
		RESTORE_CALIBRATION = 0x2,
		DELETE_CALIBRATION = 0x4,
		FIND_CALIBRATION = 0x8,
		FACTORY = 0x10,
		USER = 0x20,
		FORMAT_DEVICE = 0x100000,
	};

	/**
	 * Represents table positions
	 */
	enum TablePosition
	{
		START = 0x1,
		CENTER = 0x2,
		END = 0x4,
	};

	DMVSSensor();

	virtual ~DMVSSensor();

	static void ImageReceived2D(unsigned char* image,
	                            long width,
	                            long height,
	                            data::AdditionalImageInformation2D additional_image_information,
	                            void* context);

	static void ImageReceived3D(data::Point3D* image,
	                            long points_count,
	                            data::AdditionalImageInformation3D additional_image_information,
	                            void* context);

	CalibrationExitCode SetUpSensor(const std::shared_ptr<ConfigurationContainer>& options);

	DeviceDescription DeviceState();

	virtual DeviceType DeviceType() const;

	bool Open();

	//bool HasChannel(size_t channel) const;

	void GetCalibration(CaptureCalibration& calibration) const;

	void SetCalibration(CaptureCalibration& calibration);

	virtual bool GetDefaultCalibration(CaptureCalibration& calibration);

	bool GetDeviceCalibration(CaptureCalibration& calibration);

	bool ManageCalibration(ManageCalibrationMode calibration_mode);

	bool PushChannelImageToCache(bool synced = true);

	bool RetrieveImageFromCalibrationCache(size_t frame,
	                                       size_t channel,
	                                       cv::Mat& image) const;

	static std::string GetExtension();

	int TestLoop(bool reload_calibration = true);

	bool Connect();

	bool Release();

	bool Start() const;

	bool Run() const;

	bool Stop() const;

	bool GrabFrame();

	bool StartCapture(CapabilityType type);

	bool StopCapture();

	bool IsRunning() const;

	bool IsConnected() const;

	bool IsLive() const;

	bool SetLedPower(unsigned int power) const;

	bool SetExposureTime(double exposure) const;

	std::string GetCalibrationFilename() const;

	/**
	 * \brief Check if the device is open
	 * \note Can't be final because the tests in sensor_implementation.test.cpp
	 * use mock objects overriding this function.
	 * \return bool
	 */
	bool IsOpened() const;

	bool SetCameraCalibrationSettings(bool is_auto_exposure) const;

	bool SetScanSettings() const;

	DMVSDevice* GetConnectedDMVS() const;

	bool GrabFromSensor() const;

	bool Init(const std::string& input_filename,
	          const std::string& output_filename,
	          std::shared_ptr<ConfigurationContainer> options);

	/**
	 * \brief
	 * \param linear_stage
	 * \param plane_position_start
	 * \param plane_position_end
	 * \param plane_normal_start
	 * \param plane_normal_end
	 * \param board_detector
	 * \param image1_markers
	 * \param image2_markers
	 * \return
	 */
	bool FindTablePosition(IKinematics* linear_stage,
	                       cv::Vec3d& plane_position_start,
	                       cv::Vec3d& plane_position_end,
	                       cv::Vec3d& plane_normal_start,
	                       cv::Vec3d& plane_normal_end,
	                       ProjectorBoardDetector& board_detector,
	                       std::map<size_t, CircularMarker>& image1_markers,
	                       std::map<size_t, CircularMarker>& image2_markers);

	/**
	 * \brief
	 * \param linear_stage
	 * \param plane_position_start
	 * \param plane_position_end
	 * \param plane_normal_start
	 * \param plane_normal_end
	 * \param board_detector
	 * \param image1_markers
	 * \param image2_markers
	 * \return
	 */
	bool FindTablePositionOffline(IKinematics* linear_stage,
	                              cv::Vec3d& plane_position_start,
	                              cv::Vec3d& plane_position_end,
	                              cv::Vec3d& plane_normal_start,
	                              cv::Vec3d& plane_normal_end,
	                              ProjectorBoardDetector& board_detector,
	                              std::map<size_t, CircularMarker>& image1_markers,
	                              std::map<size_t, CircularMarker>& image2_markers);

	/**
	 * \brief
	 * \return
	 */
	std::string GetCaptureDeviceIdent() const;

	/**
	 * \brief
	 * \return
	 */
	bool InitReconstructor();

	/**
	 * \brief
	 * \param rotation
	 * \param translation
	 * \param calibration
	 */
	void CalculateNewCoordinateSystem(cv::Mat& rotation,
	                                  cv::Mat& translation,
	                                  CaptureCalibration& calibration) const;

	static bool IfFullAccess();

	static size_t ImageWidth(size_t channel);

	static size_t ImageHeight(size_t channel);

	static std::shared_ptr<const CameraSensor> GetDMVSCameraCameraCalibration(const std::string& name,
	                                                                          ImageChannel channel,
	                                                                          CameraModel camera_model,
	                                                                          const cv::Vec2d& depth_params,
	                                                                          const cv::Vec3d& depth_params_exp,
	                                                                          const ApplicationSettings& settings,
	                                                                          const cv::Mat& camera_matrix      = cv::Mat::eye(3, 3, CV_64F),
	                                                                          const cv::Mat& distortion_vector  = cv::Mat::zeros(5, 1, CV_64F),
	                                                                          const cv::Mat& rotation_matrix    = cv::Mat::eye(3, 3, CV_64F),
	                                                                          const cv::Mat& translation_vector = cv::Mat::zeros(3, 1, CV_64F));

	/**
	 * \brief
	 */
	static CriticalSection critical_section_lock;

	/**
	 * \brief
	 */
	std::shared_ptr<CaptureGrabber> grabber;

	/**
	 * \brief
	 */
	DeviceCurrentMode open_mode;

	/**
	 * \brief Frame counter
	 */
	InterlockedCounter frame_index;

	///**
	// * \brief Number of OpenCV camera port
	// */
	//int camera_port_1;

	/**
	 * \brief
	 */
	CaptureCalibration capture_calibration;

	/**
	 * \brief
	 */
	std::shared_ptr<CalibrationCache> cache;

	/**
	 * \brief
	 */
	std::shared_ptr<ConfigurationContainer> configuration_options;

	/**
	 * \brief
	 */
	std::shared_ptr<ReconstructorLaserDots3D> reconstructor;

	/**
	 * \brief
	 */
	bool stop;

	/**
	 * \brief
	 */
	bool m_snapshot;

	/**
	 * \brief Serial number of the connected
	 */
	std::string serial_number;

	/**
	 * \brief
	 */
	bool use_frame = false;

private:

	bool ConnectToDMVS();

	bool ConfigureTracking() const;

	bool Configure() const;

	static bool RetrieveGrey(size_t channel,
	                         cv::Mat& img);

	bool CreateDeviceCalibrationFile();

	bool UploadCalibration();

	bool SetBrightPointSettings() const;

	static void UseFrameCB(size_t type,
	                       void* data);

	static bool IsSupportedSensor();

	void UpdateReconstructor();

	void CaptureImageAndFindPlaneByMarkers(cv::Vec3d& plane_position,
	                                       cv::Vec3d& plane_normal,
	                                       ProjectorBoardDetector& board_detector,
	                                       std::map<size_t, CircularMarker>& detected_markers);
	static std::string GetPositionString(TablePosition position);

	void CaptureImageAndFindPlaneByMarkersOffline(cv::Vec3d& plane_position,
	                                              cv::Vec3d& plane_normal,
	                                              ProjectorBoardDetector& board_detector,
	                                              std::map<size_t, CircularMarker>& detected_markers,
	                                              TablePosition position) const;

	void FindTablePositionRep(DMVSSensor* capture_device,
	                          IKinematics* linear_stage,
	                          ProjectorBoardDetector& board_detector,
	                          std::map<size_t, CircularMarker>& first_image_marker,
	                          std::map<size_t, CircularMarker>& second_image_marker,
	                          const double step,
	                          std::vector<cv::Vec3d>& plane_positions,
	                          std::vector<cv::Vec3d>& plane_normals);

	bool FindDots(const cv::Mat& pattern_img0,
	              const cv::Mat& pattern_img1,
	              Dots& dots0,
	              Dots& dots1,
	              bool use_edge_improvement,
	              DotInfos* dots0_infos,
	              DotInfos* dots1_infos);

	static void IncreaseImageCounter(size_t type,
	                                 void* data);

	static bool GrabFromFile();

	bool InitCapture();

	std::string GetDeviceType() const;

	std::string GetSerialNumber() const;

	// Initial DMVS settings
	bool auto_exposure_;
	bool set_laser_;
	bool set_led_flash_constant_;
	bool position_interface_active_;
	bool use_frame_ = false;

	unsigned char led_power_0_;
	unsigned char led_power_1_;

	int mode_;

	double exposure_time_;
	double frame_rate_;

	/**
	 * \brief Timestamp of the last frame in milliseconds
	 */
	double timestamp_;

	std::string calibration_filename_;

	fs::path output_file_;
	fs::path input_file_;

	DMVSDevice* dmvs_device_;

	/**
	 * \brief Frame counter of grabber
	 */
	InterlockedCounter frame_grabber_count_;

	/**
	 * \brief Total number of frames (for files)
	 */
	size_t frame_count_;

	/**
	 * \brief Number of frames to skip before recording
	 */
	size_t skipped_frame_count_;

	/**
	 * \brief Width of the image
	 */
	size_t rgb_width_;

	/**
	 * \brief Height of the image
	 */
	size_t rgb_height_;

	/**
	 * \brief contains the device type string e.g. 'MS_Kinect', 'Asus_Xtion', 'Freestyle'
	 */
	std::string device_type_;
};
}
}
