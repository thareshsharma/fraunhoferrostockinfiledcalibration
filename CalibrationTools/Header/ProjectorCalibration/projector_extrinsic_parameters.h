#pragma once

struct ProjectorExtrinsicParameters
{
	ProjectorExtrinsicParameters() :
		rotation_matrix(cv::Mat(3, 3, CV_64F)),
		translation_vector(cv::Mat(3, 1, CV_64F))
	{
	}

	void GetTransformationMatrix(cv::Mat4d& transformation);

	cv::Mat rotation_matrix;
	cv::Mat translation_vector;
};

inline void ProjectorExtrinsicParameters::GetTransformationMatrix(cv::Mat4d& transformation)
{
	const auto rows = rotation_matrix.rows;
	const auto cols = rotation_matrix.cols;

	for(auto row = 0; row < rows; ++row)
	{
		for(auto col = 0; col < cols; ++col)
		{
			transformation.at<double>(row, col) = rotation_matrix.at<double>(row, col);
		}
	}

	transformation.at<double>(0, 3) = translation_vector.at<double>(0, 0);
	transformation.at<double>(1, 3) = translation_vector.at<double>(1, 0);
	transformation.at<double>(2, 3) = translation_vector.at<double>(2, 0);
	transformation.at<double>(3, 3) = 1;
}
