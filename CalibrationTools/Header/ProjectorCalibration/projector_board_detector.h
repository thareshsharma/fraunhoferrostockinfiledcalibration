#pragma once

#include "circular_marker.h"
#include "statistics_value_median_set.h"
#include "camera_sensor.h"

using namespace DMVS::CameraCalibration;

namespace Reconstruct
{
class ProjectorBoardDetector
{
public:
	ProjectorBoardDetector(std::shared_ptr<const CameraSensor> calibration,
	                       bool detect_only_markers = false);

	ProjectorBoardDetector(std::shared_ptr<const CameraSensor> calibration,
	                       bool detect_only_markers_in,
	                       int board_id,
	                       int origin_id,
	                       int x_id,
	                       int y_id,
	                       const std::map<int, cv::Point3f>& coded_markers,
	                       const std::map<int, cv::Point3f>& uncoded_markers);

	ProjectorBoardDetector(const ProjectorBoardDetector& in);

	ProjectorBoardDetector& operator =(const ProjectorBoardDetector& rhs);

	void AddCodedMarker(int marker_id,
	                    const cv::Point3f& marker_position);

	void AddUncodedMarker(int marker_id,
	                      cv::Point3f marker_position);

	bool GetCodedMarker(size_t id,
	                    cv::Vec3d& vector3d) const;

	bool GetMarker(int id,
	               cv::Vec3d& vec) const;

	//supply minGrey, maxGrey, greyStep: if deviation from usual steps as
	// defined with 8bit images is wanted.
	int Detect(const cv::Mat& image,
	           bool use_outer_circles = false,
	           bool refine_with_theoretical_centers = false,
	           bool if_mirrored_image = false,
	           int min_grey_in   = 0,
	           int max_grey_in   = 0,
	           int grey_step_in  = 0);

	int FitMarker();

	void GetBoardMarkerMap(std::map<size_t, CircularMarker>& markers) const;

	std::vector<CircularMarker> GetDetectedMarkersWithoutID() const;

	double DetectColorPlane(cv::Vec3d& position,
	                        cv::Vec3d& normal);

	bool GetOrientation(cv::Mat& rotation_vector,
	                    cv::Mat& translation_vector) const;

	void SetPlaneContour(const std::vector<cv::Vec3f>& plane_contour);

	std::vector<cv::Point3f> GetCurrentContourPoints() const;

	void DrawDetectionResult(cv::Mat& result_img);

	void GetPointsOnBoard(const std::vector<cv::Point3f>& points_3d,
	                      std::vector<int>& on_board_idx,
	                      double max_dist) const;

	double GetDistance(const cv::Point3f& pt3d) const;

	int GetMarkerPositions3d(std::vector<cv::Point3f>& marker3d);

	std::vector<cv::Vec3f> GetPlaneContourPts() const;

	double GetRMS() const;

	// this method is redundant with getCodedMarkerCoordinates and only kept for backwards compatibility
	std::map<int, cv::Point3f> GetMarkerCoordinatesInFile() const;

	void RedefineCoordinateSystemOxy();

	std::map<int, cv::Point3f> GetCodedMarkerCoordinates() const;

	std::map<int, cv::Point3f> GetUncodedMarkerCoordinates() const;

	std::map<int, cv::Point3f> GetAllMarkerCoordinates() const;

	cv::Vec3d FrameCoordinates(int id) const;

	cv::Mat GetImage() const;

	std::pair<int, cv::Point3f> GetOrigin() const;

	std::pair<int, cv::Point3f> GetX() const;

	std::pair<int, cv::Point3f> GetY() const;

	// separates board into two boards and returns the distance between
	// the relative positions of these boards.
	double GetOrientationTestQuality() const;

	int NumUnknownMarkers() const;

	int GetBoardId() const;

	void SetDetectedMarkerCoordinate(int id,
	                                 const CircularMarker& marker);

	// calculate camera orientation to board
	bool CalculateOrientation(bool filter_markers = false);

	void Reset();

	// match positions of detected markers with positions of uncoded markers on board and assign IDs
	void AssignIDsToDetectedMarkers();

	void AssignIDsToDetectedMarkers(std::vector<CircularMarker> detected_markers_without_id);

	void ClearDetectedMarkersWithoutID();

	void SetDetectedMarkersWithoutID(const std::vector<CircularMarker>& in);

	void SetOnsiteCalibrationMode(bool value);

	// convert and copy image
	void SetImage(const cv::Mat& image);

private:

	void UpdateOXZ();

	// Solves position for markers
	bool SolvePnP(const std::map<size_t, CircularMarker>& markers,
	              cv::Mat& rotation_vector,
	              cv::Mat& translation_vector) const;

	/**
	 * \brief One needs to 4 3d to 2d correspondences to solve the board pose. If there are only 3 correspondences
	 * found by using the coded markers, there may be the possibility to use a fourth, uncoded, marker. This
	 * search for an uncoded marker is done by this function.
	 */
	bool IdentifyUncodedMarkerToReach4DetectedMarkers();

	std::map<int, cv::Point2f> GetProjectedMarkerCenters(const std::map<size_t, CircularMarker>& detected_marker,
	                                                     const cv::Mat& rotation_vector,
	                                                     const cv::Mat& translation_vector) const;

	StatisticVal<double> GetStats() const;

	cv::Mat rotation_vector_;
	cv::Mat translation_vector_;

	std::shared_ptr<const CameraSensor> calibration_;
	cv::Mat image_;

	cv::Vec3d position_;
	cv::Vec3d normal_;

	// the marker coordinates of the board as stated in the board calibration file
	std::map<int, cv::Point3f> coded_markers_;

	// the coordinates of uncoded markers on the board
	std::map<int, cv::Point3f> uncoded_markers_;

	// the detected marker coordinates
	std::map<size_t, CircularMarker> detected_board_markers_;

	// additional detected coordinates of uncoded markers
	std::vector<CircularMarker> detected_markers_without_id_;

	std::vector<cv::Vec3f> plane_contour_;
	std::vector<cv::Point3f> current_contour_points_;

	int origin_id_;
	int x_id_;
	int y_id_;
	int board_id_;
	int num_unknown_marker_;

	size_t min_grey_;
	size_t max_grey_;
	size_t grey_step_;

	bool detected_;
	bool detect_only_markers_;
	bool on_site_calibration_mode_;
};
}
