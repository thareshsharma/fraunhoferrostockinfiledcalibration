#pragma once

/**
 \brief LSFitPlane performs least square fit of a plane given a vector of points
*/
class LSFitPlane
{
	/// resulting position of the plane
	cv::Vec3d planePos;
	/// resulting normal of the plane
	cv::Vec3d planeNormal;

public:
	struct PlaneParam
	{
		cv::Vec3d position;
		cv::Vec3d normal;
		cv::Vec3d majorAxis;
		cv::Vec3d minorAxis;
		double majorAxisLength;
		double minorAxisLength;
		double ptDrift;
		double ptDist;
		double semiAxesRatio;
	};

	/// fit: Performs the fit of a plane against the given points
	/// \param pointData: std::vector of points
	/// Returns the resulting plane parameters
	PlaneParam fit(const std::vector<cv::Vec3d>& points,
	               bool calculateAxisLengths = false);
};