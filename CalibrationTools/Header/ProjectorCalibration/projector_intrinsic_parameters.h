#pragma once

struct ProjectorIntrinsicParameters
{
	ProjectorIntrinsicParameters() :
		projector_intrinsic_matrix(cv::Mat(3, 3, CV_64F)),
		distortion_vector(cv::Mat(5, 1, CV_64F)),
		pattern_definition(cv::Mat(11665, 4, CV_64F))
	{
	}

	cv::Mat projector_intrinsic_matrix;

	// Order k1, k2, p1, p2, k3
	cv::Mat distortion_vector;

	cv::Mat pattern_definition;
};