#pragma once

#include "projector_board_detector.h"
#include "camera_sensor.h"

class BoardDetectorLoader
{
public:
	explicit BoardDetectorLoader(bool transform_on_load = true);

	// Implement the following properly if needed:
	BoardDetectorLoader& operator =(const BoardDetectorLoader& other) = delete;
	BoardDetectorLoader& operator =(BoardDetectorLoader&& other)      = delete;
	BoardDetectorLoader(const BoardDetectorLoader& other)             = delete;
	BoardDetectorLoader(BoardDetectorLoader&&)                        = delete;

	~BoardDetectorLoader() noexcept;

	bool ReadFile(const std::string& file,
	              int uncoded_marker_start_id = 10001);

	Reconstruct::ProjectorBoardDetector CreateDetector(const std::shared_ptr<const CameraSensor>& calibration,
	                                                   bool detect_only_markers = false) const;

private:
	void UpdateOXZ();
	void RedefineCoordinateSystemOXY();

	// the marker coordinates of the board as stated in the board calibration file
	std::map<int, cv::Point3f> coded_markers_;

	// the coordinates of uncoded markers on the board
	std::map<int, cv::Point3f> uncoded_markers_;

	int board_id_;
	int origin_id_;
	int x_id_;
	int y_id_;
	std::vector<cv::Vec3f> plane_contour_;
	const bool transform_on_load_;
};
