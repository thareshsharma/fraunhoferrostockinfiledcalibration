#pragma once

#include "enum_image_channel_type.h"
#include "projector_extrinsic_parameters.h"
#include "projector_intrinsic_parameters.h"

struct LaserInfo
{
	LaserInfo();

	LaserInfo(const std::string& laser_name,
	          ImageChannel laser_channel);

	std::string name;
	ImageChannel channel;
	ProjectorIntrinsicParameters intrinsics;
	ProjectorExtrinsicParameters extrinsics;
	cv::Mat pattern_definition;
};

inline LaserInfo::LaserInfo() :
	name("no_name"),
	channel(CHANNEL_DEFAULT)
{
}

inline LaserInfo::LaserInfo(const std::string& laser_name,
                            const ImageChannel laser_channel):
	name(laser_name),
	channel(laser_channel)
{
}
