#pragma once

#include "configuration_container.h"
#include "pattern_rotation_estimator.h"
#include "fit_plane.h"

namespace Reconstruct
{

using namespace DMVS::CameraCalibration;

class Projector
{
public:
	enum Result
	{
		MORE_DATA,
		FINISHED,
		STOPPED
	};

	enum CalibrationStep
	{
		STEP1 =0,
		STEP2 = 1,
		STEP3 = 2,
		STEP4 = 3,
	};

	enum CaptureMode
	{
		TRACKING = 0,
		CALIBRATION_CENTRAL_DOT,
		CALIBRATION_PATTERN_ALL,
		CALIBRATION_CAMERAS,
		CALIBRATION_PATTERN_BRIGHT_ONLY,
		CALIBRATION_PATTERN_ZERO_ONLY,
		CALIBRATION_WHITE_BALANCE,
		CALIBRATION_INFIELD,
		CALIBRATION_CAMERAS_LASER_ON
	};

	struct SymRms
	{
		int index;
		double rms;
		int num_matches;
	};

	struct DotInfoType
	{
		int beam_idx;
		float dia;
		float brightness;
	};

	/**
	 * \brief Constructor for debug testing
	 */
	Projector(const std::vector<cv::Point2f>& beams,
	          const std::vector<cv::Point2f>& bright_beams);

	/**
	 * \brief Constructor with initial pose of projector
	 * \param calibration initial pose and camera matrix of projector
	 * \param options initial pose and camera matrix of projector
	 */
	Projector(const std::shared_ptr<const CameraSensor>& calibration,
	          const std::shared_ptr<ConfigurationContainer>& options);

	virtual ~Projector();

	/**
	 * \brief Add new 3D points to projector.
	 * Added 3d points are projected into the projector 2d image with projection
	 * from Constructor. Then they are clustered in 2d and the cluster index
	 * is kept for every 3d point.
	 * \param reconstructed_3d_points New 3d points to be added for projector pattern calibration
	 * \param infos infos
	 */
	void Add3dPoints(const std::vector<cv::Point3f>& reconstructed_3d_points,
	                 const std::vector<DotInfoType>& infos);

	/**
	 * \brief Check if calibration finished and get current projector pose
	 * \param result current projector pose
	 * \return Result (MORE_DATA, FINISHED, STOPPED)
	 */
	bool GetCalibrationResult(std::shared_ptr<CameraSensor>& result) const;

	/**
	 * \brief Update projector rotation to Z = beam
	 * Rotate the current projector to let the given beam point to Z.
	 * X is then orthogonal to unityY and beam. Y completed right-handed.
	 * \param zero_order_beam 2d point on virtual projector image that defines new Z
	 * \param calibration projector calibration
	 * \param rotation_matrix rotation matrix
	 * \return 0 if everything goes well.
	 */
	int RotateProjectorToCenter(cv::Point2f& zero_order_beam,
	                            std::shared_ptr<CameraSensor> calibration = nullptr,
	                            cv::Mat rotation_matrix                   = cv::Mat::eye(3, 3, CV_64F)) const;

	/**
	 * \brief Get a feedback image from the calibration
	 * Returns the virtual projector image (same size as IR cameras)
	 * \return image showing the laser beams.
	 */
	cv::Mat GetFeedbackImage();

	/**
	 * \brief Get currently required Capture mode
	 * The calibration requires different capture mode within different phases
	 * The capture loop checks this and updates the Mode if required
	 * \return Capture mode as currently required by calibration
	 */
	CaptureMode GetCaptureMode() const;

	/**
	 * \brief Get the detected laser beam coordinates
	 * All detected and calibrated laser beams given as 2d coordinate in projector image
	 * and brightness as third value.
	 * \param laser_dots Matrix (nx3, CV_64F) with coordinates and brightness
	 * \return number of calibrated beams
	 */
	int GetLaserDotMatrix(cv::Mat& laser_dots) const;

	/**
	 * \brief Update laser dot matrix
	 * \param laser_dots laser dots which have to be updated
	 * \return if successful
	 */
	int UpdateLaserDotMatrix(cv::Mat& laser_dots);

	/**
	 * \brief Force next calibration step
	 */
	void ForceNextCalibrationStep();

	/**
	 * \brief Initialize calibration
	 */
	void InitCalibration();

	/**
	 * \brief Initialize recalibration
	 * \param aligned_doe aligned doe
	 */
	void InitRecalibration(const cv::Mat& aligned_doe);

	/**
	 * \brief
	 * \param beam_idx
	 * \return
	 */
	int GetUseCount(int beam_idx);

	/**
	 * \brief Get the current step for displaying it.
	 */
	int GetCalibrationStep() const;

	/**
	 * \brief
	 */
	int GetStep4PointsCount() const;

	/**
	 * \brief
	 */
	int GetStep1GreenPointsCount() const;

	/**
	 * \brief Set path to doe file
	 */
	void SetDoeYmlPath(std::string path);

	/**
	 * \brief Return the minimum number of points to detect in the last step (step 4)
	 */
	int GetMinDotNumberToFinish() const;

	/**
	 * \brief Image where step results are drawn into
	 */
	cv::Mat CrossHairImage() const;

	/**
	 * \brief Check and update calibration step. Checks whether the next
	 * calibration step can be started. Calls the check if the current
	 * step is finished and switches to the next step and initializes
	 * the next step.
	 */
	void CheckCalibrationStep();

	/**
	 * \brief Set the folder where the projector calibration log lies in
	 * \param path path of the file
	 */
	void SetOutputPath(const std::string& path);

	/**
	 * \brief Get X component of paraboloid compensation
	 */
	double GetParaboloidCompensationX() const;

	/**
	 * \brief Get Y component of paraboloid compensation
	 */
	double GetParaboloidCompensationY() const;

	/**
	 * \brief
	 * \param a
	 * \param b
	 * \return
	 */
	static bool PairCmp2nd(std::pair<int, double> a,
	                       std::pair<int, double> b);

	/**
	 * \brief
	 * \param plane_pts3d
	 * \param used_plane_pts
	 * \param orig_plane_pts_indices
	 * \return
	 */
	static LSFitPlane::PlaneParam FitPlane2(const std::vector<cv::Vec3d>& plane_pts3d,
	                                        std::vector<int>& used_plane_pts,
	                                        const std::vector<int>& orig_plane_pts_indices);

	/**
	 * \brief Check whether a vector points to the direction of the DOE pattern main axis (tolerance 5deg)
	 * \param[in] direction direction to be checked against DOE main direction
	 * \return 0: not close to main axis
	 *		   1: close to horizontal
	 *		   2: close to vertical
	 */
	static int IsDoeOrthogonalDirection(cv::Vec2d direction);

	/**
	 * \brief Check whether a vector points to the direction of the DOE pattern's 45� rotated main axis (tolerance 5deg)
	 * \param direction direction to be checked against 45� rotated DOE main directions
	 * \return 0 : not close to any diagonal
	 *		   1 : close to diagonal1
	 *		   2 : close to diagonal2
	 */
	static int IsDiagonal(cv::Vec2d direction);

private:
	/**
	 * \brief Information of a laser beam for calibration
	 */
	struct LaserBeam
	{
		// Mean point of projected 3d points based on the current projector pose
		cv::Point2f mean_point;

		// all 3d points that suppose to be on the beam
		std::vector<cv::Point3f> beam_3d_points;

		std::vector<DotInfoType> dot_infos;

		// number of points that created meanPt (for incremental mean calculation)
		int number_of_points;

		// nearest 3d point of the beam
		float nearest_3d_point_to_beam;

		// farthest3d point of the beam
		float farthest_3d_point_to_beam;

		// points describe a stable beam (by number of points and near/far distance)
		bool is_beam;
	};

	struct CentralBeamScore
	{
		double brightness;
		double num_matches;
		double symmetry;
	};

	struct ErrorCrossFit
	{
		double sum_square_line_fit_horizontal = -1.0;
		double sum_square_line_fit_vertical   = -1.0;
		double paraboloid_fit_horizontal      = -1.0;
		double paraboloid_fit_vertical        = -1.0;
		double sum_square_line_fit_length     = -1.0;
		double paraboloid_fit_length          = -1.0;

		void SetSumSquareLineFitLength();
		void SetParaboloidFitLength();
	};

	struct Cross
	{
		cv::Point2f center;
		cv::Point2f vertical_direction;
		cv::Point2f horizontal_direction;
		std::vector<cv::Point2f> vertical_cross_points;
		std::vector<cv::Point2f> horizontal_cross_points;
		ErrorCrossFit errors;
	};

	/**
	 * \brief Add points to beams
	 * \param points Points to be added
	 * \param infos points info
	 * \param beams beams
	 * \param calibration projector calibration
	 */
	void AddPointsToBeams(const std::vector<cv::Point3f>& points,
	                      const std::vector<DotInfoType>& infos,
	                      std::vector<LaserBeam>& beams,
	                      std::shared_ptr<CameraSensor> calibration = nullptr);

	/**
	 * \brief Calculate exact projector position by a bundle of beams
	 * Given a bundle of laser beams the projector position is calculated by fitting a line
	 * for every beam and get the best intersection point of all beam lines
	 * \param position new projector position
	 * \param  beams vector of laser beams
	 * \return true: OK false: failed
	 */
	bool CalculateProjectorPosition(cv::Point3f& position,
	                                const std::vector<LaserBeam>& beams) const;

	/**
	 * \brief Updates the 2d mean point of every laser beam with current projector pose
	 * LaserDot contains the 2d coordinate on the virtual projector image plane. It is calculated
	 * by averaging the projection points of all 3d points that belong to the beam.
	 * Needs to be called after changing projectors pose
	 * \param dots vector of laser beams, meanPt is updated by projecting pts3d points
	 * \param calibration projector calibration
	 */
	void UpdateProjections(std::vector<LaserBeam>& dots,
	                       std::shared_ptr<const CameraSensor> calibration = nullptr) const;

	/**
	 * Refine an initial guess projector position by points on the central beam
	 * Given a vector with points on the beam, a line is fitted through the points and the
	 * x and y position is updated by the line at z = initial z
	 * \param[in,out] pos z is input and not affected  x and y are output
	 * \param     beam_pts vector of 3d points on the central beam in device coordinates
	 * \return 0: OK  <0:error
	 */
	int EstimateCenterByCentralBeam(cv::Point3f& pos,
	                                const std::vector<cv::Point3f>& beam_pts) const;

	/**
	 * \brief Gets projector calibration
	 */
	std::shared_ptr<CameraSensor> GetProjectorCalibration() const;

	/**
	 * \brief Check if step1 is finished.
	 * Checks if all prerequisites for step1 are given and executes step 1 calculations
	 * \return true: Step1 finished  false: not yet finished, need more data to finish step1
	 */
	bool Step1Done();

	/**
	 * \brief Check if step2 is finished. Get central dot by checking symmetry of every central
	 * candidate's dot pattern projection
	 * Checks if all prerequisites for step2 are given and executes step 2 calculations
	 * \return true: Step2 finished  false: not yet finished, need more data to finish step2
	 */
	bool Step2Done();

	/**
	 * \brief Check if step3 is finished. Try to detect the cross of points around the central point around 0,0
	 * Checks if all prerequisites for step3 are given and executes step 3 calculations
	 * \return true: Step3 finished  false: not yet finished, need more data to finish step3
	 */
	bool Step3Done();

	/**
	 * \brief Check if step4 is finished.
	 * Checks if all prerequisites for step4 are given and executes step 4 calculations
	 * \return true: Step4 finished  false: not yet finished, need more data to finish step4
	 */
	bool Step4Done();

	/**
	 * \brief Retry central beam
	 */
	void RetryCentralBeam();

	/**
	 \brief Check if step3 is finished.
	 */
	bool Step100Done();

	/**
	 \brief Check if step3 is finished.
	 */
	bool Step101Done();

	/**
	 * \brief initialize step1
	 * \return true: success (currently it cannot fail)
	 */
	bool InitStep1();

	/**
	 * \brief initialize step2
	 * \return true: success (currently it cannot fail)
	 */
	bool InitStep2();

	/**
	 * \brief initialize step3
	 * \return true: success (currently it cannot fail)
	 */
	bool InitStep3();

	/**
	 * \brief initialize step4
	 * \return true: success (currently it cannot fail)
	 */
	bool InitStep4();

	/**
	 \brief Init step 100
	 */
	bool InitStep100();

	/**
	 \brief Init step 101
	 */
	bool InitStep101();

	/**
	 * \brief Update projections and beams
	 * \param beams
	 */
	void UpdateProjectionsAndBeams(std::vector<LaserBeam>& beams);

	/**
	 * \brief Tries to fill missing points within the area of given points
	 * \param candidate_sym_pts Candidate points close to the expected central beam, is completed with missing central point
	 * \param try_mean try mean
	 * \return 0: nothing added 1: candidate added
	 *							2: candidate added, already identified as central beam
	 */
	int TryFillMissingCentralBeam(std::vector<cv::Point2f>& candidate_sym_pts,
	                              bool try_mean) const;

	/**
	 * \brief Get used beams
	 */
	std::vector<LaserBeam>& GetUsedBeams();

	/**
	 * \brief Add points 3d to recalibration
	 * \param points_3d points to be added
	 * \param infos dot infos
	 */
	void AddPoints3dRecalibration(const std::vector<cv::Point3f>& points_3d,
	                              const std::vector<DotInfoType>& infos);

	/**
	 * \brief Add points 3d
	 * \param point_3d points 3d
	 * \param info dot type info
	 * \param point_2d points 2d
	 * \param beams laser beams
	 * \param beam_idx beam index
	 */
	void Add3dPoint(cv::Point3f point_3d,
	                DotInfoType info,
	                cv::Point2f point_2d,
	                std::vector<LaserBeam>& beams,
	                int beam_idx);

	/**
	 * \brief Get undistorted doe
	 * \param aligned_doe aligned doe
	 * \param pts points
	 */
	void GetUndistortDOE(cv::Mat aligned_doe,
	                     std::vector<cv::Vec2f>& pts);

	/**
	 * \brief
	 * \param doe
	 * \param pts2d
	 * \param doe_correspondences
	 */
	void ApplyParaboloidCompensation(cv::Mat& doe,
	                                 std::vector<cv::Point2f>& pts2d,
	                                 std::vector<int>& doe_correspondences);

	/**
	 * \brief
	 */
	bool RotateProjectorToBeamAndProject();

	/**
	 * \brief
	 */
	bool RotateProjectorToBeamAndProjectDMVS();

	/**
	 * \brief OFfset curvature is empiric
	 * \param beams
	 * \param bright_beams
	 * \param estimated_center
	 * \param detected_center
	 * \param curvature_offset
	 * \return
	 */
	int GetCentralBeam(const std::vector<cv::Point2f>& beams,
	                   const std::vector<cv::Point2f>& bright_beams,
	                   cv::Point2f& estimated_center,
	                   cv::Point2f& detected_center,
	                   double curvature_offset = 0.000005) const;

	/**
	 * \brief
	 * \param p1
	 * \param p2
	 * \return
	 */
	float PointDistance(const cv::Point2f& p1,
	                    const cv::Point2f& p2) const;

	/**
	 * \brief
	 * \param beams
	 * \param center
	 * \param radius
	 * \param candidates
	 */
	void GetCandidates(const std::vector<cv::Point2f>& beams,
	                   const cv::Point2f& center,
	                   int radius,
	                   std::vector<size_t>& candidates) const;

	/**
	 * \brief
	 * \param bright_beams
	 * \param center_point
	 * \param tolerances
	 * \param radius
	 * \return
	 */
	Cross FitCross(const std::vector<cv::Point2f>& bright_beams,
	               const cv::Point2f& center_point,
	               const cv::Point2f& tolerances = cv::Point2f(10, 8),
	               int radius                    = 30) const;

	/**
	 * \brief
	 * \param plane1
	 * \param plane2
	 * \param position
	 * \param direction
	 * \return
	 */
	bool IntersectPlanes(LSFitPlane::PlaneParam& plane1,
	                     LSFitPlane::PlaneParam& plane2,
	                     cv::Vec3d& position,
	                     cv::Vec3d& direction) const;

	/**
	 * \brief
	 * \param bright_beams
	 * \param estimated_center
	 * \param cross_fits
	 * \param direction_vertical
	 * \param direction_horizontal
	 * \return
	 */
	cv::Mat DrawDebugImage(const std::vector<cv::Point2f>& bright_beams,
	                       const cv::Point2f& estimated_center,
	                       std::vector<Cross>& cross_fits,
	                       const cv::Point2f& direction_vertical   = cv::Point2f(0.0, 1.0),
	                       const cv::Point2f& direction_horizontal = cv::Point2f(1.0, 0.0)) const;


	/**
	 * \brief
	 * \param points_3d
	 * \return
	 */
	static LSFitPlane::PlaneParam FitPlaneProjectorImage(std::vector<cv::Vec3d> points_3d);

	/**
	 * \brief
	 * \param bright_beams
	 * \param center_point
	 * \param tolerances
	 * \param radius
	 * \return
	 */
	Cross FindCrossAndGetLineRMS(const std::vector<cv::Point2f>& bright_beams,
	                             const cv::Point2f& center_point,
	                             const cv::Point2f& tolerances = cv::Point2f(10, 8),
	                             const int radius              = 30) const;

	/**
	 * \brief
	 * \param line_params
	 * \param points
	 * \return
	 */
	std::vector<float> PointLineDist(cv::Vec4f line_params,
	                                 std::vector<cv::Vec2f>& points);
	/**
	 * \brief
	 * \param origin
	 * \param direction
	 * \param point
	 * \return
	 */
	float PointLineDist(const cv::Point2f& origin,
	                    const cv::Point2f& direction,
	                    const cv::Point2f& point) const;

	/**
	 * \brief
	 * \param points2d
	 * \param use_y
	 * \return
	 */
	static double FitParabolaAndReturnQuadraticPart(std::vector<cv::Point2f>& points2d,
	                                                bool use_y = true);

	/**
	 * \brief
	 * \param projector_calibration
	 * \param center_candidate
	 * \param num_matches
	 * \return
	 */
	double GetPointSymmetryRmsByCoordinate(const std::shared_ptr<CameraSensor>& projector_calibration,
	                                       cv::Point2f center_candidate,
	                                       int& num_matches);

	/**
	 * \brief
	 * \param projector_calibration
	 * \param beam
	 * \param num_matches
	 * \return
	 */
	double GetPointSymmetryRmsByBeam(std::shared_ptr<CameraSensor> projector_calibration,
	                                 const LaserBeam& beam,
	                                 int& num_matches);

	float search_window_x_;
	float search_window_y_;
	float min_beam_length_;

	int search_window_width_;
	int search_window_height_;
	int min_num_beams_;

	bool use_dmvs_projector_calibration_;

	// The folder where the projector calibration log lies in
	std::string calibration_output_path_;

	// current camera matrix and pose of projector, initialized in constructor
	std::shared_ptr<CameraSensor> calibration_;

	// current calibration step
	int calibration_step_;
	bool next_step_forced_;

	// Path to DOE yml file.
	std::string doe_yml_path_;

	// flag if calibration is done completely
	bool calibration_done_;

	// Detected laser beams of calibration step 0
	std::vector<LaserBeam> laser_beams_step0_;

	// Detected laser beams of calibration step 1
	std::vector<LaserBeam> laser_beams_step1_;

	// Detected laser beams of calibration step 2
	std::vector<LaserBeam> laser_beams_step2_;

	int step1_green_points_;
	int green_points_;

	// DOE pattern rotation detector
	PatternRotationDetector rotation_detector_step1_;

	// User feed back image for calibration
	cv::Mat feedback_image_;

	// The Freestyle capture mode that is required for the
	// current step (defines camera settings e.g shutter timings)
	CaptureMode capture_mode_;

	// Distance for clustering of beam points
	double max_beam_point_projection_distance_;

	// contains the indices of the beams m_PtsStepX that have been classified as stable beam
	std::vector<int> stable_beams_indexes_;

	cv::Mat aligned_doe_;

	// cv::Mat m_undistortedDOE;
	int no_new_points_;

	// The image the cross is drawn into
	cv::Mat cross_hair_image_;

	bool step2_center_point_added_as_bright_;

	cv::Point2f central_beam_;

	std::shared_ptr<ConfigurationContainer> options_;

	//Save the paraboloid correction
	double doe_correction_rms_x_ = 0.0;
	double doe_correction_rms_y_ = 0.0;
};


}
