#pragma once

// modulo calc unspecified by default
template <class T>
class mod_trait
{
	//static T mod(T v,T m) = 0;
};

/// modulo specification for doubles
template <>
class mod_trait<double>
{
public:
	inline static double mod(double v,
	                         double m)
	{
		double int_part;
		const double frac = modf(v / m, &int_part);
		return frac * m;
	}
};

///  modulo specification for ints
template <>
class mod_trait<int>
{
public:
	inline static int mod(int v,
	                      int m)
	{
		return v % m;
	}
};

/**
 Template used for calculations with cyclic values (like encoder angles and phase angles).
 It is a genereal modulo calculation class.
 The template parameter can be every signed type (double, int, short ...).
 For double and int the modulo calculations are implemented in mod_trait templates.

 If cycle 0 is provided, the value becomes a NaN. Also if wrong operation is done
 with instances containing different cycles.
*/
template <class T>
class CyclicValue
{
public:

	T cycle() const
	{
		return _cycle;
	} ///< must not be 0

	T value() const
	{
		assert(isNan() || _value >= T(0));
		assert(isNan() || _value < cycle());
		return _value; ///< in range [0..cycle[
	}

	bool isNan() const
	{
		return _isNan;
	} ///< true if value is a NaN (cycle=0)

	/// sets new value preserving cycle
	void set(const T value)
	{
		if(value < T(0))
		{
			_value = _cycle - mod_trait<T>::mod(-value, _cycle);
			// for floating-point values, must correctly handle border case value=-delta, where delta is much smaller than cycle
			// -> in this case,_value might get rounded to _cycle!
			if(_value == _cycle)
			{
				_value = T(0);
			}
		}
		else if(value >= _cycle)
			_value = mod_trait<T>::mod(value, _cycle);
		else
			_value = value;
		assert(_value >= 0 && _value < _cycle);
	}

	/// creates new cyclic value as NaN
	CyclicValue() :
		_value(0),
		_cycle(0),
		_isNan(true)
	{
	}

	/// creates new cyclic value with given cycle range
	CyclicValue(T valueIn,
	            T cycleIn) :
		_value(valueIn),
		_cycle(cycleIn),
		_isNan(false)
	{
		if(_cycle == 0)
			_isNan = true;
		else
			set(valueIn);
	}

	/// copy-constructor
	CyclicValue(const CyclicValue<T>& cv) :
		_value(cv.value()),
		_cycle(cv.cycle()),
		_isNan(cv.isNan())
	{
	}

	/// assignment
	CyclicValue& operator=(const CyclicValue<T>& cv)
	{
		_value = cv.value();
		_cycle = cv.cycle();
		_isNan = cv.isNan();
		return *this;
	}

	/// adds another cv. Cycle must be the same
	CyclicValue operator+(const CyclicValue<T>& cv) const
	{
		if(cv.cycle() != _cycle)
		{
			assert(false);
			CyclicValue tmp(0, 0); //return NAN
			return tmp;
		}
		CyclicValue<T> tmp(_value + cv.value(), _cycle);
		return tmp;
	}

	/// increments internal value by given value. Preserves cycle.
	CyclicValue& operator+=(const T& value)
	{
		set(_value + value);
		return *this;
	}


	/// substracts given cv. Cycle must be the same.
	CyclicValue operator-(const CyclicValue<T>& cv) const
	{
		if(cv.cycle() != _cycle)
		{
			assert(false);
			CyclicValue tmp(0, 0); //return NAN
			return tmp;
		}
		CyclicValue<T> tmp(_value - cv.value(), _cycle);
		return tmp;
	}

	/// decrements internal value by given value. Preserves cycle.
	CyclicValue& operator-=(const T& value)
	{
		set(_value - value);
		return *this;
	}

	/// multiplies by a factor
	CyclicValue operator*(const T value) const
	{
		CyclicValue<T> tmp(_value * value, _cycle);
		return tmp;
	}

	/// divides
	CyclicValue operator/(const T divisor) const
	{
		assert(divisor != 0);

		return CyclicValue<T>(_value / divisor, _cycle);
	}

	/** returns step which absolute value is smaller than half cycle.
		(e.g. corresponds to shortest step from angle to angle)
		For example:
		 double d = AngleEnd.relativeTo(AngleStart); // abs(d) < PI
		 AngleStart+=d; // will always give AngleEnd
	*/
	T relativeTo(const CyclicValue<T>& cv) const
	{
		CyclicValue<T> tmp((*this) - cv);
		if(tmp.isNan())
			return 0;
		if(tmp.value() > _cycle / 2)
			return tmp.value() - _cycle;
		if(tmp.value() <= -_cycle / 2)
			return tmp.value() + _cycle;
		return tmp.value();
	}

	/**
	  returns true if this value lies inside the range defined by [from,to].
	*/
	bool inside(const CyclicValue<T>& from,
	            const CyclicValue<T>& to) const
	{
		CyclicValue<T> tmp1(to - from);
		assert(!tmp1.isNan());

		if(tmp1.isNan())
			return false;

		CyclicValue<T> tmp2(to - (*this));
		assert(!tmp2.isNan());

		if(tmp2.isNan())
			return false;

		return tmp2.value() <= tmp1.value();
	}

	/// computes average of this value with given value preserving cycle
	void averageWith(const CyclicValue<T>& cv)
	{
		T step = relativeTo(cv);
		set(cv.value() + step / 2);
	}

protected:
	bool _isNan;

private:
	T _value;
	T _cycle;
};

/// formatting cyclic value for string output
/// \relates String
template <class T>
std::string& operator<<(std::string& s,
                        const CyclicValue<T>& v)
{
	return v.isNan() ? "#NaN" : "(" + v.value() + "," << v.cycle() + ")";
}
