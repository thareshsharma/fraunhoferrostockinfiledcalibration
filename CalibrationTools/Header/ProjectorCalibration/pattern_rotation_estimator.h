#pragma once

/**
 * \brief Estimate the two principle directions of the pattern
 * and estimate the rotation
 */

namespace Reconstruct
{
class PatternRotationDetector
{
public:
	// Up or down (Image coordinates: 0,0 is top left)
	// when going clockwise
	//		   UP(0)
	//			*
	// LEFT(3)  *	RIGHT(1)
	//			*
	//		  DOWN(2)
	enum Direction
	{
		UP = 0,
		RIGHT = 1,
		DOWN =2,
		LEFT = 3,
		UNKNOWN = 4
	};

	PatternRotationDetector();

	void SetPoints(std::vector<cv::Vec2f>& points);

	int LoadOriginalPattern(const char* file);

	int ExpandCross(cv::Vec2f center,
	                std::vector<cv::Vec2f> points);

	int ExpandCross(cv::Vec2f center);

	std::vector<int> GetExpandedPoints(Direction direction);

	void CalculateTransformation();

	cv::Mat& GetAlignedDOE();

	int GetCenter() const;

	cv::Vec4f GetEndpointsVertical() const;

	cv::Vec4f GetEndpointsHorizontal() const;

	/**
	 * \brief returns the image the detected cross is drawn into.
	 */
	cv::Mat CrossImage() const;

	/**
	 * \brief Returns the minimum number of points which can define each of the 4 directions
	 * of a cross around the central point.
	 */
	static int GetNumPointsOnEachCrossDirection();

private:
	FILE* log_file_;

	cv::Point2f horizontal_end_points_[2];
	cv::Point2f vertical_end_points_[2];
	int center_;

	cv::Mat doe_design_data_;
	cv::Mat aligned_design_data_;
	cv::Mat cross_image_;

	// DOE design points and bright cross points expansion
	std::vector<cv::Vec2f> doe_image_coords_bright_;
	std::vector<cv::Vec2f> doe_image_coords_all_;
	std::vector<int> doe_urdl_points_[4];

	// measured pattern points and cross points expansion
	std::vector<cv::Vec2f> pattern_points_;
	std::vector<int> urdl_pts_[4];
};
}
