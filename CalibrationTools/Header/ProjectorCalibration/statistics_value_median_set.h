#pragma once

// -----------------------------------------------------------------------
// S t a t i s t i c V a l
// -----------------------------------------------------------------------
// Template class to store statistic values

//2013-06-25 JBU: Secure PointSet statistic access by locking
//Need to derive statisticval from basenode to allow for
//access secured by locking in PointSet.
//Although it is derived from basenode it does not have
//a protected destructor (after discussion with MOS).
//Do not manually call delete or ref/unref on such an object.
//
// ---------------------------------------------------------------------------
// Check for number being around zero.
template <class T>
inline bool approxZero(const T in,
                       const T prec = T(1e5))
{
	const int result = static_cast<int>(round(in * prec));
	return result == 0;
}

//	Check for two numbers being approximately equal.
template <class T>
inline bool approxEqual(const T a,
                        const T b,
                        const T prec = T(1e5))
{
	return approxZero(a - b, prec);
}

template <class T>
class StatisticVal
{
public:
	T meanVal;   ///< the mean value JBU: should this not always be double/float?
	T minVal;    ///< the minimal value
	T maxVal;    ///< the maximal value
	T deviation; ///< the deviation value JBU: should this not always be double/float? Ist the formula correct??

	StatisticVal(T mean = 0,
	             T min  = 0,
	             T max  = 0,
	             T dev  = 0)
	{
		meanVal   = mean;
		minVal    = min;
		maxVal    = max;
		deviation = dev;
	}

	StatisticVal(const StatisticVal<T>& src)
	{
		init(src);
	}

	~StatisticVal() throw()
	{
	}

	StatisticVal& operator =(const StatisticVal<T>& src)
	{
		init(src);
		return *this;
	}

	void init(const StatisticVal<T>& src)
	{
		meanVal   = src.meanVal;
		minVal    = src.minVal;
		maxVal    = src.maxVal;
		deviation = src.deviation;
	}

	//2013-06-10 JBU: Better gray value mapping in 3DViews
	//test if statistics container is empty.
	bool isEmpty() const
	{
		//if min and max are equal and zero, the object is empty.
		return (approxEqual<T>(maxVal, minVal) && approxZero<T>(maxVal));
	}
};


/// makes no sense for integral types.
/// Cause error by undefined template specialization.
template <>
class StatisticVal<int>;

/// out/in operators

template <class T>
inline std::ostream& operator <<(std::ostream& ost,
                                 const StatisticVal<T>& s)
{
	ost << s.meanVal << " [" << s.minVal << "," << s.maxVal << "] " << s.deviation;
	return ost;
}

template <class T>
inline std::istream& operator >>(std::istream& ist,
                                 StatisticVal<T>& s)
{
	char ch;
	ist >> std::ws >> s.meanVal >> ch >> s.minVal >> ch >> s.maxVal >> ch >> s.deviation;
	return ist;
}

// -----------------------------------------------------------------------
// S t a t i s t i c V a l S e t
// -----------------------------------------------------------------------
// Template class to store and set statistic values
template <class T>
class StatisticValSet : public StatisticVal<T>
{
public:
	long numPoints; ///< number of points set

	StatisticValSet(T mean = 0,
	                T min  = 0,
	                T max  = 0,
	                T dev  = 0)
		: StatisticVal<T>(mean, min, max, dev)
	{
		numPoints = 0;
	}

	StatisticValSet(const StatisticVal<T>& src)
	{
		numPoints = 0;
		init(src);
	}

	~StatisticValSet() throw()
	{
	}

	/// getter for public member to write templates that accept StatisticVal and StatisticValCyclic
	long getNumPoints() const
	{
		return numPoints;
	}

	/// reset: Prepare to start adding values
	void reset()
	{
		numPoints     = 0;
		this->meanVal = this->minVal = this->maxVal = this->deviation = 0;
	}

	/// add: Adds a point, keeps track of statistic parameter
	void add(const T& in)
	{
		this->meanVal += in;
		this->deviation += in * in;
		if(!numPoints)
			this->minVal = this->maxVal = in;
		else
		{
			if(in < this->minVal)
				this->minVal = in;
			else if(in > this->maxVal)
				this->maxVal = in;
		}
		numPoints++;
	}

	/// finish: Calculate parameter according to the number of input
	void finish()
	{
		if(!numPoints)
			return; // Nothing to do, no input
		this->meanVal /= numPoints;
		T variance = this->deviation / numPoints - this->meanVal * this->meanVal;
		if(variance < 0)
			variance = 0; // only reason are rounding errors
		this->deviation = (T)sqrt(variance);
	}

	/// add: Adds an additional set of points
	void add(const StatisticValSet<T>& in)
	{
		// Set must not have called finish() before calling.
		//if "this" empty -> takes values of "in"
		//it does not matter whether "in" is actually filled at this point.

		if(!this->numPoints)
		{
			this->minVal    = in.minVal;
			this->maxVal    = in.maxVal;
			this->meanVal   = in.meanVal;
			this->deviation = in.deviation;
			this->numPoints = in.numPoints;
			return;
		}

		this->meanVal += in.meanVal;
		this->deviation += in.deviation;
		this->numPoints += in.numPoints;

		//only take values min/max of "in" seriously if numpoints>0
		if(in.numPoints)
		{
			if(in.maxVal > this->maxVal)
				this->maxVal = in.maxVal;
			if(in.minVal < this->minVal)
				this->minVal = in.minVal;
		}
	}
};

/// makes no sense for integral types.
/// Cause error by undefined template specialization.
template <>
class StatisticValSet<int>;

// -----------------------------------------------------------------------
// S t a t i s t i c V a l M e d i a n
// -----------------------------------------------------------------------
// Template class to store statistic values and their median
template <class T>
class StatisticValMedian : public StatisticVal<T>
{
public:
	T medianVal; ///< median of all elements

	StatisticValMedian(T mean = 0,
	                   T min  = 0,
	                   T max  = 0,
	                   T dev  = 0)
		: StatisticVal<T>(mean, min, max, dev)
	{
	};

	StatisticValMedian(const StatisticValMedian<T>& src)
	{
		init(src);
	}

	~StatisticValMedian() throw()
	{
	}

	StatisticValMedian& operator =(const StatisticVal<T>& src)
	{
		init(src);
		return *this;
	}

	void init(const StatisticValMedian<T>& src)
	{
		StatisticVal<T>::init(src);
		medianVal = src.medianVal;
	}
};

/// makes no sense for integral types.
/// Cause error by undefined template specialization.
template <>
class StatisticValMedian<int>;

// -----------------------------------------------------------------------
// S t a t i s t i c V a l L i m i t s
// -----------------------------------------------------------------------
// Template class to store statistic values + limits defined by set class
template <class T>
class StatisticValLimits : public StatisticVal<T>
{
public:
	cv::Vec<T, 2> limits1;         ///< minimal/maximal limit 1
	cv::Vec<T, 2> limits2;         ///< minimal/maximal limit 2
	cv::Vec2d histLimitRates1; ///< limits 1 were calculated cutting given rate from both ends of histogram
	cv::Vec2d histLimitRates2; ///< limits 2 were calculated cutting given rate from both ends of histogram
	T quasiMedian;             ///< the center value of the histogram (50perc below, 50perc above this value)

	StatisticValLimits(T mean        = 0,
	                   T min         = 0,
	                   T max         = 0,
	                   T dev         = 0,
	                   T quasiMedian = 0)
		: StatisticVal<T>(mean, min, max, dev), quasiMedian(quasiMedian)
	{
		limits1         = limits2         = cv::Vec<T, 2>(T(0), T(0));
		histLimitRates1 = histLimitRates2 = cv::Vec2d(0, 0);
	}

	StatisticValLimits(const StatisticValLimits<T>& src)
	{
		init(src);
	}

	~StatisticValLimits() throw()
	{
	}

	//@}

	StatisticValLimits& operator =(const StatisticVal<T>& src)
	{
		init(src);
		return *this;
	}

	void init(const StatisticValLimits<T>& src)
	{
		StatisticVal<T>::init(src);
		limits1         = src.limits1;
		limits2         = src.limits2;
		histLimitRates1 = src.histLimitRates1;
		histLimitRates2 = src.histLimitRates2;
		quasiMedian     = src.quasiMedian;
	}


	//2013-06-17 JBU: Better gray value mapping in 3DViews
	//Do not use obsolete function getMaxGreyLimit in PointSet, instead extract limits directly from reflection statistics
	///\brief getMaxGreyLimit is used to define the grey mapping with a given reflection statistic and autocontrastflag
	///\param autoContrastFlg the autocontrastflag
	///\return the refl value that corresponds to the maximum grey value
	int getMaxGreyLimit(const int autoContrastFlg) const
	{
		T result = limits1[1];
		switch(autoContrastFlg)
		{
		case 3:
			result = limits2[1];
		}
		return static_cast<int>(floor(result));
	}

	//2013-06-17 JBU: Better gray value mapping in 3DViews
	//Do not use obsolete function getMinGreyLimit in PointSet, instead extract limits directly from reflection statistics
	///\brief getMinGreyLimit is used to define the grey mapping with a given reflection statistic and autocontrastflag
	///\param autoContrastFlg the autocontrastflag
	///\return the refl value that corresponds to the minimum grey value
	int getMinGreyLimit(const int autoContrastFlg) const
	{
		T result = limits1[0];
		switch(autoContrastFlg)
		{
		case 3:
			result = limits2[0];
		}
		return static_cast<int>(floor(result));
	}
};

/// makes no sense for integral types.
/// Cause error by undefined template specialization.
template <>
class StatisticValLimits<int>;

// -----------------------------------------------------------------------
// S t a t i s t i c V a l M e d i a n S e t
// -----------------------------------------------------------------------
// Template class to store statistic values and their median
template <class T>
class StatisticValMedianSet : public StatisticValMedian<T>
{
public:
	int numPoints; ///< number of points set
	/// ContentsList: Contains the single elements
	typedef std::vector<T> ContentList;
	ContentList contentList; ///< Store elements used in this vector

	StatisticValMedianSet(T mean = 0,
	                      T min  = 0,
	                      T max  = 0,
	                      T dev  = 0)
		: StatisticValMedian<T>(mean, min, max, dev)
	{
		numPoints = 0;
	}

	StatisticValMedianSet(const StatisticValMedianSet<T>& src)
	{
		init(src);
	}

	StatisticValMedianSet(const StatisticValMedian<T>& src)
	{
		numPoints = 0;
		StatisticValMedian<T>::init(src);
	}

	~StatisticValMedianSet() throw()
	{
	}

	StatisticValMedianSet<T>& operator =(const StatisticValMedianSet<T>& src)
	{
		init(src);
		return *this;
	}

	void init(const StatisticValMedianSet<T>& src)
	{
		StatisticValMedian<T>::init(src);
		numPoints   = src.numPoints;
		contentList = src.contentList;
	}

	/// reset: Prepare to start adding values
	void reset()
	{
		numPoints     = 0;
		this->meanVal = this->minVal = this->maxVal = this->deviation = 0;
		contentList.clear();
	}

	/// add: Adds a point, keeps track of statistic parameter
	void add(const T& in)
	{
		this->meanVal += in;
		this->deviation += in * in;
		contentList.push_back(in);
		if(!numPoints)
			this->minVal = this->maxVal = in;
		else
		{
			if(in < this->minVal)
				this->minVal = in;
			else if(in > this->maxVal)
				this->maxVal = in;
		}
		numPoints++;
	}

	void add(const StatisticValMedianSet<T>& src)
	{
		for(typename ContentList::const_iterator iter = src.contentList.begin(); iter != src.contentList.end(); ++iter)
		{
			this->add(*iter);
		}
	}

	/// finish: Calculate parameter according to the number of input
	void finish()
	{
		if(!numPoints)
			return; // Nothing to do, no input
		this->meanVal /= numPoints;
		this->deviation = (T)sqrt(this->deviation / numPoints - this->meanVal * this->meanVal);
		// Calculate median of all values
		std::sort(contentList.begin(), contentList.end());
		this->medianVal = contentList[numPoints / 2];
	}
};

/// makes no sense for integral types.
/// Cause error by undefined template specialization.
template <>
class StatisticValMedianSet<int>;


// -----------------------------------------------------------------------
// S t a t i s t i c V a l L i m i t s S e t
// -----------------------------------------------------------------------
// Template class to store statistic values + limits defined by set class
template <class T>
class StatisticValLimitsSet : public StatisticValLimits<T>
{
public:
	int numPoints; ///< number of points set
	/// HistogramTable: Contains element to number of hits map
	typedef typename std::map<const T, int> HistogramTable;

	/// ContentsList: Contains elements in sorted order
	/// MOS notes: std::set<const T> doesn't work for unknown reasons
	HistogramTable histogram; ///< histogram to store number of inputs for final calculation

public:
	/// @name Constructors, Destructor
	//@{
	StatisticValLimitsSet(T mean        = 0,
	                      T min         = 0,
	                      T max         = 0,
	                      T dev         = 0,
	                      T quasiMedian = 0)
		: StatisticValLimits<T>(mean, min, max, dev, quasiMedian)
	{
		numPoints             = 0;
		this->histLimitRates1 = cv::Vec2d(0.01, 0.01);
		this->histLimitRates2 = cv::Vec2d(0.05, 0.05);
	}

	StatisticValLimitsSet(const StatisticValLimitsSet<T>& src)
	{
		init(src);
	}

	StatisticValLimitsSet(const StatisticValLimits<T>& src)
	{
		numPoints = 0;
		StatisticValLimits<T>::init(src);
	}

	~StatisticValLimitsSet() throw()
	{
	}

	//@}

	StatisticValLimitsSet<T>& operator =(const StatisticValLimitsSet<T>& src)
	{
		init(src);
		return *this;
	}

	void init(const StatisticValLimitsSet<T>& src)
	{
		StatisticValLimits<T>::init(src);
		numPoints = src.numPoints;
		histogram = src.histogram;
	}

	/// reset: Prepare to start adding values
	void reset()
	{
		numPoints     = 0;
		this->meanVal = this->minVal  = this->maxVal = this->deviation = 0;
		this->limits1 = this->limits2 = cv::Vec2d<T>(0, 0);
		histogram.clear();
	}

	/// add: Adds a point, keeps track of statistic parameter
	void add(const T& in)
	{
		this->meanVal += in;
		this->deviation += in * in;

		histogram[in]++;
		if(!numPoints)
			this->minVal = this->maxVal = in;
		else
		{
			if(in < this->minVal)
				this->minVal = in;
			else if(in > this->maxVal)
				this->maxVal = in;
		}
		numPoints++;
	}

	/// add: Adds a point multiple times, keeps track of statistic parameter
	void add(const T& in,
	         int numHits)
	{
		this->meanVal += in * numHits;
		this->deviation += in * in * numHits * numHits;
		typename HistogramTable::iterator iter = histogram.find(in);
		if(iter == histogram.end())
		{
			// Insert new element
			typename HistogramTable::value_type keyVal(in, numHits);
			std::pair<typename HistogramTable::iterator, bool> result = histogram.insert(keyVal);
			dynamic_assert(result.second); // token must not exist
		}
		else
		{
			// Increase hit number
			iter->second += numHits;
		}
		if(!numPoints)
			this->minVal = this->maxVal = in;
		else
		{
			if(in < this->minVal)
				this->minVal = in;
			else if(in > this->maxVal)
				this->maxVal = in;
		}
		numPoints += numHits;
	}

	/// add: Adds another histogram. This way an overall histogram can be created
	/// from partial ones.
	void add(const StatisticValLimitsSet<T>& histogramSrc)
	{
		typename StatisticValLimitsSet<T>::HistogramTable::const_iterator iterHist;
		for(iterHist = histogramSrc.histogram.begin(); iterHist != histogramSrc.histogram.end(); ++iterHist)
		{
			const T& value     = iterHist->first;
			const int& numHits = iterHist->second;
			add(value, numHits);
		}
	}

	/// finish: Calculate parameter according to the number of input
	void finish()
	{
		if(!numPoints)
			return; // Nothing to do, no input
		this->meanVal /= numPoints;
		this->deviation = (T)sqrt(this->deviation / numPoints - this->meanVal * this->meanVal);

		// Use histogram in order to define limits
		// Minimum limit
		{
			typename HistogramTable::const_iterator iterHist;
			int counted = 0;
			bool done1  = false, done2 = false;
			for(iterHist = histogram.begin(); iterHist != histogram.end(); ++iterHist)
			{
				const T& value = iterHist->first;
				counted += iterHist->second;

				double percent = (double)counted / numPoints;
				if(!done1 && percent > this->histLimitRates1[0])
				{
					this->limits1[0] = value;
					done1            = true;
				}
				if(!done2 && percent > this->histLimitRates2[0])
				{
					this->limits2[0] = value;
					done2            = true;
				}
				if(done1 && done2)
					break;
			}
		}
		// Maximum limit
		{
			// MOS notes: const_reverse_iterator did not compile for unknown reasons
			typename HistogramTable::reverse_iterator iterHist;
			int counted = 0;
			bool done1  = false, done2 = false;
			for(iterHist = histogram.rbegin(); iterHist != histogram.rend(); ++iterHist)
			{
				const T& value = iterHist->first;

				counted += iterHist->second;
				double percent = (double)counted / numPoints;
				if(!done1 && percent > this->histLimitRates1[1])
				{
					this->limits1[1] = value;
					done1            = true;
				}
				if(!done2 && percent > this->histLimitRates2[1])
				{
					this->limits2[1] = value;
					done2            = true;
				}
				if(done1 && done2)
					break;
			}
		}
		getCenterValue();
	}

	/// getValueMatchingToSum: Searches for the data value that corresponds to a
	/// given integration value provided in percent.
	const T getValueMatchingToSum(double sumPercentLimit)
	{
		if(!numPoints)
			return T(0); // Nothing to do, no input
		typename HistogramTable::const_iterator iterHist;
		int counted = 0;
		for(iterHist = histogram.begin(); iterHist != histogram.end(); ++iterHist)
		{
			const T& value     = iterHist->first;
			const int& numHits = iterHist->second;
			counted += numHits;
			double percent = (double)counted / numPoints;
			if(percent >= sumPercentLimit)
			{
				return value;
			}
		}
		assert(0); // Unexpected not to have found the point containing half of the points below
		return T(0);
	}

	/// getCenterValue: Searches for the data value that corresponds to get half
	/// of all points below that value. For a gaussian distribution this is equivalent
	/// to the maximum.
	const T getCenterValue()
	{
		this->quasiMedian = getValueMatchingToSum(0.5);
		return this->quasiMedian;
	}
};

/// makes no sense for integral types.
/// Cause error by undefined template specialization.
template <>
class StatisticValLimitsSet<int>;
