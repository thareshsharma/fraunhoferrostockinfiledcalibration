#pragma once

#include "helper.h"
#include "kinematics_helpers.h"

using namespace DMVS::CameraCalibration;

class ProjectorCalibrationHelper
{
public:
	static void RemovePointsOutsideContour(std::vector<cv::Vec2f>& pts,
	                                       std::vector<cv::Point>& contour,
	                                       DotInfos& dots_infos);

	static bool GetBrightLaserDotIndices(const DotInfos& dots_infos,
	                            const std::vector<size_t>& dots,
	                            std::vector<size_t>& bright_dots_indices);

	static void AveragePositions(std::vector<cv::Point3d>& positions,
	                             cv::Vec3d& start_position,
	                             cv::Vec3d& end_position);
};
