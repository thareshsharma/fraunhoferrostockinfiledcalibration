#pragma once

class IOMessages
{
public:
	static int FilterWarning(const int message)
	{
		auto result = message;

		switch(message)
		{
		case IO_CANCELED:
		case IO_SKIPPED:
		case IO_READWARNING:
		case IO_READUNIQUENAME:
		case IO_FIT_AMBIVALENT:
		case IO_FIT_VARYNODATA:
		case IO_FIT_VARYNOSUCCESS:
		case IO_NOPOWERUSER:
		case IO_SCANDATAMISSING:
		case IO_FIT_MISSINGSCAN:
		case IO_FIT_PARTIAL:
		case IO_TURNOFF_SCANNER:
		case IO_NOANGLECALIB:
		case IO_FIXED_HORANGLES:
		case IO_NO_RESULT:
		case IO_INTERSECTION:
		case IO_NO_OPENGL_HARDWARE:
		case IO_LOCAL_WORKSPACE:
		case IO_WARN_SOME_OBJECT_NOT_LOADED:
		case IO_WARN_INCORRECT_LOADED_OBJECTS:
		case IO_SFM_VIDEO_END:
		case IO_SFM_USER_REQUESTED:
		case IO_SFM_AUTO_RESTART:
		case IO_SFM_SENSOR_RESET:
		case IO_WARN_MATERIAL_FILE_MISSING:
		case IO_WARN_TEXTURE_FILE_MISSING:
			result = IO_OK;
			break;
		default:
			break;
		}

		return result;
	}

	/// addMsg: Create a combined message from existing and new one
	static int AddMessage(const int message,
	                      const int to_add)
	{
		if(message == IO_OK)
			return to_add;

		// when we have an error keep it since this is most likely the cause of toAdd
		if(IOMessages::FilterWarning(message) != IO_OK)
			return message;

		// msg is a warning and errors overwrite that warning
		if(IOMessages::FilterWarning(to_add) != IO_OK)
			return to_add;

		// msg and toAdd are warnings, take the one that is not a skip/cancel one
		if((message == IO_CANCELED || message == IO_SKIPPED) && to_add != IO_OK)
			return to_add;

		return message;
	}

	enum IOResult
	{
		// !!! IMPORTANT !!!
		// The sequence of messages determines their numerical ID, which
		// in turn is used directly (i.e., not via the enumeration member name)
		// by automation clients. Thus, the order of message codes must not
		// change. New messages must be added either directly before IO_LAST,
		// or at the end of any category following IO_LAST below.

		// To help with maintainability, new messages can also be placed
		// into categories. These are set up such that messages can be added
		// at the end of their category without violating the constraint of
		// unchanging message numbers. However, they still must be added as
		// the last message in their category.

		/// Miscellaneous messages
		IO_OK,
		// Everything worked
		IO_FALSE,
		// Something didn't work, unknown reason
		IO_UNDEFINED,
		IO_UNEXPECTED,
		// internal error
		IO_UNKNOWN,
		// internal error, found unknown error message
		IO_NOPOWERUSER,
		// missing privilege to register controls
		IO_INSUFFICIENTPRIV,
		// insufficient privilege to run the application
		IO_INTERNALINIT,
		// internal registration problem. Please contact support team.

		/// Read / Write file messages
		IO_OPENSTGSTORAGE,
		// file wasn't a structured storage file
		IO_OPENSTREAM,
		// couldn't open a stream
		IO_OPENZIP,
		// couldn't open a zip archive
		IO_NOMODEL,
		// include error messages from ZipExtIStream
		IO_LOCAL_WORKSPACE,
		// warning: either could not load the local workspace or the workspace is not in local workspace format
		IO_NOSCAN,
		// Scan file format error
		IO_OPEN_READ,
		// unable to open a file for reading.
		IO_OPEN_WRITE,
		// unable to open a writable file
		IO_OPEN_SCAN,
		// unable to open a scan file
		IO_OPEN_SCANDATA,
		// unable to open scan data
		IO_READWRITE,
		// problem reading or writing a stream
		IO_RENAME_STREAM,
		// failed to rename temporary stream data to original data.
		IO_VERSION,
		// version too new
		IO_SCANVERSION,
		// scan version too new
		IO_UNKNOWNKEY,
		// key in model file was unknown
		IO_CANCELED,
		// operation was canceled by the user
		IO_SKIPPED,
		// operation was skipped by the user
		IO_FAILED_CREATE_DIRECTORY,
		// failed to create a directory
		IO_OUTOFMEMORY,
		// failed to get memory
		IO_DATA_MISSING,
		// essential data needed to perform the action is missing
		IO_ADDSCAN,
		// failed to add scan because there is already a scan of this name
		IO_NOT_IMPLEMENTED,
		// requested feature is not implemented
		IO_READWARNING,
		// non fatal read problems
		IO_READUNIQUENAME,
		// detected duplicate names
		IO_UNKNOWN_FORMAT,
		// File format isn't supported
		IO_UNKNOWNLOADFORMAT,
		// It isn't supported to extract a model from that file format
		IO_SCANDATAMISSING,
		// Scan couldn't be read in completely.
		IO_SIZEMISMATCH,
		// Given size and written data didn't match while saving scan data "on the fly".
		IO_SCANLINE_MISMATCH,
		// Tried to mix up ScanLine's of different size to store one scan.
		IO_OPENDIR,
		// Failed to open given directory
		IO_WARN_INCONSISTENT,
		// Inconsistent data encountered
		IO_WARN_NOT_SAVED,
		// Data needs to be saved
		IO_ENCODE_BITMAP,
		// Failed to encode bitmap
		IO_DECODE_BITMAP,
		// Failed to decode bitmap
		IO_NOANGLECALIB,
		// Scan doesn't contain horizontal angle calibration
		IO_FIXED_HORANGLES,
		// Fixed horizontal angle values
		IO_WARN_INCONSISTENT_ENCODER,
		// Inconsistent encoder data.

		// Note: add new messages at the end!

		/// Name setting messages
		IO_INVALIDNAME,
		// requested name isn't a valid name
		IO_RENAMESCANS,
		// it's forbidden to rename scan files
		IO_SCANNAMEEXISTS,
		// rename failed because of a conflict with existing scan file

		/// Object Fit messages
		IO_FIT_NOTENOUGHPOINTS,
		// fit failed because of missing points
		IO_FIT_FAILED,
		// fit algorithm failed for unknown reasons.
		IO_FIT_AMBIVALENT,
		// fit operation not good enough to be sure.
		IO_FIT_VARYNODATA,
		// fit variation wasn't performed because of missing variation data
		IO_FIT_VARYNOSUCCESS,
		// fit variation method couldn't improve the result
		IO_FIT_SMALLRADIUS,
		// fit failed, because radius seemed to be too small.
		IO_FIT_MISSINGSCAN,
		// overall fit failed, because one or several scans couldn't be placed according to others.
		IO_FIT_NONMATCHINGOBJECTS,
		// fit failed because of nonmatching objects
		IO_FIT_NOFIT,
		// fit algorithm failed because of invalid conditions (e.g. no intersection line-plane)
		IO_FIT_PARTIAL,
		// fit wasn't completed because of missing objects.
		IO_FIT_SCANDATA,
		// fit failed because underlying scan wasn't loaded.
		IO_FIT_SCANMIX,
		// fit failed because it's not allowed to add an object pointing to Scan A to Scan B
		IO_FIT_PLANARREGIONSMALL,
		// fit/expansion failed because the planar surrounding of the startpoint was too small

		// Note: add new messages at the end!

		/// Drag/Drop, Copy/Paste/Delete message
		IO_MOVE_REJECTED,
		// Move operation failed, because object or one of it's children is bound to current location.
		IO_COPY_REJECTED,
		// Copy operation failed, because object is bound to current location.
		IO_COPY_PARTIAL,
		// Copy operation wasn't complete, because one of object's children is bound to current location.
		IO_INVALID_LOCATION,
		// Object couldn't be placed to new location. New location refused to adopt object.
		IO_FIND_DESTOBJ,
		// Couldn't find destination object
		IO_REF_EXISTS,
		// A corresponding reference object already exists
		IO_DELETE_ROOT,
		// Don't allow to delete the root

		/// Connection messages
		IO_UNCONNECTED,
		// Connection not yet established
		IO_MISSINGCONNECTION,
		// Failed to send a request because connection was missing
		IO_CONNECTED,
		// Connection was build up successfully
		IO_LISTEN,
		// A valid listen connection was build up
		IO_INVALID_SOCKET,
		// Retrieved invalid socket as new socket descriptor
		IO_DNS_FAILED,
		// Domain Name Server couldn't resolve host name
		IO_BIND_FAILED,
		// Failed to bind to a given listen port
		IO_SETSOCKOPT,
		// Failed to set socket options
		IO_GETSOCKOPT,
		// Failed to get socket options
		IO_FAILED_TO_CONNECT,
		// Failed to connect to given host/port
		IO_FAILED_TO_LISTEN,
		// Failed to listen to given port
		IO_FAILED_TO_ACCEPT,
		// Failed to accept a new connection on given listen socket
		IO_ATTACH_SOCKET,
		// Failed to attach socket to a c++ stream
		IO_LOSTCONNECTION,
		// Lost connection channel.
		IO_SCANSTILLACTIVE,
		// Couldn't execute because of an active scan operation.
		IO_SCANCANCELED,
		// Scan operation was canceled by the operator
		IO_SCANOP,
		// Scanner Operation Failure
		IO_SCANPARAMADJUSTED,
		// Scan Ctrler interfered to adjust parameter to hardware.
		IO_COMMAND_CONSISTENCY,
		// Command consistency problem:
		IO_TURNOFF_SCANNER,
		// It's now safe to turn of the scanner
		IO_VERSION_MISMATCH,
		// Version mismatch error
		IO_READSCANRAWDATA,
		// Problem reading scan raw data
		// Note: add new messages at the end!

		/// Miscellaneous
		IO_POINTMISPLACEMENT,
		// Failed to pull points towards scanner, because some points would be mirrored to the back of the scanner.
		IO_POINTOUTOF2DAREA,
		// The calculated point is not part of the 2d visual area.
		IO_COMPLETE,
		// Input was completed by last call
		IO_ALREADY_EXISTS,
		// Add of input was rejected, because it already existed.
		IO_NOTYETRESOLVED,
		// Input couldn't yet been resolved.
		IO_INVALIDOBJECT,
		// Input object didn't match the requirements.
		IO_NOPOINTSELECTION,
		// Didn't find a valid selection of points.
		IO_TOOMANYOUTLIERS,
		// Algorithm found too many outliers.
		IO_OUTOFBOUNDARY,
		// Method was asked to access out of boundary data

		// 3d
		IO_RENDERING,
		// Failed to run operation, because rendering still is in process
		IO_VIEWHANDLE,
		// Failed to get/release window handles
		IO_INIT_VIEW,
		// Failed to initialize the OpenGL view
		IO_PRESERVED_UPVECT,
		// Operation denied because of a change of the Up std::vector
		IO_VRML_HEADER,
		// VRML Header Match
		IO_LAYER_HEADER,
		// Layer Header Match
		IO_VRML_IGNORE2PROTO,
		// VRML Ignore 2. prototype statement
		IO_VRML_PROTOWITHOUTNODE,
		// VRML Found prototype without a node
		IO_VRML_NOATTRINTERF,
		// VRML Couldn't extract VRML field interface to field
		IO_VRML_PROTOMISSING,
		// VRML Couldn't create node because prototype was missing
		IO_VRML_NONAMEDNODE,
		// VRML Couldn't find named node
		IO_VRML_PARSEERROR,
		// VRML Parse Error
		IO_VRML_EXTRACTOBJ,
		// VRML Couldn't extract any objects
		IO_VRML_TRANSLATIONERROR,
		// VRML couldn't be translated completely

		IO_WARN_SAVE_DISABLED,
		// Save is currently disabled

		IO_UNSPECIFIC,
		// Message without text --> use Info to display given message
		IO_CREATEPROC,
		// Failed to create a process
		IO_SCANMISSING,
		// Failed to access scan data
		IO_SCANNER_BUSY,
		// Failed to run scanner task because scanner is busy.
		IO_INVALID_IP,
		// Invalid IP Address '%s'
		// Please contact your system administrator.
		// (IP Address must not contain a value of 255)
		IO_INVALID_NUMBER,
		// Text entered is not a valid number
		IO_INVALID_BORDER,
		// Current selection doesn't yield a valid polygon to generate a PlaneEx object
		IO_INVALID_HOLES,
		// Current selection contains polygons which are to represent holes within a PlaneEx object but those polygons are invalid

		IO_NO_RESULT,
		// Algorithm couldn't extract a result (warning)
		IO_TIMEOUT,
		// Algorithm timed out to extract a result
		IO_INTERSECTION,
		// Algorithm found intersection to other object

		IO_NONUNIQUENAME,
		// Name is not unique
		IO_INVALID_RENAME,
		// Invalid name when renaming
		IO_TARGET_PLACEMENT_FAILED,
		// place scans with target based method failed
		IO_PLACE_BAD_DISTRIBUTION,
		// Place scan failed for some scans due to bad distribution of targets.

		// License state
		IO_LIC_WILL_EXPIRE,
		// License will expire soon
		IO_LIC_EXPIRED,
		// License expired
		IO_LIC_TIME_MANIP,
		// System clock cannot be trusted
		IO_LIC_NO_MAC,
		// Cannot determine system id
		IO_LIC_NO_MATCH,
		// No license found

		// Connection
		IO_BROKE_CONNECTION,
		// Connection was broken by other client
		IO_SCANPARAMMERGED,
		// Scan Ctrler merged new set of parameter to hardware.

		// Prepaid
		IO_PREPAID_ACCEPTED,
		// Prepaid key was accepted
		IO_PREPAID_ALARM,
		// Prepaid Alarm was raised
		IO_PREPAID_MISMATCH,
		// Prepaid Combination not allowed
		IO_PREPAID_EXCEEDED,
		// Prepaid Exceeded exception
		IO_PREPAID_UNKNOWN_CMD,
		// Unknown Prepaid command transmitted
		IO_PREPAID_WRONG_ID,
		// Prepaid Command doesn't match the scanner
		IO_PREPAID_TAN_USED,
		// Prepaid TAN already used
		IO_MODULE_NOTAVAILABLE,
		// Module is not available
		IO_MODULE_SWITCHEDOFF,
		// Module is switched off
		IO_MODULE_PROCESSING,
		// Module is busy
		IO_MODULE_VERSIONMISMATCH,
		// Module Parameter data mismatch
		IO_MODULE_READWRITE,
		// Module Parameter read/write error
		IO_MODULE_ERROR,
		// Module unspecific error

		IO_INCOMPATIBLE_DATABASE,
		// Scan is not compatible with the current database

		// Photo
		IO_PHOTO_NOTENOUGHPOINTS,
		// Not enough point correspondences for placing a photo.
		IO_PHOTO_NODEPTHSOURCE,
		// The depth source object (Plane) was not set.

		IO_FIT_SCANTYPE,
		// The operation can not be performed on scans of this type.

		IO_POINTLOADING_LOCKNOTGRANTED,
		// The point data could not be loaded since acquiring the write lock failed
		IO_POINTLOADING_UNDERMANUALCONTROL,
		// The point data could not be loaded since the node is under manual load control

		IO_DIGITAL_CAMERA_ERROR,
		// Error when accessing the digital camera of the scanner
		IO_DIGITAL_CAMERA_CONFIGURATION,
		// Configuration error with digital camera (e.g. auto focus)
		IO_DIGITAL_CAMERA_BATTERY,
		// Low battery in digital camera
		IO_DIGITAL_CAMERA_WARNING,
		// Warning related to the digital camera of the scanner
		IO_SCAN_SIZE_EXCEEDS_FREE_SPACE_SCANNER,
		// Warning scan file size will exceed free space on scanner hard drive.
		IO_SCAN_SIZE_EXCEEDS_FREE_SPACE_REMOTE,
		// Warning scan file size will exceed free space on remote pc hard drive.

		// 3d: OpenGL
		IO_NO_OPENGL,
		// Not even software OpenGL available.
		IO_NO_OPENGL_HARDWARE,
		// Warning that OpenGL is emulated in software.

		// Note: add new messages at the end!
		IO_INVALID_IP_IN_USE,
		// The IP Address '%s' is already used by adapter '%s'.
		IO_UNKNOWNOBJECT,
		// An unknown object was encountered.
		IO_NO_CALIB,
		// No Calibration found in a scan.
		IO_NO_SCANPLUGIN_CONNECTION,
		// No Connection to ScanPlugin found.
		IO_FAILED_SEND_SCANNER_PARAM,
		// Failed to send scanner parameter to scanner.

		// Note: add new messages at the end!
		IO_WRONG_PARAM_NO_ACTION,
		// Warning: Wrong parameters, no action taken
		IO_FAILED_OPENING_HYPERLINK,
		// Failed to open hyperlink.
		IO_FAILED_OPENING_FILE,
		// Failed to open file.

		IO_FILE_NOT_FOUND,
		// File to not found.
		IO_FAILED_COPY_ATTACHMENT,
		// Failed to copy attachment.
		IO_FOLDER_CONTAINS_NO_WORKSPACE,
		// Selected folder contains no FARO workspace.
		IO_ATTACHMENT_CLEANUP_RESULT,
		// Result output for cleanup of attachments.
		IO_NO_UNUSED_ATTACHMENTS,
		// During attachment cleanup file to clean up was found.

		// JobObject-related messages
		// Note: add new messages at the end!
		IO_JOB_INPUT_MISSING,
		// A JobObject is missing a mandatory input
		IO_JOB_INPUT_INVALID,
		// An input of a JobObject is present, but it has the wrong type or is otherwise unusable.

		// Completely miscellaneous messages, because of:
		// Note: add new messages at the end!
		IO_SCAN_NOT_FOUND,
		IO_READONLY,
		// Attempt to modify a read-only resource (file, object,...)
		IO_OPENSTORAGE,
		// Failed to open or to create a sub-storage in a structured storage file
		IO_REMOVE_ELEMENT,
		// Failed to remove a structured storage element
		IO_ELEMENT_NOT_FOUND,
		// A structured storage element (storage or stream) was not found

		IO_MODULE_UNDERVOLTAGE,
		// Module undervoltage (low battery)
		IO_SCAN_DATA_LOCKING_FAILED,
		// Locking of scan data failed (For FARO Open).
		IO_SD_CARD_NOT_INSERTED,
		// SD card is not inserted (For V6 scanner)

		IO_PICTURES_MISSING,
		// Colorize failed as Pictures are missing.

		IO_INVALID_V6_SCANPROJECT,
		// Operation on an invalid V6 Scanproject
		IO_INVALID_V6_SCANPROJECT_TRANSFER,
		// Operation on an invalid V6 Scanproject during Transfer Phase
		IO_INVALID_SD_CARD,
		// Operation on an invalid SD Card
		IO_CANNOT_DELETE_DIRECTORY,
		// Impossible to delete a directory
		IO_EXPORT_IMAGE_FAILED,
		// Exporting of image has failed
		IO_NO_FLASH_PLAYER,
		// No flash player or version too low.
		IO_VIDEO_CREATION_FAIL,
		// Video stream could not be initialized correctly
		IO_VIDEO_CREATE_DC_FAILED,
		// Creating device context for video stream recording failed
		IO_VIDEO_NO_MEMORY,
		// Heap creation of video buffer failed - no memory?
		IO_VIDEO_STREAM_CREATE_FAIL,
		// creation of video stream failed.
		IO_VIDEO_CODEC_SELECT_ERROR,
		// Error during video encoder selection/setup
		IO_VIDEO_COMPRESSION_ERROR,
		// Error during creation of compressed stream. Probably due to encoder constraints (e.g. incompatible resolution)
		IO_VIDEO_STREAM_FORMAT_ERROR,
		// Video stream format could not be set. Probably due to incompatible settings.
		IO_TRANSFER_PROJECT_LOCATIONS_UNSPECIFIED,
		// SD-Card Transfer with unspecidied Project Locations (does not know where to transfer to)
		IO_NO_CHECKERBOARD,
		// No checkerboard could be found by object marker.
		IO_READ_TOO_MANY_DATA,
		// There were more data than expected.
		IO_INCLINOMETER_ERROR,
		// Measurement of inclinometer data failed.
		IO_FW_UPDATE_FAILED,
		// Firmware update failed
		IO_GPS_INCONSISTENT_POSITIONS,
		// Some GPS positions of objects are too far away from reference GPS position.
		IO_GPS_REFERENCE_OPTIMIZATION_INCONSISTENT,
		// Some GPS positions of objects are still too far away from reference GPS position after optimization.
		IO_INCLINOMETER_GREATER5DEG,
		// The angle is greater 5� while scanning
		IO_INCLINOMETER_MOVED_DURING_SCAN,
		// The scanner was moved during the scan
		IO_WARN_SOME_OBJECT_NOT_LOADED,
		// Warning, that some objects couldn't be loaded.
		IO_NO_SPHERE,
		// No sphere could be found by object marker.
		IO_NO_CIRCULAR_FLAT,
		// No circular flat target could be found by object marker.
		IO_NO_PLANE,
		// No Plane could be found by object marker.
		IO_NO_SLAB,
		// No Slab could be found by object marker.
		IO_NO_POINT,
		// No Point could be found by object marker.
		IO_SCANFIT_PLACE_SCAN_RESTRICED,
		// Placing scan was refused by restriction level of application.
		IO_WARN_INCORRECT_LOADED_OBJECTS,
		// Some objects couldn't be correctly loaded (load of FallbackObject instead).
		IO_EXPORT_SCAN_RESTRICTED,
		// Scan export refused by restriction level.
		IO_PREPROCESS_ERROR_SEE_LOG,
		// Error occurred during preprocess, open log file for more detailed message.
		IO_CREATE_SCANNER_BASED_OBJS_NOTHING_TO_DO,
		// Nothing to do at creating scanner based objects.
		IO_POINT_CLOUD_CREATION_FAILED,
		// Point Cloud Creation Failed
		IO_INSUFFICIENT_DISK_SPACE,
		// Not enough space on a drive or network location
		IO_COLORIZE_NO_REFERENCE_PICTURE,
		// No reference picture existing for V6 colorizing scan.

		//HDR related Messages
		IO_HDR_SIZE_MISMATCH,
		// The pictures chosen to colorize the scan in HDR do not match in Size
		IO_HDR_INVALID_EXPOSURE,
		// At least one of the pictures chosen to colorize the scan in HDR does not have a exposure value
		IO_HDR_NOT_ENOUGH_PICTURES,
		// There are not enough LDR pictures to create a HDR picture (at least 3 Poctures are necessarry)

		// KDTree (OpenCV wrapper)
		IO_BUILD_TREE_ERROR,
		// Tree could not build up correctly
		IO_NOT_INITIALIZED,
		// Tree has not been initialized yet
		IO_NO_POINTS,
		// No points has been given
		IO_INVALID_POINT,
		// Given point does not match the expected one

		IO_FAILED_CLIPPING_SCANAREA,
		// Error while clipping the scandata to the requested horizontal scan area
		IO_CORRESPONDENCE_INCONSISTENCY,
		// There was a inconsistency in correspondences.
		IO_TARGET_PLACEMENT_FAILED_AUTO_CLUSTER,
		// The same error as IO_TARGET_PLACEMENT_FAILED but with the hint that auto clusters got created
		IO_PROJ_PLACEMENT_FAILED,
		// place scans with projection based method failed
		IO_PROJ_PLACEMENT_FAILED_AUTO_CLUSTER,
		//place scans with projection based method failed. Auto clustering was enabled
		IO_C2C_PLACEMENT_FAILED,
		//place scans with cloud to cloud registration failed
		IO_REFERENCE_SCAN_PLACEMENT_FAILED,
		//tried to place the reference scan. This should be only possible if references are given
		IO_SCAN_PLACEMENT_AGAINST_REF,
		//place scans against references can only performed for target based registrations
		IO_SCAN_DOWNLOAD_FAILED,
		// WebShare 1: The download of the scan failed.

		// related to the optimization of the reflection based distance compensation.
		IO_COMP_REFL_FAILED,
		// Compensation failed
		IO_COMP_REFL_TOO_FEW_SAMPLES,
		// too few samples for successful compensation
		IO_COMP_REFL_POOR_DATA_QUALITY,
		// data quality is insufficient for successful compensation
		IO_COMP_REFL_SLOPE_TOO_STEEP,
		// the compensation looks suspicious due to strong jumps between adjacent grayvalues
		IO_COMP_REFL_PARAMETER_ACCESS,
		// could not access parameters during compensation
		IO_COMP_REFL_CUTOFF_TOO_LOW,
		// The bright reflection cutoff threshold is too low to be considered reasonable
		IO_COMP_REFL_WRONG_PROFILE,
		// Process only works for normal pattern. Wrong profile was used to record scan
		IO_COMP_REFL_THRESHOLD_ALREADY_SET,
		// If threshold is already set before starting the process, necessary points are already deleted.

		// related to distance compensation data
		IO_DISTANCE_COMP_WRONGDATA,
		// Something is wrong with the distance compensation parameters
		IO_INVALID_SCAN_DATA,
		// Generic error for not existing or invalid scan data.
		IO_GRAB_IMAGE_FAILED,
		// Taking a snapshot of the 3D view, e.g. for the overview map, failed.

		// related to the optimization of the reflection based distance compensation.
		IO_COMP_REFL_MEASUREMENT_TRACK_COMPENSATION_MISSING,
		// Someone tried to execute the bright-reflection-compensation with a scanner that is missing the measurement track compensation.

		IO_OCTREE_EDGE_LENGTH_OVERFLOW,
		// Octree construction failed due to construction space edge length overflow

		IO_COMP_REFL_FROM_SCANS_TEST_FAILED,
		// An error occurred during the test command of the refl based compensation determination from scans.
		IO_READ_RAW_SFM_SCAN_FAILED,
		// Reading in raw SFM scan failed.
		IO_NO_PROJECT_OPENED,
		// No project is opened.
		IO_WARN_REFERENCE_INTENSITY_DEVIATION,
		// The intensity at the reference track deviates from the average.
		IO_SIGN_SCAN_FAILED,
		// Algorithm for signature creation of scan failed.
		IO_SIGN_SCAN_VERIFICATION_FAILED,
		// creation of signature worked, but the verification failed.
		IO_SCAN_SIGNATURE_INVALID,
		// Signature of scan is invalid, scan probably manipulated.

		IO_PROCESSING_ALREADY_RUNNING,
		// an instance of process scans is already running, only one may run at the same time

		IO_WARN_EXPERIMENTAL_SCANNER_SETTINGS,
		// current set of scanner params indicate that they are experimental, e.g. for calibration purposes.

		IO_WRITE_REGISTRY_FAILED,
		// Could not write to registry
		IO_READ_REGISTRY_FAILED,
		// Could not read from registry

		IO_DELETE_OLD_REVISIONS_FAILED,
		// When creating a new base revision not all older revisions could be deleted.

		IO_DATA_SIZE_INCONSISTENCY,
		// Inconsistency between data size in point set and file discovered

		IO_COULD_NOT_CREATE_OPENGL_CONTEXT,
		// Could not create OpenGL 4.1 context.

		IO_NO_MARKER,
		// No Marker could be found by object marker.

		IO_INVALID_SCAN_IN_PROJECT,
		// Scan with inconsistent data structure (missing stream) detected

		IO_WARN_MATERIAL_FILE_MISSING,
		// A material file was not found
		IO_WARN_TEXTURE_FILE_MISSING,
		// A texture file was not found

		IO_COMPASS_ERROR,
		// Error occurred during compass calibration
		IO_LAYOUTPLAN_RESOLUTION_TOO_LARGE,
		// Resolution of Layout Plan is too large

		IO_COLORIZE_SCAN_RES_TOO_HIGH,
		// Scan resolution to high for colorization

		IO_INCOMPATIBLE_MODEL,
		// For incompatible model versions, used to read model readonly


		// Note: add new messages before this last one, or append them to one of the categories below!
		// **********************************************************
		// It's no more necessary to add IO message at the end end, therefore there are now enumerations of
		// LSMessages (their number must never change).
		// For adding a IO code here add LS code in LSMessages.h (at end!) and add connection of numbers in
		// LSMessageConverter.cpp (for conversion).
		// **********************************************************
		IO_LAST,


		// We allow 32 categories (0..31) to later sort legacy messages into,
		// so we start with category 32 here. Each category allows for
		// 1024 (1 << 10) messages.
		_IO_OFFSET_LEGACY = 0,
		_IO_OFFSET_LOCKING = (32 << 10),
		// Category offset for locking-related messages
		_IO_OFFSET_REPOSITORY = (33 << 10),
		// Category offset for repository-related messages
		_IO_OFFSET_APPS = (34 << 10),
		// Category offset for app related messages
		_IO_OFFSET_LICENSING = (35 << 10),
		// Category offset for licensing with Sentinel HASP related messages
		_IO_OFFSET_SCANNER_PARAMETERS = (36 << 10),
		// Category offset for errors with Scanner Parameters and Settings writing and reading
		_IO_OFFSET_JSON = (37 << 10),
		// Category offset for JSON import/export related messages
		_IO_OFFSET_SFMPLUGIN = (38 << 10),
		// Category offset for sfm plugin related messages
		_IO_OFFSET_WSU = (39 << 10),
		// Category offset for WebShare Cloud upload related messages
		_IO_OFFSET_VR = (40 << 10),
		// Category offset for VR related messages
		_IO_OFFSET_MAP = (41 << 10),
		// Category offset for scan map messages

		// Locking-related messages
		IO_LOCK_TIMEOUT = _IO_OFFSET_LOCKING,
		// A timeout occurred while trying to acquire a lock
		IO_LOCK_TIMEOUT_POINTS,
		// A timeout occurred waiting for a Scan Point lock
		IO_LOCK_ALLOCSCENE_UNDER_ALLOCMAIN,
		// Somebody attempted to perform an AllocScene under an AllocMain
		IO_LOCK_GOLBAL_ALLOCSCENE_UNDER_ALLOCSCENE,
		// Somebody attempted to perform an AllocScene under an AllocMain
		IO_LOCK_ALLOCMAINALL_UNDER_ALLOCMAIN,
		// Somebody attempted to perform an AllocMainAll under an AllocMain
		IO_LOCK_WAIT_UNDER_LOCK,
		// Somebody is attempting to wait under a lock

		// Repository-related messages
		IO_REPOSITORY_WRONG_TYPE = _IO_OFFSET_REPOSITORY,
		// A repository was found, but it was not of the expected type
		IO_REPOSITORY_INVALID_SCAN,
		// A scan is not usable with the repository
		IO_REPOSITORY_READ_ONLY,
		// The repository is read-only; int cannot be modified or saved
		IO_REPOSITORY_ALREADY_LOCKED,
		// The repository could not be locked with the requested access rights
		IO_REPOSITORY_NOT_FOUND,
		// No repository could be found
		IO_OPEN_REPOSITORY,
		// The repository could not be loaded
		IO_OPEN_REVISIONS,
		// The repository could not be loaded due to an invalid revision
		IO_REPOSITORY_CREATE_DIR_FAILED,
		// Failed to create a directory within the repository

		// App related messages
		IO_APP_MANIFEST_NOT_FOUND = _IO_OFFSET_APPS,
		IO_APP_MANIFEST_COULD_NOT_BE_LOADED,
		IO_APP_DLL_NOT_FOUND,
		IO_APP_DLL_COULD_NOT_BE_LOADED,
		IO_APP_ALREADY_INSTALLED,
		IO_APP_LICENSE_MANIFEST_COULD_NOT_BE_LOADED,
		IO_APP_LICENSE_MISSING_OR_INVALID,
		IO_APP_LICENSING_INCOMPATIBLE,
		IO_APP_LICENSE_DRIVER_MISSING,
		IO_APP_LICENSE_INSTALL_TRIAL_FAILED,
		IO_APP_DIRECTORY_INACCESSIBLE,

		//licensing related errors
		IO_HASP_LEGACY_START = _IO_OFFSET_LICENSING,
		// Licensing offset
		IO_HASP_LOCAL_COMM_ERR = 35845,
		//communication to local license server failed
		IO_HASP_FEATURE_NOT_FOUND_PLUGIN_DONGLE = 35873,
		//feature not found on key, if existing plug in dongle
		IO_HASP_LEGACY_END = 35876,
		//please add entries after this


		//reading and writing Scanner Parameters and Settings
		IO_PARAMETER_FILE_ERROR = _IO_OFFSET_SCANNER_PARAMETERS,
		//parameter file is corrupt
		IO_FACTORY_PARAMETER_FILE_ERROR,
		//factory parameter file is corrupt
		IO_FACTORY_PARAMETER_FILE_READ,
		//factory parameters were read in
		IO_PARAMETER_INCONSISTENT,
		//parameters in memory are inconsistent
		IO_SETTINGS_FILE_ERROR,
		//settings file is corrupt
		IO_FACTORY_SETTINGS_FILE_ERROR,
		//factory settings file is corrupt
		IO_FACTORY_SETTINGS_FILE_READ,
		//factory settings were read in
		IO_SETTINGS_INCONSISTENT,
		//settings in memory are inconsistent

		// JSON import/export related messages
		IO_JSON_IMPORT_ERROR = _IO_OFFSET_JSON,
		// UNUSED
		IO_JSON_IMPORT_FATAL_ERROR,
		// UNUSED
		IO_JSON_IMPORT_WARNING,
		// UNUSED
		IO_JSON_IMPORT_FILE_ERROR,
		// UNUSED
		IO_JSON_IMPORT_INVALID_ATTRIBUTE,
		// JSON value is invalid
		IO_JSON_IMPORT_INVALID_OBJECT,
		// UNUSED
		IO_JSON_IMPORT_PARENT_NOT_FOUND,
		// UNUSED
		IO_JSON_IMPORT_MAP_NOT_FOUND,
		// UNUSED
		IO_JSON_IMPORT_SCAN_NOT_FOUND,
		// UNUSED
		IO_JSON_IMPORT_OBJECT_NAMING_ERROR,
		// UNUSED
		IO_JSON_EXPORT_ERROR,
		// UNUSED
		IO_JSON_EXPORT_FATAL_ERROR,
		// UNUSED
		IO_JSON_EXPORT_WARNING,
		// UNUSED
		IO_JSON_EXPORT_FILE_ERROR,
		// UNUSED
		IO_JSON_EXPORT_INVALID_OBJECT,
		// failed to export SCENE object
		IO_JSON_IMPORT_INVALID_INPUT,
		// failed to read input string
		IO_JE_UNKNOWN_OBJECT,
		// UnknownObjectException
		IO_JE_FIND_PARENT,
		// FindParentException
		IO_JE_ADD_CHILD,
		// AddChildException
		IO_JE_METHOD_NOT_ALLOWED,
		// MethodNotAllowedException
		IO_JE_STATE_CHANGE,
		// StateChangeException
		IO_JE_CONNECTION,
		// ConnectionException
		IO_JE_AUTHENTICATION,
		// AuthenticationException
		IO_JE_AUTHORIZATION,
		// AuthorizationException
		IO_JE_SERVER_RESPONSE,
		// ServerResponseException
		IO_JE_BAD_REQUEST,
		// BadRequestException
		IO_JE_READ_ATTRIBUTE,
		// ReadAttributeException
		IO_JE_WRITE_OBJECT,
		// WriteObjectException
		IO_JE_RENAME,
		// RenameException
		IO_JE_TOS,
		// BadRequestException

		// SFM related messages
		IO_SFM_NO_SDCARD_DETECTED = _IO_OFFSET_SFMPLUGIN,
		//failed to detect an SD card to save recordings to
		IO_SFM_NO_DEFAULT_PROJECTS_LOCATION,
		//no projects location is set to save recordings to
		IO_SFM_CREATE_PROJECT,
		//failed to create project needed for saving recordings
		IO_SFM_CREATE_BITMAP,
		//failed to create a bitmap
		IO_SFM_OPEN_2DVIEW,
		//failed to open a 2d view
		IO_SFM_VIDEO_END,
		//end of the video file is reached
		IO_SFM_SEQUENCE_BROKEN,
		//sequence replay failed
		IO_SFM_USER_REQUESTED,
		//the user requested to stop recording or playback
		IO_SFM_VIDEO_INPUT_ERROR,
		//the video input file has an error
		IO_SFM_NO_DEVICE,
		//no capturing device was found
		IO_SFM_GRAB_FAILED,
		//grabbing color or depth image from device failed
		IO_SFM_RETRIEVE_FAILED,
		//retrieving color or depth image failed
		IO_SFM_DISK_FULL,
		//no more data can be stored on disk
		IO_SFM_COULD_NOT_ACCESS_LASER_POWER,
		//laser power can't be read or set
		IO_SFM_AUTO_RESTART,
		//automatic restart of scanning
		IO_SFM_SENSOR_RESET,
		//reset of USB and scanning restart
		IO_SFM_SENSOR_MNGCALIB,
		//Sensor calibration management failed
		IO_SFM_OVERTEMPERATURE,
		//Device temperature is to high
		IO_SFM_UNDERTEMPERATURE,
		//Device temperature is to low

		// WebShare Cloud upload related messages
		IO_AUTH_DENIED = _IO_OFFSET_WSU,
		// Authentication: Denied
		IO_AUTH_MALFORMED_DATA,
		// Authentication: Malformed data
		IO_AUTH_INSUFFICIENT_RIGHTS,
		// Authentication: Insufficient rights
		IO_AUTH_CONNECTION_FAILED,
		// Authentication: Connection failed
		IO_AUTH_INVALID_USER_NAME,
		// Authentication: Invalid user name
		IO_AUTH_INVALID_PASSWORD,
		// Authentication: Invalid password
		IO_WSU_EXPORT_CORRUPT,
		// WebShare Cloud upload: Corrupt export data
		IO_WSU_INIT_IMAGE_FAILED,
		// WebShare Cloud upload: Initializing the project image failed
		IO_WSU_INIT_METADATA_FAILED,
		// WebShare Cloud upload: Initializing the project metadata failed
		IO_WSU_INIT_SERVER_FAILED,
		// WebShare Cloud upload: Initializing project on the server failed
		IO_WSU_SERVER_ERROR,
		// WebShare Cloud upload: Received server error
		IO_WSU_RECEIVED_INVALID_DATA,
		// WebShare Cloud upload: Received invalid data from server
		IO_WSU_DUPLICATE_NAME,
		// WebShare Cloud upload: Duplicate project ID
		IO_WSU_NEW_PROJECT,
		// WebShare Cloud upload: State: Is new project
		IO_WSU_INCREMENTAL_UPLOAD,
		// WebShare Cloud upload: State: Is incremental upload
		IO_WSU_RESUME_UPLOAD,
		// WebShare Cloud upload: Use existing project for reupload
		IO_WSU_RESUME_UPLOAD_DISALLOWED,
		// WebShare Cloud upload: Reupload of project is disallowed
		IO_WSU_FINISH_FAILED,
		// WebShare Cloud upload: Finishing project upload failed
		IO_WSU_FILES_FAILED,
		// WebShare Cloud upload: Failed to upload one or more files
		IO_WSU_SKIPPED_AFTER_EXPORT,
		// WebShare Cloud upload during export skipped after export was already successful
		IO_WSU_POINTDATA_UPLOAD_FAILED,
		// WebShare Cloud entity upload failed (CPE upload, Send to WebShare Cloud)
		IO_WSU_POINTDATA_ENCODING_FAILED,
		// Pointcloud encoding (CPE format) failed

		// VR related messages
		IO_VR_STEAM_NOT_FOUND = _IO_OFFSET_VR,
		// Steam not installed, even if Oculus is installed
		IO_VR_GOGGLES_NOT_FOUND,
		// VR goggles not attached or not powered
		IO_VR_MISSING_PROJECT_POINT_CLOUD,
		// No project point cloud found
		IO_VR_MISSING_CLOSED_SURFACES,
		// The project point cloud has no closed surfaces

		// Scan map related messages
		IO_MAP_UNKNOWN_RESOURCE = _IO_OFFSET_MAP,
		// An unknown object was encountered.
		IO_MAP_INVALID_VALUE,
		// essential data needed to perform the action is missing
		IO_MAP_IMPORT_ERROR,
		// Failed to decode bitmap
		IO_MAP_CREATE_FOLDER_FAILED,
		// failed to create a directory
		IO_MAP_UNKNOWN_ERROR,
		// internal error, found unknown error message
	};
};