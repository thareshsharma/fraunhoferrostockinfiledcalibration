#pragma once

#include "io_messages.h"

namespace fs = boost::filesystem;

class ConfigurationContainer;

// definition for portable use of access() (aka _access(), _waccess())
#define F_OK 0x00
#define W_OK 0x02
#define R_OK 0x04

#define RETURN_IF_NOT_OK(CALL)		\
{								\
	const auto callResult = CALL; \
	typedef std::is_same<decltype(callResult), const int> is_const_int; \
	typedef std::is_same<decltype(callResult), const IOMessages::IOResult> is_io_except; \
	static_assert(is_const_int::value || is_io_except::value, "Incompatible type for IO exception number."); \
	IOMessages::IOResult returnCode = static_cast<IOMessages::IOResult>(callResult);		\
	if (returnCode != IOMessages::IO_OK )	\
	{							\
		return returnCode;		\
	}							\
}

/**
 * \brief This class manages files and directories specific to calibration data.
 * The directories can be set with an xml file. Input files are not checked for existence!
 */
class CalibrationFileBase
{
public:
	/**
	 * \brief Initializes an object of type CalibrationFileBase.
	 * \param serial_number The serial number of the device.
	 * \param config_file_path The path to an xml config file.
	 * \return IOResult The error message
	 */
	IOMessages::IOResult Init(const std::string& serial_number,
	                          const fs::path& config_file_path);

	/**
	 * \brief Returns the root path where all data lies
	 * \return
	 */
	std::shared_ptr<const fs::path> GetRootPath() const;

	/**
	 * \brief Returns the root path where all data lies
	 */
	IOMessages::IOResult SetRootPathPermanent(const std::string& new_root_path);

	/**
	 * \brief Returns the root path where all data lies
	 * Returns "root path\Freestyle_SerialNumber"
	 */
	std::shared_ptr<const fs::path> GetRootPathCalibration() const;

	/**
	 * \brief Returns the directory in which all calibration input data lies.
	 */
	std::shared_ptr<const fs::path> GetInputPath() const;

	/**
	 * \brief
	 * \param filename
	 * \return
	 */
	IOMessages::IOResult GetCalibrationParametersYML(std::string& filename) const;

	/**
	 * \brief Template for InHouse plate conversion
	 */
	IOMessages::IOResult GetCalibrationParametersTemplateYML(std::string& filename) const;

	/**
	 * \brief Path for board reference files
	 */
	IOMessages::IOResult GetCalibrationBoardsPath(std::string& boards_file_path) const;

	/**
	 * \brief
	 * \param filename
	 * \param sensor
	 * \return
	 */
	IOMessages::IOResult GetDefaultCameraCalibration(fs::path& filename,
	                                                 const std::string& sensor = "") const;

	IOMessages::IOResult GetCameraCalibrationFolder(std::string& filename) const;

	IOMessages::IOResult GetCameraCalibrationFSVOutputFile(std::string& filename) const;

	IOMessages::IOResult GetCameraCalibrationLinearStagePositionOutputFile(std::string& filename) const;

	IOMessages::IOResult GetCameraCalibrationLogOutputFile(std::string& filename) const;

	IOMessages::IOResult GetCameraCalibrationYMLOutputFile(std::string& filename) const;

	IOMessages::IOResult GetCameraCalibrationSequenceOutputFile(std::string& filename) const;

	IOMessages::IOResult GetCameraCalibrationPDFOutputFile(std::string& filename) const;

	IOMessages::IOResult GetCameraCalibrationReportOutputFile(std::string& filename) const;

	IOMessages::IOResult GetCameraCalibrationImageOutputDirectory(int camera,
	                                                              std::shared_ptr<fs::path>& directory) const;

	IOMessages::IOResult GetCameraCalibrationStatisticsOutputDirectory(std::shared_ptr<fs::path>& directory) const;

	IOMessages::IOResult GetCameraCalibrationSubDirectory(const std::string& sub_directory_name,
	                                                      fs::path& directory) const;

	IOMessages::IOResult GetProjectorCalibrationOutputFolder(std::string& filename) const;

	IOMessages::IOResult GetProjectorCalibrationDoeYML(std::string& filename) const;

	IOMessages::IOResult GetProjectorCalibrationFSVOutputFile(std::string& filename) const;

	IOMessages::IOResult GetProjectorCalibrationLogOutputFile(std::string& filename) const;

	IOMessages::IOResult GetProjectorCalibrationDataJSONFile(std::string& filename) const;

	IOMessages::IOResult GetProjectorCalibrationXMLOutputFile(std::string& filename) const;

	IOMessages::IOResult GetProjectorCalibrationReportOutputFile(std::string& filename) const;


	IOMessages::IOResult GetFQCReportOutput(std::string& filename) const;

	IOMessages::IOResult GetFQCLogOutputFile(std::string& filename) const;

	std::shared_ptr<const fs::path> GetFQCPath() const;

	IOMessages::IOResult GetInfieldCalibrationFSVOutputFile(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationAllLogOutputFile(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationBrightLogOutputFile(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationTransShiftLogOutputFile(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationBrightXML(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationAllXML(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationOriginalXML(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationTransShiftXML(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationProjLogOutputFile(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationProjXML(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationCompleteLogOutputFile(std::string& filename) const;

	IOMessages::IOResult GetInfieldCalibrationReportFile(std::string& filename) const;

	/**
	 * \brief Removes old infield calibration folders. Keep numToKeep newest folders.
	 */
	IOMessages::IOResult RemoveOldInFieldFolders(int num_to_keep) const;

	/**
	 * \brief Default InField calibration board template for FreestyleTool conversion
	 */
	IOMessages::IOResult GetInfieldCalibrationBoardTemplate(std::string& filename) const;

	/**
	 * \brief Counts all folder within the calibration data root folder
	 * which start with "Freestyle_".
	 */
	IOMessages::IOResult GetNumCalibrationFoldersOnLocalDrive(int& folder_num) const;

	/**
	 * \brief Returns the path to where the data is securely saved.
	 */
	//std::shared_ptr<const fs::path> getNetworkStoragePath(bool toQPRFolder = false);

	IOMessages::IOResult CreateFolderIfNotExist(const std::shared_ptr<fs::path>& file) const;

	static IOMessages::IOResult CreateFolderIfNotExist(const std::shared_ptr<const fs::path>& directory);

	~CalibrationFileBase();

	/**
	 * \brief Initialize directory and filename management
	 * \param serial_number
	 * \param options
	 * \return
	 */
	static std::shared_ptr<CalibrationFileBase> GetFileBase(const std::string& serial_number,
	                                                        std::shared_ptr<ConfigurationContainer> options);

protected:
	IOMessages::IOResult ReadConfigFile() const;

	IOMessages::IOResult CreateDefaultConfigFile() const;

	// File containing needed information.
	std::shared_ptr<fs::path> config_file_path_;

	// The device's serial number
	std::string serial_number_;

	// The path under which all data lies
	std::shared_ptr<fs::path> root_path_;

	// The path under which all output data lies (Root path\Freestyle_SN)
	std::shared_ptr<fs::path> root_path_calibration_;

	// The path under which all input data lies
	std::shared_ptr<fs::path> input_path_;

	// The path under which all camera calibration lies
	std::shared_ptr<fs::path> camera_calibration_path_;

	// The path under which all infield calibration output data lies
	std::shared_ptr<fs::path> infield_calibration_path_;

	// The path under which all projector calibration output data lies
	std::shared_ptr<fs::path> projection_calibration_path_;

	// The path under which all FQC check data lies
	std::shared_ptr<fs::path> fqc_path_;
};
