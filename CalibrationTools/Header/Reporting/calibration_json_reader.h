#pragma once

#include "calibration_data.h"
#include <nlohmann/json.hpp>

using json = nlohmann::json;
using namespace DMVS::CameraCalibration;

class CalibrationJSONReader
{
public:
	CalibrationJSONReader();

	static void ReadCameraCalibration(const std::string& json_file_path,
	                                  CalibrationData& calibration_data);
};
