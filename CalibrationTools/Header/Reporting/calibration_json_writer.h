#pragma once

#include "calibration_data.h"
#include <nlohmann/json.hpp>

using namespace DMVS::CameraCalibration;

class CalibrationJSONWriter
{
public:
	CalibrationJSONWriter();

	// Write out camera calibration information
	static void Write(const std::string& calibration_folder,
	                  double overall_rms_left,
	                  double overall_rms_right,
	                  double tangential_rms_left,
	                  double tangential_rms_right,
	                  double radial_rms_left,
	                  double radial_rms_right,
	                  bool success,
	                  const std::string& serial_number);

	// Write out camera calibration information
	static void WriteProjectorCalibrationJSON(const std::string& calibration_folder,
	                                          bool success,
	                                          double paraboloid_rms_x,
	                                          double paraboloid_rms_y,
	                                          size_t overall_points_detected,
	                                          const std::string& serial_number);

	static void WriteCameraCalibration(CalibrationData& calibration_object,
	                                   const std::string& full_file_path);

private:
	static nlohmann::json GenerateCameraCalibrationReport(double overall_rms_left,
	                                                      double overall_rms_right,
	                                                      double tangential_rms_left,
	                                                      double tangential_rms_right,
	                                                      double radial_rms_left,
	                                                      double radial_rms_right,
	                                                      bool success);

	static nlohmann::json GenerateCameraCalibrationPathExport(const std::string& calibration_folder);

	static void CheckAndCreateDMVSAppDirectory(const std::string& serial_number);

	static std::string GetCurrentAppPath();

	static bool AddProjectorCalibrationPathExport(nlohmann::json& calibration_path_test,
	                                              const std::string& calibration_folder);

	static void WriteToConsole(nlohmann::json& json_to_write);

	static std::string GetTimeCurrent();

	//Folder for app
	static const char* DMVS_APP_FOLDER;
	static const char* PROJECTOR_CALIBRATION_FILE_NAME;
	static const char* CAMERA_CALIBRATION_FILE_NAME;

	//Projector Calibration
	static const char* PROJECTOR_CALIBRATION_SUCCESS_STRING;
	static const char* PARABOLOID_RMS_X_STRING;
	static const char* PARABOLOID_RMS_Y_STRING;
	static const char* PROJECTOR_CALIBRATION_DATE_STRING;
	static const char* PROJECTOR_POINTS_DETECTED_STRING;
	static const char* PROJECTOR_CALIBRATION_FOLDER_STRING;

	//Camera Calibration
	static const char* CAMERA_CALIBRATION_SUCCESS_STRING;
	static const char* RMS_LEFT_STRING;
	static const char* RMS_RIGHT_STRING;
	static const char* TANGENTIAL_RMS_LEFT_STRING;
	static const char* TANGENTIAL_RMS_RIGHT_STRING;
	static const char* RADIAL_RMS_LEFT_STRING;
	static const char* RADIAL_RMS_RIGHT_STRING;
	static const char* CAMERA_CALIBRATION_DATE_STRING;
	static const char* CAMERA_CALIBRATION_FOLDER_STRING;

	//Standard JSON fields
	static const char* SERIAL_STRING;
	static const char* TESTS_STRING;
	static const char* TEST_STRING;
	static const char* MESSAGE_STRING;
	static const char* NAME_STRING;
	static const char* KEY_STRING;
	static const char* VALUE_STRING;
	static const char* VALUES_STRING;
	static const char* DESCRIPTION_STRING;
	static const char* LEVEL_STRING;
	static const char* STATUS_STRING;

	//Values
	static const char* CALIBRATION_DESCRIPTION_STRING;
	static const char* CAMERA_CALIBRATION_NAME_STRING;
	static const char* CAMERA_CALIBRATION_MESSAGE_STRING;

	static const char* PROJECTOR_CALIBRATION_DESCRIPTION_STRING;
	static const char* PROJECTOR_CALIBRATION_NAME_STRING;
	static const char* PROJECTOR_CALIBRATION_MESSAGE_STRING;

	//Calibration Folder stuff
	static const char* CALIBRATION_PATH_DESCRIPTION_STRING;
	static const char* CAMERA_CALIBRATION_PATH_STRING;
	static const char* PROJECTOR_CALIBRATION_PATH_STRING;
};
