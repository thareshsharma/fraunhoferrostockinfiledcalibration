#pragma once

#include "camera_sensor.h"
#include "transformation.h"
#include "calibration_boards_transformation.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief Base class for intrinsic parameter optimization (and relative
 * extrinsic parameters if camera system consist of multiple cameras).
 */
class CameraParameterOptimizer
{
public:
	virtual ~CameraParameterOptimizer();

	/**
	 * \brief
	 * \return the relative cam position of camera camera_id, i.e. the extrinsic parameters
	 * connecting it to the reference camera.
	 */
	Transformation GetRelativeCameraPosition(size_t camera_id) const;

	/**
	 * \brief
	 * \return the current calibration of camera id
	 */
	virtual std::shared_ptr<CameraSensor> GetCalibration(size_t camera_id) const;

	/**
	 * \brief
	 * \return all calibrations in its current state
	 */
	virtual std::map<size_t, std::shared_ptr<const CameraSensor>> GetAllCalibrations() const;

	/**
	 * \brief
	 * \return id of reference camera
	 */
	size_t GetReferenceCameraId() const;

	/**
	 * \brief
	 * \return The number of parameters necessary to set all intrinsic and extrinsic
	 * parameters of all cameras optimized by this object
	 */
	virtual size_t GetRequiredParameterCount() const = 0;

	/**
	 * \brief Sets object to values given in parameters and removes those values
	 */
	virtual void TakeFromParameters(std::vector<double>& parameters);

	virtual void TakeFromProjectorParameters(std::vector<double>& parameters);

	/**
	 * \brief
	 * \return Serialized state of the calibrations
	 */
	virtual std::vector<double> GetParameters() const = 0;

	virtual std::vector<double> GetProjectorParameters() const = 0;

	/**
	 * \brief Sets initial parameters
	 */
	virtual void SetInitialParameters(const ObservationMap& observations,
	                                  std::shared_ptr<const CameraSystemTransformation> camera_system_transformation,
	                                  std::shared_ptr<const CalibrationBoardsTransformation> boards) = 0;

	/**
	 * \brief if set to true (default) reference camera has identity transformation.
	 * If not set to true, transformation of all cameras are relative to the Camera System
	 * Position Finder coordinate system.
	 */
	void SetReferenceCameraIsOrigin(bool if_identity_transformation);

	/**
	 * \brief
	 */
	bool GetReferenceCameraIsOrigin() const;

protected:
	/**
	 * \brief constructor with one default value for all channels
	 * \param camera_ids the ids of all cameras to optimize
	 * \param calibration the initial calibration that needs to contain
	 * at least a realistic cameraMatrix.
	 * \param reference_camera_id the id of the reference camera
	 */
	CameraParameterOptimizer(std::set<size_t> camera_ids,
	                         std::shared_ptr<const CameraSensor> calibration,
	                         size_t reference_camera_id);

	/**
	 * \brief constructor with separate initial values for each channel
	 * \param calibration channel with initial calibration
	 * \param reference_camera_id the id of the reference camera
	 */
	CameraParameterOptimizer(std::map<size_t, std::shared_ptr<const CameraSensor>> calibration,
	                         size_t reference_camera_id);

	/**
	 * \brief the camera calibrations to optimize
	 */
	std::map<size_t, std::shared_ptr<CameraSensor>> camera_sensor_map_;

	/**
	 * \brief the reference camera id
	 */
	size_t reference_camera_id_;

	/**
	 * \brief
	 */
	bool if_reference_camera_is_at_origin_;

	const std::set<size_t> camera_ids_;

	const std::shared_ptr<const CameraSensor> calibration_;
};
}
}
