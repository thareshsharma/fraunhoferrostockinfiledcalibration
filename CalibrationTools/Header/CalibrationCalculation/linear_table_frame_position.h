#pragma once

#include "transformation.h"
#include "camera_system_transformation.h"
#include "camera_parameter_optimizer.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief Frame positions are co-linear
 * Every frame has :
 * - 3 dof if rotation is free
 * - 0 dof if rotation is not free
 * Base frame: 6 dof (rotation + translation)
 * Linear table: 2 dof (direction)
 */
class LinearTableFramePosition : public CameraSystemTransformation
{
public:

	LinearTableFramePosition(const std::map<size_t, double>& linear_table_positions);

	/**
	 * \brief
	 * \return
	 */
	size_t GetRequiredParameterCount() const override;

	/**
	 * \brief
	 * \param parameters
	 */
	void TakeFromParameters(std::vector<double>& parameters) override;

	/**
	 * \brief
	 * \return
	 */
	std::vector<double> GetParameters() const override;

	/**
	 * \brief
	 * \param observations
	 * \param camera_param_optimizer
	 * \param boards
	 */
	void SetInitialParameters(const ObservationMap& observations,
	                          std::shared_ptr<const CameraParameterOptimizer> camera_param_optimizer,
	                          std::shared_ptr<const CalibrationBoardsTransformation> boards) override;

	/**
	 * \brief
	 * \return
	 */
	std::shared_ptr<CameraSystemTransformation> Duplicate() const override;

	/**
	 * \brief
	 */
	void PreInit() override;

protected:

	/**
	 * \brief for each frame (key) the position of the linear table
	 */
	std::map<size_t, double> linear_table_positions_;

	/**
	 * \brief The position/rotation of the linear table only 5 dof because rotation
	 * is only a direction mult(x) moves x from the linear table coordinate to base
	 * coordinate system
	 */
	Transformation linear_table_to_board_;

	/**
	 * \brief
	 */
	const int frame_dof_;

private:
	Transformation GetLinearTableToBoardTransformationFromParams(const Eigen::Vector3d& pos,
	                                                             double dir_x,
	                                                             double dir_y) const;

	static std::vector<double> GetParamsFromLinearTableTransformation(const Transformation& linear_table_pose);
};
}
}
