#pragma once

#include "camera_sensor.h"
#include "camera_parameter_optimizer.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief Camera parameter optimizer for a system of cameras that have fix
 * relative position w.r.t. each other.
 */
class RelativeFixCamerasParameterOptimizer : public CameraParameterOptimizer
{
public:
	/**
	 * \brief
	 * \param camera_ids
	 * \param initial_calibration
	 * \param reference_camera_id
	 */
	RelativeFixCamerasParameterOptimizer(const std::set<size_t>& camera_ids,
	                                     const std::shared_ptr<const CameraSensor>& initial_calibration,
	                                     size_t reference_camera_id);

	/**
	 * \brief
	 * \param camera_map
	 * \param reference_camera_id
	 */
	RelativeFixCamerasParameterOptimizer(const std::map<size_t, std::shared_ptr<const CameraSensor>>& camera_map,
	                                     ImageChannel reference_camera_id);

	/**
	 * \brief Take all parameters from the back of provided parameter vector and update all SingleCameraCalibrations
	 * This function has to be consistent with GetParameters(). The same number and the same order of parameters is
	 * a must. Checks if sufficient parameters are available. Parameter vector is reduced by the used parameters
	 * from vector by taking from the back of the vector
	 * \param parameters new parameters, shortened by used values
	 */
	void TakeFromParameters(std::vector<double>& parameters) override;

	void TakeFromProjectorParameters(std::vector<double>& parameters) override;

	/**
	 * \brief Gets entire state of all the cameras as a double vector.
	 * This function has to be consistent with TakeFromParameters(...). The same number and the same order of parameters
	 * is a must.
	 */
	std::vector<double> GetParameters() const override;

	/**
	* \brief Gets entire state of all the cameras as a double vector.
	* This function has to be consistent with TakeFromParameters(...). The same number and the same order of parameters
	* is a must.
	*/
	std::vector<double> GetProjectorParameters() const override;

	/**
	 * \brief Creates a duplicate object of the current object
	 * \return
	 */
	virtual std::shared_ptr<CameraParameterOptimizer> Duplicate() const;

	/**
	 * \brief Initial parameters, only the relative extrinsic parameters. Intrinsic parameters
	 * are not guessed. Also, initial calibration is used for processing.
	 * \param observations
	 * \param camera_system_transformation
	 * \param boards_transformation
	 */
	void SetInitialParameters(const ObservationMap& observations,
	                          std::shared_ptr<const CameraSystemTransformation> camera_system_transformation,
	                          std::shared_ptr<const CalibrationBoardsTransformation> boards_transformation) override;

	/**
	 * \brief Set current calibration state from camera sensor map.
	 * \param calibrations
	 */
	void SetCalibration(std::map<size_t, std::shared_ptr<const CameraSensor>> calibrations);

	///**
	// * \brief
	// * \param channel
	// * \param calibration
	// */
	//void SetCalibration(ImageChannel channel,
	//                    std::shared_ptr<const CameraSensor> calibration);

	/**
	 * \brief Sets number of minimum markers shared by two cameras on one board to initialize relative pose of pair
	 * \param required_marker_count
	 */
	void SetRequiredNumberMarkers(size_t required_marker_count);

	/**
	 * \brief
	 * \return
	 */
	size_t GetRequiredParameterCount() const override;

private:

	/**
	 * \brief
	 * \param relative_transformation
	 * \param first_camera_id
	 * \param second_camera_id
	 * \param observations
	 * \param camera_system_transformation
	 * \param boards
	 * \return
	 */
	size_t GetRelativeTransformation(Transformation& relative_transformation,
	                                 CameraID first_camera_id,
	                                 CameraID second_camera_id,
	                                 const ObservationMap& observations,
	                                 std::shared_ptr<const CameraSystemTransformation> camera_system_transformation,
	                                 std::shared_ptr<const CalibrationBoardsTransformation> boards) const;

	/**
	 * \brief
	 * \param calibration
	 * \param parameters
	 * \return
	 */
	static std::shared_ptr<CameraSensor> GetModifiedCalibration(std::shared_ptr<CameraSensor> calibration,
	                                                            const std::vector<double>& parameters);

	/**
	 * \brief Gets parameters which can be changed (free parameters) from Calibration
	 * \param calibration
	 * \return
	 */
	static std::vector<double> GetFreeParametersFromCalibration(std::shared_ptr<const CameraSensor> calibration);

	/**
	 * \brief Marks if this CameraParameterOptimizer is already initialized
	 * (e.g. by creation from valid Freestyle calibration)
	 */
	bool initialization_done_;

	size_t required_marker_count_;
};
}
}
