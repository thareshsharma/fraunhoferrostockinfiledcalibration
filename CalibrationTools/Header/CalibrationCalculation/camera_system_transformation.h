#pragma once

#include "transformation.h"

namespace DMVS
{
namespace CameraCalibration
{
struct ObservationMap;
class CameraParameterOptimizer;
class CalibrationBoardsTransformation;

/**
 * \brief Defines interface for CameraSystemPositionFinder class.
 * Class is supposed to locate camera system in every frame.
 * See also math, described here:
 * "\\hq-eu-rd01\03engine\07_Activities\2015\V8ScannerAnglesFromCamera_ImproveRobustness\Concept.docx"
 */
class CameraSystemTransformation
{
public:

	CameraSystemTransformation();

	virtual ~CameraSystemTransformation();

	/**
	 * \brief returns the camera position in the global system of frame frameID
	 * \param frame_id
	 * \return
	 */
	const Transformation& GetFrameTransformation(size_t frame_id) const;

	/**
	 * \brief Returns the number of necessary parameters to define all frame positions
	 * \return
	 */
	virtual size_t GetRequiredParameterCount() const = 0;

	/**
	 * \brief Takes (includes "removes") the frame position parameters from parameters. Takes the required
	 * number of parameters from calculation of all frame poses and updates the poses for each frame.
	 * \param parameters parameter vector containing at least the required number of parameters
	 * to define all frames positions. Used parameters beginning from the front are removed
	 */
	virtual void TakeFromParameters(std::vector<double>& parameters) = 0;

	/**
	 * \brief Gets current data as serialized parameter set.
	 * \return
	 */
	virtual std::vector<double> GetParameters() const = 0;

	/**
	 * \brief Creates a copy of itself
	 * \return
	 */
	virtual std::shared_ptr<CameraSystemTransformation> Duplicate() const = 0;

	/**
	 * \brief called before actually guessing initial parameters. Such a pre-initialization state may be
	 * necessary to have valid relative transformation between the frames already. However not positioned
	 * in the global system correctly (cameraOrigin = ident).
	 */
	virtual void PreInit() = 0;

	/**
	 * \brief Guesses initial parameters for the frame positions and the cameraOrigin.
	 */
	virtual void SetInitialParameters(const ObservationMap& observations,
	                                  std::shared_ptr<const CameraParameterOptimizer> cams,
	                                  std::shared_ptr<const CalibrationBoardsTransformation> boards) = 0;

	/**
	 * \brief Gets frame transformation map
	 */
	virtual std::map<size_t, Transformation> GetFrameTransformationMap();

protected:

	/**
	 * \brief The cached frame transformations. They are at the moment identified
	 * frameID = index of vector. This may be dangerous. Transform 3d points from reference
	 * board coordinate system into device/camera coordinate system for the given frame.
	 */
	std::map<size_t, Transformation> frame_transformation_map_;
};
};
}
