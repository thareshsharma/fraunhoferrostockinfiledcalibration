#pragma once

/**
 * \brief Base class for fits that implements interface as expected by Eigen.
 * see: http://eigen.tuxfamily.org/dox-devel/unsupported/group__NonLinearOptimization__Module.html
 * and the examples in NonLinearOptimization.cpp
 * This class manages data type and dimensions of the NonLinearOptimization problem
 * It provides the implicit interfaces to access
 * - int inputs() : number of parameters to be estimated
 * - int values() : number of measurements
 * \tparam _Scalar
 * \tparam NX
 * \tparam NY
 */
template <typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
class GeneralFitFunctor
{
protected:

	// which data format to use, e.g. double, float...
	typedef _Scalar Scalar;

	enum
	{
		InputsAtCompileTime = NX,
		ValuesAtCompileTime = NY
	};

public:

	typedef Eigen::Matrix<Scalar, InputsAtCompileTime, 1> InputType;
	typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
	typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

	/**
	 * \brief access to the number of parameters
	 * \return the number of parameters
	 */
	int inputs() const
	{
		return inputs_;
	}

	/**
	  * \brief access to the number of function values
	  * \return the number of function values
	 */
	int values() const
	{
		return values_;
	}

protected:

	/**
	 * \brief constructor with dimensions of parameters and values known at compile time (faster)
	 */
	GeneralFitFunctor() : inputs_(InputsAtCompileTime), values_(ValuesAtCompileTime)
	{
	}

	/**
	 * \brief constructor with dimensions of parameters and values not
	 * known at compile time(slower)
	 * \param [in] inputs number of parameters
	 * \param [in] values number of function values
	 */
	GeneralFitFunctor(const int inputs,
	                  const int values) : inputs_(inputs), values_(values)
	{
	}

	// the number of parameters (inputs) and function values (values)
	const int inputs_, values_;
};
