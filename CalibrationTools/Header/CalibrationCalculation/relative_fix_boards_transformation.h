#pragma once

#include "calibration_boards_transformation.h"

using namespace DMVS::CameraCalibration;

/**
 * \brief Class for boards that are not moved during calibration process.
 */
class RelativeFixCalibrationBoardsTransformation : public CalibrationBoardsTransformation,
                                                   std::enable_shared_from_this<RelativeFixCalibrationBoardsTransformation>
{
public:
	RelativeFixCalibrationBoardsTransformation(const std::map<BoardID, std::shared_ptr<CalibrationTargetBase>>& boards,
	                                           const std::set<BoardID>& boards_to_fix);

	/**
	 * \brief
	 * \return
	 */
	size_t GetRequiredParameterCount() const override;

	/**
	 * \brief
	 * \param parameters
	 */
	void TakeFromParameters(std::vector<double>& parameters) override;

	/**
	 * \brief
	 * \return
	 */
	std::vector<double> GetParameters() const override;

	/**
	 * \brief set the relative position of a certain board. It is not
	 * tested here that refBoard is ident matrix. Should be done.
	 * \param board_positions_in
	 */
	void SetRelativePositions(const std::map<BoardID, Transformation>& board_positions_in);

	/**
	 * \brief unfixes all board positions for optimization.
	 * Attention: This changes number of parameters -> needs new camera calibration fit object.
	 */
	void UnfreezeAllBoardPositions();

protected:

	std::set<BoardID> boards_fixed_in_position_;
};
