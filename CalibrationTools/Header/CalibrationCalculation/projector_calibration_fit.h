#pragma once

#include "camera_parameter_optimizer.h"
#include "camera_system_transformation.h"
#include "calibration_boards_transformation.h"
#include "general_fit_functor.h"
#include "observation.h"
#include "calibration_target_fix_markers.h"

namespace DMVS
{
namespace CameraCalibration
{
typedef std::vector<std::shared_ptr<Observation>> VectorObservationPtr;

/**
 * \brief Optimize intrinsic and extrinsic parameters of a multi camera system at once.
 * The system to optimize consists of three objects:
 * - CameraParameterOptimizer: Through this object the camera calibrations are accessed (including
 *   their extrinsic parameters in the camera coordinate system)
 * - CameraSystemTransformation: Through this object the position of the camera system to
 *   the global system is accessed. This changes for example for every acquired frame.
 * - CalibrationBoardsTransformation: Contains the board positions in the global system. This is
 *   independent of the frame. The reference board coordinate system corresponds to the
 *   global coordinate system.
 * The algorithm does the following:
 * ObjPt == 3dpoint of the observed marker as given by the board file given by boardID.
 * imgPt == 2d point under which the marker is observed by camera camID in frame frameID.
 * 1. Calculate 3d point in camera system: trafoCam(camID) * trafoCamSystem(frameID) * trafoBoard(boardID)
 * 2. Project this 3dpoint into camera and compare with imgPt. Store deviation in error.
 */
class ProjectorCalibrationFit : public GeneralFitFunctor<double>
{
public:

	/**
	 * \brief
	 */
	ProjectorCalibrationFit(const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer,
	                        const std::map<FrameID, std::vector<cv::Point3d>>& reconstructed_points,
	                        const std::map<FrameID, std::vector<cv::Point2d>>& correspondences);

	/**
	 * \brief Sets serialized parameters x into the objects through which values can be accessed.
	 * \param parameters_eigen_vector The serialized parameters
	 * \param camera_parameter_optimizer Contains the modified camera calibrations
	 */
	static void FromEigen(const Eigen::VectorXd& parameters_eigen_vector,
	                      const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer);

	/**
	 * \brief Returns the serialized parameters describing the system state
	 * \param camera_parameter_optimizer The camera calibrations
	 */
	static Eigen::VectorXd ToEigen(const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer);

	/**
	 * \brief writes x,y errors of projection given by parameters x into fvec
	 * is used for CERES optimizer
	 */
	bool operator()(double const* const* parameters,
	                double* residual) const;

	/**
	 * \brief writes x,y errors of projection given by parameters x into fvec
	 * has been used for optimizer of Eigen library
	*/
	size_t operator()(const Eigen::VectorXd& start_params,
	                  Eigen::VectorXd& residuals) const;

	/**
	 * \brief
	 * \param reconstructed_3d_points
	 * \param correspondences
	 * \return the delta vectors, i.e. the errors, of the markers detected with camera_id, frame_id and board_id
	 */
	std::vector<cv::Point2d> GetProjectionErrors(const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
	                                             std::vector<cv::Point3d> reconstructed_3d_points,
	                                             std::vector<cv::Point2d> correspondences) const;

	/**
	 * \brief
	 * \param reconstructed_points
	 * \return
	 */
	std::vector<cv::Point2d> ProjectReconstructed3DPoints(const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
	                                                      std::vector<cv::Point3d>& reconstructed_points) const;

private:
	/**
	 * \brief returns how many measurements are available in reconstructed_points_map
	 */
	int GetValues(const std::map<FrameID, std::vector<cv::Point3d>>& reconstructed_points_map);

	/**
	 * \brief map consisting of frame and reconstructed 3d points belonging to this frame
	  * The order and the number in reconstructed points map is exactly same as in correspondences map
	 */
	std::map<FrameID, std::vector<cv::Point3d>> reconstructed_points_map_;

	/**
	 * \brief map consisting of frame and correspondences (2d Points) belonging to this frame
	 * The order and number of correspondences map is exactly same as in reconstructed points map
	 */
	std::map<FrameID, std::vector<cv::Point2d>> correspondences_;

	/**
	 * \brief converts parameters to modified camera calibrations
	 */
	const std::shared_ptr<CameraParameterOptimizer> camera_parameter_optimizer_;

	/**
	 * \brief
	 */
	size_t num_params_;

	/**
	 * \brief
	 */
	size_t num_values_;
};
}
}
