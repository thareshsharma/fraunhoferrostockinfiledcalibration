#pragma once

#include "relative_fix_boards_transformation.h"

using namespace DMVS::CameraCalibration;

class RelativeFixBoardsWithOverlapTransformation : public RelativeFixCalibrationBoardsTransformation,
                                                   std::enable_shared_from_this<RelativeFixBoardsWithOverlapTransformation>
{
public:
	RelativeFixBoardsWithOverlapTransformation(const std::map<BoardID, std::shared_ptr<CalibrationTargetBase>>& boards,
	                                           const std::set<BoardID>& boards_to_fix = std::set<BoardID>());

	/**
	* \brief Initializes positions with the assumption that all boards have a chain of overlap.
	* CameraSystemPositionFinder is therefore not used.
	*/
	void SetInitialParameters(const ObservationMap& observations,
	                          std::shared_ptr<const CameraParameterOptimizer> camera_parameter_optimizer,
	                          std::shared_ptr<const CameraSystemTransformation> camera_system_transformation) override;

private:

	int GetRelativeTransformation(Transformation& relative_transformation,
	                              BoardID first_board_id,
	                              BoardID second_board_id,
	                              const ObservationMap& observations,
	                              const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
	                              const std::shared_ptr<const CameraSystemTransformation>& camera_system_transformation) const;
};
