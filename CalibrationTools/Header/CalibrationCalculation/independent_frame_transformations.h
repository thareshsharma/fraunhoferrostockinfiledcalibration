#pragma once

#include "camera_system_transformation.h"
#include "calibration_boards_transformation.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief There is no relation between the frame positions. Every frame needs 6
 * parameters (3 rotation, 3 translation).
 */
class IndependentFrameTransformations : public CameraSystemTransformation
{
public:
	/**
	 * \brief
	 * \param frame_count
	 */
	IndependentFrameTransformations(size_t frame_count);

	/**
	 * \brief
	 * \param frame_indices
	 */
	IndependentFrameTransformations(std::set<size_t> frame_indices);

	/**
	 * \brief
	 * \return
	 */
	size_t GetRequiredParameterCount() const override;

	/**
	 * \brief
	 * \param parameters
	 */
	void TakeFromParameters(std::vector<double>& parameters) override;

	/**
	 * \brief
	 * \return
	 */
	std::vector<double> GetParameters() const override;

	/**
	 * \brief
	 * \return
	 */
	std::shared_ptr<CameraSystemTransformation> Duplicate() const override;

	/**
	 * \brief
	 */
	void PreInit() override;

	/**
	 * \brief
	 * \param observations
	 * \param camera_parameter_optimizer
	 * \param boards
	 */
	void SetInitialParameters(const ObservationMap& observations,
	                          std::shared_ptr<const CameraParameterOptimizer> camera_parameter_optimizer,
	                          std::shared_ptr<const CalibrationBoardsTransformation> boards) override;

	/**
	 * \brief
	 * \param transformations
	 */
	void SetFrameTransformations(const std::map<int, Transformation>& transformations);

	/**
 	 * \brief Sets number of minimum markers in one camera on one board to initialize frame pose
	 * \param marker_count
	 */
	void SetNumMarkerRequired(size_t marker_count);

protected:

	size_t min_marker_count_;
	std::set<size_t> frame_indices_;
};
};
}
