#pragma once

#include "observation.h"
#include "calibration_target_base.h"

namespace DMVS
{
namespace CameraCalibration
{
struct CalibrationBoardData
{
	std::vector<size_t> marker_ids;
	CircularMarkerVector markers;
	std::vector<std::shared_ptr<Observation>> observations;
};

typedef std::map<BoardID, CalibrationBoardData> CalibrationBoardDataMap;

struct CameraMeasurements
{
	CalibrationBoardDataMap calibration_board_data_map;
};

typedef std::map<CameraID, CameraMeasurements> CameraMeasurementsMap;

struct FrameMeasurements
{
	CameraMeasurementsMap camera_measurement_map;
};

typedef std::map<FrameID, FrameMeasurements> FramesMeasurementMap;

struct ObservationMap
{
	FramesMeasurementMap frames_measurement_map;
};

/**
* \brief Implementation of CalibrationTargetBase for boards with fix marker coordinates.
* Marker positions are externally measured. No parameter necessary to determine their 3d position.
* The usual calibration boards are of this type.
*/

class CalibrationTargetFixMarkers : public CalibrationTargetBase
{
public:
	/**
	 * \brief constructor with marker positions
	 * \param markers
	 */
	CalibrationTargetFixMarkers(const std::map<BoardID, cv::Point3d>& markers);

	/**
	 * \brief get marker 3d position
	 * \param marker_id
	 * \return
	 */
	cv::Point3d GetMarkerPosition(size_t marker_id) const override;

	/**
	 * \brief Sets board positions to positions given by parameters. Removes the used parameters
	 * from the parameters vector!
	 * \param parameters
	 */
	void TakeFromParameters(std::vector<double>& parameters) override;

	/**
	 * \brief returns how many parameters are necessary for this object
	 * \return
	 */
	size_t GetRequiredParameterCount() const override;

	/**
	 * \brief returns the parameters describing the board positions
	 * \return
	 */
	std::vector<double> GetParameters() const override;

	/**
	 * \brief see base class.As markers do not depend on any parameters-- > always
	 * the value is returned.
	 * \return
	 */
	const std::map<BoardID, cv::Point3d> GetMarkerPositionMap() const override;

	/**
	 * \brief
	 * \param observations
	 * \param cams
	 * \param camera_system_transformation
	 * \param target_id
	 */
	void SetInitialParameters(const ObservationMap& observations,
	                          std::shared_ptr<const CameraParameterOptimizer> cams,
	                          std::shared_ptr<const CameraSystemTransformation> camera_system_transformation,
	                          size_t target_id) override;

protected:
	/**
	 * \brief the marker ids and their 3d position
	 */
	const std::map<BoardID, cv::Point3d> markers_;
};
}
}
