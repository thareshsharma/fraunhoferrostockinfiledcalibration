#pragma once

#include "camera_parameter_optimizer.h"
#include "camera_system_transformation.h"
#include "calibration_boards_transformation.h"
#include "general_fit_functor.h"
#include "constants.h"
#include "observation.h"
#include "calibration_target_fix_markers.h"

namespace DMVS
{
namespace CameraCalibration
{
typedef std::vector<std::shared_ptr<Observation>> VectorObservationPtr;

/**
 * \brief Optimize intrinsic and extrinsic parameters of a multi camera system at once.
 * The system to optimize consists of three objects:
 * - CameraParameterOptimizer: Through this object the camera calibrations are accessed (including
 *   their extrinsic parameters in the camera coordinate system)
 * - CameraSystemPositionFinder: Through this object the position of the camera system to
 *   the global system is accessed. This changes for example for every acquired frame.
 * - CalibrationBoardsPositionFinder: Contains the board positions in the global system. This is
 *   independent of the frame. The reference board coordinate system corresponds to the
 *   global coordinate system.
 * The algorithm does the following:
 * ObjPt == 3dpoint of the observed marker as given by the board file given by boardID.
 * imgPt == 2d point under which the marker is observed by camera camID in frame frameID.
 * 1. Calculate 3d point in camera system: trafoCam(camID) * trafoCamSystem(frameID) * trafoBoard(boardID)
 * 2. Project this 3dpoint into camera and compare with imgPt. Store deviation in error.
 */
class CameraCalibrationFit : public GeneralFitFunctor<double>
{
public:

	struct MarkerIdentificationParams
	{
		MarkerIdentificationParams() :
			max_projection_distance(5.0),
			min_radius(0.003),
			max_radius(0.03),
			max_image_center_distance(0.9),
			image_width(Constants::DMVS_IMAGE_WIDTH),
			image_height(Constants::DMVS_IMAGE_HEIGHT)
		{
		}

		double max_projection_distance;
		double min_radius;
		double max_radius;
		double max_image_center_distance;
		double image_width;
		double image_height;
	};

	enum class OptimizationMode
	{
		PROJECTED_ERROR_2D,
		CARTESIAN_ERROR_3D
	};

	/**
	 * \brief
	 * \param camera_parameter_optimizer
	 * \param camera_system_transformation
	 * \param boards_transformation
	 * \param observations
	 * \param use_raw_ellipse_center
	 * \param optimization_mode
	 */
	CameraCalibrationFit(const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer,
	                     const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
	                     const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
	                     const VectorObservationPtr& observations,
	                     bool use_raw_ellipse_center        = true,
	                     OptimizationMode optimization_mode = OptimizationMode::PROJECTED_ERROR_2D);

	/**
	 * \brief Sets serialized parameters x into the objects through which values can be accessed.
	 * \param parameters_eigen_vector The serialized parameters
	 * \param camera_parameter_optimizer Contains the modified camera calibrations
	 * \param camera_system_transformation Contains the modified positions of the camera system to the board system  (== global system)
	 * \param boards_transformation Contains the modified positions of the boards
	 */
	static void FromEigen(const Eigen::VectorXd& parameters_eigen_vector,
	                      const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer,
	                      const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
	                      const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation);

	/**
	 * \brief Sets serialized parameters x into the objects through which values can be accessed.
	 * \param parameters_eigen_vector The serialized parameters
	 * \param camera_parameter_optimizer Contains the modified camera calibrations
	 */
	static void FromEigen(const Eigen::VectorXd& parameters_eigen_vector,
	                      const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer);

	/**
	 * \brief Returns the serialized parameters describing the system state
	 * \param camera_parameter_optimizer The camera calibrations
	 * \param camera_system_transformation The camera system positions mapped to the frame index
	 * \param boards_transformation The board positions
	 */
	static Eigen::VectorXd ToEigen(const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
	                               const std::shared_ptr<const CameraSystemTransformation>& camera_system_transformation,
	                               const std::shared_ptr<const CalibrationBoardsTransformation>& boards_transformation);

	/**
	 * \brief writes x,y errors of projection given by parameters x into fvec
	 * is used for CERES optimizer
	 */
	bool operator()(double const* const* parameters,
	                double* residual) const;

	/**
	 * \brief writes x,y errors of projection given by parameters x into fvec
	 * has been used for optimizer of Eigen library
	*/
	size_t operator()(const Eigen::VectorXd& x,
	                  Eigen::VectorXd& residual) const;

	/**
	 * \brief Gets errors
	 * \param camera_system_transformation
	 * \param cams_modifer
	 * \param frame_id
	 * \param camera_id
	 * \param board_id
	 * \param boards_transformation
	 * \param markers
	 * \returns the delta vectors, i.e. the errors, of the markers detected with camera_id, frame_id and board_id
	 */
	std::vector<cv::Point3d> GetErrors(const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
	                                   const std::shared_ptr<CameraParameterOptimizer>& cams_modifer,
	                                   size_t frame_id,
	                                   size_t camera_id,
	                                   size_t board_id,
	                                   const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
	                                   std::pair<std::vector<size_t>, CircularMarkerVector>* markers = nullptr) const;

	/**
	 * \brief
	 * \param camera_system_transformation
	 * \param camera_param_optimizer
	 * \param frame_id
	 * \param channel
	 * \param board_id
	 * \param boards_transformation
	 * \param markers
	 * \return the delta vectors, i.e. the errors, of the markers detected with camera_id, frame_id and board_id
	 */
	std::vector<cv::Point2d> GetProjectionErrors(const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
	                                             const std::shared_ptr<CameraParameterOptimizer>& camera_param_optimizer,
	                                             size_t frame_id,
	                                             size_t channel,
	                                             size_t board_id,
	                                             const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
	                                             std::pair<std::vector<size_t>, CircularMarkerVector>* markers = nullptr) const;

	/**
	 * \brief
	 * \param points3d
	 * \param points2d
	 * \return the delta vectors, i.e. the errors, of the markers detected with camera_id, frame_id and board_id
	 */
	std::vector<cv::Point2d> GetProjectionErrors(std::vector<cv::Point3d> points3d,
	                                             std::vector<cv::Point2d> points2d) const;

	/**
	 * \brief
	 * \param camera_system_transformation
	 * \param camera_param_optimizer
	 * \param frame_id
	 * \param camera_id
	 * \param board_id
	 * \param boards_transformation
	 \returns the cartesian error, i.e. the registration errors
	 */
	std::vector<cv::Point3d> Get3DPositionError(const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
	                                            const std::shared_ptr<CameraParameterOptimizer>& camera_param_optimizer,
	                                            size_t frame_id,
	                                            size_t camera_id,
	                                            size_t board_id,
	                                            const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
	                                            std::pair<std::vector<size_t>, CircularMarkerVector>* = nullptr) const;

	/**
	 * \brief Gets 2d projection coordinates of board markers of one board
	 * \param camera_system_transformation Contains the pose of the reference camera to the reference board for all frames
	 * \param frame_id The frame for which the markers are projection
	 * \param camera_parameter_optimizer Contains the fix relative poses of the cameras to the reference camera
	 * \param camera_id The camera into which the markers are projected
	 * \param board_id The board which is projected
	 * \param boards_transformation Contains relative fix poses of the boards to the reference board
	 * \param camera_point_3d The projected board markers in global 3d coordinates ( system of reference camera  + superior trafo)
	 * \param markers
	 * \param projected_measurements All marker measurements of detected and projected board marker
	 * \return Vector of 2d image coordinates of projections
	 */
	std::vector<cv::Point2d> ProjectBoardPoints(const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
	                                            const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer,
	                                            const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
	                                            size_t frame_id,
	                                            size_t camera_id,
	                                            size_t board_id,
	                                            std::pair<std::vector<size_t>, CircularMarkerVector>* markers = nullptr,
	                                            CalibrationBoardData* projected_measurements                  = nullptr,
	                                            std::vector<cv::Point3d>* camera_point_3d                     = nullptr) const;

	/**
	 * \brief
	 * \param reconstructed_points
	 * \return
	 */
	std::vector<cv::Point2d> ProjectBoardPoints(std::vector<cv::Point3d>& reconstructed_points) const;

	/**
	 * \brief Gets 3d coordinates of board markers in frame coordinate system
	 * \param camera_system_transformation Contains the pose of the reference camera to the reference board for all frames
	 * \param frame_id The frame for which the markers are projection
	 * \param camera_parameter_optimizer Contains the fix relative poses of the cameras to the reference camera
	 * \param camera_id The camera into which the markers are projected
	 * \param board_id The board which is projected
	 * \param boards_transformation Contains relative fix poses of the boards to the reference board
	 * \param markers
	 * \param projected_measurements All marker measurements of detected and projected board marker
	 * \return Vector of 3d coordinates of projections. In case of laser scan, in ideal case this should only have a z component,
	 *         as z is the direction of propagation in frame coordinate system.
	 */
	std::vector<cv::Point3d> GetMarkerPositionsInFrameCoordinateSystem(const std::shared_ptr<CameraSystemTransformation>&
	                                                                   camera_system_transformation,
	                                                                   const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer,
	                                                                   const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
	                                                                   size_t frame_id,
	                                                                   size_t camera_id,
	                                                                   size_t board_id,
	                                                                   std::pair<std::vector<size_t>, CircularMarkerVector>* markers,
	                                                                   CalibrationBoardData* projected_measurements) const;

	/**
	 * \brief initializes the objects with parameters.
	 * \param initialize_cams
	 * \param initialize_camera_system_position_finder
	 * \param initialize_boards_position_provider
	 * \return
	 */
	Eigen::VectorXd GetInitialParameters(bool initialize_cams                          = true,
	                                     bool initialize_camera_system_position_finder = true,
	                                     bool initialize_boards_position_provider      = true) const;

	/**
	 * \brief
	 * \return
	 */
	const ObservationMap& GetObservations() const;

	/**
	 * \brief
	 * \param entry
	 * \param correction
	 */
	void SetDistortionCorrection(const std::tuple<size_t, size_t, size_t, size_t>& entry,
	                             const cv::Vec2f& correction);

	/**
	 * \brief
	 * \param if_use_raw_ellipse_center
	 */
	void SetUseRawEllipseCenter(bool if_use_raw_ellipse_center);

	/**
	 * \brief
	 * \return
	 */
	Eigen::VectorXd GetCurrentParameters() const;

	/**
	 * \brief Updates the ellipse correction term of projected circles
	 * The projection of a circle center differs from the ellipse center of a projected circle. This method updates the
	 * correction term of the circle observations based on the current system setup
	 * \param  observations all Marker observations including not identified
	 * \param camera_parameter_optimizer Relative camera poses
	 * \param camera_system_transformation Frame poses reference camera to reference board
	 * \param boards_transformation Relative board poses
	 * \param filter_value  absolute filter value, if e.g. 2sigma is required calculate it before and pass value here
	 * \return number of observations
	 */
	static size_t UpdateEllipseCorrections(VectorObservationPtr& observations,
	                                       const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
	                                       const std::shared_ptr<const CameraSystemTransformation>& camera_system_transformation,
	                                       const std::shared_ptr<const CalibrationBoardsTransformation>& boards_transformation,
	                                       double filter_value = -1.0);

	/**
	 * \brief
	 * \param observations
	 * \param marker
	 * \param camera_id
	 * \param board_id
	 * \param marker_id
	 * \param frame_id
	 */
	static void AddObservation(VectorObservationPtr& observations,
	                           const CircularMarker& marker,
	                           size_t camera_id,
	                           size_t board_id,
	                           size_t marker_id,
	                           size_t frame_id);

	/**
	 * \brief Identify board and marker of uncoded markers based on the current boards/cameras setup
	 * \param observations  all Marker observations including not identified
	 * \param camera_parameter_optimizer Relative camera poses
	 * \param camera_system_transformation Frame poses reference camera to reference board
	 * \param boards_transformation Relative board poses
	 * \param identification_params
	 * \return Number of identifications
	 */
	static size_t IdentifyUncodedMarker(VectorObservationPtr& observations,
	                                    const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
	                                    const std::shared_ptr<const CameraSystemTransformation>& camera_system_transformation,
	                                    const std::shared_ptr<const CalibrationBoardsTransformation>& boards_transformation,
	                                    const MarkerIdentificationParams& identification_params);

	/**
	 * \brief converts array of observations to mapped observations for simpler access
	*/
	static ObservationMap GetObservationMap(const VectorObservationPtr& observations);

private:

	/**
	 * \brief returns how many parameters are required by the model
	 */
	static bool IsUsedObservation(const std::shared_ptr<const Observation>& observation);

	/**
	 * \brief returns how many parameters are required by the model
	 */
	static size_t CountParameters(const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
	                              const std::shared_ptr<const CameraSystemTransformation>& camera_system_transformation,
	                              const std::shared_ptr<const CalibrationBoardsTransformation>& boards_transformation);

	/**
	 * \brief returns how many function values are used for optimization
	 */
	static size_t CountValues(const VectorObservationPtr& observations,
	                          OptimizationMode optimization_mode);

	/**
	 * \brief converts parameters to modified camera calibrations
	 */
	const std::shared_ptr<CameraParameterOptimizer> camera_parameter_optimizer_;

	/**
	 * \brief converts parameters to modified board positions. Reference board defines global coordinate system.
	 */
	const std::shared_ptr<CalibrationBoardsTransformation> boards_transformation_;

	/**
	 * \brief converts parameters to modified camera system positions, i.e. frame dependent trafo
	 */
	const std::shared_ptr<CameraSystemTransformation> camera_system_transformation_;

	/**
	 * \brief the mapped individual marker observations
	 */
	ObservationMap observations_;

	/**
	 * \brief whether to use the raw ellipse or the perspective distortion corrected ellipse center
	 */
	bool use_raw_ellipse_center_;

	/**
	 * \brief
	 */
	const OptimizationMode optimization_mode_;

	/**
	 * \brief
	 */
	size_t num_params_;

	/**
	 * \brief
	 */
	size_t num_values_;
};
}
}
