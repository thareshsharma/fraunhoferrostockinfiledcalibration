#pragma once

#include "camera_sensor.h"

using namespace DMVS::CameraCalibration;

/**
* \brief Class to determine the difference between the projection of a circles center
* to the center of the (distorted) ellipse created by projection of the circle
* Working principle:
* Given : circle in 3D with position, radius and normal
* A: Calculate projection of circle center with given camera parameters
* B: Create sample points on the 3D circle. Project all sample points to camera
*    coordinates with given camera parameters and fit ellipse to the 2d coordinates
* C: Calculate difference.
* */
class EllipseDetectionDistortionCorrection
{
public:
	/**
	 * \brief input struct for circles
	*/
	struct Circle
	{
		cv::Vec3d center; // global center point of the circle
		cv::Vec3d normal; // normal of the circle surface
		float radius;     // radius in m
	};

	/**
	* \brief constructor
	* \param calibration
	* \param rotation_matrix the current rotation of the camera to the global system (in which the circles are given)
	* \param translation_vector the current translation of the camera
	* \param calibration camera the camera matrix
	*/
	EllipseDetectionDistortionCorrection(const cv::Mat& rotation_matrix,
	                                     const cv::Mat& translation_vector,
	                                     const std::shared_ptr<const CameraSensor>& calibration);

	/**
	* \brief calculate corrections for a vector of circles
	* \param [in] model vector of 3D circles for which the 2D corrections are calculated
	* \param projected_centers_out
	* \param projected_circles_out
	* \returns Corrected Vector to projection Of Circle Center for each circle
	*/
	std::vector<cv::Vec2d> GetCorrections(const std::vector<Circle>& model,
	                                      std::vector<cv::Vec2d>* projected_centers_out       = nullptr,
	                                      std::vector<cv::RotatedRect>* projected_circles_out = nullptr) const;

	/**
	* \brief gets the projections of the circle centers of the last getCorrections() call
	* \return 2D coordinates of projection of circle centers, same size as circles at last
	*/
	//std::vector<cv::Vec2d> GetCircleCentersOfLastCorrection() const;

	///**
	// * \brief get the projections of the circles last getCorrections() call
	// * Ellipses are created by projection of sample points of the 3D circle followed
	// * by a 2d ellipse fit to the projected sample points
	// * \return ellipses of the circle projections of latest getCorrections(...) call
	// */
	//std::vector<cv::RotatedRect> GetCircleProjections() const;

	/**
	 * \brief calculates the real world circle diameter assuming the circle has z=1m
	 * distance to camera
	 */
	std::vector<double> GetNormedRealWorldDiameters(const std::vector<cv::RotatedRect>& ellipses) const;

private:

	// the rotatory position of the camera
	const cv::Mat rotation_matrix_;

	// the translation position of the camera
	const cv::Mat translation_vector_;

	const std::shared_ptr<const CameraSensor> calibration_;

	// sample points of a base circle - used to calculate 3d sample points for 3d circles
	std::vector<cv::Vec3d> base_circle_pts_;

	const int num_sample_points_;

	// normal vector of base circle
	const cv::Vec3d base_normal_;

	// center of base circle
	const cv::Vec3d base_center_;

	// radius of base circle
	const double base_radius_;
};
