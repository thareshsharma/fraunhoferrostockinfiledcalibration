#pragma once

#include "constants.h"
#include "circular_marker.h"

namespace DMVS
{
namespace CameraCalibration
{
template <typename T>
class MarkerQualityEstimator
{
public:
	explicit MarkerQualityEstimator(cv::Mat_<T>& image) : image_(image)
	{
	}

	bool EstimateMarkerQuality(CircularMarker& marker)
	{
		//Get sub image containing ellipse data in Ellipse.
		//This is the first estimation of the real ellipse contour
		//This is scaled by 20 % to get some edge parts
		auto scaled_ellipse = marker.CorrectedEllipse();
		scaled_ellipse.size.width *= Constants::SCALING_FACTOR_EDGE_PARTS;
		scaled_ellipse.size.height *= Constants::SCALING_FACTOR_EDGE_PARTS;

		//Maximum height and width check
		const auto aoi = scaled_ellipse.boundingRect();

		if(aoi.height > Constants::MAXIMUM_AOI_SIZE || aoi.width > Constants::MAXIMUM_AOI_SIZE)
			return false;

		//Check if area of interest is fully inside image,
		//exclude 1 pixel border because there are no gradients available
		if((aoi.x < 1) || (aoi.x > image_.cols - 2) || (aoi.y < 1) || (aoi.y > image_.rows - 2))
			return false;

		const auto bounding_rectangle_width  = aoi.width;
		const auto bounding_rectangle_height = aoi.height;

		if((aoi.x + bounding_rectangle_width > image_.cols - 2) || (aoi.y + bounding_rectangle_height > image_.
			rows - 2))
			return false;

		//Create a mask that defines which pixels are used for the refinement
		//Translate the center of the scaled ellipse so that it is correct in the area of interest patch
		auto outer_ellipse = marker.CorrectedEllipse();
		outer_ellipse.center.x -= aoi.x;
		outer_ellipse.center.y -= aoi.y;

		outer_ellipse.size.width *= Constants::SCALING_FACTOR_MASK;
		outer_ellipse.size.height *= Constants::SCALING_FACTOR_MASK;

		cv::Mat mask = cv::Mat::zeros(bounding_rectangle_height, bounding_rectangle_width, image_.type());
		cv::ellipse(mask, outer_ellipse, Constants::OUTER_ELLIPSE_COLOR, -1);
		double surrounding_sum     = 0;
		double inner_sum           = 0;
		uint64 count_inner         = 0;
		uint64 count_surrounding   = 0;
		const auto local_mask_rows = mask.rows - 1;
		const auto local_mask_cols = mask.cols - 1;

		for(auto y = 0; y < local_mask_rows; ++y)
		{
			for(auto x = 0; x < local_mask_cols; ++x)
			{
				if(mask.at<T>(y, x) == 0)
				{
					surrounding_sum += double(image_.at<T>(aoi.y + y, aoi.x + x));
					count_surrounding++;
				}
				else
				{
					inner_sum += double(image_.at<T>(aoi.y + y, aoi.x + x));
					count_inner++;
				}
			}
		}

		marker.average_inner_area       = inner_sum / double(count_inner);
		marker.average_surrounding_area = surrounding_sum / double(count_surrounding);

		return true;
	}

private:
	cv::Mat_<T> image_;
};
}
}
