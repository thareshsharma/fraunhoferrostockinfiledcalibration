#pragma once

#include "BoardDetector/board_detector.h"
#include "observation.h"
#include "enum_image_channel_type.h"
#include "relative_fix_cameras_parameter_optimizer.h"
#include "calibration_boards_transformation.h"
#include "capture_calibration.h"

namespace DMVS
{
namespace CameraCalibration
{
class CameraCalibrationFit;

/**
 * \brief Class for calculating Calibration. This class contains the mechanism
 * for processing the observations and replacing old with the newly calculated
 * calibration.
 */
class CalibrationCalculator
{
public:
	CalibrationCalculator();

	CalibrationCalculator(std::vector<std::shared_ptr<Observation>> observations);

	~CalibrationCalculator();

	/**
	 * \brief Copy constructor
	 * \param rhs
	 */
	CalibrationCalculator(const CalibrationCalculator& rhs);

	/**
	 * \brief Assignment operator
	 * \param rhs
	 * \return
	 */
	CalibrationCalculator& operator=(const CalibrationCalculator& rhs);

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	CalibrationCalculator(CalibrationCalculator&& rhs) noexcept = delete;

	/**
	 * \brief Forcing a move assignment constructor to be deleted by the compiler.
	 */
	CalibrationCalculator& operator=(CalibrationCalculator&& rhs) noexcept = delete;

	/**
	 * \brief Set the calibration boards
	 * \param boards
	 * \return
	 */
	int SetBoards(std::vector<std::shared_ptr<BoardDetector>> boards);

	/**
	 * \brief Sets the relative fixed camera using camera sensor map
	 * \param camera_sensor_map
	 * \param image_channel
	 * \return
	 */
	size_t SetRelativeFixedCamera(std::map<size_t, std::shared_ptr<const CameraSensor>>& camera_sensor_map,
	                              ImageChannel image_channel = CHANNEL_BGR0);

	/**
	 * \brief Sets the relative fixed camera using capture calibration
	 * \param capture_calibration
	 * \return
	 */
	int SetRelativeFixedCamera(const CaptureCalibration& capture_calibration);

	/**
	 * \brief Sets the relative fixed camera using calibration and channels
	 * \param calibration
	 * \param channels
	 * \return
	 */
	int SetRelativeFixedCamera(const std::shared_ptr<const CameraSensor>& calibration,
	                           const std::set<size_t>& channels);

	/**
	 * \brief Gets the relative fixed camera. This will make sense only after
	 * processing observations.
	 * \return
	 */
	std::shared_ptr<RelativeFixCamerasParameterOptimizer> GetRelativeFixedCamerasParameterOptimizer() const;

	/**
	 * \brief Gets the relative fixed camera. This will make sense only after
	 * processing observations.
	 * \return
	 */
	std::shared_ptr<RelativeFixCamerasParameterOptimizer> GetRelativeFixedCamerasParameterOptimizerLens() const;

	/**
	 * \brief Gets the board positions. This method should be called only after
	 * processing observations.
	 * \return
	 */
	std::shared_ptr<CalibrationBoardsTransformation> GetCalibrationBoardsTransformation() const;

	/**
	 * \brief Gets the camera system position. This method should be called only after
	 * processing observations.
	 * \return
	 */
	std::shared_ptr<CameraSystemTransformation> GetCameraSystemTransformation() const;

	/**
	 * \brief Sets the camera system position finder using frame count
	 * \param frame_count
	 * \return
	 */
	int InitializeCameraSystemTransformation(size_t frame_count);

	/**
	 * \brief Sets the camera system position finder using camera system position finder object
	 * \param system_position_finder
	 * \return
	 */
	int SetCameraSystemTransformation(std::shared_ptr<CameraSystemTransformation>& system_position_finder);

	/**
	 * \brief Process the observation that have been recorded
	 * \return
	 */
	bool ProcessObservationsInfield();

	/**
	 * \brief Process the observation that have been recorded
	 * \return
	 */
	bool ProcessObservationsInHouse(bool use_existing_observations = false);

	/**
	 * \brief Gets the results of processing
	 * \param result_calibration
	 * \return
	 */
	int ReplaceWithNewCalibration(CaptureCalibration& result_calibration) const;

	/**
	 * \brief Sets number of marker required for initialization of poses
	 * \param minimum_initial_frame_marker Required number of marker in one cam
	 * and on one board to calculate an initial frame pose
	 * \param minimum_initial_marker_relative_camera Required number of marker in
	 * two cam and on one board to calculate an initial relative pose
	*/
	void SetMinimumInitialMarker(size_t minimum_initial_frame_marker,
	                             size_t minimum_initial_marker_relative_camera);

	/**
	 * \brief Error feedback
	 * \param image
	 * \param frame_id
	 * \param channel
	 * \param color
	 */
	void DrawErrors(cv::Mat& image,
	                size_t frame_id,
	                size_t channel,
	                const cv::Scalar& color = Constants::DARK_BLUE) const;

	/**
	 * \brief
	 * \param out
	 * \param frame_id
	 * \param camera_id
	 * \return
	 */
	double AppendErrors(std::ofstream& out,
	                    size_t frame_id,
	                    size_t camera_id) const;

	/**
	 * \brief
	 * \param out
	 */
	void AppendFrameAndBoardPositions(std::ofstream& out) const;

	/**
	 * \brief
	 * \param camera_id
	 * \return
	 */
	double GetChannelRMS(size_t camera_id);

	/**
	 * \brief
	 * \param camera_id
	 * \return
	 */
	std::pair<double, double> GetChannelRMSTanRad(size_t camera_id);

	/**
	 * \brief Gets the calibration result of a single channel. Replaces the calibration parameters
	 * of the given channel in the given CaptureCalibration.
	 * The channel is the camera ID which has been passed implicitly with the observations as CameraIDs
	 * \param result_cal CaptureCalibration in which the channels calibration should be updated with the result
	 * \param channel The camera channel that should be updated (e.g. CHANNEL_BGR0)
	 * \return 0: OK < 0 error
	 */
	int GetChannelResult(CaptureCalibration& result_cal,
	                     ImageChannel channel) const;

	/**
	 * \brief
	 * \return
	 */
	size_t GetObservationSize() const;

	/**
	 * \brief
	 * \param start_params
	 * \param correspondences_3d_points_map
	 * \param correspondences_calibrated_2d_points_map
	 * \return
	 */
	std::pair<size_t, double> OptimizeProjectorSystem(Eigen::VectorXd& start_params,
	                                                  std::map<FrameID, std::vector<cv::Point3d>>& correspondences_3d_points_map,
	                                                  std::map<FrameID, std::vector<cv::Point2d>>& correspondences_calibrated_2d_points_map) const;

private:
	void ClearObservationsDeactivation();

	static void LogParams(std::map<unsigned long long, std::shared_ptr<const CameraSensor>>::value_type& calibration,
	                      std::vector<double> params);

	double InitSystem(Eigen::VectorXd& start_params) const;

	std::pair<size_t, double> OptimizeSystem(Eigen::VectorXd& start_params,
	                                         const std::string& description,
	                                         bool initialize_board_positions = false) const;

	static size_t DeactivateCodedMarker(std::vector<std::shared_ptr<Observation>>& observations,
	                                    const std::vector<std::shared_ptr<BoardDetector>>& boards);

	double Get1dRMS(const Eigen::VectorXd& vector) const;

	double Get2dRMS(const Eigen::VectorXd& vector) const;

	double GetXRMS(const Eigen::VectorXd& vector) const;

	double GetYRMS(const Eigen::VectorXd& vector) const;

	std::shared_ptr<CameraCalibrationFit> camera_calibration_fit_;
	std::shared_ptr<CalibrationBoardsTransformation> calibration_boards_transformation_;
	std::shared_ptr<RelativeFixCamerasParameterOptimizer> relative_fix_cameras_parameter_optimizer_;
	std::shared_ptr<CameraSystemTransformation> camera_system_transformation_;
	std::vector<std::shared_ptr<Observation>> observations_;
	std::vector<std::shared_ptr<BoardDetector>> boards_;
	std::pair<size_t, double> option_result_;

	size_t minimum_initial_frame_marker_;
	size_t minimum_initial_marker_relative_camera_;

	size_t image_width_;
	size_t image_height_;
	bool if_observations_processed_;
};
}
}
