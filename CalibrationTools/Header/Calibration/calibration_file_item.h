#pragma once

#include "helper.h"

/**
 * \brief Header for stored items.
 */
#pragma pack(push, CalibrationFileItem, 1)

typedef struct CalibrationFileItem
{
	int record_number;
	TCHAR name[64];
	uint32_t flags;
	size_t size;
	uint32_t sync;

	static const uint32_t SYNC = 0xFAFBFCFD;

	void Set(const int record_number,
	         const std::string& name,
	         const uint32_t flags,
	         const size_t size)
	{
		memset(this, 0x0, sizeof(CalibrationFileItem));

		this->record_number = record_number;
		_tcscpy_s(this->name, DMVS::CameraCalibration::Helper::Widen(name.c_str()));
		this->flags = flags;
		this->size  = size;
		this->sync  = SYNC;
	}
} CalibrationFileItem;

#pragma pack(pop, CalibrationFileItem)
