#pragma once

#include "io_messages.h"
#include "calibration_cache_item.h"
#include "calibration_cache_manager.h"
#include "critical_section.h"

/**
 * \brief Implementation of hybrid cache for Calibration
 */
class CalibrationCacheImpl
{
public:

	static const size_t DEFAULT_MAX_ITEMS = 200;
	static const size_t DEFAULT_MIN_ITEMS = 5;

	/**
	 * \brief Create memory based cache
	 * \return
	 */
	CalibrationCacheImpl();

	/**
	 * \brief Forcing a copy constructor to be deleted by the compiler.
	 */
	CalibrationCacheImpl(const CalibrationCacheImpl& rhs) = delete;

	/**
	 * \brief Forcing a assignment operator to be deleted by the compiler.
	 */
	CalibrationCacheImpl& operator=(const CalibrationCacheImpl& rhs) = delete;

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	CalibrationCacheImpl(CalibrationCacheImpl&& rhs) = delete;

	/**
	 * \brief Forcing a move assignment operator to be deleted by the compiler.
	 */
	CalibrationCacheImpl& operator=(CalibrationCacheImpl&& rhs) = delete;

	/**
	 * \brief Free resources.
	 * \return
	 */
	~CalibrationCacheImpl();

	/**
	 * \brief Reads a magic number from a file. The magic
	 * is a 64 bit number. Returns -1 if something is wrong.
	 * \param path
	 * \return
	 */
	static uint64_t Magic(const std::string& path);

	/**
	 * \brief Attaches a file for hybrid cache using memory and file.
	 * \param path
	 * \param mode
	 * \param magic
	 * \return IOMessages::IOResult
	*/
	IOMessages::IOResult Attach(const std::string& path,
	                            int mode,
	                            uint64_t magic);

	/**
	 * \brief Returns the name of the file used by the cache.
	 * \return std::string
	 */
	std::string Filename() const;

	/**
	 * \brief Gets a list of all cache items (table of contents)
	 * \param items
	 * \return
	 */
	IOMessages::IOResult TableOfContents(std::vector<CalibrationCacheItem>& items);

	/**
	 * \brief Closes the associate file of the hybrid cache.
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Close();

	/**
	 * \brief Sets maximal number of cached items.
	 * \param max_size
	 * \return
	*/
	void MaximumItems(size_t max_size);

	/**
	 * \brief Add a string.
	 * \param record_number
	 * \param name
	 * \param string
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Put(int record_number,
	                         const std::string& name,
	                         const std::string& string);

	/**
	 ** \brief Gets a string.
	 * \return IOMessages::IOResult
	 * \param record_number
	 * \param name
	 * \param string
	 */
	IOMessages::IOResult Get(int record_number,
	                         const std::string& name,
	                         std::string& string);

	/**
	 * \brief Add a raw byte buffer to the cache
	 * \param record_number
	 * \param name
	 * \param bytes
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Put(int record_number,
	                         const std::string& name,
	                         const std::vector<unsigned char>& bytes);

	/**
	 * \brief Gets a raw byte buffer from the cache
	 * \param record_number
	 * \param name
	 * \param bytes
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Get(int record_number,
	                         const std::string& name,
	                         std::vector<unsigned char>& bytes);

	/**
	 * \brief Add a configuration container.
	 * \param record_number
	 * \param name
	 * \param container
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Put(int record_number,
	                         const std::string& name,
	                         const std::shared_ptr<ConfigurationContainer>& container);

	/**
	 * \brief Gets a SCENE attribute container.
	 * \param record_number
	 * \param name
	 * \param container
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Get(int record_number,
	                         const std::string& name,
	                         std::shared_ptr<ConfigurationContainer>& container);

	/**
	 * \brief Add an OpenCV matrix.
	 * \param record_number
	 * \param name
	 * \param mat
	 * \param matrix_type
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Put(int record_number,
	                         const std::string& name,
	                         const cv::Mat& mat,
	                         uint32_t matrix_type);

	/**
	 * \brief Add an existing arbitrary item.
	 * \param item to add, record_number and name is part of the item
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Put(const CalibrationCacheItem& item);

	/**
	 * \brief Gets data an existing item. Loads the empty table of content item
	 * \param item to get, record_number and name is part of the item
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Get(CalibrationCacheItem& item);

	/**
	 * \brief Gets an OpenCV matrix.
	 * \param record_number
	 * \param name
	 * \param mat
	 * \param matrix_type
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Get(int record_number,
	                         const std::string& name,
	                         cv::Mat& mat,
	                         uint32_t matrix_type);

private :
	/**
	 * \brief Item map. Indexed by key.
	 */
	typedef std::pair<CalibrationCacheItem::Key, CalibrationCacheItem> CalibrationCacheEntry;
	typedef std::map<CalibrationCacheItem::Key, CalibrationCacheItem> CalibrationCacheItems;

	/**
	 * \brief Gets the index of an item in the cache. Returns end() if
	 * no data loaded or if the data type is invalid.
	 * \param record_number
	 * \param name
	 * \param flags
	 * \return
	 */
	CalibrationCacheItems::const_iterator Get(int record_number,
	                                          const std::string& name,
	                                          uint32_t flags);

	/**
	 * \brief Add a new item to the cache. Invoke memory management
	 * and storing if necessary.
	 * \param record_number
	 * \param name
	 * \param item
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Put(int record_number,
	                         const std::string& name,
	                         const CalibrationCacheItem& item);

	/**
	 * \brief Manage cache for the given item. Calls rejuvenateItem and removeOldItems.
	 * removeOldItems may be called on another thread.
	 * \param key
	 * \return IOMessages::IOResult
	*/
	IOMessages::IOResult Manage(CalibrationCacheItem::Key key);

	/**
	 * \brief Remove items from cache until the given cache limit
	 * is reached. If we are in async mode, this method is called
	 * in a separate thread.
	 * \return void
	 */
	void RemoveOldItems();

	/**
	 * \brief The CalibrationCacheManager calls the function rejuvenateItem
	 */
	friend class CalibrationCacheManager;

	/**
	 * \brief Increase the age of a given cache item. Prevents
	 * that the removeOldItems method unloads it.
	 * \param key
	 * \return void
	 */
	void RejuvenateItem(CalibrationCacheItem::Key key);

	/**
	 * \brief Associates a file with the hybrid cache. Loads the contents
	 * if necessary.
	 * \param path
	 * \param mode
	 * \param magic
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Open(const std::string& path,
	                          int mode,
	                          uint64_t magic);
	/**
	 * \brief Name of the file used
	 */
	std::string filename_;

	CalibrationCacheItems items_;

	/**
	 * \brief Lock for access to the m_items field. The
	 * CalibrationCacheManager m_writer may run in another thread.
	 */
	CriticalSection items_lock_;

	/**
	 * \brief Items sorted by age.
	 */
	typedef std::list<CalibrationCacheItem::Key> CalibrationCacheAges;
	CalibrationCacheAges ages_;

	/**
	 * \brief Lock for access to the m_ages field. The
	 * CalibrationCacheManager m_writer may run in another thread.
	 */
	CriticalSection ages_lock_;

	/**
	 * \brief Thread for asynchronous writing
	 */
	CalibrationCacheManager* writer_;

	/**
	 * \brief Maximal number of cached items.
	 */
	size_t max_items_;

	/**
	 * \brief Handle for I/O. I/O is implemented in the
	 * CalibrationCacheItem class. This class has knowledge how to
	 * read and write data to file.
	 */
	CalibrationCacheItem::FileIO file_;
};
