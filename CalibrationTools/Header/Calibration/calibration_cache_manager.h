#pragma once

#include "thread_base.h"

class CalibrationCacheImpl;

/**
 *\brief Class for asynchronous writing of data to file.
 */
class CalibrationCacheManager : public ThreadBase
{
public:
	/**
	 * \brief Create the saver thread. Pass a SFMCacheImpl object
	 * for the save call.
	 * \param cache
	 * \return
	 */
	explicit CalibrationCacheManager(CalibrationCacheImpl* cache);

	/**
	 * \brief Forcing a copy constructor to be deleted by the compiler.
	 */
	CalibrationCacheManager(const CalibrationCacheManager& rhs) = delete;

	/**
	 * \brief Forcing a assignment operator to be deleted by the compiler.
	 */
	CalibrationCacheManager& operator=(const CalibrationCacheManager& rhs) = delete;

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	CalibrationCacheManager(CalibrationCacheManager&& rhs) = delete;

	/**
	 * \brief Forcing a move assignment operator to be deleted by the compiler.
	 */
	CalibrationCacheManager& operator=(CalibrationCacheManager&& rhs) = delete;

	virtual ~CalibrationCacheManager();

protected:
	/**
	 * \brief Call the cache cleanup method (removeOldItems) of
	 * the SFMCacheImpl class.
	 * \return bool
	 */
	bool RunThreadProc() override;

private:
	CalibrationCacheImpl* calibration_cache_impl_;
};
