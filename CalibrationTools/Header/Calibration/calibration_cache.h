#pragma once

#include "io_messages.h"
#include "calibration_cache_item.h"

class CalibrationCacheImpl;

/**
 * \brief Caching for dmvs data. Provides a hybrid cache using memory and file.
 * Old items will be removed from memory if the cache is full. If necessary,
 * data is loaded from file again.
 * If no data file is given, just manage the memory cache. Data is written to file
 * as fas as possible. OpenCV matrices and SCENE attribute container use references.
 * Data is serialized when writing to file only.
 */
class CalibrationCache
{
public:
	/**
	 * \brief I/O modes for cache
	 * Read: Open an existing file and read the table of contents. Writes are allowed when needed.
	 * Write: Open a new file.
	 * Async: Write data to file in an extra thread - without blocking the caller.
	 */
	enum
	{
		CACHE_CLOSED = 0x0,
		CACHE_READ = 0x1,
		CACHE_WRITE = 0x2,
		CACHE_ASYNC = 0x10,
	};

	/**
	 * \brief Magic number in data files. The ASCII version is FARO.
	 */
	static const uint64_t CALIBRATION_CACHE_MAGIC      = 0x4F524146;
	static const uint64_t CALIBRATION_CACHE_MAGIC_MASK = 0xFFFFFFFF;

	/**
	 * \brief Create a memory cache.
	 * \return
	 */
	CalibrationCache();

	CalibrationCache(const std::shared_ptr<ConfigurationContainer>& configuration_container,
	                 const std::string& serial_number,
	                 const std::string& file_name);

	/**
	 * \brief Free all resources.
	 * \return
	 */
	virtual ~CalibrationCache();

	/**
	 * \brief Forcing a copy constructor to be deleted by the compiler.
	 */
	CalibrationCache(const CalibrationCache& rhs) = delete;

	/**
	 * \brief Forcing a assignment operator to be deleted by the compiler.
	 */
	CalibrationCache& operator=(const CalibrationCache& rhs) = delete;

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	CalibrationCache(CalibrationCache&& rhs) = delete;

	/**
	 * \brief Forcing a move assignment operator to be deleted by the compiler.
	 */
	CalibrationCache& operator=(CalibrationCache&& rhs) = delete;

	/**
	 * \brief Create a magic number using the FARO magic number and
	 * a code. The code may be the sensor code (VG_CAP_...).
	 * \param code
	 * \return magic code
	*/
	static uint64_t MagicCode(size_t code);

	/**
	 * \brief Inverse of \fn MagicCode
	 * \param magic number
	 * \return int
	 */
	static int CodeMagic(uint64_t magic);

	/**
	 * \brief Reads a magic number from a file. The magic is a 64 bit
	 * number. Returns -1 if something is wrong.
	 * \param path
	 * \return
	 */
	static uint64_t Magic(const std::string& path);

	/**
	 * \brief Attaches a file for hybrid cache using memory and file.
	 * Test for the magic number if the file exists. Write the magic number
	 * if the file doesn't exist.
	 * \param path
	 * \param mode
	 * \param magic
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Attach(const std::string& path,
	                            size_t mode,
	                            uint64_t magic = CALIBRATION_CACHE_MAGIC);

	/**
	 * \brief Returns the name of the file used by the cache.
	 * \return Cache file name
	 */
	std::string Filename() const;

	/**
	 * \brief Returns the current open mode of the cache. Returns 0 if not open.
	 * \return
	 */
	unsigned int Mode() const;

	/**
	 * \brief Gets a list of all cache items (table of contents)
	 * \param items : Container which gets filled with table of contents
	 * \return
	 */
	IOMessages::IOResult TableOfContents(std::vector<CalibrationCacheItem>& items) const;

	/**
	 * \brief Close the cache. Need this method for testing CPPUNIT.
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Close();

	/**
	 * \brief Sets maximal number of cached items.
	 * \param max_size : maximum size of the cache
	*/
	void MaximumItems(size_t max_size) const;

	/**
	 * \brief Add a string value to cache.
	 * \param record_number
	 * \param name
	 * \param string
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Put(size_t record_number,
	                         const std::string& name,
	                         const std::string& string) const;

	/**
	 * \brief Gets a string value from cache.
	 * \param record_number
	 * \param name
	 * \param string
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Get(size_t record_number,
	                         const std::string& name,
	                         std::string& string) const;
	/**
	 * \brief Add a raw bytes vector to the cache. Data in not processed.
	 * \param record_number
	 * \param name
	 * \param bytes
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Put(size_t record_number,
	                         const std::string& name,
	                         const std::vector<unsigned char>& bytes) const;

	/**
	 * \brief Gets a raw bytes vector from the cache. Data in not processed.
	 * \param record_number
	 * \param name
	 * \param bytes
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Get(size_t record_number,
	                         const std::string& name,
	                         std::vector<unsigned char>& bytes) const;

	/**
	 * \brief Add a configuration container to cache.
	 * \return IOMessages::IOResult
	 * \param record_number
	 * \param name
	 * \param container
	 */
	IOMessages::IOResult Put(size_t record_number,
	                         const std::string& name,
	                         const std::shared_ptr<ConfigurationContainer>& container) const;

	/**
	 * \brief Gets a configuration container from cache.
	 * \param record_number
	 * \param name
	 * \param container
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Get(size_t record_number,
	                         const std::string& name,
	                         std::shared_ptr<ConfigurationContainer>& container) const;

	/**
	 * \brief Add an OpenCV matrix to cache.
	 * Assumes that 8-bit color images are in BGR format. Uses JPEG compression to
	 * store such images. If your image show funny colors, check the format. Use
	 * cv::cvtColor(..., ..., CV_RGB2BGR) to transform the images.
	 * All other images are stored as raw buffers with (lossless) lz4 compression.
	 * \param record_number
	 * \param name
	 * \param mat
	 * \param matrix_type
	 * \return IOMessages::IOResult
	*/
	IOMessages::IOResult Put(size_t record_number,
	                         const std::string& name,
	                         const cv::Mat& mat,
	                         uint32_t matrix_type = CalibrationCacheItem::CV_MATRIX) const;

	/**
	 * \brief Gets an OpenCV matrix from cache.
	 * \return IOMessages::IOResult
	 * \param record_number
	 * \param name
	 * \param mat
	 * \param matrix_type
	 */
	IOMessages::IOResult Get(size_t record_number,
	                         const std::string& name,
	                         cv::Mat& mat,
	                         uint32_t matrix_type = CalibrationCacheItem::CV_MATRIX) const;
	/**
	* \brief Add an existing arbitrary item.
	* \param item calibration cache item to add, record_number and name is part of the item
	* \return IOMessages::IOResult
	*/
	IOMessages::IOResult Put(const CalibrationCacheItem& item) const;

	/**
	 * \brief Gets data an existing item. Loads the empty table of content item
	 * \param item calibration cache item to get, record_number and name is part of the item
	 * \return IOMessages::IOResult
	 */
	IOMessages::IOResult Get(CalibrationCacheItem& item) const;

	/**
	 * \brief Create OpenCV image from JPEG buffer. Used to decode
	 * images from FPGA. Implemented in SFMCacheItem.cpp because it is
	 * similar to the decode method.
	 * JPEG data start at byte "startByte" in the buffer.
	 * \param bytes
	 * \param mat
	 * \param start_byte
	 * \return
	*/
	static IOMessages::IOResult FromJPEG(const std::vector<unsigned char>& bytes,
	                                     cv::Mat& mat,
	                                     size_t start_byte = 0);
	/**
	 * \brief JPEG compression for an OpenCV image. Used to encode
	 * images. Implemented in SFMCacheItem.cpp because it is
	 * similar to the encode method.
	 * JPEG data start at byte "startByte" in the buffer.
	 * \param mat
	 * \param bytes
	 * \param start_byte
	 * \return
	 */
	static IOMessages::IOResult ToJPEG(const cv::Mat& mat,
	                                   std::vector<unsigned char>& bytes,
	                                   size_t start_byte = 0);

private:
	CalibrationCacheImpl* cache_;
	size_t mode_;
};
