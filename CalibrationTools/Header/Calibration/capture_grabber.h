#pragma once

#include "smoother.h"
#include "thread_base.h"
#include "dmvs.h"

/**
 * \brief Class for asynchronous grabbing.
 */
class CaptureGrabber final : public ThreadBase
{
public:
	/**
	 * \brief Create the grabber thread. Pass a capture object for the grab call.
	 * \param capture
	 * \return
	 */
	explicit CaptureGrabber(const std::shared_ptr<DMVSSensor>& capture)
		: capture_(capture),
		  tick_(GetTickCount64()),
		  grab_interval_(90)
	{
		if(!capture_)
			return;

		// Grabbing speed in milliseconds
		// Use an adjustment parameter to take care for delays.
		grab_interval_ = static_cast<int>(1000.0 / 10.0);
		grab_interval_ = std::max(16, std::min(grab_interval_, 1000));
		grab_adjust_   = -10;
	}

	virtual ~CaptureGrabber()
	{
		capture_ = nullptr;
	}

	/**
	 * \brief Forcing a copy constructor to be deleted by the compiler.
	 */
	CaptureGrabber(const CaptureGrabber& rhs) = delete;

	/**
	 * \brief Forcing a assignment operator to be deleted by the compiler.
	 */
	CaptureGrabber& operator=(const CaptureGrabber& rhs) = delete;

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	CaptureGrabber(CaptureGrabber&& rhs) = delete;

	/**
	 * \brief Forcing a move assignment operator to be deleted by the compiler.
	 */
	CaptureGrabber& operator=(CaptureGrabber&& rhs) = delete;

	double Framerate()
	{
		return framerate_.Avg() > 0.0 ? 1.0 / framerate_.Avg() : 0.0;
	}

protected:
	/**
	 * \brief Call the grab method of the RSSensor class. Take care
	 * for the grab frequency
	 * \return bool
	 */
	bool RunThreadProc() override
	{
		// Sanity checks
		if(!capture_)
			return false;

		if(capture_->open_mode != DeviceCurrentMode::GRABBING)
			return false;

		if(thread_terminate_)
			return false;

		// Make sure the we are not faster than the given frame rate.
		auto tick_diff = GetTickCount64() - tick_;

		if(tick_diff < grab_interval_ + grab_adjust_)
			SleepEx((DWORD)(grab_interval_ + grab_adjust_ - tick_diff), TRUE);

		tick_diff = GetTickCount64() - tick_;
		tick_     = GetTickCount64();

		// Save grabbing interval for frame rate calculation
		// If the interval is too big, reset the smoother.
		if(tick_diff / 1000.0 < 10.0)
			framerate_.Add(tick_diff / 1000.0);
		else
			framerate_.ClearHistory();

		// Grab the next frame
		//return m_capture->GrabFromSensor();
		return true;
	}

private:
	Smoother<10> framerate_;
	std::shared_ptr<DMVSSensor> capture_;
	ULONGLONG tick_;
	int grab_interval_;
	int grab_adjust_;
};
