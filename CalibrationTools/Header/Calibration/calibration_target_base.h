#pragma once

#include "typedefs.h"

namespace DMVS
{
namespace CameraCalibration
{
struct ObservationMap;
class CameraParameterOptimizer;
class CalibrationBoardsTransformation;
class CameraSystemTransformation;

/**
* \brief This class is used in combination with CameraCalibrationFit. This class defines an
* interface to basic calibration targets. These may be a Freestyle OnSiteCalibration
* board as an example. Each calibration target has several markers. The 3d position
* of these markers may depend on parameters. This dependence on parameters must be
* implemented in the derived class of CalibrationTarget.
* See math: "\\hq-eu-rd01\03engine\07_Activities\2015\V8ScannerAnglesFromCamera_ImproveRobustness\Concept.docx"
*/

class CalibrationTargetBase
{
public:

	virtual ~CalibrationTargetBase() = default;

	/**
	 * \brief Getter for position of the given marker_id
	 */
	virtual cv::Point3d GetMarkerPosition(size_t marker_id) const = 0;

	/**
	 * \brief Initializes the board positions by taking values from parameter vector.
	 * The values which are used are then removed from the parameter vector.
	 * \param parameters
	 */
	virtual void TakeFromParameters(std::vector<double>& parameters) = 0;

	/**
	 * \brief Returns how many parameters are necessary for this object
	 */
	virtual size_t GetRequiredParameterCount() const = 0;

	/**
	 * \brief Returns the parameters describing the board positions
	 */
	virtual std::vector<double> GetParameters() const = 0;

	/**
	 * \brief Sets initial parameters
	 * \param observations
	 * \param cameras
	 * \param camera_system_transformation
	 * \param target_id
	 */
	virtual void SetInitialParameters(const ObservationMap& observations,
	                                  std::shared_ptr<const CameraParameterOptimizer> cameras,
	                                  std::shared_ptr<const CameraSystemTransformation> camera_system_transformation,
	                                  size_t target_id) = 0;

	/**
	 * \brief Updates the camera dependent markers
	 * \param cameras
	 * \param camera_system_transformation
	 * \param boards
	 * \param target_id
	 */
	virtual void UpdateCameraDependentMarkers(std::shared_ptr<const CameraParameterOptimizer> cameras,
	                                          std::shared_ptr<const CameraSystemTransformation> camera_system_transformation,
	                                          std::shared_ptr<const CalibrationBoardsTransformation> boards,
	                                          size_t target_id)
	{
		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	}

	/**
	 * \brief Gets all current marker coordinates
	 */
	virtual const std::map<BoardID, cv::Point3d> GetMarkerPositionMap() const = 0;

protected:

	CalibrationTargetBase() = default;
};
}
}
