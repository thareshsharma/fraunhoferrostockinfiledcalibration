#pragma once

#include "laser_info.h"
#include "camera_info.h"

namespace DMVS
{
namespace CameraCalibration
{
struct CalibrationData
{
	CalibrationData();

	std::string hardware_version;
	std::string calibration_date;
	std::string serial_number;
	std::string firmware_version;
	std::string calibration_software_version;
	bool is_depth_supported;

	double laser_temperature_begin;
	double laser_temperature_end;

	double camera1_temperature_begin;
	double camera1_temperature_end;

	double camera2_temperature_begin;
	double camera2_temperature_end;

	double back_plate_sensor1_temperature_begin;
	double back_plate_sensor1_temperature_end;

	double back_plate_sensor2_temperature_begin;
	double back_plate_sensor2_temperature_end;

	double back_plate_sensor3_temperature_begin;
	double back_plate_sensor3_temperature_end;

	double pressure_begin;
	double pressure_end;

	double humidity_begin;
	double humidity_end;

	double final_rms;
	size_t detected_points;

	CameraInfo camera_left;
	CameraInfo camera_right;
	LaserInfo projector;
};
}
}
