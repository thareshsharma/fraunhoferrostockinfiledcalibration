#pragma once

struct MarkerData
{
	size_t marker_id;
	size_t frame_id;
	cv::Vec2f position_2d_grey0;
	cv::Vec2f position_2d_grey1;
	cv::Point3d position_3d;
	bool is_valid;
};

struct MarkerInfo
{
	size_t marker_id;
	size_t frame_id;
	cv::Vec2f measured_position_2d;
	cv::Point3d reconstructed_position_3d;
	bool is_valid;
};