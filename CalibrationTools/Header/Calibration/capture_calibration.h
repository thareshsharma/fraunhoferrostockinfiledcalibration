#pragma once

#include "camera_sensor.h"

namespace DMVS
{
namespace CameraCalibration
{
struct CalibrationData;
}
}

namespace fs = boost::filesystem;
//using namespace DMVS::CameraCalibration;

/**
 * \brief Keeps all the relevant data for single camera sensor
 */
class CaptureCalibration
{
	typedef std::vector<ImageChannel> ImageChannelVector;
	typedef std::vector<char> BinaryBuffer;
	typedef std::map<ImageChannel, std::shared_ptr<const CameraSensor>> CameraSensor2Map;
	typedef std::map<std::string, cv::Mat> CustomMatrices;

public:
	CaptureCalibration();

	CaptureCalibration(const CaptureCalibration& rhs);

	CaptureCalibration& operator=(const CaptureCalibration& rhs);

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	CaptureCalibration(CaptureCalibration&& rhs) = default;

	/**
	 * \brief Forcing a move assignment operator to be deleted by the compiler.
	 */
	CaptureCalibration& operator=(CaptureCalibration&& rhs) = delete;

	~CaptureCalibration();

	static std::shared_ptr<const CameraSensor> CreateFromCalibrationData(const CalibrationData& calibration_data,
	                                                                     ImageChannel image_channel);

	/**
	 * \brief Gets all calibration data of a channel camera
	 * \param channel image channel of device e.g. CHANNEL_BGR0, CHANNEL_GREY0
	 * \return structure containing intrinsic and extrinsic calibration data
	 */
	std::shared_ptr<const CameraSensor> GetCalibration(ImageChannel channel) const;

	/**
	 * \brief Sets calibration data for a single camera sensor ( access as channel)
	 * \param channel image channel of device e.g. CHANNEL_BGR0, CHANNEL_GREY0
	 * \param camera_calibration camera calibration (with camera matrix, distortion matrix
	 * rotation matrix and translation vector)
	 */
	void SetCalibration(ImageChannel channel,
	                    const std::shared_ptr<const CameraSensor>& camera_calibration);

	/**
	 * \brief Sets custom matrix for a channel (access via channel).
	 * Currently used for projector beam directions
	 * \param name name of the custom matrix
	 * \param custom_mat
	 */
	void SetCustomMatrix(const char* name,
	                     const cv::Mat& custom_mat);

	/**
	 * \brief Gets a custom matrix by name. laser_beam is our only custom matrix for now.
	 * \param name Name of the custom matrix
	 * \return custom matrix with the given name
	 */
	const cv::Mat& GetCustomMatrix(const char* name) const;

	/**
	 * \brief Gets a custom matrix by name. laser_beam is our only custom matrix for now.
	 * \return custom matrix with the given name
	 */
	const cv::Mat& GetPatternDefinition() const;

	/**
	 * \brief Save the device calibration to disk. The destination folder
	 * is automatically created by a static VgVideoCapture function
	 * \param filename filename to be used for saving the calibration
	 * \param if_overwrite Flag used for overwriting the file
	 * \return success: true failed: false
	 */
	bool Save(const std::string& filename,
	          const std::string& if_overwrite) const;

	/**
	 * \brief Load the device calibration from disk. The search folder
	 * is automatically created by a static function
	 * \param calibration_filename filename to be used
	 * \return success: true failed: false
	 */
	bool Load(const std::string& calibration_filename);

	/**
	* \brief check if the device calibration is on disk.  The search folder
	* is automatically created by a static function
	* \param filename filename to be used
	* \return success: true failed: false
	*/
	bool Exists(const std::string& filename);

	/**
	 * \brief Gets the time of the calibration. We use the
	 * "last write time" of the file.
	 * \param filename filename of the calibration file.
	 * \return
	 */
	__time64_t GetCalibrationTime(const fs::path& filename) const;

	/**
	 * \brief Load calibration file for given recording. Searches
	 * calibration using the recording path with extension xml.
	 * \param filename filename to be used
	 * \return success: true failed: false
	 */
	bool LoadForRecording(const std::string& filename);

	/**
	 * \brief Load calibration from file. Needs valid file.
	 * \param full_file_path Full file path of the file which has to be loaded
	 * \return bool if the file is loaded or not
	 */
	bool LoadFile(fs::path& full_file_path);

	/**
	 * \brief Load calibration from file in XML format. Needs valid file.
	 * \param full_file_path path to xml file which has to be loaded.	 *
	 * \return bool if the file is loaded or not
	 */
	bool LoadCalibrationFromXmlFile(fs::path & full_file_path);

	/**
	 * \brief Load calibration from file in JSON format. Needs valid file.
	 * \param full_file_path path to JSON file which has to be loaded.
	 * \return bool if the file is loaded or not
	 */
	bool LoadCalibrationFromJsonFile(fs::path& full_file_path);

	/**
	 * \brief Initializes field by extracting fields from Default_Calibration.json
	 * \param full_file_path calibration file from which the data has to be loaded
	 * \return true if everything worked.
	 */
	bool FillFromContainer(const fs::path& full_file_path);

	using BinaryBuffer = std::vector<char>;

	/**
	* \brief Convert the calibration to a compressed binary
	* buffer. Used for storing the calibration in the EEPROM.
	* Compressed size ~95KB.
	* \param binary
	* \return bool
	*/
	bool ToBinary(BinaryBuffer& binary);

	/**
	 * \brief Restore the calibration from compressed binary
	 * buffer. Used for storing the calibration in the EEPROM.
	 * Compressed size ~95KB.
	 * \param binary
	 * \return
	 */
	bool FromBinary(BinaryBuffer& binary);

	/**
	 * \brief Gets all channels of the calibration e.g. CHANNEL_BGR0, CHANNEL_GREY0
	 * \return vector with channels
	 */
	ImageChannelVector GetImageChannels() const;

	/**
	 * \brief
	 * \param rotation_matrix
	 * \param translation_vector
	 */
	void MoveToCoordinateSystem(const cv::Mat& rotation_matrix,
	                            const cv::Mat& translation_vector);

	/**
	 * \brief MoveToCoordinateSystem seems to be not correct for every transformation.
	 * This method can be used to move to the projector system
	 * \param rotation
	 * \param translation
	 */
	void MoveToCoordinateSystemAdjusted(const cv::Mat& rotation,
	                                    const cv::Mat& translation);

	/**
	 * \brief
	 * \return
	 */
	std::string GetSerialNumber() const;

private:
	// Map containing all cameras of the capture device
	CameraSensor2Map cameras_sensors_map_;
	CustomMatrices custom_matrices_;
	std::string serial_number_;
	std::string calibration_date_;
	double timestamp_;
};
