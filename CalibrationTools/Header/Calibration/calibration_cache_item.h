#pragma once

#pragma warning(disable: 4996 4267 4946)

#include "io_messages.h"
#include "critical_section.h"

/**
 * \brief Data item in the cache.  Items may contain a string  or a matrix.
 * The key of an item is calculated using the
 * record number and the name. This is an integral data type with public
 * members only. It is possible to use a C++11 initializer list.
 */
class ConfigurationContainer;

class CalibrationCacheItem
{
public:
	typedef unsigned char Byte;
	typedef size_t Key;
	typedef HANDLE FileIO;

	/**
	 * \brief Constants: Flag values for persistence and data type.
	 **/
	enum PersistenceFlags
	{
		// File and cache state
		VOLATILE = 0x0,
		PERSISTENT = 0x1,
		STORED_IN_FILE = 0x2,
		NO_DATA_LOADED = 0x4,

		// Data types
		STRING = 0x01000,
		ATTRIBUTE_CONTAINER = 0x02000,
		CONFIG_CONTAINER = 0x04000,
		RAW_BYTES = 0x10000,
		CV_MATRIX = 0x20000,
		CV_RAW_MATRIX = 0x40000,
		DATA_TYPES = 0xFF000,

		// File position
		NOT_STORED = 0xFFFFFFFF,
	};

	CalibrationCacheItem();

	CalibrationCacheItem(size_t record_number,
	                     const std::string& name,
	                     const std::string& input_string);

	CalibrationCacheItem(size_t record_number,
	                     const std::string& name,
	                     const std::vector<unsigned char>& bytes);

	CalibrationCacheItem(size_t record_number,
	                     const std::string& name,
	                     const std::shared_ptr<ConfigurationContainer>& container);

	CalibrationCacheItem(size_t record_number,
	                     const std::string& name,
	                     const cv::Mat& mat,
	                     uint32_t matrix_type);

	CalibrationCacheItem(const CalibrationCacheItem& rhs);

	CalibrationCacheItem& operator=(const CalibrationCacheItem& rhs);

	/**
	 *\brief Open a file for caching of items.
	 * The cache item class is responsible for read and writes
	 * to file. Therefore the open and close functions are in
	 * this class.
	 * \param path
	 * \param mode
	 * \param io
	 * \return
	 */
	static IOMessages::IOResult Open(const std::string& path,
	                                 size_t mode,
	                                 FileIO& io);
	/**
	* \brief Close a file for caching of items. The cache item class is responsible for
	* read and writes to file. Therefore the open and close functions are in this class.
	* \param io
	* \return
	*/
	static IOMessages::IOResult Close(FileIO& io);

	/**
	 * \brief Read raw data from file at the given position. If pos is -1, no seek.
	 * \return
	 * \param io
	 * \param data
	 * \param num_bytes
	 * \param pos
	*/
	static IOMessages::IOResult Read(FileIO& io,
	                                 void* data,
	                                 size_t num_bytes,
	                                 uint64_t pos);

	/**
	 * \brief Appends raw data to file. Returns the write position in the pos variable.
	 * \param io
	 * \param data
	 * \param num_bytes
	 * \param pos
	 * \return
	 */
	static IOMessages::IOResult Write(FileIO& io,
	                                  const void* data,
	                                  size_t num_bytes,
	                                  uint64_t& pos);
	/**
	 * \brief Reads an item from file. Updates the item status.
	 * \param io
	 * \param toc_mode
	 * \return
	 */
	IOMessages::IOResult Read(FileIO& io,
	                          bool toc_mode);

	/**
	 * \brief Writes an item to file. Updates the item status.
	 * \param io
	 * \return
	 */
	IOMessages::IOResult Write(FileIO& io);

	/**
	 * \brief Release associated memory
	 * \return void
	 */
	void Release();

	/**
	 * \brief Generate key value for cache.
	 * \param record_number
	 * \param name
	 * \return size_t
	*/
	static Key KeyValue(size_t record_number,
	                    const std::string& name);

	/**
	 * \brief Record number
	 */
	int record_number;

	/**
	 * \brief  Name
	 */
	std::string name;

	/**
	 * \brief Flags (see above)
	 */
	uint32_t flags;

	/**
	 * \brief Position in file. Has value NotStored if not stored (yet)
	 */
	uint64_t stored_position;

	/**
	 * \brief Size in file
	 */
	size_t stored_size;

	/**
	 * \brief String data
	 */
	std::string string;

	/**
	 * \brief OpenCV matrix
	 */
	cv::Mat image_mat;

	/**
	 * \brief
	 */
	std::shared_ptr<ConfigurationContainer> configuration_container;

	/**
	 * \brief For record locking by the item cache
	 */
	mutable CriticalSection critical_section;

private:
	/**
	 * \brief Create a encoded version of the item. Used to store the item on file.
	 * \param bytes
	 * \return
	*/
	IOMessages::IOResult Encode(std::vector<Byte>& bytes) const;

	/**
	 * \brief Create a encoded version of the item. Used to store the item on file.
	 * \param bytes
	 * \return
	 */
	IOMessages::IOResult EncodeContainer(std::vector<Byte>& bytes) const;

	/**
	 * \brief Create a encoded version of the item. Used to store the item on file.
	 * \param bytes
	 * \return
	 */
	IOMessages::IOResult EncodeString(std::vector<Byte>& bytes) const;

	/**
	 * \brief Create a encoded version of the item. Used to store the item on file.
	 * \param bytes
	 * \return
	 */
	IOMessages::IOResult EncodeMatrix(std::vector<Byte>& bytes) const;

	/**
	 * \brief Create a encoded version of the item. Used to store the item on file.
	 * \param bytes
	 * \return
	 */
	IOMessages::IOResult EncodeRawMatrix(std::vector<Byte>& bytes) const;

	/**
	 * \brief Create a encoded version of the item. Used to store the item on file.
	 * \param bytes
	 * \return
	 */
	IOMessages::IOResult EncodeRawBytes(std::vector<Byte>& bytes) const;

	/**
	 * \brief Initialize the item using the encoded data. used to load the item from file.
	 * \param bytes
	 * \return
	*/
	IOMessages::IOResult Decode(const std::vector<Byte>& bytes);

	/**
	 * \brief Initialize the item using the encoded data. used to load the item from file.
	 * \param bytes
	 * \return
	*/
	static IOMessages::IOResult DecodeContainer(const std::vector<Byte>& bytes);

	/**
	 * \brief Initialize the item using the encoded data. used to load the item from file.
	 * \param bytes
	 * \return
	*/
	IOMessages::IOResult DecodeMatrix(const std::vector<Byte>& bytes);

	/**
	 * \brief Initialize the item using the encoded data. used to load the item from file.
	 * \param bytes
	 * \return
	*/
	IOMessages::IOResult DecodeRawMatrix(const std::vector<Byte>& bytes);

	/**
	 * \brief Initialize the item using the encoded data. used to load the item from file.
	 * \param bytes
	 * \return
	*/
	IOMessages::IOResult DecodeRawBytes(const std::vector<Byte>& bytes);
};
