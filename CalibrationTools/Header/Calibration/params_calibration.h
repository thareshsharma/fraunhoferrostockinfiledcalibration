#pragma once

namespace DMVS
{
namespace CameraCalibration
{
struct ParamsCalibration
{
	ParamsCalibration() = default;

	int threshold_step;
	int min_threshold;
	int max_threshold;

	size_t min_repeatability;
	float min_distance_between_blobs;

	bool set_id_by_color;
	uchar blob_color;

	bool filter_by_area;
	float min_area;
	float max_area;

	bool filter_by_circularity;
	float min_circularity;
	float max_circularity;

	bool filter_by_inertia;
	float min_inertia_ratio;
	float max_inertia_ratio;

	bool filter_by_convexity;
	float min_convexity;
	float max_convexity;

	bool filter_by_ring_in_ring;
	double min_viewing_angle;

	int white_circle_id;
	int black_circle_id;

	//Defines max number of iterations for gradient based refinement
	int refine_ellipses;
	bool decode_ring_marker;
	bool coded_only;

	double percentage_agreement_with_artificial_ellipse;

	int filter_ellipse_width;
	double scale;
};
}
}
