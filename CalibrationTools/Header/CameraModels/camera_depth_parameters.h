#pragma once

/**
 * \brief Container for depth parameters of a camera
 */
struct CameraDepthParameters
{
	CameraDepthParameters() :
		scalar(cv::Vec2d(0.0, 0.0)),
		exponential(cv::Vec3d(0.0, 0.0, 0.0))
	{
	}

	cv::Vec2d scalar;
	cv::Vec3d exponential;
};
