#pragma once

#include "camera_info.h"
#include "sensor.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief Base calibration for all camera calibration models.
 * Keeps all the relevant data for single camera sensor.
 */
class CameraSensorNew : public Sensor
{
public:
	CameraSensorNew();

	explicit CameraSensorNew(const CameraInfo& camera_info);

	explicit CameraSensorNew(const cv::Mat4d& transformation_matrix);

	CameraSensorNew(const cv::Mat& rotation_matrix,
	                const cv::Mat& translation_vector,
	                ImageChannel image_channel);

	CameraSensorNew(const CameraSensorNew& rhs);

	CameraSensorNew& operator=(const CameraSensorNew& rhs);

	CameraSensorNew(CameraSensorNew&& rhs) noexcept;

	CameraSensorNew& operator=(CameraSensorNew&& rhs) noexcept;

	virtual ~CameraSensorNew();

	/**
	 * \brief Creates an duplicate of the current object
	 */
	std::shared_ptr<Sensor> Duplicate() const = 0;

	/**
	 * \brief Projects a vector of 3d points in superordinate coordinates into 2d
	 * image coordinates of the camera.
	 * \param points_3d 3D points to be projected either in superordinate system
	 * coordinates or in camera coordinates (see apply_camera_extrinsics)
	 * \param points_2d 2D points to be projected either in superordinate system
	 * coordinates or in camera coordinates (see apply_camera_extrinsics)
	 * \param apply_camera_extrinsics true: points_3d given in superordinate system, own R and T applied
	 *								  false: points_3d given in camera system, own R and T ignored
	 * \param ignore_depth flag for ignoring the depth
	 */
	void ProjectPoints(cv::InputArray points_3d,
	                   cv::OutputArray points_2d,
	                   bool apply_camera_extrinsics,
	                   bool ignore_depth) const = 0;

	/**
	* \brief Undistort image coordinates to pinhole coordinates with focal length 1m
	* \param points_2d Image coordinates to be undistorted to pinhole focal length 1m coordinates
	* \param out undistorted 2d pinhole focal length 1 coordinates
	* \param points_3d approximated 3d position for depth dependent lens corrections
	*/
	void UndistortPoints(cv::InputArray points_2d,
	                     cv::OutputArray out,
	                     cv::InputArray points_3d) const = 0;

	/**
	 * \brief Indicates whether straighten points can handle the points3d input
	 * \return true: Camera Sensor has depth dependent camera model
	 *		   false: no depth dependent camera model
	 */
	bool HandlesDepthValues() const = 0;

	/**
	 * \brief Fixes extrinsic parameters
	 * \param fix_extrinsic_parameters Flag to fix extrinsic parameters
	 */
	virtual void KeepExtrinsicParametersFix(bool fix_extrinsic_parameters) = 0;

	/**
	 * \brief Fixes distortion
	 * \param fix_distortion Flag to fix distortion
	 */
	virtual void KeepDistortionFix(bool fix_distortion) = 0;

	/**
	 * \brief Check if this method has to be here or not. To me it doesn't
	 * make sense as laser projector doesn't have any
	 * Gets the best open CV model
	 * \param camera_matrix Camera intrinsic matrix
	 * \param distortion_vector Distortion vector containing distortion values
	 * \return
	 */
	virtual bool GetBestFitCVModel(cv::Mat& camera_matrix,
	                               cv::Mat& distortion_vector) const = 0;

	/**
	 * \brief Reconstructs ray direction in world coordinates.
	 * \param points_2d
	 * \param out
	 * \param normalize
	 * \param points3d
	 */
	void RayReconstructionWCS(cv::InputArray points_2d,
	                          cv::OutputArray out,
	                          bool normalize,
	                          cv::InputArray points3d = cv::noArray()) const override;

	/**
	 * \brief Reconstructs ray direction in world coordinates.
	 * \param points_2d
	 * \param out
	 * \param normalize
	 * \param points3d
	 */
	void RayReconstructionSCS(cv::InputArray points_2d,
	                          cv::OutputArray out,
	                          bool normalize,
	                          cv::InputArray points3d = cv::noArray()) const override;

	/**
	 * \brief
	 * \param points_2d
	 * \param plane_position
	 * \param plane_normal
	 * \param intersection_points_coordinate_system
	 */
	virtual void GetPlaneIntersectionPoints(cv::InputArray& points_2d,
	                                        const cv::Vec3d& plane_position,
	                                        const cv::Vec3d& plane_normal,
	                                        cv::OutputArray intersection_points_coordinate_system) const;

private:
	// Intrinsic parameters
	cv::Mat intrinsic_matrix_;
	cv::Mat distortion_vector_;
};
}
}
