#pragma once

/**
 * \brief Different camera models that are supported by camera calibration
 */
enum CameraModel
{
	DEPTH_DEPENDENT_MODEL = 0,

	SINGLE_CAMERA_MODEL,
};
