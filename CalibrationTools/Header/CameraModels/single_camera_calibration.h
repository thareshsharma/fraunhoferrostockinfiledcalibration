#pragma once

#include "camera_sensor.h"
#include "camera_info.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief Base calibration for all calibration models that are based
 * on the open cv camera model. Derived classes need to overwrite undistortToBaseModel
 * and distortToBaseModel so that the physics described by this class can be used.
 * Keeps all the relevant data for single camera sensor
 */
class SingleCameraCalibration : public CameraSensor
{
public:
	SingleCameraCalibration();

	SingleCameraCalibration(const CameraInfo& camera_info);

	/**
	 * Constructs matrix with individual elements, using defaults if necessary
	 */
	SingleCameraCalibration(cv::InputArray& camera_matrix_in,
	                        cv::InputArray& distortion            = cv::noArray(),
	                        cv::InputArray& rotation_matrix       = cv::noArray(),
	                        cv::InputArray& translation_vector_in = cv::noArray());

	/**
	 * Copy constructor makes real copies of matrices
	 */
	SingleCameraCalibration(std::shared_ptr<const SingleCameraCalibration> source);

	/**
	 * \brief
	 * \param pos
	 * \return
	 */
	static std::shared_ptr<const CameraSensor> FromBinary(std::vector<char>::const_iterator& pos);

	/**
	 * \brief
	 * \return
	 */
	std::vector<double> GetParamsVector() const override;

	/**
	 * \brief
	 * \return
	 */
	std::vector<size_t> GetFixations() const override;

	/**
	 * \brief Converts the parameter vector to camera info object
	 * \param params
	 * \param camera_info
	 * \return
	 */
	static bool ParamVectorToCameraInfo(const std::vector<double>& params,
	                                    CameraInfo& camera_info);

	/**
	 * \brief Converts the CameraInfo object to parameter vector
	 * \param camera_info
	 * \param params
	 * \return
	 */
	static bool CameraInfoToParamVector(const CameraInfo& camera_info,
	                                    std::vector<double>& params);
	/**
	 * \brief Gets CameraInfo object containing calibration data
	 * \return returns the camera_info object
	 */
	CameraInfo GetCameraInfo() const;

	/**
	 * \brief Gets the current camera intrinsic parameter namely camera matrix
	 * and distortion vector
	 * \param camera_matrix Camera intrinsic matrix
	 * \param distortion_vector Distortion vector containing distortion values
	 */
	void GetCameraIntrinsicParams(cv::Mat& camera_matrix,
	                              cv::Mat& distortion_vector) const override;

	/**
	 * \brief Initializes from param vector
	 * \param params parameter vector containing all the camera specific parameters
	 * \return
	 */
	size_t InitFromParamsVector(const std::vector<double>& params) override;

	/**
	 * \brief Creates duplicate of the current object
	 * \return shared pointer to CameraSensor object
	 */
	std::shared_ptr<CameraSensor> Duplicate() const override;

	/**
	 * \brief
	 * \return
	 */
	std::shared_ptr<CameraSensor> DuplicateLaser() const override;

	/**
	 * \brief Projects a vector of 3d points in world coordinates int 2d
	 * image coordinates of the camera.
	 * \param points3d
	 * \param points2d
	 * \param if_apply_camera_extrinsics true: R and T describe position to system in which points3d is defined.
	 *									 false: points3d are defined in the same system as camera.
	 * \param if_ignore_depth
	 */
	void ProjectPoints(cv::InputArray& points3d,
	                   cv::OutputArray points2d,
	                   bool if_apply_camera_extrinsics = true,
	                   bool if_ignore_depth            = false) const override;

	/**
	 * \brief
	 * \param points_3d
	 * \param points_2d
	 * \param apply_camera_extrinsics
	 * \param ignore_depth
	 */
	virtual void LaserProjectPoints(cv::InputArray& points_3d,
	                                cv::OutputArray points_2d,
	                                bool apply_camera_extrinsics = true,
	                                bool ignore_depth            = true) const override;

	/**
	* \brief Analogue to openCV function, but also considers the table based correction
	* \param points_2d
	* \param out
	* \param points_3d
	*/
	void UndistortPoints2(cv::InputArray points_2d,
	                      cv::OutputArray out,
	                      cv::InputArray points_3d = cv::noArray()) const override;
	/**
	 * \brief Checks for depth value support
	 * \return if the depth values are being considered
	 */
	bool HandlesDepthValues() const override;

	/**
	 * \brief Fixes extrinsic parameters
	 * \param fix_extrinsic_parameters
	 */
	void KeepExtrinsicParametersFix(bool fix_extrinsic_parameters) override;

	/**
	 * \brief Fixes distortion parameters
	 * \param fix_distortion if distortion has to be fixed
	 */
	void KeepDistortionFix(bool fix_distortion) override;

	/**
	 * \brief Fixes same focal length
	 * \param fix_same_focal_length if same focal length has to be fixed
	 */
	void KeepSameFocalLengthFix(bool fix_same_focal_length) override;

	/**
	 * \brief Fixes same focal length
	 * \param fix_focal_length if focal length has to be fixed
	 */
	void KeepFocalLengthFix(bool fix_focal_length) override;

	/**
	 * \brief Ensures that the depth dependent parameters scalar 1 is fixed
	 * \param fix_dd_scalar_param1_fix if depth dependent scalar 1 parameter is kept fixed
	 */
	void KeepDepthDependentScalarParam1Fix(bool fix_dd_scalar_param1_fix) override;

	/**
	 * \brief Ensures that the depth dependent parameters scalar 2 is fixed
	 * \param fix_dd_scalar_param2_fix if depth dependent scalar 2 parameter is kept fixed
	 */
	void KeepDepthDependentScalarParam2Fix(bool fix_dd_scalar_param2_fix) override;

	/**
	 * \brief Ensures that the depth dependent parameters exponential 1 is fixed
	 * \param fix_dd_exp_param1_fix if depth dependent exponential 1 parameter is kept fixed
	 */
	void KeepDepthDependentExpParam1Fix(bool fix_dd_exp_param1_fix) override;

	/**
	 * \brief Ensures that the depth dependent parameters exponential 2 is fixed
	 * \param fix_dd_exp_param2_fix if depth dependent exponential 2 parameter is kept fixed
	 */
	void KeepDepthDependentExpParam2Fix(bool fix_dd_exp_param2_fix) override;

	/**
	 * \brief Ensures that the depth dependent parameters exponential 3 is fixed
	 * \param fix_dd_exp_param3_fix if depth dependent exponential 3 parameter is kept fixed
	 */
	void KeepDepthDependentExpParam3Fix(bool fix_dd_exp_param3_fix) override;

	/**
	 * \brief
	 * \param camera_matrix
	 * \param distortion
	 * \return
	 */
	bool GetBestFitCvModel(cv::Mat& camera_matrix,
	                       cv::Mat& distortion) const;

	/**
	 * \brief
	 * \param camera_matrix
	 * \param distortion_vector
	 */
	void SetIntrinsicParameters(const cv::Mat& camera_matrix,
	                            const cv::Mat& distortion_vector) override;

protected:

	/**
	 * \brief
	 * \param point
	 * \param if_apply_camera_extrinsics
	 * \return
	 */
	cv::Point2f ProjectPoint(const cv::Point3f& point,
	                         bool if_apply_camera_extrinsics = true) const;

	/**
	 * \brief Reconstructs ray direction in world coordinates (= freestyle device coordinates).
	 * \param points2d
	 * \param out
	 * \param if_normalize
	 */
	void RayReconstruction(cv::InputArray points2d,
	                       cv::OutputArray out,
	                       bool if_normalize = false) const;

	/**
	 * \brief
	 * \param pts2d
	 * \param plane_pos
	 * \param plane_normal
	 * \param intersection_pts_freestyle_cs
	 */
	void GetPlaneIntersectionPoints(cv::InputArray& pts2d,
	                                const cv::Vec3d& plane_pos,
	                                const cv::Vec3d& plane_normal,
	                                cv::OutputArray intersection_pts_freestyle_cs) const;

	/**
	 * \brief
	 * \param rotation_vector_mean
	 * \param translation_vector_mean
	 * \param other
	 */
	void GetInterpolatedRt(cv::Mat& rotation_vector_mean,
	                       cv::Mat& translation_vector_mean,
	                       SingleCameraCalibration& other) const;

	/**
	 * \brief Same as openCV solvePnP but includes the tabled based distortion correction also.
	 * \param object_points
	 * \param image_points
	 * \param rotation_vector
	 * \param translation_vector
	 * \param if_use_extrinsic_guess
	 * \param flags
	 * \return
	 */
	bool SolvePnP(cv::InputArray& object_points,
	              cv::InputArray& image_points,
	              cv::Mat& rotation_vector,
	              cv::Mat& translation_vector,
	              bool if_use_extrinsic_guess = false,
	              int flags                   = 0) const override; //cv::SOLVEPNP_ITERATIVE) const;

	/**
	 * \brief Same as openCV stereoCalibrate but includes the tabled based distortion correction also.
	 * \param object_points
	 * \param img_pts_this
	 * \param img_pts_other
	 * \param other
	 * \param size
	 * \param rotation
	 * \param translation
	 * \param essential_matrix
	 * \param fundamental_matrix
	 * \param criteria
	 * \param flags
	 * \return
	 */
	virtual double StereoCalibrate(cv::InputArray& object_points,
	                               cv::InputArrayOfArrays& img_pts_this,
	                               cv::InputArrayOfArrays& img_pts_other,
	                               std::shared_ptr<SingleCameraCalibration> other,
	                               cv::Size size,
	                               cv::OutputArray rotation,
	                               cv::OutputArray translation,
	                               cv::OutputArray essential_matrix,
	                               cv::OutputArray fundamental_matrix,
	                               cv::TermCriteria criteria = cv::TermCriteria(cv::TermCriteria::COUNT + cv::TermCriteria::EPS, 30, 1e-6),
	                               int flags                 = 0) const; //cv::CALIB_FIX_INTRINSIC);

	/**
	 * \brief Simple wrapper for openCV stereo rectify. Does not use table based correction.
	 * \param other
	 * \param size
	 * \param rotation
	 * \param translation
	 * \param rectification_transform1
	 * \param rectification_transform2
	 * \param projection_matrix1
	 * \param projection_matrix2
	 * \param disparity_to_depth_matrix
	 * \param flags
	 * \param alpha
	 * \param new_image_size
	 * \param valid_pix_roi1
	 * \param valid_pix_roi2
	 */
	void StereoRectify(const std::shared_ptr<const SingleCameraCalibration>& other,
	                   cv::Size size,
	                   cv::InputArray& rotation,
	                   cv::InputArray& translation,
	                   cv::OutputArray rectification_transform1,
	                   cv::OutputArray rectification_transform2,
	                   cv::OutputArray projection_matrix1,
	                   cv::OutputArray projection_matrix2,
	                   cv::OutputArray disparity_to_depth_matrix,
	                   int flags,
	                   double alpha             = -1,
	                   cv::Size new_image_size  = cv::Size(),
	                   cv::Rect* valid_pix_roi1 = nullptr,
	                   cv::Rect* valid_pix_roi2 = nullptr) const;

	/**
	 * \brief
	 * \param buffer
	 */
	void ToBinary(std::vector<char>& buffer) const override;

	/**
	 * \brief
	 * \return
	 */
	ImageChannel GetImageChannel() const override;

	/**
	 * \brief Every camera has a channel ID
	 * \param image_channel
	 */
	void SetImageChannel(ImageChannel image_channel) override;

	/**
	 * \brief
	 * \param other
	 * \param this_px_coords
	 * \param other_px_coords
	 * \param points3d
	 * \param err2d
	 * \return
	 */
	size_t StereoIntersect(std::shared_ptr<const SingleCameraCalibration> other,
	                       const std::vector<cv::Vec2f>& this_px_coords,
	                       const std::vector<cv::Vec2f>& other_px_coords,
	                       std::vector<cv::Vec3d>& points3d,
	                       std::vector<cv::Vec2f>* err2d = nullptr) const;

	/**
	 * \brief inverse of undistortPoints (equivalent does not exist in opencv)
	 * \param points2d
	 * \param out
	 */
	virtual void UndistortToBaseModel(cv::InputArray& points2d,
	                                  cv::OutputArray out) const;

	virtual void DistortToBaseModel(cv::InputArray& points2d,
	                                cv::OutputArray out) const;

	void UndistortPointsInternal(cv::InputArray& points2d,
	                             cv::OutputArray out,
	                             cv::InputArray& rotation   = cv::noArray(),
	                             cv::InputArray& projection = cv::noArray()) const;

	void InitParameterFixations();

public:
	void UndistortPointsLaser(cv::InputArray points_2d,
		cv::OutputArray out,
		cv::InputArray points_3d) const override
	{

	}
protected:
	CameraInfo camera_info_;
};
}
}
