#pragma once

#define CALIBRATION_IDENT 0xFA807EC

struct CalibrationHeader
{
	/**
	 * \brief To test whether header is valid
	 */
	uint32_t magic_number;

	/**
	 * \brief Major version of the calibration
	 */
	uint16_t major_version;

	/**
	* \brief Header size, i.e. static calibration header data starts
	* at this offset relative to CalibrationHeader start.
	*/
	uint32_t static_data_offset;
};
