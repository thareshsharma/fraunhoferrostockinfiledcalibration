#pragma once

#include "laser_projector_sensor.h"

using namespace DMVS::CameraCalibration;

/**
 * \brief This class represents a camera with depth dependent camera sensor.
 * It keeps all the relevant data for camera sensor in one place and provide
 * easy access.
 */
class LaserProjectorNew : public LaserProjectorSensor, std::enable_shared_from_this<LaserProjectorNew>
{
public:
	LaserProjectorNew();

	explicit LaserProjectorNew(const LaserInfo& laser_info);

	/**
	 * \brief Copy constructor makes real copies of matrices
	 */
	LaserProjectorNew(std::shared_ptr<const LaserProjectorNew> rhs);

	/**
	 * \brief Constructs matrix with individual elements, using defaults if necessary
	 */
	LaserProjectorNew(cv::InputArray& camera_matrix,
	                  cv::InputArray& distortion         = cv::noArray(),
	                  cv::InputArray& rotation_matrix    = cv::noArray(),
	                  cv::InputArray& translation_vector = cv::noArray());

	~LaserProjectorNew() override;

	/**
	 * \brief Gets CameraInfo object containing calibration data
	 * \return returns the camera_info object
	 */
	LaserInfo GetLaserInfo() const;

	/**
	 * \brief Initialize values from camera info object
	 */
	void SetLaserInfo(const LaserInfo& laser_info);

	/**
	 * \brief Creates duplicate of the current object
	 * \return shared pointer to CameraSensor object
	 */
	std::shared_ptr<Sensor> Duplicate() const override;

	/**
	 * \brief Project 3d points to obtain 2d points
	 * \param points_3d 3D points which has to be projected
	 * \param points_2d 2D points obtained by projecting input 3D Points
	 * \param apply_camera_extrinsics if extrinsics has to be applied
	 * \param ignore_depth if depth is to be considered or not
	 */
	void ProjectPoints(cv::InputArray& points_3d,
	                   cv::OutputArray points_2d,
	                   bool apply_camera_extrinsics = true,
	                   bool ignore_depth            = true) const override;

	/**
	 * \brief Undistort the input points 2d
	 * \param points_3d 3D points which has to be projected
	 * \param points_2d 2D points obtained by projecting input 3D Points
	 * \param undistorted_points_2d
	 */
	void UndistortPoints(cv::InputArray points_2d,
	                     cv::OutputArray undistorted_points_2d,
	                     cv::InputArray points_3d) const override;

	/**
	 * \brief Checks if camera sensor supports depth values
	 * \return if the depth values are being considered
	 */
	bool HandlesDepthValues() const override;

protected:
	/**
	* \brief
	* \param source
	* \param pts3d
	* \param destination
	* \param camera_matrix
	* \param distortion_coefficients
	*/
	virtual void UndistortPointsDistanceDependent(const cv::Mat& source,
	                                              const cv::Mat& pts3d,
	                                              const cv::Mat& destination,
	                                              const cv::Mat& camera_matrix,
	                                              const cv::Mat& distortion_coefficients) const;
	/**
	 * \brief
	 * \param points3d
	 * \param rotation_matrix
	 * \param translation_vector
	 * \param points2d
	 * \param ignore_depth
	 */
	virtual void ProjectPointsInternal(cv::InputArray points3d,
	                                   cv::Mat rotation_matrix,
	                                   cv::Mat translation_vector,
	                                   cv::OutputArray points2d,
	                                   bool ignore_depth = false) const;

	LaserInfo laser_info_;
};
