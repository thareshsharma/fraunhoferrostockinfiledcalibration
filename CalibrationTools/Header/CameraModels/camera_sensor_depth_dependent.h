#pragma once

#include "camera_sensor.h"
#include "camera_info.h"

using namespace DMVS::CameraCalibration;

/**
 * \brief This class represents a camera with depth dependent camera sensor.
 * It keeps all the relevant data for camera sensor in one place and provide
 * easy access.
 */
class CameraSensorDepthDependent : public CameraSensor
{
public:
	CameraSensorDepthDependent();

	explicit CameraSensorDepthDependent(const CameraInfo& camera_info);

	explicit CameraSensorDepthDependent(const LaserInfo& laser_info);

	/**
	 * \brief Copy constructor makes real copies of matrices
	 */
	CameraSensorDepthDependent(std::shared_ptr<const CameraSensorDepthDependent> rhs);

	/**
	 * \brief Constructs matrix with individual elements, using defaults if necessary
	 */
	CameraSensorDepthDependent(const std::string& name,
	                           ImageChannel channel,
	                           const cv::Vec2d& depth_params,
	                           const cv::Vec3d& depth_params_exp,
	                           cv::InputArray& camera_matrix,
	                           cv::InputArray& distortion         = cv::noArray(),
	                           cv::InputArray& rotation_matrix    = cv::noArray(),
	                           cv::InputArray& translation_vector = cv::noArray());

	~CameraSensorDepthDependent() override;

	/**
	 * \brief Converts the parameter vector to camera info object
	 * \param params
	 * \param camera_info
	 * \return
	 */
	static bool ParamVectorToCameraInfo(const std::vector<double>& params,
	                                    CameraInfo& camera_info);

	/**
	 * \brief Converts the CameraInfo object to parameter vector
	 * \param camera_info
	 * \param params
	 * \return
	 */
	static bool CameraInfoToParamVector(const CameraInfo& camera_info,
	                                    std::vector<double>& params);

	/**
	 * \brief Converts the CameraInfo object to parameter vector
	 * \param laser_info
	 * \param params
	 * \return
	 */
	static bool LaserInfoToParamVector(const LaserInfo& laser_info,
	                                   std::vector<double>& params);

	/**
	 * \brief Gets CameraInfo object containing calibration data
	 * \return returns the camera_info object
	 */
	CameraInfo GetCameraInfo() const;

	/**
	 * \brief Initialize values from camera info object
	 */
	void SetCameraInfo(const CameraInfo& camera_info);

	/**
	 * \brief Gets the current camera intrinsic parameter namely camera matrix
	 * and distortion vector
	 * \param camera_matrix Camera intrinsic matrix
	 * \param distortion_vector Distortion vector containing distortion values
	 */
	void GetCameraIntrinsicParams(cv::Mat& camera_matrix,
	                              cv::Mat& distortion_vector) const override;

	/**
	 * \brief Sets the current camera intrinsic parameter namely camera matrix
	 * and distortion vector
	 * \param intrinsic_matrix Camera intrinsic matrix
	 * \param distortion_vector Distortion vector containing distortion values
	 */
	void SetIntrinsicParameters(const cv::Mat& intrinsic_matrix,
	                            const cv::Mat& distortion_vector) override;

	/**
	 * \brief Initializes from param vector
	 * \param params parameter vector containing all the camera specific parameters
	 * \return
	 */
	size_t InitFromParamsVector(const std::vector<double>& params) override;

	/**
	 * \brief Get parameter vector
	 * \return
	 */
	std::vector<double> GetParamsVector() const override;

	/**
	 * \brief
	 * \return
	 */
	std::vector<size_t> GetFixations() const override;

	/**
	 * \brief Creates duplicate of the current object
	 * \return shared pointer to CameraSensor object
	 */
	std::shared_ptr<CameraSensor> Duplicate() const override;

	/**
	 * \brief Creates duplicate of the current object
	 * \return shared pointer to CameraSensor object
	*/
	std::shared_ptr<CameraSensor> DuplicateLaser() const override;

	/**
	 * \brief Gets image channel of the current object.
	 * \return
	 */
	ImageChannel GetImageChannel() const override;

	/**
	 * \brief Set the image channel of the camera sensor
	 * \param image_channel
	 */
	void SetImageChannel(ImageChannel image_channel) override;

	/**
	 * \brief Project 3d points to obtain 2d points
	 * \param points_3d 3D points which has to be projected
	 * \param points_2d 2D points obtained by projecting input 3D Points
	 * \param apply_camera_extrinsics if extrinsics has to be applied
	 * \param ignore_depth if depth is to be considered or not
	 */
	void ProjectPoints(cv::InputArray points_3d,
	                   cv::OutputArray points_2d,
	                   bool apply_camera_extrinsics,
	                   bool ignore_depth) const override;

	/**
	 * \brief Project 3d points to obtain 2d points
	 * \param points_3d 3D points which has to be projected
	 * \param points_2d 2D points obtained by projecting input 3D Points
	 * \param apply_camera_extrinsics if extrinsics has to be applied
	 * \param ignore_depth if depth is to be considered or not
	 */
	void LaserProjectPoints(cv::InputArray& points_3d,
	                        cv::OutputArray points_2d,
	                        bool apply_camera_extrinsics = true,
	                        bool ignore_depth            = true) const override;

	/**
	 * \brief Undistort the input points 2d
	 * \param points_3d 3D points which has to be projected
	 * \param points_2d 2D points obtained by projecting input 3D Points
	 * \param undistorted_points_2d
	 */
	void UndistortPoints2(cv::InputArray points_2d,
	                      cv::OutputArray undistorted_points_2d,
	                      cv::InputArray points_3d) const override;

	void UndistortPointsLaser(cv::InputArray points_2d,
	                          cv::OutputArray undistorted_points_2d,
	                          cv::InputArray points_3d) const override;

	/**
	 * \brief Checks if camera sensor supports depth values
	 * \return if the depth values are being considered
	 */
	bool HandlesDepthValues() const override;

	/**
	 * \brief Ensures that the extrinsic parameters are kept fixed during camera calibration process
	 * \param fix_extrinsic_parameters
	 */
	void KeepExtrinsicParametersFix(bool fix_extrinsic_parameters) override;

	/**
	 * \brief Ensures that the distortion parameters are kept fixed during camera calibration process
	 * \param fix_distortion if distortion has to be fixed
	 */
	void KeepDistortionFix(bool fix_distortion) override;

	/**
	 * \brief Ensures that the focal length is kept same during camera calibration process
	 * \param fix_same_focal_length if same focal length has to be fixed
	 */
	void KeepSameFocalLengthFix(bool fix_same_focal_length) override;

	/**
	 * \brief Ensures that the focal length is kept fixed during camera calibration process
	 * \param fix_focal_length if focal length has to be fixed
	 */
	void KeepFocalLengthFix(bool fix_focal_length) override;

	/**
	 * \brief Ensures that the depth dependent parameters scalar 1 is fixed
	 * \param fix_dd_scalar_param1_fix if depth dependent scalar 1 parameter is kept fixed
	 */
	void KeepDepthDependentScalarParam1Fix(bool fix_dd_scalar_param1_fix) override;

	/**
	 * \brief Ensures that the depth dependent parameters scalar 2 is fixed
	 * \param fix_dd_scalar_param2_fix if depth dependent scalar 2 parameter is kept fixed
	 */
	void KeepDepthDependentScalarParam2Fix(bool fix_dd_scalar_param2_fix) override;

	/**
	 * \brief Ensures that the depth dependent parameters exponential 1 is fixed
	 * \param fix_dd_exp_param1_fix if depth dependent exponential 1 parameter is kept fixed
	 */
	void KeepDepthDependentExpParam1Fix(bool fix_dd_exp_param1_fix) override;

	/**
	 * \brief Ensures that the depth dependent parameters exponential 2 is fixed
	 * \param fix_dd_exp_param2_fix if depth dependent exponential 2 parameter is kept fixed
	 */
	void KeepDepthDependentExpParam2Fix(bool fix_dd_exp_param2_fix) override;

	/**
	 * \brief Ensures that the depth dependent parameters exponential 3 is fixed
	 * \param fix_dd_exp_param3_fix if depth dependent exponential 3 parameter is kept fixed
	 */
	void KeepDepthDependentExpParam3Fix(bool fix_dd_exp_param3_fix) override;

	/**
	 * \brief Gets the best open CV model
	 * \param camera_matrix Camera intrinsic matrix
	 * \param distortion_vector Distortion vector containing distortion values
	 * \return
	 */
	bool GetBestFitCVModel(cv::Mat& camera_matrix,
	                       cv::Mat& distortion_vector) const override;

	/**
	 * \brief Gets the best open CV model
	 * \param intrinsic_matrix Camera intrinsic matrix
	 * \param distortion_vector Distortion vector containing distortion values
	 * \return
	 */
	bool GetBestFitCVModelLaser(cv::Mat& intrinsic_matrix,
	                            cv::Mat& distortion_vector) const override;
	/**
	 * \brief Converts the buffer to binary storage
	 * \param buffer buffer which needs to be converted
	 */
	void ToBinary(std::vector<char>& buffer) const override;

	/**
	 * \brief Converts the binary storage to Camera sensor object
	 * \param pos
	 * \return
	 */
	static std::shared_ptr<const CameraSensor> FromBinary(std::vector<char>::const_iterator& pos);

protected:

	/**
	 * \brief
	 * \param source
	 * \param pts3d
	 * \param destination
	 * \param camera_matrix
	 * \param distortion_coefficients
	 */
	virtual void UndistortPointsDistanceDependent(const CvMat* source,
	                                              const CvMat* pts3d,
	                                              const CvMat* destination,
	                                              const CvMat* camera_matrix,
	                                              const CvMat* distortion_coefficients) const;

	/**
	* \brief
	* \param source
	* \param pts3d
	* \param destination
	* \param camera_matrix
	* \param distortion_coefficients
	*/
	virtual void UndistortPointsDistanceDependent2(const cv::Mat& source,
	                                               const cv::Mat& pts3d,
	                                               const cv::Mat& destination,
	                                               const cv::Mat& camera_matrix,
	                                               const cv::Mat& distortion_coefficients) const;

	void UndistortPointsDistanceDependentLaser(const cv::Mat& source,
	                                           const cv::Mat& pts3d,
	                                           const cv::Mat& destination,
	                                           const cv::Mat& laser_matrix,
	                                           const cv::Mat& distortion_coefficients) const;

	/**
	 * \brief
	 * \param points3d
	 * \param rotation_matrix
	 * \param translation_vector
	 * \param points2d
	 * \param ignore_depth
	 */
	virtual void ProjectPointsInternal(cv::InputArray points3d,
	                                   const cv::Mat& rotation_matrix,
	                                   const cv::Mat& translation_vector,
	                                   cv::OutputArray points2d,
	                                   bool ignore_depth = false) const;

	virtual void ProjectPointsInternalLaser(cv::InputArray points3d,
	                                        const cv::Mat& rotation_matrix,
	                                        const cv::Mat& translation_vector,
	                                        cv::OutputArray points2d) const;

	CameraInfo camera_info_;
	LaserInfo laser_info_;
};
