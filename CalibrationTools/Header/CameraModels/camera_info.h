#pragma once

#include "constants.h"
#include "camera_intrinsic_parameters.h"
#include "camera_extrinsic_parameters.h"

using namespace DMVS::CameraCalibration;

struct CameraInfo
{
	CameraInfo() :
		name("no_name"),
		channel(CHANNEL_DEFAULT),
		width(0),
		height(0)
	{
	}

	CameraInfo(std::string name,
	           const ImageChannel camera_channel) :
		name(std::move(name)),
		channel(camera_channel),
		width(0),
		height(0)
	{
	}

	std::string name;
	ImageChannel channel;
	size_t width;
	size_t height;
	CameraIntrinsicParameters intrinsics;
	CameraExtrinsicParameters extrinsics;
};
