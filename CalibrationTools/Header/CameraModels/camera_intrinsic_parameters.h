#pragma once

#include "camera_depth_parameters.h"

/**
 * \brief Container for intrinsic parameters of a camera
 */
struct CameraIntrinsicParameters
{
	CameraIntrinsicParameters() :
		camera_intrinsic_matrix(cv::Mat(3, 3, CV_64F)),
		distortion_vector(cv::Mat(5, 1, CV_64F))
	{
	}

	cv::Vec2d GetFocalLength();

	cv::Vec2d GetPrincipalPoint();

	cv::Mat camera_intrinsic_matrix;

	// Order k1, k2, p1, p2, k3
	cv::Mat distortion_vector;

	CameraDepthParameters depth_parameters;
};

inline cv::Vec2d CameraIntrinsicParameters::GetFocalLength()
{
	return cv::Vec2d(camera_intrinsic_matrix.at<double>(0, 0), camera_intrinsic_matrix.at<double>(1, 1));
}

inline cv::Vec2d CameraIntrinsicParameters::GetPrincipalPoint()
{
	return cv::Vec2d(camera_intrinsic_matrix.at<double>(0, 2), camera_intrinsic_matrix.at<double>(1, 2));
}
