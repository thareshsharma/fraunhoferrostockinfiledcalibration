#pragma once

#include "typedefs.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief Base class for basic sensor modeling.
 */
class Sensor : std::enable_shared_from_this<Sensor>
{
public:
	Sensor();

	explicit Sensor(const cv::Mat4d& transformation_matrix,
	                ImageChannel image_channel);

	Sensor(const cv::Mat& rotation_matrix,
	       const cv::Mat& translation_vector,
	       ImageChannel image_channel);

	Sensor(const Sensor& rhs);

	Sensor& operator=(const Sensor& rhs);

	Sensor(Sensor&& rhs) noexcept;

	Sensor& operator=(Sensor&& rhs) noexcept;

	virtual ~Sensor();

	/**
	 * \brief Creates an duplicate of the current object
	 */
	virtual std::shared_ptr<Sensor> Duplicate() const = 0;

	/**
	 * \brief Projects a vector of 3d points (world coordinates) onto 2d
	 * image coordinates of the sensor.
	 * \param points_3d 3D points to be projected either in world coordinates
	 * system or in sensor coordinates (see apply_camera_extrinsics)
	 * \param points_2d 2D points to be projected either in world coordinates
	 * system or in sensor coordinates (see apply_camera_extrinsics)
	 * \param apply_camera_extrinsics true: points_3d given in world coordinates system, own R and T applied
	 *								  false: points_3d given in sensor system, own R and T ignored
	 * \param ignore_depth flag for ignoring the depth
	 */
	virtual void ProjectPoints(cv::InputArray points_3d,
	                           cv::OutputArray points_2d,
	                           bool apply_camera_extrinsics = true,
	                           bool ignore_depth            = false) const = 0;

	/**
	* \brief Undistort image coordinates to pinhole coordinates with focal length 1m
	* \param points_2d Image coordinates to be undistorted to pinhole focal length 1m coordinates
	* \param out undistorted 2d pinhole focal length 1 coordinates
	* \param points_3d approximated 3d position for depth dependent lens corrections
	*/
	virtual void UndistortPoints(cv::InputArray points_2d,
	                             cv::OutputArray out,
	                             cv::InputArray points_3d = cv::noArray()) const = 0;

	/**
	 * \brief Indicates whether undistort points can handle the points3d input
	 * \return true : Handles depth dependent model
	 *		   false: Does'nt handles depth dependent model
	 */
	virtual bool HandlesDepthValues() const = 0;

	ImageChannel GetImageChannel() const;

	/**
	 * \brief Returns current extrinsic parameters of the sensor as cv::Mat4d transformation.
	 * The transformation contains rotation matrix(3x3) and translation vector(3x1)
	 */
	cv::Mat4d GetTransformationMatrix() const;

	/**
	 * \brief Sets the transformation matrix. The transformation matrix contains
	 * rotation matrix(3x3) and translation vector(3x1)
	 */
	void SetTransformationMatrix(const cv::Mat4d& transformation_matrix);

	/**
	 * \brief Returns current extrinsic parameters of the sensor.
	 * \param rotation_matrix
	 * \param translation_vector
	 */
	void GetExtrinsicParameters(cv::Mat& rotation_matrix,
	                            cv::Mat& translation_vector) const;

	/**
	 * \brief Sets current extrinsic parameters of the sensor.
	 * \param rotation_matrix 3x3 rotation matrix (no 3x1 Rodrigues vector)
	 * \param translation_vector
	 */
	void SetExtrinsicParameters(const cv::Mat& rotation_matrix,
	                            const cv::Mat& translation_vector);

	/**
	 * \brief Same as openCV solvePnP but applies own un-distort model before.
	 * \param object_points Point of the object
	 * \param image_points Points in the image
	 * \param rotation_vector Rotation vector
	 * \param translation_vector Translation Vector
	 * \param if_use_extrinsic_guess Flag for using extrinsic guess
	 * \param flags SolvePnP method
	 * \return
	 */
	virtual bool SolvePnP(cv::InputArray& object_points,
	                      cv::InputArray& image_points,
	                      cv::Mat& rotation_vector,
	                      cv::Mat& translation_vector,
	                      bool if_use_extrinsic_guess = false,
	                      int flags                   = cv::SOLVEPNP_ITERATIVE) const;

	/**
	 * \brief Gets intersection point of rays defined by two image coordinates
	 * \param other The other sensor for which intersection has to be obtained
	 * \param this_px_coords Pixel coordinates of the calling sensor
	 * \param other_px_coords Pixel coordinates of the other sensor
	 * \param points_3d
	 * \param error_2d
	 * \param ignore_depth If the depth has to be ignored
	 * \return
	 */
	size_t StereoIntersect(std::shared_ptr<const Sensor> other,
	                       const std::vector<cv::Vec2f>& this_px_coords,
	                       const std::vector<cv::Vec2f>& other_px_coords,
	                       std::vector<cv::Vec3d>& points_3d,
	                       std::vector<cv::Vec2f>* error_2d = nullptr,
	                       bool ignore_depth                = false) const;

	/**
	 * \brief Reconstructs ray direction in world coordinate system.
	 * \param points_2d
	 * \param out
	 * \param normalize
	 * \param points3d
	 */
	virtual void RayReconstructionWCS(cv::InputArray points_2d,
	                                  cv::OutputArray out,
	                                  bool normalize          = false,
	                                  cv::InputArray points3d = cv::noArray()) const = 0;

	/**
	 * \brief Reconstructs ray direction in sensor coordinate system.
	 * \param points_2d
	 * \param out
	 * \param normalize
	 * \param points3d
	 */
	virtual void RayReconstructionSCS(cv::InputArray points_2d,
	                                  cv::OutputArray out,
	                                  bool normalize          = false,
	                                  cv::InputArray points3d = cv::noArray()) const = 0;


	/**
	 * \brief
	 * \param points_2d
	 * \param plane_position
	 * \param plane_normal
	 * \param intersection_points_coordinate_system
	 */
	virtual void GetPlaneIntersectionPoints(cv::InputArray& points_2d,
	                                        const cv::Vec3d& plane_position,
	                                        const cv::Vec3d& plane_normal,
	                                        cv::OutputArray intersection_points_coordinate_system) const;

	/**
	 * \brief Transform a 3D point from sensor coordinates to world coordinate system
	 * \param point 3D point<float> which need to be transformed
	 * \return transformed point<float>
	 */
	cv::Point3f FromSensorToWorldCoordinateSystem(const cv::Point3f& point) const;

	/**
	 * \brief Transform a 3D point from sensor coordinates to world coordinate system
	 * \param point 3D point<float> which need to be transformed
	 * \param rotation_matrix
	 * \param translation_vector
	 * \return
	 */
	cv::Point3f FromSensorToWorldCoordinateSystem(const cv::Point3f& point,
	                                              const cv::Mat& rotation_matrix,
	                                              const cv::Mat& translation_vector) const;

	/**
	 * \brief Transform a 3D point from sensor coordinates to world coordinate system
	 * \param point 3D point<double> which need to be transformed
	 * \return transformed point<double>
	 */
	cv::Point3d FromSensorToWorldCoordinateSystem(const cv::Point3d& point) const;

	/**
	 * \brief Transform an eigen vector from sensor coordinates to world coordinate system
	 * \param eigen_vector eigen vector<float> which need to be transformed
	 * \return transformed eigen vector<float>
	 */
	Eigen::Vector3f FromSensorToWorldCoordinateSystem(const Eigen::Vector3f& eigen_vector) const;

	/**
	 * \brief Transform an eigen Matrix from sensor coordinates to world coordinate system
	 * \param eigen_matrix eigen matrix<float> which need to be transformed
	 * \return transformed eigen matrix<float>
	 */
	Eigen::Matrix3f FromSensorToWorldCoordinateSystem(const Eigen::Matrix3f& eigen_matrix) const;

	/**
	 * \brief Transform an eigen vector from sensor coordinates to world coordinate system
	 * \param eigen_vector eigen vector<double> which need to be transformed
	 * \return transformed eigen_vector<double>
	 */
	Eigen::Vector3d FromSensorToWorldCoordinateSystem(const Eigen::Vector3d& eigen_vector) const;

	/**
	 * \brief Transform an eigen matrix from sensor coordinates to world coordinate system
	 * \param eigen_matrix eigen matrix<double> which need to be transformed
	 * \return transformed eigen matrix<double>
	 */
	Eigen::Matrix3d FromSensorToWorldCoordinateSystem(const Eigen::Matrix3d& eigen_matrix) const;

	/**
	 * \brief Transform a 3D point<float> from world coordinate system to sensor coordinates
	 * \param point point<float> which need to be transformed
	 * \return transformed point<float>
	 */
	cv::Point3f FromWorldCoordinateSystemToSensor(const cv::Point3f& point) const;

	/**
	 * \brief Transform a 3D point from world coordinate system to sensor coordinates
	 * \param point point<double> which need to be transformed
	 * \return transformed point<double>
	 */
	cv::Point3d FromWorldCoordinateSystemToSensor(const cv::Point3d& point) const;

	/**
	 * \brief Transform an eigen vector from world coordinate system to sensor coordinates
	 * \param eigen_vector eigen vector<float> which need to be transformed
	 * \return transformed eigen vector<float>
	 */
	Eigen::Vector3f FromWorldCoordinateSystemToSensor(const Eigen::Vector3f& eigen_vector) const;

	/**
	 * \brief Transform an eigen matrix from world coordinate system to sensor coordinates
	 * \param eigen_matrix eigen matrix<float> which need to be transformed
	 * \return transformed eigen matrix<float>
	 */
	Eigen::Matrix3f FromWorldCoordinateSystemToSensor(const Eigen::Matrix3f& eigen_matrix) const;

	/**
	 * \brief Transform an eigen vector from world coordinate system to sensor coordinates
	 * \param eigen_vector eigen vector<double> which need to be transformed
	 * \return transformed eigen vector<double>
	 */
	Eigen::Vector3d FromWorldCoordinateSystemToSensor(const Eigen::Vector3d& eigen_vector) const;

	/**
	 * \brief Transform a eigen matrix from world coordinate system to sensor coordinates
	 * \param eigen_matrix eigen matrix<double> which need to be transformed
	 * \return transformed eigen matrix<double>
	 */
	Eigen::Matrix3d FromWorldCoordinateSystemToSensor(const Eigen::Matrix3d& eigen_matrix) const;

protected:

	/**
	 * \brief Method for keeping the transformation matrix synchronized
	 * \param rotation_matrix
	 * \param translation_vector
	 */
	void SyncTransformationMatrix();

	/**
	 * \brief Method for keeping the extrinsic parameters synchronized
	 * \param transformation_matrix
	 */
	void SyncExtrinsicParameters();

	/**
	 * \brief Convenience function for simpler access to the ProjectPoints method
	 * \param point_3d 3D point to be projected either in world  system coordinates
	 * or in sensor coordinates
	 * \param apply_camera_extrinsics true: points_3d given in world system, own R and T applied
	 *                                false: points_3d given in sensor system, own R and T ignored
	 * \return
	 */
	cv::Vec2d ProjectPoint(const cv::Vec3d& point_3d,
	                       bool apply_camera_extrinsics = true) const;

	/**
	 * \brief Convenience function for simpler access to the UndistortPoints method
	 * Set point_depth to cv::Vec3d(0,0,0) for ignoring the depth correction
	 * \param point_pixel
	 * \param point_depth
	 * \return
	 */
	cv::Vec2d UndistortPoint(const cv::Vec2d& point_pixel,
	                         const cv::Vec3d& point_depth = cv::Vec3d(0, 0, 0)) const;

	template <typename T>
	T GetValue(cv::Mat translated_vector,
	           const int column) const
	{
		return T(rotation_matrix_.col(column).at<double>(0) * translated_vector.at<double>(0) +
			rotation_matrix_.col(column).at<double>(1) * translated_vector.at<double>(1) +
			rotation_matrix_.col(column).at<double>(2) * translated_vector.at<double>(2));
	}

	template <typename T>
	T GetValue(cv::Mat translated_vector,
	           const cv::Mat& rotation_matrix,
	           const int column) const
	{
		return T(rotation_matrix.col(column).at<double>(0) * translated_vector.at<double>(0) +
			rotation_matrix.col(column).at<double>(1) * translated_vector.at<double>(1) +
			rotation_matrix.col(column).at<double>(2) * translated_vector.at<double>(2));
	}

	// Extrinsic parameters
	cv::Mat rotation_matrix_;
	cv::Mat translation_vector_;

	// Transformation
	cv::Mat4d transformation_matrix_;

	// Image channel
	ImageChannel image_channel_;
};
}
}
