#pragma once

#include "laser_info.h"
#include "sensor.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief Base calibration for all camera calibration models.
 * Keeps all the relevant data for single camera sensor.
 */
class LaserProjectorSensor : public Sensor
{
public:
	LaserProjectorSensor();

	explicit LaserProjectorSensor(const LaserInfo& laser_info);

	explicit LaserProjectorSensor(const cv::Mat4d& transformation_matrix);

	LaserProjectorSensor(const cv::Mat& rotation_matrix,
	                     const cv::Mat& translation_vector,
	                     ImageChannel image_channel);

	LaserProjectorSensor(const LaserProjectorSensor& rhs);

	LaserProjectorSensor& operator=(const LaserProjectorSensor& rhs);

	LaserProjectorSensor(LaserProjectorSensor&& rhs) noexcept;

	LaserProjectorSensor& operator=(LaserProjectorSensor&& rhs) noexcept;

	virtual ~LaserProjectorSensor();

	/**
	 * \brief Creates an duplicate of the current object
	 */
	virtual std::shared_ptr<Sensor> Duplicate() const = 0;

	/**
	 * \brief Projects a vector of 3d points in world coordinates into 2d
	 * image coordinates of the camera.
	 * \param points_3d 3D points to be projected either in world system
	 * coordinates or in camera coordinates (see apply_camera_extrinsics)
	 * \param points_2d 2D points to be projected either in world system
	 * coordinates or in camera coordinates (see apply_camera_extrinsics)
	 * \param apply_camera_extrinsics true: points_3d given in world system, own R and T applied
	 *								  false: points_3d given in camera system, own R and T ignored
	 * \param ignore_depth flag for ignoring the depth
	 */
	virtual void ProjectPoints(cv::InputArray points_3d,
	                           cv::OutputArray points_2d,
	                           bool apply_camera_extrinsics = true,
	                           bool ignore_depth            = false) const = 0;


	virtual void UndistortPoints(cv::InputArray points_2d,
	                             cv::OutputArray out,
	                             cv::InputArray points_3d = cv::noArray()) const = 0;

	bool HandlesDepthValues() const = 0;

	void RayReconstructionWCS(cv::InputArray points_2d,
	                          cv::OutputArray out,
	                          bool normalize,
	                          cv::InputArray points3d = cv::noArray()) const override;

	/**
	 * \brief Reconstructs ray direction in world coordinates.
	 * \param points_2d
	 * \param out
	 * \param normalize
	 * \param points3d
	 */
	void RayReconstructionSCS(cv::InputArray points_2d,
	                          cv::OutputArray out,
	                          bool normalize,
	                          cv::InputArray points3d = cv::noArray()) const override;

	/**
	 * \brief
	 * \param points_2d
	 * \param plane_position
	 * \param plane_normal
	 * \param intersection_points_coordinate_system
	 */
	virtual void GetPlaneIntersectionPoints(cv::InputArray& points_2d,
	                                        const cv::Vec3d& plane_position,
	                                        const cv::Vec3d& plane_normal,
	                                        cv::OutputArray intersection_points_coordinate_system) const;
};
}
}
