#pragma once

#include "camera_info.h"
#include "laser_info.h"

namespace DMVS
{
namespace CameraCalibration
{
class Transformation;
/**
 * \brief Base calibration for all camera calibration models.
 * Keeps all the relevant data for single camera sensor.
 */
class CameraSensor : std::enable_shared_from_this<CameraSensor>
{
public:
	CameraSensor();

	explicit CameraSensor(const CameraInfo& camera_info);

	explicit CameraSensor(const LaserInfo& laser_info);

	explicit CameraSensor(const cv::Mat& transformation_matrix);

	CameraSensor(const cv::Mat& rotation_matrix,
	             const cv::Mat& translation_vector,
	             ImageChannel image_channel);

	CameraSensor(const CameraSensor& rhs);

	CameraSensor& operator=(const CameraSensor& rhs);

	CameraSensor(CameraSensor&& rhs) noexcept;

	CameraSensor& operator=(CameraSensor&& rhs) noexcept;

	virtual ~CameraSensor();

	template <typename T>
	T GetValue(cv::Mat translated_back_vector,
	           const size_t id_0,
	           const size_t id_1,
	           const size_t id_2) const
	{
		return T(rotation_matrix_.at<double>(int(id_0)) * translated_back_vector.at<double>(0) +
			rotation_matrix_.at<double>(int(id_1)) * translated_back_vector.at<double>(1) +
			rotation_matrix_.at<double>(int(id_2)) * translated_back_vector.at<double>(2));
	}

	template <typename T>
	T GetValue(cv::Mat translated_back_vector,
	           const size_t id_0,
	           const size_t id_1,
	           const size_t id_2,
	           cv::Mat rotation_matrix) const
	{
		return T(rotation_matrix.at<double>(int(id_0)) * translated_back_vector.at<double>(0) +
			rotation_matrix.at<double>(int(id_1)) * translated_back_vector.at<double>(1) +
			rotation_matrix.at<double>(int(id_2)) * translated_back_vector.at<double>(2));
	}

	/**
	 * \brief Initialize object by double parameter set
	 * \param params vector of parameters
	 * \return size size
	 */
	virtual size_t InitFromParamsVector(const std::vector<double>& params) = 0;

	/**
	 * \brief Any implementation needs a representation by a double vector.
	 * This representation is used within the calibration fit at calibration time.
	 * Thus the representation should be parameter minimal and carefully chosen.
	 * \return current parameter set of object
	 */
	virtual std::vector<double> GetParamsVector() const = 0;

	/**
	 * \brief Get the fixation vector which tells which parameters have to be kept fixed
	 * for calibration.
	 * fixation[i] == i : parameter free and used for optimization
	 * fixation[i] == max(size_t) : parameter fixed, do not change
	 * fixation[i] <  j with  j < i : parameter i same as parameter j
	 * \return vector representing fixations
	 */
	virtual std::vector<size_t> GetFixations() const = 0;

	/**
	 * \brief Creates an duplicate of the current object
	 */
	virtual std::shared_ptr<CameraSensor> Duplicate() const = 0;

	/**
	 * \brief Creates an duplicate of the current object
	 */
	virtual std::shared_ptr<CameraSensor> DuplicateLaser() const = 0;

	/**
	 * \brief Projects a vector of 3d points in superordinate coordinates into 2d
	 * image coordinates of the camera.
	 * \param points_3d 3D points to be projected either in superordinate system
	 * coordinates or in camera coordinates (see apply_camera_extrinsics)
	 * \param points_2d 2D points to be projected either in superordinate system
	 * coordinates or in camera coordinates (see apply_camera_extrinsics)
	 * \param apply_camera_extrinsics true: points_3d given in superordinate system, own R and T applied
	 *								  false: points_3d given in camera system, own R and T ignored
	 * \param ignore_depth flag for ignoring the depth
	 */
	virtual void ProjectPoints(cv::InputArray points_3d,
	                           cv::OutputArray points_2d,
	                           bool apply_camera_extrinsics = true,
	                           bool ignore_depth            = false) const = 0;

	virtual void LaserProjectPoints(cv::InputArray& points_3d,
	                                cv::OutputArray points_2d,
	                                bool apply_camera_extrinsics = true,
	                                bool ignore_depth            = true) const = 0;

	/**
	 * \brief Undistort image coordinates to pinhole coordinates with focal length 1m
	 * \param points_2d Image coordinates to be undistorted to pinhole focal length 1m coordinates
	 * \param out undistorted 2d pinhole focal length 1 coordinates
	 * \param points_3d approximated 3d position for depth dependent lens corrections
	 */
	//virtual void UndistortPoints(cv::InputArray points_2d,
	//                             cv::OutputArray out,
	//                             cv::InputArray points_3d = cv::noArray()) const = 0;

	/**
	* \brief Undistort image coordinates to pinhole coordinates with focal length 1m
	* \param points_2d Image coordinates to be undistorted to pinhole focal length 1m coordinates
	* \param out undistorted 2d pinhole focal length 1 coordinates
	* \param points_3d approximated 3d position for depth dependent lens corrections
	*/
	virtual void UndistortPoints2(cv::InputArray points_2d,
	                              cv::OutputArray out,
	                              cv::InputArray points_3d = cv::noArray()) const = 0;

	/**
	* \brief Undistort image coordinates to pinhole coordinates with focal length 1m
	* \param points_2d Image coordinates to be undistorted to pinhole focal length 1m coordinates
	* \param out undistorted 2d pinhole focal length 1 coordinates
	* \param points_3d approximated 3d position for depth dependent lens corrections
	*/
	virtual void UndistortPointsLaser(cv::InputArray points_2d,
	                                  cv::OutputArray out,
	                                  cv::InputArray points_3d = cv::noArray()) const = 0;

	/**
	 * \brief Indicates whether straighten points can handle the points3d input
	 * \return true: Camera Sensor has depth dependent camera model
	 *		   false: no depth dependent camera model
	 */
	virtual bool HandlesDepthValues() const = 0;

	/**
	 * \brief Fixes extrinsic parameters
	 * \param fix_extrinsic_parameters Flag to fix extrinsic parameters
	 */
	virtual void KeepExtrinsicParametersFix(bool fix_extrinsic_parameters) = 0;

	/**
	 * \brief Fixes distortion
	 * \param fix_distortion Flag to fix distortion
	 */
	virtual void KeepDistortionFix(bool fix_distortion) = 0;

	/**
	 * \brief Fix same focal length
	 * \param fix_same_focal_length Flag to fix same focal length
	 */
	virtual void KeepSameFocalLengthFix(bool fix_same_focal_length) = 0;

	/**
	 * \brief Ensures that the focal length is kept fixed during camera calibration process
	 * \param fix_focal_length if focal length has to be fixed
	 */
	virtual void KeepFocalLengthFix(bool fix_focal_length) = 0;

	/**
	* \brief Ensures that the depth dependent parameters scalar 1 is fixed
	* \param fix_dd_scalar_param1_fix if depth dependent scalar 1 parameter is kept fixed
	*/
	virtual void KeepDepthDependentScalarParam1Fix(bool fix_dd_scalar_param1_fix) =0;

	/**
	 * \brief Ensures that the depth dependent parameters scalar 2 is fixed
	 * \param fix_dd_scalar_param2_fix if depth dependent scalar 2 parameter is kept fixed
	 */
	virtual void KeepDepthDependentScalarParam2Fix(bool fix_dd_scalar_param2_fix) = 0;

	/**
	 * \brief Ensures that the depth dependent parameters exponential 1 is fixed
	 * \param fix_dd_exp_param1_fix if depth dependent exponential 1 parameter is kept fixed
	 */
	virtual void KeepDepthDependentExpParam1Fix(bool fix_dd_exp_param1_fix) = 0;

	/**
	 * \brief Ensures that the depth dependent parameters exponential 2 is fixed
	 * \param fix_dd_exp_param2_fix if depth dependent exponential 2 parameter is kept fixed
	 */
	virtual void KeepDepthDependentExpParam2Fix(bool fix_dd_exp_param2_fix) = 0;

	/**
	 * \brief Ensures that the depth dependent parameters exponential 3 is fixed
	 * \param fix_dd_exp_param3_fix if depth dependent exponential 3 parameter is kept fixed
	 */
	virtual void KeepDepthDependentExpParam3Fix(bool fix_dd_exp_param3_fix) = 0;

	/**
	 * \brief Gets the current camera intrinsic parameter namely camera matrix
	 * and distortion vector
	 * \param camera_matrix Camera intrinsic matrix
	 * \param distortion_vector Distortion vector containing distortion values
	 */
	virtual void GetCameraIntrinsicParams(cv::Mat& camera_matrix,
	                                      cv::Mat& distortion_vector) const = 0;

	/**
	 * \brief Sets the current camera intrinsic parameter namely camera matrix
	 * and distortion vector
	 * \param camera_matrix Camera intrinsic matrix
	 * \param distortion_vector Distortion vector containing distortion values
	 */
	virtual void SetIntrinsicParameters(const cv::Mat& camera_matrix,
	                                    const cv::Mat& distortion_vector) = 0;

	/**
	 * \brief To binary
	 * \param buffer buffer
	 */
	virtual void ToBinary(std::vector<char>& buffer) const = 0;

	/**
	 * \brief From binary
	 * \param pos position
	 * \return Camera sensor object
	 */
	std::shared_ptr<const CameraSensor> CreateFromBinary(std::vector<char>::const_iterator& pos);

	/**
	 * \brief Gets the best open CV model
	 * \param camera_matrix Camera intrinsic matrix
	 * \param distortion_vector Distortion vector containing distortion values
	 * \return If successful
	 */
	virtual bool GetBestFitCVModel(cv::Mat& camera_matrix,
	                               cv::Mat& distortion_vector) const;

	/**
	 * \brief Gets the best open CV model
	 * \param camera_matrix Camera intrinsic matrix
	 * \param distortion_vector Distortion vector containing distortion values
	 * \return  If successful
	 */
	virtual bool GetBestFitCVModelLaser(cv::Mat& camera_matrix,
	                                    cv::Mat& distortion_vector) const;

	/**
	 * \brief Returns current extrinsic parameters of the camera as cv::Mat4d transformation.
	 * The transformation contains rotation matrix(3x3) and translation vector(3x1)
	 */
	void GetTransformationMatrix(cv::Mat& transformation) const;

	/**
	 * \brief Sets the transformation matrix. The transformation matrix contains
	 * rotation matrix(3x3) and translation vector(3x1)
	 */
	void SetTransformationMatrix(const cv::Mat& transformation);

	void ResetOrigin(const cv::Mat& transformation);

	/**
	 * \brief Returns current extrinsic parameters of the camera.
	 * \param rotation_matrix rotation matrix
	 * \param translation_vector translation vector
	 */
	void GetExtrinsicParameters(cv::Mat& rotation_matrix,
	                            cv::Mat& translation_vector) const;

	/**
	 * \brief Sets current extrinsic parameters of the camera.
	 * \param rotation_matrix 3x3 rotation matrix (no 3x1 Rodrigues vector)
	 * \param translation_vector translation vector
	 */
	void SetExtrinsicParameters(const cv::Mat& rotation_matrix,
	                            const cv::Mat& translation_vector);

	/**
	 * \brief Gets image channel
	 * \return image channel
	 */
	virtual ImageChannel GetImageChannel() const;

	/**
	 * \brief Set Image channel
	 * \param image_channel image channel
	 */
	virtual void SetImageChannel(ImageChannel image_channel);

	/**
	 * \brief This is for calibration only and marks free parameters to be optimized
	 * \param free_parameters parameters for optimization
	 */
	void SetFreeParameter(size_t free_parameters);

	/**
	 * \brief Set fixed parameters
	 * \param fixed_param parameters which will remain fixed and not be optimized
	 */
	void SetFixedParameter(size_t fixed_param);

	/**
	 * \brief Sets all parameters fixed and hence no optimization for any of the parameters
	 */
	void SetAllParametersFixed();

	/**
	 * \brief Transform a 3D point from camera coordinates to world coordinate system
	 * \param point 3D point<float> which need to be transformed
	 * \return transformed point<float>
	 */
	cv::Point3f FromCameraToWorldCoordinateSystem(const cv::Point3f& point) const;

	/**
	 * \brief Transform a 3D point from camera coordinates to world coordinate system
	 * \param point 3D point<float> which need to be transformed
	 * \param rotation_matrix 3D rotation matrix
	 * \param translation_vector translation vector
	 * \return translation_vector point<float>
	 */
	cv::Point3f FromCameraToWorldCoordinateSystem(const cv::Point3f& point,
	                                              cv::Mat rotation_matrix,
	                                              cv::Mat translation_vector) const;

	/**
	 * \brief Transform a 3D point from camera coordinates to world coordinate system
	 * \param point 3D point<double> which need to be transformed
	 * \return transformed point<double>
	 */
	cv::Point3d FromCameraToWorldCoordinateSystem(const cv::Point3d& point) const;

	/**
	 * \brief Transform an eigen vector from camera coordinates to world coordinate system
	 * \param eigen_vector eigen vector<float> which need to be transformed
	 * \return transformed eigen vector<float>
	 */
	Eigen::Vector3f FromCameraToWorldCoordinateSystem(const Eigen::Vector3f& eigen_vector) const;

	/**
	 * \brief Transform an eigen Matrix from camera coordinates to world coordinate system
	 * \param eigen_matrix eigen matrix<float> which need to be transformed
	 * \return transformed eigen matrix<float>
	 */
	Eigen::Matrix3f FromCameraToWorldCoordinateSystem(const Eigen::Matrix3f& eigen_matrix) const;

	/**
	 * \brief Transform an eigen vector from camera coordinates to world coordinate system
	 * \param eigen_vector eigen vector<double> which need to be transformed
	 * \return transformed eigen_vector<double>
	 */
	Eigen::Vector3d FromCameraToWorldCoordinateSystem(const Eigen::Vector3d& eigen_vector) const;

	/**
	 * \brief Transform an eigen matrix from camera coordinates to world coordinate system
	 * \param eigen_matrix eigen matrix<double> which need to be transformed
	 * \return transformed eigen matrix<double>
	 */
	Eigen::Matrix3d FromCameraToWorldCoordinateSystem(const Eigen::Matrix3d& eigen_matrix) const;

	/**
	 * \brief Transform a 3D point<float> from world coordinate system to camera coordinates
	 * \param point point<float> which need to be transformed
	 * \return transformed point<float>
	 */
	cv::Point3f FromWorldCoordinateSystemToCamera(const cv::Point3f& point) const;

	/**
	 * \brief Transform a 3D point from world coordinate system to camera coordinates
	 * \param point point<double> which need to be transformed
	 * \return transformed point<double>
	 */
	cv::Point3d FromWorldCoordinateSystemToCamera(const cv::Point3d& point,
	                                              bool if_log = false) const;

	/**
	 * \brief Transform an eigen vector from world coordinate system to camera coordinates
	 * \param eigen_vector eigen vector<float> which need to be transformed
	 * \return transformed eigen vector<float>
	 */
	Eigen::Vector3f FromWorldCoordinateSystemToCamera(const Eigen::Vector3f& eigen_vector) const;

	/**
	 * \brief Transform an eigen matrix from world coordinate system to camera coordinates
	 * \param eigen_matrix eigen matrix<float> which need to be transformed
	 * \return transformed eigen matrix<float>
	 */
	Eigen::Matrix3f FromWorldCoordinateSystemToCamera(const Eigen::Matrix3f& eigen_matrix) const;

	/**
	 * \brief Transform an eigen vector from world coordinate system to camera coordinates
	 * \param eigen_vector eigen vector<double> which need to be transformed
	 * \return transformed eigen vector<double>
	 */
	Eigen::Vector3d FromWorldCoordinateSystemToCamera(const Eigen::Vector3d& eigen_vector) const;

	/**
	 * \brief Transform a eigen matrix from world coordinate system to camera coordinates
	 * \param eigen_matrix eigen matrix<double> which need to be transformed
	 * \return transformed eigen matrix<double>
	 */
	Eigen::Matrix3d FromWorldCoordinateSystemToCamera(const Eigen::Matrix3d& eigen_matrix) const;

	/**
	 * \brief Same as openCV solvePnP but applies own un-distort model before.
	 * \param object_points Point of the object
	 * \param image_points Points in the image
	 * \param rotation_vector Rotation vector
	 * \param translation_vector Translation Vector
	 * \param if_use_extrinsic_guess Flag for using extrinsic guess
	 * \param flags Solve pnp method
	 * \return if solve pnp was successful
	 */
	virtual bool SolvePnP(cv::InputArray& object_points,
	                      cv::InputArray& image_points,
	                      cv::Mat& rotation_vector,
	                      cv::Mat& translation_vector,
	                      bool if_use_extrinsic_guess = false,
	                      int flags                   = cv::SOLVEPNP_ITERATIVE) const;

	/**
	 * \brief Gets intersection point of rays defined by two image coordinates
	 * \param other The other camera sensor for which intersection has to be obtained
	 * \param this_px_coords Pixel coordinates of the calling camera sensor
	 * \param other_px_coords Pixel coordinates of the other camera sensor
	 * \param points_3d points 3d
	 * \param error_2d error 2d
	 * \param ignore_depth If the depth h as to be ignored
	 * \return size of point 3d
	 */
	size_t StereoIntersect(std::shared_ptr<const CameraSensor> other,
	                       const Dots& this_px_coords,
	                       const Dots& other_px_coords,
	                       std::vector<cv::Vec3d>& points_3d,
	                       std::vector<cv::Vec2f>* error_2d = nullptr,
	                       bool ignore_depth                = false) const;

	/**
	 * \brief Reconstructs ray direction in world coordinates.
	 * \param points_2d input points 2d
	 * \param out result
	 * \param normalize if normalize
	 * \param points3d reconstructed 3d points
	 */
	void RayReconstruction(cv::InputArray points_2d,
	                       cv::OutputArray out,
	                       bool normalize          = false,
	                       cv::InputArray points3d = cv::noArray()) const;
	/**
	 * \brief Reconstructs ray direction in world coordinates.
	 * \param points_2d input points 2d
	 * \param out result
	 * \param normalize if normalize
	 * \param points3d reconstructed 3d points
	 */
	void RayReconstruction2(cv::InputArray points_2d,
	                        cv::OutputArray out,
	                        bool normalize          = false,
	                        cv::InputArray points3d = cv::noArray()) const;
	/**
	 * \brief Get plane intersection points
	 * \param points_2d Input points 2d
	 * \param plane_position plane position
	 * \param plane_normal plane normal
	 * \param intersection_points_camera_cs intersection points in camera coordinate system
	 */
	virtual void GetPlaneIntersectionPoints(cv::InputArray& points_2d,
	                                        const cv::Vec3d& plane_position,
	                                        const cv::Vec3d& plane_normal,
	                                        cv::OutputArray intersection_points_camera_cs) const;

protected:
	/**
	 * \brief Method for keeping the intrinsic parameters synchronized. Here
	 * camera matrix and distortion vector are updated
	 * \param camera_matrix camera matrix
	 * \param distortion_vector distortion vector
	 */
	void UpdateIntrinsicParameters(const cv::Mat& camera_matrix,
	                               const cv::Mat& distortion_vector);

	/**
	 * \brief Method for keeping the transformation matrix synchronized
	 * \param rotation_matrix rotation matrix
	 * \param translation_vector translation vector
	 */
	void UpdateTransformationMatrix(const cv::Mat& rotation_matrix,
	                                const cv::Mat& translation_vector);

	/**
	 * \brief Method for keeping the extrinsic parameters synchronized
	 * \param transformation_matrix transformation matrix
	 */
	void UpdateExtrinsicParameters(const cv::Mat4d& transformation_matrix);

	/**
	 * \brief Convenience function for simpler access to the ProjectPoints method
	 * \param point_3d 3D point to be projected either in superordinate
	 *		  system coordinates or in camera coordinates
	 * \param apply_camera_extrinsics true: points_3d given in superordinate system, own R and T applied
	 *                                false: points_3d given in camera system, own R and T ignored
	 * \return Projected point
	 */
	cv::Vec2d ProjectPoint(const cv::Vec3d& point_3d,
	                       bool apply_camera_extrinsics = true) const;

	/**
	 * \brief Convenience function for simpler access to the UndistortPoints method
	 * Set point_depth to cv::Vec3d(0, 0, 0) for ignoring the depth correction
	 * \param point_pixel point pixel
	 * \param point_depth point depth
	 * \return Undistorted point
	 */
	cv::Vec2d UndistortPoint(const cv::Vec2d& point_pixel,
	                         const cv::Vec3d& point_depth = cv::Vec3d(0, 0, 0)) const;

	/**
	 * \brief
	 * \param source
	*/
	/*explicit CameraSensor2(std::shared_ptr<const CameraSensor2> source);*/

	// Extrinsic parameters
	cv::Mat rotation_matrix_;
	cv::Mat translation_vector_;
	cv::Mat transformation_matrix_;

	// Intrinsic
	cv::Mat camera_intrinsic_matrix_;
	cv::Mat distortion_vector_;

	ImageChannel image_channel_;
	std::vector<size_t> parameter_fixations_;
	std::vector<double> parameters_;
};
}
}
