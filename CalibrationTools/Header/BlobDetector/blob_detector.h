#pragma once
#include "symbol_export.h"
#include "circular_marker.h"

namespace DMVS
{
namespace CameraCalibration
{
/**
 * \brief This class implements of Blob detection algorithm
 */
class DMVS_CAMERA_CALIBRATION_DLLIO BlobDetector
{
public:
	typedef std::vector<std::vector<cv::Point>> VectorCVPoint;
	typedef std::vector<cv::Moments> VectorCVMoments;

	/**
	 * \brief Structure container which contains all the parameters for blob detection
	 */
	struct DMVS_CAMERA_CALIBRATION_DLLIO BlobDetectorParams
	{
		BlobDetectorParams();

		size_t threshold_step;
		size_t min_threshold;
		size_t max_threshold;

		size_t min_repeatability;
		float min_distance_between_blobs;

		bool set_id_by_color;
		uchar blob_color;

		bool filter_by_area;
		float min_area;
		float max_area;

		bool filter_by_circularity;
		float min_circularity;
		float max_circularity;

		bool filter_by_inertia;
		float min_inertia_ratio;
		float max_inertia_ratio;

		bool filter_by_convexity;
		float min_convexity;
		float max_convexity;

		bool filter_by_ring_in_ring;
		double min_viewing_angle;

		size_t white_circle_id;
		size_t black_circle_id;

		//Defines max number of iterations for gradient based refinement
		size_t refine_ellipses;
		bool decode_ring_marker;
		bool coded_only;

		double percentage_agreement_with_artificial_ellipse;

		size_t filter_ellipse_width;
		double scale;
	};

	BlobDetector(const BlobDetectorParams& params = BlobDetectorParams());

	BlobDetector(const BlobDetector& rhs);

	BlobDetector& operator=(const BlobDetector& rhs);

	BlobDetector(BlobDetector&& rhs) = delete;

	BlobDetector& operator=(BlobDetector&& rhs) = delete;

	~BlobDetector();

	/**
	 * \brief Detects blob in the given image and finds all the circular marker
	 * \param image Input image in which blobs are detected.
	 * \param key_points Circular markers which are found as a result of blob detection
	 */
	void DetectBlob(const cv::Mat& image,
	                CircularMarkerVector& key_points) const;

private:

	static bool CheckBoundingRectangle(const cv::Mat& image,
	                                   const cv::Rect& rectangle);

	static bool IsContourSizeOk(const VectorCVPoint& contours,
	                            size_t index);

	static double CalculateRatio(cv::Moments moment,
	                             double denominator);

	static size_t GetOverlapCount(const cv::Mat& binary_image,
	                              const std::vector<cv::Point_<int>>& scaled_contour);

	static bool BlobToBlackRingRatio(const std::vector<cv::Point_<int>>& contour,
	                                 size_t count);

	static bool IsPointOnImage(const cv::Mat& image,
	                           const std::vector<cv::Point_<int>>::value_type& point);

	void FindBlobs(const cv::Mat& image,
	               const cv::Mat& contour_search_image,
	               size_t threshold,
	               CircularMarkerVector& centers) const;

	void GetBinaryImage(const cv::Mat& image,
	                    const cv::Mat& contour_search_image,
	                    size_t threshold,
	                    cv::Mat& contour_search_binary_image) const;

	void EnsureOriginalImageSize(VectorCVPoint contours) const;

	bool FilterOnParamSettings(const VectorCVPoint& contours,
	                           const VectorCVMoments& moments,
	                           size_t contour_idx,
	                           size_t parent_idx,
	                           cv::Moments moment,
	                           cv::Moments moment_parent,
	                           double& ratio) const;

	bool IsLessThanMinRatio(const cv::RotatedRect& rectangle) const;

	bool FilterByArea(double area) const;

	bool FilterByCircularity(cv::Moments moment,
	                         cv::Moments parent_moment,
	                         const VectorCVPoint& contours,
	                         size_t contour_idx,
	                         size_t parent_idx) const;

	bool FilterByConvexity(cv::Moments moment,
	                       const VectorCVPoint& contours,
	                       size_t contour_idx) const;


	bool FilterByInertia(cv::Moments moment,
	                     double& ratio) const;

	bool IfBlobInsideBlackRing(const cv::Mat& contour_search_binary_image,
	                           VectorCVPoint contours,
	                           size_t contour_idx,
	                           const CircularMarker& center,
	                           cv::Moments moment,
	                           cv::Moments moment_parent) const;


	bool IsBlobToBlackRingRatioOk(cv::Moments moment,
	                              cv::Moments moment_parent,
	                              const std::vector<cv::Point_<int>>& contour,
	                              size_t count) const;

	bool IsParentRightSize(double area,
	                       double area_parent) const;

	BlobDetectorParams params_;
};
}
}
