#pragma once

#include "Interfaces/iimage_processing.h"
#include "params_calibration.h"

namespace DMVS
{
namespace CameraCalibration
{
class ImageProcessing : public IImageProcessing
{
public:
	typedef std::vector<std::vector<cv::Point>> VectorCVPoint;
	typedef std::vector<cv::Moments> VectorCVMoments;

	void DetectBlob(const cv::Mat& image,
	                const cv::Mat& contour_search_image,
	                int threshold,
	                CircularMarkerVector& centers) override;

	void DetectBlob(const cv::Mat& image,
	                CircularMarkerVector& key_points) override;

	void DrawRings() override;
	void DrawCenters() override;
	void Reset() override;
	void DetectMarkers() override;

private:
	ParamsCalibration params_;

	void FindBlobs(const cv::Mat& image,
	               const cv::Mat& contour_search_image,
	               int threshold,
	               CircularMarkerVector& centers) const;

	void GetBinaryImage(const cv::Mat& image,
	                    const cv::Mat& contour_search_image,
	                    int threshold,
	                    cv::Mat& contour_search_binary_image) const;

	void EnsureOriginalImageSize(VectorCVPoint contours) const;

	bool FilterOnParamSettings(const VectorCVPoint& contours,
	                           const VectorCVMoments& moments,
	                           size_t contour_idx,
	                           size_t parent_idx,
	                           cv::Moments moment,
	                           cv::Moments moment_parent,
	                           double& ratio) const;

	bool FilterByArea(double area) const;

	bool FilterByCircularity(cv::Moments moment,
	                         cv::Moments parent_moment,
	                         const VectorCVPoint& contours,
	                         size_t contour_idx,
	                         size_t parent_idx) const;

	bool FilterByConvexity(cv::Moments moment,
	                       const VectorCVPoint& contours,
	                       size_t contour_idx) const;

	static double CalculateRatio(cv::Moments moment,
	                             double denominator);

	bool FilterByInertia(cv::Moments moment,
	                     double& ratio) const;

	bool IsLessThanMinRatio(const cv::RotatedRect& rectangle) const;

	static bool CheckBoundingRectangle(const cv::Mat& image,
	                                   const cv::Rect& rectangle);

	bool IfBlobInsideBlackRing(const cv::Mat& contour_search_binary_image,
	                           VectorCVPoint contours,
	                           size_t contour_idx,
	                           const CircularMarker& center,
	                           cv::Moments moment,
	                           cv::Moments moment_parent) const;

	static size_t GetOverlapCount(const cv::Mat& binary_image,
	                              const std::vector<cv::Point_<int>>& scaled_contour);

	bool IsBlobToBlackRingRatioOk(cv::Moments moment,
	                              cv::Moments moment_parent,
	                              const std::vector<cv::Point_<int>>& contour,
	                              size_t count) const;

	bool IsParentRightSize(double area,
	                       double area_parent) const;

	static bool BlobToBlackRingRatio(std::vector<cv::Point_<int>> contour,
	                                 size_t count);

	static bool IsPointOnImage(cv::Mat image,
	                           const std::vector<cv::Point_<int>>::value_type& point);

	static bool IsContourSizeOk(const VectorCVPoint& contours,
	                            size_t index);
};
}
}
