#pragma once

#include "transformation.h"
#include "camera_system_transformation.h"
#include "calibration_target_base.h"

namespace DMVS
{
namespace CameraCalibration
{
class CameraParameterOptimizer;

/**
 * \brief This class provides access to board transformation. One object manages multiple boards.
 * One board is defined as reference board (== world coordinate system), the transformation
 * of this board is the identity matrix. All other boards then have a transformation relative
 * to this reference board. Conversion between board positions and parameters must be defined
 * by derived class. The class is used in combination with CameraCalibrationFit.
 * See math: "\\hq-eu-rd01\03engine\07_Activities\2015\V8ScannerAnglesFromCamera_ImproveRobustness\Concept.docx"
 */
class CalibrationBoardsTransformation
{
public:
	CalibrationBoardsTransformation();

	/**
	 * \brief constructor
	 * \param board_map the boards managed by the current object
	 */
	CalibrationBoardsTransformation(const std::map<BoardID, std::shared_ptr<CalibrationTargetBase>>& board_map);

	virtual ~CalibrationBoardsTransformation();

	/**
	 * \brief Forcing a copy constructor to be deleted by the compiler.
	 */
	CalibrationBoardsTransformation(const CalibrationBoardsTransformation& rhs);

	/**
	 * \brief Forcing a assignment operator to be deleted by the compiler.
	 */
	CalibrationBoardsTransformation& operator=(const CalibrationBoardsTransformation& rhs);

	/**
	 * \brief Forcing a move copy constructor to be deleted by the compiler.
	 */
	CalibrationBoardsTransformation(CalibrationBoardsTransformation&& rhs) = delete;

	/**
	 * \brief Forcing a move assignment operator to be deleted by the compiler.
	 */
	CalibrationBoardsTransformation& operator=(CalibrationBoardsTransformation&& rhs) = delete;

	/**
	 * \brief Returns the id of all the calibration boards which are currently handled.
	 */
	std::set<size_t> GetBoardIDs() const;

	/**
	 * \brief Returns marker position of marker_id that belongs to board with
	 * board_id in world coordinate system (WCS)
	 * \param board_id
	 * \param marker_id
	 */
	cv::Point3d GetMarkerPosition(size_t board_id,
	                              size_t marker_id) const;

	/**
	 * \brief Gets board transformation with board_id in world coordinate system (WCS)
	 * \param board_id
	 * \return
	 */
	const Transformation& GetBoardTransformation(size_t board_id) const;

	/**
	 * \brief Gets board transformation for all the boards handled by the current object
	 * Everything is in world coordinate system (WCS)
	 */
	const std::map<BoardID, Transformation>& GetBoardTransformationMap() const;

	/**
	 * \brief Sets board transformation with board id and transformation
	 * \param board_id
	 * \param transformation
	 */
	void SetBoardTransformation(BoardID board_id,
	                            const cv::Mat4d& transformation);

	/**
	 * \brief Sets board transformation with board id and transformation
	 */
	void SetBoardTransformation(BoardID board_id,
	                            const Transformation& transformation);

	/**
	 * \brief Sets board transformation using board transformation map
	 * \param board_transformation_map
	 */
	void SetBoardTransformations(const std::map<BoardID, Transformation>& board_transformation_map);

	/**
	 * \brief Returns the board data for the given board id, not transformed to
	 * world coordinate system (WCS)
	 * \param board_id
	 * \return
	 */
	std::shared_ptr<const CalibrationTargetBase> GetBoard(size_t board_id) const;

	/**
	 * \brief Returns how many parameters are necessary for this object
	 */
	virtual size_t GetRequiredParameterCount() const;

	/**
	 *\brief Initializes the board positions by taking values from parameter vector.
	 * The values which are used are then removed from the parameter vector.
	 * \param parameters
	 */
	virtual void TakeFromParameters(std::vector<double>& parameters);

	/**
	 * \brief returns the parameters describing the board positions
	 */
	virtual std::vector<double> GetParameters() const;

	/**
	 * \brief Getter for reference board id
	 */
	size_t GetReferenceBoardID() const;

	/**
	 * \brief Setter for reference board id
	 * \param reference_board_id
	 */
	void SetReferenceBoardID(size_t reference_board_id);

	/**
	 * \brief Sets an initial guessed board positions. For this an approximate
	 * camera matrix for the reference camera is taken from cameras
	 * \param observations
	 * \param camera_parameter_optimizer
	 * \param camera_system_transformation
	 */
	virtual void SetInitialParameters(const ObservationMap& observations,
	                                  std::shared_ptr<const CameraParameterOptimizer> camera_parameter_optimizer,
	                                  std::shared_ptr<const CameraSystemTransformation> camera_system_transformation);

	/**
	 * \brief Update camera dependent markers
	 * \param cameras
	 * \param camera_system_transformation
	 */
	void UpdateCameraDependentMarkers(std::shared_ptr<const CameraParameterOptimizer> cameras,
	                                  std::shared_ptr<const CameraSystemTransformation> camera_system_transformation);

	/**
	 * \brief Is the board with given board id initialized
	 * \param board_id
	 */
	bool IsBoardInitialized(BoardID board_id) const;

protected:
	/**
	 * \brief Getter for initialized boards
	 */
	const std::map<BoardID, bool>& GetInitializedBoardsMap() const;

	/**
	 * \brief Setter for initialized boards
	 * \param initialized_boards_map
	 */
	void SetInitializedBoardsMap(const std::map<BoardID, bool>& initialized_boards_map);

	/**
	 * \brief The managed boards (contains board id and 3d markers, not in global
	 * coordinate system, except for reference board)
	 */
	std::map<BoardID, std::shared_ptr<CalibrationTargetBase>> board_data_map_;

	/**
	 * \brief the relative positions of the boards to the reference board
	 */
	std::map<BoardID, Transformation> board_transformation_map_;

	/**
	 * \brief Holds a map of initialized boards
	 */
	std::map<BoardID, bool> initialized_boards_map_;

	/**
	 * \brief Holds reference board id
	 */
	size_t reference_board_id_;
};
};
}
