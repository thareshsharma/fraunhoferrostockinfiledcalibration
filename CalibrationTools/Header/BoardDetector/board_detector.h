#pragma once

#include "circular_marker.h"
#include "camera_sensor.h"

namespace fs = boost::filesystem;

class CalibrationFileBase;
class ConfigurationContainer;

namespace DMVS
{
namespace CameraCalibration
{
class BoardDetector
{
public:

	typedef std::map<BoardID, cv::Point3d>::iterator MarkerIterator;
	typedef std::map<BoardID, CircularMarker>::iterator DetectedIterator;

	BoardDetector();

	BoardDetector(const BoardDetector& rhs);

	BoardDetector& operator=(const BoardDetector& rhs);

	BoardDetector(BoardDetector&& rhs) noexcept;

	BoardDetector& operator=(BoardDetector&& rhs) noexcept;

	~BoardDetector();

	/**
	 * \brief Load board parameters from .yml file. File contains identifiable markers and optionally un-coded additional markers
	 * matrix d nx4 : codedMarkerCoordinates:      ID x y z  ( Ring code marker starting with ID 1001 )
	 * matrix d nx4 : circleGridMarkerCoordinates: ID x y z  ( exact circle grid marker with ID 101 )
	 * matrix d nx3 : additionalMarkerCoordinates     x y z  ( additional unidentifiable markers)
	 * \param file_name  name of the file containing board file
	 * \return
	 */
	int LoadFromFile(const fs::path& file_name);

	/**
	 * \brief Gets all board marker IDs and the positions in board coordinate system
	 * \return Marker IDs and the fix positions in board coordinates
	 */
	std::map<BoardID, cv::Point3d> GetBoardMarkers() const;

	/**
	 * \brief Detect the board within the given markers
	 * Detects the board with coded markers and finds corresponding uncoded markers
	 * \param markers: Markers to find the board in
	 * \return Number of detected and used markers
	 */
	BoardID Detect(const CircularMarkerVector& markers);

	/**
	 * \brief Gets board id
	 */
	BoardID GetBoardId() const;

	/**
	 * \brief Gets all detected marker after detect call
	 */
	std::map<BoardID, CircularMarker> GetDetectedMarkers() const;

	/**
	 * \brief Gets the first ID if uncoded markers, all coded or pattern IDs are smaller
	 */
	BoardID GetUncodedMarkerStartId() const;

	/**
	 * \brief Checks if the marker has circle grid marker
	 */
	bool HasCircleGridMarker() const;

	/**
	 * \brief Sets minimum area of detected coded marker ellipses in pixel
	 */
	void SetMinCodedMarkerArea(double area);

	/**
	 * \brief
	 * \param camera_sensor
	 * \param rotation_matrix
	 * \param translation_vector
	 * \return
	 */
	bool GetOrientation(const std::shared_ptr<CameraSensor>& camera_sensor,
	                    cv::Mat& rotation_matrix,
	                    cv::Mat& translation_vector);

	/**
	 * \brief
	 * \param board_vertices
	 */
	void GetProjectPlaneContour(std::vector<cv::Point3d>& board_vertices) const;

	/**
	 * \brief
	 * \param file_base
	 * \param boards
	 * \param configuration_options
	 * \return
	 */
	static size_t LoadDetectorBoards(const std::shared_ptr<CalibrationFileBase>& file_base,
	                                 std::vector<std::shared_ptr<BoardDetector>>& boards,
	                                 std::shared_ptr<ConfigurationContainer> configuration_options);

	static size_t LoadDetectorBoardsInfield(const std::string& calibration_board_files_folder,
	                                        std::vector<std::shared_ptr<BoardDetector>>& boards);

private:
	/**
	 * \brief
	 * \param id
	 * \return
	 */
	cv::Point3d CameraCoordinates(size_t id);

	/**
	 * \brief
	 * \param camera_sensor
	 * \return
	 */
	bool CalculateOrientation(const std::shared_ptr<CameraSensor>& camera_sensor);

	/**
	 * \brief Transform all coordinates if coordinate system definition markers are available
	 * You can set coordinate system definition marker to define origin, x axis and y axis
	 * All marker coordinates coded and uncoded are translated to that coordinate system
	 * \param origin_id
	 * \param x_id
	 * \param y_id
	 */
	void RedefineCoordinateSystemOxy(size_t origin_id,
	                                 size_t x_id,
	                                 size_t y_id);

	/**
	 * \brief Gets all coded markers that belong to the board
	 * \param markers markers all available markers
	 * \return number of detected coded markers
	 */
	size_t DetectCodedMarkers(const CircularMarkerVector& markers);

	/**
	 * \brief Calculates camera pose with current detected markers
	 */
	bool UpdatePose();

	/**
	 * \brief Find corresponding uncoded marker after detection of board with coded marker
	 * Try to identify additional uncoded board marker within the provided markers.
	 * Adds the identified markers to m_detectedMarkers with an internal ID : 10000 + index
	 */
	size_t FindUncodedMarker();

	/**
	 * \brief
	 * \param markers
	 * \param camera_sensor
	 * \param rotation_vector
	 * \param translation_vector
	 * \return
	 */
	bool SolvePosition(const std::map<BoardID, CircularMarker>& markers,
	                   std::shared_ptr<const CameraSensor> camera_sensor,
	                   cv::Mat& rotation_vector,
	                   cv::Mat& translation_vector) const;

	/**
	 * \brief All board markers with ID and position in board coordinates
	 */
	std::map<BoardID, cv::Point3d> board_markers_;

	/**
	 * \brief The detected marker coordinates provided by external blob detection
	 */
	std::map<BoardID, CircularMarker> detected_marker_;

	/**
	 * \brief camera parameter of observing camera
	 */
	std::shared_ptr<CameraSensor> calibration_;

	/**
	 * \brief Rotation vector
	 */
	cv::Mat rotation_matrix_;

	/**
	 * \brief Translation vector
	 */
	cv::Mat translation_vector_;

	/**
	 * \brief
	 */
	bool is_detected_;

	/**
	 * \brief Marks if the board contains a cv::circle grid
	 */
	bool has_circle_grid_marker_;

	/**
	 * \brief Marks if the board has additional un-coded marker
	 */
	bool has_uncoded_marker_;

	/**
	 * \brief Board id
	 */
	int board_id_;

	/**
	 * \brief Start ID of un-coded Markers
	 */
	BoardID uncoded_start_id_;

	/**
	 * \brief Minimum ellipse area for detection of coded marker (for stability ) deactivated if < 0
	 */
	double coded_marker_area_min_{};

	std::vector<cv::Point3d> board_vertices_;

	/**
	 * \brief Coordinate system definition
	 */
	cv::Vec3d coordinate_system_definition_;
};
}
}
