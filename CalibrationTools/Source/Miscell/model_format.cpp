#include "model_format.h"

const wchar_t ModelFormat::END_OF_BLOCK           = '}';
const wchar_t ModelFormat::START_OF_BLOCK         = '{';
const std::string ModelFormat::END_OF_BLOCK_STR   = "}";
const std::string ModelFormat::START_OF_BLOCK_STR = "{";
const std::string ModelFormat::XML_HEADER         = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
const std::string ModelFormat::XML_V2_COMMENT     = "<!-- XML v. 2 -->";
const std::string ModelFormat::XML_NEXT_ROOT      = "<!-- NEXT: XML Root Element -->";
const std::string ModelFormat::XML_NEXT_VERSION   = "<!-- NEXT: Version Information -->";
const std::string ModelFormat::XML_START_COMMENT  = "<!-- ";
const std::string ModelFormat::XML_END_COMMENT    = " -->";
const std::string ModelFormat::XML_ROOT_START_TAG = "<FARO>";
const std::string ModelFormat::XML_ROOT_END_TAG   = "</FARO>";
const std::string ModelFormat::XML_FOOTER         = "</FARO>";
