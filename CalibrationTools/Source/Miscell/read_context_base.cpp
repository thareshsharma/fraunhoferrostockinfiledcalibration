#include "read_context_base.h"

ReadContextBase::ReadContextBase(const ModelFormat::Format format) :
	flag_read_demand_data_(false),
	flag_read_marked_(false),
	flag_update_object_(false),
	flag_read_encrypted_(false),
	format_(format)
{
}

ReadContextBase::~ReadContextBase() noexcept
= default;

bool ReadContextBase::ReadDemandData() const
{
	return flag_read_demand_data_;
}

void ReadContextBase::SetReadDemandData(const bool read_demand_data)
{
	flag_read_demand_data_ = read_demand_data;
}

bool ReadContextBase::ReadMarked() const
{
	return flag_read_marked_;
}

void ReadContextBase::SetReadMarked(const bool read_marked)
{
	flag_read_marked_ = read_marked;
}

bool ReadContextBase::ReadEncrypted() const
{
	return flag_read_encrypted_;
}

void ReadContextBase::SetReadEncrypted(const bool read_encrypted)
{
	flag_read_encrypted_ = read_encrypted;
}

bool ReadContextBase::UpdateObject() const
{
	return flag_update_object_;
}

void ReadContextBase::SetUpdateObject(const bool update_object)
{
	flag_update_object_ = update_object;
}

ModelFormat::Format ReadContextBase::GetFormat() const
{
	return format_;
}

void ReadContextBase::SetFormat(const ModelFormat::Format format)
{
	format_ = format;
}
