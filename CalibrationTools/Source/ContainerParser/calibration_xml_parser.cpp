#include "calibration_xml_parser.h"
#include <boost/algorithm/string.hpp>

#include "helper.h"

CalibrationXMLParser::CalibrationXMLParser(fs::path xml_file_path) :
	calibration_file_path_(std::move(xml_file_path))
{
	const auto result = document_.load_file(calibration_file_path_.c_str(), pugi::parse_comments);
	const auto outcome = result.description() == "No error" ? "has been loaded" : "could not be loaded";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration XML file [" << calibration_file_path_ << "] " << outcome;
}

CalibrationXMLParser::~CalibrationXMLParser() = default;

std::string CalibrationXMLParser::GetValueAttributeOfAttrTag(const std::string& attribute) const
{
	std::string result;

	auto attrs = document_.select_nodes("/FARO/AttrContainer/Attr");

	for(auto attr : attrs)
	{
		auto names = attr.node().select_nodes("Name");

		for(auto name : names)
		{
			std::string name_attribute_value = name.node().first_attribute().value();

			if(name_attribute_value == attribute)
			{
				auto values = attr.node().select_nodes("Value");
				for(auto value : values)
				{
					result = value.node().first_attribute().value();
				}
			}
		}
	}

	return result;
}

std::string CalibrationXMLParser::GetSerialNumber() const
{
	return GetValueAttributeOfAttrTag("'serialNumber'");
}

double CalibrationXMLParser::GetCalibrationTimestamp() const
{
	return std::stod(GetValueAttributeOfAttrTag("'calibrationTimestamp'"));
}

void CalibrationXMLParser::SaveCalibrationFile(const bool append_header,
                                               const std::string& name,
                                               const bool if_create_new)
{
	std::string file_name = name.empty() ? "Calibration_" + Helper::GetTimeStamp() + ".xml" : name;
	file_name             = if_create_new ? "Calibration_" + Helper::GetTimeStamp() + ".xml" : file_name;

	if(file_name.find(".xml") == std::string::npos)
		file_name = file_name + ".xml";

	const auto file_path = Helper::GetCamerasDirectory() / file_name;

	// add a custom declaration node
	if(append_header)
	{
		auto decl                         = document_.prepend_child(pugi::node_declaration);
		decl.append_attribute("version")  = "1.0";
		decl.append_attribute("encoding") = "UTF-8";
	}

	if(!document_.save_file(file_path.c_str(), "\t", pugi::format_default, pugi::encoding_utf8))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << file_path << " file could not be saved. ";
	}
}

//cv::Mat CalibrationXMLParser::GetSingleCameraCalibration() const
//{
//	//cv::Mat calibration;
//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << DMVS::CameraCalibration::Constants::FUNCTION_NOT_IMPLEMENTED;
//	return cv::Mat();
//}

cv::Mat GetDataVector(std::string values_string)
{
	boost::erase_all(values_string, "{");
	boost::erase_all(values_string, "}");

	std::vector<double> double_values;
	std::istringstream string_stream(values_string);
	double value;

	while(string_stream >> value)
	{
		double_values.push_back(value);
	}

	cv::Mat data_matrix = cv::Mat::zeros(11665, 4, CV_64FC1);
	auto counter        = 0;
	for(auto row = 0; row < data_matrix.rows; row++)
	{
		if(counter < double_values.size())
		{
			data_matrix.at<double>(row, 0) = double_values[counter];
			data_matrix.at<double>(row, 1) = double_values[counter + 1];
			data_matrix.at<double>(row, 2) = double_values[counter + 2];
			data_matrix.at<double>(row, 3) = double_values[counter + 3];
			counter                        = counter + 4;
		}
	}

	return data_matrix;
}

std::vector<double> GetDVector(std::string values_string)
{
	boost::erase_all(values_string, "{");
	boost::erase_all(values_string, "}");

	std::vector<double> double_values;
	std::istringstream string_stream(values_string);
	double value;

	while(string_stream >> value)
	{
		auto temp = value != 0 && std::abs(value) < 1e-15 ? 0.0 : value;
		double_values.push_back(temp);
	}

	return double_values;
}

cv::Mat CalibrationXMLParser::GetCustomMatrices() const
{
	cv::Mat data_vector_mat;
	auto sub_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer");

	for(auto sub_container : sub_containers)
	{
		std::string custom_matrices = sub_container.node().child("ArrayContainer").first_attribute().value();

		if(custom_matrices == "'customMatrices'")
		{
			auto attrs = sub_container.node().child("AttrContainer").child("SubContainer").child("AttrContainer").select_nodes("Attr");

			for(auto attr : attrs)
			{
				std::string d_vector_string = attr.node().first_attribute().value();

				if(d_vector_string == "DVector")
				{
					data_vector_mat = GetDataVector(attr.node().child("Value").first_attribute().value());
				}
			}
		}
	}

	return data_vector_mat;
}

void CalibrationXMLParser::SetCustomMatrices(const cv::Mat& pattern_definition) const
{
	auto sub_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer");

	for(auto sub_container : sub_containers)
	{
		std::string custom_matrices = sub_container.node().child("ArrayContainer").first_attribute().value();

		if(custom_matrices == "'customMatrices'")
		{
			auto attrs = sub_container.node().child("AttrContainer").child("SubContainer").child("AttrContainer").select_nodes("Attr");

			for(auto attr : attrs)
			{
				std::string d_vector_string = attr.node().first_attribute().value();

				if(d_vector_string == "DVector")
				{
					std::string data_vector = "{ ";

					for(auto row = 0; row < pattern_definition.rows; row++)
					{
						for(auto col = 0; col < pattern_definition.cols; col++)
						{
							data_vector += std::to_string(pattern_definition.at<double>(row, col)) + " ";
						}
					}

					data_vector += " }";
					auto custom_matrix_data = attr.node().child("Value").attribute("value");
					custom_matrix_data.set_value(data_vector.c_str());
				}
			}
		}
	}
}

void CalibrationXMLParser::GetDistortionVector(const int channel,
                                               cv::Mat& distortion_vector) const
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer/SubContainer");

				for(auto sub_container : sub_containers)
				{
					std::string matrix_container_value = sub_container.node().child("MatrixContainer").first_attribute().value();

					if(matrix_container_value == "'distortion'")
					{
						auto sub_container_attrs = sub_container.node().child("AttrContainer").select_nodes("Attr");

						for(auto sub_container_attr : sub_container_attrs)
						{
							std::string attr_first = sub_container_attr.node().first_attribute().value();

							if(attr_first == "DVector")
							{
								auto d_vector = GetDVector(sub_container_attr.node().child("Value").first_attribute().value());

								if(d_vector.size() == 5)
								{
									distortion_vector = cv::Mat(5, 1, CV_64F);

									for(int i = 0; i < 5; ++i)
									{
										distortion_vector.at<double>(i, 0) = d_vector[i];
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::SetDistortionVector(const int channel,
                                               const cv::Mat& distortion_vector) const
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer/SubContainer");

				for(auto sub_container : sub_containers)
				{
					std::string matrix_container_value = sub_container.node().child("MatrixContainer").first_attribute().value();

					if(matrix_container_value == "'distortion'")
					{
						auto sub_container_attrs = sub_container.node().child("AttrContainer").select_nodes("Attr");

						for(auto sub_container_attr : sub_container_attrs)
						{
							std::string attr_first = sub_container_attr.node().first_attribute().value();

							if(attr_first == "DVector")
							{
								std::string data_vector = "{ ";

								for(auto row = 0; row < distortion_vector.rows; row++)
								{
									data_vector += std::to_string(distortion_vector.at<double>(row, 0)) + " ";
								}

								data_vector += " }";
								auto distortion_data = sub_container_attr.node().child("Value").attribute("value");
								distortion_data.set_value(data_vector.c_str());
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::GetCameraMatrix(const int channel,
                                           cv::Mat& camera_matrix) const
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer/SubContainer");

				for(auto sub_container : sub_containers)
				{
					std::string matrix_container_value = sub_container.node().child("MatrixContainer").first_attribute().value();

					if(matrix_container_value == "'cameraMatrix'")
					{
						auto sub_container_attrs = sub_container.node().child("AttrContainer").select_nodes("Attr");

						for(auto sub_container_attr : sub_container_attrs)
						{
							std::string attr_first = sub_container_attr.node().first_attribute().value();

							if(attr_first == "DVector")
							{
								auto d_vector = GetDVector(sub_container_attr.node().child("Value").first_attribute().value());

								if(d_vector.size() == 9)
								{
									camera_matrix = cv::Mat(3, 3, CV_64F);

									for(auto i = 0; i < 3; ++i)
									{
										for(auto j = 0; j < 3; ++j)
										{
											camera_matrix.at<double>(i, j) = d_vector[j + 3 * i];
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::SetCameraMatrix(const int channel,
                                           const cv::Mat& camera_matrix) const
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer/SubContainer");

				for(auto sub_container : sub_containers)
				{
					std::string matrix_container_value = sub_container.node().child("MatrixContainer").first_attribute().value();

					if(matrix_container_value == "'cameraMatrix'")
					{
						auto sub_container_attrs = sub_container.node().child("AttrContainer").select_nodes("Attr");

						for(auto sub_container_attr : sub_container_attrs)
						{
							std::string attr_first = sub_container_attr.node().first_attribute().value();

							if(attr_first == "DVector")
							{
								std::string data_vector = "{ ";

								for(auto row = 0; row < camera_matrix.rows; row++)
								{
									for(auto col = 0; col < camera_matrix.cols; col++)
									{
										data_vector += std::to_string(camera_matrix.at<double>(row, col)) + " ";
									}
								}

								data_vector += " }";
								auto camera_matrix_data = sub_container_attr.node().child("Value").attribute("value");
								camera_matrix_data.set_value(data_vector.c_str());
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::GetRotationMatrix(const int channel,
                                             cv::Mat& rotation_matrix) const
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer/SubContainer");

				for(auto sub_container : sub_containers)
				{
					std::string matrix_container_value = sub_container.node().child("MatrixContainer").first_attribute().value();

					if(matrix_container_value == "'r'")
					{
						auto sub_container_attrs = sub_container.node().child("AttrContainer").select_nodes("Attr");

						for(auto sub_container_attr : sub_container_attrs)
						{
							std::string attr_first = sub_container_attr.node().first_attribute().value();

							if(attr_first == "DVector")
							{
								auto d_vector = GetDVector(sub_container_attr.node().child("Value").first_attribute().value());

								if(d_vector.size() == 9)
								{
									rotation_matrix = cv::Mat(3, 3, CV_64F);

									for(int i = 0; i < 3; ++i)
									{
										for(int j = 0; j < 3; ++j)
										{
											rotation_matrix.at<double>(i, j) = d_vector[j + 3 * i];
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::SetRotationMatrix(const int channel,
                                             const cv::Mat& rotation_matrix) const
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer/SubContainer");

				for(auto sub_container : sub_containers)
				{
					std::string matrix_container_value = sub_container.node().child("MatrixContainer").first_attribute().value();

					if(matrix_container_value == "'r'")
					{
						auto sub_container_attrs = sub_container.node().child("AttrContainer").select_nodes("Attr");

						for(auto sub_container_attr : sub_container_attrs)
						{
							std::string attr_first = sub_container_attr.node().first_attribute().value();

							if(attr_first == "DVector")
							{
								std::string data_vector = "{ ";

								for(auto row = 0; row < rotation_matrix.rows; row++)
								{
									for(auto col = 0; col < rotation_matrix.cols; col++)
									{
										data_vector += std::to_string(rotation_matrix.at<double>(row, col)) + " ";
									}
								}

								data_vector += " }";
								auto rotation_matrix_data = sub_container_attr.node().child("Value").attribute("value");
								rotation_matrix_data.set_value(data_vector.c_str());
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::GetTranslationVector(const int channel,
                                                cv::Mat& translation_vector) const
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer/SubContainer");

				for(auto sub_container : sub_containers)
				{
					std::string matrix_container_value = sub_container.node().child("MatrixContainer").first_attribute().value();

					if(matrix_container_value == "'t'")
					{
						auto sub_container_attrs = sub_container.node().child("AttrContainer").select_nodes("Attr");

						for(auto sub_container_attr : sub_container_attrs)
						{
							std::string attr_first = sub_container_attr.node().first_attribute().value();

							if(attr_first == "DVector")
							{
								auto d_vector = GetDVector(sub_container_attr.node().child("Value").first_attribute().value());

								if(d_vector.size() == 3)
								{
									translation_vector = cv::Mat(3, 1, CV_64F);

									for(int i = 0; i < 3; ++i)
									{
										translation_vector.at<double>(i, 0) = d_vector[i];
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::SetTranslationVector(const int channel,
                                                const cv::Mat& translation_vector) const
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer/SubContainer");

				for(auto sub_container : sub_containers)
				{
					std::string matrix_container_value = sub_container.node().child("MatrixContainer").first_attribute().value();

					if(matrix_container_value == "'t'")
					{
						auto sub_container_attrs = sub_container.node().child("AttrContainer").select_nodes("Attr");

						for(auto sub_container_attr : sub_container_attrs)
						{
							std::string attr_first = sub_container_attr.node().first_attribute().value();

							if(attr_first == "DVector")
							{
								std::string data_vector = "{ ";

								for(auto row = 0; row < translation_vector.rows; row++)
								{
									data_vector += std::to_string(translation_vector.at<double>(row, 0)) + " ";
								}

								data_vector += " }";
								auto translation_vector_data = sub_container_attr.node().child("Value").attribute("value");
								translation_vector_data.set_value(data_vector.c_str());
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::GetDepthDependentScalarParameters(const int channel,
                                                             cv::Vec2d& depth_params) const
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer");

				for(auto sub_container : sub_containers)
				{
					auto attrs2 = sub_container.node().select_nodes("Attr");

					for(auto attr2 : attrs2)
					{
						std::string attr_first = attr2.node().child("Name").first_attribute().value();

						if(attr_first == "'ddParamSC'")
						{
							auto d_vector = GetDVector(attr2.node().child("Value").first_attribute().value());

							if(d_vector.size() == 2)
							{
								depth_params = cv::Vec2d(d_vector[0], d_vector[1]);
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::SetDepthDependentScalarParameters(const int channel,
                                                             const cv::Vec2d& depth_params)
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer");

				for(auto sub_container : sub_containers)
				{
					auto attrs2 = sub_container.node().select_nodes("Attr");

					for(auto attr2 : attrs2)
					{
						std::string attr_first = attr2.node().child("Name").first_attribute().value();

						if(attr_first == "'ddParamSC'")
						{
							auto data_vector         = std::to_string(depth_params[0]) + " " + std::to_string(depth_params[1]);
							auto depth_params_scalar = attr2.node().child("Value").attribute("value");
							depth_params_scalar.set_value(data_vector.c_str());
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::GetDepthDependentExponentialParameters(const int channel,
                                                                  cv::Vec3d& depth_params) const
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer");

				for(auto sub_container : sub_containers)
				{
					auto attrs2 = sub_container.node().select_nodes("Attr");

					for(auto attr2 : attrs2)
					{
						std::string attr_first = attr2.node().child("Name").first_attribute().value();

						if(attr_first == "'ddParamExp'")
						{
							auto d_vector = GetDVector(attr2.node().child("Value").first_attribute().value());

							if(d_vector.size() == 3)
							{
								depth_params = cv::Vec3d(d_vector[0], d_vector[1], d_vector[2]);
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::SetDepthDependentExponentialParameters(const int channel,
                                                                  const cv::Vec3d& depth_params)
{
	auto attr_containers = document_.select_nodes("/FARO/AttrContainer/SubContainer/AttrContainer/SubContainer/AttrContainer");

	for(auto attr_container : attr_containers)
	{
		auto attrs = attr_container.node().select_nodes("Attr");

		for(auto attr : attrs)
		{
			const auto value = attr.node().child("Value").first_attribute().value();

			if(value == std::to_string(channel))
			{
				auto sub_containers = attr_container.node().select_nodes("SubContainer/AttrContainer");

				for(auto sub_container : sub_containers)
				{
					auto attrs2 = sub_container.node().select_nodes("Attr");

					for(auto attr2 : attrs2)
					{
						std::string attr_first = attr2.node().child("Name").first_attribute().value();

						if(attr_first == "'ddParamExp'")
						{
							auto data_vector = std::to_string(depth_params[0]) + " " + std::to_string(depth_params[1]) + " " + std::to_string(
								depth_params[2]);
							auto depth_params_scalar = attr2.node().child("Value").attribute("value");
							depth_params_scalar.set_value(data_vector.c_str());
						}
					}
				}
			}
		}
	}
}

void CalibrationXMLParser::GetCalibrationData(CalibrationData& calibration_data) const
{
	calibration_data.hardware_version = " ";
	calibration_data.calibration_date = std::to_string(GetCalibrationTimestamp());
	calibration_data.serial_number    = GetSerialNumber();

	CameraInfo camera_left;
	camera_left.name    = "left";
	camera_left.channel = CHANNEL_GREY0;
	camera_left.height  = Constants::DMVS_IMAGE_HEIGHT;
	camera_left.width   = Constants::DMVS_IMAGE_WIDTH;

	cv::Mat distortion_vector_left, camera_matrix_left, rotation_left, translation_left;
	GetDistortionVector(20, distortion_vector_left);
	GetCameraMatrix(20, camera_matrix_left);
	GetRotationMatrix(20, rotation_left);
	GetTranslationVector(20, translation_left);
	cv::Vec2d depth_params_left;
	GetDepthDependentScalarParameters(20, depth_params_left);

	cv::Vec3d depth_params_exp_left;
	GetDepthDependentExponentialParameters(20, depth_params_exp_left);

	camera_left.intrinsics.camera_intrinsic_matrix      = camera_matrix_left;
	camera_left.intrinsics.distortion_vector            = distortion_vector_left;
	camera_left.extrinsics.rotation_matrix              = rotation_left;
	camera_left.extrinsics.translation_vector           = translation_left;
	camera_left.intrinsics.depth_parameters.exponential = depth_params_exp_left;
	camera_left.intrinsics.depth_parameters.scalar      = depth_params_left;

	CameraInfo camera_right;
	camera_right.name    = "right";
	camera_right.channel = CHANNEL_GREY1;
	camera_right.height  = Constants::DMVS_IMAGE_HEIGHT;
	camera_right.width   = Constants::DMVS_IMAGE_WIDTH;

	cv::Mat distortion_vector_right, camera_matrix_right, rotation_right, translation_right;
	GetDistortionVector(21, distortion_vector_right);
	GetCameraMatrix(21, camera_matrix_right);
	GetRotationMatrix(21, rotation_right);
	GetTranslationVector(21, translation_right);

	cv::Vec2d depth_params_right;
	GetDepthDependentScalarParameters(21, depth_params_right);

	cv::Vec3d depth_params_exp_right;
	GetDepthDependentExponentialParameters(21, depth_params_exp_right);

	camera_right.intrinsics.camera_intrinsic_matrix      = camera_matrix_right;
	camera_right.intrinsics.distortion_vector            = distortion_vector_right;
	camera_right.extrinsics.rotation_matrix              = rotation_right;
	camera_right.extrinsics.translation_vector           = translation_right;
	camera_right.intrinsics.depth_parameters.exponential = depth_params_exp_right;
	camera_right.intrinsics.depth_parameters.scalar      = depth_params_right;

	LaserInfo projector;
	projector.name    = "projector";
	projector.channel = CHANNEL_PROJECTOR0;

	cv::Mat distortion_vector_projector, intrinsic_matrix_projector, rotation_projector, translation_projector;
	GetDistortionVector(31, distortion_vector_projector);
	GetCameraMatrix(31, intrinsic_matrix_projector);
	GetRotationMatrix(31, rotation_projector);
	GetTranslationVector(31, translation_projector);

	projector.intrinsics.projector_intrinsic_matrix = intrinsic_matrix_projector;
	projector.intrinsics.distortion_vector          = distortion_vector_projector;
	projector.extrinsics.rotation_matrix            = rotation_projector;
	projector.extrinsics.translation_vector         = translation_projector;
	projector.intrinsics.pattern_definition         = GetCustomMatrices();

	calibration_data.camera_left  = camera_left;
	calibration_data.camera_right = camera_right;
	calibration_data.projector    = projector;
}
