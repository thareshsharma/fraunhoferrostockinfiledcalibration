#include "camera_system_transformation.h"

namespace DMVS
{
namespace CameraCalibration
{
CameraSystemTransformation::CameraSystemTransformation()
= default;

CameraSystemTransformation::~CameraSystemTransformation() = default;

const Transformation& CameraSystemTransformation::GetFrameTransformation(const size_t frame_id) const
{
	if(frame_transformation_map_.find(frame_id) == frame_transformation_map_.end())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : No frame available in frame transformation map with id [" << frame_id;
		throw std::exception("No frame with this id.");
	}

	return frame_transformation_map_.at(frame_id);
}

std::map<size_t, Transformation> CameraSystemTransformation::GetFrameTransformationMap()
{
	return frame_transformation_map_;
}
}
}
