#include "independent_frame_transformations.h"
#include "camera_parameter_optimizer.h"
#include "calibration_target_fix_markers.h"
#include "helper.h"

namespace DMVS
{
namespace CameraCalibration
{
IndependentFrameTransformations::IndependentFrameTransformations(const size_t frame_count) :
	min_marker_count_(4)
{
	for(size_t i = 0; i < frame_count; ++i)
	{
		frame_transformation_map_[i] = Transformation();
	}
}

IndependentFrameTransformations::IndependentFrameTransformations(std::set<size_t> frame_indices) :
	min_marker_count_(4),
	frame_indices_(std::move(frame_indices))
{
	for(auto frame_index : frame_indices)
	{
		frame_transformation_map_[frame_index] = Transformation();
	}
}

size_t IndependentFrameTransformations::GetRequiredParameterCount() const
{
	return frame_transformation_map_.size() * 6;
}

void IndependentFrameTransformations::TakeFromParameters(std::vector<double>& parameters)
{
	if(parameters.size() < GetRequiredParameterCount())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not enough parameters.";
		return;
	}

	// take parameters from front so that push and pop of parameters h
	for(auto& frame_transformation_pair : frame_transformation_map_)
	{
		std::vector<double> temp(parameters.begin(), parameters.begin() + 6);
		frame_transformation_pair.second = Transformation(temp);

		parameters = std::vector<double>(parameters.begin() + 6, parameters.end());
	}
}

std::vector<double> IndependentFrameTransformations::GetParameters() const
{
	std::vector<double> parameters;

	for(const auto& frame_transformation_pair : frame_transformation_map_)
	{
		auto temp = frame_transformation_pair.second.Serialized();
		parameters.insert(parameters.end(), temp.begin(), temp.end());
	}

	return parameters;
}

std::shared_ptr<CameraSystemTransformation> IndependentFrameTransformations::Duplicate() const
{
	/*std::set<int> frame_indices;
	for(const auto& frame_pos : frame_positions_)
		frame_indices.insert(frame_pos.first);

	auto duplicate = std::make_shared<IndependentFramePositions>(frame_indices);
	auto params    = this->GetParameters();
	duplicate->TakeFromParameters(params);
	duplicate->SetNumMarkerRequired(min_marker_count_);*/
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;

	return nullptr;
}

void IndependentFrameTransformations::SetInitialParameters(const ObservationMap& observations,
                                                           const std::shared_ptr<const CameraParameterOptimizer> camera_parameter_optimizer,
                                                           const std::shared_ptr<const CalibrationBoardsTransformation> boards)
{
	// assumes that boards have been initialized
	// uses only reference cam, i.e. at least one board has to be detected in the reference cam in all frames
	// indexed by camId
	std::map<size_t, std::vector<Transformation>> transformations;

	// for each frame (key) the transformation idx (value) with max. number of markers
	std::map<size_t, size_t> max_marker_transformation;

	//use only reference board for relative position estimation, as its matrix is the ident matrix
	for(const auto& frame : observations.frames_measurement_map)
	{
		for(const auto& camera : frame.second.camera_measurement_map)
		{
			// Estimate frame positions with the reference cam only
			if(camera.first != camera_parameter_optimizer->GetReferenceCameraId())
				continue;

			const auto calibration = camera_parameter_optimizer->GetCalibration(camera.first);

			size_t max_marker_used_for_pose_estimation = 0;

			/*	std::vector<cv::Point3d> points_3d_all_boards;*/
			std::vector<cv::Point2d> points_2d_all_boards;

			for(const auto& board_data : camera.second.calibration_board_data_map)
			{
				// get the board using the board ID
				//const auto board_id = board.first;
				std::vector<cv::Point3d> marker_positions_3d;

				for(const auto marker : board_data.second.marker_ids)
				{
					// Apply transformation of current board to marker points (here assumes that relative board positions are already initialized).
					//points_3d_all_boards.push_back(boards->GetMarkerPosition(board_data.first, marker));
					marker_positions_3d.push_back(boards->GetMarkerPosition(board_data.first, marker));
				}

				//if(markers_point_3d_stream.is_open())
				//{
				//	for(auto positions_3d : marker_positions_3d)
				//	{
				//		markers_point_3d_stream << positions_3d.x << " " << positions_3d.y << " " << positions_3d.z << " " << board_data.first << " " << frame.first << "\n";
				//	}
				//}

				auto marker_centers_2d = Helper::GetMarkersCenter<double>(board_data.second.markers);

				//if(markers_point_2d_stream.is_open())
				//{
				//	for(auto marker_center_2d : marker_centers_2d)
				//	{
				//		markers_point_2d_stream << marker_center_2d.x << " " << marker_center_2d.y << " " << board_data.first << " " << frame.first << "\n";
				//	}
				//}

				for(const auto& marker_center_2d : marker_centers_2d)
				{
					points_2d_all_boards.push_back(marker_center_2d);
				}

				cv::Mat rotation_vector, translation_vector;

				if(marker_positions_3d.size() >= 6 && calibration->SolvePnP(marker_positions_3d,
				                                                            marker_centers_2d,
				                                                            rotation_vector,
				                                                            translation_vector))
				{
					if(marker_positions_3d.size() > max_marker_used_for_pose_estimation)
					{
						max_marker_used_for_pose_estimation    = marker_positions_3d.size();
						max_marker_transformation[frame.first] = transformations[frame.first].size();
					}

					cv::Mat rotation_matrix;
					cv::Rodrigues(rotation_vector, rotation_matrix);
					Transformation trans(rotation_matrix, translation_vector);

					cv::Mat left_trans;
					camera_parameter_optimizer->GetCalibration(CHANNEL_GREY0)->GetTransformationMatrix(left_trans);
					cv::Mat inv_left_tran = left_trans.inv();
					Transformation trans_final(inv_left_tran);
					auto final_t = trans_final.Multiply(trans);

					transformations[frame.first].push_back(final_t);
				}
			}
		}
	}

	//markers_point_2d_stream.close();
	//markers_point_3d_stream.close();
	//extrinsic_params_stream.close();

	// Set averaged transformations
	for(const auto& transformation : transformations)
	{
		frame_transformation_map_[transformation.first] = transformations[transformation.first].empty()
			                                                  ? Transformation()
			                                                  : transformation.second[max_marker_transformation[transformation.first]];
	}
}

void IndependentFrameTransformations::SetFrameTransformations(const std::map<int, Transformation>& transformations)
{
	for(const auto& transformation : transformations)
		frame_transformation_map_[transformation.first] = transformation.second;
}

void IndependentFrameTransformations::SetNumMarkerRequired(const size_t marker_count)
{
	min_marker_count_ = marker_count;
}

void IndependentFrameTransformations::PreInit()
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
}
}
}
