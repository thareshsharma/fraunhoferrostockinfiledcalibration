#include "projector_calibration_fit.h"
#include "ellipse_detection_distortion_correction.h"

namespace DMVS
{
namespace CameraCalibration
{
ProjectorCalibrationFit::ProjectorCalibrationFit(const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer,
                                                 const std::map<FrameID, std::vector<cv::Point3d>>& reconstructed_points,
                                                 const std::map<FrameID, std::vector<cv::Point2d>>& correspondences) :
	GeneralFitFunctor<double>(6, GetValues(reconstructed_points)),
	reconstructed_points_map_(reconstructed_points),
	correspondences_(correspondences),
	camera_parameter_optimizer_(camera_parameter_optimizer),
	num_params_(6),
	num_values_(GetValues(reconstructed_points))
{
}

void ProjectorCalibrationFit::FromEigen(const Eigen::VectorXd& parameters_eigen_vector,
                                        const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer)
{
	//Convert parameters to std vector
	std::vector<double> parameters(parameters_eigen_vector.data(),
	                               parameters_eigen_vector.data() + parameters_eigen_vector.rows() * parameters_eigen_vector.cols());

	//Each object removes its parameters!
	camera_parameter_optimizer->TakeFromProjectorParameters(parameters);

	assert(parameters.empty());
}

Eigen::VectorXd ProjectorCalibrationFit::ToEigen(const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer)
{
	//fetch parameters from all objects and store in "out"
	std::vector<double> out;
	{
		const auto& tmp = camera_parameter_optimizer->GetProjectorParameters();
		out.insert(out.end(), tmp.begin(), tmp.end());
	}

	Eigen::VectorXd out2 = Eigen::VectorXd::Zero(out.size());

	for(size_t i = 0; i < out.size(); ++i)
		out2[i]  = out[i];

	return out2;
}

bool ProjectorCalibrationFit::operator()(double const* const* parameters,
                                         double* residual) const
{
	Eigen::VectorXd x_eigen = Eigen::VectorXd::Zero(num_params_);

	for(auto i     = 0; i < num_params_; ++i)
		x_eigen[i] = parameters[0][i];

	Eigen::VectorXd fvec;
	this->operator()(x_eigen, fvec);

	for(auto i      = 0; i < num_values_; ++i)
		residual[i] = fvec[i];

	return true;
}

size_t ProjectorCalibrationFit::operator()(const Eigen::VectorXd& start_params,
                                           Eigen::VectorXd& residuals) const
{
	if(reconstructed_points_map_.size() != correspondences_.size())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "Reconstructed Points size " << reconstructed_points_map_.size() << " Correspondences size " << correspondences_.size() << " not same";
		return -1;
	}

	residuals = Eigen::VectorXd::Zero(num_values_);

	// Ensure all the projector and ensure which params are fixed
	const auto camera_parameter_optimizer = camera_parameter_optimizer_;
	FromEigen(start_params, camera_parameter_optimizer_);

	residuals.setZero();
	std::vector<size_t> frame_indices;
	std::map<size_t, std::vector<double>> residual_values;

	for(const auto& reconstructed_point_pair : reconstructed_points_map_)
	{
		frame_indices.push_back(reconstructed_point_pair.first);
		residual_values[reconstructed_point_pair.first] = std::vector<double>();
	}

	// use everything at once, calculate overall rms instead of per frame RMS
	for(auto i = 0; i < reconstructed_points_map_.size(); ++i)
	{
		auto& value_local = residual_values.find(i)->second;
		const auto& frame_points3d_map  = reconstructed_points_map_.find(i);
		const auto& correspondences_map = correspondences_.find(i);

		// use same index for accessing the correspondence from now on
		const auto errors_2d = GetProjectionErrors(camera_parameter_optimizer, frame_points3d_map->second, correspondences_map->second);

		for(const auto& error_2d : errors_2d)
		{
			auto x = error_2d.x;
			auto y = error_2d.y;

			/*if(abs(x) > 5 || abs(y) > 5)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Error Frame Id [" << frame_points3d_map->first << "] x [" << x << "] y [" << y << "]";
			}*/

			value_local.push_back(x);
			value_local.push_back(y);
		}
	}

	//Combine results of individual frames
	//residuals = Eigen::VectorXd::Map(residual_values.data(), residual_values.size());

	auto index = 0;
	//Combine results of individual frames
	for(auto frame_index : frame_indices)
	{
		const auto& value = residual_values.at(frame_index);

		for(const auto& residue : value)
		{
			residuals[index++] = residue;
		}
	}

	return 0;
}

std::vector<cv::Point2d> ProjectorCalibrationFit::GetProjectionErrors(
	const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
	std::vector<cv::Point3d> reconstructed_3d_points,
	std::vector<cv::Point2d> correspondences) const
{
	// calculate the board marker transformation/projection into the camera via the kinematic chain given by camSystem/camModifier/BoardsModifier
	auto points2d_from_reconstructed_points = ProjectReconstructed3DPoints(camera_parameter_optimizer, reconstructed_3d_points);

	//return vector contains 2d differences between projected board marker and corresponding measurements
	std::vector<cv::Point2d> errors(points2d_from_reconstructed_points.size());

	// return empty vector if no projected points available
	if(points2d_from_reconstructed_points.empty())
		return errors;

	for(auto i = 0; i < points2d_from_reconstructed_points.size(); ++i)
	{
		errors[i] = points2d_from_reconstructed_points[i] - correspondences[i];
	}

	return errors;
}

std::vector<cv::Point2d> ProjectorCalibrationFit::ProjectReconstructed3DPoints(
	const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
	std::vector<cv::Point3d>& reconstructed_points) const
{
	// create empty return vector
	std::vector<cv::Point2d> projected_2d_from_reconstructed_points;
	const auto calibration = camera_parameter_optimizer->GetCalibration(CHANNEL_PROJECTOR0);

	// project points into projector.
	calibration->LaserProjectPoints(reconstructed_points, projected_2d_from_reconstructed_points);

	return projected_2d_from_reconstructed_points;
}

int ProjectorCalibrationFit::GetValues(const std::map<FrameID, std::vector<cv::Point3d>>& reconstructed_points_map)
{
	int size = 0;

	for(const auto& points_map : reconstructed_points_map)
	{
		size += int(points_map.second.size()) * 2;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Reconstructed Points size " << size;
	return size;
}
}
}
