#include "relative_fix_board_with_overlap_transformation.h"
#include "camera_parameter_optimizer.h"
#include "helper.h"
#include "calibration_target_fix_markers.h"

RelativeFixBoardsWithOverlapTransformation::RelativeFixBoardsWithOverlapTransformation(
	const std::map<BoardID, std::shared_ptr<CalibrationTargetBase>>& boards,
	const std::set<BoardID>& boards_to_fix) :
	RelativeFixCalibrationBoardsTransformation(boards, boards_to_fix)
{
}

int RelativeFixBoardsWithOverlapTransformation::GetRelativeTransformation(Transformation& relative_transformation,
                                                                          BoardID first_board_id,
                                                                          BoardID second_board_id,
                                                                          const ObservationMap& observations,
                                                                          const std::shared_ptr<const CameraParameterOptimizer>&
                                                                          camera_parameter_optimizer,
                                                                          const std::shared_ptr<const CameraSystemTransformation>&
                                                                          camera_system_transformation) const
{
	// check whether both boards have been detected in at least one frame with at least one camera
	std::vector<Transformation> transformations;
	Transformation max_marker_transformation;
	size_t maximum_marker_count = 0;

	for(const auto& frame_measurement_map : observations.frames_measurement_map)
	{
		for(const auto& camera_measurement_pair : frame_measurement_map.second.camera_measurement_map)
		{
			// cam.second.boards contains all board detected in current frame/cam
			const auto& detected_calibration_board_data_map = camera_measurement_pair.second.calibration_board_data_map;

			// check that both boards have been detected
			if(detected_calibration_board_data_map.find(first_board_id) == detected_calibration_board_data_map.end() ||
				detected_calibration_board_data_map.find(second_board_id) == detected_calibration_board_data_map.end())
			{
				continue;
			}

			// Get the CameraSensor for SolvePNP to both boards, needs to be initialized with approx. model parameters
			auto calibration = camera_parameter_optimizer->GetCalibration(camera_measurement_pair.first);

			// Access 2d measurements of both boards
			const auto& first_calibration_board_data_map  = detected_calibration_board_data_map.at(first_board_id);
			const auto& second_calibration_board_data_map = detected_calibration_board_data_map.at(second_board_id);

			// access 3d board local coordinates of board marker
			const auto& first_board_markers_map  = GetBoard(first_board_id)->GetMarkerPositionMap();
			const auto& second_board_markers_map = GetBoard(second_board_id)->GetMarkerPositionMap();

			// 3d Points in board local coordinates for both boards
			std::vector<cv::Point3d> first_board_markers_3d_positions, second_board_markers_3d_positions;

			// Add all first board local 3D marker coordinates of detected board marker
			for(auto marker_id : first_calibration_board_data_map.marker_ids)
			{
				first_board_markers_3d_positions.push_back(first_board_markers_map.at(marker_id));
			}

			// Add all second board local 3D marker coordinates of detected board marker
			for(auto marker_id : second_calibration_board_data_map.marker_ids)
			{
				second_board_markers_3d_positions.push_back(second_board_markers_map.at(marker_id));
			}

			// Calculate transformation between first board and current camera
			cv::Mat rotation_vector1, translation_vector1;

			if(first_board_markers_3d_positions.size() < 6 || !calibration->SolvePnP(first_board_markers_3d_positions,
			                                                                         Helper::GetRawEllipseCenters<double>(
				                                                                         first_calibration_board_data_map.markers),
			                                                                         rotation_vector1,
			                                                                         translation_vector1))
			{
				continue;
			}

			// Calculate transformation between second board and current camera
			cv::Mat rotation_vector2, translation_vector2;
			if(second_board_markers_3d_positions.size() < 6 || !calibration->SolvePnP(second_board_markers_3d_positions,
			                                                                          Helper::GetRawEllipseCenters<double>(
				                                                                          second_calibration_board_data_map.markers),
			                                                                          rotation_vector2,
			                                                                          translation_vector2))
			{
				continue;
			}

			// create Transformation objects from cv rvec, tvec representation
			auto first_board_transformation  = Transformation(rotation_vector1, translation_vector1);
			auto second_board_transformation = Transformation(rotation_vector2, translation_vector2);

			// collect relative transformation from first to second board...mean calculated later
			auto relative_transformation_first_to_second = first_board_transformation.GetRelTransformation(second_board_transformation);

			auto minimum_marker_count = std::min(first_board_markers_3d_positions.size(), second_board_markers_3d_positions.size());

			if(minimum_marker_count > maximum_marker_count)
			{
				maximum_marker_count      = minimum_marker_count;
				max_marker_transformation = relative_transformation_first_to_second;
			}

			transformations.push_back(relative_transformation_first_to_second);
		}
	}

	relative_transformation = max_marker_transformation;

	return int(transformations.size());
}

void RelativeFixBoardsWithOverlapTransformation::SetInitialParameters(const ObservationMap& observations,
                                                                      const std::shared_ptr<const CameraParameterOptimizer>
                                                                      camera_parameter_optimizer,
                                                                      const std::shared_ptr<const CameraSystemTransformation>
                                                                      camera_system_transformation)
{
	RelativeFixCalibrationBoardsTransformation::SetInitialParameters(observations, camera_parameter_optimizer, camera_system_transformation);

	// assumes that all cams contains valid calibrations for projecting points.
	// set all boards expect reference board to "not yet initialized"
	std::vector<size_t> not_yet_initialized_board_ids;
	std::vector<size_t> docking_board_ids;
	std::set<size_t> board_ids = GetBoardIDs();

	for(const auto board_id : board_ids)
	{
		if(board_id == reference_board_id_)
		{
			docking_board_ids.push_back(board_id);

			// reference board is always initialized, it defines the boards reference coordinate system
			initialized_boards_map_[reference_board_id_] = true;
			continue;
		}

		if(boards_fixed_in_position_.find(board_id) != boards_fixed_in_position_.end())
		{
			docking_board_ids.push_back(board_id);
			continue;
		}

		not_yet_initialized_board_ids.push_back(board_id);
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Init board [" << reference_board_id_ << "] as reference board ";

	while(!docking_board_ids.empty())
	{
		std::vector<size_t> next_docking_boards;
		std::vector<size_t> still_not_initialized;

		// try all not initialized boards to init by detection of relative pose to at least one "docking board"
		for(auto board_id : not_yet_initialized_board_ids)
		{
			// try all recently initialized boards as "docking boards"
			for(auto docking_board_id : docking_board_ids)
			{
				Transformation transformation;
				const auto num_detection = GetRelativeTransformation(transformation,
				                                                     docking_board_id,
				                                                     board_id,
				                                                     observations,
				                                                     camera_parameter_optimizer,
				                                                     camera_system_transformation);

				if(num_detection > 0)
				{
					board_transformation_map_[board_id] = board_transformation_map_[docking_board_id].Multiply(transformation);
					initialized_boards_map_[board_id]   = true;
					next_docking_boards.push_back(board_id);
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Init Board [" << board_id << "] from " << docking_board_id;
					break;
				}
			}

			// check if the board is initialized now
			if(!initialized_boards_map_[board_id])
			{
				still_not_initialized.push_back(board_id);
			}
		}

		// try all remaining uninitialized to initialize via recently initialized
		docking_board_ids             = next_docking_boards;
		not_yet_initialized_board_ids = still_not_initialized;
	}

	if(!not_yet_initialized_board_ids.empty())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : ---------------- NOT ALL BOARDS INITIALIZED ---------------";
	}
}
