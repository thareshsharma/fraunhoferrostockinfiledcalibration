#include "relative_fix_cameras_parameter_optimizer.h"
#include "helper.h"
#include "calibration_target_fix_markers.h"

namespace DMVS
{
namespace CameraCalibration
{
RelativeFixCamerasParameterOptimizer::RelativeFixCamerasParameterOptimizer(const std::set<size_t>& camera_ids,
                                                                           const std::shared_ptr<const CameraSensor>& initial_calibration,
                                                                           const size_t reference_camera_id) :
	CameraParameterOptimizer(camera_ids, initial_calibration, reference_camera_id),
	initialization_done_(true),
	required_marker_count_(4)
{
}

RelativeFixCamerasParameterOptimizer::RelativeFixCamerasParameterOptimizer(const std::map<size_t, std::shared_ptr<const CameraSensor>>& camera_map,
                                                                           const ImageChannel reference_camera_id) :
	CameraParameterOptimizer(camera_map, reference_camera_id),
	initialization_done_(true),
	required_marker_count_(4)
{
}

void RelativeFixCamerasParameterOptimizer::TakeFromParameters(std::vector<double>& parameters)
{
	const auto initial_param_count  = parameters.size();
	const auto required_param_count = GetRequiredParameterCount();

	if(initial_param_count < required_param_count)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not enough parameters. Params Available [" << initial_param_count << "] Required Params ["
 << required_param_count << "]";
		throw std::exception("Not enough parameters.");
	}

	// take parameters for all cameras from parameters vector
	for(auto& camera_pair : camera_sensor_map_)
	{
		// number of free parameter for this single camera
		const auto free_parameters_count = GetFreeParametersFromCalibration(camera_pair.second).size();

		// take camera parameter vector from front, parameter vectors are pushed to vector
		// at vector creation in getParameters() and cameras are iterated in the same order.
		std::vector<double> tmp(parameters.begin(), parameters.begin() + free_parameters_count);
		camera_pair.second = GetModifiedCalibration(camera_pair.second, tmp);

		// remove used parameters from front of parameter vector
		parameters = std::vector<double>(parameters.begin() + free_parameters_count, parameters.end());
	}
}

void RelativeFixCamerasParameterOptimizer::TakeFromProjectorParameters(std::vector<double>& parameters)
{
	assert(parameters.size() == 6);

	auto camera = camera_sensor_map_[CHANNEL_PROJECTOR0];
	{
		const cv::Mat rotation_vector = (cv::Mat_<double>(3, 1) << parameters[0], parameters[1], parameters[2]);
		const cv::Mat translation_vector = (cv::Mat_<double>(3, 1) << parameters[3], parameters[4], parameters[5]);
		cv::Mat rotation_matrix;
		cv::Rodrigues(rotation_vector, rotation_matrix);
		assert(rotation_matrix.cols == 3 && rotation_matrix.rows == 3);

		camera->SetExtrinsicParameters(rotation_matrix, translation_vector);

		// remove used parameters from front of parameter vector
		parameters = std::vector<double>();
	}
}

std::shared_ptr<CameraParameterOptimizer> RelativeFixCamerasParameterOptimizer::Duplicate() const
{
	std::set<size_t> camera_ids;
	for(const auto& pair : camera_sensor_map_)
	{
		camera_ids.insert(pair.first);
	}

	auto out = std::make_shared<RelativeFixCamerasParameterOptimizer>(
		RelativeFixCamerasParameterOptimizer(camera_ids, camera_sensor_map_.begin()->second->Duplicate(), reference_camera_id_));
	out->SetReferenceCameraIsOrigin(this->GetReferenceCameraIsOrigin());
	auto params = this->GetParameters();
	out->TakeFromParameters(params);

	return out;
}

void RelativeFixCamerasParameterOptimizer::SetInitialParameters(const ObservationMap& observations,
                                                                std::shared_ptr<const CameraSystemTransformation> camera_system_transformation,
                                                                std::shared_ptr<const CalibrationBoardsTransformation> boards_transformation)
{
	//Assumes that camera_system_transformation has been initialized and boards_transformation has been initialized
	if(initialization_done_)
		return;

	// Add all cams expect reference cam to "not yet initialized"
	std::vector<size_t> cameras_not_yet_initialized;

	for(auto calibration : GetAllCalibrations())
	{
		if(calibration.first == reference_camera_id_)
			continue;

		cameras_not_yet_initialized.push_back(calibration.first);
	}

	// start initialization from reference camera, i.e. reference camera has transformation 0|E
	std::vector<size_t> docking_camera_ids;
	docking_camera_ids.push_back(reference_camera_id_);

	while(!docking_camera_ids.empty())
	{
		std::vector<size_t> next_docking_cams;
		std::vector<size_t> still_not_initialized;

		// try all not initialized cameras to be initialized by detection of relative pose to at least one "docking camera"
		for(auto camera_not_yet_initialized : cameras_not_yet_initialized)
		{
			// try all recently initialized cameras as "docking cams"
			auto is_initialization_done = false;

			for(auto docking_camera_id : docking_camera_ids)
			{
				Transformation transformation;
				auto relative_transformation_count = GetRelativeTransformation(transformation,
				                                                               docking_camera_id,
				                                                               camera_not_yet_initialized,
				                                                               observations,
				                                                               camera_system_transformation,
				                                                               boards_transformation);

				if(relative_transformation_count)
				{
					Transformation docked_camera_transformation(camera_sensor_map_[docking_camera_id]);
					Transformation accumulated_transformation = docked_camera_transformation.Multiply(transformation);

					accumulated_transformation.SetToCalibration(camera_sensor_map_[camera_not_yet_initialized]);
					next_docking_cams.push_back(camera_not_yet_initialized);
					is_initialization_done = true;

					auto parameter_vector = accumulated_transformation.Serialized();
					auto rotation         = accumulated_transformation.GetRotationalTransformation();
					auto translation      = accumulated_transformation.GetTranslationalTransformation();

					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Camera " << &(*camera_sensor_map_[camera_not_yet_initialized]) <<
					" Initialized Camera " << camera_not_yet_initialized << " from " << docking_camera_id <<
					" Location " << parameter_vector[3] << " " << parameter_vector[4] << " " << parameter_vector[5] <<
					" Rotation " << parameter_vector[0] << " " << parameter_vector[1] << " " << parameter_vector[2];
					break;
				}
			}

			// check if the camera is initialized now
			if(!is_initialization_done)
			{
				still_not_initialized.push_back(camera_not_yet_initialized);
			}
		}

		// try all remaining uninitialized cameras to be initialized in the next run
		docking_camera_ids          = next_docking_cams;
		cameras_not_yet_initialized = still_not_initialized;
	}

	return;
}

void RelativeFixCamerasParameterOptimizer::SetCalibration(std::map<size_t, std::shared_ptr<const CameraSensor>> calibrations)
{
	for(auto& camera_sensor_pair : camera_sensor_map_)
	{
		camera_sensor_pair.second = calibrations.at(camera_sensor_pair.first)->Duplicate();
	}
}

//void RelativeFixCamerasParameterOptimizer::SetCalibration(const ImageChannel channel,
//                                                          const std::shared_ptr<const CameraSensor> calibration)
//{
//	camera_sensor_map_[channel] = calibration;
//}

void RelativeFixCamerasParameterOptimizer::SetRequiredNumberMarkers(const size_t required_marker_count)
{
	required_marker_count_ = required_marker_count;
}

size_t RelativeFixCamerasParameterOptimizer::GetRequiredParameterCount() const
{
	size_t overall_params_count = 0;

	for(auto& camera_map : camera_sensor_map_)
	{
		overall_params_count += GetFreeParametersFromCalibration(camera_map.second).size();
	}

	return overall_params_count;
}

std::vector<double> RelativeFixCamerasParameterOptimizer::GetParameters() const
{
	std::vector<double> out;

	for(const auto& camera : camera_sensor_map_)
	{
		//get intrinsic parameters for all camera
		const auto tmp = GetFreeParametersFromCalibration(camera.second);
		out.insert(out.end(), tmp.begin(), tmp.end());
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Parameters count [" << out.size() << "] ";

	return out;
}

std::vector<double> RelativeFixCamerasParameterOptimizer::GetProjectorParameters() const
{
	std::vector<double> out;

	auto camera = camera_sensor_map_.find(CHANNEL_PROJECTOR0);
	{
		cv::Mat rotation_matrix, rotation_vector, translation_vector;
		camera->second->GetExtrinsicParameters(rotation_matrix, translation_vector);

		cv::Rodrigues(rotation_matrix, rotation_vector);

		out.push_back(rotation_vector.at<double>(0, 0));
		out.push_back(rotation_vector.at<double>(1, 0));
		out.push_back(rotation_vector.at<double>(2, 0));
		out.push_back(translation_vector.at<double>(0, 0));
		out.push_back(translation_vector.at<double>(1, 0));
		out.push_back(translation_vector.at<double>(2, 0));
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Parameters count [" << out.size() << "] ";

	return out;
}

//std::shared_ptr<const CameraSensor2> RelativeFixCameras::GetCalibration(size_t cam_id) const
//{
//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Function not Implemented";
//	throw std::exception("Function not Implemented");
//}
//
//std::map<size_t, std::shared_ptr<const CameraSensor2>> RelativeFixCameras::GetAllCalibrations() const
//{
//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Function not Implemented";
//	throw std::exception("Function not Implemented");
//}

size_t RelativeFixCamerasParameterOptimizer::GetRelativeTransformation(Transformation& relative_transformation,
                                                                       CameraID first_camera_id,
                                                                       CameraID second_camera_id,
                                                                       const ObservationMap& observations,
                                                                       std::shared_ptr<const CameraSystemTransformation>
                                                                       camera_system_transformation,
                                                                       std::shared_ptr<const CalibrationBoardsTransformation> boards) const
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Try to calculate relative transformation of camera [" << first_camera_id << "] to camera [" <<
 second_camera_id << "]";

	// check for all frame/board pairs whether the two cams can be aligned with
	std::vector<Transformation> transformations;

	for(const auto& frame : observations.frames_measurement_map)
	{
		// check existence of both cams in the current frame
		const auto& cameras_in_frame = frame.second.camera_measurement_map;

		if(cameras_in_frame.find(first_camera_id) == cameras_in_frame.end() ||
			cameras_in_frame.find(second_camera_id) == cameras_in_frame.end())
		{
			continue;
		}

		const auto& boards_in_first_camera = cameras_in_frame.at(first_camera_id).calibration_board_data_map;
		const auto& second_cam_boards      = cameras_in_frame.at(second_camera_id).calibration_board_data_map;

		auto first_camera  = GetCalibration(first_camera_id);
		auto second_camera = GetCalibration(second_camera_id);

		// check if a board has been seen in both cameras
		for(const auto& board_in_first_camera : boards_in_first_camera)
		{
			// check if board also detected in second camera
			size_t board_id = board_in_first_camera.first;

			if(second_cam_boards.find(board_id) == second_cam_boards.end())
			{
				continue;
			}

			// Board measurements of both cameras containing 2d measurements of the board
			const auto& first_cam_board_meas  = board_in_first_camera.second;
			const auto& second_cam_board_meas = second_cam_boards.at(board_in_first_camera.first);

			// Board data containing 3d marker positions of the board,
			const auto board_data = boards->GetBoard(board_id)->GetMarkerPositionMap();

			// Try to calculate both cams poses relative to the board
			Transformation first_camera_transformation, second_camera_transformation;
			{
				std::vector<cv::Point3d> points_3d;

				for(const auto& marker : first_cam_board_meas.marker_ids)
				{
					points_3d.push_back(board_data.at(marker));
				}

				cv::Mat rotation_vector, translation_vector;
				auto meas2d = Helper::GetMarkersCenter<double>(first_cam_board_meas.markers);

				if(points_3d.size() < required_marker_count_ || !first_camera->SolvePnP(points_3d, meas2d, rotation_vector, translation_vector))
				{
					continue;
				}

				first_camera_transformation = Transformation(rotation_vector, translation_vector);
			}

			// try to align second cam to the board
			{
				std::vector<cv::Point3d> points_3d;
				for(const auto& marker : second_cam_board_meas.marker_ids)
				{
					points_3d.push_back(board_data.at(marker));
				}

				auto meas2d = Helper::GetMarkersCenter<double>(second_cam_board_meas.markers);

				cv::Mat rotation_vector, translation_vector;

				if(points_3d.size() < required_marker_count_ || !second_camera->SolvePnP(points_3d, meas2d, rotation_vector, translation_vector))
				{
					continue;
				}

				second_camera_transformation = Transformation(rotation_vector, translation_vector);
			}

			// calculate relative pose from cam1 to cam 2 via board
			auto first_camera_to_second_camera = second_camera_transformation.Multiply(first_camera_transformation.GetInverse());
			transformations.push_back(first_camera_to_second_camera);
		}
	}

	// calculate mean parameters of relative transformation between first and second board
	std::vector<double> mean_transformation(6, 0.0);
	const auto size = transformations.size();

	for(auto i = 0; i < size; ++i)
	{
		auto transformation = transformations[i].Serialized();

		for(auto j = 0; j < size; ++j)
		{
			mean_transformation[j] += transformation[j];
		}
	}

	for(auto i = 0; i < size; ++i)
	{
		mean_transformation[i] /= double(size);
	}

	// copy result data, return number of trafos as quality indicator
	relative_transformation = Transformation(mean_transformation);

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_END;

	return size;
}

std::shared_ptr<CameraSensor> RelativeFixCamerasParameterOptimizer::GetModifiedCalibration(std::shared_ptr<CameraSensor> calibration,
                                                                                           const std::vector<double>& parameters)
{
	auto fixations  = calibration->GetFixations();
	auto all_params = calibration->GetParamsVector();

	// Create mapping between free parameters and all parameters
	size_t modified_idx = 0;
	for(auto idx = 0; idx < all_params.size(); ++idx)
	{
		// parameter is fix, do not apply any change
		if(fixations[idx] == Constants::DEFAULT_SIZE_T_MAX_VALUE)
			continue;

		// parameter is free, apply change from modified params
		if(fixations[idx] == idx)
		{
			all_params[idx] = parameters[modified_idx];
			modified_idx++;
			continue;
		}

		// parameter is equal to earlier free parameter, copy parameter
		if(fixations[idx] < idx)
		{
			all_params[idx] = all_params[fixations[idx]];
			continue;
		}

		// invalid uncaught state
		assert(false);
	}

	assert(parameters.size() == modified_idx);
	calibration->InitFromParamsVector(all_params);

	return calibration;
}

std::vector<double> RelativeFixCamerasParameterOptimizer::GetFreeParametersFromCalibration(const std::shared_ptr<const CameraSensor> calibration)
{
	// Get the full parameter vector of the SingleCameraCalibration and information about which
	// parameters are free for optimization.
	auto fixations  = calibration->GetFixations();
	auto all_params = calibration->GetParamsVector();

	// Create the parameter vector with free parameters
	std::vector<double> free_to_change_params;

	for(size_t idx = 0; idx < all_params.size(); ++idx)
	{
		if(fixations[idx] == idx)
			free_to_change_params.push_back(all_params[idx]);
	}

	return free_to_change_params;
}
}
}
