#include "calibration_target_fix_markers.h"
#include "constants.h"

namespace DMVS
{
namespace CameraCalibration
{
CalibrationTargetFixMarkers::CalibrationTargetFixMarkers(const std::map<size_t, cv::Point3d>& markers)
	: markers_(markers)
{
}

cv::Point3d CalibrationTargetFixMarkers::GetMarkerPosition(const size_t marker_id) const
{
	const auto marker = markers_.find(marker_id);

	if(marker == markers_.end())
		throw std::exception("Marker not found.");

	return marker->second;
}

void CalibrationTargetFixMarkers::TakeFromParameters(std::vector<double>& parameters)
{
	//BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED_IN_SCENE;
}

size_t CalibrationTargetFixMarkers::GetRequiredParameterCount() const
{
	//BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED_IN_SCENE;
	return 0;
}

std::vector<double> CalibrationTargetFixMarkers::GetParameters() const
{
	return std::vector<double>();
}

void CalibrationTargetFixMarkers::SetInitialParameters(const ObservationMap& observations,
                                                       std::shared_ptr<const CameraParameterOptimizer> cams,
                                                       std::shared_ptr<const CameraSystemTransformation> camera_system_transformation,
                                                       size_t target_id)
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED_IN_SCENE;
}

const std::map<BoardID, cv::Point3d> CalibrationTargetFixMarkers::GetMarkerPositionMap() const
{
	return markers_;
}
}
}
