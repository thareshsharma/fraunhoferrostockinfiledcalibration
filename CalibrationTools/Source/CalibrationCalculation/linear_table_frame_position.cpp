#include "linear_table_frame_position.h"
#include "transformation.h"
#include "helper.h"
#include "calibration_target_fix_markers.h"

namespace DMVS
{
namespace CameraCalibration
{
LinearTableFramePosition::LinearTableFramePosition(const std::map<size_t, double>& linear_table_positions) :
	linear_table_positions_(linear_table_positions), frame_dof_(3)
{
	for(auto& linear_table_position : linear_table_positions)
	{
		frame_transformation_map_[linear_table_position.first] = Transformation();
	}
}

size_t LinearTableFramePosition::GetRequiredParameterCount() const
{
	//2 dof direction (x,y @z=1) of linear table
	size_t out = 2;

	//position of linear table
	out += 3;

	//rotation is free for each linear table position
	out += frame_transformation_map_.size() * frame_dof_;

	return out;
}

void LinearTableFramePosition::TakeFromParameters(std::vector<double>& parameters)
{
	if(parameters.size() < GetRequiredParameterCount())
		return;

	auto iterator = parameters.begin();

	//Get pose of linear table from parameters
	const auto dir_x = *(iterator++);
	const auto dir_y = *(iterator++);

	Eigen::Vector3d pos;
	pos(0) = *(iterator++);
	pos(1) = *(iterator++);
	pos(2) = *(iterator++);

	linear_table_to_board_ = GetLinearTableToBoardTransformationFromParams(pos, dir_x, dir_y);

	// Get Transformation for every frame based on its Transformation relative to linear table
	// and the linear table Transformation
	for(auto& frame_position : frame_transformation_map_)
	{
		// Apply linear shift to each camera based on its linear table position
		std::vector<double> frame_to_linear;

		for(auto idx = 0; idx < frame_dof_; ++idx)
		{
			frame_to_linear.push_back(*(iterator++));
		}

		// add translation along z axis (moving direction of linear table)
		frame_to_linear.push_back(0.0);
		frame_to_linear.push_back(0.0);
		frame_to_linear.push_back(linear_table_positions_.at(frame_position.first));

		Transformation frame_to_linear_transformation(frame_to_linear);
		Transformation linear_table_to_camera_transformation = frame_to_linear_transformation.GetInverse();

		// Frame transformation is base to cam: base to linTable then linTable to cam
		frame_position.second = linear_table_to_camera_transformation.Multiply(linear_table_to_board_.GetInverse());
	}

	// Removed used parameters from parameter vector
	parameters = std::vector<double>(iterator, parameters.end());
}

std::vector<double> LinearTableFramePosition::GetParameters() const
{
	// All parameter vector to be filled
	std::vector<double> out;

	// Add the parameters defining pose of linear table
	auto linear_table = GetParamsFromLinearTableTransformation(linear_table_to_board_);
	out.insert(out.end(), linear_table.begin(), linear_table.end());

	// Add 3 rotation parameters for each frame
	for(const auto& frame_position : frame_transformation_map_)
	{
		// FramePositions contains the baseToFrame Transformation for each frame
		// for the parameters we need the linTable to frame Transformations
		auto board_to_frame        = frame_position.second;
		auto linear_table_to_board = linear_table_to_board_;
		auto linear_table_to_frame = board_to_frame.Multiply(linear_table_to_board);
		auto frame_to_liner_table  = linear_table_to_frame.GetInverse();

		auto serialize_relative = frame_to_liner_table.Serialized();
		out.push_back(serialize_relative[0]);
		out.push_back(serialize_relative[1]);
		out.push_back(serialize_relative[2]);
	}

	return out;
}

void LinearTableFramePosition::SetInitialParameters(const ObservationMap& observations,
                                                    const std::shared_ptr<const CameraParameterOptimizer> camera_param_optimizer,
                                                    const std::shared_ptr<const CalibrationBoardsTransformation> boards)
{
	// assumes that cams and boards have been initialized
	// uses only reference cam, i.e. at least one board has to be detected in the reference cam in all frames
	std::map<size_t, Transformation> reference_board_to_frame_transformation;

#pragma region CALC_BOARD_TO_FRAME_TRAFOS
	// for each frame...
	// frame is an element of FramesType: std::map< int-frameID , FrameMeasurements>
	// FrameMeasurements contains all boards all cams measurements of one frame
	for(const auto& frame_measurement_map : observations.frames_measurement_map)
	{
		// for reference cam only...
		// cam is an element of camsType: std::map< int-camID, CamMeasurements >
		// CamMeasurements contains all board measurements of one cam of one frame
		for(const auto& camera_measurement : frame_measurement_map.second.camera_measurement_map)
		{
			if(camera_measurement.first != camera_param_optimizer->GetReferenceCameraId())
			{
				continue;
			}

			auto calibration = camera_param_optimizer->GetCalibration(camera_measurement.first);

			// find all detected boards and collect the 2d / 3d correspondences
			// 3d marker in reference board coordinates
			std::vector<cv::Point3d> pts3d;
			std::vector<cv::Point2d> pts2d;

			// board is an element of BoardsType: std::vector< int boardID, BoardMeasurementType>
			// it contains the measurements of one board of one cam of one frame
			for(const auto& board_data_map : camera_measurement.second.calibration_board_data_map)
			{
				const auto calibration_board_id           = board_data_map.first;
				const auto& calibration_board_measurement = board_data_map.second;
				const auto& marker_ids                    = calibration_board_measurement.marker_ids;
				//const auto& circular_markers = calibration_board_measurement.markers;

				// get 3d positions in reference board coordinates for all markers of the board
				for(const auto& marker_id : marker_ids)
				{
					// getMarkerPosition returns the 3d position in reference board coordinates
					// relative board positions are already initialized).
					pts3d.push_back(boards->GetMarkerPosition(calibration_board_id, marker_id));
				}

				// markerIDs and circularMarkers are always in sync.
				auto meas2d = Helper::GetMarkersCenter<double>(board_data_map.second.markers);
				pts2d.insert(pts2d.end(), meas2d.begin(), meas2d.end());
			}

			cv::Mat rotation_vector, translation_vector;

			if(pts3d.size() > 3 && calibration->SolvePnP(pts3d, pts2d, rotation_vector, translation_vector))
			{
				reference_board_to_frame_transformation[frame_measurement_map.first] = Transformation(rotation_vector, translation_vector);
			}
		}
	}

	if(reference_board_to_frame_transformation.size() < frame_transformation_map_.size())
	{
		return;
	}
#pragma endregion CALC_BOARD_TO_FRAME_TRAFOS

	std::vector<cv::Point3f> frame_positions;
	for(const auto& frame : reference_board_to_frame_transformation)
	{
		// get the position of the frame in board coordinates
		// Position of BoardToFrame is the position of the boards origin in frame coordinate system
		// we want the position of the frame in board coordinate system i.e. the position of trans.inverse()
		auto translation_transformation = frame.second.GetInverse().GetTranslationalTransformation();
		cv::Point3f frame_position(float(translation_transformation.at<double>(0, 0)),
		                           float(translation_transformation.at<double>(1, 0)),
		                           float(translation_transformation.at<double>(2, 0)));

		frame_positions.push_back(frame_position);
	}

	//We need to find the direction to apply the linear table transformations onto
	auto sum_z = 0.0f;
	////////////////////////////////////////////////////////////////////////////////////////////////- CHECK THIS
	// Here it is assumed that origin is known and we move either in +z direction or -z direction in a single measurement.
	//Model is based on moving in roughly Z direction
	for(const auto& frame_pos : frame_positions)
	{
		sum_z += frame_pos.z;
	}

	////If we are moving in the negative direction invert the calculation
	//if(sum_z < 0.0f)
	//{
	//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Inverting the direction as sum_z [" << sum_z << "]";
	//	for(auto& linear_table_position : linear_table_positions_)
	//	{
	//		linear_table_position.second = -linear_table_position.second;
	//	}
	//}

	////////////////////////////////////////////////////////////////////////////////////////////////

	// Fit a line to the frame positions
	cv::Vec6f line_params;

	// returns direction 0..2 and position 3..5
	cv::fitLine(frame_positions, line_params, cv::DIST_HUBER, 0.5, 0.001, 0.001);

	// make Z positive and normalize
	Eigen::Vector3d v(line_params[0] / line_params[2], line_params[1] / line_params[2], 1);
	v.normalize();

	// calculate the zero position of the linear table from all frames and its known linearTablePositions
	Eigen::Vector3d origin(0, 0, 0);
	for(const auto& frame : reference_board_to_frame_transformation)
	{
		// get position of frame in board coordinate system
		auto translational_transformation = frame.second.GetInverse().GetTranslationalTransformation();
		auto frame_position               = Eigen::Vector3d(translational_transformation.at<double>(0, 0),
		                                                    translational_transformation.at<double>(1, 0),
		                                                    translational_transformation.at<double>(2, 0));

		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : frame.first [" << frame.first << "] linear_table_positions_.size()" << linear_table_positions_.size();
		auto linear_table_pos = linear_table_positions_.at(frame.first);
		origin                = origin + frame_position - linear_table_pos * v;
	}

	origin /= double(reference_board_to_frame_transformation.size());

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Linear Table : Origin  [" << origin[0] << " " << origin[1] << " " << origin[2] << "]";

	linear_table_to_board_ = GetLinearTableToBoardTransformationFromParams(origin, v[0] / v[2], v[1] / v[2]);

	for(const auto& frame : reference_board_to_frame_transformation)
	{
		frame_transformation_map_[frame.first] = frame.second;
	}

	std::vector<cv::Point3f> fr_check;
	for(auto& frame_position : frame_transformation_map_)
	{
		/*auto board_to_frame  = frame_position.second;
		auto linear_to_board = linear_table_to_board_;
		auto linear_to_frame = board_to_frame.Multiply(linear_to_board);
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Board to Frame [" << board_to_frame << "]";
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Linear to Board [" << linear_to_board << "]";
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Linear to Frame [" << linear_to_frame << "]";*/
	}

	fr_check.clear();
	for(auto& frame_position : frame_transformation_map_)
	{
		/*	auto board_to_frame  = frame_position.second;
			auto linear_to_board = linear_table_to_board_;
			auto linear_to_frame = board_to_frame.Multiply(linear_to_board);
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Reconstructed board to frame [" << board_to_frame << "]";
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Reconstructed linear to board [" << linear_to_board << "]";
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Reconstructed linear to frame [" << linear_to_frame << "]";*/
	}
}

Transformation LinearTableFramePosition::GetLinearTableToBoardTransformationFromParams(const Eigen::Vector3d& pos,
                                                                                       const double dir_x,
                                                                                       const double dir_y) const
{
	// Linear table direction is parameterized as vector (x,y,1) and defines Z axis of linear table coordinate system
	Eigen::Vector3d z_axis(dir_x, dir_y, 1);
	z_axis.normalize();

	auto x_axis = Eigen::Vector3d::UnitY().cross(z_axis);
	x_axis.normalize();

	auto y_axis = z_axis.cross(x_axis);
	y_axis.normalize();

	// create rotation part of Transformation...
	Eigen::Matrix3d m;
	m.col(0) = x_axis;
	m.col(1) = y_axis;
	m.col(2) = z_axis;
	cv::Mat rot;
	cv::eigen2cv(m, rot);

	//create translation part of Transformation..
	cv::Mat trans;
	eigen2cv(pos, trans);

	//Create and return Transformation from rotation ans translation from cv types...
	return Transformation(rot, trans);
}

std::vector<double> LinearTableFramePosition::GetParamsFromLinearTableTransformation(const Transformation& linear_table_pose)
{
	std::vector<double> out;

	//Add 5 parameters for linear table direction and position
	auto r_mat = linear_table_pose.GetRotationalTransformation();

	//The last column of the the rotation matrix contains the image of the z axis
	cv::Vec3d z_axis(r_mat.at<double>(0, 2), r_mat.at<double>(1, 2), r_mat.at<double>(2, 2));
	const auto x_dir = z_axis[0] / z_axis[2];
	const auto y_dir = z_axis[1] / z_axis[2];

	out.push_back(x_dir);
	out.push_back(y_dir);

	auto translation_vector = linear_table_pose.GetTranslationalTransformation();
	out.push_back(translation_vector.at<double>(0, 0));
	out.push_back(translation_vector.at<double>(1, 0));
	out.push_back(translation_vector.at<double>(2, 0));

	return out;
}

void LinearTableFramePosition::PreInit()
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED_IN_SCENE;
}

std::shared_ptr<CameraSystemTransformation> LinearTableFramePosition::Duplicate() const
{
	throw std::exception(Constants::FUNCTION_NOT_IMPLEMENTED.c_str());
}
}
}
