#include "relative_fix_boards_transformation.h"

RelativeFixCalibrationBoardsTransformation::RelativeFixCalibrationBoardsTransformation(
	const std::map<BoardID, std::shared_ptr<CalibrationTargetBase>>& boards,
	const std::set<BoardID>& boards_to_fix /* = std::set<BoardID>() */) :
	CalibrationBoardsTransformation(boards),
	boards_fixed_in_position_(boards_to_fix)
{
	for(const auto& board_id : boards_fixed_in_position_)
	{
		if(board_data_map_.find(board_id) == board_data_map_.end())
			throw std::exception("Board set to fixed that does not exist.");
	}
}

size_t RelativeFixCalibrationBoardsTransformation::GetRequiredParameterCount() const
{
	const auto base_num_parameters = CalibrationBoardsTransformation::GetRequiredParameterCount();

	// if boards_fixed_in_position_ contains the reference board, do not count this
	// board. This is per-se fixed in position as it is the origin.
	auto num_boards_fix_in_position = boards_fixed_in_position_.size();

	if(reference_board_id_ >= 0 && boards_fixed_in_position_.find(GetReferenceBoardID()) != boards_fixed_in_position_.end())
		num_boards_fix_in_position--;

	return base_num_parameters + int((board_data_map_.size() - num_boards_fix_in_position - 1) * 6);
}

void RelativeFixCalibrationBoardsTransformation::TakeFromParameters(std::vector<double>& parameters)
{
	if(int(parameters.size() < GetRequiredParameterCount()))
		throw std::exception("Not enough parameters to set board positions.");

	CalibrationBoardsTransformation::TakeFromParameters(parameters);

	for(const auto& board : board_data_map_)
	{
		if(board.first == GetReferenceBoardID())
			continue;

		if(boards_fixed_in_position_.find(board.first) == boards_fixed_in_position_.end())
		{
			//check if this board is fix in position, only if not, overwrite the position
			std::vector<double> tmp(parameters.begin(), parameters.begin() + 6);
			board_transformation_map_[board.first] = Transformation(tmp);
			parameters                             = std::vector<double>(parameters.begin() + 6, parameters.end());
		}
	}
}

std::vector<double> RelativeFixCalibrationBoardsTransformation::GetParameters() const
{
	auto out = CalibrationBoardsTransformation::GetParameters();

	for(const auto& board : board_data_map_)
	{
		if(board.first == GetReferenceBoardID())
			continue;

		if(boards_fixed_in_position_.find(board.first) == boards_fixed_in_position_.end())
		{
			//write parameters of board only if it is not set fixed
			const auto tmp = board_transformation_map_.at(board.first).Serialized();
			out.insert(out.end(), tmp.begin(), tmp.end());
		}
	}

	return out;
}

void RelativeFixCalibrationBoardsTransformation::SetRelativePositions(const std::map<size_t, Transformation>& board_positions_in)
{
	board_transformation_map_ = board_positions_in;
}

void RelativeFixCalibrationBoardsTransformation::UnfreezeAllBoardPositions()
{
	boards_fixed_in_position_.clear();
}
