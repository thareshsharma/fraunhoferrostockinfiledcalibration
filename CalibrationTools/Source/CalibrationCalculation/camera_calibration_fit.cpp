#include "camera_calibration_fit.h"
#include "ellipse_detection_distortion_correction.h"
#include "helper.h"

namespace DMVS
{
namespace CameraCalibration
{
CameraCalibrationFit::CameraCalibrationFit(const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer,
                                           const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
                                           const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
                                           const VectorObservationPtr& observations,
                                           const bool use_raw_ellipse_center,
                                           const OptimizationMode optimization_mode) :
	GeneralFitFunctor<double>(int(CountParameters(camera_parameter_optimizer, camera_system_transformation, boards_transformation)),
	                          int(CountValues(observations, optimization_mode))),
	camera_parameter_optimizer_(camera_parameter_optimizer),
	boards_transformation_(boards_transformation),
	camera_system_transformation_(camera_system_transformation),
	observations_(GetObservationMap(observations)),
	use_raw_ellipse_center_(use_raw_ellipse_center),
	optimization_mode_(optimization_mode),
	num_params_(CountParameters(camera_parameter_optimizer, camera_system_transformation, boards_transformation)),
	num_values_(CountValues(observations, optimization_mode))
{
}

void CameraCalibrationFit::FromEigen(const Eigen::VectorXd& parameters_eigen_vector,
                                     const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer,
                                     const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
                                     const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation)
{
	//Convert parameters to std vector
	/*std::vector<double> parameters;

	for(auto i = 0; i < parameters_eigen_vector.size(); ++i)
		parameters.push_back(parameters_eigen_vector[i]);*/

	std::vector<double> parameters(parameters_eigen_vector.data(),
	                               parameters_eigen_vector.data() + parameters_eigen_vector.rows() * parameters_eigen_vector.cols());

	//Each object removes its parameters!
	camera_parameter_optimizer->TakeFromParameters(parameters);
	camera_system_transformation->TakeFromParameters(parameters);
	boards_transformation->TakeFromParameters(parameters);
	//boards_transformation->UpdateCameraDependentMarkers(camera_parameter_optimizer, camera_system_transformation);

	assert(parameters.empty());
}

void CameraCalibrationFit::FromEigen(const Eigen::VectorXd& parameters_eigen_vector,
                                     const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer)
{
	//Convert parameters to std vector
	std::vector<double> parameters(parameters_eigen_vector.data(),
	                               parameters_eigen_vector.data() + parameters_eigen_vector.rows() * parameters_eigen_vector.cols());

	//Each object removes its parameters!
	camera_parameter_optimizer->TakeFromParameters(parameters);

	assert(parameters.empty());
}

Eigen::VectorXd CameraCalibrationFit::ToEigen(const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
                                              const std::shared_ptr<const CameraSystemTransformation>& camera_system_transformation,
                                              const std::shared_ptr<const CalibrationBoardsTransformation>& boards_transformation)
{
	//fetch parameters from all objects and store in "out"
	std::vector<double> out;
	{
		const auto& tmp = camera_parameter_optimizer->GetParameters();
		out.insert(out.end(), tmp.begin(), tmp.end());
	}

	{
		const auto& tmp = camera_system_transformation->GetParameters();
		out.insert(out.end(), tmp.begin(), tmp.end());
	}

	{
		const auto& tmp = boards_transformation->GetParameters();
		out.insert(out.end(), tmp.begin(), tmp.end());
	}

	Eigen::VectorXd out2 = Eigen::VectorXd::Zero(out.size());

	for(size_t i = 0; i < out.size(); ++i)
		out2[i]  = out[i];

	return out2;
}

bool CameraCalibrationFit::operator()(double const* const* parameters,
                                      double* residual) const
{
	Eigen::VectorXd x_eigen = Eigen::VectorXd::Zero(num_params_);

	for(auto i     = 0; i < num_params_; ++i)
		x_eigen[i] = parameters[0][i];

	Eigen::VectorXd fvec;
	this->operator()(x_eigen, fvec);

	for(auto i      = 0; i < num_values_; ++i)
		residual[i] = fvec[i];

	return true;
}

size_t CameraCalibrationFit::operator()(const Eigen::VectorXd& x,
                                        Eigen::VectorXd& residual) const
{
	residual                                = Eigen::VectorXd::Zero(num_values_);
	const auto camera_parameter_optimizer   = camera_parameter_optimizer_;
	const auto camera_system_transformation = camera_system_transformation_;
	const auto boards_transformation        = boards_transformation_;
	FromEigen(x, camera_parameter_optimizer, camera_system_transformation, boards_transformation);

	auto index = 0;
	residual.setZero();

	// For open mp parallelization, results from every frame is stored in a vector first
	// and combined afterwards
	std::vector<size_t> frame_indices;
	std::map<size_t, std::vector<double>> residual_values;

	for(const auto& frame_measurement_map : observations_.frames_measurement_map)
	{
		frame_indices.push_back(frame_measurement_map.first);
		residual_values[frame_measurement_map.first] = std::vector<double>();
	}

	std::vector<std::future<void>> results;

	/*tbb::parallel_for_each(frame_indices.begin(),
	                       frame_indices.end(),
	                       [this, &residual_values, &camera_system_transformation, &camera_parameter_optimizer, &boards_transformation](
	                       const size_t i) -> void*/

	for(auto i = 0; i < frame_indices.size(); ++i)

	{
		auto& value_local   = residual_values.find(i)->second;
		const auto& frame   = observations_.frames_measurement_map.find(i);
		if(frame != observations_.frames_measurement_map.end())
		{
			const auto frame_id = frame->first;

			for(const auto& camera_pair : frame->second.camera_measurement_map)
			{
				const auto camera_id = camera_pair.first;

				for(const auto& board_data : camera_pair.second.calibration_board_data_map)
				{
					const auto board_id = board_data.first;

					switch(optimization_mode_)
					{
					case OptimizationMode::PROJECTED_ERROR_2D:
						{
							const auto errors_2d = GetProjectionErrors(camera_system_transformation,
                                                                       camera_parameter_optimizer,
                                                                       frame_id,
                                                                       camera_id,
                                                                       board_id,
                                                                       boards_transformation);

							const auto error_size = errors_2d.size();

							for(auto idx = 0; idx < error_size; ++idx)
							{
								auto x = errors_2d[idx].x * board_data.second.markers[idx].weight;
								auto y = errors_2d[idx].y * board_data.second.markers[idx].weight;

								if(abs(x) > 20 || abs(y) > 20)
								{
									/*			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << " : Frame Id " << frame_id << " Board Id " << board_id << " Camera Id " <<
									camera_id << " x [" << x << "] y [" << y << "]";*/
									//continue;
								}

								/*if(abs(x) > 100 || abs(y) > 100)
								{

								}*/

								value_local.push_back(x);
								value_local.push_back(y);
							}

							break;
						}
					case OptimizationMode::CARTESIAN_ERROR_3D:
						{
							const auto points = Get3DPositionError(camera_system_transformation,
                                                                   camera_parameter_optimizer,
                                                                   frame_id,
                                                                   camera_id,
                                                                   board_id,
                                                                   boards_transformation);

							const auto points_size = points.size();

							for(auto pt_idx = 0; pt_idx < points_size; ++pt_idx)
							{
								value_local.push_back(points[pt_idx].x * board_data.second.markers[pt_idx].weight);
								value_local.push_back(points[pt_idx].y * board_data.second.markers[pt_idx].weight);
								value_local.push_back(points[pt_idx].z * board_data.second.markers[pt_idx].weight);
							}

							break;
						}
					default:
						throw std::exception("Optimization mode not supported.");
					}
				}
			}
		}
	}
	//);

	//for(auto& result : results)
	//	result.get();

	//Combine results of individual frames
	for(auto frame_index : frame_indices)
	{
		const auto& value = residual_values.at(frame_index);

		for(const auto& residue : value)
		{
			residual[index++] = residue;
		}
	}

	return 0;
}

std::vector<cv::Point3d> CameraCalibrationFit::GetErrors(const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
                                                         const std::shared_ptr<CameraParameterOptimizer>& cams_modifer,
                                                         const size_t frame_id,
                                                         const size_t camera_id,
                                                         const size_t board_id,
                                                         const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
                                                         std::pair<std::vector<size_t>, CircularMarkerVector>* markers) const
{
	switch(optimization_mode_)
	{
	case OptimizationMode::CARTESIAN_ERROR_3D:
		return Get3DPositionError(camera_system_transformation, cams_modifer, frame_id, camera_id, board_id, boards_transformation, markers);

	case OptimizationMode::PROJECTED_ERROR_2D:
		{
			auto projection_errors = GetProjectionErrors(camera_system_transformation,
			                                             cams_modifer,
			                                             frame_id,
			                                             camera_id,
			                                             board_id,
			                                             boards_transformation,
			                                             markers);
			std::vector<cv::Point3d> errors;
			std::transform(projection_errors.begin(),
			               projection_errors.end(),
			               std::back_inserter(errors),
			               [](const cv::Point2d& in) -> cv::Point3d
			               {
				               return cv::Point3d(in.x, in.y, 0);
			               });

			return errors;
		}
	default:
		throw std::exception("Unknown optimization mode.");
	}

	return std::vector<cv::Point3d>();
}

std::vector<cv::Point2d> CameraCalibrationFit::GetProjectionErrors(const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
                                                                   const std::shared_ptr<CameraParameterOptimizer>& camera_param_optimizer,
                                                                   const size_t frame_id,
                                                                   const size_t channel,
                                                                   const size_t board_id,
                                                                   const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
                                                                   std::pair<std::vector<size_t>, CircularMarkerVector>* markers) const
{
	CalibrationBoardData projected_markers;

	// calculate the board marker transformation/projection into the camera via the kinematic chain given by camSystem/camModifier/BoardsModifier
	auto points_2d = ProjectBoardPoints(camera_system_transformation,
	                                    camera_param_optimizer,
	                                    boards_transformation,
	                                    frame_id,
	                                    channel,
	                                    board_id,
	                                    markers,
	                                    &projected_markers);

	// return vector contains 2d differences between projected board marker and corresponding measurements
	std::vector<cv::Point2d> errors(points_2d.size());

	// return empty vector if no projected points available
	if(points_2d.empty())
		return errors;

	// get marker IDs which are in the first vector of a BoardMeasurementType, second vector contains measurements as CircularMarker
	const auto& markers_ids = projected_markers.marker_ids;

	const auto frame_transformation = camera_system_transformation->GetFrameTransformation(frame_id);
	const auto calibration          = camera_param_optimizer->GetCalibration(channel);

	// Get observed board marker measurements of requested frame/cam/board/
	auto entry = observations_.frames_measurement_map.at(frame_id).camera_measurement_map.at(channel).calibration_board_data_map.at(board_id);

	// add additional markers to be projected if passed to function
	if(markers)
	{
		entry.marker_ids.insert(entry.marker_ids.end(), markers->first.begin(), markers->first.end());
		entry.markers.insert(entry.markers.end(), markers->second.begin(), markers->second.end());
	}

	for(auto i = 0; i < points_2d.size(); ++i)
	{
		errors[i] = points_2d[i] - (use_raw_ellipse_center_
			                            ? cv::Point2d(entry.markers[i].raw_ellipse.center)
			                            : cv::Point2d(entry.markers[i].CorrectedCenter()));
	}

	return errors;
}


std::vector<cv::Point2d> CameraCalibrationFit::GetProjectionErrors(std::vector<cv::Point3d> points3d,
                                                                   std::vector<cv::Point2d> points2d) const
{
	std::exception("Get Projection Error is not working");
	//// calculate the board marker transformation/projection into the camera via the kinematic chain given by camSystem/camModifier/BoardsModifier
	//auto points2d_from_reconstructed_points = ProjectBoardPoints(camera_system_transformation,
	//	camera_param_optimizer,
	//	boards_transformation,
	//	frame_id,
	//	camera_id,
	//	board_id,
	//	markers,
	//	&projected_markers);

	// return vector contains 2d differences between projected board marker and corresponding measurements
	std::vector<cv::Point2d> errors(10/*points2d_from_reconstructed_points.size()*/);

	//// return empty vector if no projected points available
	//if(points2d_from_reconstructed_points.empty())
	//	return errors;

	//assert(points2d_from_reconstructed_points.size() == point2d.size());

	//for(auto i = 0; i < points2d_from_reconstructed_points.size(); ++i)
	//{
	//	errors[i] = points2d_from_reconstructed_points[i] - points2d[i];
	//}

	return errors;
}

std::vector<cv::Point3d> CameraCalibrationFit::Get3DPositionError(const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
                                                                  const std::shared_ptr<CameraParameterOptimizer>& camera_param_optimizer,
                                                                  const size_t frame_id,
                                                                  const size_t camera_id,
                                                                  const size_t board_id,
                                                                  const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
                                                                  std::pair<std::vector<size_t>, CircularMarkerVector>* markers) const
{
	CalibrationBoardData projected_markers;
	// calculate the board marker transformation/projection into the camera via the kinematic chain given by camSystem/camModifier/BoardsModifier
	// pts3d should then only contain the length of the ray is in the z direction (propagation direction of laser).
	// components x and y are due to angular error.
	const auto points_3d = GetMarkerPositionsInFrameCoordinateSystem(camera_system_transformation,
	                                                                 camera_param_optimizer,
	                                                                 boards_transformation,
	                                                                 frame_id,
	                                                                 camera_id,
	                                                                 board_id,
	                                                                 markers,
	                                                                 &projected_markers);


	// return vector contains 2d differences between projected board marker and corresponding measurements
	std::vector<cv::Point3d> errors(points_3d.size());

	// return empty vector if no projected points available
	if(points_3d.empty())
		return errors;

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	/*std::shared_ptr<LaserScannerSystemPositioner> sysPos = smart_cast<LaserScannerSystemPositioner>(camSystem);
	if(!sysPos)
	{
		EXCEPTION(InternalErrorException, L"Only supports laser scanner system positioners.");
	}*/

	// get observed board marker measurements of requested frame/cam/board/
	/*auto entry = observations_.frames.at(frame_id).cams.at(camera_id).boards.at(board_id);

	for(auto i = 0; i < pts3d.size(); ++i)
	{
		cv::Point3d delta(pts3d[i].x, pts3d[i].y, entry.second[i].raw_distance + sysPos->getDistOffset() - pts3d[i].z);
		errors[i] = delta;
	}*/

	return errors;
}

std::vector<cv::Point2d> CameraCalibrationFit::ProjectBoardPoints(const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
                                                                  const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer,
                                                                  const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
                                                                  const size_t frame_id,
                                                                  const size_t camera_id,
                                                                  const size_t board_id,
                                                                  std::pair<std::vector<size_t>, CircularMarkerVector>* markers,
                                                                  CalibrationBoardData* projected_measurements,
                                                                  std::vector<cv::Point3d>* camera_point_3d) const
{
	// create empty return vector
	std::vector<cv::Point2d> pts2d;

	// check existence of measurements - return empty vector if any data is missing
	// if no observation present in this frame id and cam id, no position information available
	const auto frame_iterator = observations_.frames_measurement_map.find(frame_id);
	if(frame_iterator == observations_.frames_measurement_map.end())
		return pts2d;

	const auto camera_iterator = frame_iterator->second.camera_measurement_map.find(camera_id);
	if(camera_iterator == frame_iterator->second.camera_measurement_map.end())
		return pts2d;

	// if markers are provided and are not empty, there is at least one observation of this
	// combination frame_id, camera_id and board_id that can be projected
	if(!markers || markers->first.empty())
	{
		const auto board_iterator = camera_iterator->second.calibration_board_data_map.find(board_id);
		if(board_iterator == camera_iterator->second.calibration_board_data_map.end())
			return pts2d;
	}

	auto points_3d = GetMarkerPositionsInFrameCoordinateSystem(camera_system_transformation,
	                                                           camera_parameter_optimizer,
	                                                           boards_transformation,
	                                                           frame_id,
	                                                           camera_id,
	                                                           board_id,
	                                                           markers,
	                                                           projected_measurements);


	const auto calibration = camera_parameter_optimizer->GetCalibration(camera_id);
	//cv::Mat camera_matrix, distortion_vector;
	//calibration->GetCameraIntrinsicParams(camera_matrix, distortion_vector);

	// project points into camera.
	// keep camera 3d coordinates if requested
	if(camera_point_3d)
	{
		camera_point_3d->clear();
		for(auto& point_3d : points_3d)
		{
			//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << calibration->FromWorldCoordinateSystemToCamera(point_3d);
			camera_point_3d->push_back(calibration->FromWorldCoordinateSystemToCamera(point_3d, true));
		}
	}

	calibration->ProjectPoints(points_3d, pts2d);


	return pts2d;
}

std::vector<cv::Point2d> CameraCalibrationFit::ProjectBoardPoints(std::vector<cv::Point3d>& reconstructed_points) const
{
	// create empty return vector
	std::vector<cv::Point2d> projected_2d_from_reconstructed_points;
	const auto calibration = camera_parameter_optimizer_->GetCalibration(CHANNEL_PROJECTOR0);

	// project points into projector.
	calibration->ProjectPoints(reconstructed_points, projected_2d_from_reconstructed_points);

	return projected_2d_from_reconstructed_points;
}

std::vector<cv::Point3d> CameraCalibrationFit::GetMarkerPositionsInFrameCoordinateSystem(
	const std::shared_ptr<CameraSystemTransformation>& camera_system_transformation,
	const std::shared_ptr<CameraParameterOptimizer>& camera_parameter_optimizer,
	const std::shared_ptr<CalibrationBoardsTransformation>& boards_transformation,
	const size_t frame_id,
	const size_t camera_id,
	const size_t board_id,
	std::pair<std::vector<size_t>, CircularMarkerVector>* markers,
	CalibrationBoardData* projected_measurements) const
{
	const auto frame_transformation = camera_system_transformation->GetFrameTransformation(frame_id);
	const auto calibration          = camera_parameter_optimizer->GetCalibration(camera_id);

	// get observed board marker measurements of requested frame/cam/board/
	auto entry = observations_.frames_measurement_map.at(frame_id).camera_measurement_map.at(camera_id).calibration_board_data_map.at(board_id);

	// add additional markers to be projected if passed to function
	if(markers)
	{
		entry.marker_ids.insert(entry.marker_ids.end(), markers->first.begin(), markers->first.end());
		entry.markers.insert(entry.markers.end(), markers->second.begin(), markers->second.end());
	}

	// copy the measurement of the projected markers to output if requested
	if(projected_measurements)
	{
		*projected_measurements = entry;
	}

	// get marker IDs which are in the first vector of a BoardMeasurementType, second vector contains measurements as CircularMarker
	const auto marker_ids = entry.marker_ids;

	// vector for all board local 3d coordinates of detected board marker
	std::vector<cv::Point3d> board_marker_3d_coordinates;

	// transform board local coordinates into device coordinates via kinematic chain:
	// board local  boardSystem  device
	// device camera is part of SingleCameraCalibrations project points
	for(auto marker_id : marker_ids)
	{
		auto position = boards_transformation->GetMarkerPosition(board_id, marker_id);
		board_marker_3d_coordinates.push_back(frame_transformation.Multiply(position));
	}

	//std::ofstream board_marker_3d_coordinates_stream;
	//auto paired_observations_path = R"(C:\CalibrationDataFS\10 input\DMVS_DMVS011900000\board_marker_3d_coordinates)" + Helper::GetTimeStamp() + ".txt";
	//board_marker_3d_coordinates_stream.open(paired_observations_path);

	//for(auto& board_marker_3d_coordinate : board_marker_3d_coordinates)
	//{
	//	board_marker_3d_coordinates_stream << board_marker_3d_coordinate.x << " " << board_marker_3d_coordinate.y << " " << board_marker_3d_coordinate
	//		.z << "\n";
	//}

	//board_marker_3d_coordinates_stream.close();

	return board_marker_3d_coordinates;
}

Eigen::VectorXd CameraCalibrationFit::GetInitialParameters(const bool initialize_cams,
                                                           const bool initialize_camera_system_position_finder,
                                                           const bool initialize_boards_position_provider) const
{
	//NOTE : The sequence of matrix multiplication is to derived from board coordinates to point in camera coordinate system:
	// trafo = trafoCalib * trafoFrame * trafoBoard.
	// Further, note that trafoCalib is done by the SingleCameraCalibration object.
	// Thus boards have to be initialized first
	// Subsequently the trafos of the frames to the board system
	// And at last the relative trafos between the cameras
	if(initialize_camera_system_position_finder)
		camera_system_transformation_->PreInit();

	if(initialize_boards_position_provider)
		boards_transformation_->SetInitialParameters(observations_, camera_parameter_optimizer_, camera_system_transformation_);

	if(initialize_camera_system_position_finder)
		camera_system_transformation_->SetInitialParameters(observations_, camera_parameter_optimizer_, boards_transformation_);

	if(initialize_cams)
		camera_parameter_optimizer_->SetInitialParameters(observations_, camera_system_transformation_, boards_transformation_);

	return ToEigen(camera_parameter_optimizer_, camera_system_transformation_, boards_transformation_);
}

const ObservationMap& CameraCalibrationFit::GetObservations() const
{
	return observations_;
}

void CameraCalibrationFit::SetDistortionCorrection(const std::tuple<size_t, size_t, size_t, size_t>& entry,
                                                   const cv::Vec2f& correction)
{
	const FrameID fr_id    = std::get<0>(entry);
	const CameraID cam_id  = std::get<1>(entry);
	const BoardID board_id = std::get<2>(entry);

	auto& obs = observations_.frames_measurement_map.at(fr_id).camera_measurement_map.at(cam_id).calibration_board_data_map.at(board_id);

	// find the marker ID in the first vector ofBoardMeasurementType obs
	const auto iterator = std::find(obs.marker_ids.begin(), obs.marker_ids.end(), std::get<3>(entry));

	if(iterator == obs.marker_ids.end())
	{
		assert(false);
		return;
	}

	const auto index                       = std::distance(obs.marker_ids.begin(), iterator);
	obs.markers[index].center_correction.x = correction[0];
}

void CameraCalibrationFit::SetUseRawEllipseCenter(const bool if_use_raw_ellipse_center)
{
	use_raw_ellipse_center_ = if_use_raw_ellipse_center;
}

Eigen::VectorXd CameraCalibrationFit::GetCurrentParameters() const
{
	return ToEigen(camera_parameter_optimizer_, camera_system_transformation_, boards_transformation_);
}

size_t CameraCalibrationFit::UpdateEllipseCorrections(VectorObservationPtr& observations,
                                                      const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
                                                      const std::shared_ptr<const CameraSystemTransformation>& camera_system_transformation,
                                                      const std::shared_ptr<const CalibrationBoardsTransformation>& boards_transformation,
                                                      double filter_value)
{
	auto observation_count = 0;
	auto observation       = GetObservationMap(observations);

	for(auto& frame_iterator : observation.frames_measurement_map)
	{
		// Get the transformation of current frame to reference board
		const auto frame_id             = frame_iterator.first;
		const auto frame_transformation = camera_system_transformation->GetFrameTransformation(frame_id);
		auto rotational_transformation  = frame_transformation.GetRotationalTransformation().clone();
		auto translation_vector         = frame_transformation.GetTranslationalTransformation().clone();
		cv::Mat rotational_vector;
		cv::Rodrigues(rotational_transformation, rotational_vector);

		for(const auto& camera_measurement : frame_iterator.second.camera_measurement_map)
		{
			const auto camera_id   = camera_measurement.first;
			const auto calibration = camera_parameter_optimizer->GetCalibration(camera_id);
			auto ellipse_correction = std::make_shared<EllipseDetectionDistortionCorrection>(rotational_vector, translation_vector, calibration);

			for(auto board_data_map : camera_measurement.second.calibration_board_data_map)
			{
				auto board_id    = board_data_map.first;
				const auto& meas = board_data_map.second.markers;

				// collect marker positions in camera coordinates and corresponding measured ellipses
				// circle marker in board system coordinates
				std::vector<cv::Point3d> board_system_pts;

				// circle marker in reference camera coordinates ( Freestyle device coordinates)
				std::vector<cv::Point3d> ref_cam_pts;

				// circle marker in local camera coordinates ( e.g. IR 0 camera )
				std::vector<cv::Point3d> cam_local_pts;

				// ellipse in image coordinates
				std::vector<cv::RotatedRect> ellipses;

				// get all circle position in camera local coordinates to calculate the circle real world radius later
				for(auto marker_idx = 0; marker_idx < board_data_map.second.marker_ids.size(); ++marker_idx)
				{
					const auto marker_id      = board_data_map.second.marker_ids[marker_idx];
					const auto& circle_marker = board_data_map.second.markers[marker_idx];
					ellipses.push_back(circle_marker.raw_ellipse);

					// marker coordinate in board system coordinates, frame independent
					const auto board_system_pt = boards_transformation->GetMarkerPosition(board_id, marker_id);
					board_system_pts.push_back(board_system_pt);

					// 3d coordinate of marker in reference camera system in this frame
					auto ref_cam_pt = frame_transformation.Multiply(board_system_pt);
					ref_cam_pts.push_back(ref_cam_pt);

					// transform from reference camera to observing camera coordinate system
					auto something = calibration->FromWorldCoordinateSystemToCamera(ref_cam_pt);
					cam_local_pts.push_back(something);
				}

				// Projection Errors
				std::vector<double> proj_errors;
				std::vector<cv::Point2d> proj;
				calibration->ProjectPoints(ref_cam_pts, proj);

				for(auto i = 0; i < proj.size(); ++i)
				{
					auto center = meas[i].CorrectedCenter();
					double diff = cv::norm(proj[i] - cv::Point2d(center.x, center.y));
					proj_errors.push_back(diff);
				}

				// calculate real world marker radii from  1 meter norm diameter and distance
				std::vector<double> norm_diameters, real_marker_radii;
				norm_diameters = ellipse_correction->GetNormedRealWorldDiameters(ellipses);

				for(auto i = 0; i < cam_local_pts.size(); ++i)
				{
					real_marker_radii.push_back(norm_diameters[i] * cam_local_pts[i].z * 0.5);
				}

				// Circles with position, radius and normal used for ellipse correction
				std::vector<EllipseDetectionDistortionCorrection::Circle> circles;

				// Board normal is the normal for all board markers
				auto board_normal = boards_transformation->GetBoardTransformation(board_id).MultiplyWithRotation(cv::Point3d(0., 0., 1.));

				for(auto i = 0; i < cam_local_pts.size(); ++i)
				{
					EllipseDetectionDistortionCorrection::Circle circle;
					circle.radius = float(real_marker_radii[i]);
					circle.center = cv::Vec3d(board_system_pts[i].x, board_system_pts[i].y, board_system_pts[i].z);
					circle.normal = cv::Vec3d(board_normal.x, board_normal.y, board_normal.z);
					circles.push_back(circle);
				}

				// for check against measurement
				std::vector<cv::Vec2d> projected_centers;

				// calculate theoretical ellipse correction
				auto corrections = ellipse_correction->GetCorrections(circles, &projected_centers);

				std::vector<double> diff;
				for(auto marker_idx = 0; marker_idx < board_data_map.second.marker_ids.size(); ++marker_idx)
				{
					// deactivate marker with errors exceeding filterVal if enabled (filterVal>0)
					if(filter_value > 0 && proj_errors[marker_idx] > filter_value)
					{
						board_data_map.second.observations[marker_idx]->GetDeactivated();
					}
					else
					{
						auto circular_marker              = board_data_map.second.markers[marker_idx];
						circular_marker.center_correction = cv::Vec2f(float(corrections[marker_idx][0]), float(corrections[marker_idx][1]));
						board_data_map.second.observations[marker_idx]->SetFromCircularMarker(circular_marker);
					}
				}
			}
		}
	}

	return observation_count;
}

void CameraCalibrationFit::AddObservation(VectorObservationPtr& observations,
                                          const CircularMarker& marker,
                                          const size_t camera_id,
                                          const size_t board_id,
                                          const size_t marker_id,
                                          const size_t frame_id)
{
	auto observation = std::make_shared<Observation>();
	observation->SetFromCircularMarker(marker);
	observation->SetImageChannel(camera_id);
	observation->SetBoardId(board_id);
	observation->SetMarkerId(marker_id);
	observation->SetFrameId(frame_id);

	observations.push_back(observation);
}

size_t CameraCalibrationFit::IdentifyUncodedMarker(VectorObservationPtr& observations,
                                                   const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
                                                   const std::shared_ptr<const CameraSystemTransformation>& camera_system_transformation,
                                                   const std::shared_ptr<const CalibrationBoardsTransformation>& boards_transformation,
                                                   const MarkerIdentificationParams& identification_params)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_START;
	// get all observation that are not yet identified
	// unknownMarker[frameID][camID]
	std::map<size_t, std::map<size_t, std::vector<cv::Point2d>>> unknown_marker_pts;
	std::map<size_t, std::map<size_t, std::vector<size_t>>> unknown_marker_idx;
	const auto observations_size = observations.size();

	for(auto i = 0; i < observations_size; ++i)
	{
		const auto observation = observations[i];

		// Check if observation is identified
		const auto board_id  = observation->GetBoardId();
		const auto marker_id = observation->GetMarkerId();

		// Skip marker measurements that are already identified
		if(board_id != Constants::DEFAULT_SIZE_T_VALUE && marker_id != Constants::DEFAULT_SIZE_T_VALUE)
			continue;

		// Get frame, camera and marker position
		auto frame_id           = observation->GetFrameId();
		auto camera_id          = observation->GetImageChannel();
		auto raw_ellipse_center = observation->GetRawEllipseCenter();

		// Observation Index and position
		unknown_marker_pts[frame_id][camera_id].push_back(cv::Point2d(raw_ellipse_center));
		unknown_marker_idx[frame_id][camera_id].push_back(i);
	}

	auto num_identified = 0;
	// looping over frame
	for(auto unknown_marker_iterator = unknown_marker_pts.begin(); unknown_marker_iterator != unknown_marker_pts.end(); ++unknown_marker_iterator)
	{
		const auto frame_id             = unknown_marker_iterator->first;
		const auto frame_transformation = camera_system_transformation->GetFrameTransformation(frame_id);

		// looping over cameras
		for(auto camera_iterator = unknown_marker_iterator->second.begin(); camera_iterator != unknown_marker_iterator->second.end(); ++
		    camera_iterator)
		{
			const auto camera_id            = camera_iterator->first;
			const auto& observation_markers = camera_iterator->second;

			const auto calibration = camera_parameter_optimizer->GetCalibration(camera_id);
			auto board_ids         = boards_transformation->GetBoardIDs();

			EllipseDetectionDistortionCorrection ellipse_correction(cv::Mat::zeros(1, 3, CV_64F), cv::Mat::zeros(1, 3, CV_64F), calibration);

			// for all markers of all boards project markers
			std::vector<std::pair<size_t, size_t>> board_marker_vec;
			std::vector<cv::Point3d> boards_pts;

			// Looping over boards
			for(const auto& board_id : board_ids)
			{
				if(!boards_transformation->IsBoardInitialized(board_id))
					continue;

				//std::vector<cv::Point3d> marker_positions1;
				std::vector<size_t> marker_ids;
				auto calibration_board = boards_transformation->GetBoard(board_id);

				if(calibration_board)
				{
					for(const auto& marker : calibration_board->GetMarkerPositionMap())
					{
						const auto marker_id                = marker.first;
						const auto frame_cs_marker_position = boards_transformation->GetMarkerPosition(board_id, marker_id);

						// Calibration points as availabe in calirbation file (cpd) ???
						boards_pts.push_back(frame_transformation.Multiply(frame_cs_marker_position)); //!!?

						// keep source (board and marker) of 3d point in equally indexing vector
						std::pair<size_t, size_t> board_id_marker_id_pair;
						board_id_marker_id_pair.first  = board_id;
						board_id_marker_id_pair.second = marker_id;
						board_marker_vec.push_back(board_id_marker_id_pair);
					}
				}
			}

			// project calibration points into camera image
			std::vector<cv::Point2d> pts2d;
			calibration->ProjectPoints(boards_pts, pts2d);

			cv::Mat intrinsics;
			cv::Mat distortion;

			//TODO CHECK THIS METHOD
			calibration->GetCameraIntrinsicParams(intrinsics, distortion);
			cv::Point2d principal_point(intrinsics.at<double>(0, 2), intrinsics.at<double>(1, 2));

			cv::Point2d img_center(identification_params.image_width / 2.0, identification_params.image_height / 2.0);
			auto max_radius = cv::norm(img_center) * identification_params.max_image_center_distance;

			// compare all detected marker with projected board points
			// assign board and marker ID if close enough( < 5px )
			// goes over all the observation markers in the current frame and camera
			auto frame_marker_identified = 0;
			for(auto points2d_idx = 0; points2d_idx < pts2d.size(); ++points2d_idx)
			{
				// check distance to image center (crop image corners)
				if(cv::norm(pts2d[points2d_idx] - img_center) > max_radius)
				{
					continue;
				}

				for(auto marker_idx = 0; marker_idx < observation_markers.size(); ++marker_idx)
				{
					auto projection_distance = cv::norm(observation_markers[marker_idx] - pts2d[points2d_idx]);

					if(projection_distance < identification_params.max_projection_distance)
					{
						// get observation of current marker
						auto observation_idx = unknown_marker_idx[frame_id][camera_id][marker_idx];
						auto observation     = observations[observation_idx];

						CircularMarker circular_marker;
						observation->GetAsCircularMarker(circular_marker);
						std::vector<cv::RotatedRect> ellipses(1, circular_marker.CorrectedEllipse());
						auto normalized_diameters = ellipse_correction.GetNormedRealWorldDiameters(ellipses);

						// marker size check
						auto cam_local           = calibration->FromWorldCoordinateSystemToCamera(boards_pts[points2d_idx]);
						auto marker_depth        = cam_local.z;
						auto real_world_diameter = normalized_diameters[0] * marker_depth;

						const auto board_id  = board_marker_vec[points2d_idx].first;
						const auto marker_id = board_marker_vec[points2d_idx].second;
						observations[observation_idx]->SetBoardId(board_id);
						observations[observation_idx]->SetMarkerId(marker_id);

						num_identified++;
						frame_marker_identified++;
					}
					/*else if(projection_distance < 4 * identification_params.max_projection_distance)
					{
						BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : FAILED Projection distance [" << projection_distance << "]";
					}*/
				}
			}
		}
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_END;

	return num_identified;
}

bool CameraCalibrationFit::IsUsedObservation(const std::shared_ptr<const Observation>& observation)
{
	return !(observation->GetBoardId() == Constants::DEFAULT_SIZE_T_VALUE && observation->GetMarkerId() == Constants::DEFAULT_SIZE_T_VALUE && !
		observation->GetDeactivated());
}

size_t CameraCalibrationFit::CountParameters(const std::shared_ptr<const CameraParameterOptimizer>& camera_parameter_optimizer,
                                             const std::shared_ptr<const CameraSystemTransformation>& camera_system_transformation,
                                             const std::shared_ptr<const CalibrationBoardsTransformation>& boards_transformation)
{
	size_t out = 0;
	out += camera_parameter_optimizer->GetRequiredParameterCount();
	out += camera_system_transformation->GetRequiredParameterCount();
	out += boards_transformation->GetRequiredParameterCount();

	return out;
}

size_t CameraCalibrationFit::CountValues(const VectorObservationPtr& observations,
                                         const OptimizationMode optimization_mode)
{
	auto valid_object_count = 0;

	for(const auto observation : observations)
	{
		// Only count assigned observations that are not deactivated
		if(IsUsedObservation(observation))
		{
			switch(optimization_mode)
			{
			case OptimizationMode::PROJECTED_ERROR_2D:
				valid_object_count += 2;
				break;
			case OptimizationMode::CARTESIAN_ERROR_3D:
				valid_object_count += 3;
				break;
			default:
				throw std::exception("Unknown optimization mode.");
			}
		}
	}

	return valid_object_count;
}

ObservationMap CameraCalibrationFit::GetObservationMap(const VectorObservationPtr& observations)
{
	ObservationMap out;

	for(const auto& observation : observations)
	{
		// Skip marker measurements that are not identified or deactivated
		if(!IsUsedObservation(observation))
			continue;

		auto camera_id = observation->GetImageChannel();
		auto board_id  = observation->GetBoardId();
		auto frame_id  = observation->GetFrameId();
		auto marker_id = observation->GetMarkerId();

		CircularMarker circular_marker;
		observation->GetAsCircularMarker(circular_marker);
		out.frames_measurement_map[frame_id].camera_measurement_map[camera_id].calibration_board_data_map[board_id].marker_ids.push_back(marker_id);
		out.frames_measurement_map[frame_id].camera_measurement_map[camera_id].calibration_board_data_map[board_id]
			.markers.push_back(circular_marker);
		out.frames_measurement_map[frame_id].camera_measurement_map[camera_id].calibration_board_data_map[board_id]
			.observations.push_back(observation);
	}

	return out;
}
}
}
