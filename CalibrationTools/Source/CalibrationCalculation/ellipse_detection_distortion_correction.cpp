#include "ellipse_detection_distortion_correction.h"
#include "helper.h"
#include "transformation.h"

using namespace DMVS::CameraCalibration;

EllipseDetectionDistortionCorrection::EllipseDetectionDistortionCorrection(const cv::Mat& rotation_matrix,
                                                                           const cv::Mat& translation_vector,
                                                                           const std::shared_ptr<const CameraSensor>& calibration) :
	rotation_matrix_(rotation_matrix),
	translation_vector_(translation_vector),
	calibration_(calibration),
	num_sample_points_(100),
	base_normal_(cv::Vec3d(0., 0., 1.)),
	base_center_(cv::Vec3d(0., 0., 0.)),
	base_radius_(1.0)
{
	for(auto i = 0; i < num_sample_points_; ++i)
	{
		const auto angle = 2 * M_PI / num_sample_points_ * i;
		base_circle_pts_.push_back(cv::Vec3d(sin(angle), cos(angle), 0)); // Multiply with base radius ?!
	}
}

std::vector<cv::Vec2d> EllipseDetectionDistortionCorrection::GetCorrections(const std::vector<Circle>& model,
                                                                            std::vector<cv::Vec2d>* projected_centers_out,
                                                                            std::vector<cv::RotatedRect>* projected_circles_out) const
{
	// the determined corrections (projected circle center  - fitted ellipse center).
	std::vector<cv::Vec2d> corrections;

	// Projections of the circle's center points  ( projection of center )
	std::vector<cv::Vec2d> projected_circle_centers;

	// Ellipses as result from ellipse fit to projected sample points of 3D circle (center of projection)
	std::vector<cv::RotatedRect> projected_circles;

	// circles needed
	if(model.empty())
		return corrections;

	// Calculate ellipses centers of projected circles for all circles
	for(const auto& circle : model)
	{
		// calculate 3D sample points of circle from unit base circle
		std::vector<cv::Point3d> sample_points;
		{
			Transformation transformation;
			transformation.Rotate(base_normal_, circle.normal);
			transformation.Translate(circle.center - base_center_);

			for(const auto& point : base_circle_pts_)
			{
				const auto ratio          = double(circle.radius / base_radius_);
				const cv::Vec3d point_new = point * ratio;
				auto pt = transformation.Multiply(cv::Point3d(point_new[0], point_new[1], point_new[2]));
				sample_points.push_back(pt);
			}
		}

		// Sample points are in board system coordinates, apply frame trafo to move to reference cam coordinate system (Freestyle device coordinates system)
		auto sample_points_local = Helper::ApplyTransformation(rotation_matrix_, translation_vector_, sample_points);

		// project all sample points of 3d circle to local camera coordinates
		std::vector<cv::Vec2d> camera_points;
		calibration_->ProjectPoints(sample_points_local, camera_points);

		std::vector<cv::Vec2f> camera_points_float(camera_points.size());

		for(auto i                 = 0; i < camera_points.size(); ++i)
			camera_points_float[i] = cv::Vec2f(float(camera_points[i][0]), float(camera_points[i][1]));

		// fit an ellipse to the projected sample points and add to ellipse vector
		projected_circles.push_back(cv::fitEllipse(camera_points_float));
	}

	// Calculate projection of circle center
	// convert 3d circle sample points to OpenCV
	std::vector<cv::Point3d> circle_centers3d(model.size());
	std::transform(model.begin(),
	               model.end(),
	               circle_centers3d.begin(),
	               [](const Circle& circle) -> cv::Point3d
	               {
		               return cv::Vec3d(circle.center[0], circle.center[1], circle.center[2]);
	               });

	// project 3d circle centers to camera
	const auto circle_centers3d_local = Helper::ApplyTransformation(rotation_matrix_, translation_vector_, circle_centers3d);
	calibration_->ProjectPoints(circle_centers3d_local, projected_circle_centers);

	// Calculate correction vectors
	corrections.resize(projected_circles.size());
	std::transform(projected_circles.begin(),
	               projected_circles.end(),
	               projected_circle_centers.begin(),
	               corrections.begin(),
	               [](const cv::RotatedRect& ellipse,
	                  const cv::Vec2d& corrected_center) -> cv::Vec2d
	               {
		               return corrected_center - cv::Vec2d(ellipse.center.x, ellipse.center.y);
	               });

	// update output parameter if given
	if(projected_centers_out)
		*projected_centers_out = projected_circle_centers;

	if(projected_circles_out)
		*projected_circles_out = projected_circles;

	return corrections;
}

//std::vector<cv::Vec2d> EllipseDetectionDistortionCorrection::GetCircleCentersOfLastCorrection() const
//{
//}

//std::vector<cv::RotatedRect> EllipseDetectionDistortionCorrection::GetCircleProjections() const
//{
//}

std::vector<double> EllipseDetectionDistortionCorrection::GetNormedRealWorldDiameters(const std::vector<cv::RotatedRect>& ellipses) const
{
	std::vector<double> diameters;

	for(const auto ellipse : ellipses)
	{
		// Ellipses from VgFreestyleBlobDetector should always arrive width > height
		// ensure that the ellipse rotation angle relative to bigger axis
		auto ellipse_angle_rad = ellipse.angle * M_PI / 180.0;
		auto major_radius      = ellipse.size.width / 2.0;
		auto minor_radius      = ellipse.size.height / 2.0;

		if(major_radius < minor_radius)
		{
			const auto tmp = major_radius;
			major_radius   = minor_radius;
			minor_radius   = tmp;
			ellipse_angle_rad += M_PI / 2.0;
		}

		//const auto basicRepr = calib->getBasicRepresentation();
		//double focalLength = basicRepr.focalLength[0];
		// Get the vector to main axis point from ellipse center
		Eigen::Vector2d major_axis_pt2d_a, minor_axis_pt2d_a, major_axis_pt2d_b, minor_axis_pt2d_b;
		major_axis_pt2d_a[0] = major_radius * cos(ellipse_angle_rad) + ellipse.center.x;
		major_axis_pt2d_a[1] = major_radius * sin(ellipse_angle_rad) + ellipse.center.y;

		minor_axis_pt2d_a[0] = minor_radius * -sin(ellipse_angle_rad) + ellipse.center.x;
		minor_axis_pt2d_a[1] = minor_radius * cos(ellipse_angle_rad) + ellipse.center.y;

		major_axis_pt2d_b[0] = major_radius * -cos(ellipse_angle_rad) + ellipse.center.x;
		major_axis_pt2d_b[1] = major_radius * -sin(ellipse_angle_rad) + ellipse.center.y;

		minor_axis_pt2d_b[0] = minor_radius * sin(ellipse_angle_rad) + ellipse.center.x;
		minor_axis_pt2d_b[1] = minor_radius * -cos(ellipse_angle_rad) + ellipse.center.y;

		// Straighten the measured points
		std::vector<cv::Point2d> pts, pts_linearized;
		pts.emplace_back(ellipse.center.x, ellipse.center.y);
		pts.emplace_back(major_axis_pt2d_a.x(), major_axis_pt2d_a.y());
		pts.emplace_back(minor_axis_pt2d_a.x(), minor_axis_pt2d_a.y());
		pts.emplace_back(major_axis_pt2d_b.x(), major_axis_pt2d_b.y());
		pts.emplace_back(minor_axis_pt2d_b.x(), minor_axis_pt2d_b.y());

		// This will result in normalized points
		calibration_->UndistortPoints2(pts, pts_linearized);

		// create lines out of undistorted radius point coordinates
		cv::Vec3d p0(pts_linearized[0].x, pts_linearized[0].y, 1.0);
		cv::Vec3d p1(pts_linearized[1].x, pts_linearized[1].y, 1.0);
		cv::Vec3d p2(pts_linearized[2].x, pts_linearized[2].y, 1.0);
		//cv::Vec3d p3(pts_linearized[3].x, pts_linearized[3].y, 1.0);
		//cv::Vec3d p4(pts_lin[4].x, pts_lin[4].y, 1.0);

		// get the corresponding real world diameter at z = 1m
		const auto angle_p0_to_p1 = Helper::AngleTo(p0, p1);
		const auto angle_p0_to_p2 = Helper::AngleTo(p0, p2);
		auto real_world_diameter  = tan(angle_p0_to_p1) * Helper::VectorLength(p0) + tan(angle_p0_to_p2) * Helper::VectorLength(p0);

		diameters.push_back(real_world_diameter);
	}

	return diameters;
}
