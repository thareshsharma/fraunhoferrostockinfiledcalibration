#include "camera_parameter_optimizer.h"
#include "constants.h"

namespace DMVS
{
namespace CameraCalibration
{
CameraParameterOptimizer::CameraParameterOptimizer(const std::set<size_t> camera_ids,
                                                   const std::shared_ptr<const CameraSensor> calibration,
                                                   const size_t reference_camera_id):
	camera_ids_(camera_ids),
	calibration_(calibration)
{
	if(camera_ids.empty())
		throw std::exception("No camera ids given.");

	reference_camera_id_ = (reference_camera_id == Constants::DEFAULT_SIZE_T_MAX_VALUE ? *camera_ids.begin() : reference_camera_id);

	for(const auto& camera_id : camera_ids)
		camera_sensor_map_[camera_id] = calibration->Duplicate();

	if(camera_sensor_map_.find(reference_camera_id_) == camera_sensor_map_.end())
		throw std::exception("Reference camera id  not in camera ids.");

	SetReferenceCameraIsOrigin(true);
}

CameraParameterOptimizer::CameraParameterOptimizer(const std::map<size_t, std::shared_ptr<const CameraSensor>> calibration,
                                                   const size_t reference_camera_id)
{
	if(calibration.empty())
		throw std::exception("No camera ids given.");

	reference_camera_id_ = reference_camera_id == Constants::DEFAULT_SIZE_T_MAX_VALUE ? calibration.begin()->first : reference_camera_id;

	for(const auto& camera_channel_map : calibration)
		camera_sensor_map_[camera_channel_map.first] = camera_channel_map.second->Duplicate();

	if(camera_sensor_map_.find(reference_camera_id_) == camera_sensor_map_.end())
		throw std::exception("Reference camera id  not in camera ids.");

	SetReferenceCameraIsOrigin(true);
}

CameraParameterOptimizer::~CameraParameterOptimizer() = default;

Transformation CameraParameterOptimizer::GetRelativeCameraPosition(const size_t camera_id) const
{
	return Transformation(GetCalibration(camera_id));
}

std::shared_ptr<CameraSensor> CameraParameterOptimizer::GetCalibration(const size_t camera_id) const
{
	const auto iterator = camera_sensor_map_.find(camera_id);

	if(iterator == camera_sensor_map_.end())
		throw std::exception("Camera not found.");

	return iterator->second;
}

std::map<size_t, std::shared_ptr<const CameraSensor>> CameraParameterOptimizer::GetAllCalibrations() const
{
	std::map<size_t, std::shared_ptr<const CameraSensor>> out;

	for(const auto& calibration : camera_sensor_map_)
		out[calibration.first] = calibration.second;

	return out;
}

size_t CameraParameterOptimizer::GetReferenceCameraId() const
{
	return reference_camera_id_;
}

void CameraParameterOptimizer::TakeFromParameters(std::vector<double>& parameters)
{
}

void CameraParameterOptimizer::TakeFromProjectorParameters(std::vector<double>& parameters)
{
}

void CameraParameterOptimizer::SetInitialParameters(const ObservationMap& observations,
                                                    std::shared_ptr<const CameraSystemTransformation> camera_system_transformation,
                                                    std::shared_ptr<const CalibrationBoardsTransformation> boards)
{
}

void CameraParameterOptimizer::SetReferenceCameraIsOrigin(const bool if_identity_transformation)
{
	if_reference_camera_is_at_origin_ = if_identity_transformation;
	camera_sensor_map_[reference_camera_id_]->KeepExtrinsicParametersFix(if_identity_transformation);
}

bool CameraParameterOptimizer::GetReferenceCameraIsOrigin() const
{
	return if_reference_camera_is_at_origin_;
}
}
}
