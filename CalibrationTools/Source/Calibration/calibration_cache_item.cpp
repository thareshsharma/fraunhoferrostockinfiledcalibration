#include "calibration_cache_item.h"
#include "helper.h"
#include "calibration_cache.h"
#include "calibration_file_item.h"

using namespace DMVS::CameraCalibration;

#define RETURN_ERROR(code) \
{\
	TCHAR msg[256]; \
	DWORD err = GetLastError(); \
	_stprintf_s(msg, L"file %hs line %d code %d sys %d\n", __FILE__, __LINE__, code, err); \
	OutputDebugString(msg); \
	return code; \
}

CalibrationCacheItem::CalibrationCacheItem(const size_t record_number,
                                           const std::string& name,
                                           const cv::Mat& mat,
                                           const uint32_t matrix_type) :
	record_number(record_number),
	name(name),
	flags(matrix_type),
	stored_position(NOT_STORED),
	stored_size(0),
	string(""),
	image_mat(mat),
	configuration_container(nullptr)
{
}

CalibrationCacheItem::CalibrationCacheItem(const size_t record_number,
                                           const std::string& name,
                                           const std::shared_ptr<ConfigurationContainer>& container) :
	record_number(record_number),
	name(name),
	flags(CONFIG_CONTAINER),
	stored_position(NOT_STORED),
	stored_size(0),
	string(""),
	configuration_container(container)
{
}

CalibrationCacheItem::CalibrationCacheItem(const size_t record_number,
                                           const std::string& name,
                                           const std::vector<unsigned char>& bytes) :
	record_number(record_number),
	name(name),
	flags(RAW_BYTES),
	stored_position(NOT_STORED),
	stored_size(0),
	string(""),
	image_mat(bytes, true),
	configuration_container(nullptr)
{
	// Raw byte buffers stored in an OpenCV matrix. Use the
	// OpenCV conversion between vector and matrix to get the
	// right data types.
}

CalibrationCacheItem::CalibrationCacheItem(const size_t record_number,
                                           const std::string& name,
                                           const std::string& input_string) :
	record_number(record_number),
	name(name),
	flags(STRING),
	stored_position(NOT_STORED),
	stored_size(0x0),
	string(input_string),
	configuration_container(nullptr)
{
}

CalibrationCacheItem::CalibrationCacheItem() :
	record_number(0),
	name(""),
	flags(NO_DATA_LOADED),
	stored_position(sizeof(uint64_t)),
	stored_size(0x0),
	string(""),
	configuration_container(nullptr)
{
}

CalibrationCacheItem::CalibrationCacheItem(const CalibrationCacheItem& rhs) :
	record_number(rhs.record_number),
	name(rhs.name),
	flags(rhs.flags),
	stored_position(rhs.stored_position),
	stored_size(rhs.stored_size),
	string(rhs.string),
	image_mat(rhs.image_mat),
	configuration_container(rhs.configuration_container)
{
}

CalibrationCacheItem& CalibrationCacheItem::operator=(const CalibrationCacheItem& rhs)
{
	record_number           = rhs.record_number;
	name                    = rhs.name;
	flags                   = rhs.flags;
	stored_position         = rhs.stored_position;
	stored_size             = rhs.stored_size;
	string                  = rhs.string;
	image_mat               = rhs.image_mat;
	configuration_container = rhs.configuration_container;

	return *this;
}

IOMessages::IOResult CalibrationCacheItem::Open(const std::string& path,
                                                const size_t mode,
                                                FileIO& io)
{
	if(io != INVALID_HANDLE_VALUE)
	RETURN_ERROR(IOMessages::IO_FALSE);

	// Open file for read/write
	io = CreateFile(DMVS::CameraCalibration::Helper::Widen(path.c_str()),
	                GENERIC_WRITE + GENERIC_READ,
	                FILE_SHARE_READ,
	                nullptr,
	                mode == CalibrationCache::CACHE_WRITE ? CREATE_ALWAYS : OPEN_ALWAYS,
	                FILE_ATTRIBUTE_NORMAL /*+ FILE_FLAG_OVERLAPPED + FILE_FLAG_WRITE_THROUGH*/,
	                nullptr);

	if(io == INVALID_HANDLE_VALUE)
	RETURN_ERROR(IOMessages::IO_FALSE);

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheItem::Close(FileIO& io)
{
	if(io == INVALID_HANDLE_VALUE)
		return IOMessages::IO_OK;

	if(!CloseHandle(io))
	RETURN_ERROR(IOMessages::IO_FALSE);

	io = INVALID_HANDLE_VALUE;

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheItem::Read(FileIO& io,
                                                const bool toc_mode)
{
	if(io == INVALID_HANDLE_VALUE)
		return IOMessages::IO_OK;

	if((flags & NO_DATA_LOADED) != NO_DATA_LOADED)
		return IOMessages::IO_OK;

	if(stored_position == NOT_STORED) RETURN_ERROR(IOMessages::IO_FALSE);

	// Read header
	CalibrationFileItem header;
	if(Read(io, &header, sizeof(CalibrationFileItem), stored_position) != IOMessages::IO_OK)
	RETURN_ERROR(IOMessages::IO_FALSE);

	if(header.sync != CalibrationFileItem::SYNC)
	RETURN_ERROR(IOMessages::IO_FALSE);

	record_number = header.record_number;
	name          = Helper::Narrow(header.name);
	flags         = header.flags | STORED_IN_FILE;
	stored_size   = header.size;

	// No need for reading if we create the TOC only.
	// Just mark the item.
	if(toc_mode)
	{
		flags = flags | NO_DATA_LOADED;
		return IOMessages::IO_OK;
	}

	// Read data
	if(stored_size <= sizeof(CalibrationFileItem))
		return IOMessages::IO_OK;

	std::vector<Byte> encoded(stored_size - sizeof(CalibrationFileItem), 0x0);

	if(Read(io, &encoded[0], encoded.size(), -1) != IOMessages::IO_OK)
	RETURN_ERROR(IOMessages::IO_FALSE);

	// Update data
	if(Decode(encoded) != IOMessages::IO_OK)
	RETURN_ERROR(IOMessages::IO_FALSE);

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheItem::Read(FileIO& io,
                                                void* data,
                                                const size_t num_bytes,
                                                const uint64_t pos)
{
	// Sanity checks
	if(io == INVALID_HANDLE_VALUE)
	RETURN_ERROR(IOMessages::IO_FALSE);
	if(!data)
	RETURN_ERROR(IOMessages::IO_FALSE);
	if(num_bytes < 1)
	RETURN_ERROR(IOMessages::IO_FALSE);

	// Seek
	if(pos != uint64_t(-1))
	{
		LARGE_INTEGER p0;
		LARGE_INTEGER p1;
		p0.QuadPart = pos;

		if(SetFilePointerEx(io, p0, &p1, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
		RETURN_ERROR(IOMessages::IO_FALSE);

		if(p0.QuadPart != p1.QuadPart)
		RETURN_ERROR(IOMessages::IO_FALSE);
	}

	// Read
	const auto bytes_to_read = DWORD(num_bytes);
	DWORD bytes_read         = 0;

	if(!ReadFile(io, data, bytes_to_read, &bytes_read, nullptr))
	RETURN_ERROR(IOMessages::IO_FALSE);

	if(bytes_read != bytes_to_read)
	RETURN_ERROR(IOMessages::IO_FALSE);

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheItem::Write(FileIO& io)
{
	if(io == INVALID_HANDLE_VALUE)
		return IOMessages::IO_OK;

	if(stored_position != NOT_STORED)
		return IOMessages::IO_OK;

	if((flags & NO_DATA_LOADED) == NO_DATA_LOADED)
	RETURN_ERROR(IOMessages::IO_FALSE);

	// Prepare item data for writing
	std::vector<Byte> encoded;
	if(Encode(encoded) != IOMessages::IO_OK)
	RETURN_ERROR(IOMessages::IO_FALSE);

	// Create header
	CalibrationFileItem header;
	header.Set(record_number, name, flags, sizeof(CalibrationFileItem) + encoded.size());

	// Write header and encoded buffer
	if(Write(io, &header, sizeof(CalibrationFileItem), stored_position) != IOMessages::IO_OK)
	RETURN_ERROR(IOMessages::IO_FALSE);

	uint64_t encode_position;
	if(!encoded.empty() && Write(io, &encoded[0], encoded.size(), encode_position) != IOMessages::IO_OK)
	RETURN_ERROR(IOMessages::IO_FALSE);

	// Update item
	flags       = flags | STORED_IN_FILE;
	stored_size = header.size;

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheItem::Write(FileIO& io,
                                                 const void* data,
                                                 const size_t num_bytes,
                                                 uint64_t& pos)
{
	// Sanity checks
	if(io == INVALID_HANDLE_VALUE)
	RETURN_ERROR(IOMessages::IO_FALSE);

	if(!data)
	RETURN_ERROR(IOMessages::IO_FALSE);

	if(num_bytes < 1)
	RETURN_ERROR(IOMessages::IO_FALSE);

	// Seek to end of file.
	LARGE_INTEGER p0;
	LARGE_INTEGER p1;
	p0.QuadPart = 0;

	if(SetFilePointerEx(io, p0, &p1, FILE_END) == INVALID_SET_FILE_POINTER)
	RETURN_ERROR(IOMessages::IO_FALSE);

	// Write
	const auto bytes_to_write = DWORD(num_bytes);
	DWORD bytes_written       = 0;

	if(!WriteFile(io, data, bytes_to_write, &bytes_written, nullptr))
	RETURN_ERROR(IOMessages::IO_FALSE);

	if(bytes_to_write != bytes_written)
	RETURN_ERROR(IOMessages::IO_FALSE);

	// Store file position
	pos = p1.QuadPart;

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheItem::Encode(std::vector<Byte>& bytes) const
{
	bytes.clear();

	switch(flags & DATA_TYPES)
	{
	case RAW_BYTES:
		return EncodeRawBytes(bytes);

	case CV_RAW_MATRIX:
		return EncodeRawMatrix(bytes);

	case CV_MATRIX:
		return EncodeMatrix(bytes);

	case STRING:
		return EncodeString(bytes);

	case ATTRIBUTE_CONTAINER:
		return EncodeContainer(bytes);
	}

	RETURN_ERROR(IOMessages::IO_FALSE);
}

IOMessages::IOResult CalibrationCacheItem::Decode(const std::vector<Byte>& bytes)
{
	string    = "";
	image_mat = cv::Mat();
	//attribute_container = nullptr;

	if(bytes.empty())
		return IOMessages::IO_OK;

	switch(flags & DATA_TYPES)
	{
	case RAW_BYTES:
		return DecodeRawBytes(bytes);

	case CV_RAW_MATRIX:
		return DecodeRawMatrix(bytes);

	case CV_MATRIX:
		return DecodeMatrix(bytes);

	case STRING:
		string = (CHAR*)(&bytes[0]);
		return IOMessages::IO_OK;

	case ATTRIBUTE_CONTAINER:
		return DecodeContainer(bytes);

	default:
		break;
	}

	RETURN_ERROR(IOMessages::IO_FALSE);
}

IOMessages::IOResult CalibrationCacheItem::EncodeContainer(std::vector<Byte>& bytes) const
{
	//if(!attribute_container)
	//	return IOMessages::IO_OK;

	//if(SFMFile::containerToBytes(m_container, bytes, m_name + "__" + std::to_string(m_recno)))
	//	return IOMessages::IO_OK;
	//else
	//
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	return IOMessages::IO_FALSE;
}

IOMessages::IOResult CalibrationCacheItem::DecodeContainer(const std::vector<Byte>& bytes)
{
	//if(SFMFile::bytesToContainer(bytes, m_container, m_name + "__" + std::to_string(m_recno)))
	//	return IOMessages::IO_OK;
	//else
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	return IOMessages::IO_UNKNOWN_FORMAT;
}

IOMessages::IOResult CalibrationCacheItem::EncodeString(std::vector<Byte>& bytes) const
{
	if(string.empty())
		return IOMessages::IO_OK;

	//bytes.resize(m_string.bufferSize(), 0x0);
	//memcpy_s(&bytes[0], bytes.size(), m_string, bytes.size());

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheItem::EncodeMatrix(std::vector<Byte>& bytes) const
{
	// Data header
	auto rows     = image_mat.rows;
	auto cols     = image_mat.cols;
	auto channels = image_mat.channels();
	auto type     = image_mat.type();

	// Compress data
	switch(type)
	{
	case CV_8UC3:
	case CV_8UC1:
		bytes.resize(sizeof(int) * 4);

		memcpy(&bytes[0 * sizeof(int)], &rows, sizeof(int));
		memcpy(&bytes[1 * sizeof(int)], &cols, sizeof(int));
		memcpy(&bytes[2 * sizeof(int)], &channels, sizeof(int));
		memcpy(&bytes[3 * sizeof(int)], &type, sizeof(int));

		return image_mat.empty() ? IOMessages::IO_OK : CalibrationCache::ToJPEG(image_mat, bytes, 4 * sizeof(int));

	default:
		return EncodeRawMatrix(bytes);
	}
}

IOMessages::IOResult CalibrationCacheItem::DecodeMatrix(const std::vector<Byte>& bytes)
{
	if(bytes.empty()) RETURN_ERROR(IOMessages::IO_FALSE);

	int type;
	memcpy(&type, &bytes[3 * sizeof(int)], sizeof(int));

	switch(type)
	{
	case CV_8UC3:
	case CV_8UC1:
		return CalibrationCache::FromJPEG(bytes, image_mat, 4 * sizeof(int));

	default:
		return DecodeRawMatrix(bytes);
	}
}

IOMessages::IOResult CalibrationCacheItem::EncodeRawMatrix(std::vector<Byte>& bytes) const
{
	// Data header
	auto rows     = image_mat.rows;
	auto cols     = image_mat.cols;
	auto channels = image_mat.channels();
	auto type     = image_mat.type();

	bytes.resize(sizeof(int) * 4);

	memcpy(&bytes[0 * sizeof(int)], &rows, sizeof(int));
	memcpy(&bytes[1 * sizeof(int)], &cols, sizeof(int));
	memcpy(&bytes[2 * sizeof(int)], &channels, sizeof(int));
	memcpy(&bytes[3 * sizeof(int)], &type, sizeof(int));

	if(image_mat.empty())
		return IOMessages::IO_OK;

	//size_t msize = m_mat.total() * m_mat.elemSize();
	//bytes.resize(sizeof(int) * 4 + LZ4_compressBound((int)msize));

	//unsigned long destLen = LZ4_compressBound((int)msize);
	//unsigned long srcLen  = (unsigned long)msize;

	//destLen = LZ4_compress_limitedOutput((char*)m_mat.ptr(), (char*)&bytes[4 * sizeof(int)], srcLen, destLen);
	//if(destLen == 0)
	//RETURN_ERROR(IOMessages::IO_FALSE);

	//bytes.resize(sizeof(int) * 4 + destLen);

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheItem::DecodeRawMatrix(const std::vector<Byte>& bytes)
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;

	int rows;
	int cols;
	int channels;
	int type;

	memcpy(&rows, &bytes[0 * sizeof(int)], sizeof(int));
	memcpy(&cols, &bytes[1 * sizeof(int)], sizeof(int));
	memcpy(&channels, &bytes[2 * sizeof(int)], sizeof(int));
	memcpy(&type, &bytes[3 * sizeof(int)], sizeof(int));

	if(rows * cols <= 0)
	{
		image_mat = cv::Mat();
		return IOMessages::IO_OK;
	}

	image_mat.create(rows, cols, type);

	const auto mat_size = image_mat.total() * image_mat.elemSize();

	auto destination_length = unsigned long(mat_size);
	auto source_length      = unsigned long(bytes.size() - 4 * sizeof(int));

	/*source_length = LZ4_decompress_fast((char*)&bytes[4 * sizeof(int)], (char*)mat.ptr(), destination_length);

	if(source_length != (unsigned long)(bytes.size() - 4 * sizeof(int)))
	RETURN_ERROR(IOMessages::IO_FALSE);*/

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheItem::EncodeRawBytes(std::vector<Byte>& bytes) const
{
	// Data header
	auto rows     = image_mat.rows;
	auto cols     = image_mat.cols;
	auto channels = image_mat.channels();
	auto type     = image_mat.type();

	bytes.resize(sizeof(int) * 4);

	memcpy(&bytes[0 * sizeof(int)], &rows, sizeof(int));
	memcpy(&bytes[1 * sizeof(int)], &cols, sizeof(int));
	memcpy(&bytes[2 * sizeof(int)], &channels, sizeof(int));
	memcpy(&bytes[3 * sizeof(int)], &type, sizeof(int));

	if(image_mat.empty())
		return IOMessages::IO_OK;

	bytes.insert(bytes.end(), image_mat.datastart, image_mat.dataend);

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheItem::DecodeRawBytes(const std::vector<Byte>& bytes)
{
	int rows;
	int cols;
	int channels;
	int type;

	memcpy(&rows, &bytes[0 * sizeof(int)], sizeof(int));
	memcpy(&cols, &bytes[1 * sizeof(int)], sizeof(int));
	memcpy(&channels, &bytes[2 * sizeof(int)], sizeof(int));
	memcpy(&type, &bytes[3 * sizeof(int)], sizeof(int));

	if(rows * cols <= 0)
	{
		image_mat = cv::Mat();
		return IOMessages::IO_OK;
	}

	image_mat = cv::Mat(rows, cols, type, (void*)&bytes[4 * sizeof(int)]).clone();

	return IOMessages::IO_OK;
}

void CalibrationCacheItem::Release()
{
	flags |= NO_DATA_LOADED;

	switch(flags & DATA_TYPES)
	{
	case STRING:
		string = "";
		break;

	case ATTRIBUTE_CONTAINER:
		//attribute_container = nullptr;
		break;

	case CONFIG_CONTAINER:
		configuration_container = nullptr;
		break;

	case CV_RAW_MATRIX:
		image_mat.release();
		break;

	case CV_MATRIX:
		image_mat.release();
		break;

	default:
		break;
	}
}

CalibrationCacheItem::Key CalibrationCacheItem::KeyValue(const size_t record_number,
                                                         const std::string& name)
{
	std::hash<std::string> hash_fn;

	// hash is shifted to prevent collisions
	const auto sh = hash_fn(name);

	// TODO_RD: Please reconsider using string hashes. Probably enums would be enough
	return record_number + (sh << 32);
}
