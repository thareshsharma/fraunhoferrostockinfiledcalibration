#include "calibration_calculator.h"
#include "relative_fix_cameras_parameter_optimizer.h"
#include "camera_calibration_fit.h"
#include "helper.h"
#include "independent_frame_transformations.h"
#include "unsupported/Eigen/src/NumericalDiff/NumericalDiff.h"
#include "relative_fix_board_with_overlap_transformation.h"
#include "enum_image_channel_type.h"
#include "projector_calibration_fit.h"

namespace DMVS
{
namespace CameraCalibration
{
CalibrationCalculator::CalibrationCalculator() :
	camera_calibration_fit_(nullptr),
	calibration_boards_transformation_(nullptr),
	relative_fix_cameras_parameter_optimizer_(nullptr),
	camera_system_transformation_(nullptr),
	minimum_initial_frame_marker_(4),
	minimum_initial_marker_relative_camera_(4),
	image_width_(Constants::DMVS_IMAGE_WIDTH),
	image_height_(Constants::DMVS_IMAGE_HEIGHT),
	if_observations_processed_(false)
{
}

CalibrationCalculator::CalibrationCalculator(std::vector<std::shared_ptr<Observation>> observations) :
	camera_calibration_fit_(nullptr),
	calibration_boards_transformation_(nullptr),
	relative_fix_cameras_parameter_optimizer_(nullptr),
	camera_system_transformation_(nullptr),
	observations_(std::move(observations)),
	minimum_initial_frame_marker_(4),
	minimum_initial_marker_relative_camera_(4),
	image_width_(Constants::DMVS_IMAGE_WIDTH),
	image_height_(Constants::DMVS_IMAGE_HEIGHT),
	if_observations_processed_(false)
{
}

CalibrationCalculator::~CalibrationCalculator()
= default;

CalibrationCalculator::CalibrationCalculator(const CalibrationCalculator& rhs)
{
	camera_calibration_fit_                   = rhs.camera_calibration_fit_;
	calibration_boards_transformation_        = rhs.calibration_boards_transformation_;
	relative_fix_cameras_parameter_optimizer_ = rhs.relative_fix_cameras_parameter_optimizer_;
	camera_system_transformation_             = rhs.camera_system_transformation_;
	observations_                             = rhs.observations_;
	boards_                                   = rhs.boards_;
	option_result_                            = rhs.option_result_;

	minimum_initial_frame_marker_           = rhs.minimum_initial_frame_marker_;
	minimum_initial_marker_relative_camera_ = rhs.minimum_initial_marker_relative_camera_;

	image_width_  = rhs.image_width_;
	image_height_ = rhs.image_height_;

	if_observations_processed_ = rhs.if_observations_processed_;
}

CalibrationCalculator& CalibrationCalculator::operator=(const CalibrationCalculator& rhs)
{
	if(this != &rhs)
	{
		camera_calibration_fit_                   = rhs.camera_calibration_fit_;
		calibration_boards_transformation_        = rhs.calibration_boards_transformation_;
		relative_fix_cameras_parameter_optimizer_ = rhs.relative_fix_cameras_parameter_optimizer_;
		camera_system_transformation_             = rhs.camera_system_transformation_;
		observations_                             = rhs.observations_;
		boards_                                   = rhs.boards_;
		option_result_                            = rhs.option_result_;

		minimum_initial_frame_marker_           = rhs.minimum_initial_frame_marker_;
		minimum_initial_marker_relative_camera_ = rhs.minimum_initial_marker_relative_camera_;

		image_width_  = rhs.image_width_;
		image_height_ = rhs.image_height_;
	}

	return *this;
}

int CalibrationCalculator::SetBoards(std::vector<std::shared_ptr<BoardDetector>> boards)
{
	if(boards.empty())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Boards vector is empty. Can't do anything with empty boards so returning back";
		return -1;
	}

	// copy of boards needed later for deactivation of coded marker
	boards_ = std::move(boards);

	//BoardsPositionFinder: Get the calibration boards in format required by BoardsPositionFinder
	std::map<BoardID, std::shared_ptr<CalibrationTargetBase>> all_boards;

	for(auto& board : boards_)
	{
		const auto board_id  = board->GetBoardId();
		all_boards[board_id] = std::make_shared<CalibrationTargetFixMarkers>(board->GetBoardMarkers());
	}

	calibration_boards_transformation_ = std::make_shared<RelativeFixBoardsWithOverlapTransformation>(all_boards);

	//The first board is the reference board
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Set reference board to : " << boards_[0]->GetBoardId();
	calibration_boards_transformation_->SetReferenceBoardID(boards_[0]->GetBoardId());

	return 0;
}

size_t CalibrationCalculator::SetRelativeFixedCamera(std::map<size_t, std::shared_ptr<const CameraSensor>>& camera_sensor_map,
                                                     ImageChannel image_channel)
{
	// Use this cams modifier for calibration
	relative_fix_cameras_parameter_optimizer_ = std::make_shared<RelativeFixCamerasParameterOptimizer>(camera_sensor_map, image_channel);
	relative_fix_cameras_parameter_optimizer_->SetRequiredNumberMarkers(minimum_initial_marker_relative_camera_);

	return 0;
}

int CalibrationCalculator::SetRelativeFixedCamera(const CaptureCalibration& capture_calibration)
{
	std::map<size_t, std::shared_ptr<const CameraSensor>> init_cam_sensors;
	//init_cam_sensors[CHANNEL_BGR0]  = capture_calibration.GetCalibration(CHANNEL_BGR0);
	init_cam_sensors[CHANNEL_PROJECTOR0] = capture_calibration.GetCalibration(CHANNEL_PROJECTOR0);
	init_cam_sensors[CHANNEL_GREY0]      = capture_calibration.GetCalibration(CHANNEL_GREY0);
	init_cam_sensors[CHANNEL_GREY1]      = capture_calibration.GetCalibration(CHANNEL_GREY1);

	// Create a rigid camera constellation and set specific parameters for initialization
	relative_fix_cameras_parameter_optimizer_ = std::make_shared<RelativeFixCamerasParameterOptimizer>(init_cam_sensors, CHANNEL_BGR0);
	relative_fix_cameras_parameter_optimizer_->SetRequiredNumberMarkers(minimum_initial_marker_relative_camera_);

	//use this cams modifier for calibration
	/*relative_fix_cameras_parameter_optimizer_ = camera_parameter_optimizer;*/
	return 0;
}

int CalibrationCalculator::SetRelativeFixedCamera(const std::shared_ptr<const CameraSensor>& calibration,
                                                  const std::set<size_t>& channels)
{
	relative_fix_cameras_parameter_optimizer_ = std::make_shared<RelativeFixCamerasParameterOptimizer>(channels, calibration, *channels.begin());
	return 0;
}

std::shared_ptr<RelativeFixCamerasParameterOptimizer> CalibrationCalculator::GetRelativeFixedCamerasParameterOptimizer() const
{
	if(!relative_fix_cameras_parameter_optimizer_)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Observation not processed. Returning null pointer ";
	}

	return if_observations_processed_ ? relative_fix_cameras_parameter_optimizer_ : nullptr;
}

std::shared_ptr<RelativeFixCamerasParameterOptimizer> CalibrationCalculator::GetRelativeFixedCamerasParameterOptimizerLens() const
{
	if(!relative_fix_cameras_parameter_optimizer_)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Observation not processed. Returning null pointer ";
	}

	return relative_fix_cameras_parameter_optimizer_;
}

std::shared_ptr<CalibrationBoardsTransformation> CalibrationCalculator::GetCalibrationBoardsTransformation() const
{
	if(!relative_fix_cameras_parameter_optimizer_)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Observation not processed. Returning null pointer ";
	}

	return if_observations_processed_ ? calibration_boards_transformation_ : nullptr;
}

std::shared_ptr<CameraSystemTransformation> CalibrationCalculator::GetCameraSystemTransformation() const
{
	if(!relative_fix_cameras_parameter_optimizer_)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Observation not processed. Returning null ptr ";
	}

	return if_observations_processed_ ? camera_system_transformation_ : nullptr;
}

int CalibrationCalculator::InitializeCameraSystemTransformation(const size_t frame_count)
{
	std::shared_ptr<CameraSystemTransformation> camera_system_transformation = std::make_shared<IndependentFrameTransformations>(frame_count);
	SetCameraSystemTransformation(camera_system_transformation);

	return 0;
}

int CalibrationCalculator::SetCameraSystemTransformation(std::shared_ptr<CameraSystemTransformation>& system_position_finder)
{
	camera_system_transformation_ = system_position_finder;
	return 0;
}

void CalibrationCalculator::ClearObservationsDeactivation()
{
	// just used all the observations. no check is implemented for 2 sigma.
	for(auto& observation : observations_)
		observation->SetDeactivated(false);
}

void CalibrationCalculator::LogParams(std::map<unsigned long long, std::shared_ptr<const CameraSensor>>::value_type& calibration,
                                      std::vector<double> params)
{
	const cv::Mat rotation_vector{params[9], params[10], params[11]};
	cv::Mat rotation_matrix;

	cv::Rodrigues(rotation_vector, rotation_matrix);

	std::stringstream rot_mat_stream;

	for(auto i = 0; i < 3; ++i)
	{
		for(auto j = 0; j < 3; ++j)
		{
			rot_mat_stream << " " << rotation_matrix.at<double>(i, j);
		}
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Channel [" << calibration.first << "] Parameters -----------------------------------" <<
 Constants::LOG_START;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Channel [" << calibration.first << "] Camera Matrix [" << params[0] << " 0 " << params[2] <<
 " 0 " << params[1] << " " << params[3] << " 0 0 1 ]";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Channel [" << calibration.first << "] Distortion [" << params[4] << " " << params[5] << " " <<
 params[6] << " " << params[7] << " " << params[8] << "]";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Channel [" << calibration.first << "] Rotation Matrix [3x3] Row Major [" << rot_mat_stream.
str() << "]";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Channel [" << calibration.first << "] Translation Vector[" << params[12] << " " << params[13]
 << " " << params[14] << "]";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Channel [" << calibration.first << "] Parameters -------------------------------------" <<
 Constants::LOG_END;
}

bool CalibrationCalculator::ProcessObservationsInHouse(bool use_existing_observations)
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_START;

	// latestRunResult: in/out result parameters of the latest step
	Eigen::VectorXd latest_run_result;

	if(use_existing_observations)
	{
		auto rms_init  = InitSystem(latest_run_result);
		option_result_ = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_WITH_EXISTING_OBSERVATIONS);
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Using Existing observations Initial RMS [ " << rms_init << " ]";
		return false;
	}

	auto rms_init = InitSystem(latest_run_result);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initiated system with first RMS [ " << rms_init << " ]";

	// Deactivate observations above 2 sigma and recalculate initial values ???????????????
	// This means that observation shall be used for calculation and not thrown away.
	ClearObservationsDeactivation();

	CameraCalibrationFit::UpdateEllipseCorrections(observations_,
	                                               relative_fix_cameras_parameter_optimizer_,
	                                               camera_system_transformation_,
	                                               calibration_boards_transformation_,
	                                               2 * rms_init);

	rms_init = InitSystem(latest_run_result);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initiated system with second RMS [ " << rms_init << " ]";

	// Output for board position initialization
	{
		auto board_ids = calibration_boards_transformation_->GetBoardIDs();

		for(const auto board_id : board_ids)
		{
			auto marker_map = calibration_boards_transformation_->GetBoard(board_id)->GetMarkerPositionMap();

			for(const auto& marker : marker_map)
			{
				auto marker_position = calibration_boards_transformation_->GetMarkerPosition(board_id, marker.first);
				//			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initialization Results of Board Position -> Board Id [ " << board_id <<
				//			" ] Marker Id [ " << marker.first << " ] Marker Position : x [ " << marker_position.x << " ]  y [ " << marker_position.y << " ]  z [ " <<
				//marker_position.z << " ]";
			}
		}
	}

	// Output for relative camera position initialization
	{
		for(auto calibration_map : relative_fix_cameras_parameter_optimizer_->GetAllCalibrations())
		{
			cv::Mat rotational_matrix, translation_vector;
			calibration_map.second->GetExtrinsicParameters(rotational_matrix, translation_vector);
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initialization Results of Relative Camera Position [ " << calibration_map.first <<
				" ] Translation Vector [ " <<
				translation_vector.at<double>(0, 0) << " " <<
				translation_vector.at<double>(1, 0) << " " <<
				translation_vector.at<double>(2, 0) << " ] Rotational Matrix [ " <<
				rotational_matrix.at<double>(0, 0) << " " <<
				rotational_matrix.at<double>(1, 0) << " " <<
				rotational_matrix.at<double>(2, 0) << " " <<
				rotational_matrix.at<double>(0, 1) << " " <<
				rotational_matrix.at<double>(1, 1) << " " <<
				rotational_matrix.at<double>(2, 1) << " " <<
				rotational_matrix.at<double>(0, 2) << " " <<
				rotational_matrix.at<double>(1, 2) << " " <<
				rotational_matrix.at<double>(2, 2) << " ]";
		}
	}


	// Output for frame position initialization
	{
		for(auto frame_map : camera_system_transformation_->GetFrameTransformationMap())
		{
			auto rotational_matrix  = frame_map.second.GetRotationalTransformation();
			auto translation_vector = frame_map.second.GetTranslationalTransformation();
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initialization Results of Frame Position [ " << frame_map.first <<
				" ] Translation Vector  [ " <<
				translation_vector.at<double>(0, 0) << " " <<
				translation_vector.at<double>(1, 0) << " " <<
				translation_vector.at<double>(2, 0) << " ] Rotational Matrix [ " <<
				rotational_matrix.at<double>(0, 0) << " " <<
				rotational_matrix.at<double>(1, 0) << " " <<
				rotational_matrix.at<double>(2, 0) << " " <<
				rotational_matrix.at<double>(0, 1) << " " <<
				rotational_matrix.at<double>(1, 1) << " " <<
				rotational_matrix.at<double>(2, 1) << " " <<
				rotational_matrix.at<double>(0, 2) << " " <<
				rotational_matrix.at<double>(1, 2) << " " <<
				rotational_matrix.at<double>(2, 2) << " ] ";
		}
	}

	// Parameter for identification of additional marker
	CameraCalibrationFit::MarkerIdentificationParams marker_identification_params;
	marker_identification_params.image_height            = image_height_;
	marker_identification_params.image_width             = image_width_;
	marker_identification_params.max_projection_distance = 2.0;
	// DEFAULT
	marker_identification_params.max_image_center_distance = 0.9; // 750px as in old version for comparison
	marker_identification_params.min_radius                = 0.003;
	marker_identification_params.max_radius                = 0.03;

	int marker_identified_count_last_run    = -1;
	int marker_identified_count_current_run = 0;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : In Optimize outside while Loop -> Current Run [ " << marker_identified_count_current_run <<
		" ]  Last Run [ " << marker_identified_count_last_run << " ]";
	// try to assign new observations iteratively until no new are found any more
	while(marker_identified_count_current_run > marker_identified_count_last_run)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : In Optimize while Loop -> Current Run [ " << marker_identified_count_current_run <<
			" ]  Last Run [ " <<
			marker_identified_count_last_run << " ]";

		// New iteration starts current becomes last run identification
		marker_identified_count_last_run = marker_identified_count_current_run;

		// Optimize with all assigned observations,
		auto rms_num = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_WITHOUT_ELLIPSE_CORRECTION);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : RMS [ " << rms_num.second << " ] used [ " << rms_num.first << " ] Markers";

		// Output number of identified marker
		// reset all marker detections
		for(auto& observation : observations_)
		{
			observation->SetBoardId(Constants::DEFAULT_SIZE_T_VALUE);
			observation->SetMarkerId(Constants::DEFAULT_SIZE_T_VALUE);
		}

		// Identify all marker with current calibration result
		marker_identified_count_current_run = int(CameraCalibrationFit::IdentifyUncodedMarker(observations_,
		                                                                                      relative_fix_cameras_parameter_optimizer_,
		                                                                                      camera_system_transformation_,
		                                                                                      calibration_boards_transformation_,
		                                                                                      marker_identification_params));

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Found Uncoded Marker [ " << marker_identified_count_current_run << " ]";
	}

	// board/marker assignment of observations is final now
	{
		auto rms_num = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_WITH_ELLIPSE_CORRECTION);
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : First optimization of initialization RMS [ " << rms_num.second << " ] used [ " << rms_num.first << " ] Markers";

		for(auto& calibration : relative_fix_cameras_parameter_optimizer_->GetAllCalibrations())
		{
			std::vector<size_t> fixations;
			//auto params = calibration.second->GetParamsVector(fixations);
			//LogParams(calibration, params);
		}
	}

	// optimize with all assigned observations with applied ellipse correction
	ClearObservationsDeactivation();
	CameraCalibrationFit::UpdateEllipseCorrections(observations_,
	                                               relative_fix_cameras_parameter_optimizer_,
	                                               camera_system_transformation_,
	                                               calibration_boards_transformation_);

	std::pair<size_t, double> rms_num;
	{
		// Check if this is ellipse correction or not
		rms_num = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_WITH_ELLIPSE_CORRECTION);

		// Output final calibration
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : RMS [ " << rms_num.second << " ] used [ " << rms_num.first << " ] Markers";
			for(auto& calibration : relative_fix_cameras_parameter_optimizer_->GetAllCalibrations())
			{
				std::vector<size_t> fixations;
				/*		auto params = calibration.second->GetParamsVector(fixations);
						LogParams(calibration, params);*/
			}
		}
	}

	// optimize with outlier removal and ellipse correction
	ClearObservationsDeactivation();
	CameraCalibrationFit::UpdateEllipseCorrections(observations_,
	                                               relative_fix_cameras_parameter_optimizer_,
	                                               camera_system_transformation_,
	                                               calibration_boards_transformation_,
	                                               rms_num.second * 2.0);
	{
		auto all_params_free_rms = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_REASSIGNED_OUTLIER_REMOVED);
		// Output final calibration
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : RMS [ " << all_params_free_rms.second << " ] used [ " << all_params_free_rms.first <<
				" ] Marker";
			for(auto& calibration : relative_fix_cameras_parameter_optimizer_->GetAllCalibrations())
			{
				std::vector<size_t> fixations;
				//auto params = calibration.second->GetParamsVector(fixations);
				//LogParams(calibration, params);
			}
		}
	}

	// final reset of all marker detections
	for(auto& obs : observations_)
	{
		obs->SetBoardId(Constants::DEFAULT_SIZE_T_VALUE);
		obs->SetMarkerId(Constants::DEFAULT_SIZE_T_VALUE);
	}

	auto num_identified = CameraCalibrationFit::IdentifyUncodedMarker(observations_,
	                                                                  relative_fix_cameras_parameter_optimizer_,
	                                                                  camera_system_transformation_,
	                                                                  calibration_boards_transformation_,
	                                                                  marker_identification_params);

	auto num_deactivated = DeactivateCodedMarker(observations_, boards_);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Identified [ " << num_identified << " ] marker, Deactivated [ " << num_deactivated <<
		" ] Coded Marker";

	// optimize with outlier removal and ellipse correction
	ClearObservationsDeactivation();
	CameraCalibrationFit::UpdateEllipseCorrections(observations_,
	                                               relative_fix_cameras_parameter_optimizer_,
	                                               camera_system_transformation_,
	                                               calibration_boards_transformation_);
	{
		auto all_params_free_rms = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_REASSIGNED_ALL);
		rms_num                  = all_params_free_rms;
		// Output final calibration
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : RMS [ " << all_params_free_rms.second << " ] used [ " << all_params_free_rms.first <<
				" ] Marker";
			for(auto& calibration : relative_fix_cameras_parameter_optimizer_->GetAllCalibrations())
			{
				std::vector<size_t> fixations;
				/*auto params = calibration.second->GetParamsVector(fixations);
				LogParams(calibration, params);*/
			}
		}
	}

	ClearObservationsDeactivation();

	// final calculation should be without coded marker because coded marker tend to drift
	// toward set bits of the code ring in blurred images at longer distances
	num_deactivated = DeactivateCodedMarker(observations_, boards_);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Identified [ " << num_identified << " ] marker, deactivated [" << num_deactivated <<
		" ] coded marker";
	CameraCalibrationFit::UpdateEllipseCorrections(observations_,
	                                               relative_fix_cameras_parameter_optimizer_,
	                                               camera_system_transformation_,
	                                               calibration_boards_transformation_,
	                                               rms_num.second * 2.0);
	{
		auto all_params_free_rms = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_REASSIGNED_OUTLIER_REMOVED);
		// Output final calibration
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : RMS [ " << all_params_free_rms.second << " ] used  [ " << all_params_free_rms.first <<
				" ] Marker ";
			for(auto& calibration : relative_fix_cameras_parameter_optimizer_->GetAllCalibrations())
			{
				std::vector<size_t> fixations;
				//auto params = calibration.second->GetParamsVector(fixations);
				//LogParams(calibration, params);
			}
		}

		option_result_ = all_params_free_rms;
	}

	if_observations_processed_ = rms_num.second < Constants::CALIBRATION_MAX_PERMITTED_RMS_ERROR;

	if(!if_observations_processed_)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Observations processing failed------ ";
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_END;

	return if_observations_processed_;
}

bool CalibrationCalculator::ProcessObservationsInfield()
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_START;

	// latestRunResult: in/out result parameters of the latest step
	Eigen::VectorXd latest_run_result;
	const auto rms_init = InitSystem(latest_run_result);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initiated system with first RMS [" << rms_init << "]";

	// Deactivate observations above 2 sigma and recalculate initial values ???????????????
	// This means that observation shall be used for calculation and not thrown away.
	ClearObservationsDeactivation();

	//CameraCalibrationFit::UpdateEllipseCorrections(observations_,
	//                                               relative_fix_cameras_parameter_optimizer_,
	//                                               camera_system_position_finder_,
	//                                               calibration_boards_transformation_,
	//                                               2 * rms_init);
	// rms_init = InitSystem(latest_run_result);
	// BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initiated system with second RMS [" << rms_init << "]";

	// Optimize with all assigned observations,
	const auto rms_num = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_WITHOUT_ELLIPSE_CORRECTION);


	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : RMS [" << rms_num.second << "] used [" << rms_num.first << "] Markers";

	// Ellipse correction optimization
	//{
	//	const auto rms_num = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_WITH_ELLIPSE_CORRECTION);
	//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : First optimization of initialization RMS [" << rms_num.second << "] used [" << rms_num.first << "] Markers";

	for(auto& calibration : relative_fix_cameras_parameter_optimizer_->GetAllCalibrations())
	{
		const auto params = calibration.second->GetParamsVector();
		LogParams(calibration, params);
	}


	// Optimize with all assigned observations with applied ellipse correction
	// CameraCalibrationFit::UpdateEllipseCorrections(observations_, relative_fix_cameras_parameter_optimizer_, camera_system_position_finder_, calibration_boards_transformation_);

	// Check if this is ellipse correction or not
	//rms_num = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_WITH_ELLIPSE_CORRECTION);

	// Optimize with outlier removal and ellipse correction
	//CameraCalibrationFit::UpdateEllipseCorrections(observations_,
	//                                               relative_fix_cameras_parameter_optimizer_,
	//                                               camera_system_position_finder_,
	//                                               calibration_boards_transformation_,
	//                                               rms_num.first * 2.0);

	//auto all_params_free_rms = OptimizeSystem(latest_run_result, Constants::CALIBRATION_CALC_REASSIGNED_OUTLIER_REMOVED);

	if_observations_processed_ = rms_num.second < 0.065;

	if(!if_observations_processed_)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Observations processing failed : " << rms_num.second;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_END;

	return if_observations_processed_;
}

int CalibrationCalculator::ReplaceWithNewCalibration(CaptureCalibration& result_calibration) const
{
	result_calibration.SetCalibration(CHANNEL_GREY0, relative_fix_cameras_parameter_optimizer_->GetCalibration(CHANNEL_GREY0));
	result_calibration.SetCalibration(CHANNEL_GREY1, relative_fix_cameras_parameter_optimizer_->GetCalibration(CHANNEL_GREY1));
	result_calibration.SetCalibration(CHANNEL_PROJECTOR0, relative_fix_cameras_parameter_optimizer_->GetCalibration(CHANNEL_PROJECTOR0));

	return 0;
}

void CalibrationCalculator::SetMinimumInitialMarker(const size_t minimum_initial_frame_marker,
                                                    const size_t minimum_initial_marker_relative_camera)
{
	minimum_initial_frame_marker_           = minimum_initial_frame_marker;
	minimum_initial_marker_relative_camera_ = minimum_initial_marker_relative_camera;
}

void CalibrationCalculator::DrawErrors(cv::Mat& image,
                                       const size_t frame_id,
                                       size_t channel,
                                       const cv::Scalar& color) const
{
	const CameraCalibrationFit camera_calibration_fit(relative_fix_cameras_parameter_optimizer_,
	                                                  camera_system_transformation_,
	                                                  calibration_boards_transformation_,
	                                                  observations_,
	                                                  false);
	const auto board_ids = calibration_boards_transformation_->GetBoardIDs();
	auto board_count     = 0;
	std::stringstream text1;
	text1 << "Channel " << channel << " Frame Id " << frame_id;
	cv::putText(image, text1.str(), cv::Point(25, 25), cv::FONT_HERSHEY_DUPLEX, 1.0, Constants::WHITE, 1, cv::LINE_AA);

	for(const auto& board_id : board_ids)
	{
		if(channel == Constants::DEFAULT_SIZE_T_VALUE)
			channel = relative_fix_cameras_parameter_optimizer_->GetReferenceCameraId();

		// get 3D points in camera local coordinates
		std::vector<cv::Point3d> camera_3d_points;
		auto projected_points = camera_calibration_fit.ProjectBoardPoints(camera_system_transformation_,
		                                                                  relative_fix_cameras_parameter_optimizer_,
		                                                                  calibration_boards_transformation_,
		                                                                  frame_id,
		                                                                  channel,
		                                                                  board_id,
		                                                                  nullptr,
		                                                                  nullptr,
		                                                                  &camera_3d_points);

		auto projection_error = camera_calibration_fit.GetProjectionErrors(camera_system_transformation_,
		                                                                   relative_fix_cameras_parameter_optimizer_,
		                                                                   frame_id,
		                                                                   channel,
		                                                                   board_id,
		                                                                   calibration_boards_transformation_);

		const auto calibration = relative_fix_cameras_parameter_optimizer_->GetCalibration(channel);
		auto square_sum        = 0.0;

		for(auto& p : projection_error)
			square_sum += p.dot(p);

		const auto rms = sqrt(square_sum / double(projection_error.size()));
		text1.str("");
		text1.clear();

		text1 << "Board Id [" << board_id << "] RMS [" << std::setprecision(3) << rms << "]";
		cv::putText(image, text1.str(), cv::Point(25, board_count + 60), cv::FONT_HERSHEY_DUPLEX, 1.0, Constants::WHITE, 1, cv::LINE_AA);
		board_count += 35;

		std::vector<cv::Point2d> undistorted_points;

		if(projected_points.empty())
		{
			continue;
		}

		calibration->UndistortPoints2(projected_points, undistorted_points, camera_3d_points);

		// sum up all errors orthogonal to radial direction
		double non_radian_component_sum  = 0;
		const auto projected_points_size = projected_points.size();

		for(auto idx = 0; idx < projected_points_size; ++idx)
		{
			const auto scale = 100.0;
			auto image_point = projected_points[idx] - projection_error[idx];
			cv::line(image, image_point, image_point + projection_error[idx] * scale, color, 3, cv::LINE_AA);
			cv::Vec2d error_vector(projection_error[idx].x, projection_error[idx].y);
			cv::Vec2d rad_dir(undistorted_points[idx].x, undistorted_points[idx].y);
			const auto angle                = Helper::AngleTo(error_vector, rad_dir);
			auto radial_component           = cos(angle);
			const auto non_radial_component = sin(angle) * Helper::VectorLength(error_vector);
			non_radian_component_sum += (non_radial_component * non_radial_component);
		}

		non_radian_component_sum /= double(projected_points_size);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Frame Id [" << frame_id << "] RMS [" << rms << "] : (Non Radian Component [" << sqrt(
			non_radian_component_sum) << "] )";

		// draw all observations
		for(auto& observation : observations_)
		{
			if(observation->GetImageChannel() != channel || observation->GetFrameId() != frame_id)
			{
				continue;
			}

			const auto center     = observation->GetRawEllipseCenter();
			auto center_point_vec = cv::Vec2d(center.x, center.y);
			const auto correction = observation->GetRawEllipseDistortionCorrection();
			auto correction_vec   = cv::Vec2d(correction.x, correction.y);
			auto dot_color        = observation->GetDeactivated() ? Constants::BRIGHT_BLUE : Constants::BRIGHT_GREEN;
			cv::circle(image, cv::Point2d(center_point_vec[0] + correction_vec[0], center_point_vec[1] + correction_vec[1]), 2, dot_color, -1);
		}
	}
}

double CalibrationCalculator::AppendErrors(std::ofstream& out,
                                           const size_t frame_id,
                                           size_t camera_id) const
{
	const CameraCalibrationFit camera_calibration_fit(relative_fix_cameras_parameter_optimizer_,
	                                                  camera_system_transformation_,
	                                                  calibration_boards_transformation_,
	                                                  observations_,
	                                                  false);

	const auto board_ids = calibration_boards_transformation_->GetBoardIDs();
	auto error           = 0.0;
	auto sum             = 0.0;

	for(const auto& board_id : board_ids)
	{
		if(camera_id == Constants::DEFAULT_SIZE_T_VALUE)
			camera_id = relative_fix_cameras_parameter_optimizer_->GetReferenceCameraId();

		// get 3D points in camera local coordinates
		std::vector<cv::Point3d> camera_points_3d;
		CalibrationBoardData board_measurement_type;

		auto projection_points = camera_calibration_fit.ProjectBoardPoints(camera_system_transformation_,
		                                                                   relative_fix_cameras_parameter_optimizer_,
		                                                                   calibration_boards_transformation_,
		                                                                   frame_id,
		                                                                   camera_id,
		                                                                   board_id,
		                                                                   nullptr,
		                                                                   &board_measurement_type,
		                                                                   &camera_points_3d);

		auto projection_error = camera_calibration_fit.GetProjectionErrors(camera_system_transformation_,
		                                                                   relative_fix_cameras_parameter_optimizer_,
		                                                                   frame_id,
		                                                                   camera_id,
		                                                                   board_id,
		                                                                   calibration_boards_transformation_);

		const auto board_marker_ids_ok = board_measurement_type.marker_ids.size() == projection_points.size();

		if(projection_points.empty())
		{
			continue;
		}

		const auto calibration = relative_fix_cameras_parameter_optimizer_->GetCalibration(camera_id);
		std::vector<cv::Point2d> undistorted_points;

		if(projection_points.empty())
		{
			continue;
		}

		calibration->UndistortPoints2(projection_points, undistorted_points, camera_points_3d);
		const auto projection_error_size = projection_error.size();

		for(auto i = 0; i < projection_error_size; ++i)
		{
			// calc radial component
			cv::Vec2d error_vector(projection_error[i].x, projection_error[i].y);
			cv::Vec2d radial_direction(undistorted_points[i].x, undistorted_points[i].y);
			cv::Vec2d tangential_direction(radial_direction[1], -radial_direction[0]);

			const auto angle_radial_direction = Helper::AngleTo(error_vector, radial_direction);
			const auto radial_component       = cos(angle_radial_direction) * Helper::VectorLength(error_vector);

			//const auto angle_tangential_direction = Helper::AngleTo(error_vector, tangential_direction);
			const auto tangential_component       = sin(angle_radial_direction) * Helper::VectorLength(error_vector);

			char str[2048];
			sprintf_s(str,
			          "BoardId %d FrameID %d CameraId %d ErrorX %f ErrorY %f ErrorRad %f ErrorTan %f MeanXY %f %f CameraLocalXYZ %f %f %f MarkerId %d ",
			          int(board_id),
			          int(frame_id),
			          int(camera_id),
			          projection_error[i].x,
			          projection_error[i].y,
			          radial_component,
			          tangential_component,
			          projection_points[i].x,
			          projection_points[i].y,
			          camera_points_3d[i].x,
			          camera_points_3d[i].y,
			          camera_points_3d[i].z,
			          board_marker_ids_ok ? int(board_measurement_type.marker_ids[i]) : -1);

			out << str << std::endl;
			error += cv::norm(projection_error[i]);
			sum += 1.0;
		}
	}

	return error / sum;
}

void CalibrationCalculator::AppendFrameAndBoardPositions(std::ofstream& out) const
{
	// Add all board positions
	const auto board_ids = calibration_boards_transformation_->GetBoardIDs();

	for(const auto& board_id : board_ids)
	{
		auto transformation = calibration_boards_transformation_->GetBoardTransformation(board_id);

		// Get transformation of board
		cv::Mat translation_vector = transformation.GetTranslationalTransformation();
		cv::Mat rotational_vector;
		cv::Rodrigues(transformation.GetRotationalTransformation(), rotational_vector);

		char str[2048];
		sprintf_s(str,
		          "Board Position Id %d tvec %f %f %f rvec %f %f %f",
		          int(board_id),
		          translation_vector.at<double>(0, 0),
		          translation_vector.at<double>(1, 0),
		          translation_vector.at<double>(2, 0),
		          rotational_vector.at<double>(0, 0),
		          rotational_vector.at<double>(1, 0),
		          rotational_vector.at<double>(2, 0));

		out << str << std::endl;
	}

	// Add all frame positions
	const auto frames = camera_system_transformation_->GetFrameTransformationMap();

	for(std::pair<size_t, Transformation> frame : frames)
	{
		auto transformation = frame.second;

		// Get transformation of frame
		cv::Mat translational_vector = transformation.GetTranslationalTransformation();
		cv::Mat rotational_vector;
		cv::Rodrigues(transformation.GetRotationalTransformation(), rotational_vector);

		char str[2048];
		sprintf_s(str,
		          "Frame Position Id %d tvec %f %f %f rvec %f %f %f",
		          int(frame.first),
		          translational_vector.at<double>(0, 0),
		          translational_vector.at<double>(1, 0),
		          translational_vector.at<double>(2, 0),
		          rotational_vector.at<double>(0, 0),
		          rotational_vector.at<double>(1, 0),
		          rotational_vector.at<double>(2, 0));

		out << str << std::endl;
	}
}

double CalibrationCalculator::GetChannelRMS(const size_t camera_id)
{
	const CameraCalibrationFit camera_calibration_fit(relative_fix_cameras_parameter_optimizer_,
	                                                  camera_system_transformation_,
	                                                  calibration_boards_transformation_,
	                                                  observations_,
	                                                  false);

	std::set<size_t> frame_ids;
	for(const auto observation : observations_)
	{
		frame_ids.insert(observation->GetFrameId());
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Get RMS of camera id [" << camera_id << "] from " << frame_ids.size() << "frames  ";
	const auto board_ids = calibration_boards_transformation_->GetBoardIDs();

	auto error_sum         = 0.0;
	auto measurement_count = 0;

	for(const auto& board_id : board_ids)
	{
		for(auto frame_id : frame_ids)
		{
			auto projection_errors = camera_calibration_fit.GetProjectionErrors(camera_system_transformation_,
			                                                                    relative_fix_cameras_parameter_optimizer_,
			                                                                    frame_id,
			                                                                    camera_id,
			                                                                    board_id,
			                                                                    calibration_boards_transformation_);

			for(const auto& projection_error : projection_errors)
			{
				error_sum += projection_error.x * projection_error.x + projection_error.y * projection_error.y;
				measurement_count++;
			}
		}
	}

	return sqrt(error_sum / double(measurement_count));
}

std::pair<double, double> CalibrationCalculator::GetChannelRMSTanRad(size_t camera_id)
{
	const CameraCalibrationFit camera_calibration_fit(relative_fix_cameras_parameter_optimizer_,
	                                                  camera_system_transformation_,
	                                                  calibration_boards_transformation_,
	                                                  observations_,
	                                                  false);

	std::set<size_t> frame_ids;

	for(auto observation : observations_)
	{
		frame_ids.insert(observation->GetFrameId());
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Get RMS of camera id [" << camera_id << "] from " << frame_ids.size() << "frames  ";

	auto error_sum_rad     = 0.0;
	auto error_sum_tan     = 0.0;
	auto measurement_count = 0;

	const auto board_ids = calibration_boards_transformation_->GetBoardIDs();

	for(const auto& board_id : board_ids)
	{
		for(auto frame_id : frame_ids)
		{
			auto projection_error = camera_calibration_fit.GetProjectionErrors(camera_system_transformation_,
			                                                                   relative_fix_cameras_parameter_optimizer_,
			                                                                   frame_id,
			                                                                   camera_id,
			                                                                   board_id,
			                                                                   calibration_boards_transformation_);

			// get 3D points in camera local coordinates
			std::vector<cv::Point3d> cam_pts_3d;
			CalibrationBoardData board_measurement;
			auto projected_points = camera_calibration_fit.ProjectBoardPoints(camera_system_transformation_,
			                                                                  relative_fix_cameras_parameter_optimizer_,
			                                                                  calibration_boards_transformation_,
			                                                                  frame_id,
			                                                                  camera_id,
			                                                                  board_id,
			                                                                  nullptr,
			                                                                  &board_measurement,
			                                                                  &cam_pts_3d);

			auto calibration = relative_fix_cameras_parameter_optimizer_->GetCalibration(camera_id);
			std::vector<cv::Point2d> straighten_points;

			if(projected_points.empty())
				continue;

			calibration->UndistortPoints2(projected_points, straighten_points, cam_pts_3d);

			for(auto i = 0; i < projection_error.size(); ++i)
			{
				// calc radial component
				cv::Vec2d error_vector(projection_error[i].x, projection_error[i].y);
				cv::Vec2d radial_direction(straighten_points[i].x, straighten_points[i].y);
				cv::Vec2d tangential_direction(radial_direction[1], -radial_direction[0]);

				auto angle_radial_direction = Helper::AngleTo(error_vector, radial_direction);
				error_sum_rad += fabs(cos(angle_radial_direction) * Helper::VectorLength(error_vector));

				auto angle_tangential_direction = Helper::AngleTo(error_vector, tangential_direction);
				error_sum_tan += fabs(cos(angle_tangential_direction) * Helper::VectorLength(error_vector));
				measurement_count++;
			}
		}
	}

	return std::pair<double, double>(sqrt(error_sum_tan / double(measurement_count)), sqrt(error_sum_rad / double(measurement_count)));
}

int CalibrationCalculator::GetChannelResult(CaptureCalibration& result_cal,
                                            const ImageChannel channel) const
{
	result_cal.SetCalibration(channel, relative_fix_cameras_parameter_optimizer_->GetCalibration(channel));
	return 0;
}

size_t CalibrationCalculator::GetObservationSize() const
{
	return observations_.size();
}

double CalibrationCalculator::InitSystem(Eigen::VectorXd& start_params) const
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initialization " << Constants::LOG_START;

	const CameraCalibrationFit camera_calibration_fit(relative_fix_cameras_parameter_optimizer_,
	                                                  camera_system_transformation_,
	                                                  calibration_boards_transformation_,
	                                                  observations_,
	                                                  false);


	Eigen::NumericalDiff<CameraCalibrationFit> num_differentiation(camera_calibration_fit, 1E-7);
	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<CameraCalibrationFit>, double> levenberg_marquardt(num_differentiation);

	start_params = num_differentiation.GetInitialParameters();
	CameraCalibrationFit::FromEigen(start_params,
	                                relative_fix_cameras_parameter_optimizer_,
	                                camera_system_transformation_,
	                                calibration_boards_transformation_);
	Eigen::VectorXd eigen_vector(num_differentiation.values());
	num_differentiation(start_params, eigen_vector);

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initialization " << Constants::LOG_END;
	return Get2dRMS(eigen_vector);
}

std::pair<size_t, double> CalibrationCalculator::OptimizeSystem(Eigen::VectorXd& start_params,
                                                                const std::string& description,
                                                                bool initialize_board_positions) const
{
	const CameraCalibrationFit camera_calibration_fit(relative_fix_cameras_parameter_optimizer_,
	                                                  camera_system_transformation_,
	                                                  calibration_boards_transformation_,
	                                                  observations_,
	                                                  false);

	Eigen::NumericalDiff<CameraCalibrationFit> num_diff(camera_calibration_fit, 1E-7);
	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<CameraCalibrationFit>, double> lm(num_diff);

	// get start values for optimization if not given
	// Eigen::VectorXd start = start_params;
	CameraCalibrationFit::FromEigen(start_params,
	                                relative_fix_cameras_parameter_optimizer_,
	                                camera_system_transformation_,
	                                calibration_boards_transformation_);

	Eigen::VectorXd residuals(num_diff.values());
	num_diff(start_params, residuals);
	const auto rms_before2d = Get2dRMS(residuals);
	lm.parameters.maxfev    = 20000;
	lm.parameters.xtol      = 1E-15;
	const int ret           = lm.minimize(start_params);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : LevenbergMarquardt Iterations " << lm.iter;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : LevenbergMarquardt Return value " << ret;

	num_diff(start_params, residuals);
	auto rms = Get2dRMS(residuals);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << description << " RMS 2d [" << rms << "] before [" << rms_before2d << "] at [" <<
 residuals.size() << "] Observations [" << residuals.size() / 2 << " marker ] ";

	// write back optimization results
	CameraCalibrationFit::FromEigen(start_params,
	                                relative_fix_cameras_parameter_optimizer_,
	                                camera_system_transformation_,
	                                calibration_boards_transformation_);

	// it is halved so the marker seen once which is based on the assumption that both
	// cameras see exactly the same markers (observations)
	return std::make_pair(residuals.size() / 2, rms);
}

std::pair<size_t, double> CalibrationCalculator::OptimizeProjectorSystem(Eigen::VectorXd& start_params,
                                                                         std::map<FrameID, std::vector<cv::Point3d>>& correspondences_3d_points_map,
                                                                         std::map<FrameID, std::vector<cv::Point2d>>&
                                                                         correspondences_calibrated_2d_points_map) const
{
	const ProjectorCalibrationFit projector_calibration_fit(relative_fix_cameras_parameter_optimizer_,
	                                                        correspondences_3d_points_map,
	                                                        correspondences_calibrated_2d_points_map);
	Eigen::NumericalDiff<ProjectorCalibrationFit> num_diff(projector_calibration_fit, 1E-7);
	Eigen::LevenbergMarquardt<Eigen::NumericalDiff<ProjectorCalibrationFit>, double> lm(num_diff);

	// get start values for optimization if not given
	Eigen::VectorXd start = start_params;
	ProjectorCalibrationFit::FromEigen(start_params, relative_fix_cameras_parameter_optimizer_);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : num_diff.values() " << num_diff.values();
	Eigen::VectorXd residuals(num_diff.values());
	num_diff(start_params, residuals);

	const auto rms_before2d = Get2dRMS(residuals);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : rms before minimization " << rms_before2d;

	lm.parameters.maxfev = 20000;
	lm.parameters.xtol   = 1E-15;
	const int ret        = lm.minimize(start_params);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : LM Iterations " << lm.iter;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : LM Return value " << ret;

	num_diff(start_params, residuals);
	auto rms = Get2dRMS(residuals);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : rms after minimization " << rms;
	ProjectorCalibrationFit::FromEigen(start_params, relative_fix_cameras_parameter_optimizer_);

	// it is halved so the marker seen once which is based on the assumption that both
	// cameras see exactly the same markers (observations)
	return std::make_pair(residuals.size() / 2, rms);
}

size_t CalibrationCalculator::DeactivateCodedMarker(std::vector<std::shared_ptr<Observation>>& observations,
                                                    const std::vector<std::shared_ptr<BoardDetector>>& boards)
{
	size_t deactivated_count = 0;
	std::map<size_t, size_t> board_uncoded_start_id_map;

	for(const auto& board : boards)
		board_uncoded_start_id_map[board->GetBoardId()] = board->GetUncodedMarkerStartId();

	for(auto& observation : observations)
	{
		const auto uncoded_start     = board_uncoded_start_id_map[observation->GetBoardId()];
		auto const current_marker_id = observation->GetMarkerId();

		if(Helper::InRange(current_marker_id, Constants::MARKER_ID_START, uncoded_start))
		{
			observation->SetDeactivated(false);
			deactivated_count++;
		}
	}

	return deactivated_count;
}

double CalibrationCalculator::Get1dRMS(const Eigen::VectorXd& vector) const
{
	return sqrt(vector.dot(vector) / vector.size());
}

double CalibrationCalculator::Get2dRMS(const Eigen::VectorXd& vector) const
{
	auto sum        = 0.0;
	const auto size = vector.size() - 1;

	for(auto i = 0; i < size; i += 2)
		sum += double(vector[i] * vector[i] + vector[i + 1] * vector[i + 1]);

	const auto mean_sq = sum / vector.size() / 2;

	return sqrt(mean_sq);
}

double CalibrationCalculator::GetXRMS(const Eigen::VectorXd& vector) const
{
	auto sum        = 0.0;
	const auto size = vector.size();

	for(auto i = 0; i < size; i += 2)
		sum += vector[i] * vector[i];

	const auto mean_sq = sum / (size / 2);

	return sqrt(mean_sq);
}

double CalibrationCalculator::GetYRMS(const Eigen::VectorXd& vector) const
{
	auto sum        = 0.0;
	const auto size = vector.size();

	for(auto i = 1; i < size; i += 2)
		sum += vector[i] * vector[i];

	const auto mean_sq = sum / (size / 2.0);

	return sqrt(mean_sq);
}
}
}
