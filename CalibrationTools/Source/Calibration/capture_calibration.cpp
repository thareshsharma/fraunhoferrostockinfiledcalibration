#include "capture_calibration.h"
#include "helper.h"
#include "calibration_xml_parser.h"
#include "calibration_json_reader.h"
#include "single_camera_calibration.h"
#include "camera_sensor_depth_dependent.h"
#include "io_messages.h"
#include <chrono>

namespace fs = boost::filesystem;

CaptureCalibration::CaptureCalibration():
	serial_number_("DMVS00000000000"),
	calibration_date_("no-date"),
	timestamp_(0.0)
{
}

CaptureCalibration::CaptureCalibration(const CaptureCalibration& rhs)
{
	serial_number_ = rhs.serial_number_;
	timestamp_     = rhs.timestamp_;

	// Real copy of all channels
	auto channels            = rhs.GetImageChannels();
	const auto channels_size = channels.size();

	for(size_t i = 0; i < channels_size; ++i)
	{
		const std::shared_ptr<const CameraSensor> camera_sensor = rhs.GetCalibration(channels[i])->Duplicate();

		if(!camera_sensor)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Inconsistent calibration, cannot access channel " << channels[i];
			continue;
		}

		SetCalibration(channels[i], camera_sensor);
	}

	// real copy of custom matrices
	for(auto it = rhs.custom_matrices_.begin(); it != rhs.custom_matrices_.end(); ++it)
	{
		custom_matrices_[it->first] = cv::Mat();
		it->second.copyTo(custom_matrices_[it->first]);
	}
}

CaptureCalibration::~CaptureCalibration() = default;

std::shared_ptr<const CameraSensor> CaptureCalibration::CreateFromCalibrationData(const CalibrationData& calibration_data,
                                                                                  const ImageChannel image_channel)
{
	if(calibration_data.is_depth_supported)
	{
		CameraInfo camera_info;

		if(image_channel == CHANNEL_GREY0)
			return std::make_shared<CameraSensorDepthDependent>(calibration_data.camera_left);

		if(image_channel == CHANNEL_GREY1)
			return std::make_shared<CameraSensorDepthDependent>(calibration_data.camera_right);

		// We should create a projector sensor model for a cleaner implementation
		if(image_channel == CHANNEL_PROJECTOR0)
			return std::make_shared<CameraSensorDepthDependent>(calibration_data.projector);

		throw std::exception("No relevant image channel found in calibration data");
	}

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;

	return std::make_shared<SingleCameraCalibration>();
}

std::string CaptureCalibration::GetSerialNumber() const
{
	return serial_number_;
}

CaptureCalibration& CaptureCalibration::operator=(const CaptureCalibration& rhs)
{
	if(this == &rhs)
		return *this;

	serial_number_ = rhs.serial_number_;

	// Real copy of all channels
	auto channels = rhs.GetImageChannels();

	for(size_t i = 0; i < channels.size(); ++i)
	{
		const std::shared_ptr<const CameraSensor> camera_sensor = rhs.GetCalibration(channels[i])->Duplicate();

		if(!camera_sensor)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Inconsistent calibration, cannot access channel " << channels[i];
			continue;
		}

		SetCalibration(channels[i], camera_sensor);
	}

	// real copy of custom matrices
	for(auto it = rhs.custom_matrices_.begin(); it != rhs.custom_matrices_.end(); ++it)
	{
		custom_matrices_[it->first] = cv::Mat();
		it->second.copyTo(custom_matrices_[it->first]);
	}

	return *this;
}

void CaptureCalibration::SetCalibration(const ImageChannel channel,
                                        const std::shared_ptr<const CameraSensor>& camera_calibration)
{
	cameras_sensors_map_[channel] = camera_calibration;
}

std::shared_ptr<const CameraSensor> CaptureCalibration::GetCalibration(const ImageChannel channel) const
{
	const auto it = cameras_sensors_map_.find(channel);

	if(it == cameras_sensors_map_.end())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Channel " << channel << " not available";
		return nullptr;
	}

	return it->second;
}

std::vector<ImageChannel> CaptureCalibration::GetImageChannels() const
{
	std::vector<ImageChannel> image_channel;

	for(auto it = cameras_sensors_map_.begin(); it != cameras_sensors_map_.end(); ++it)
	{
		image_channel.push_back(it->first);
	}

	return image_channel;
}

bool CaptureCalibration::Save(const std::string& filename,
                              const std::string& if_overwrite) const
{
	if(filename.empty())
		return true;

	// Create path to store calibrations
	fs::path calibration_directory(Helper::GetCamerasDirectory());

	// check if directory exists on file system, create if not
	if(!exists(calibration_directory))
	{
		if(!create_directory(calibration_directory))
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Cannot create calibration directory";
			return false;
		}
	}

	// Create full path for calibration file
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Try to save to " << calibration_directory.string();

	const auto calibration_file(calibration_directory.append(filename));

	// Check success
	if(!exists(calibration_file))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Couldn't save to " << filename;
		return false;
	}

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;

	return is_regular_file(calibration_file) /*&& SaveFile(calibration_file.filename().string(), nullptr)*/;
}

__time64_t CaptureCalibration::GetCalibrationTime(const fs::path& filepath) const
{
	if(!is_regular_file(filepath))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Calibration file " << filepath << " could not be found ";
		return 0;
	}

	/*char time_buffer[100];
	time_t raw_time = last_write_time(filepath);
	struct tm time_info {};
	time(&raw_time);
	localtime_s(&time_info, &raw_time);
	strftime(time_buffer, 100, "%Y%m%d-%H%M%S", &time_info);*/
	return last_write_time(filepath);
}

bool CaptureCalibration::Load(const std::string& calibration_filename)
{
	// Get the directory with the camera calibrations and
	// check if we can find a calibration for the given camera.
	fs::path calibration_filepath(Helper::GetCamerasDirectory());

	if(is_directory(calibration_filepath))
	{
		calibration_filepath.append(calibration_filename);

		if(is_regular_file(calibration_filepath))
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Trying to load calibration file " << calibration_filename << " from " <<
 calibration_filepath.parent_path();

			return LoadFile(calibration_filepath);
		}

		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : No calibration file " << calibration_filepath << " found";
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : No calibration directory " << calibration_filepath.parent_path() << " found";
	}

	return false;
}

bool CaptureCalibration::Exists(const std::string& filename)
{
	// Get the directory with the camera calibrations and check
	// if we can find a calibration for the given camera.
	const auto calibrations_path        = Helper::GetCalibrationsDirectory();
	fs::path calibration_file_full_path = calibrations_path / filename;
	return is_regular_file(calibration_file_full_path) && LoadFile(calibration_file_full_path);
}

bool CaptureCalibration::LoadForRecording(const std::string& filename)
{
	fs::path file(filename);
	auto camera = is_regular_file(file) ? file.replace_extension("xml") : "";

	return is_regular_file(camera) && LoadFile(camera);
}

static CaptureCalibration LoadYmlFileOldStyle(const char* filename)
{
	CaptureCalibration out;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Found calibration from " << filename;

	// Format of the file. YML written by OpenCV or binary written by us
	FILE* fp;
	auto err = fopen_s(&fp, filename, "r");

	if(err)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Cannot open calibration file " << filename;
		return out;
	}

	char format[10];
	fread(format, _countof(format) - 1, 1, fp);
	fclose(fp);

	format[_countof(format) - 1] = '\0';

	// Our format
	if(strcmp(format, "%YAML:1.0") == 0)
	{
		// Get full path of the camera and load the calibration
		cv::FileStorage fs(filename, cv::FileStorage::READ);
		if(!fs.isOpened())
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot open file";
			return out;
		}

		cv::Mat channels;
		fs["used_channels"] >> channels;

		for(auto i = 0; i < channels.cols; ++i)
		{
			auto channel = ImageChannel(channels.at<INT32>(0, i));

			char str_matrix[1024], str_distortion[1024], str_rotation[1024], str_translation[1024];
			sprintf_s(str_matrix, "channel_%d_camera_matrix", int(channel));
			sprintf_s(str_distortion, "channel_%d_distortion", int(channel));
			sprintf_s(str_rotation, "channel_%d_R", int(channel));
			sprintf_s(str_translation, "channel_%d_T", int(channel));

			cv::Mat camera_matrix, rotation, translation, distortion;

			fs[str_matrix] >> camera_matrix;
			fs[str_rotation] >> rotation;
			fs[str_translation] >> translation;
			fs[str_distortion] >> distortion;

			std::shared_ptr<CameraSensor> cal = std::make_shared<SingleCameraCalibration>(camera_matrix, distortion, rotation, translation);

			if(!distortion.empty())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Distortion " << str_distortion << "   " << distortion.at<double>(1, 0);
			}

			out.SetCalibration(channel, cal);
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Set calibration " << channel;
		}

		cv::Mat laser_beam;
		fs["laser_beams"] >> laser_beam;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Loaded laser beam with " << laser_beam.rows << " rows";
		out.SetCustomMatrix("laser_beams", laser_beam.clone());

		cv::Mat ir0_poses;
		fs["ir0Poses"] >> ir0_poses;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Loaded ir0 poses with " << ir0_poses.rows << " rows";
		out.SetCustomMatrix("ir0Poses", ir0_poses.clone());

		cv::Mat color_poses;
		fs["colorPoses"] >> color_poses;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Loaded color poses with " << color_poses.rows << " rows";
		out.SetCustomMatrix("colorPoses", color_poses.clone());

		cv::Mat ir1_poses;
		fs["ir1Poses"] >> ir1_poses;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Loaded ir1 poses with " << ir1_poses.rows << " rows";
		out.SetCustomMatrix("ir1Poses", ir1_poses.clone());

		cv::Mat pattern_board_pts;
		fs["patternBoardPts"] >> pattern_board_pts;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Loaded pattern board points with " << pattern_board_pts.rows << "rows";
		out.SetCustomMatrix("patternBoardPts", pattern_board_pts.clone());

		cv::Mat additional_board_pts;
		fs["additionalBoardPts"] >> additional_board_pts;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Loaded additional board points with" << additional_board_pts.rows << " rows";
		out.SetCustomMatrix("additionalBoardPts", additional_board_pts.clone());
	}

	return out;
}

bool CaptureCalibration::LoadFile(fs::path& full_file_path)
{
	if(full_file_path.empty())
		return false;

	if(!exists(full_file_path))
		return false;

	auto calibration_file(full_file_path);

	if(exists(calibration_file))
	{
		const auto if_read = Helper::Read(full_file_path) != IOMessages::IO_OK;

		if(if_read && FillFromContainer(full_file_path))
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Error reading calibration file " << full_file_path;
			return false;
		}

		return true;
	}

	calibration_file.replace_extension("yml");

	if(is_regular_file(calibration_file))
	{
		// Create a new xml file
		auto temp = LoadYmlFileOldStyle(calibration_file.string().c_str());

		// We should have an XML now
		if(!fs::exists(calibration_file))
			return false;

		// Try loading again
		return LoadFile(full_file_path);
	}

	return false;
}

bool CaptureCalibration::LoadCalibrationFromXmlFile(fs::path& full_file_path)
{
	if(full_file_path.empty())
		return false;

	if(!exists(full_file_path))
		return false;

	if(exists(full_file_path))
	{
		CalibrationData calibration_data;

		const CalibrationXMLParser calibration_xml_parser(full_file_path);

		calibration_xml_parser.GetCalibrationData(calibration_data);

		serial_number_ = calibration_data.serial_number;

		cameras_sensors_map_[calibration_data.camera_left.channel] = CreateFromCalibrationData(
			calibration_data,
			calibration_data.camera_left.channel);
		cameras_sensors_map_[calibration_data.camera_right.channel] = CreateFromCalibrationData(
			calibration_data,
			calibration_data.camera_right.channel);
		cameras_sensors_map_[calibration_data.projector.channel] = CreateFromCalibrationData(calibration_data, calibration_data.projector.channel);

		return true;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Error reading calibration file " << full_file_path;
	return false;
}



bool CaptureCalibration::LoadCalibrationFromJsonFile(fs::path& full_file_path)
{
	if(full_file_path.empty())
		return false;

	if(!exists(full_file_path))
		return false;

	if(exists(full_file_path))
	{
		CalibrationData calibration_data;
		CalibrationJSONReader::ReadCameraCalibration(full_file_path.string(), calibration_data);
		serial_number_ = calibration_data.serial_number;

		cameras_sensors_map_[calibration_data.camera_left.channel] = CreateFromCalibrationData(
			calibration_data,
			calibration_data.camera_left.channel);
		cameras_sensors_map_[calibration_data.camera_right.channel] = CreateFromCalibrationData(
			calibration_data,
			calibration_data.camera_right.channel);
		cameras_sensors_map_[calibration_data.projector.channel] = CreateFromCalibrationData(calibration_data, calibration_data.projector.channel);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration JSON file [" << full_file_path << "] has been loaded";
		return true;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Error reading calibration file " << full_file_path;
	return false;
}

bool CaptureCalibration::FillFromContainer(const fs::path& full_file_path)
{
	const CalibrationXMLParser file_parser(full_file_path);
	serial_number_ = file_parser.GetSerialNumber();
	timestamp_     = file_parser.GetCalibrationTimestamp();

	return true;
}

void CaptureCalibration::SetCustomMatrix(const char* name,
                                         const cv::Mat& custom_mat)
{
	custom_matrices_[name] = cv::Mat();
	custom_mat.copyTo(custom_matrices_[name]);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Set customMatrix with " << custom_matrices_[name].rows << " rows";
}

const cv::Mat& CaptureCalibration::GetCustomMatrix(const char* name) const
{
	if(custom_matrices_.find(name) == custom_matrices_.end())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot find custom matrix " << name;
		return Constants::EMPTY_MATRIX;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Get custom matrix " << custom_matrices_.find(name)->second.rows << " rows";
	return custom_matrices_.find(name)->second;
}

const cv::Mat& CaptureCalibration::GetPatternDefinition() const
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Get custom matrix rows";
	return cv::Mat();
}

bool CaptureCalibration::ToBinary(BinaryBuffer& binary)
{
	binary.clear();

	// insert binary timestamp
	time_t now = time(nullptr);
	binary.insert(binary.end(), (char*)&now, (char*)&now + sizeof(now));

	// All camera channels
	// insert number of channels following
	auto cam_count = int(cameras_sensors_map_.size());
	binary.insert(binary.end(), (char*)&cam_count, (char*)&cam_count + sizeof(cam_count));

	// insert channel ID and channel binary data for each channel
	for(auto iterator = cameras_sensors_map_.begin(); iterator != cameras_sensors_map_.end(); ++iterator)
	{
		// insert channel ID
		auto channel_id = iterator->first;
		binary.insert(binary.end(), (char*)&channel_id, (char*)&channel_id + sizeof(channel_id));

		// insert channel binary data managed by the camera channel itself
		iterator->second->ToBinary(binary);
	}

	// Custom matrix with laser beams (stored binary as 32bit float, compressed)
	cv::Mat beams;
	GetCustomMatrix("laser_beams").convertTo(beams, CV_32F);

	// insert number of rows and number of columns of laser beam matrix
	binary.insert(binary.end(), (char*)&beams.rows, (char*)&beams.rows + sizeof(beams.rows));
	binary.insert(binary.end(), (char*)&beams.cols, (char*)&beams.cols + sizeof(beams.cols));

	// insert compressed laser beam matrix data
	if(beams.rows > 0 && beams.cols > 0)
	{
		// TODO
		/*BinaryBuffer cmpr;
		cmpr.resize(SFMCompress::compressBound((uint32_t)(beams.total() * beams.elemSize())));

		uint32_t len = (uint32_t)cmpr.size();
		if(SFMCompress::compress(&cmpr[0], &len, beams.data, (uint32_t)(beams.total() * beams.elemSize())) != Z_OK)
			return false;

		binary.insert(binary.end(), (char*)&len, (char*)&len + sizeof(len));
		binary.insert(binary.end(), &cmpr[0], &cmpr[0] + len);*/
	}

	return true;
}

bool CaptureCalibration::FromBinary(BinaryBuffer& binary)
{
	cameras_sensors_map_.clear();
	custom_matrices_.clear();

	// get iterator with current buffer position
	BinaryBuffer::iterator pos = binary.begin();

	// get calibration time from time_t
	time_t now = *((time_t*)&(*pos));
	std::advance(pos, sizeof(now));

	// get number of channels following
	auto cam_count = *((int*)&(*pos));
	std::advance(pos, sizeof(cam_count));

	for(auto i = 0; i < cam_count; ++i)
	{
		// get the channel ID ( VgCaptureDevice::Channel enum e.g. CHANNEL_BGR0  (10) )
		auto channel_id = ImageChannel(*((int*)&(*pos)));
		std::advance(pos, sizeof(channel_id));

		// creation of DMVS::CameraCalibration::CameraSensor2 managed by the DMVS::CameraCalibration::CameraSensor2 itself
		// TODO
		//cameras_[channelID] = DMVS::CameraCalibration::CameraSensor2::CreateFromBinary(pos);

		if(!cameras_sensors_map_[channel_id])
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not construct camera " << channel_id << " from binary data";
			return false;
		}
	}

	// get number of rows and cols of laser beam matrix
	auto rows = *((int*)&(*pos));
	std::advance(pos, sizeof(rows));
	auto cols = *((int*)&(*pos));
	std::advance(pos, sizeof(cols));

	// uncompress laser beam matrix data and fill beams matrix
	// binary stored 32bit float compressed, working data is double
	if(rows > 0 && cols > 0)
	{
		// TODO
		/*cv::Mat beams(rows, cols, CV_32FC1);

		uLongf len = *(uLongf*)&(*pos);
		std::advance(pos, sizeof(len));

		uint32_t matlen = (uint32_t)(beams.total() * beams.elemSize());
		if(SFMCompress::uncompress(beams.data, &matlen, &(*pos), len))
			return false;

		std::advance(pos, len);

		cv::Mat beamsD;
		beams.convertTo(beamsD, CV_64FC1);
		setCustomMatrix("laser_beams", beamsD);*/
	}

	return true;
}

void CaptureCalibration::MoveToCoordinateSystem(const cv::Mat& rotation_matrix,
                                                const cv::Mat& translation_vector)
{
	const Eigen::Vector3d eigen_translational(translation_vector.at<double>(0, 0),
	                                          translation_vector.at<double>(1, 0),
	                                          translation_vector.at<double>(2, 0));
	Eigen::Matrix3d eigen_rotational;
	cv2eigen(rotation_matrix, eigen_rotational);

	CameraSensor2Map new_cameras;

	for(auto cam_it = cameras_sensors_map_.begin(); cam_it != cameras_sensors_map_.end(); ++cam_it)
	{
		Eigen::Vector3d channel_translational;
		Eigen::Matrix3d channel_rotational;
		cv::Mat cam_rotational, cam_translational;
		cam_it->second->GetExtrinsicParameters(cam_rotational, cam_translational);
		cv2eigen(cam_rotational, channel_rotational);
		cv2eigen(cam_translational, channel_translational);

		Eigen::Matrix3d channel_rotational_new    = eigen_rotational.inverse() * channel_rotational;
		Eigen::Vector3d channel_translational_new = eigen_rotational.inverse() * (channel_translational - eigen_translational);

		eigen2cv(channel_rotational_new, cam_rotational);
		eigen2cv(channel_translational_new, cam_translational);
		auto new_camera = cam_it->second->Duplicate();
		new_camera->SetExtrinsicParameters(cam_rotational, cam_translational);
		new_cameras[cam_it->first] = new_camera;
	}

	cameras_sensors_map_ = new_cameras;
}

void CaptureCalibration::MoveToCoordinateSystemAdjusted(const cv::Mat& rotation,
                                                        const cv::Mat& translation)
{
	CameraSensor2Map new_cameras;
	const Eigen::Vector3d eigen_translational(translation.at<double>(0, 0), translation.at<double>(1, 0), translation.at<double>(2, 0));
	Eigen::Matrix3d eigen_rotational;
	cv2eigen(rotation, eigen_rotational);

	for(auto iterator = cameras_sensors_map_.begin(); iterator != cameras_sensors_map_.end(); ++iterator)
	{
		////Eigen::Vector3d eigen_translation;
		////Eigen::Matrix3d eigen_rotational;
		//cv::Mat rotation_mat, translation_mat;

		//iterator->second->GetExtrinsicParameters(rotation_mat, translation_mat);
		///*cv2eigen(rotation_mat, eigen_rotational);
		//cv2eigen(translation_mat, eigen_translation);

		//Eigen::Matrix3d eigen_rotational_new    = eigen_rotational * eigen_rotational.transpose();
		//Eigen::Vector3d eigen_translational_new = eigen_translation - eigen_rotational * eigen_translational;*/

		//auto rotational_new = rotation.t() * rotation_mat;
		//auto translational_new = rotation.inv() * (translation_mat - translation);

		///*eigen2cv(eigen_rotational_new, rotation_mat);
		//eigen2cv(eigen_translational_new, translation_mat);*/

		//auto new_camera = iterator->first == CHANNEL_PROJECTOR0 ? iterator->second->DuplicateLaser() : iterator->second->Duplicate();
		//new_camera->SetExtrinsicParameters(rotational_new, translational_new);

		//new_cameras[iterator->first] = new_camera;

		Eigen::Vector3d channel_translational;
		Eigen::Matrix3d channel_rotational;
		cv::Mat cam_rotational, cam_translational;
		iterator->second->GetExtrinsicParameters(cam_rotational, cam_translational);
		cv2eigen(cam_rotational, channel_rotational);
		cv2eigen(cam_translational, channel_translational);

		Eigen::Matrix3d channel_rotational_new = eigen_rotational.inverse() * channel_rotational;
		Eigen::Vector3d channel_translational_new = eigen_rotational.inverse() * (channel_translational - eigen_translational);

		eigen2cv(channel_rotational_new, cam_rotational);
		eigen2cv(channel_translational_new, cam_translational);
		auto new_camera = iterator->second->Duplicate();
		new_camera->SetExtrinsicParameters(cam_rotational, cam_translational);
		new_cameras[iterator->first] = new_camera;
	}

	cameras_sensors_map_ = new_cameras;
}

//const CaptureCalibration::CustomMatrices& CaptureCalibration::GetCustomMatrices() const
//{
//	throw std::exception("CaptureCalibration::GetCustomMatrices() Not implemented");
//}
//
//CaptureCalibration::CameraSensor2Map CaptureCalibration::GetCameraSensorMap() const
//{
//	throw std::exception("CaptureCalibration::GetCameraSensor2Map() Not implemented");
//}
