#include "calibration_cache_impl.h"
#include "calibration_cache.h"

CalibrationCacheImpl::CalibrationCacheImpl() :
	filename_(""),
	writer_(nullptr),
	max_items_(DEFAULT_MAX_ITEMS),
	file_(INVALID_HANDLE_VALUE)
{
}

IOMessages::IOResult CalibrationCacheImpl::Attach(const std::string& path,
                                                  const int mode,
                                                  const uint64_t magic)
{
	if(mode & CalibrationCache::CACHE_ASYNC)
	{
		writer_ = new CalibrationCacheManager(this);
		writer_->InitThread();
	}

	return Open(path, mode & 0x0F, magic);
}

std::string CalibrationCacheImpl::Filename() const
{
	return filename_;
}

IOMessages::IOResult CalibrationCacheImpl::TableOfContents(std::vector<CalibrationCacheItem>& items)
{
	items.clear();

	CriticalSection::Access lock(items_lock_, CriticalSection::read);

	for(CalibrationCacheEntry i : items_)
		items.push_back(i.second);

	return IOMessages::IO_OK;
}

CalibrationCacheImpl::~CalibrationCacheImpl()
{
	Close();
}

void CalibrationCacheImpl::MaximumItems(const size_t max_size)
{
	max_items_ = std::max(DEFAULT_MIN_ITEMS, max_size);
}

IOMessages::IOResult CalibrationCacheImpl::Get(CalibrationCacheItem& item)
{
	return item.Read(file_, false);
}

IOMessages::IOResult CalibrationCacheImpl::Get(const int record_number,
                                               const std::string& name,
                                               std::string& string)
{
	const auto iterator = Get(record_number, name, CalibrationCacheItem::STRING);

	if(iterator == items_.end())
		return IOMessages::IO_FALSE;

	string = iterator->second.string;

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheImpl::Get(const int record_number,
                                               const std::string& name,
                                               std::vector<unsigned char>& bytes)
{
	// Raw byte buffers stored in an OpenCV matrix. Use the OpenCV conversion between
	// vector and matrix to get the right data types.
	const auto iterator = Get(record_number, name, CalibrationCacheItem::RAW_BYTES);

	if(iterator == items_.end())
		return IOMessages::IO_FALSE;

	bytes.assign(iterator->second.image_mat.datastart, iterator->second.image_mat.dataend);

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheImpl::Get(const int record_number,
                                               const std::string& name,
                                               cv::Mat& mat,
                                               const uint32_t matrix_type)
{
	const auto iterator = Get(record_number, name, matrix_type);

	if(iterator == items_.end())
		return IOMessages::IO_FALSE;

	mat = iterator->second.image_mat;

	return IOMessages::IO_OK;
}

CalibrationCacheImpl::CalibrationCacheItems::const_iterator CalibrationCacheImpl::Get(const int record_number,
                                                                                      const std::string& name,
                                                                                      const uint32_t flags)
{
	CalibrationCacheItem::Key key = 0;

	CriticalSection::Access lock(items_lock_, CriticalSection::read);

	// AK: moved inside lock scope: there is no guarantee end() is const always
	//auto iterator = items_.end();

	// Find item in cache
	key      = CalibrationCacheItem::KeyValue(record_number, name);
	auto iterator = items_.find(key);

	if(iterator == items_.end())
		return items_.end();

	// Read from file
	if(iterator->second.Read(file_, false) != IOMessages::IO_OK)
		return items_.end();

	// Check if we have valid data
	if((iterator->second.flags & CalibrationCacheItem::NO_DATA_LOADED) == CalibrationCacheItem::NO_DATA_LOADED)
		return items_.end();

	// Check if we have the correct data type
	if((iterator->second.flags & CalibrationCacheItem::DATA_TYPES) != (flags & CalibrationCacheItem::DATA_TYPES))
		return items_.end();

	// Cache management
	Manage(key);

	return iterator;
}

IOMessages::IOResult CalibrationCacheImpl::Put(const int record_number,
                                               const std::string& name,
                                               const std::string& string)
{
	const CalibrationCacheItem item(record_number, name, string);

	return Put(record_number, name, item);
}

IOMessages::IOResult CalibrationCacheImpl::Put(const int record_number,
                                               const std::string& name,
                                               const std::vector<unsigned char>& bytes)
{
	const CalibrationCacheItem item(record_number, name, bytes);

	return Put(record_number, name, item);
}

IOMessages::IOResult CalibrationCacheImpl::Put(const int record_number,
                                               const std::string& name,
                                               const std::shared_ptr<ConfigurationContainer>& container)
{
	const CalibrationCacheItem item(record_number, name, container);

	return Put(record_number, name, item);
}

IOMessages::IOResult CalibrationCacheImpl::Get(const int record_number,
                                               const std::string& name,
                                               std::shared_ptr<ConfigurationContainer>& container)
{
	const auto iterator = Get(record_number, name, CalibrationCacheItem::CONFIG_CONTAINER);

	if(iterator == items_.end())
		return IOMessages::IO_FALSE;

	container = iterator->second.configuration_container;

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheImpl::Put(const int record_number,
                                               const std::string& name,
                                               const cv::Mat& mat,
                                               const uint32_t matrix_type)
{
	const CalibrationCacheItem item(record_number, name, mat.clone(), matrix_type);

	return Put(record_number, name, item);
}

IOMessages::IOResult CalibrationCacheImpl::Put(const CalibrationCacheItem& item)
{
	return Put(item.record_number, item.name, item);
}

IOMessages::IOResult CalibrationCacheImpl::Put(const int record_number,
                                               const std::string& name,
                                               const CalibrationCacheItem& item)
{
	CalibrationCacheItem::Key key = 0;
	CriticalSection::Access lock(items_lock_, CriticalSection::write);

	// AK: moved inside lock scope: there is no guarantee end() is const always
	auto i = items_.end();

	// TODO_RD: rework using map::insert method -> don't search 2 times.
	// Check if the item is in the cache already
	key = CalibrationCacheItem::KeyValue(record_number, name);
	i   = items_.find(key);

	if(i != items_.end())
		return IOMessages::IO_FALSE;

	// Add item to cache
	items_.emplace(key, item);

	return Manage(key);
}

IOMessages::IOResult CalibrationCacheImpl::Manage(const CalibrationCacheItem::Key key)
{
	// Sanity checks
	if(key == 0)
		return IOMessages::IO_OK;

	// Increase age of this item
	RejuvenateItem(key);

	// Remove old items from the cache
	if(writer_)
		writer_->Activate(false);
	else
		RemoveOldItems();

	return IOMessages::IO_OK;
}

void CalibrationCacheImpl::RemoveOldItems()
{
	// If the cache is full, release the memory of the oldest item. Save the item if necessary
	while(ages_.size() > max_items_)
	{
		CriticalSection::Access lock_a(ages_lock_, CriticalSection::write);
		if(!ages_.empty())
		{
			auto oldest = ages_.front();

			// Save and release
			{
				CriticalSection::Access lock_i(items_lock_, CriticalSection::write);

				items_[oldest].Write(file_);
				items_[oldest].Release();
			}

			ages_.erase(ages_.begin());
		}
	}
}

void CalibrationCacheImpl::RejuvenateItem(const CalibrationCacheItem::Key key)
{
	CriticalSection::Access lock(ages_lock_, CriticalSection::write);

	// Check if the item in in the cache list already
	const auto iterator = std::find(ages_.begin(), ages_.end(), key);

	if(iterator != ages_.end())
		ages_.erase(iterator);

	ages_.push_back(key);
}

uint64_t CalibrationCacheImpl::Magic(const std::string& path)
{
	HANDLE io              = INVALID_HANDLE_VALUE;
	uint64_t magic_in_file = -1;

	if(CalibrationCacheItem::Open(path, CalibrationCache::CACHE_READ, io) != IOMessages::IO_OK)
		return magic_in_file;

	CalibrationCacheItem::Read(io, &magic_in_file, sizeof(magic_in_file), 0);

	CalibrationCacheItem::Close(io);

	return magic_in_file;
}

IOMessages::IOResult CalibrationCacheImpl::Open(const std::string& path,
                                                const int mode,
                                                uint64_t magic)
{
	// Open file
	if(CalibrationCacheItem::Open(path, mode, file_) != IOMessages::IO_OK)
		return IOMessages::IO_FALSE;

	// Write magic
	if((mode & CalibrationCache::CACHE_WRITE) == CalibrationCache::CACHE_WRITE)
	{
		uint64_t pos;
		if(CalibrationCacheItem::Write(file_, &magic, sizeof(magic), pos) != IOMessages::IO_OK)
			return IOMessages::IO_FALSE;
	}
	else // Check magic - ignore the sensor code. Just look for FARO.
	{
		uint64_t magic_in_file;
		if(CalibrationCacheItem::Read(file_, &magic_in_file, sizeof(magic_in_file), 0) != IOMessages::IO_OK)
			return IOMessages::IO_FALSE;

		if((magic_in_file & CalibrationCache::CALIBRATION_CACHE_MAGIC_MASK) != (magic & CalibrationCache::CALIBRATION_CACHE_MAGIC_MASK))
			return IOMessages::IO_FALSE;
	}

	// Create TOC
	//	Read the item header from file,
	//	add it the item cache and
	//	increment the file position for the next round
	CalibrationCacheItem item;
	while(item.Read(file_, true) == IOMessages::IO_OK)
	{
		Put(item.record_number, item.name, item);

		item.stored_position += item.stored_size;
		item.flags       = CalibrationCacheItem::NO_DATA_LOADED;
		item.stored_size = 0x0;
	}

	filename_ = path;

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCacheImpl::Close()
{
	// Stop threading
	if(writer_)
	{
		writer_->TerminateThread();

		// Wait a little bit so that the thread can terminate
		delete writer_;
		writer_ = nullptr;
	}

	// Store all items
	//	Set the number of cached items to 0 and
	//	call the cache manager.
	max_items_ = 0;
	RemoveOldItems();

	auto result = IOMessages::IO_OK;

	{
		CriticalSection::Access lock(items_lock_, CriticalSection::write);
		result = CalibrationCacheItem::Close(file_);
	}

	return result;
}
