#include "calibration_cache_manager.h"
#include "calibration_cache_impl.h"

CalibrationCacheManager::CalibrationCacheManager(CalibrationCacheImpl* cache) :
	calibration_cache_impl_(cache)
{
	thread_name_ = "CalibrationCacheManager";
}

CalibrationCacheManager::~CalibrationCacheManager() = default;

bool CalibrationCacheManager::RunThreadProc()
{
	if(calibration_cache_impl_)
		calibration_cache_impl_->RemoveOldItems();

	return false;
}