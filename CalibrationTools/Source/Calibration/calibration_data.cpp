#include "calibration_data.h"

namespace DMVS
{
namespace CameraCalibration
{
CalibrationData::CalibrationData():
	hardware_version("0.0.0.0"),
	calibration_date("Fri Jul 00 00:00 : 00 0000"),
	serial_number("device_serial_number"),
	firmware_version("0.0.0.0"),
	calibration_software_version("0.0.0.0"),
	is_depth_supported(true),
	laser_temperature_begin(1000.00),
	laser_temperature_end(1000.00),
	camera1_temperature_begin(1000.00),
	camera1_temperature_end(1000.00),
	camera2_temperature_begin(1000.00),
	camera2_temperature_end(1000.00),
	back_plate_sensor1_temperature_begin(1000.00),
	back_plate_sensor1_temperature_end(1000.00),
	back_plate_sensor2_temperature_begin(1000.00),
	back_plate_sensor2_temperature_end(1000.00),
	back_plate_sensor3_temperature_begin(1000.00),
	back_plate_sensor3_temperature_end(1000.00),
	pressure_begin(1000.00),
	pressure_end(1000.00),
	humidity_begin(1000.00),
	humidity_end(1000.00),
	final_rms(100.00),
	detected_points(0)
{
	camera_left.name     = "left";
	camera_left.channel  = CHANNEL_GREY0;
	camera_right.name    = "right";
	camera_right.channel = CHANNEL_GREY1;
	projector.name       = "projector";
	projector.channel    = CHANNEL_PROJECTOR0;
}
}
}
