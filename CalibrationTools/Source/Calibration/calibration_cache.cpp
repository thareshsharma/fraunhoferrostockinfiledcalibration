#include "calibration_cache.h"
#include "calibration_cache_impl.h"
#include "helper.h"
#include "constants.h"
#include "device_description.h"

using namespace DMVS::CameraCalibration;

CalibrationCache::CalibrationCache()
	: mode_(CACHE_CLOSED)
{
	cache_ = new CalibrationCacheImpl;
}

CalibrationCache::CalibrationCache(const std::shared_ptr<ConfigurationContainer>& configuration_container,
                                   const std::string& serial_number,
                                   const std::string& file_name)
{
	cache_ = new CalibrationCacheImpl;
	MaximumItems(10000);
	Attach(file_name, CACHE_WRITE | CACHE_ASYNC, MagicCode(DMVS_CAPTURE));

	if(Put(0, Constants::DMVS_SERIAL_NO, serial_number) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Serial Number could not be saved in calibration cache";
	}

	if(Put(0, Constants::DMVS_WIDTH, std::to_string(DMVS::CameraCalibration::Constants::DMVS_IMAGE_HEIGHT)) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Image Width could not be saved in calibration cache";
	}

	if(Put(0, Constants::DMVS_HEIGHT, std::to_string(Constants::DMVS_IMAGE_WIDTH)) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Image Height could not be saved in calibration cache";
	}

	if(Put(0, Constants::DMVS_OPTIONS, configuration_container) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Configuration options could not be saved in calibration cache";
	}
}

CalibrationCache::~CalibrationCache()
{
	mode_ = CACHE_CLOSED;

	if(cache_)
	{
		delete cache_;
		cache_ = nullptr;
	}
}

uint64_t CalibrationCache::MagicCode(const size_t code)
{
	// The capture device class uses an offset of 10000
	// to generate device IDs not colliding with OpenCV.
	// "Old" SFM files have magic codes without this offset.
	return code > 10000 ? CALIBRATION_CACHE_MAGIC + ((code % 10000 & CALIBRATION_CACHE_MAGIC_MASK) << 32) : 0;
}

int CalibrationCache::CodeMagic(const uint64_t magic)
{
	// Note: Works for VG_xxx_CAP constants only.
	return ((magic - uint64_t(CALIBRATION_CACHE_MAGIC)) >> 32 & CALIBRATION_CACHE_MAGIC_MASK) + 10000;
}

uint64_t CalibrationCache::Magic(const std::string& path)
{
	return CalibrationCacheImpl::Magic(path);
}

IOMessages::IOResult CalibrationCache::Attach(const std::string& path,
                                              const size_t mode,
                                              const uint64_t magic)
{
	mode_ = mode;
	return cache_ ? cache_->Attach(path, mode, magic) : IOMessages::IO_FALSE;
}

std::string CalibrationCache::Filename() const
{
	return cache_ ? cache_->Filename() : "";
}

unsigned int CalibrationCache::Mode() const
{
	return mode_;
}

IOMessages::IOResult CalibrationCache::TableOfContents(std::vector<CalibrationCacheItem>& items) const
{
	return cache_ ? cache_->TableOfContents(items) : IOMessages::IO_FALSE;
}

IOMessages::IOResult CalibrationCache::Close()
{
	mode_ = CACHE_CLOSED;

	return cache_ ? cache_->Close() : IOMessages::IO_FALSE;
}

void CalibrationCache::MaximumItems(const size_t max_size) const
{
	if(cache_)
		cache_->MaximumItems(max_size);
}

IOMessages::IOResult CalibrationCache::Put(const CalibrationCacheItem& item) const
{
	return cache_ ? cache_->Put(item) : IOMessages::IO_FALSE;
}

IOMessages::IOResult CalibrationCache::Get(CalibrationCacheItem& item) const
{
	return cache_ ? cache_->Get(item) : IOMessages::IO_FALSE;
}

IOMessages::IOResult CalibrationCache::Put(const size_t record_number,
                                           const std::string& name,
                                           const std::string& string) const
{
	return cache_ ? cache_->Put(record_number, name, string) : IOMessages::IO_FALSE;
}

IOMessages::IOResult CalibrationCache::Get(const size_t record_number,
                                           const std::string& name,
                                           std::string& string) const
{
	return cache_ ? cache_->Get(record_number, name, string) : IOMessages::IO_FALSE;
}

IOMessages::IOResult CalibrationCache::Put(const size_t record_number,
                                           const std::string& name,
                                           const std::vector<unsigned char>& bytes) const
{
	return cache_ ? cache_->Put(record_number, name, bytes) : IOMessages::IO_FALSE;
}

IOMessages::IOResult CalibrationCache::Get(const size_t record_number,
                                           const std::string& name,
                                           std::vector<unsigned char>& bytes) const
{
	return cache_ ? cache_->Get(record_number, name, bytes) : IOMessages::IO_FALSE;
}

//IOMessages::IOResult CalibrationCache::Put(const size_t record_number,
//                                           const std::string& name,
//                                           const std::shared_ptr<AttributeContainer>& container) const
//{
//	return cache_ ? cache_->Put(record_number, name, container) : IOMessages::IO_FALSE;
//}

IOMessages::IOResult CalibrationCache::Put(const size_t record_number,
                                           const std::string& name,
                                           const std::shared_ptr<ConfigurationContainer>& container) const
{
	return cache_ ? cache_->Put(record_number, name, container) : IOMessages::IO_FALSE;
}

//IOMessages::IOResult CalibrationCache::Get(const size_t record_number,
//                                           const std::string& name,
//                                           std::shared_ptr<AttributeContainer>& container) const
//{
//	return cache_ ? cache_->Get(record_number, name, container) : IOMessages::IO_FALSE;
//}

IOMessages::IOResult CalibrationCache::Get(const size_t record_number,
                                           const std::string& name,
                                           std::shared_ptr<ConfigurationContainer>& container) const
{
	return cache_ ? cache_->Get(record_number, name, container) : IOMessages::IO_FALSE;
}

IOMessages::IOResult CalibrationCache::Put(const size_t record_number,
                                           const std::string& name,
                                           const cv::Mat& mat,
                                           const uint32_t matrix_type) const
{
	return cache_ ? cache_->Put(record_number, name, mat, matrix_type) : IOMessages::IO_FALSE;
}

IOMessages::IOResult CalibrationCache::Get(const size_t record_number,
                                           const std::string& name,
                                           cv::Mat& mat,
                                           const uint32_t matrix_type) const
{
	return cache_ ? cache_->Get(record_number, name, mat, matrix_type) : IOMessages::IO_FALSE;
}

IOMessages::IOResult CalibrationCache::ToJPEG(const cv::Mat& mat,
                                              std::vector<unsigned char>& bytes,
                                              const size_t start_byte)
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	//if(mat.empty()) RETURN_ERROR(IOMessages::IO_FALSE);

	//// Data header
	//int rows     = mat.rows;
	//int cols     = mat.cols;
	//int channels = mat.channels();
	//int type     = mat.type();

	//int quality = 95;
	//int subsamp = channels == 3 ? TJSAMP_420 : TJSAMP_GRAY;
	//int format  = channels == 3 ? TJPF_BGR : TJPF_GRAY;

	//tjhandle tj = tjInitCompress();
	//if(!tj)
	//{
	//	OutputDebugStringA(tjGetErrorStr());
	//	RETURN_ERROR(IOMessages::IO_FALSE);
	//}

	//unsigned long destLen = tjBufSize(cols, rows, subsamp);
	//bytes.resize(startByte + destLen);

	//int pitch           = (int)mat.step[0];
	//unsigned char* dest = (unsigned char*)&bytes[startByte];


	//if(tjCompress2(tj,
	//               (unsigned char*)mat.ptr(),
	//               cols,
	//               pitch,
	//               rows,
	//               format,
	//               &dest,
	//               &destLen,
	//               subsamp,
	//               quality,
	//               TJFLAG_NOREALLOC) != 0)
	//{
	//	OutputDebugStringA(tjGetErrorStr());
	//	RETURN_ERROR(IOMessages::IO_FALSE);
	//}

	//if(tjDestroy(tj) != 0)
	//{
	//	OutputDebugStringA(tjGetErrorStr());
	//	RETURN_ERROR(IOMessages::IO_FALSE);
	//}

	//bytes.resize(startByte + destLen);

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationCache::FromJPEG(const std::vector<unsigned char>& bytes,
                                                cv::Mat& mat,
                                                const size_t start_byte)
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	// Sanity check
	/*if(bytes.empty()) RETURN_ERROR(IOMessages::IO_FALSE);
	if(bytes.size() < start_byte) RETURN_ERROR(IOMessages::IO_FALSE);*/

	/*if(!SFMFile::jpegToCVMat(mat, (unsigned long)(bytes.size() - startByte), &bytes[startByte], SFMFile::AUTO))
	RETURN_ERROR(IOMessages::IO_FALSE);*/

	return IOMessages::IO_OK;
}
