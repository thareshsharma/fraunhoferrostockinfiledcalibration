#include "observation.h"
#include "helper.h"

namespace DMVS
{
namespace CameraCalibration
{
Observation::Observation()
{
	Init();
}

Observation::Observation(const std::shared_ptr<CircularMarker> marker)
{
	raw_ellipse_center_                = marker->raw_ellipse.center;
	raw_ellipse_angle_                 = marker->raw_ellipse.angle;
	raw_ellipse_height_                = marker->raw_ellipse.size.height;
	raw_ellipse_width_                 = marker->raw_ellipse.size.width;
	marker_id_                         = marker->id;
	raw_distance_                      = marker->raw_distance;
	raw_ellipse_radius_                = marker->radius;
	raw_ellipse_distortion_correction_ = marker->center_correction;
	weight_                            = marker->weight;
	image_channel_                     = 0;
	board_id_                          = 0;
	frame_id_                          = 0;
	scanner_position_id_               = 0;
	camera_coordinates3d_              = cv::Vec3d(0.0, 0.0, 0.0);
	encoder_angles_                    = cv::Point2f(0.0, 0.0);
	scan_half_                         = false;
	is_deactivated_                    = false;
	is_processed_                      = false;
}

size_t Observation::GetMarkerId() const
{
	return marker_id_;
}

void Observation::SetMarkerId(const size_t marker_id)
{
	marker_id_ = marker_id;
}

size_t Observation::GetBoardId() const
{
	return board_id_;
}

void Observation::SetBoardId(const size_t board_id)
{
	board_id_ = board_id;
}

size_t Observation::GetImageChannel() const
{
	return image_channel_;
}

void Observation::SetImageChannel(const size_t channel)
{
	image_channel_ = channel;
}

size_t Observation::GetFrameId() const
{
	return frame_id_;
}

void Observation::SetFrameId(const size_t frame_id)
{
	frame_id_ = frame_id;
}

cv::Point2f Observation::GetRawEllipseCenter() const
{
	return raw_ellipse_center_;
}

void Observation::SetRawEllipseCenter(const cv::Point2f& ellipse_center)
{
	raw_ellipse_center_ = ellipse_center;
}

double Observation::GetRawEllipseWidth() const
{
	return raw_ellipse_width_;
}

void Observation::SetRawEllipseWidth(const double& ellipse_width)
{
	raw_ellipse_width_ = ellipse_width;
}

double Observation::GetRawEllipseHeight() const
{
	return raw_ellipse_height_;
}

void Observation::SetRawEllipseHeight(const double& ellipse_height)
{
	raw_ellipse_height_ = ellipse_height;
}

double Observation::GetRawEllipseAngle() const
{
	return raw_ellipse_angle_;
}

void Observation::SetRawEllipseAngle(const double& ellipse_angle)
{
	raw_ellipse_angle_ = ellipse_angle;
}

double Observation::GetRawEllipseRadius() const
{
	return raw_ellipse_radius_;
}

void Observation::SetRawEllipseRadius(const double& ellipse_radius)
{
	raw_ellipse_radius_ = ellipse_radius;
}

cv::Point2f Observation::GetRawEllipseDistortionCorrection() const
{
	return raw_ellipse_distortion_correction_;
}

void Observation::SetRawEllipseDistortionCorrection(const cv::Point2f& distortion_correction)
{
	raw_ellipse_distortion_correction_ = distortion_correction;
}

cv::Vec3d Observation::GetCameraCoordinates3d() const
{
	return camera_coordinates3d_;
}

void Observation::SetCameraCoordinates3d(const cv::Vec3d& camera_coordinates_3d)
{
	camera_coordinates3d_ = camera_coordinates_3d;
}

size_t Observation::GetScannerPositionId() const
{
	return scanner_position_id_;
}

void Observation::SetScannerPositionId(const size_t scanner_id)
{
	scanner_position_id_ = scanner_id;
}

bool Observation::GetDeactivated() const
{
	return is_deactivated_;
}

void Observation::SetDeactivated(const bool is_deactivated)
{
	is_deactivated_ = is_deactivated;
}

bool Observation::GetIsProcessed() const
{
	return is_processed_;
}

void Observation::SetIsProcessed(const bool is_processed)
{
	is_processed_ = is_processed;
}

double Observation::GetRawDistance() const
{
	return raw_distance_;
}

void Observation::SetRawDistance(const double raw_distance)
{
	raw_distance_ = raw_distance;
}

cv::Point2f Observation::GetEncoderAngles() const
{
	return encoder_angles_;
}

void Observation::SetEncoderAngles(const cv::Point2f& encoder_angles)
{
	encoder_angles_ = encoder_angles;
}


bool Observation::GetScanHalf() const
{
	return scan_half_;
}

void Observation::SetScanHalf(const bool is_scan_half)
{
	scan_half_ = is_scan_half;
}

double Observation::GetWeight() const
{
	return weight_;
}

void Observation::SetWeight(const double weight)
{
	weight_ = weight;
}

void Observation::GetAsCircularMarker(CircularMarker& marker) const
{
	marker.raw_ellipse.center      = raw_ellipse_center_;
	marker.raw_ellipse.angle       = float(raw_ellipse_angle_);
	marker.raw_ellipse.size.height = float(raw_ellipse_height_);
	marker.raw_ellipse.size.width  = float(raw_ellipse_width_);
	marker.id                      = marker_id_;
	marker.raw_distance            = raw_distance_;
	marker.radius                  = raw_ellipse_radius_;
	marker.center_correction       = raw_ellipse_distortion_correction_;
	marker.weight                  = weight_;
}

std::shared_ptr<Observation> Observation::CreateFromCircularMarker(const CircularMarker& marker) const
{
	auto out = std::make_shared<Observation>();
	out->SetRawEllipseCenter(marker.raw_ellipse.center);
	out->SetRawEllipseAngle(double(marker.raw_ellipse.angle));
	out->SetRawEllipseHeight(double(marker.raw_ellipse.size.height));
	out->SetRawEllipseWidth(double(marker.raw_ellipse.size.width));
	out->SetBoardId(marker.id);
	out->SetRawEllipseRadius(marker.radius);
	out->SetRawDistance(marker.raw_distance);
	out->SetRawEllipseDistortionCorrection(marker.center_correction);
	out->SetWeight(marker.weight);

	return out;
}

Observation& Observation::operator=(const Observation& rhs)
{
	if(this != &rhs)
	{
		raw_ellipse_center_                = rhs.raw_ellipse_center_;
		raw_ellipse_angle_                 = rhs.raw_ellipse_angle_;
		raw_ellipse_height_                = rhs.raw_ellipse_height_;
		raw_ellipse_width_                 = rhs.raw_ellipse_width_;
		marker_id_                         = rhs.marker_id_;
		raw_distance_                      = rhs.raw_distance_;
		raw_ellipse_radius_                = rhs.raw_ellipse_radius_;
		raw_ellipse_distortion_correction_ = rhs.raw_ellipse_center_;
		weight_                            = rhs.weight_;
		image_channel_                     = rhs.image_channel_;
		board_id_                          = rhs.board_id_;
		frame_id_                          = rhs.frame_id_;
		scanner_position_id_               = rhs.scanner_position_id_;
		camera_coordinates3d_              = rhs.camera_coordinates3d_;
		encoder_angles_                    = rhs.encoder_angles_;
		scan_half_                         = rhs.scan_half_;
		is_deactivated_                    = rhs.is_deactivated_;
		is_processed_                      = rhs.is_processed_;
	}

	return *this;
}

Observation::Observation(const Observation& rhs)
{
	raw_ellipse_center_                = rhs.raw_ellipse_center_;
	raw_ellipse_angle_                 = rhs.raw_ellipse_angle_;
	raw_ellipse_height_                = rhs.raw_ellipse_height_;
	raw_ellipse_width_                 = rhs.raw_ellipse_width_;
	marker_id_                         = rhs.marker_id_;
	raw_distance_                      = rhs.raw_distance_;
	raw_ellipse_radius_                = rhs.raw_ellipse_radius_;
	raw_ellipse_distortion_correction_ = rhs.raw_ellipse_center_;
	weight_                            = rhs.weight_;
	image_channel_                     = rhs.image_channel_;
	board_id_                          = rhs.board_id_;
	frame_id_                          = rhs.frame_id_;
	scanner_position_id_               = rhs.scanner_position_id_;
	camera_coordinates3d_              = rhs.camera_coordinates3d_;
	encoder_angles_                    = rhs.encoder_angles_;
	scan_half_                         = rhs.scan_half_;
	is_deactivated_                    = rhs.is_deactivated_;
	is_processed_                      = rhs.is_processed_;
}

void Observation::SetFromCircularMarker(const CircularMarker& circular_marker)
{
	SetRawEllipseCenter(cv::Point2f(circular_marker.raw_ellipse.center.x, circular_marker.raw_ellipse.center.y));
	SetRawEllipseAngle(circular_marker.raw_ellipse.angle);
	SetRawEllipseWidth(circular_marker.raw_ellipse.size.width);
	SetRawEllipseHeight(circular_marker.raw_ellipse.size.height);
	SetRawEllipseDistortionCorrection(circular_marker.center_correction);
	SetMarkerId(circular_marker.id);
	SetWeight(circular_marker.weight);
	SetRawDistance(circular_marker.raw_distance);
}

Observation::~Observation() noexcept
= default;

void Observation::Init()
{
	marker_id_            = 0;
	image_channel_        = 0;
	board_id_             = 0;
	frame_id_             = 0;
	scanner_position_id_  = 0;
	camera_coordinates3d_ = cv::Vec3d(0.0, 0.0, 0.0);

	raw_ellipse_center_                = cv::Point2f(0.0, 0.0);
	raw_ellipse_distortion_correction_ = cv::Point2f(0.0, 0.0);
	encoder_angles_                    = cv::Point2f(0.0, 0.0);

	raw_ellipse_width_  = 0.005;
	raw_ellipse_height_ = 0.005;
	raw_ellipse_angle_  = 0.005;
	raw_ellipse_radius_ = 0.005;
	raw_distance_       = 0.0;
	weight_             = 1.0;

	scan_half_      = false;
	is_deactivated_ = false;
}
}
}
