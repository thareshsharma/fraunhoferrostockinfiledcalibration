#include "board_detector.h"
#include "helper.h"
#include "ellipse_detection_distortion_correction.h"
#include "configuration_container.h"
#include "io_messages.h"
#include "calibration_file_base.h"

namespace DMVS
{
namespace CameraCalibration
{
BoardDetector::BoardDetector() = default;

BoardDetector::BoardDetector(const BoardDetector& rhs)
{
	if(this != &rhs)
	{
		board_id_               = rhs.board_id_;
		board_markers_          = rhs.board_markers_;
		rotation_matrix_        = rhs.rotation_matrix_;
		translation_vector_     = rhs.translation_vector_;
		is_detected_            = rhs.is_detected_;
		detected_marker_        = rhs.detected_marker_;
		has_circle_grid_marker_ = rhs.has_circle_grid_marker_;
		has_uncoded_marker_     = rhs.has_uncoded_marker_;
		uncoded_start_id_       = rhs.uncoded_start_id_;
		coded_marker_area_min_  = rhs.coded_marker_area_min_;
	}
}

BoardDetector& BoardDetector::operator=(const BoardDetector& rhs)
{
	if(this != &rhs)
	{
		board_id_               = rhs.board_id_;
		board_markers_          = rhs.board_markers_;
		rotation_matrix_        = rhs.rotation_matrix_;
		translation_vector_     = rhs.translation_vector_;
		is_detected_            = rhs.is_detected_;
		detected_marker_        = rhs.detected_marker_;
		has_circle_grid_marker_ = rhs.has_circle_grid_marker_;
		has_uncoded_marker_     = rhs.has_uncoded_marker_;
		uncoded_start_id_       = rhs.uncoded_start_id_;
		coded_marker_area_min_  = rhs.coded_marker_area_min_;
	}

	return *this;
}

BoardDetector::BoardDetector(BoardDetector&& rhs) noexcept
{
	if(this != &rhs)
	{
		board_id_               = rhs.board_id_;
		board_markers_          = rhs.board_markers_;
		rotation_matrix_        = rhs.rotation_matrix_;
		translation_vector_     = rhs.translation_vector_;
		is_detected_            = rhs.is_detected_;
		detected_marker_        = rhs.detected_marker_;
		has_circle_grid_marker_ = rhs.has_circle_grid_marker_;
		has_uncoded_marker_     = rhs.has_uncoded_marker_;
		uncoded_start_id_       = rhs.uncoded_start_id_;
		coded_marker_area_min_  = rhs.coded_marker_area_min_;

		//Free rhs from its values
		rhs.board_id_ = 0;
		rhs.board_markers_.clear();
		rhs.rotation_matrix_    = cv::Mat::zeros(3, 3, 0);
		rhs.translation_vector_ = cv::Mat::zeros(3, 1, 0);
		rhs.is_detected_        = false;
		rhs.detected_marker_.clear();
		rhs.has_circle_grid_marker_ = false;
		rhs.has_uncoded_marker_     = false;
		rhs.uncoded_start_id_       = 0;
		rhs.coded_marker_area_min_  = 0.0;
	}
}

BoardDetector& BoardDetector::operator=(BoardDetector&& rhs) noexcept
{
	if(this != &rhs)
	{
		board_id_               = rhs.board_id_;
		board_markers_          = rhs.board_markers_;
		rotation_matrix_        = rhs.rotation_matrix_;
		translation_vector_     = rhs.translation_vector_;
		is_detected_            = rhs.is_detected_;
		detected_marker_        = rhs.detected_marker_;
		has_circle_grid_marker_ = rhs.has_circle_grid_marker_;
		has_uncoded_marker_     = rhs.has_uncoded_marker_;
		uncoded_start_id_       = rhs.uncoded_start_id_;
		coded_marker_area_min_  = rhs.coded_marker_area_min_;

		//Free rhs from its values
		rhs.board_id_ = 0;
		rhs.board_markers_.clear();
		rhs.rotation_matrix_    = cv::Mat::zeros(3, 3, 0);
		rhs.translation_vector_ = cv::Mat::zeros(3, 1, 0);
		rhs.is_detected_        = false;
		rhs.detected_marker_.clear();
		rhs.has_circle_grid_marker_ = false;
		rhs.has_uncoded_marker_     = false;
		rhs.uncoded_start_id_       = 0;
		rhs.coded_marker_area_min_  = 0.0;
	}

	return *this;
}

BoardDetector::~BoardDetector() = default;

int BoardDetector::LoadFromFile(const fs::path& file_name)
{
	//Open .YML file with board data
	const cv::FileStorage fs(file_name.string(), cv::FileStorage::READ);

	if(!fs.isOpened())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : File stream is not open " << file_name;
		return -1;
	}

	const auto n = fs["boardID"];

	if(n.isInt())
	{
		n >> board_id_;
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : BoardID read is not an integer";
		return -1;
	}

	//Try to load coded marker coordinates
	cv::Mat coded_marker;
	fs["projectorPlanePts"] >> coded_marker;

	for(auto i = 0; i < coded_marker.rows; ++i)
	{
		const auto marker_id = size_t(coded_marker.at<double>(i, 0));
		const cv::Point3d model_pt(coded_marker.at<double>(i, 1),
		                           coded_marker.at<double>(i, 2),
		                           coded_marker.at<double>(i, 3));
		board_markers_[marker_id] = model_pt;

		//Internal IDs of un-coded marker start immediately after coded IDs
		if(marker_id >= uncoded_start_id_)
		{
			uncoded_start_id_ = marker_id + 1;
		}
	}

	//Try to load circle grid marker coordinates
	cv::Mat circle_grid;
	fs["circleGridMarkerCoordinates"] >> circle_grid;

	for(auto i = 0; i < circle_grid.rows; ++i)
	{
		const auto circle_id = size_t(circle_grid.at<double>(i, 0));
		const cv::Point3d model_pt(circle_grid.at<double>(i, 1),
		                           circle_grid.at<double>(i, 2),
		                           circle_grid.at<double>(i, 3));

		board_markers_[circle_id] = model_pt;

		//Internal IDs of un-coded marker start immediately after coded IDs
		uncoded_start_id_       = circle_id >= uncoded_start_id_ ? circle_id + 1 : uncoded_start_id_;
		has_circle_grid_marker_ = true;
	}

	if(board_markers_.size() < 4)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Board Marker size is  <  " << board_markers_.size();
		return -1;
	}

	//Try to load additional unidentifiable marker
	cv::Mat additional_marker;
	fs["additionalMarkerCoordinates"] >> additional_marker;

	for(auto i = 0; i < additional_marker.rows; ++i)
	{
		const cv::Point3d model_pt(additional_marker.at<double>(i, 0),
		                           additional_marker.at<double>(i, 1),
		                           additional_marker.at<double>(i, 2));

		board_markers_[i + uncoded_start_id_] = model_pt;
	}

	cv::Mat coordinate_system;
	fs["coordinateSystemDefinition"] >> coordinate_system;
	if(coordinate_system.rows == 3)
	{
		RedefineCoordinateSystemOxy(coordinate_system.at<int>(0, 0),
		                            coordinate_system.at<int>(1, 0),
		                            coordinate_system.at<int>(2, 0));
	}

	//load plane contour
	cv::Mat projector_plane_contour;
	fs["projectorPlaneContour"] >> projector_plane_contour;

	if(projector_plane_contour.rows < 1)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot open projector plane contour matrix from " << file_name;
		return -1;
	}

	// contour
	std::vector<cv::Point3d> plane_contour;
	for(auto row = 0; row < projector_plane_contour.rows; ++row)
	{
		cv::Point3d pos(projector_plane_contour.at<double>(row, 0),
		                projector_plane_contour.at<double>(row, 1),
		                projector_plane_contour.at<double>(row, 2));
		plane_contour.push_back(pos);
	}

	board_vertices_ = plane_contour;
	return 0;
}

std::map<BoardID, cv::Point3d> BoardDetector::GetBoardMarkers() const
{
	return board_markers_;
}

BoardID BoardDetector::Detect(const CircularMarkerVector& markers)
{
	return DetectCodedMarkers(markers);
}

BoardID BoardDetector::GetBoardId() const
{
	return board_id_;
}

std::map<BoardID, CircularMarker> BoardDetector::GetDetectedMarkers() const
{
	return detected_marker_;
}

BoardID BoardDetector::GetUncodedMarkerStartId() const
{
	return uncoded_start_id_;
}

bool BoardDetector::HasCircleGridMarker() const
{
	return has_circle_grid_marker_;
}

void BoardDetector::SetMinCodedMarkerArea(const double area)
{
	coded_marker_area_min_ = area;
}

bool BoardDetector::GetOrientation(const std::shared_ptr<CameraSensor>& camera_sensor,
                                   cv::Mat& rotation_matrix,
                                   cv::Mat& translation_vector)
{
	// Initial estimation of board
	if(!CalculateOrientation(camera_sensor))
		return false;

	//correct distortion and perspective distortion
	EllipseDetectionDistortionCorrection corrector(rotation_matrix_, translation_vector_, camera_sensor);
	std::vector<EllipseDetectionDistortionCorrection::Circle> model;

	// get Z=1 normed diameters of circles only from 2d ellipses
	std::vector<cv::RotatedRect> ellipses;
	std::vector<BoardID> used_ids;

	for(const auto& marker : detected_marker_)
	{
		ellipses.push_back(marker.second.raw_ellipse);
		used_ids.push_back(marker.first);
	}

	auto diameters = corrector.GetNormedRealWorldDiameters(ellipses);

	// scale normed diameters to real world diameters by initial board estimation
	const auto used_ids_count = used_ids.size();

	for(auto marker_id = 0; marker_id < used_ids_count; ++marker_id)
	{
		auto point_board  = board_markers_[used_ids[marker_id]];
		auto point_camera = CameraCoordinates(used_ids[marker_id]);

		// scale the Z=1 normalized diameter to real world diameter
		diameters[marker_id] *= point_camera.z;
		EllipseDetectionDistortionCorrection::Circle circle;
		circle.center = cv::Vec3d(point_board.x, point_board.y, point_board.z);

		// this should be the normal of the plane fit to the board markers ?!
		circle.normal = cv::Vec3d(0., 0., 1.0);
		circle.radius = float(diameters[marker_id] * 0.5);
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Marker = [" << used_ids[marker_id] << "] diameter = [" << diameters[marker_id] << "]";
		model.push_back(circle);
	}

	// calculate the corrections based on board detection normal and known circle size
	auto corrections = corrector.GetCorrections(model, nullptr, nullptr);

	for(size_t i = 0; i < used_ids.size(); ++i)
	{
		detected_marker_[used_ids[i]].center_correction = cv::Point2f(corrections[i][0], corrections[i][1]);
	}

	// recalculate camera orientation to board with new marker coordinates
	if(!CalculateOrientation(camera_sensor))
		return false;

	rotation_matrix    = rotation_matrix_.clone();
	translation_vector = translation_vector_.clone();

	return true;
}

void BoardDetector::GetProjectPlaneContour(std::vector<cv::Point3d>& board_vertices) const
{
	board_vertices = board_vertices_;
}

cv::Point3d BoardDetector::CameraCoordinates(const size_t id)
{
	//Get 3d position in board coordinates of detected coded marker on board
	const auto pt_board = board_markers_.at(id);

	//Get the 3d position in camera coordinates
	auto points = std::vector<cv::Point3d>(1, pt_board);
	auto pt_cam = Helper::ApplyTransformation(rotation_matrix_, translation_vector_, points);
	return pt_cam[0];
}

bool BoardDetector::CalculateOrientation(const std::shared_ptr<CameraSensor>& camera_sensor)
{
	is_detected_ = false;

	//check sufficient number of markers
	if(detected_marker_.size() < Constants::MIN_REQUIRED_MARKERS)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not sufficient markers found";
		return false;
	}

	is_detected_ = SolvePosition(detected_marker_, camera_sensor, rotation_matrix_, translation_vector_);

	return is_detected_;
}

void BoardDetector::RedefineCoordinateSystemOxy(const size_t origin_id,
                                                const size_t x_id,
                                                const size_t y_id)
{
	const auto origin = Eigen::Vector3d(board_markers_[origin_id].x, board_markers_[origin_id].y, board_markers_[origin_id].z);
	const auto x      = Eigen::Vector3d(board_markers_[x_id].x, board_markers_[x_id].y, board_markers_[x_id].z);
	const auto y      = Eigen::Vector3d(board_markers_[y_id].x, board_markers_[y_id].y, board_markers_[y_id].z);

	// create the rotation matrix
	Eigen::Matrix3d rot;
	rot.col(0) = (x - origin).normalized();
	rot.col(1) = (y - origin).normalized();
	rot.col(2) = (rot.col(0).cross(rot.col(1))).normalized();
	rot.col(1) = (rot.col(2).cross(rot.col(0))).normalized();

	// Apply transformation to all board marker
	for(auto iterator = board_markers_.begin(); iterator != board_markers_.end(); ++iterator)
	{
		Eigen::Vector3d point(iterator->second.x, iterator->second.y, iterator->second.z);
		Eigen::Vector3d point_new = rot.inverse() * (point - origin);
		iterator->second.x        = point_new[0];
		iterator->second.y        = point_new[1];
		iterator->second.z        = point_new[2];
	}
}

size_t BoardDetector::DetectCodedMarkers(const CircularMarkerVector& markers)
{
	detected_marker_.clear();
	auto const markers_size = markers.size();

	for(auto i = 0; i < markers_size; ++i)
	{
		//Skip marker if ID is not a board marker ID or if it is an intern un-coded marker ID
		const auto marker_id = markers[i].id;
		auto marker_iterator = board_markers_.find(marker_id);

		if(marker_iterator == board_markers_.end() || marker_id > uncoded_start_id_)
			continue;

		const auto area = M_PI * markers[i].raw_ellipse.size.width * markers[i].raw_ellipse.size.height;
		auto newVvalue  = coded_marker_area_min_;

		if(Helper::InRange(area, 0.0, 30.0))
			continue;

		//Add marker to map of detected markers
		detected_marker_[marker_id] = markers[i];
	}

	return detected_marker_.size();
}

bool BoardDetector::UpdatePose()
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED_IN_SCENE;
	return true;
}

size_t BoardDetector::FindUncodedMarker()
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED_IN_SCENE;
	return 0;
}

bool BoardDetector::SolvePosition(const std::map<BoardID, CircularMarker>& markers,
                                  const std::shared_ptr<const CameraSensor> camera_sensor,
                                  cv::Mat& rotation_vector,
                                  cv::Mat& translation_vector) const
{
	std::vector<cv::Point2d> coordinates;
	std::vector<cv::Point3d> model_points;

	for(const std::pair<BoardID, CircularMarker> detected_marker : detected_marker_)
	{
		// use only valid coded ring marker
		if(detected_marker.second.confidence < 0.0)
			continue;

		const auto marker_id = detected_marker.first;
		coordinates.push_back(detected_marker.second.CorrectedCenter());
		model_points.push_back(board_markers_.at(marker_id));
	}

	return camera_sensor->SolvePnP(model_points, coordinates, rotation_vector, translation_vector);
}

size_t BoardDetector::LoadDetectorBoardsInfield(const std::string& calibration_board_files_folder,
                                                std::vector<std::shared_ptr<BoardDetector>>& boards)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Load Detector Boards " << Constants::LOG_START;
	// load all calibration boards
	auto number_boards_with_circle_grid = 0;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Load boards from " << calibration_board_files_folder;

	std::vector<fs::path> used_board_file_paths;
	for(auto i = 1; i < 999; ++i)
	{
		auto board_filename = "board_0" + std::to_string(i) + ".cpd";
		fs::path file_path(calibration_board_files_folder + "\\" + board_filename);

		if(!fs::exists(file_path))
			continue;

		boards.push_back(std::make_shared<BoardDetector>());
		boards.back()->LoadFromFile(file_path.string().c_str());

		if(boards.back()->HasCircleGridMarker())
			number_boards_with_circle_grid++;

		used_board_file_paths.push_back(file_path);
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Load Detector Boards " << Constants::LOG_END;
	return used_board_file_paths.size();
}

size_t BoardDetector::LoadDetectorBoards(const std::shared_ptr<CalibrationFileBase>& file_base,
                                         std::vector<std::shared_ptr<BoardDetector>>& boards,
                                         const std::shared_ptr<ConfigurationContainer> configuration_options)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Load Detector Boards " << Constants::LOG_START;
	// load all calibration boards
	auto number_boards_with_circle_grid = 0;

	// select base folder
	// - replay calibration : same as replay file
	// - live calibration   : configured data folder
	const auto camera_calibration_file = configuration_options->GetConfigurationDataFile();
	auto boards_folder                 = camera_calibration_file.empty() ? *file_base->GetInputPath() : camera_calibration_file.parent_path();
	boards_folder                      = boards_folder.append("Boards");

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Load boards from " << boards_folder;

	std::vector<fs::path> used_board_file_paths;
	for(auto i = 1; i < 999; ++i)
	{
		auto board_filename = "board_0" + std::to_string(i) + ".cpd";
		auto file_path(boards_folder / board_filename);

		if(!fs::exists(file_path))
			continue;

		boards.push_back(std::make_shared<BoardDetector>());
		boards.back()->LoadFromFile(file_path.string().c_str());

		if(boards.back()->HasCircleGridMarker())
			number_boards_with_circle_grid++;

		used_board_file_paths.push_back(file_path);
	}

	// Only one board with circle grid pattern is allowed
	/*if(number_boards_with_circle_grid > 1)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Multiple boards with circle grid pattern - not allowed ";
		boards.clear();
		used_board_file_paths.clear();
	}*/

	// copy the used board files to the calibration directory in sub directory "boards"
	fs::path boards_directory;
	if(file_base->GetCameraCalibrationSubDirectory("Boards", boards_directory) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not create and access statistics sub-directory ";
	}


	for(auto used_board_file_path : used_board_file_paths)
	{
		fs::path dest_file_name = boards_directory / used_board_file_path.filename();
		fs::copy_file(used_board_file_path, dest_file_name);
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Copy Board File From [" << used_board_file_path << "] Destination [" << boards_directory
 << "]";
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Load Detector Boards " << Constants::LOG_END;
	return used_board_file_paths.size();
}
}
}
