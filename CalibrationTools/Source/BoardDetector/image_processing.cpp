#include "image_processing.h"
#include "current_center_compare.h"
#include "ring_code_decoder.h"
#include "constants.h"
#include "helper.h"

namespace DMVS
{
namespace CameraCalibration
{
using namespace boost::accumulators;

void ImageProcessing::DetectBlob(const cv::Mat& image,
                                 const cv::Mat& contour_search_image,
                                 const int threshold,
                                 CircularMarkerVector& centers)
{
}

void ImageProcessing::DetectBlob(const cv::Mat& image,
                                 CircularMarkerVector& key_points)
{
	key_points.clear();
	cv::Mat gray_image;
	cv::cvtColor(image, gray_image, cv::COLOR_BGR2GRAY);

	//Why channel 1 is being used as gray channel calculation can't we just call
	//cvtcolor method to convert the image to gray scale.
	//if(image.channels() == 3)
	//{
	//	/*gray_image = cv::Mat::zeros(image.size(), CV_8U);
	//	const auto rows = image.rows;
	//	const auto cols = image.cols;
	//	for(auto row = 0; row < rows; ++row)
	//	{
	//		for(auto col = 0; col < cols; ++col)
	//		{
	//			gray_image.at<uchar>(row, col) = image.at<cv::Vec3b>(row, col)[1];
	//		}
	//	}*/
	//}
	//else
	//{
	//	gray_image = image;
	//}

	std::vector<CircularMarkerVector> centers;
	std::multiset<CircularMarkerVector, CircularMarkerComparator> current_centers;
	cv::Mat eroded_gray_image;
	cv::erode(gray_image, eroded_gray_image, cv::Mat());
	cv::Mat contour_search_image;

	if(params_.scale != 1.0)
	{
		cv::resize(eroded_gray_image,
		           contour_search_image,
		           cv::Size(),
		           params_.scale,
		           params_.scale,
		           params_.scale < 1.0 ? cv::INTER_AREA : cv::INTER_LANCZOS4);
	}

	CircularMarkerVector current_centers_int;
	if(params_.max_threshold - params_.min_threshold > params_.threshold_step)
	{
#pragma omp parallel for
		{
			for(auto threshold = 0; threshold < params_.max_threshold; threshold += params_.threshold_step)
			{
				FindBlobs(eroded_gray_image, contour_search_image, threshold, current_centers_int);
#pragma omp critical
				{
					current_centers.insert(current_centers_int);
				}
			}
		}
	}
	else
	{
		FindBlobs(eroded_gray_image, contour_search_image, 0, current_centers_int);
		current_centers.insert(current_centers_int);
	}

	const auto current_center_size = current_centers.size();
	const auto center_size         = centers.size();

	for(const auto& center : current_centers)
	{
		std::vector<CircularMarkerVector> new_centers;

		for(auto current_center_idx = 0; current_center_idx < current_center_size; ++current_center_idx)
		{
			auto is_new = true;

			for(auto center_idx = 0; center_idx < center_size; ++center_idx)
			{
				auto mean_center = std::accumulate(centers[current_center_idx].begin(),
				                                   centers[current_center_idx].end(),
				                                   cv::Point2f(0., 0.),
				                                   [](const cv::Point2f& in,
				                                      decltype(centers[center_idx].front()) next)-> cv::Point2f
				                                   {
					                                   return in + next.raw_ellipse.center;
				                                   });

				//Todo check it as there is another condition line 464 in VGFreestyleBlobDetector
				mean_center *= 1. / centers[center_idx].size();
				const auto distance = cv::norm(mean_center - center[current_center_idx].raw_ellipse.center);

				is_new = distance >= params_.min_distance_between_blobs;

				if(!is_new)
				{
					centers[center_idx].push_back(center[current_center_idx]);
#if 0
					//for mean calculation: add it to the first entry
					centers[center_idx][0].raw_ellipse.center = centers[current_center_idx][0].raw_ellipse.center + center[current_center_idx].raw_ellipse.center;
#endif

					auto size = centers[center_idx].size() - 1;
					while(size > 1 && centers[center_idx][size].radius < centers[center_idx][size - 1].radius)
					{
						centers[center_idx][size] = centers[center_idx][size - 1];
						size--;
					}

					centers[center_idx][size] = center[current_center_idx];
					break;
				}
			}

			if(is_new)
			{
#if 1
				//new_centers.push_back(std::vector< CircularMarker>(1, center[current_center_idx]));
				new_centers.emplace_back(1, center[current_center_idx]);
#else
				//Create the first one twice. first entry is to keep track of the sum of the centers (for mean calculation)
				//second one to store this center
				new_centers.push_back(std::vector< CircularMarker>(2, center[current_center_idx]));
#endif
			}
		}
		std::copy(new_centers.begin(), new_centers.end(), std::back_inserter(centers));
	}

	//Create a ring code decoder if detector should only detect coded marker
	RingCodeDecoder* ring_code_decoder = nullptr;

	if(params_.decode_ring_marker)
		ring_code_decoder = new RingCodeDecoder(image,
		                                        Constants::OUTER_IMAGE_RING_FACTOR,
		                                        Constants::NUMBER_OF_BITS);

	for(auto& center : centers)
	{
		if(center.size() < params_.min_repeatability)
			continue;

		CircularMarker mean_center;
		mean_center.confidence         = 0.0;
		mean_center.radius             = 0.0;
		mean_center.raw_ellipse.angle  = 0.0f;
		mean_center.raw_ellipse.center = cv::Point2f(0.0f, 0.0f);
		mean_center.raw_ellipse.size   = cv::Size2f(0.0f, 0.0f);

		float weight_sum = 0.0;
		std::map<size_t, size_t> class_ids;

		accumulator_set<double, features<tag::mean, tag::median>> x, y, angle, width, height, radius;
		auto use_statistics = center.size() > 3;

		//std::vector< float > angles, widths, heights;
		for(auto j = 0; j < center.size(); ++j)
		{
			auto angle_diff = center[j].raw_ellipse.angle - center[0].raw_ellipse.angle;

			//make sure that 0 to 180 deg jumps are handled correctly for averaging
			//angles are 0..180 by find blobs
			if(fabs(angle_diff) > 160.0)
			{
				if(center[0].raw_ellipse.angle < 20)
					center[j].raw_ellipse.angle -= 180.0f;
				else
					center[j].raw_ellipse.angle += 180.0f;
			}

			auto weight = float(center[j].confidence);

			//count the single center class ID
			auto class_id = center[j].id;

			if(class_ids.find(class_id) == class_ids.end())
				class_ids[class_id] = 0;

			class_ids[class_id]++;

			if(use_statistics)
			{
				x(center[j].raw_ellipse.center.x);
				y(center[j].raw_ellipse.center.y);
				angle(center[j].raw_ellipse.angle);
				width(center[j].raw_ellipse.size.width);
				height(center[j].raw_ellipse.size.height);
				radius(center[j].radius);
			}
			else
			{
				mean_center.raw_ellipse.center.x += center[j].raw_ellipse.center.x * weight;
				mean_center.raw_ellipse.center.y += center[j].raw_ellipse.center.y * weight;
				mean_center.raw_ellipse.angle += center[j].raw_ellipse.angle * weight;
				mean_center.raw_ellipse.size.width += center[j].raw_ellipse.size.width * weight;
				mean_center.raw_ellipse.size.height += center[j].raw_ellipse.size.height * weight;
				mean_center.radius = std::max(mean_center.radius, center[j].radius);
				weight_sum += weight;
			}
		}
		if(use_statistics)
		{
			/*x.finish();
			y.finish();
			angle.finish();
			width.finish();
			height.finish();
			radius.finish();*/
			mean_center.raw_ellipse.center.x    = float(median(x));
			mean_center.raw_ellipse.center.y    = float(median(y));
			mean_center.raw_ellipse.angle       = float(median(angle));
			mean_center.raw_ellipse.size.width  = float(median(width));
			mean_center.raw_ellipse.size.height = float(median(height));
			mean_center.radius                  = float(median(radius));
		}
		else
		{
			mean_center.raw_ellipse.center *= (1.0f / weight_sum);
			mean_center.raw_ellipse.angle *= (1.0f / weight_sum);
			mean_center.raw_ellipse.size.width *= (1.0f / weight_sum);
			mean_center.raw_ellipse.size.height *= (1.0f / weight_sum);
		}
		mean_center.confidence = 1.0;

		//check width and height not NaN and positive
		if((mean_center.raw_ellipse.size.width != mean_center.raw_ellipse.size.width) ||
			(mean_center.raw_ellipse.size.height != mean_center.raw_ellipse.size.height) ||
			(mean_center.raw_ellipse.size.height < 0.001) ||
			(mean_center.raw_ellipse.size.width < 0.001))
		{
			continue;
		}

		//Averaging can cause angles out of 0...180 range shift angle to range again
		while(mean_center.raw_ellipse.angle >= 180.0f)
			mean_center.raw_ellipse.angle -= 180.0f;

		while(mean_center.raw_ellipse.angle < 0.0f)
			mean_center.raw_ellipse.angle += 180.0f;

		mean_center.id = -1;

		for(auto it = class_ids.begin(); it != class_ids.end(); ++it)
		{
			//check if the circle has a stable class ID
			if(it->second > center.size() / 2)
				mean_center.id = it->first;
		}

		//if gradient based refinement is active do not use keypoints where refinement failed
		cv::RotatedRect out;
		auto ellipse_fit_success = true;
		/*int nIters = 0;*/
		//for(auto i = 0; i < params_.refine_ellipses; ++i)
		//{
		//	out = ellipseOptimizer.optimize(mean_center.raw_ellipse);
		//	ellipseFitSuccess = ellipseOptimizer.success();
		//	bool converged = (cv::norm(out.center - mean_center.raw_ellipse.center) < 0.001);
		//	nIters++;
		//	mean_center.raw_ellipse = out;
		//	if(converged || !ellipseFitSuccess)
		//		break;
		//}

		if(!ellipse_fit_success)
		{
			continue;
		}

		//check viewing angle (ellipse axes ratio) after refinement
		const auto min_ratio = cos(Helper::AngleInRadians(params_.min_viewing_angle));

		if((out.size.height / out.size.width) < min_ratio || (out.size.height / out.size.width) > (1.0 /
			min_ratio))
		{
			continue;
		}

		if(ring_code_decoder)
		{
			double outer_ring_scale;
			double outer_ring_scale1;
			double outer_ring_scale2;
			auto id = ring_code_decoder->GetMarkerId(mean_center.CorrectedEllipse(),
			                                         outer_ring_scale,
			                                         outer_ring_scale1,
			                                         outer_ring_scale2);

			if(id > 0)
			{
				mean_center.id           = id + 1000;
				mean_center.outer_radius_scale = outer_ring_scale;
			}
		}
		key_points.push_back(mean_center);
	}

	if(ring_code_decoder)
	{
		delete(ring_code_decoder);
		ring_code_decoder = nullptr;
	}
}

void ImageProcessing::DrawRings()
{
}

void ImageProcessing::DrawCenters()
{
}

void ImageProcessing::Reset()
{
}

void ImageProcessing::DetectMarkers()
{
}

void ImageProcessing::FindBlobs(const cv::Mat& image,
                                const cv::Mat& contour_search_image,
                                const int threshold,
                                CircularMarkerVector& centers) const
{
	if(image.empty())
		return;

	centers.clear();
	//The threshold image to search the contours use image if no special contour search image
	//is available special contour search image is a scaled image by params.scale
	cv::Mat contour_search_binary_image;
	GetBinaryImage(image, contour_search_image, threshold, contour_search_binary_image);

	//Compressed chains are faster when computing moments and area. But the result may differ from
	//uncompressed chains. The projector calibration fails with compressed chains. Therefore we
	//use compressed chains for marker detection in tracking mode only.
	//TODO : find root cause for this behavior.
	VectorCVPoint contours;
	std::vector<cv::Vec4i> hierarchy;
	const auto method = params_.decode_ring_marker ? cv::CHAIN_APPROX_SIMPLE : cv::CHAIN_APPROX_NONE;
	cv::findContours(contour_search_binary_image, contours, hierarchy, cv::RETR_TREE, method);
	EnsureOriginalImageSize(contours);

	//Cache moments
	auto contours_size = contours.size();
	VectorCVMoments moments(contours_size);
	for(auto contour_idx   = 0; contour_idx < contours_size; ++contour_idx)
		moments[contour_idx] = cv::moments(contours[contour_idx]);

	for(auto contour_idx = 0; contour_idx < contours.size(); ++contour_idx)
	{
		if(IsContourSizeOk(contours, contour_idx))
			continue;

		//If for the contour i there are no next, previous, parent, or nested contours, the
		//corresponding elements of hierarchy[i] will be negative.
		const auto child_idx = hierarchy[contour_idx][2];
		if(child_idx != -1)
			continue;

		//Check if parent index may be (-1)
		auto parent_idx = hierarchy[contour_idx][3];
		parent_idx      = parent_idx > 0 ? parent_idx : 0;

		//Moments of contour and parent.
		const auto moment        = moments[contour_idx];
		const auto moment_parent = moments[parent_idx];

		double ratio;
		if(!FilterOnParamSettings(contours, moments, contour_idx, parent_idx, moment, moment_parent, ratio))
			continue;

		CircularMarker center;
		center.confidence = ratio * ratio;

		cv::Point2d center_point(moment.m10 / moment.m00, moment.m01 / moment.m00);
		center.raw_ellipse.center = cv::Point2f(float(center_point.x), float(center_point.y));

		//Add ellipse fit to improve circle center detection
		auto rotated_rectangle = cv::fitEllipse(contours[contour_idx]);

		if(IsLessThanMinRatio(rotated_rectangle))
			continue;

		//Ellipse center and blob center have to be very close
		if(cv::norm(rotated_rectangle.center - center.raw_ellipse.center) > Constants::BLOB_CENTER_TOLERANCE)
			continue;

		const auto bounding_rectangle = rotated_rectangle.boundingRect();
		if(CheckBoundingRectangle(image, bounding_rectangle))
			continue;

		//Check if the blob is contained inside a back ring experimental code, it is switched off by default
		if(params_.coded_only && params_.decode_ring_marker && params_.filter_by_ring_in_ring)
		{
			if(IfBlobInsideBlackRing(contour_search_binary_image,
			                         contours,
			                         contour_idx,
			                         center,
			                         moment,
			                         moment_parent))
			{
				continue;
			}
		}

		cv::Size patch_size(bounding_rectangle.size().width + 2 * Constants::BORDER,
		                    bounding_rectangle.size().height + 2 * Constants::BORDER);
		cv::Mat comparing_image = cv::Mat(patch_size, CV_8U, Constants::BLACK);

		//draw 1-px contour line white
		std::vector<cv::Point> local_contour = contours[contour_idx];
		for(auto& i : local_contour)
			i -= cv::Point(bounding_rectangle.x + Constants::BORDER, bounding_rectangle.y + Constants::BORDER);

		std::vector<std::vector<cv::Point>> local_contours;
		local_contours.push_back(local_contour);
		cv::drawContours(comparing_image,
		                 local_contours,
		                 Constants::DRAW_CONTOURS_INDEX,
		                 cv::Scalar(Constants::DRAW_CONTOURS_COLOR),
		                 Constants::DRAW_CONTOURS_THICKNESS);

		//Draw 3px ellipse in black as detected from contour if contour is ellipse +-2px the white 1px contour is completely deleted then
		const auto ellipse_image_center = rotated_rectangle.center - cv::Point2f(
			float(bounding_rectangle.x + Constants::ELLIPSE_IMAGE_CENTER_OFFSET),
			float(bounding_rectangle.y + Constants::ELLIPSE_IMAGE_CENTER_OFFSET));
		const auto patch_ellipse_data = cv::RotatedRect(ellipse_image_center,
		                                                rotated_rectangle.size,
		                                                rotated_rectangle.angle);
		cv::ellipse(comparing_image, patch_ellipse_data, Constants::BLACK, params_.filter_ellipse_width);

		const auto num_differences     = cv::countNonZero(comparing_image);
		const auto allowed_differences = Helper::RoundToInt(
			(local_contours[0].size() * (Constants::ALLOWED_DIFFERENCE_OFFSET - params_.
				percentage_agreement_with_artificial_ellipse)));

		if(num_differences > allowed_differences)
			continue;

		center.raw_ellipse = rotated_rectangle;

		if(params_.set_id_by_color)
			center.id = image.at<uchar>(cvRound(center.raw_ellipse.center.y),
			                                  cvRound(center.raw_ellipse.center.x)) < threshold
				                  ? params_.black_circle_id
				                  : params_.white_circle_id;

		center.radius     = std::max(rotated_rectangle.size.width, rotated_rectangle.size.height);
		center.confidence = Constants::DEFAULT_CENTER_CONFIDENCE;

		//make sure ellipse is in portrait mode
		if(center.raw_ellipse.size.width < center.raw_ellipse.size.height)
		{
			const auto width               = center.raw_ellipse.size.width;
			center.raw_ellipse.size.width  = center.raw_ellipse.size.height;
			center.raw_ellipse.size.height = width;
			center.raw_ellipse.angle += 90.0f;
		}

		//make sure rotation is between 0..180
		while(center.raw_ellipse.angle >= Constants::ELLIPSE_MAX_ROTATION_ANGLE)
			center.raw_ellipse.angle -= Constants::ELLIPSE_MAX_ROTATION_ANGLE;

		while(center.raw_ellipse.angle < Constants::ELLIPSE_MIN_ROTATION_ANGLE)
			center.raw_ellipse.angle += Constants::ELLIPSE_MAX_ROTATION_ANGLE;


		centers.push_back(center);
	}
}

void ImageProcessing::GetBinaryImage(const cv::Mat& image,
                                     const cv::Mat& contour_search_image,
                                     const int threshold,
                                     cv::Mat& contour_search_binary_image) const
{
	if(threshold)
	{
		cv::threshold(contour_search_image.empty() ? image : contour_search_image,
		              contour_search_binary_image,
		              threshold,
		              1,
		              cv::THRESH_BINARY);
	}
	else
	{
		//Why use bit operator here ( | 0x1 )instead just add one to the final value
		const auto block_size = int(Constants::BLOCK_SIZE_CONSTANT * params_.scale) + 1;

		//Do not erode here, otherwise central blob overlaps with outer ring
		//for markers seen from far distance or with a large angle to the camera
		cv::adaptiveThreshold(contour_search_image.empty() ? image : contour_search_image,
		                      contour_search_binary_image,
		                      Constants::ADAPTIVE_THRESHOLD_MAX_VALUE,
		                      cv::ADAPTIVE_THRESH_MEAN_C,
		                      cv::THRESH_BINARY,
		                      block_size,
		                      Constants::ADAPTIVE_THRESHOLD_OFFSET);
	}
}

void ImageProcessing::EnsureOriginalImageSize(VectorCVPoint contours) const
{
	//Readjust contour to original image size if contours have been searched in scaled image
	if(params_.scale - Constants::DEFAULT_SCALE >= FLT_EPSILON)
	{
		for(auto& contour_i : contours)
		{
			for(auto& contour_j : contour_i)
			{
				contour_j.x = cvRound(contour_j.x / params_.scale);
				contour_j.y = cvRound(contour_j.y / params_.scale);
			}
		}
	}
}

bool ImageProcessing::FilterOnParamSettings(const VectorCVPoint& contours,
                                            const VectorCVMoments& moments,
                                            const size_t contour_idx,
                                            const size_t parent_idx,
                                            const cv::Moments moment,
                                            const cv::Moments moment_parent,
                                            double& ratio) const
{
	if(params_.filter_by_area && FilterByArea(moment.m00))
		return false;

	if(params_.filter_by_circularity && !FilterByCircularity(
		moments[contour_idx],
		moment_parent,
		contours,
		contour_idx,
		parent_idx))
		return false;

	if(params_.filter_by_convexity && !FilterByConvexity(moments[contour_idx], contours, contour_idx))
		return false;

	if(params_.filter_by_inertia && !FilterByInertia(moments[contour_idx], ratio))
		return false;

	return true;
}

bool ImageProcessing::FilterByArea(const double area) const
{
	return area < params_.min_area || area >= params_.max_area;
}

bool ImageProcessing::FilterByCircularity(const cv::Moments moment,
                                          const cv::Moments parent_moment,
                                          const VectorCVPoint& contours,
                                          const size_t contour_idx,
                                          const size_t parent_idx) const
{
	const auto area      = moment.m00;
	const auto perimeter = cv::arcLength(contours[contour_idx], true);
	const auto ratio     = 4 * CV_PI * area / (perimeter * perimeter);

	if(ratio < params_.min_circularity || ratio >= params_.max_circularity)
		return false;

	if(params_.coded_only && params_.decode_ring_marker)
	{
		const auto area_parent      = parent_moment.m00;
		const auto perimeter_parent = cv::arcLength(contours[parent_idx], true);
		const auto ratio_parent     = 4 * CV_PI * area_parent / (perimeter_parent * perimeter_parent);

		if(ratio_parent < params_.min_circularity || ratio_parent >= params_.max_circularity)
			return false;

		if(perimeter_parent > Constants::FILTER_BY_CIRCULARITY_FACTOR * perimeter)
			return false;
	}

	return true;
}

bool ImageProcessing::FilterByConvexity(const cv::Moments moment,
                                        const VectorCVPoint& contours,
                                        const size_t contour_idx) const
{
	std::vector<cv::Point> hull;
	cv::convexHull(contours[contour_idx], hull);
	const auto ratio = moment.m00 / cv::contourArea(hull);

	return ratio < params_.min_convexity || ratio >= params_.max_convexity ? false : true;
}

double ImageProcessing::CalculateRatio(const cv::Moments moment,
                                       const double denominator)
{
	const auto cos_min = (moment.mu20 - moment.mu02) / denominator;
	const auto sin_min = 2 * moment.mu11 / denominator;
	const auto cos_max = -cos_min;
	const auto sin_max = -sin_min;
	const auto i_min   = 0.5 * (moment.mu20 + moment.mu02) -
		0.5 * (moment.mu20 - moment.mu02) * cos_min - moment.mu11 * sin_min;
	const auto i_max = 0.5 * (moment.mu20 + moment.mu02) - 0.5 * (moment.mu20 - moment.mu02) * cos_max - moment.
		mu11 * sin_max;

	return i_min / i_max;
}

bool ImageProcessing::FilterByInertia(const cv::Moments moment,
                                      double& ratio) const
{
	const auto denominator = sqrt(pow(2 * moment.mu11, 2) + pow(moment.mu20 - moment.mu02, 2));
	const auto ratio_in    = denominator < Constants::FILTER_BY_INERTIA_EPS
		                         ? CalculateRatio(moment, denominator)
		                         : 1;

	if(ratio_in < params_.min_inertia_ratio || ratio_in >= params_.max_inertia_ratio)
		return false;

	ratio = ratio_in * ratio_in;

	return true;
}

bool ImageProcessing::IsLessThanMinRatio(const cv::RotatedRect& rectangle) const
{
	const auto min_ratio = float(cos(Helper::AngleInRadians(params_.min_viewing_angle)));
	const auto ratio     = rectangle.size.height / rectangle.size.width;

	return Helper::InRange(ratio, min_ratio, 1.0f / min_ratio);
}

bool ImageProcessing::CheckBoundingRectangle(const cv::Mat& image,
                                             const cv::Rect& rectangle)
{
	return rectangle.x < 0 ||
		rectangle.y < 0 ||
		rectangle.x + rectangle.width > image.cols ||
		rectangle.y + rectangle.height > image.rows;
}

bool ImageProcessing::IfBlobInsideBlackRing(const cv::Mat& contour_search_binary_image,
                                            const VectorCVPoint contours,
                                            const size_t contour_idx,
                                            const CircularMarker& center,
                                            const cv::Moments moment,
                                            const cv::Moments moment_parent) const
{
	auto scaled_contour = contours[contour_idx];
	const auto point    = 2.2 * cv::Point(int(center.raw_ellipse.center.x), int(center.raw_ellipse.center.y));

	for(auto i          = 0; i < scaled_contour.size(); ++i)
		scaled_contour[i] = scaled_contour[i] * 3.2 - point;

	const auto overlap_count = GetOverlapCount(contour_search_binary_image, scaled_contour);

	//If less than 60% is black, then the blob is probably not inside a black ring
	return IsBlobToBlackRingRatioOk(moment, moment_parent, scaled_contour, overlap_count);
}

size_t ImageProcessing::GetOverlapCount(const cv::Mat& binary_image,
                                        const std::vector<cv::Point_<int>>& scaled_contour)
{
	size_t count = 0;

	for(const auto& point : scaled_contour)
	{
		if(IsPointOnImage(binary_image, point) && binary_image.at<char>(point) == 0)
		{
			count++;
		}
	}

	return count;
}

bool ImageProcessing::IsBlobToBlackRingRatioOk(const cv::Moments moment,
                                               const cv::Moments moment_parent,
                                               const std::vector<cv::Point_<int>>& contour,
                                               const size_t count) const
{
	if(BlobToBlackRingRatio(contour, count))
	{
		//In case the scaledContour was not properly scaled, check if the blob has a parent with the right size
		if(!params_.filter_by_area)
			return true;

		if(moment_parent.m00 > Constants::MAX_PARENT_SIZE * moment.m00)
			return true;
	}

	return false;
}

bool ImageProcessing::IsParentRightSize(const double area,
                                        const double area_parent) const
{
	//In case the scaledContour was not properly scaled, check if the blob has a parent with the right size
	return params_.filter_by_area && params_.coded_only &&
		params_.decode_ring_marker &&
		area_parent > Constants::MAX_PARENT_SIZE * area;
}

bool ImageProcessing::BlobToBlackRingRatio(const std::vector<cv::Point_<int>> contour,
                                           const size_t count)
{
	return float(count) / float(contour.size()) < Constants::BLOB_BLACK_RING_RATIO;
}

bool ImageProcessing::IsPointOnImage(const cv::Mat image,
                                     const std::vector<cv::Point_<int>>::value_type& point)
{
	return point.x > 0 &&
		point.y > 0 &&
		point.x < image.cols &&
		point.y < image.rows;
}

bool ImageProcessing::IsContourSizeOk(const VectorCVPoint& contours,
                                      const size_t index)
{
	//Contour should be big enough
	return contours[index].size() < Constants::DEFAULT_CONTOUR_SIZE;
}
}
}
