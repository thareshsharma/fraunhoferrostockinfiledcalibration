#include "calibration_boards_transformation.h"
#include "constants.h"

namespace DMVS
{
namespace CameraCalibration
{
CalibrationBoardsTransformation::CalibrationBoardsTransformation() :
	reference_board_id_(Constants::DEFAULT_SIZE_T_VALUE)
{
}

CalibrationBoardsTransformation::CalibrationBoardsTransformation(const std::map<size_t, std::shared_ptr<CalibrationTargetBase>>& board_map) :
	board_data_map_(board_map),
	reference_board_id_(Constants::DEFAULT_SIZE_T_VALUE)
{
	//Initialize to default relative positions
	for(auto& board : board_map)
	{
		board_transformation_map_[board.first] = Transformation();
		initialized_boards_map_[board.first]   = false;
	}
}

CalibrationBoardsTransformation::~CalibrationBoardsTransformation() = default;

CalibrationBoardsTransformation::CalibrationBoardsTransformation(const CalibrationBoardsTransformation& rhs)
{
	board_data_map_           = rhs.board_data_map_;
	board_transformation_map_ = rhs.board_transformation_map_;
	initialized_boards_map_   = rhs.initialized_boards_map_;
	reference_board_id_       = rhs.reference_board_id_;

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
}

CalibrationBoardsTransformation& CalibrationBoardsTransformation::operator=(const CalibrationBoardsTransformation& rhs)
{
	if(this != &rhs)
	{
		board_data_map_           = rhs.board_data_map_;
		board_transformation_map_ = rhs.board_transformation_map_;
		initialized_boards_map_   = rhs.initialized_boards_map_;
		reference_board_id_       = rhs.reference_board_id_;
	}

	return *this;
}

std::set<size_t> CalibrationBoardsTransformation::GetBoardIDs() const
{
	std::set<size_t> board_ids;

	for(const auto& board : board_data_map_)
	{
		board_ids.insert(board.first);
	}

	return board_ids;
}

cv::Point3d CalibrationBoardsTransformation::GetMarkerPosition(const size_t board_id,
                                                               const size_t marker_id) const
{
	const auto board = GetBoard(board_id);

	if(!board)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Board " << board_id << " not found in board positions";
		return cv::Point3_<double>(0., 0., 0.);
	}

	const auto marker_position       = board->GetMarkerPosition(marker_id);
	const auto& board_transformation = GetBoardTransformation(board_id);
	return board_transformation.Multiply(marker_position);
}

const Transformation& CalibrationBoardsTransformation::GetBoardTransformation(const size_t board_id) const
{
	const auto board_transformation = board_transformation_map_.find(board_id);

	if(board_transformation == board_transformation_map_.end())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Board " << board_id << " not found in board positions";
		throw std::exception("Board not found in board positions");
	}

	return board_transformation->second;
}

const std::map<BoardID, Transformation>& CalibrationBoardsTransformation::GetBoardTransformationMap() const
{
	return board_transformation_map_;
}

std::shared_ptr<const CalibrationTargetBase> CalibrationBoardsTransformation::GetBoard(const size_t board_id) const
{
	const auto iterator = board_data_map_.find(board_id);

	if(iterator == board_data_map_.end())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Board id [" << board_id << "] not found ";
		return nullptr;
	}

	return iterator->second;
}

size_t CalibrationBoardsTransformation::GetRequiredParameterCount() const
{
	size_t parameter_count = 0;

	for(const auto& board : board_data_map_)
	{
		parameter_count += board.second->GetRequiredParameterCount();
	}

	return parameter_count;
}

void CalibrationBoardsTransformation::TakeFromParameters(std::vector<double>& parameters)
{
	for(auto& board : board_data_map_)
	{
		board.second->TakeFromParameters(parameters);
	}
}

std::vector<double> CalibrationBoardsTransformation::GetParameters() const
{
	std::vector<double> parameters;

	for(const auto& board : board_data_map_)
	{
		const auto temp = board.second->GetParameters();
		parameters.insert(parameters.end(), temp.begin(), temp.end());
	}

	return parameters;
}

size_t CalibrationBoardsTransformation::GetReferenceBoardID() const
{
	return reference_board_id_;
}

void CalibrationBoardsTransformation::SetReferenceBoardID(const size_t reference_board_id)
{
	if(board_transformation_map_.find(reference_board_id_) == board_transformation_map_.end())
	{
		//probably initial setting of reference board id
		reference_board_id_ = reference_board_id;
	}
	else
	{
		// Change of reference board id. We have to calculate the new positions
		// Assumes that old transformation is unit matrix.
		if(!initialized_boards_map_.at(reference_board_id))
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Trying to set board id to " << reference_board_id <<
 ". But board is not yet initialized. The board may not have been detected during data acquisition.";
			return;
		}

		const auto new_reference = board_transformation_map_.at(reference_board_id);

		for(auto& board_position_map : board_transformation_map_)
		{
			board_position_map.second = new_reference.GetInverse().Multiply(board_position_map.second);
		}

		reference_board_id_ = reference_board_id;
	}
}

void CalibrationBoardsTransformation::SetInitialParameters(const ObservationMap& observations,
                                                           const std::shared_ptr<const CameraParameterOptimizer> camera_parameter_optimizer,
                                                           const std::shared_ptr<const CameraSystemTransformation> camera_system_transformation)
{
	for(auto& board : board_data_map_)
	{
		board.second->SetInitialParameters(observations, camera_parameter_optimizer, camera_system_transformation, board.first);
	}
}

void CalibrationBoardsTransformation::SetBoardTransformation(const BoardID board_id,
                                                             const cv::Mat4d& transformation)
{
	board_transformation_map_[board_id] = Transformation(transformation);
	initialized_boards_map_[board_id]   = true;
}

void CalibrationBoardsTransformation::SetBoardTransformation(const BoardID board_id,
                                                             const Transformation& transformation)
{
	board_transformation_map_[board_id] = Transformation(transformation);
	initialized_boards_map_[board_id]   = true;
}

void CalibrationBoardsTransformation::SetBoardTransformations(const std::map<BoardID, Transformation>& board_transformation_map)
{
	board_transformation_map_.clear();

	for(auto const& position_map : board_transformation_map)
	{
		board_transformation_map_.insert(position_map);
	}
}

void CalibrationBoardsTransformation::UpdateCameraDependentMarkers(const std::shared_ptr<const CameraParameterOptimizer> cameras,
                                                                   const std::shared_ptr<const CameraSystemTransformation>
                                                                   camera_system_transformation)
{
	for(auto& board_data_pair : board_data_map_)
	{
		auto shared_object                       = std::make_shared<CalibrationBoardsTransformation>(board_data_map_);
		shared_object->board_transformation_map_ = board_transformation_map_;
		shared_object->initialized_boards_map_   = initialized_boards_map_;
		shared_object->reference_board_id_       = reference_board_id_;
		board_data_pair.second->UpdateCameraDependentMarkers(cameras, camera_system_transformation, shared_object, board_data_pair.first);
	}
}

bool CalibrationBoardsTransformation::IsBoardInitialized(const BoardID board_id) const
{
	const auto iterator = initialized_boards_map_.find(board_id);

	return iterator == initialized_boards_map_.end() ? false : iterator->second;
}

const std::map<BoardID, bool>& CalibrationBoardsTransformation::GetInitializedBoardsMap() const
{
	return initialized_boards_map_;
}

void CalibrationBoardsTransformation::SetInitializedBoardsMap(const std::map<BoardID, bool>& initialized_boards_map)
{
	initialized_boards_map_ = initialized_boards_map;
}
}
}
