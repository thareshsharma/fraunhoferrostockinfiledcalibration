#include "transformation.h"

namespace DMVS
{
namespace CameraCalibration
{
Transformation::Transformation()
= default;

Transformation::Transformation(const cv::Mat& rotation_matrix,
                               const cv::Mat& translation)
{
	auto rotation = rotation_matrix.clone();

	if(rotation.cols == 1 || rotation.rows == 1)
		cv::Rodrigues(rotation_matrix, rotation);

	transformation_ = cv::Affine3d(rotation, translation);
}

Transformation::Transformation(const Transformation& rhs)
{
	matrix_         = rhs.matrix_.clone();
	transformation_ = rhs.transformation_;
}

Transformation& Transformation::operator=(const Transformation& rhs)
{
	matrix_         = rhs.matrix_;
	transformation_ = rhs.transformation_;
	return *this;
}

//Transformation::Transformation(Transformation&& rhs) noexcept
//{
//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
//}
//
//Transformation& Transformation::operator=(Transformation&& rhs) noexcept
//{
//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
//	matrix_ = rhs.matrix_;
//	transformation_ = rhs.transformation_;
//
//	return *this;
//}

Transformation::Transformation(const cv::Mat& matrix) : matrix_(matrix)
{
	assert(matrix.cols == 4 && matrix.rows == 4);

	transformation_ = cv::Affine3d(matrix);
}

Transformation::Transformation(const cv::Affine3d& transformation) : transformation_(transformation)
{
}

//Transformation::Transformation(const std::shared_ptr<const CameraSensor> sensor)
//{
//	cv::Mat rotation_matrix, translation_vector;
//	sensor->GetExtrinsicParameters(rotation_matrix, translation_vector);
//
//	transformation_ = cv::Affine3d(rotation_matrix, translation_vector);
//}

Transformation::Transformation(const std::shared_ptr<const CameraSensor> sensor)
{
	cv::Mat rotation_matrix, translation_vector;
	sensor->GetExtrinsicParameters(rotation_matrix, translation_vector);

	transformation_ = cv::Affine3d(rotation_matrix, translation_vector);
}

Transformation::Transformation(const std::vector<double>& vector_of_doubles)
{
	cv::Vec3d rotation_vector;
	cv::Vec3d translational_vector;

	for(auto i = 0; i < 3; ++i)
	{
		rotation_vector[i] = vector_of_doubles[i];
	}

	for(auto i = 0; i < 3; ++i)
	{
		translational_vector[i] = vector_of_doubles[i + 3];
	}

	transformation_ = cv::Affine3d(rotation_vector, translational_vector);
}

Transformation::Transformation(const cv::Mat4d& matrix_4d)
{
	const auto rows = matrix_4d.rows;
	const auto cols = matrix_4d.cols;
	assert(rows == 4 && cols == 4);

	for(auto row = 0; row < rows; ++row)
	{
		for(auto col = 0; col < cols; ++col)
		{
			transformation_.matrix(row, col) = matrix_4d.at<double>(row, col);
		}
	}
}

//void Transformation::Identity() const
//{
//	transformation_.Identity();
//}

void Transformation::Rotate(const cv::Vec3d& p,
                            const cv::Vec3d& q)
{
	transformation_.rotation(p);
}

void Transformation::Translate(const cv::Vec3d& translation)
{
	transformation_.translation(translation);
}

//cv::Mat4d Transformation::GetMat4d() const
//{
//	cv::Mat4d out = cv::Mat::zeros(4, 4, CV_64FC1);
//
//	for(auto row = 0; row < 4; ++row)
//	{
//		for(auto col = 0; col < 4; ++col)
//		{
//			out(col, row) = transformation_.matrix(row, col);
//		}
//	}
//
//	return out;
//}

std::vector<double> Transformation::Serialized() const
{
	auto rotation    = transformation_.rvec();
	auto translation = transformation_.translation();

	std::vector<double> out(6);
	out[0] = rotation[0];
	out[1] = rotation[1];
	out[2] = rotation[2];

	out[3] = translation[0];
	out[4] = translation[1];
	out[5] = translation[2];

	return out;
}

Transformation Transformation::Multiply(const Transformation& transformation) const
{
	return Transformation(GetMat4() * transformation.GetMat4());
}

Transformation Transformation::GetRelTransformation(const Transformation& in) const
{
	return Transformation(GetInverseAffine() * in.GetMat4());
}

Transformation Transformation::GetInverse() const
{
	return Transformation(GetMat4().inv());
}

cv::Affine3d Transformation::GetInverseAffine() const
{
	return GetMat4().inv();
}

void Transformation::SetToCalibration(std::shared_ptr<CameraSensor> sensor) const
{
	sensor->SetExtrinsicParameters(GetRotationalTransformation(), GetTranslationalTransformation());
}

cv::Mat Transformation::GetRotationalTransformation() const
{
	return cv::Mat(transformation_.rotation());
}

cv::Mat Transformation::GetTranslationalTransformation() const
{
	return cv::Mat(transformation_.translation());
}

const cv::Affine3d& Transformation::GetMat4() const
{
	return transformation_;
}

bool Transformation::QueryMirrorMat() const
{
	return cv::determinant(transformation_.matrix) < 0;
}
}
}
