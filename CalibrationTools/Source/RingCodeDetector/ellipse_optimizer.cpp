#include "RingCodeDetector/ellipse_optimizer.h"
#include "statistics_value_median_set.h"
#include "cyclic_value.h"
#include <Eigen/Dense>
#include "constants.h"

using namespace DMVS::CameraCalibration;

const size_t EllipseFitOptimizer::minNumGradients = 10;

std::vector<double> toVector(const Eigen::VectorXd& data)
{
	std::vector<double> out(data.size(), 0.);
	for(int i = 0; i < data.size(); ++i)
	{
		out[i] = data[i];
	}

	return out;
}

bool EllipseFitOptimizer::success()
{
	return m_success;
}

std::vector<std::vector<double>> to2DVector(const Eigen::MatrixXd& data)
{
	std::vector<std::vector<double>> out;
	for(int col = 0; col < data.cols(); ++col)
	{
		std::vector<double> nextRow;
		for(int row = 0; row < data.rows(); ++row)
		{
			nextRow.push_back(data(col, row));
		}
		out.push_back(nextRow);
	}

	return out;
}

EllipseFitOptimizer::EllipseFitOptimizer(const cv::Mat& img) :
	m_img(img),
	maxDistSq(1.5 * 1.5),
	m_success(false)
{
	cv::Sobel(img, grad_x, CV_16S, 1, 0, 3);
	cv::Sobel(img, grad_y, CV_16S, 0, 1, 3);
}

cv::RotatedRect EllipseFitOptimizer::optimize(const cv::RotatedRect& inEllipse,
                                              double distance,
                                              const std::vector<std::pair<double, double>>& angleExclude,
                                              double* minVal,
                                              double* maxVal)
{
	if(m_img.depth() == CV_8U)
	{
		return optimizeInternal<uchar>(inEllipse, distance, angleExclude, minVal, maxVal);
	}
	else if(m_img.depth() == CV_16S)
	{
		return optimizeInternal<short>(inEllipse, distance, angleExclude, minVal, maxVal);
	}
	else if(m_img.depth() == CV_16U)
	{
		return optimizeInternal<unsigned short>(inEllipse, distance, angleExclude, minVal, maxVal);
	}
	else
	{
		return optimizeInternal<uchar>(inEllipse, distance, angleExclude, minVal, maxVal);
	}
}

template <class T>
cv::RotatedRect EllipseFitOptimizer::optimizeInternal(const cv::RotatedRect& inEllipse,
                                                      double distance,
                                                      const std::vector<std::pair<double, double>>& angleExclude,
                                                      double* minVal,
                                                      double* maxVal)
{
	// get sub image containing ellipse data
	// inEllipse is the first estimation of the real ellipse contour
	// we scale by 15% to make sure we have the edge in our patch

	cv::RotatedRect scaledEllipse = inEllipse;
	scaledEllipse.size.width *= 1.15f;
	scaledEllipse.size.height *= 1.15f;

	cv::Rect aoi = scaledEllipse.boundingRect();

	// some maximum size checks...
	if(aoi.height > 300 || aoi.width > 300)
	{
		m_success = false;
		return inEllipse;
	}

	const cv::Point2i ellipseCenterInCpyImg(aoi.x + aoi.width / 2, aoi.y + aoi.height / 2);

	// check if area of interest is fully inside image, exclude 1px border because there are no gradients available
	if((aoi.x < 1) || (aoi.x > m_img.cols - 2) || (aoi.y < 1) || (aoi.y > m_img.rows - 2))
	{
		m_success = false;
		return inEllipse;
	}

	// create a mask that defines which pixels are used for the refinement
	// Translate the center of the scaled ellipse so that it is correct in the area of interest patch
	cv::RotatedRect outerEllipse = inEllipse;
	outerEllipse.center.x -= aoi.x;
	outerEllipse.center.y -= aoi.y;

	// cv::RotatedRect innerEllipse = outerEllipse;
	// innerEllipse.size.width *= 0.5;
	// innerEllipse.size.height *= 0.5;

	outerEllipse.size.width *= 1.3f;
	outerEllipse.size.height *= 1.3f;


	cv::Mat localMask = cv::Mat::zeros(aoi.height, aoi.width, cv::DataType<T>::depth);
	cv::ellipse(localMask, outerEllipse, Constants::PURE_BLUE, -1);
	// cv::ellipse(localMask, innerEllipse, cv::Scalar(0), -1);


	//values for determining distance of coordinate from ellipse. If point is too far away from starting ellipse, it is ignored.
	const double cosTheta = cos(inEllipse.angle);
	const double sinTheta = sin(inEllipse.angle);

	//all gradients used in the analysis
	std::vector<Gradient> gradients;

	StatisticValSet<double> stats;

	//do not include first/last col/row because gradient is not well defined there
	for(int y = 0; y < localMask.rows; ++y)
	{
		for(int x = 0; x < localMask.cols - 1; ++x)
		{
			if(localMask.at<T>(y, x) == 0)
				continue;

			cv::Point2i imgPoint(x + aoi.x, y + aoi.y);
			// 			{
			// 				//test whether point is too far away from starting ellipse to be relevant
			// 				const double u = cosTheta*(imgPoint.x - inEllipse.center.x) + sinTheta*(imgPoint.y - inEllipse.center.y);
			// 				const double v = -sinTheta*(imgPoint.x - inEllipse.center.x) + cosTheta*(imgPoint.y - inEllipse.center.y);
			//
			// 				const double dsq = square((u/inEllipse.size.width)) + square((v/inEllipse.size.height));
			// 				if (dsq > maxDistSq)
			// 					continue;
			// 			}


			//use this point
			Gradient next;
			next.coord = cv::Point2i(imgPoint.x, imgPoint.y);
			if(imgPoint.x >= 0 && imgPoint.x < grad_x.cols &&
				imgPoint.y >= 0 && imgPoint.y < grad_x.rows)
			{
				bool validAngle    = true;
				cv::Point2i delta  = next.coord - ellipseCenterInCpyImg;
				const double angle = CyclicValue<double>(atan2(delta.y, delta.x), 2 * M_PI).value();
				for(const auto& angleRegion : angleExclude)
				{
					assert(angleRegion.first < angleRegion.second);
					int multiple = (int)(angleRegion.first / 2 * M_PI);
					for(int i = -3; i <= 3; ++i)
					{
						double locAngle = angle + multiple * 2 * M_PI + i * 2 * M_PI;
						if(locAngle >= angleRegion.first && locAngle <= angleRegion.second)
						{
							validAngle = false;
							break;
						}
					}
					if(!validAngle)
						break;
				}

				if(validAngle)
				{
					localMask.at<T>(y, x) = 150;
					next.gradientX        = grad_x.at<__int16>(imgPoint);
					next.gradientY        = grad_y.at<__int16>(imgPoint);
					stats.add(m_img.at<T>(y, x));
					gradients.push_back(next);
				}
				else
				{
					localMask.at<T>(y, x) = 50;
				}
			}
		}
	}

	stats.finish();
	if(minVal)
	{
		*minVal = stats.minVal;
	}
	if(maxVal)
	{
		*maxVal = stats.maxVal;
	}

	if(gradients.size() < minNumGradients)
	{
		m_success = false;
		return inEllipse;
	}

	//setup linear system of equations (Eq. 5 in paper)
	Eigen::MatrixXd left  = Eigen::MatrixXd::Zero(5, 5);
	Eigen::VectorXd right = Eigen::VectorXd::Zero(5);

	//theses are just for debugging purposes
	std::vector<std::vector<double>> leftStd;
	std::vector<double> rightStd;

	for(std::vector<Gradient>::const_iterator grad = gradients.begin(); grad != gradients.end(); ++grad)
	{
		Eigen::MatrixXd m1 = Eigen::MatrixXd::Zero(5, 1);

		//see Eq. 6 input into line equation of paragraph "Background on conic and dual conic"
		const double a = grad->gradientX;
		const double b = grad->gradientY;
		const double c = -Eigen::Vector2d(a, b).dot(Eigen::Vector2d(grad->coord.x, grad->coord.y));

		//see equation for Ki below Eq. 3 in paper
		m1(0) = a * a;
		m1(1) = a * b;
		m1(2) = b * b;
		m1(3) = a * c;
		m1(4) = b * c;
		//the last element of Ki is left out to constrain result to ellipses, see Section 3.2

		//add this gradient to the linear system of equations given by Eq. 5.
		//Note that the sum is formed over all gradients, i.e. the dimensions of vector and matrix is independent on the number of gradients.
		Eigen::MatrixXd m2 = m1.transpose();
		left += m1 * m2;

		right += -m1 * (c * c);

#if 0
		//debugging..
		leftStd = to2DVector(left);
		rightStd = toVector(right);
#endif
	}

	//this is the dual conic
	const Eigen::VectorXd result = left.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV).solve(right);

	//now calculate the corresponding conic by matrix inversion
	Eigen::Matrix3d dualConic;
	{
		const double A = result[0];
		const double B = result[1];
		const double C = result[2];
		const double D = result[3];
		const double E = result[4];
		const double F = 1;
		dualConic <<
			A, B / 2, D / 2,
			B / 2, C, E / 2,
			D / 2, E / 2, F;
	}

	if(std::fabs(dualConic.determinant()) < DBL_EPSILON)
	{
		m_success = false;
		return inEllipse;
	}

	Eigen::Matrix3d conic = dualConic.inverse();
	double angle, angle2;
	cv::Vec2d center;
	double a, b;

	{
		//analysis on the conic, done via equations given here:

		const double A = conic(0, 0);
		const double B = conic(1, 0) * 2;
		const double C = conic(1, 1);
		const double D = conic(2, 0) * 2;
		const double E = conic(2, 1) * 2;
		const double F = conic(2, 2);

		//http://math.stackexchange.com/questions/280937/finding-the-angle-of-rotation-of-an-ellipse-from-its-general-equation-and-the-ot
		angle     = atan2(B, A - C) / 2;
		center[0] = (B * E - 2 * C * D) / (4 * A * C - (B * B));
		center[1] = (D * B - 2 * A * E) / (4 * A * C - (B * B));
		//analysis can also be done according to this. I have this so far only implemented in python, the results are the same as in the above method.
		//http://en.wikipedia.org/wiki/Matrix_representation_of_conic_sections

		Eigen::Matrix2d a33;
		a33 << A, B / 2, B / 2, C;

		Eigen::EigenSolver<Eigen::Matrix2d> es;
		es.compute(a33, true);

		const double lambda1 = es.eigenvalues().array()[0].real();
		const double lambda2 = es.eigenvalues().array()[1].real();

		Eigen::Vector2d ev1, ev2;
		ev1[0] = es.eigenvectors().col(0)[0].real();
		ev1[1] = es.eigenvectors().col(0)[1].real();
		ev2[0] = es.eigenvectors().col(1)[0].real();
		ev2[1] = es.eigenvectors().col(1)[1].real();

		const Eigen::Vector2d& firstAxis  = ev1;
		const double firstAxisLength      = lambda1;
		const Eigen::Vector2d& secondAxis = ev2;
		const double secondAxisLength     = lambda2;

		std::complex<double> complexVal(firstAxis[0], firstAxis[1]);
		angle2 = std::arg(complexVal);

		//see also: http://www.cs.cornell.edu/cv/OtherPdf/Ellipse.pdf
		a = sqrt(conic.determinant() / (a33.determinant() * fabs(firstAxisLength)));
		b = sqrt(conic.determinant() / (a33.determinant() * fabs(secondAxisLength)));
	}

	cv::RotatedRect out(inEllipse);
	out.center.x    = (float)center[0];
	out.center.y    = (float)center[1];
	out.size.width  = (float)(2.0 * a);
	out.size.height = (float)(2.0 * b);
	out.angle       = (float)(angle2 * 180.0) / M_PI;

	// 	// some output...
	//  	cv::ellipse( cpyImg, inEllipse, cv::Scalar(0,0,255) );
	// 	cv::ellipse( cpyImg, out, cv::Scalar(0,255,0) );
	//  	cv::imshow( "mask", localMask );
	//  	cv::Point2f correction = out.center - inEllipse.center;
	//  	correction *= 50.0f;
	//  	cv::line( cpyImg, inEllipse.center, inEllipse.center+correction, cv::Scalar( 0 ,255,255), 1 );
	//  	cv::imshow( "img ", cpyImg );
	//  	cv::waitKey();

	m_success = true;
	return out;
}
