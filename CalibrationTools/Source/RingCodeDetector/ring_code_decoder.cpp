#include "ring_code_decoder.h"
#include "helper.h"
#include "constants.h"

namespace DMVS
{
namespace CameraCalibration
{
RingCodeDecoder::RingCodeDecoder(const cv::Mat& image,
                                 const float outer_code_ring_scale,
                                 const int num_bits,
                                 const bool img_mirrored_in) :
	outer_code_ring_scale_(outer_code_ring_scale),
	invert_bit_order_(img_mirrored_in),
	stable_only_(true),
	num_bits_(num_bits),
	step_threshold_(20),
	min_threshold_(80),
	max_threshold_(160),
	max_id_(1023),
	min_hits_(2),
	pixel_per_bit_(20),
	num_rows_(50)
{
	if(image.channels() == 3)
		cv::cvtColor(image, image_, cv::COLOR_BGR2GRAY);
	else
		image.copyTo(image_);
}

int RingCodeDecoder::GetMarkerId(const cv::RotatedRect& ellipse,
                                 double& outer_ring_scale,
                                 double& middle_outer_ring_scale,
                                 double& angle) const
{
	const auto is_outer = false;
	double outer_ring_scale_in;
	double angle_in;
	double middle_outer_ring_scale_in;
	const auto marker_id = GetIdInternal(ellipse,
	                                     is_outer,
	                                     outer_ring_scale_in,
	                                     middle_outer_ring_scale_in,
	                                     angle_in);

	if(marker_id == Constants::DEFAULT_SIZE_T_VALUE)
		return Constants::DEFAULT_SIZE_T_VALUE;

	outer_ring_scale        = outer_ring_scale_in;
	angle                   = angle_in;
	middle_outer_ring_scale = middle_outer_ring_scale_in;

	return marker_id;
}

cv::Point2f RingCodeDecoder::GetEllipsePixelOffset(const cv::RotatedRect& ellipse,
                                                   const float angle,
                                                   const float ring_scale) const
{
	const auto angle_radian         = Helper::AngleInRadians(angle);
	const auto ellipse_angle_radian = Helper::AngleInRadians(ellipse.angle);
	const auto aspect_ratio         = ellipse.size.height / ellipse.size.width;

	cv::Point2f offset;
	offset.x = ellipse.size.width * sin(angle_radian) * 0.5f * ring_scale;
	offset.y = ellipse.size.width * cos(angle_radian) * aspect_ratio * 0.5f * ring_scale;

	cv::Point2f offset_rotated;
	offset_rotated.x = offset.x * cos(ellipse_angle_radian) - offset.y * sin(ellipse_angle_radian);
	offset_rotated.y = offset.x * sin(ellipse_angle_radian) + offset.y * cos(ellipse_angle_radian);

	return offset_rotated;
}


int RingCodeDecoder::DecodeImage(cv::Mat& coded_image_full_size,
                                 double& outer_ring_scale,
                                 double& angle,
                                 double& middle_outer_ring_scale) const
{
	int id = -1;

	// check circle center is black (first row is circle center)
	if(coded_image_full_size.at<unsigned char>(0, 0) != 0)
	{
		return id;
	}

	// go to the first row that is completely white, if patch contains some black
	// in the marker surrounding area
	int first_zero_row;

	for(first_zero_row = coded_image_full_size.rows - 1; first_zero_row > 0; first_zero_row--)
	{
		if(cv::countNonZero(coded_image_full_size.row(first_zero_row)) == coded_image_full_size.cols)
			break;
	}

	if(first_zero_row < coded_image_full_size.rows * 0.75)
	{
		//white row not visible at all. we can start looking for first black row at beginning of picture
		first_zero_row = coded_image_full_size.rows - 1;
	}

	// find the row in which the outer ring becomes visible
	auto first_outer_ring_row = first_zero_row;

	for(; first_outer_ring_row > 0; first_outer_ring_row--)
	{
		if(cv::countNonZero(coded_image_full_size.row(first_outer_ring_row)) < coded_image_full_size.cols)
			break;
	}

	if(first_outer_ring_row == 0)
	{
		return id;
	}

	auto coded_image = coded_image_full_size(cv::Rect(0, 0, coded_image_full_size.cols, first_outer_ring_row));

	cv::Mat first_black_all(10, coded_image.cols, CV_8U);

	// find the row with the first black value from bottom (outside) for each column
	for(auto col = 0; col < coded_image.cols; ++col)
	{
		for(auto row = 0; row < coded_image.rows; ++row)
		{
			if(coded_image.at<unsigned char>(coded_image.rows - row - 1, col) == 0)
			{
				for(auto i                                    = 0; i < 10; ++i)
					first_black_all.at<unsigned char>(i, col) = row;

				break;
			}
		}
	}

	auto first_black = first_black_all(cv::Rect(0, 1, first_black_all.cols, first_black_all.rows - 1));

	// Outer ring is at least 20% of marker diameter
	auto ring_thresh = first_outer_ring_row / 5;
	cv::threshold(first_black, first_black, ring_thresh, 255, cv::THRESH_BINARY);

	auto num_white = cv::countNonZero(first_black.row(0));

	// at least 40% tolerance for pixel per bit which is 3px at each side of a 20px block
	auto tolerance_px = int(ceil(float(pixel_per_bit_ * 0.4f)));

	if(abs(pixel_per_bit_ - num_white) > tolerance_px)
	{
		return id;
	}

	std::vector<int> black_to_white, white_to_black; // blackToWhite and WhiteToBlack columns

	// get the last column value
	auto last_val = first_black.at<unsigned char>(0, first_black.cols - 1);

	for(auto col = 0; col < first_black.cols; ++col)
	{
		auto current_val = first_black.at<unsigned char>(0, col);

		if(last_val == 0 && current_val > 0)
			black_to_white.push_back(col);

		if(last_val > 0 && current_val == 0)
			white_to_black.push_back(col);

		last_val = current_val;
	}

	if(white_to_black.size() != 1 || black_to_white.size() != 1)
	{
		return id;
	}

	// zero mark peak is in the middle of the two detected peaks
	// shift by half image size if white area is wrapped
	auto peak = (black_to_white[0] + white_to_black[0]) / 2;

	if(white_to_black[0] < black_to_white[0])
		peak = (peak + first_black.cols / 2) % first_black.cols;

	// if the zero marker wraps, the mean value is to the wrong direction
	// 		int peak = ((peak0+peak1) / 2) ;
	// 		if ( (abs(peak-peak0) > pixel_per_bit_)  || ( abs(peak-peak1) > pixel_per_bit_) )
	// 			peak = (peak - sobelMat8.cols/2) % sobelMat8.cols;
	//

	// estimate width of data ring from black bit at zero mark
	int data_ring_start = first_black_all.at<unsigned char>(0, peak);

	////VGDebug(L" data Ring start: %d", dataRingStart);
	auto data_ring_end = data_ring_start + 1;

	while(data_ring_end < coded_image.rows)
	{
		if(coded_image.at<unsigned char>(coded_image.rows - data_ring_end, peak) > 0)
			break;
		data_ring_end++;
	}
	////VGDebug(L" data Ring end: %d", dataRingEnd);

	// take bit value in the middle of the data ring
	auto data_ring_row = coded_image.rows - (data_ring_end + data_ring_start) / 2;

	////VGDebug(L" data ring row: %d", dataRingRow );

	id = 0;
	for(auto k = 0; k < num_bits_; ++k)
	{
		cv::Point bit_point((peak + (invert_bit_order_ ? (coded_image.cols - k * pixel_per_bit_) : k * pixel_per_bit_)) % coded_image.cols,
		                    data_ring_row);

		auto bit_value = coded_image.at<unsigned char>(bit_point.y, bit_point.x);

		// for stability reasons all neighbors of the sampling pixel must have the same value
		if(stable_only_)
		{
			auto wrong_neighbors = 0;

			for(auto i = std::max(0, bit_point.x - 1); (i < bit_point.x + 2) && (id > -1); ++i)
			{
				auto i_col = i % coded_image.cols;

				for(auto i_row = std::max(0, bit_point.y - 1); i_row < bit_point.y + 2 && id > -1; ++i_row)
				{
					if(coded_image.at<unsigned char>(i_row, i_col) != bit_value)
						wrong_neighbors++;
				}
			}

			if(wrong_neighbors > 1)
			{
				id = -1;
				break;
			}
		}

		// check Bit at outer ring zero mark has to be black
		if(k == 0)
		{
			if(bit_value > 0)
			{
				id = -1;
				break;
			}

			continue;
		}

		// neighbor of zero marker bit (first data bit) need to be white
		if(k == num_bits_ - 1)
		{
			if(bit_value == 0)
			{
				id = -1;
				break;
			}
			continue;
		}

		auto shift_value = num_bits_ - k - 2;
		auto add_value   = 1 << shift_value;

		if(bit_value == 0)
			id += add_value;

		// //VGDebug(L" Bitval %d: %d shift %d add %d  sum %d", iBit, bitVal, shiftVal, addVal, id );
	}

	// Marker successfully detected, use the same IDs as Photomodeler, starts with ID 1
	if(!(id < 0))
	{
		id++;

		if(outer_ring_scale)
		{
			outer_ring_scale = float(first_outer_ring_row) / float(num_rows_) * float(outer_code_ring_scale_);
		}

		if(middle_outer_ring_scale)
		{
			middle_outer_ring_scale = float(first_outer_ring_row - float(data_ring_start) / 2.) / float(num_rows_) * float(outer_code_ring_scale_);
		}

		// image width is 360 deg , translate column to angle
		if(angle)
		{
			angle = double(peak) / double(coded_image.cols) * 2.0 * M_PI;
		}
	}

	return id;
}

int RingCodeDecoder::GetIdInternal(const cv::RotatedRect& ellipse,
                                   const bool is_outer,
                                   double& outer_ring_scale,
                                   double& middle_outer_ring_scale,
                                   double& angle) const
{
	auto id            = Constants::DEFAULT_SIZE_T_VALUE;
	auto outer_ellipse = ellipse;

	auto outer_code_ring_scale = is_outer ? 1.1f : outer_code_ring_scale_;

	outer_ellipse.size.width *= outer_code_ring_scale;
	outer_ellipse.size.height *= outer_code_ring_scale;

	auto bound_rect = outer_ellipse.boundingRect();
	cv::Rect img_rect(0, 0, image_.cols, image_.rows);

	cv::Mat patch;
	auto copy_rect = bound_rect & img_rect;

	if(copy_rect.area() < 5 * 5)
	{
		return Constants::DEFAULT_SIZE_T_VALUE;
	}

	image_(copy_rect).copyTo(patch);

	auto local_ellipse = ellipse;
	local_ellipse.center.x -= copy_rect.x;
	local_ellipse.center.y -= copy_rect.y;
	outer_ellipse.center.x -= copy_rect.x;
	outer_ellipse.center.y -= copy_rect.y;

	auto patch_ellipse = local_ellipse;
	auto rotated_mat   = getRotationMatrix2D(local_ellipse.center, ellipse.angle, 1.0);

	// perform the affine transformation
	cv::Mat patch_rotated;
	warpAffine(patch, patch_rotated, rotated_mat, patch.size(), cv::INTER_LANCZOS4, cv::BORDER_CONSTANT, cv::Scalar(0xff));

	local_ellipse.angle = 0.0;
	// cv::ellipse( patchRotated, localEllipse , cv::Scalar(0) );

	if(fabs(local_ellipse.size.height) < FLT_EPSILON)
		return Constants::DEFAULT_SIZE_T_VALUE;

	auto scale            = 200.0f / std::min(patch.rows, patch.cols);
	auto vertical_scale   = scale * local_ellipse.size.width / local_ellipse.size.height;
	auto horizontal_scale = scale * 1.0f;

	local_ellipse.center.x *= horizontal_scale;
	local_ellipse.size.width *= horizontal_scale;
	local_ellipse.center.y *= vertical_scale;
	local_ellipse.size.height *= vertical_scale;

	// Test for reasonable scaling
	if(vertical_scale / horizontal_scale > 4.0f)
		return Constants::DEFAULT_SIZE_T_VALUE;

	if(horizontal_scale / vertical_scale > 4.0f)
		return Constants::DEFAULT_SIZE_T_VALUE;

	if(patch_rotated.rows * vertical_scale <= 0 || (patch_rotated.rows * vertical_scale) > 1000)
		return Constants::DEFAULT_SIZE_T_VALUE;

	if(patch_rotated.cols * horizontal_scale <= 0 || patch_rotated.cols * horizontal_scale > 1000)
		return Constants::DEFAULT_SIZE_T_VALUE;

	cv::Mat patch_rotated_resized;
	cv::resize(patch_rotated, patch_rotated_resized, cv::Size(0, 0), horizontal_scale, vertical_scale, cv::INTER_LANCZOS4);

	auto cols           = pixel_per_bit_ * num_bits_;
	auto radius_step    = outer_code_ring_scale / float(num_rows_);
	cv::Mat coded_image = cv::Mat::zeros(num_rows_, cols, CV_8U);

	for(auto col = 0; col < cols; ++col)
	{
		auto angle_a = 2.0 * M_PI * col / cols;
		auto sin_a   = sin(angle_a);
		auto cos_a   = cos(angle_a);

		for(auto row = 0; row < num_rows_; ++row)
		{
			auto radius_scale = row * radius_step;
			auto x            = sin_a * radius_scale * local_ellipse.size.width * 0.5 + local_ellipse.center.x;
			auto y            = cos_a * radius_scale * local_ellipse.size.height * 0.5 + local_ellipse.center.y;

			auto xi = cvRound(x);
			auto yi = cvRound(y);

			coded_image.at<unsigned char>(row, col) = xi < 0 || xi >= patch_rotated_resized.cols || yi < 0 || yi >= patch_rotated_resized.rows
				                                          ? 0xFF
				                                          : patch_rotated_resized.at<unsigned char>(yi, xi);
		}
	}

	cv::Mat binary_coded_image;
	auto deskewed_angle = 0.0;
	cv::normalize(coded_image, coded_image, 0, 0xFF, cv::NORM_MINMAX);

	// Loop over thresholds and try to find the marker id
	std::vector<int> ids;

	for(auto thresh = min_threshold_; thresh <= max_threshold_; thresh += step_threshold_)
	{
		cv::threshold(coded_image, binary_coded_image, thresh, 255, cv::THRESH_BINARY);

		auto idl = DecodeImage(binary_coded_image, outer_ring_scale, deskewed_angle, middle_outer_ring_scale);

		if(idl != -1)
			ids.push_back(idl);
	}

	// No ids found
	if(int(ids.size()) < min_hits_)
		return Constants::DEFAULT_SIZE_T_VALUE;

	// All ids should be the same
	id = ids[0];

	for(auto idl : ids)
	{
		if(id != idl)
			return Constants::DEFAULT_SIZE_T_VALUE;
	}

	if(id < 0 || id > max_id_)
		return Constants::DEFAULT_SIZE_T_VALUE;

	// Calculate the marker angle
	// get point on zero mark as detected in coded image
	auto radius = local_ellipse.size.width / 2.0f * outer_ring_scale;
	auto x      = local_ellipse.center.x + sin(deskewed_angle) * radius;
	auto y      = local_ellipse.center.y + cos(deskewed_angle) * radius;

	// undo image resize
	y /= vertical_scale;
	x /= horizontal_scale;
	x -= patch_ellipse.center.x;
	y -= patch_ellipse.center.y;

	// undo image rotation
	auto ang = ellipse.angle / 180.0 * M_PI;
	cv::Vec2f p_new;
	p_new[0] = float(x * cos(ang) - y * sin(ang));
	p_new[1] = float(y * cos(ang) + x * sin(ang));

	// calculate angle
	auto s         = cv::norm(p_new);
	auto direction = (p_new[0] > 0) ? 1.0 : -1.0;
	angle          = direction * acos(-p_new[1] / s);

	return int(id);
}

size_t RingCodeDecoder::FindMarkerIdFirst(const cv::Mat& coded_image,
                                          double& outer_ring_scale,
                                          double& middle_outer_ring_scale,
                                          double& deskewed_angle) const
{
	cv::Mat binary_coded_image;
	deskewed_angle = 0.0;
	std::vector<int> marker_ids;
	for(auto threshold = Constants::MIN_THRESHOLD; threshold <= Constants::MAX_THRESHOLD; threshold +=
	    Constants::STEP_THRESHOLD)
	{
		cv::threshold(coded_image,
		              binary_coded_image,
		              threshold,
		              Constants::MAX_THRESHOLD_VALUE,
		              cv::THRESH_BINARY);

		auto marker_id = DecodeImage(binary_coded_image,
		                             outer_ring_scale,
		                             deskewed_angle,
		                             middle_outer_ring_scale);

		if(marker_id != -1)
			marker_ids.push_back(marker_id);
	}

	if(MarkerIdsSanityCheck(marker_ids) == Constants::DEFAULT_SIZE_T_VALUE)
		return Constants::DEFAULT_SIZE_T_VALUE;

	return marker_ids[0];
}

size_t RingCodeDecoder::MarkerIdsSanityCheck(const std::vector<int>& marker_ids)
{
	//No ids found
	if(marker_ids.size() < Constants::MIN_HITS)
		return Constants::DEFAULT_SIZE_T_VALUE;

	//All ids should be the same
	const auto marker_id_first = marker_ids[0];
	for(auto marker_id : marker_ids)
	{
		if(marker_id_first != marker_id)
			return Constants::DEFAULT_SIZE_T_VALUE;
	}

	if(Helper::InRange(marker_id_first, 0, Constants::MAX_ID))
		return Constants::DEFAULT_SIZE_T_VALUE;

	return 1;
}

bool RingCodeDecoder::IsScalingReasonable(const cv::Mat& patch,
                                          const float vertical_scale,
                                          const float horizontal_scale)
{
	if(vertical_scale / horizontal_scale > Constants::REASONABLE_SCALING_RATIO)
		return false;

	if(horizontal_scale / vertical_scale > Constants::REASONABLE_SCALING_RATIO)
		return false;

	if(Helper::InRange(int(patch.cols * horizontal_scale),
	                   Constants::RANGE_LOWER_LIMIT,
	                   Constants::RANGE_UPPER_LIMIT))
		return false;

	if(Helper::InRange(int(patch.rows * vertical_scale),
	                   Constants::RANGE_LOWER_LIMIT,
	                   Constants::RANGE_UPPER_LIMIT))
		return false;

	return true;
}

void RingCodeDecoder::CalculateCodedImage(const cv::RotatedRect& ellipse,
                                          const cv::Mat& patch,
                                          const float code_ring_scale,
                                          cv::Mat& coded_image) const
{
	const auto cols         = Constants::PIXEL_PER_BIT * num_bits_;
	const auto radius_step  = code_ring_scale / Constants::RING_DECODER_ROWS;
	coded_image             = cv::Mat::zeros(Constants::RING_DECODER_ROWS, cols, CV_8U);
	const auto multi_factor = 2.0 * M_PI / cols;

	for(auto col = 0; col < cols; ++col)
	{
		const auto angle = multi_factor * col;
		const auto sin_a = sin(angle);
		const auto cos_a = cos(angle);

		for(auto row = 0; row < Constants::RING_DECODER_ROWS; ++row)
		{
			const auto radius_scale         = row * radius_step;
			const auto center_x             = cvRound(sin_a * radius_scale * ellipse.size.width * 0.5 + ellipse.center.x);
			const auto center_y             = cvRound(cos_a * radius_scale * ellipse.size.height * 0.5 + ellipse.center.y);
			const auto is_center_x_in_range = Helper::InRange(center_x, 0, patch.cols);
			const auto is_center_y_in_range = Helper::InRange(center_y, 0, patch.rows);

			coded_image.at<unsigned char>(row, col) = !is_center_x_in_range || !is_center_y_in_range
				                                          ? Constants::PIXEL_COLOR_WHITE
				                                          : patch.at<unsigned char>(center_y, center_x);
		}
	}
}

double RingCodeDecoder::CalculateMarkerAngle(const cv::RotatedRect& local_ellipse,
                                             const cv::RotatedRect& patch_ellipse,
                                             const double rotation,
                                             const double outer_ring_scale,
                                             const float vertical_scale,
                                             const float horizontal_scale,
                                             const double deskewed_angle) const
{
	//Calculate the marker angle get point on zero mark as detected in coded image
	const auto local_ellipse_radius = local_ellipse.size.width / 2.0f * outer_ring_scale;
	auto center_x                   = local_ellipse.center.x + sin(deskewed_angle) * local_ellipse_radius;
	auto center_y                   = local_ellipse.center.y + cos(deskewed_angle) * local_ellipse_radius;

	//Undo image resize
	center_x /= horizontal_scale;
	center_y /= vertical_scale;

	center_x -= patch_ellipse.center.x;
	center_y -= patch_ellipse.center.y;

	//Undo image rotation
	cv::Vec2f p_new;
	p_new[0] = float(center_x * cos(rotation) - center_y * sin(rotation));
	p_new[1] = float(center_y * cos(rotation) + center_x * sin(rotation));

	//Calculate angle
	const auto s         = cv::norm(p_new);
	const auto direction = p_new[0] > 0 ? 1.0 : -1.0;

	return direction * acos(-p_new[1] / s);
}

bool RingCodeDecoder::GetBoundingRectangle(const cv::RotatedRect& ellipse,
                                           const float outer_code_ring_scale,
                                           cv::Rect_<int>& bounding_rectangle) const
{
	auto outer_ellipse = ellipse;
	outer_ellipse.size.width *= outer_code_ring_scale;
	outer_ellipse.size.height *= outer_code_ring_scale;
	const cv::Rect rectangle(0, 0, image_.cols, image_.rows);
	bounding_rectangle = outer_ellipse.boundingRect() & rectangle;

	//Here if return value false, then bounding rectangle is incorrect
	return bounding_rectangle.area() < Constants::BOUNDING_RECTANGLE_MIN_AREA;
}

void RingCodeDecoder::ScaleEllipse(cv::RotatedRect& local_ellipse,
                                   const float vertical_scale,
                                   const float horizontal_scale)
{
	local_ellipse.center.x *= horizontal_scale;
	local_ellipse.center.y *= vertical_scale;
	local_ellipse.size.width *= horizontal_scale;
	local_ellipse.size.height *= vertical_scale;
}

void RingCodeDecoder::CalculatePatchRotated(const cv::Mat& patch,
                                            const cv::Point2f& center,
                                            const float angle,
                                            cv::Mat& patch_rotated) const
{
	const auto rotation_matrix = getRotationMatrix2D(center, angle, Constants::DEFAULT_SCALING_FACTOR);
	warpAffine(patch, patch_rotated, rotation_matrix, patch.size(), cv::INTER_LANCZOS4, cv::BORDER_CONSTANT, cv::Scalar(0xff));
}

bool RingCodeDecoder::GetEllipseAndScales(const cv::RotatedRect& ellipse,
                                          cv::RotatedRect& local_ellipse,
                                          const cv::Mat& patch,
                                          float& vertical_scale,
                                          float& horizontal_scale,
                                          cv::Mat& patch_rotated_resized) const
{
	cv::Mat patch_rotated;
	CalculatePatchRotated(patch, local_ellipse.center, ellipse.angle, patch_rotated);
	local_ellipse.angle = Constants::ZERO_ANGLE;

	if(fabs(local_ellipse.size.height) < FLT_EPSILON)
		return false;

	const auto scale = Constants::SCALING_CONSTANT / std::min(patch.rows, patch.cols);
	vertical_scale   = scale * ellipse.size.width / ellipse.size.height;
	horizontal_scale = scale * 1.0f;

	//Test for reasonable scaling
	if(!IsScalingReasonable(patch_rotated, vertical_scale, horizontal_scale))
		return false;

	ScaleEllipse(local_ellipse, vertical_scale, horizontal_scale);

	cv::resize(patch_rotated,
	           patch_rotated_resized,
	           cv::Size(0, 0),
	           horizontal_scale,
	           vertical_scale,
	           cv::INTER_LANCZOS4);

	return true;
}
}
}
