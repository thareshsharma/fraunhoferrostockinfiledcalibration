#include "coded_ring_center_detector.h"
#include "ellipse_optimizer.h"

CodedRingCenterDetector::CodedRingCenterDetector(const cv::Mat& input_image) :
	ratio_outer_vs_inner_radius_(0.02 / 0.004)
{
	if(input_image.channels() == 3)
		cvtColor(input_image, image_, cv::COLOR_BGR2GRAY);
	else
		input_image.copyTo(image_);

	filled_ = image_.clone();
}

bool CodedRingCenterDetector::GetOuterEllipse(const cv::RotatedRect& inner_circle,
                                              const double coding_angle,
                                              cv::RotatedRect& out,
                                              const double outer_scale,
                                              double& min_val,
                                              double& max_val)
{
	if(image_.depth() == CV_8U)
	{
		return GetOuterEllipseIntern<uchar>(inner_circle, coding_angle, out, outer_scale, min_val, max_val);
	}

	if(image_.depth() == CV_16S)
	{
		return GetOuterEllipseIntern<short>(inner_circle, coding_angle, out, outer_scale, min_val, max_val);
	}

	if(image_.depth() == CV_16U)
	{
		return GetOuterEllipseIntern<unsigned short>(inner_circle, coding_angle, out, outer_scale, min_val, max_val);
	}

	return GetOuterEllipseIntern<uchar>(inner_circle, coding_angle, out, outer_scale, min_val, max_val);
}

void CodedRingCenterDetector::UseDistortionCorrection(const SingleCameraCalibration& camera)
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
}

template <class T>
bool CodedRingCenterDetector::GetOuterEllipseIntern(const cv::RotatedRect& in,
                                                    double coding_angle,
                                                    cv::RotatedRect& out,
                                                    const double outer_scale,
                                                    double& min_val,
                                                    double& max_val)
{
	//double oldAng = coding_angle;

	coding_angle -= M_PI / 2;

	//1. determine which angle region to leave out in the determination.
	// This is necessary as the orientation is coded in the outer ring as
	// a white part. The opposing angle also needs to be left out then.
	//2. fill the circles with constant values
	//3. determine new rotated rect

	//1. angle: 0deg -> +col; 90deg +row
	//this determines the patch size
	const auto ratio1 = ratio_outer_vs_inner_radius_ * 1.2;

	//this determines the artificial inner ellipse that is used to fill the white space
	const auto ratio2 = outer_scale;
	auto center_grey  = image_.at<uchar>(in.center);

	cv::RotatedRect filler(in);
	filler.size = cv::Size2f(in.size.width * float(ratio1), in.size.height * float(ratio2));

	cv::RotatedRect full_filler(in);
	full_filler.size = cv::Size2f(in.size.width * float(ratio1), in.size.height * float(ratio2));

	cv::Rect aoi = full_filler.boundingRect();

	if(aoi.x + aoi.width >= filled_.cols || aoi.y + aoi.height >= filled_.rows || aoi.x < 0 || aoi.y < 0)
		return false;

	cv::Mat patch = filled_(aoi).clone();
	cv::Mat patch_rotated;

	cv::RotatedRect loc_filler(filler);
	{
		//draw the ellipses in the patch
		loc_filler.center -= cv::Point2f(float(aoi.x), float(aoi.y));
		cv::ellipse(patch, loc_filler, center_grey, -1);

		//draw an outer ellipse to delete objects that mey reach into the patch but do not belong to the ellipse.
		cv::RotatedRect outside_filler(loc_filler);
		outside_filler.size.width *= 1.3f;
		outside_filler.size.height *= 1.3f;

		cv::Mat mask = cv::Mat::zeros(patch.rows, patch.cols, cv::DataType<T>::depth);
		cv::ellipse(mask, outside_filler, 255, -1);

		cv::Mat cut_out = patch.clone();

		for(auto row = 0; row < patch.rows; ++row)
		{
			for(auto col = 0; col < patch.cols; ++col)
			{
				cut_out.at<T>(row, col) = !mask.at<T>(row, col) ? 0 : patch.at<T>(row, col);
			}
		}

		double min_value, max_value;
		cv::minMaxIdx(cut_out, &min_value, &max_value);

		T white_val = T(max_value);

		for(auto row = 0; row < patch.rows; ++row)
		{
			for(auto col = 0; col < patch.cols; ++col)
			{
				if(!mask.at<T>(row, col))
				{
					patch.at<T>(row, col) = white_val; //GH: Why not set zero as optimizer should then ignore those gradients.
				}
			}
		}
	}

	std::pair<double, double> range(coding_angle - ((30. * M_PI) / 180.0), coding_angle + ((30. * M_PI) / 180.0));
	std::vector<std::pair<double, double>> angle_exclude;
	angle_exclude.push_back(range);
	angle_exclude.push_back(std::make_pair(range.first + M_PI, range.second + M_PI));

	auto optimizer = std::make_shared<EllipseFitOptimizer>(patch);
	out            = optimizer->optimize(loc_filler, 0.0, angle_exclude, &min_val, &max_val);

	//cv::ellipse(patchOrig,out,100);
	out.center += cv::Point2f(float(aoi.x), float(aoi.y));

	return optimizer->success();
}
