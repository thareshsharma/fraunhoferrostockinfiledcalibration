#include "application_settings.h"
#include "helper.h"
#include <boost/dll.hpp>

namespace po = boost::program_options;

ApplicationSettings::ApplicationSettings(int ac,
                                         char* av[]) :
	record(true),
	store_every_image(false),
	dd_exp1(true),
	dd_exp2(true),
	dd_exp3(true),
	dd_scalar1(true),
	dd_scalar2(false),
	distortion(false),
	extrinsic(false),
	focal_length(false),
	application_name("FactoryCalibration"),
	path_to_setting_file("dmvs_infield_compensation_settings.json"),
	calibration_files_folder(Helper::GetCamerasDirectory().string()),
	calibration_filename("dmvs_calibration_template.xml"),
	serial_no("DMVS_DMVS011900000"),
	storage_path(boost::dll::program_location().parent_path().string() + "\\" + application_name + "_" + Helper::GetTimeStamp()),
	board_files_path(R"(C:\ProgramData\FARO\Freestyle\Calibrations\Boards)"),
	config_file_path("config.txt"),
	depth_dependent_exp1(0.25),
	depth_dependent_exp2(3.0),
	depth_dependent_exp3(5.0),
	depth_dependent_scalar1(0.4),
	depth_dependent_scalar2(0.0065),
	max_led_saturation(0.2),
	min_led_fraction(0.1),
	laser_dot_threshold(3000),
	images_to_record(40),
	valid_image_count(0),
	image_interval(0.7f),
	doublet_tolerance(0.2f)
{
	po::options_description config_options("Allowed options");

	config_options.add_options()
		("help", "produce help message")
		("config,c", po::value<std::string>(&config_file_path), "Name of the application config file")
		("dmvssettingsfile", po::value<std::string>(&this->path_to_setting_file), "Path and filename of used dmvs setting file")
		("applicationname", po::value<std::string>(&this->application_name), "Application which needs to be executed (InfieldCompensation or FactoryCalibration or  LensTestingApp)")
		("storagepath", po::value<std::string>(&this->storage_path), "Path for storing recorded images")
		("calibrationfilename", po::value<std::string>(&this->calibration_filename), "Calibration file name")
		("boardfilespath", po::value<std::string>(&this->board_files_path), "Path to read the board files")
		("interval", po::value<float>(&this->image_interval),"Interval between images [s]")
		("tolerance", po::value<float>(&this->doublet_tolerance),"Maximum time between LED and Laser images")
		("record", po::value<bool>(&this->record), "Process recorded images")
		("ddexp1", po::value<bool>(&this->dd_exp1), "Keep depth dependent exp param 1 fixed")
		("ddexp2", po::value<bool>(&this->dd_exp2), "Keep depth dependent exp param 2 fixed")
		("ddexp3", po::value<bool>(&this->dd_exp3), "Keep depth dependent exp param 3 fixed")
		("ddscalar1", po::value<bool>(&this->dd_scalar1), "Keep depth dependent scalar param 1 fixed")
		("ddscalar2", po::value<bool>(&this->dd_scalar2), "Keep depth dependent scalar param 2 fixed")
		("distortion", po::value<bool>(&this->distortion), "Keep camera distortion params fixed")
		("extrinsic", po::value<bool>(&this->extrinsic), "Keep extrinsic parameters fixed")
		("focallength", po::value<bool>(&this->focal_length), "Keep focal length parameters fixed")
		("depthedependentscalar1", po::value<double>(&this->depth_dependent_scalar1), "Depth dependent scalar 1")
		("depthedependentscalar2", po::value<double>(&this->depth_dependent_scalar2), "Depth dependent scalar 2")
		("depthedependentexp1", po::value<double>(&this->depth_dependent_exp1), "Depth dependent exp1")
		("depthedependentexp2", po::value<double>(&this->depth_dependent_exp2), "Depth dependent exp2")
		("depthedependentexp3", po::value<double>(&this->depth_dependent_exp3), "Depth dependent exp3")
		("minledfraction", po::value<double>(&this->min_led_fraction), "Minimum LED fraction for images")
		("maxledsaturation", po::value<double>(&this->max_led_saturation), "Maximum LED saturation")
		("allimages", po::value<bool>(&this->store_every_image), "Store all recorded images")
		("laserimage", po::value<int>(&this->laser_dot_threshold), "Min. number of dots that should be recognized in a laser image")
		("imagestorecord", po::value<int>(&this->images_to_record), "Number of image to record");

	po::options_description cmd_line_options;
	cmd_line_options.add(config_options);
	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, cmd_line_options), vm);
	po::notify(vm);

	if(vm.count("help"))
	{
		std::cout << config_options << "\n";
		return;
	}

	std::ifstream config_stream;

	if(!(fs::exists(config_file_path) && fs::is_regular_file(config_file_path)))
	{
		config_file_path = boost::dll::program_location().parent_path().string() + "\\" + config_file_path;

		if(config_file_path.find(".txt") == std::string::npos)
		{
			config_file_path = config_file_path + ".txt";
		}
	}

	config_stream.open(config_file_path.c_str());

	if(config_stream)
	{
		BOOST_LOG_TRIVIAL(info) << "Opened config settings file [" << config_file_path << "]";

		try
		{
			//'true' means 'allow unregistered'. If set to 'false', unknown options will cause a crash
			po::store(po::parse_config_file(config_stream, config_options, true), vm);
		}
		catch(...)
		{
			BOOST_LOG_TRIVIAL(error) << "Exception while parsing config file";
		}
	}
	else
	{
		BOOST_LOG_TRIVIAL(warning) << "Unable to open settings file: " << config_file_path << " Using default values!!";
	}

	BOOST_LOG_TRIVIAL(info) << "Following settings will be used :";
	BOOST_LOG_TRIVIAL(info) << "    config_file_path \t: " << config_file_path;
	BOOST_LOG_TRIVIAL(info) << "    record \t: " << std::boolalpha << record;
	BOOST_LOG_TRIVIAL(info) << "    store_every_image \t: " << std::boolalpha << store_every_image;
	BOOST_LOG_TRIVIAL(info) << "    Keep dd_exp1 parameter fix \t: " << std::boolalpha << dd_exp1;
	BOOST_LOG_TRIVIAL(info) << "    Keep dd_exp2 parameter fix \t: " << std::boolalpha << dd_exp2;
	BOOST_LOG_TRIVIAL(info) << "    Keep dd_exp3 parameter fix \t: " << std::boolalpha << dd_exp3;
	BOOST_LOG_TRIVIAL(info) << "    Keep dd_scalar1 parameter fix \t: " << std::boolalpha << dd_scalar1;
	BOOST_LOG_TRIVIAL(info) << "    Keep dd_scalar2 parameter fix \t: " << std::boolalpha << dd_scalar2;
	BOOST_LOG_TRIVIAL(info) << "    Keep extrinsic distortion fix \t: " << std::boolalpha << distortion;
	BOOST_LOG_TRIVIAL(info) << "    Keep extrinsic parameter fix \t: " << std::boolalpha << extrinsic;
	BOOST_LOG_TRIVIAL(info) << "    Keep focal_length param fix \t: " << std::boolalpha << focal_length;
	BOOST_LOG_TRIVIAL(info) << "    application_name \t: " << application_name;
	BOOST_LOG_TRIVIAL(info) << "    path_to_setting_file \t: " << path_to_setting_file;
	BOOST_LOG_TRIVIAL(info) << "    calibration_filename \t: " << calibration_filename;
	BOOST_LOG_TRIVIAL(info) << "    serial_no \t: " << serial_no;
	BOOST_LOG_TRIVIAL(info) << "    storage_path \t: " << storage_path;
	BOOST_LOG_TRIVIAL(info) << "    board_files_path \t: " << board_files_path;
	BOOST_LOG_TRIVIAL(info) << "    depth_dependent_exp1 \t: " << std::to_string(depth_dependent_exp1);
	BOOST_LOG_TRIVIAL(info) << "    depth_dependent_exp2 \t: " << std::to_string(depth_dependent_exp2);
	BOOST_LOG_TRIVIAL(info) << "    depth_dependent_exp3 \t: " << std::to_string(depth_dependent_exp3);
	BOOST_LOG_TRIVIAL(info) << "    depth_dependent_scalar1 \t: " << std::to_string(depth_dependent_scalar1);
	BOOST_LOG_TRIVIAL(info) << "    depth_dependent_scalar2 \t: " << std::to_string(depth_dependent_scalar2);
	BOOST_LOG_TRIVIAL(info) << "    laser_dot_threshold \t: " << std::to_string(laser_dot_threshold);
	BOOST_LOG_TRIVIAL(info) << "    images_to_record \t: " << std::to_string(images_to_record);
	BOOST_LOG_TRIVIAL(info) << "    valid_image_count \t: " << std::to_string(valid_image_count);
	BOOST_LOG_TRIVIAL(info) << "    image_interval \t: " << std::to_string(image_interval);
	BOOST_LOG_TRIVIAL(info) << "    doublet_tolerance \t: " << std::to_string(doublet_tolerance);

	po::notify(vm);
	config_stream.close();
}

ApplicationSettings::~ApplicationSettings() = default;
