#include "ImageRecorder/image_class.h"
#include "laser_dot_detector_2d_m.h"

ImageClass::ImageClass(const int bright_pixel_threshold) :
	time_reference_(std::chrono::high_resolution_clock::now()),
	bright_pixel_threshold_(bright_pixel_threshold)
{
}

void ImageClass::AddImage(unsigned char* image,
                          const int rows,
                          const int cols,
                          const size_t counter)
{
	mutex_.lock();
	const auto frame_image = cv::Mat(cols, rows, CV_8U, image);
	const auto now         = std::chrono::high_resolution_clock::now();

	ImageContainer this_image;
	frame_image.copyTo(this_image.image);
	this_image.is_laser     = false; // IsLaserImage();
	this_image.time         = now - time_reference_;
	this_image.counter      = counter;
	this_image.bright_count = 0;
	this_image.saturation   = 0.0;

	images_.emplace(this_image);
	GetImageSaturation(bright_pixel_threshold_);
	mutex_.unlock();
}

void ImageClass::PopImage()
{
	if(images_.size() > 0)
	{
		mutex_.lock();
		images_.pop();
		mutex_.unlock();
	}
}

float ImageClass::ReturnLastDuration()
{
	return images_.front().time.count();
}

float ImageClass::ReturnLastBrightFraction(const int total_number_of_pixels)
{
	return float(images_.front().bright_count) / float(total_number_of_pixels);
}

float ImageClass::ReturnLastSaturation()
{
	return images_.front().saturation;
}

size_t ImageClass::ReturnCounter()
{
	return images_.front().counter;
}

cv::Mat ImageClass::GrabImage(const bool pop)
{
	cv::Mat image;

	if(images_.size())
	{
		mutex_.lock();
		images_.front().image.copyTo(image);

		if(pop)
			images_.pop();

		mutex_.unlock();
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : No Images in images container";
	}

	return image;
}

void ImageClass::GrabCurrentImage(cv::Mat* output)
{
	mutex_.lock();

	if(output && !images_.empty())
	{
		try
		{
			images_.back().image.copyTo(*output);
		}
		catch(...)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Exception in reading image. Container size [" << images_.size() << "]";
		}
	}

	mutex_.unlock();
}

bool ImageClass::IsLaserImage(const int threshold)
{
	images_.front().is_laser = FindDots(images_.front().image, threshold);
	return images_.front().is_laser;
}

float ImageClass::GetImageSaturation(const int threshold)
{
	cv::Mat saturation_mask, threshold_mask;
	cv::threshold(images_.front().image, saturation_mask, 254, 255, cv::THRESH_BINARY);
	cv::threshold(images_.front().image, threshold_mask, threshold, 255, cv::THRESH_BINARY);

	const auto saturation_count = cv::countNonZero(saturation_mask);
	const auto threshold_count  = cv::countNonZero(threshold_mask);

	images_.front().saturation   = double(saturation_count) / double(threshold_count);
	images_.front().bright_count = threshold_count;
	return images_.front().saturation;
}

size_t ImageClass::GetSize() const
{
	return images_.size();
}

bool ImageClass::IsLedImage()
{
	mutex_.lock();

	auto average_image_value = cv::mean(images_.back().image);

	mutex_.unlock();

	return average_image_value[0] > 20;
}

bool ImageClass::FindDots(const cv::Mat& image,
                          const int threshold)
{
	// detect dots in raw image coordinates
	std::vector<cv::Vec2f> dot_coordinates;

	// For dot brightness, to separate bright from dark dots
	DotInfos dot_infos;

	LaserDotDetector2D_M::Params params;
	params.bg_limit = 10;

	LaserDotDetector2D_M ldd0(image, params);
	ldd0.FindLaserDots(dot_coordinates, &dot_infos);

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Dot Coordinates size [" << dot_coordinates.size() << "] threshold [" << threshold << "]";

	return dot_coordinates.size() > threshold;
}
