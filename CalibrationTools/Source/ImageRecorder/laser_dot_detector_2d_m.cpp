#include "tbb/parallel_for.h"
#include <vector>
#include <numeric>

#include "constants.h"
#include "laser_dot_detector_2d_m.h"
#include "sub_pixel_m.h"
using namespace CameraCalibration;

LaserDotDetector2D_M::LaserDotDetector2D_M(const cv::Mat& source_image) :
	source_image_(source_image),
	params_(Params())
{
	InitRingMask(params_.outer_ring, params_.inner_ring);
}

LaserDotDetector2D_M::LaserDotDetector2D_M(const cv::Mat& source_image,
                                           const Params& params) :
	source_image_(source_image),
	params_(params)
{
	InitRingMask(params_.outer_ring, params_.inner_ring);
}

double LaserDotDetector2D_M::FindLaserDots(std::vector<cv::Vec2f>& dots,
                                           DotInfos* dot_infos)
{
	cv::Mat img_cv_32f;
	cv::Mat image_dilated;
	FilterImages(source_image_, img_cv_32f, image_dilated);

	return FindLaserDots(source_image_, img_cv_32f, image_dilated, dots, dot_infos);
}

double LaserDotDetector2D_M::FindLaserDots(const cv::Mat& source_image,
                                           cv::Mat& img_32,
                                           cv::Mat& img_dil,
                                           std::vector<cv::Vec2f>& dots,
                                           DotInfos* dot_infos)
{
	// ideas:
	// reject dots at edges using a circular mask (all pixels within this mask have to me smaller than center pixel
	// do not use img32 for better dynamic range but normalized image
	// mask dark regions early
	// detect all dots and reject bad ones:
	dots.clear();
	dots.reserve(DMVS::CameraCalibration::Constants::MAX_LASER_DOTS);

	// if additional dot infos of sub-pixel fit are requested reserve memory
	if(dot_infos)
	{
		dot_infos->clear();
		dot_infos->reserve(DMVS::CameraCalibration::Constants::MAX_LASER_DOTS);
	}

	// experimental code skipped in normal mode
	// Does not run the ring based filter to verify/reduce detected maxima
	if(UseAllCandidates(img_32, img_dil, dots, dot_infos) == 0.0)
		return 0.0;

	// find local maximum pixels (dilate set each pixel to the max of the neighbor-hood
	// So a pixel with equal value in dilated and original image is a maximum
	// In addition we require that not only the neighbors from dilation kernel but
	// all other pixel within a certain ring mask are darker
	auto candidates = SelectCandidates(img_32, img_dil);

	// Reserve memory for determination of candidate fitness. Avoids synchronization
	// points for parallel computing
	// ATTENTION: keep must be char not bool. a bool vector is not thread safe.
	std::vector<unsigned char> keep(candidates.size(), 0);
	std::vector<cv::Vec2f> dots_local(candidates.size());
	DotInfos dot_infos_local(candidates.size());
	std::vector<double> outer_ring_means(candidates.size(), 0);

	tbb::parallel_for(size_t(0),
	                  candidates.size(),
	                  [&](const size_t i)
	                  //for(auto i = 0; i < candidates.size(); ++i)
	                  {
		                  const auto x   = candidates[i][0];
		                  const auto y   = candidates[i][1];
		                  const auto xy0 = img_32.ptr<float>(y)[x];

		                  // get the mean value and standard deviation of the outer ring centered to the candidate pixel
		                  const auto patch = source_image(cv::Rect(x - params_.outer_ring, y - params_.outer_ring, ring_.cols, ring_.rows));
		                  cv::Scalar mean, std_deviation;
		                  cv::meanStdDev(patch, mean, std_deviation, ring_);

		                  outer_ring_means[i] = mean[0];
		                  // the peak must be significantly brighter than the pixels in an outer ring around the dot.
		                  // Significantly means it depends on the noise of the outer-ring pixels and the delta in brightness
		                  // check peak is brighter than mean + a factor times the standard deviation of the outer ring
		                  if(xy0 > mean[0] + params_.mean_peak_factor * std_deviation[0])
		                  {
			                  // calculate sub-pixel accuracy and sub-pixel fit statistics (dotInfos)
			                  auto peak = SubPixel::subPixel(img_32, cv::Point(x, y), params_.sub_pixel_patch_size);

			                  if(peak[5] == FLT_MAX)
				                  return;

			                  // mark candidate as good and keep it
			                  keep[i]            = 1;
			                  dots_local[i]      = cv::Vec2f(peak[0], peak[1]);
			                  dot_infos_local[i] = cv::Vec6f(peak[2], peak[3], peak[4], peak[5], peak[6], 0.f);

			                  return;
		                  }

		                  // we have problems with points on or close to texture edges because
		                  // the stddev of the outer ring is to big therefore the maximum is not bright enough
		                  // test a separation in two groups brighter/darker than mean and calculate
		                  // standard deviation of each group. Take the bigger one
		                  if(params_.edge_dot_detection)
		                  {
			                  cv::Mat mask_dark, mask_bright;
			                  cv::threshold(patch, mask_bright, mean[0], 255, cv::THRESH_BINARY);
			                  cv::threshold(patch, mask_dark, mean[0], 255, cv::THRESH_BINARY_INV);
			                  cv::min(mask_bright, ring_, mask_bright);
			                  cv::min(mask_dark, ring_, mask_dark);

			                  cv::Scalar dark_mean, dark_std_deviation, bright_mean, bright_std_deviation;
			                  {
				                  // PROFILE_BLOCK( post_calculation_dots)
				                  cv::meanStdDev(patch, dark_mean, dark_std_deviation, mask_dark);
				                  cv::meanStdDev(patch, bright_mean, bright_std_deviation, mask_bright);
			                  }

			                  if((xy0 > bright_mean[0] + params_.mean_peak_factor * bright_std_deviation[0]) &&
				                  (xy0 > dark_mean[0] + params_.mean_peak_factor * dark_std_deviation[0]) &&
				                  (bright_mean[0] / dark_mean[0] > 1.5f))
			                  {
				                  cv::Vec<float, 7> peak = SubPixel::subPixel(img_32, candidates[i], params_.sub_pixel_patch_size);

				                  if(peak[5] == FLT_MAX)
					                  return;

				                  keep[i]            = 1;
				                  dots_local[i]      = cv::Vec2f(peak[0], peak[1]);
				                  dot_infos_local[i] = cv::Vec6f(peak[2], peak[3], peak[4], peak[5], peak[6], 0.f);
				                  return;
			                  }
		                  }
	                  });
	//); // loop candidates

	// Assemble result after parallel calculation. This way we do not need synchronization
	// points in the multi-threaded calculation
	for(auto i = 0; i < keep.size(); ++i)
	{
		if(keep[i])
		{
			dots.emplace_back(std::move(dots_local[i]));

			if(dot_infos)
			{
				dot_infos->emplace_back(std::move(dot_infos_local[i]));
			}
		}
	}

	auto outer_ring_mean_mean = 0.0;
	outer_ring_mean_mean      = std::accumulate(outer_ring_means.begin(), outer_ring_means.end(), outer_ring_mean_mean);
	outer_ring_mean_mean /= double(outer_ring_means.size());

	// return the mean value of outer ring means as indicator for overall brightness of image
	return outer_ring_mean_mean;
}

std::vector<cv::Vec2i> LaserDotDetector2D_M::SelectCandidates(cv::Mat& image_cv_32f,
                                                              cv::Mat& image_dilate) const
{
	std::vector<cv::Vec2i> candidates;

	if(image_cv_32f.empty())
		return candidates;

	if(image_cv_32f.size() != image_dilate.size())
		return candidates;

	// create a byte image indicating candidate pixels
	cv::Mat candidate_mask(image_cv_32f.size(), CV_8UC1, cv::Scalar(0x0));

	const auto radius = params_.outer_ring;
	const auto min_x  = radius + 2;
	const auto max_x  = image_cv_32f.cols - radius - 2;
	const auto min_y  = radius + 2;
	const auto max_y  = image_cv_32f.rows - radius - 2;

	tbb::parallel_for(min_y,
	                  max_y,
	                  [&](const int y)
	                  //for(auto y = min_y; y < max_y; ++y)
	                  {
		                  for(auto x = min_x; x < max_x; ++x)
		                  {
			                  const auto xy0 = image_cv_32f.ptr<float>(y)[x];
			                  const auto xy1 = image_dilate.ptr<float>(y)[x];

			                  if(std::abs(xy0 - xy1) < 0.0000001 && xy0 > params_.bg_limit)
			                  {
				                  auto is_max = true;

				                  // check neighborhood within the ring mask
				                  for(auto i = y - radius; i <= y + radius && is_max; ++i)
				                  {
					                  for(auto j = x - radius; j <= x + radius; ++j)
					                  {
						                  // considered pixel is darker, max crit not hurt
						                  if(image_cv_32f.ptr<float>(i)[j] < xy0)
							                  continue;

						                  // considered pixel equal brightness - depends where it is
						                  if(image_cv_32f.ptr<float>(i)[j] == xy0)
						                  {
							                  // its the direct bottom neighbor or
							                  // one of the three right neighbors
							                  if((i - y == 1 && j == x) ||
								                  (i - y == -1 || i - y == 0 || i - y == 1) &&
								                  (j - x == 1))
							                  {
								                  continue;
							                  }

							                  if(i == y && j == x)
								                  continue;

							                  is_max = false;
							                  break;
						                  }
						                  else
						                  {
							                  // considered pixel brighter - not a maximum
							                  is_max = false;
							                  break;
						                  }
					                  }
				                  }

				                  // mark pixel as candidate pixel
				                  if(is_max)
				                  {
					                  candidate_mask.ptr<unsigned char>(y)[x] = 0xff;
				                  }
			                  }
		                  }
	                  });

	// create a list of all non - zero pixels candidate pixels
	cv::findNonZero(candidate_mask, candidates);


	return candidates;
}

std::vector<cv::Vec2i> LaserDotDetector2D_M::SelectCandidates2(cv::Mat& img32,
                                                               cv::Mat& img_dil)
{
	std::vector<cv::Vec2i> candidates;

	if(img32.empty())
		return candidates;
	if(img32.size() != img_dil.size())
		return candidates;


	// create a byte image indicating candidate pixels
	cv::Mat candMask(img32.size(), CV_8UC1, cv::Scalar(0x0));

	int radius = params_.outer_ring;
	int minx   = radius + 2;
	int maxx   = img32.cols - radius - 2;
	int miny   = radius + 2;
	int maxy   = img32.rows - radius - 2;

	img32.forEach<float>([&](float& xy0,
	                         const int yx[])
	{
		if(miny > yx[0] || yx[0] >= maxy || minx > yx[1] || yx[1] >= maxx)
			return;

		if(xy0 != img_dil.at<float>(yx) || xy0 <= params_.bg_limit)
			return;

		for(int i = yx[0] - radius; i <= yx[0] + radius; ++i)
			for(int j = yx[1] - radius; j <= yx[1] + radius; ++j)
			{
				float xyr = img32.at<float>(i, j);

				// considered pixel is darker, max crit not hurt
				if(xyr < xy0)
					continue;

				// considered pixel brighter - not a maximum
				if(xyr != xy0)
					return;

				// considered pixel equal brightness - depends where it is
				if( // its the direct bottom neighbor or
					((i - yx[0] == 1) && (j == yx[1])) ||
					// one of the three right neighbors
					(((i - yx[0] == -1) || (i - yx[0] == 0) || (i - yx[0] == 1)) && (j - yx[1] == 1))
				)
					continue;

				// Same pixel
				if(i == yx[0] && j == yx[1])
					continue;

				// not a maximum
				return;
			}

		// all tests passed - mark pixel as candidate pixel
		candMask.at<uchar>(yx) = 0xff;
	});

	cv::findNonZero(candMask, candidates);

	return candidates;
}

double LaserDotDetector2D_M::UseAllCandidates(cv::Mat& img32,
                                              cv::Mat& img_dil,
                                              std::vector<cv::Vec2f>& dots,
                                              DotInfos* dot_infos) const
{
	if(params_.use_all_candidates)
	{
		for(int y = params_.outer_ring + 2; y < source_image_.rows - params_.outer_ring - 2; ++y)
		{
			for(int x = params_.outer_ring + 2; x < source_image_.cols - params_.outer_ring - 2; ++x)
			{
				float xy0 = img32.at<float>(y, x);
				if(xy0 == img_dil.at<float>(y, x) && xy0 > params_.bg_limit)
				{
					cv::Vec<float, 7> peak = SubPixel::subPixel(img32, cv::Point(x, y), params_.sub_pixel_patch_size);
					if(peak[5] == FLT_MAX)
						continue;

					dots.emplace_back(cv::Vec2f(peak[0], peak[1]));

					if(dot_infos)
						dot_infos->emplace_back(cv::Vec6f(peak[2], peak[3], peak[4], peak[5], peak[6], 0.f));
				}
			}
		}
		return 0.0;
	}

	return 1.0;
}

void LaserDotDetector2D_M::SetRingMask(const int outer,
                                       const int inner)
{
	InitRingMask(outer, inner);
}

void LaserDotDetector2D_M::FilterImages(const cv::Mat& source_image,
                                        cv::Mat& image_cv_32f,
                                        cv::Mat& image_dilated) const
{
	source_image.convertTo(image_cv_32f, CV_32F);

	if(params_.dot_blur > 0 && image_cv_32f.rows != 0 && image_cv_32f.cols != 0)
	{
		GaussianBlur(image_cv_32f, image_cv_32f, cv::Size(params_.dot_blur, params_.dot_blur), params_.sigma_dot_blur, params_.sigma_dot_blur);
	}

	if(!image_cv_32f.empty())
	{
		const auto dilation_size = 2;
		const auto element       = getStructuringElement(cv::MORPH_RECT,
		                                                 cv::Size(2 * dilation_size + 1, 2 * dilation_size + 1),
		                                                 cv::Point(dilation_size, dilation_size));
		cv::dilate(image_cv_32f, image_dilated, element);
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Image is empty";
	}
	// create dilated image to find local maxima
	// kernel for finding local maxima
}

void LaserDotDetector2D_M::InitRingMask(int outer_radius,
                                        const int inner_radius)
{
	//ring mask used to calculate signal around dots (background signal)
	int sqOuter = outer_radius * outer_radius;
	int sqInner = inner_radius * inner_radius;

	ring_ = cv::Mat::zeros(outer_radius * 2 + 1, outer_radius * 2 + 1, CV_8U);

	ring_.forEach<uchar>([&](uchar& m,
	                         const int yx[])
	{
		int radius = (yx[0] - outer_radius) * (yx[0] - outer_radius) + (yx[1] - outer_radius) * (yx[1] - outer_radius);
		if(sqInner <= radius && radius <= sqOuter)
			m = 0xff;
	});

	return;
}
