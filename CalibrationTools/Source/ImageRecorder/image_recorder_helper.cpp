#include "image_recorder_helper.h"

namespace ImageRecorder
{
std::string GetConfigFileName(dmvs_sdk::DMVSDevice* device,
                              const std::string prefix)
{
	auto path = boost::filesystem::path(prefix + "\\" + device->GetSerialNumber() + "-Settings.json");

	// Make Absolute
	if(path.is_relative())
	{
		path = boost::filesystem::absolute(path);
	}

	return path.make_preferred().string();
}

bool SaveSettings(dmvs_sdk::DMVSDevice* device,
                  const std::string prefix)
{
	auto error_occurred      = false;
	const auto settings_file = GetConfigFileName(device, prefix);
	const auto error_code    = device->SaveParameters(settings_file.c_str());

	if(error_code != dmvs_sdk::error::OK)
	{
		error_occurred = true;
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << " ERROR : " << dmvs_sdk::ErrorCodeToString(error_code);
	}

	return error_occurred;
}

bool LoadSettings(dmvs_sdk::DMVSDevice* device,
                  const std::string prefix)
{
	auto error_occurred = false;

	const auto settings_file = GetConfigFileName(device, prefix);
	//LOG(INFO) << "Restoring settings of DMVS " << device->getSerialNumber() << " ...";
	const auto error_code = device->LoadParameters(settings_file.c_str());

	if(error_code != dmvs_sdk::error::OK)
	{
		error_occurred = true;
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << " Could not reset configuration! " << dmvs_sdk::ErrorCodeToString(error_code);
	}

	return error_occurred;
}
}
