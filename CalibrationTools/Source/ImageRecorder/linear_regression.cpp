#include "linear_regression.h"
#include "statistics_calculations.h"

LinearRegression::LinearRegression(std::vector<double>& x_values,
                                   std::vector<double>& y_values) :
	slope_(0),
	offset_(0),
	x_(x_values),
	y_(y_values)
{
	CalculateRegression();
}

double LinearRegression::GetSlope() const
{
	return slope_;
}

double LinearRegression::GetOffset() const
{
	return offset_;
}

double LinearRegression::GetRSquared() const
{
	return r_squared_;
}

double LinearRegression::GetResidualsRMS() const
{
	return residual_rms_;
}

int LinearRegression::CalculateRegression()
{
	StatisticsCalculations x_statistics(x_);
	StatisticsCalculations y_statistics(y_);

	if(x_.size() != y_.size())
	{
		return 1;
	}
	std::vector<double> values;
	const auto x_average = x_statistics.CalculateAverage(values);
	const auto y_average = y_statistics.CalculateAverage(values);

	auto slope_a = 0.0;
	auto slope_b = 0.0;

	for(auto k = 0; k < x_.size(); ++k)
	{
		slope_a += (x_[k] - x_average) * (y_[k] - y_average);
		slope_b += (x_[k] - x_average) * (x_[k] - x_average);
	}

	slope_  = slope_a / slope_b;
	offset_ = y_average - slope_ * x_average;

	//create list of values for each measured x value
	y_fit_.resize(y_.size());
	residuals_.resize(y_.size());
	auto r_sq_a = 0.0;
	auto r_sq_b = 0.0;

	for(auto k = 0; k < x_.size(); ++k)
	{
		y_fit_[k]     = offset_ + slope_ * x_[k];
		residuals_[k] = y_[k] - y_fit_[k];

		r_sq_a += residuals_[k] * residuals_[k];
		r_sq_b += (y_[k] - y_average) * (y_[k] - y_average);
	}

	std::vector<double> values2;
	StatisticsCalculations residual_statistics(residuals_);
	residual_rms_ = residual_statistics.RMS(values2);
	r_squared_    = 1 - r_sq_a / r_sq_b;
	return 0;
}
