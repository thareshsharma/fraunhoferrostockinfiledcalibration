#include "statistics_calculations.h"

StatisticsCalculations::StatisticsCalculations(std::vector<double>& input) :
	data_(input),
	rms_(-1.0),
	average_(-1.0),
	median_(-1.0),
	sigma_{{0, 0, 0, 0}}
{
}

double StatisticsCalculations::CalculateAverage(std::vector<double>& values)
{
	auto tmp_average     = 0.0;
	auto store_to_member = false;

	if(values.empty())
	{
		values          = data_;
		store_to_member = true;
	}

	for(auto value : values)
	{
		tmp_average += value;
	}

	if(store_to_member)
	{
		average_ = tmp_average / data_.size();
	}

	return tmp_average / data_.size();
}

std::array<double, 4> StatisticsCalculations::CalculateSigma(std::vector<double>& values,
                                                             const double offset)
{
	bool store_to_member = false;

	if(values.empty())
	{
		values          = data_;
		store_to_member = true;
	}

	std::array<double, 4> s     = {0.6827, 0.9545, 0.9973, 0.99994};
	std::array<double, 4> sigma = {0.0, 0.0, 0.0, 0.0};

	auto v = values;

	if(std::abs(offset) > 1e-8)
	{
		for(auto& vv : v)
		{
			vv = std::abs(vv - offset);
		}
	}

	std::sort(v.begin(), v.end());

	for(auto is = 0; is < s.size(); is++)
	{
		const auto index = int(s.at(is) * v.size());
		sigma.at(is)     = index >= v.size() ? 0.0 : v.at(index);
	}

	if(store_to_member)
	{
		sigma_ = sigma;
	}

	return sigma;
}

double StatisticsCalculations::CalculateSigma(const int sigma,
                                              std::vector<double>& values)
{
	auto sigma_data = CalculateSigma(values);
	return sigma_data[sigma];
}

std::array<double, 4> StatisticsCalculations::CalculateSigmaToMedian()
{
	return CalculateSigma(data_, median_);
}

double StatisticsCalculations::CalculateMedian(std::vector<double>& values)
{
	auto store_to_member = false;

	if(values.empty())
	{
		values          = data_;
		store_to_member = true;
	}

	//1. sort data vector
	auto values_sorted = values;
	std::sort(values_sorted.begin(), values_sorted.end());

	auto result = 0.0;

	if(values_sorted.size() % 2 == 0)
	{
		//if vector has even length
		const auto v1 = values_sorted.at(values_sorted.size() / 2 - 1);
		const auto v2 = values_sorted.at(values_sorted.size() / 2);
		result        = (v1 + v2) / 2;
	}
	else
	{
		//if vector has odd length
		result = values_sorted.at((values_sorted.size() + 1) / 2 - 1);
	}

	if(store_to_member)
	{
		median_ = result;
	}

	return result;
}

double StatisticsCalculations::RMS(std::vector<double>& values)
{
	auto store_to_member = false;

	if(values.empty())
	{
		values          = data_;
		store_to_member = true;
	}

	std::vector<double> squared_list;
	squared_list.reserve(values.size());

	for(auto value : values)
	{
		squared_list.emplace_back(value * value);
	}

	const auto result = std::sqrt(CalculateAverage(squared_list));

	if(store_to_member)
		rms_ = result;

	return result;
}
