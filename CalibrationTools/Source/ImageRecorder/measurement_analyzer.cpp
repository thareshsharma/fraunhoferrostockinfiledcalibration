#include "measurement_analyzer.h"

void MeasurementAnalyzer::AddMainValues(long position,
                                        std::vector<double> values)
{
	//the vector expects the current radius, one entry for each segment
	std::pair<long, std::vector<double>> entry = {position, values};
	main_measurements_.insert(entry);

	if(main_measurements_.size() > 1)
	{
		//loop over the horizontal segment values
		for(auto k = 0; k < values.size(); ++k)
		{
			// if a new value is smaller, it resets the current position storage
			if(values[k] < minimum_radius_[k].value * (1 - minimum_tolerance_))
			{
				minimum_radius_[k].value = values[k];
				minimum_radius_[k].positions_with_minimum.resize(1);
				minimum_radius_[k].positions_with_minimum.push_back(position);
			}

			// if the value is within a certain tolerance equal to the minimum, add its position
			if(values[k] <= minimum_radius_[k].value * (1 + minimum_tolerance_))
			{
				minimum_radius_[k].positions_with_minimum.push_back(position);
			}
		}
	}
	else
	{
		for(auto value : values)
		{
			MinimumContainer tmp;
			tmp.value = value;
			tmp.positions_with_minimum.push_back(position);
			minimum_radius_.push_back(tmp);
		}
	}
}
