#include "polynomial_least_squares.h"
#include "statistics_calculations.h"

PolynomialLeastSquares::PolynomialLeastSquares(std::vector<double>& x_values,
                                               std::vector<double>& y_values,
                                               const int degree) :
	x_(x_values),
	y_(y_values),
	offset_{0},
	minimum_position_{0},
	quadratic_component_(0),
	residual_rms_(0),
	degree_(degree)
{
	CalculateLeastSquares();
}

int PolynomialLeastSquares::GetQuadraticComponents(double& quadratic,
                                                   double& position,
                                                   double& offset) const
{
	// If not quadratic, there will be no component
	if(degree_ != 2)
	{
		return -1;
	}

	quadratic = quadratic_component_;
	offset    = offset_;
	position  = minimum_position_;

	return 0;
}

void PrintMatrix(std::vector<std::vector<double>> matrix)
{
	for(const auto& matrix_row : matrix)
	{
		std::stringstream row_entries;
		for(auto row_entry : matrix_row)
		{
			row_entries << row_entry << " ";
		}

		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << row_entries.str();
	}
}

int PolynomialLeastSquares::CalculateLeastSquares()
{
	//code from https://www.bragitoff.com/2015/09/c-program-for-polynomial-fit-least-squares/
	//Array that will store the values of sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
	std::vector<double> x(2 * degree_ + 1);

	for(auto i = 0; i < 2 * degree_ + 1; i++)
	{
		x[i] = 0;
		//consecutive positions of the array will store N,sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
		for(auto j = 0; j < x_.size(); j++)
		{
			x[i] = x[i] + pow(x_[j], i);
		}
	}

	std::vector<std::vector<double>> b(degree_ + 1);

	for(auto i = 0; i <= degree_; i++)
	{
		std::vector<double> b_line(degree_ + 2);

		for(auto j = 0; j <= degree_; j++)
		{
			b_line[j] = x[i + j];
		}

		b[i] = b_line;
	}

	std::vector<double> y(degree_ + 1);

	for(auto i = 0; i < degree_ + 1; i++)
	{
		y[i] = 0;
		//consecutive positions will store sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
		for(auto j = 0; j < x_.size(); j++)
		{
			y[i] = y[i] + pow(x_[j], i) * y_[j];
		}
	}

	//load the values of Y as the last column of B(Normal Matrix but augmented)
	for(auto i = 0; i <= degree_; i++)
	{
		b[i][degree_ + 1] = y[i];
	}

	const auto degree_internal = degree_ + 1;

	//PrintMatrix(B);

	//From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
	for(auto i = 0; i < degree_internal; i++)
	{
		for(auto k = i + 1; k < degree_internal; k++)
		{
			if(fabs(b[i][i]) < fabs(b[k][i]))
			{
				for(auto j = 0; j <= degree_internal; j++)
				{
					const auto temp = b[i][j];
					b[i][j]         = b[k][j];
					b[k][j]         = temp;
				}
			}
		}
	}

	/*PrintMatrix(B);*/
	//loop to perform the gauss elimination
	for(auto i = 0; i < degree_internal; i++)
	{
		for(auto k = i + 1; k < degree_internal; k++)
		{
			const auto t = b[k][i] / b[i][i];
			//make the elements below the pivot elements equal to zero or eliminate the variables
			for(auto j = 0; j <= degree_internal; j++)
			{
				b[k][j] = b[k][j] - t * b[i][j];
			}
		}
	}

	//PrintMatrix(B);
	estimated_parameters_.resize(degree_internal);
	for(auto i = degree_; i >= 0; i--) //back-substitution
	{
		//x is an array whose values correspond to the values of x,y,z..
		//make the variable to be calculated equal to the rhs of the last equation
		estimated_parameters_[i] = b[i][degree_internal];

		for(auto j = i; j < degree_internal; j++)
		{
			//then subtract all the lhs values except the coefficient of the variable whose value is being calculated
			if(j != i)
			{
				estimated_parameters_[i] = estimated_parameters_[i] - b[i][j] * estimated_parameters_[j];
			}
		}

		//now finally divide the rhs by the coefficient of the variable to be calculated
		estimated_parameters_[i] = estimated_parameters_[i] / b[i][i];
	}

	//std::cout << "Estimated Parameters: ";
	//for (auto value : parameter)
	//{
	//	std::cout << value << " ";
	//}
	//std::cout << std::endl;

	if(degree_ == 2)
	{
		quadratic_component_ = estimated_parameters_[2];
		minimum_position_    = -estimated_parameters_[1] / (2 * quadratic_component_);
		offset_              = estimated_parameters_[0] + quadratic_component_ * minimum_position_ * minimum_position_;
	}

	CalculateResiduals();

	return 0;
}

int PolynomialLeastSquares::CalculateResiduals()
{
	StatisticsCalculations x_statistics(x_);
	StatisticsCalculations y_statistics(y_);

	std::vector<double> values;
	auto y_average = y_statistics.CalculateAverage(values);

	calculated_values_.resize(x_.size());
	residuals_.resize(x_.size());

	for(auto k = 0; k < x_.size(); ++k)
	{
		calculated_values_[k] = 0;

		for(auto d = 0; d < degree_; ++d)
		{
			calculated_values_[k] += std::pow(x_[k], d) * estimated_parameters_[d];
		}

		residuals_[k] = y_[k] - calculated_values_[k];
	}

	std::vector<double> values2;
	StatisticsCalculations residual_statistics(residuals_);
	residual_rms_ = residual_statistics.RMS(values2);

	return 0;
}
