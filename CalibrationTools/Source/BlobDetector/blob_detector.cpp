#include "blob_detector.h"
#include "current_center_compare.h"
#include "ring_code_decoder.h"
#include "constants.h"
#include "helper.h"
#include "ellipse_optimizer.h"

namespace DMVS
{
namespace CameraCalibration
{
using namespace boost::accumulators;

BlobDetector::BlobDetectorParams::BlobDetectorParams()
{
	min_area                                     = Constants::BLOB_DETECTION_MIN_AREA;
	max_area                                     = Constants::BLOB_DETECTION_MAX_AREA;
	threshold_step                               = Constants::BLOB_DETECTION_THRESHOLD_STEP;
	min_threshold                                = Constants::BLOB_DETECTION_MIN_THRESHOLD;
	max_threshold                                = Constants::BLOB_DETECTION_MAX_THRESHOLD;
	min_repeatability                            = Constants::BLOB_DETECTION_MIN_REPEATABILITY;
	min_distance_between_blobs                   = Constants::BLOB_DETECTION_MIN_DISTANCE_BETWEEN_BLOBS;
	set_id_by_color                              = Constants::BLOB_DETECTION_SET_ID_BY_COLOR;
	blob_color                                   = Constants::BLOB_DETECTION_BLOB_COLOR;
	filter_by_area                               = true;
	filter_by_circularity                        = false;
	min_circularity                              = 0.80000f;
	max_circularity                              = std::numeric_limits<float>::max();
	filter_by_inertia                            = true;
	min_inertia_ratio                            = 0.1000001f;
	max_inertia_ratio                            = std::numeric_limits<float>::max();
	filter_by_convexity                          = true;
	min_convexity                                = 0.94999988f;
	max_convexity                                = std::numeric_limits<float>::max();
	filter_by_ring_in_ring                       = false;
	min_viewing_angle                            = Constants::BLOB_DETECTION_MIN_VIEWING_ANGLE;
	white_circle_id                              = Constants::BLOB_DETECTION_WHITE_CIRCLE_ID;
	black_circle_id                              = Constants::BLOB_DETECTION_BLACK_CIRCLE_ID;
	refine_ellipses                              = Constants::BLOB_DETECTION_REFINE_ELLIPSES;
	decode_ring_marker                           = Constants::BLOB_DETECTION_DECODE_RING_MARKER;
	coded_only                                   = false;
	percentage_agreement_with_artificial_ellipse = 1.00;
	filter_ellipse_width                         = 2.0;
	scale                                        = 1.0;
}

BlobDetector::BlobDetector(const BlobDetectorParams& params)
{
	params_ = params;
}

BlobDetector::BlobDetector(const BlobDetector& rhs)
{
	if(this != &rhs)
	{
		params_ = rhs.params_;
	}
}

BlobDetector& BlobDetector::operator=(const BlobDetector& rhs)
{
	if(this != &rhs)
	{
		params_ = rhs.params_;
	}

	return *this;
}

BlobDetector::~BlobDetector() = default;

void BlobDetector::DetectBlob(const cv::Mat& image,
                              CircularMarkerVector& key_points) const
{
	//TODO: support mask
	key_points.clear();
	cv::Mat gray_scale_image;

	if(image.channels() == 3)
	{
		gray_scale_image = cv::Mat::zeros(image.size(), CV_8U);

		for(auto row = 0; row < image.rows; ++row)
		{
			for(auto col = 0; col < image.cols; ++col)
			{
				gray_scale_image.at<uchar>(row, col) = image.at<cv::Vec3b>(row, col)[1];
			}
		}
	}
	else
	{
		gray_scale_image = image;
	}

	EllipseFitOptimizer ellipse_optimizer(gray_scale_image);
	std::vector<CircularMarkerVector> centers;
	std::multiset<CircularMarkerVector, CircularMarkerComparator> all_cur_centers;
	cv::Mat eroded_gray_img;
	cv::erode(gray_scale_image, eroded_gray_img, cv::Mat());
	cv::Mat contour_search_img;

	if(params_.scale != 1.0)
		cv::resize(eroded_gray_img,
		           contour_search_img,
		           cv::Size(),
		           params_.scale,
		           params_.scale,
		           params_.scale < 1.0 ? cv::INTER_AREA : cv::INTER_LANCZOS4);

	if(params_.max_threshold - params_.min_threshold > params_.threshold_step)
	{
#if !defined(DEBUG) && !defined(_LINE_INFO)
		//#pragma omp parallel for
#endif
		for(auto thresh = params_.min_threshold; thresh < params_.max_threshold;
		    thresh += params_.threshold_step)
		{
			CircularMarkerVector cur_centers;
			FindBlobs(eroded_gray_img, contour_search_img, thresh, cur_centers);

#if !defined(DEBUG) && !defined(_LINE_INFO)
			//#pragma omp critical
#endif
			{
				// Blob detector crashes without the braces. But code should
				// the the same with and without braces.
				all_cur_centers.insert(cur_centers);
			}
		}
	}
	else
	{
		CircularMarkerVector current_centers;
		FindBlobs(eroded_gray_img, contour_search_img, 0, current_centers);
		all_cur_centers.insert(current_centers);
	}

	for(const auto& cur_centers : all_cur_centers)
	{
		std::vector<CircularMarkerVector> new_centers;

		for(auto i = 0; i < cur_centers.size(); ++i)
		{
			bool is_new = true;
			for(auto j = 0; j < centers.size(); ++j)
			{
				//double dist = cv::norm(centers[j][ centers[j].size() / 2 ].raw_ellipse.center - curCenters[i].raw_ellipse.center );
				//get mean dist of this center
#if 1
				cv::Point2f mean_center = std::accumulate(centers[j].begin(),
				                                          centers[j].end(),
				                                          cv::Point2f(0., 0.),
				                                          [](cv::Point2f in,
				                                             decltype(centers[j].front()) next) -> cv::Point2f
				                                          {
					                                          return in + next.raw_ellipse.center;
				                                          });

				mean_center *= 1. / centers[j].size();

#else
				const cv::Point2f meanCenter = centers[j][0].raw_ellipse.center / float(centers[j].size());
#endif
				double dist = cv::norm(mean_center - cur_centers[i].raw_ellipse.center);

				is_new = (dist >= params_.min_distance_between_blobs);

				if(!is_new)
				{
					//center is not new. add it to the list of already assigned detections of this center
					centers[j].push_back(cur_centers[i]);
#if 0
					//for mean calculation: add it to the first entry
					centers[j][0].raw_ellipse.center = centers[j][0].raw_ellipse.center + curCenters[i].raw_ellipse.center;
#endif

					auto k = centers[j].size() - 1;

					while(k > 1 && centers[j][k].radius < centers[j][k - 1].radius)
					{
						centers[j][k] = centers[j][k - 1];
						k--;
					}
					centers[j][k] = cur_centers[i];

					break;
				}
			}
			if(is_new)
			{
#if 1
				new_centers.push_back(CircularMarkerVector(1, cur_centers[i]));
#else
				//create the first one twice. first entry is to keep track of the sum of the centers (for mean calculation)
				//second one to store this center
				new_centers.push_back(std::vector< CircularMarker>(2, curCenters[i]));
#endif
			}
		}

		std::copy(new_centers.begin(), new_centers.end(), std::back_inserter(centers));
	}

	// create a ring code decoder if detector should only detect coded marker
	std::unique_ptr<RingCodeDecoder> ring_code_decoder;

	if(params_.decode_ring_marker)
		ring_code_decoder = std::make_unique<RingCodeDecoder>(image, 6.0f, 12);

	for(auto& center : centers)
	{
		if(center.size() < params_.min_repeatability)
			continue;

		CircularMarker mean_center;
		mean_center.confidence         = 0.0;
		mean_center.radius             = 0.0;
		mean_center.raw_ellipse.angle  = 0.0f;
		mean_center.raw_ellipse.center = cv::Point2f(0.0f, 0.0f);
		mean_center.raw_ellipse.size   = cv::Size2f(0.0f, 0.0f);

		float weight_sum = 0.0;
		std::map<size_t, size_t> class_ids;

		accumulator_set<double, features<tag::mean, tag::median>> x, y, angle, width, height, radius;
		auto use_statistics = center.size() > 3;

		//std::vector< float > angles, widths, heights;
		for(auto j = 0; j < center.size(); ++j)
		{
			auto angle_diff = center[j].raw_ellipse.angle - center[0].raw_ellipse.angle;

			//make sure that 0 to 180 deg jumps are handled correctly for averaging
			//angles are 0..180 by find blobs
			if(fabs(angle_diff) > 160.0)
			{
				if(center[0].raw_ellipse.angle < 20)
					center[j].raw_ellipse.angle -= 180.0f;
				else
					center[j].raw_ellipse.angle += 180.0f;
			}

			auto weight = float(center[j].confidence);

			//count the single center class ID
			auto marker_id = center[j].id;

			if(class_ids.find(marker_id) == class_ids.end())
				class_ids[marker_id] = 0;

			class_ids[marker_id]++;

			if(use_statistics)
			{
				x(center[j].raw_ellipse.center.x);
				y(center[j].raw_ellipse.center.y);
				angle(center[j].raw_ellipse.angle);
				width(center[j].raw_ellipse.size.width);
				height(center[j].raw_ellipse.size.height);
				radius(center[j].radius);
			}
			else
			{
				mean_center.raw_ellipse.center.x += center[j].raw_ellipse.center.x * weight;
				mean_center.raw_ellipse.center.y += center[j].raw_ellipse.center.y * weight;
				mean_center.raw_ellipse.angle += center[j].raw_ellipse.angle * weight;
				mean_center.raw_ellipse.size.width += center[j].raw_ellipse.size.width * weight;
				mean_center.raw_ellipse.size.height += center[j].raw_ellipse.size.height * weight;
				mean_center.radius = std::max(mean_center.radius, center[j].radius);
				weight_sum += weight;
			}
		}
		if(use_statistics)
		{
			mean_center.raw_ellipse.center.x    = float(median(x));
			mean_center.raw_ellipse.center.y    = float(median(y));
			mean_center.raw_ellipse.angle       = float(median(angle));
			mean_center.raw_ellipse.size.width  = float(median(width));
			mean_center.raw_ellipse.size.height = float(median(height));
			mean_center.radius                  = float(median(radius));
		}
		else
		{
			mean_center.raw_ellipse.center *= 1.0f / weight_sum;
			mean_center.raw_ellipse.angle *= 1.0f / weight_sum;
			mean_center.raw_ellipse.size.width *= 1.0f / weight_sum;
			mean_center.raw_ellipse.size.height *= 1.0f / weight_sum;
		}
		mean_center.confidence = 1.0;

		//check width and height not NaN and positive
		if(mean_center.raw_ellipse.size.width != mean_center.raw_ellipse.size.width ||
			mean_center.raw_ellipse.size.height != mean_center.raw_ellipse.size.height ||
			mean_center.raw_ellipse.size.height < 0.001 ||
			mean_center.raw_ellipse.size.width < 0.001)
		{
			continue;
		}

		//Averaging can cause angles out of 0...180 range shift angle to range again
		while(mean_center.raw_ellipse.angle >= 180.0f)
			mean_center.raw_ellipse.angle -= 180.0f;

		while(mean_center.raw_ellipse.angle < 0.0f)
			mean_center.raw_ellipse.angle += 180.0f;

		mean_center.id = -1;

		for(auto it = class_ids.begin(); it != class_ids.end(); ++it)
		{
			//check if the circle has a stable class ID
			if(it->second > center.size() / 2)
				mean_center.id = it->first;
		}

		//if gradient based refinement is active do not use key points where refinement failed
		//cv::RotatedRect out;
		//auto ellipse_fit_success = true;
		//auto num_iterations = 0;

		//for(auto i = 0; i < params_.refine_ellipses; ++i)
		//{
		//	out = ellipse_optimizer.optimize(mean_center.raw_ellipse);
		//	ellipse_fit_success = ellipse_optimizer.success();
		//	auto converged = cv::norm(out.center - mean_center.raw_ellipse.center) < 0.001;
		//	num_iterations++;
		//	mean_center.raw_ellipse = out;

		//	if(converged || !ellipse_fit_success)
		//		break;
		//}

		//if(!ellipse_fit_success)
		//{
		//	continue;
		//}

		//check viewing angle (ellipse axes ratio) after refinement
		const auto min_ratio = cos(Helper::AngleInRadians(params_.min_viewing_angle));

		//if(out.size.height / out.size.width < min_ratio || out.size.height / out.size.width > 1.0 / min_ratio)
		//{
		//	continue;
		//}

		if(ring_code_decoder)
		{
			double outer_ring_scale;
			double outer_ring_scale1;
			double outer_ring_scale2;
			auto id = ring_code_decoder->GetMarkerId(mean_center.CorrectedEllipse(),
			                                         outer_ring_scale,
			                                         outer_ring_scale1,
			                                         outer_ring_scale2);

			if(id > 0)
			{
				mean_center.id                 = id + 1000;
				mean_center.outer_radius_scale = outer_ring_scale;
			}
		}

		key_points.push_back(mean_center);
	}

	//if(ring_code_decoder)
	//{
	//	delete ring_code_decoder;
	//	ring_code_decoder = nullptr;
	//}
}

void BlobDetector::FindBlobs(const cv::Mat& image,
                             const cv::Mat& contour_search_image,
                             size_t threshold,
                             CircularMarkerVector& centers) const
{
	centers.clear();

	if(image.empty())
		return;

	// The thresholded image to search the contours
	// use image if no special contour search image is available
	// special contour search image is a scaled image by params.scale
	cv::Mat contour_search_binary_image;

	if(threshold > 0)
	{
		cv::threshold(contour_search_image.empty() ? image : contour_search_image,
		              contour_search_binary_image,
		              threshold,
		              1,
		              cv::THRESH_BINARY);
	}
	else
	{
		auto block_size = int(30 * params_.scale) | 0x1;
		auto offset     = 1;
		//auto erode      = params_.scale < 1.0 ? 1 : 3;

		cv::adaptiveThreshold(contour_search_image.empty() ? image : contour_search_image,
		                      contour_search_binary_image,
		                      1,
		                      cv::ADAPTIVE_THRESH_MEAN_C,
		                      cv::THRESH_BINARY,
		                      block_size,
		                      offset);

		// do not erode here, otherwise central blob overlaps with outer ring for markers
		// seen from far distance or with a large angle to the camera
	}

	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;
	std::vector<cv::Moments> moments;

	// Compressed chains are faster when computing moments and area. But the
	//	result may differ from uncompressed chains. The projector calibration
	//	fails with compressed chains. Therefore we use compressed chains for
	//	marker detection in tracking mode only.
	// TODO : find root cause for this behavior.
	cv::findContours(contour_search_binary_image,
	                 contours,
	                 hierarchy,
	                 cv::RETR_TREE,
	                 params_.decode_ring_marker ? cv::CHAIN_APPROX_SIMPLE : cv::CHAIN_APPROX_NONE);

	// readjust contour to original image size if contours have been searched in scaled image
	if(params_.scale != 1.0)
	{
		for(auto i = 0; i < contours.size(); ++i)
		{
			for(auto j = 0; j < contours[i].size(); ++j)
			{
				contours[i][j].x = cvRound(contours[i][j].x / params_.scale);
				contours[i][j].y = cvRound(contours[i][j].y / params_.scale);
			}
		}
	}

	// Cache moments
	moments.resize(contours.size());
	for(auto contour_idx = 0; contour_idx < contours.size(); ++contour_idx)
	{
		moments[contour_idx] = cv::moments(contours[contour_idx]);
	}

	for(auto contour_idx = 0; contour_idx < contours.size(); ++contour_idx)
	{
		CircularMarker center;
		center.confidence = 1;
		auto parent_idx   = hierarchy[contour_idx][3];
		auto child_idx    = hierarchy[contour_idx][2];

		// Contour should be big enough
		if(contours[contour_idx].size() < 6)
			continue;

		// Contour should have no holes
		if(child_idx != -1)
			continue;

		// Moments of contour and parent. Parent index may be -1
		// Parent info is used only if "decodeRingMarker" is true.
		cv::Moments& moms = moments[contour_idx];
		cv::Moments& momp = moments[parent_idx < 0 ? 0 : parent_idx];

		if(params_.filter_by_area)
		{
			auto area = moms.m00;

			if(area < params_.min_area || area >= params_.max_area)
				continue;
		}

		// in case the scaledContour was not properly scaled, check if the blob has a parent with the right size
		if(params_.filter_by_area && params_.coded_only && params_.decode_ring_marker)
		{
			auto area   = moms.m00;
			auto area_p = momp.m00;

			if(area_p > 6.0 * 6.0 * area)
				continue;
		}

		if(params_.filter_by_circularity)
		{
			auto area      = moms.m00;
			auto perimeter = cv::arcLength(contours[contour_idx], true);
			auto ratio     = 4 * CV_PI * area / (perimeter * perimeter);

			if(ratio < params_.min_circularity || ratio >= params_.max_circularity)
				continue;

			if(params_.coded_only && params_.decode_ring_marker)
			{
				auto area_p      = momp.m00;
				auto perimeter_p = cv::arcLength(contours[parent_idx], true);
				auto ratio_p     = 4 * CV_PI * area_p / (perimeter_p * perimeter_p);

				if(ratio_p < params_.min_circularity || ratio_p >= params_.max_circularity)
					continue;

				if(perimeter_p > 6 * perimeter)
					continue;
			}
		}

		if(params_.filter_by_convexity)
		{
			std::vector<cv::Point> hull;
			cv::convexHull(contours[contour_idx], hull);
			auto area      = moms.m00;
			auto hull_area = cv::contourArea(hull);
			auto ratio     = area / hull_area;

			if(ratio < params_.min_convexity || ratio >= params_.max_convexity)
				continue;
		}

		if(params_.filter_by_inertia)
		{
			auto denominator = sqrt(pow(2 * moms.mu11, 2) + pow(moms.mu20 - moms.mu02, 2));
			const auto eps   = 1e-2;
			double ratio;

			if(denominator > eps)
			{
				auto cos_min = (moms.mu20 - moms.mu02) / denominator;
				auto sin_min = 2 * moms.mu11 / denominator;
				auto cos_max = -cos_min;
				auto sin_max = -sin_min;

				auto i_min = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cos_min - moms.mu11 * sin_min;
				auto i_max = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cos_max - moms.mu11 * sin_max;
				ratio      = i_min / i_max;
			}
			else
			{
				ratio = 1;
			}

			if(ratio < params_.min_inertia_ratio || ratio >= params_.max_inertia_ratio)
				continue;

			center.confidence = ratio * ratio;
		}

		cv::Point2d c(moms.m10 / moms.m00, moms.m01 / moms.m00);
		center.raw_ellipse.center = cv::Point2f(float(c.x), float(c.y));

		// Add ellipse fit to improve circle center detection
		auto rotated_rect = cv::fitEllipse(contours[contour_idx]);

		// Ellipse center and blob center have to be very close
		if(cv::norm(rotated_rect.center - center.raw_ellipse.center) > 1.0)
			continue;

		auto bounding_rect = rotated_rect.boundingRect();

		if(bounding_rect.x < 0 || bounding_rect.y < 0 ||
			bounding_rect.x + bounding_rect.width > image.cols ||
			bounding_rect.y + bounding_rect.height > image.rows)
			continue;

		auto min_ratio = cos(params_.min_viewing_angle / 180.0 * 3.1415925);

		if(rotated_rect.size.height / rotated_rect.size.width < min_ratio ||
			rotated_rect.size.height / rotated_rect.size.width > 1.0 / min_ratio)
			continue;

		// check if the blob is contained inside a back ring
		// experimental code, it is switched off by default
		if(params_.coded_only && params_.decode_ring_marker && params_.filter_by_ring_in_ring)
		{
			auto scaled_contour = contours[contour_idx];

			for(auto i = 0; i < scaled_contour.size(); ++i)
			{
				cv::Point point(int(center.raw_ellipse.center.x), int(center.raw_ellipse.center.y));
				scaled_contour[i] = scaled_contour[i] * 3.2 - 2.2 * point;
			}

			auto overlap_count = 0;
			for(auto& point : scaled_contour)
			{
				auto value         = contour_search_binary_image.at<char>(point);
				auto is_x_in_range = Helper::InRange(point.x, 0, contour_search_binary_image.cols);
				auto is_y_in_range = Helper::InRange(point.y, 0, contour_search_binary_image.rows);

				if(is_x_in_range && is_y_in_range && value == 0)
					overlap_count++;
			}

			// if less than 60% is black, then the blob is probably not inside a black ring
			if(float(overlap_count) / float(scaled_contour.size()) < 0.6)
			{
				// in case the scaledContour was not properly scaled, check if the blob has a parent with the right size
				if(params_.filter_by_area)
				{
					auto area   = moms.m00;
					auto area_p = momp.m00;

					if(area_p > 6.0 * 6.0 * area)
						continue;
				}
				else
				{
					continue;
				}
			}
		}

		auto border = 1;
		cv::Size patch_size(bounding_rect.size().width + 2 * border, bounding_rect.size().height + 2 * border);
		auto comparison_image = cv::Mat(patch_size, CV_8U, Constants::BLACK);

		// draw 1px contour line white
		auto local_contour = contours[contour_idx];

		for(auto i = 0; i < local_contour.size(); ++i)
			local_contour[i] -= cv::Point(bounding_rect.x + border, bounding_rect.y + border);

		std::vector<std::vector<cv::Point>> local_contours;
		local_contours.push_back(local_contour);
		cv::drawContours(comparison_image, local_contours, -1, Constants::BRIGHT_RED, 1);

		// draw 3px ellipse in black as detected from contour
		// if contour is ellipse +-2px the white 1px contour is completely deleted then
		auto ellipse_img_center = rotated_rect.center - cv::Point2f(float(bounding_rect.x) + 1.0f, float(bounding_rect.y) + 1.0f);
		auto patch_ellipse_data = cv::RotatedRect(ellipse_img_center, rotated_rect.size, rotated_rect.angle);
		cv::ellipse(comparison_image, patch_ellipse_data, Constants::BLACK, int(params_.filter_ellipse_width));

		auto differences_count         = cv::countNonZero(comparison_image);
		const auto allowed_differences = Helper::RoundToInt((local_contours[0].size() * (1. - params_.percentage_agreement_with_artificial_ellipse)));

		if(differences_count > allowed_differences)
		{
			continue;
		}

		center.raw_ellipse = rotated_rect;

		if(params_.set_id_by_color)
		{
			center.id = image.at<uchar>(cvRound(center.raw_ellipse.center.y), cvRound(center.raw_ellipse.center.x)) < threshold
				            ? params_.black_circle_id
				            : params_.white_circle_id;
		}

		center.radius     = std::max(rotated_rect.size.width, rotated_rect.size.height);
		center.confidence = 1.0;

		// make sure ellipse is in portrait mode
		if(center.raw_ellipse.size.width < center.raw_ellipse.size.height)
		{
			auto w                         = center.raw_ellipse.size.width;
			center.raw_ellipse.size.width  = center.raw_ellipse.size.height;
			center.raw_ellipse.size.height = w;
			center.raw_ellipse.angle += 90.0f;
		}

		// make sure rotation is between 0..180
		while(center.raw_ellipse.angle >= 180.0f)
			center.raw_ellipse.angle -= 180.0f;

		while(center.raw_ellipse.angle < 0.0f)
			center.raw_ellipse.angle += 180.0f;

		centers.push_back(center);
	}
}

void BlobDetector::GetBinaryImage(const cv::Mat& image,
                                  const cv::Mat& contour_search_image,
                                  const size_t threshold,
                                  cv::Mat& contour_search_binary_image) const
{
	if(threshold)
	{
		cv::threshold(contour_search_image.empty() ? image : contour_search_image,
		              contour_search_binary_image,
		              threshold,
		              1,
		              cv::THRESH_BINARY);
	}
	else
	{
		//Why use bit operator here ( | 0x1 )instead just add one to the final value
		const auto block_size = int(Constants::BLOCK_SIZE_CONSTANT * params_.scale) + 1;

		//Do not erode here, otherwise central blob overlaps with outer ring
		//for markers seen from far distance or with a large angle to the camera
		cv::adaptiveThreshold(contour_search_image.empty() ? image : contour_search_image,
		                      contour_search_binary_image,
		                      Constants::ADAPTIVE_THRESHOLD_MAX_VALUE,
		                      cv::ADAPTIVE_THRESH_MEAN_C,
		                      cv::THRESH_BINARY,
		                      block_size,
		                      Constants::ADAPTIVE_THRESHOLD_OFFSET);
	}
}

void BlobDetector::EnsureOriginalImageSize(VectorCVPoint contours) const
{
	//Readjust contour to original image size if contours have been searched in scaled image
	if(params_.scale - Constants::DEFAULT_SCALE >= FLT_EPSILON)
	{
		for(auto& contour_i : contours)
		{
			for(auto& contour_j : contour_i)
			{
				contour_j.x = cvRound(contour_j.x / params_.scale);
				contour_j.y = cvRound(contour_j.y / params_.scale);
			}
		}
	}
}

bool BlobDetector::IsContourSizeOk(const VectorCVPoint& contours,
                                   const size_t index)
{
	//Contour should be big enough
	return contours[index].size() < Constants::DEFAULT_CONTOUR_SIZE;
}

bool BlobDetector::FilterOnParamSettings(const VectorCVPoint& contours,
                                         const VectorCVMoments& moments,
                                         const size_t contour_idx,
                                         const size_t parent_idx,
                                         const cv::Moments moment,
                                         const cv::Moments moment_parent,
                                         double& ratio) const
{
	if(params_.filter_by_area && FilterByArea(moment.m00))
		return false;

	if(params_.filter_by_circularity && !FilterByCircularity(moments[contour_idx],
	                                                         moment_parent,
	                                                         contours,
	                                                         contour_idx,
	                                                         parent_idx))
		return false;

	if(params_.filter_by_convexity && !FilterByConvexity(moments[contour_idx], contours, contour_idx))
		return false;

	if(params_.filter_by_inertia && !FilterByInertia(moments[contour_idx], ratio))
		return false;

	return true;
}

bool BlobDetector::IsLessThanMinRatio(const cv::RotatedRect& rectangle) const
{
	const auto min_ratio = float(cos(Helper::AngleInRadians(params_.min_viewing_angle)));
	const auto ratio     = rectangle.size.height / rectangle.size.width;

	return Helper::InRange(ratio, min_ratio, 1.0f / min_ratio);
}

bool BlobDetector::FilterByArea(const double area) const
{
	return area < params_.min_area || area >= params_.max_area;
}

bool BlobDetector::FilterByCircularity(const cv::Moments moment,
                                       const cv::Moments parent_moment,
                                       const VectorCVPoint& contours,
                                       const size_t contour_idx,
                                       const size_t parent_idx) const
{
	const auto area      = moment.m00;
	const auto perimeter = cv::arcLength(contours[contour_idx], true);
	const auto ratio     = 4 * CV_PI * area / (perimeter * perimeter);

	if(ratio < params_.min_circularity || ratio >= params_.max_circularity)
		return false;

	if(params_.coded_only && params_.decode_ring_marker)
	{
		const auto area_parent      = parent_moment.m00;
		const auto perimeter_parent = cv::arcLength(contours[parent_idx], true);
		const auto ratio_parent     = 4 * CV_PI * area_parent / (perimeter_parent * perimeter_parent);

		if(ratio_parent < params_.min_circularity || ratio_parent >= params_.max_circularity)
			return false;

		if(perimeter_parent > Constants::FILTER_BY_CIRCULARITY_FACTOR * perimeter)
			return false;
	}

	return true;
}

bool BlobDetector::FilterByConvexity(const cv::Moments moment,
                                     const VectorCVPoint& contours,
                                     const size_t contour_idx) const
{
	std::vector<cv::Point> hull;
	cv::convexHull(contours[contour_idx], hull);
	const auto ratio = moment.m00 / cv::contourArea(hull);

	return ratio < params_.min_convexity || ratio >= params_.max_convexity ? false : true;
}

double BlobDetector::CalculateRatio(const cv::Moments moment,
                                    const double denominator)
{
	const auto cos_min = (moment.mu20 - moment.mu02) / denominator;
	const auto sin_min = 2 * moment.mu11 / denominator;
	const auto cos_max = -cos_min;
	const auto sin_max = -sin_min;
	const auto i_min   = 0.5 * (moment.mu20 + moment.mu02) - 0.5 * (moment.mu20 - moment.mu02) * cos_min - moment.mu11 * sin_min;
	const auto i_max   = 0.5 * (moment.mu20 + moment.mu02) - 0.5 * (moment.mu20 - moment.mu02) * cos_max - moment.mu11 * sin_max;

	return i_min / i_max;
}

bool BlobDetector::FilterByInertia(const cv::Moments moment,
                                   double& ratio) const
{
	const auto denominator = sqrt(pow(2 * moment.mu11, 2) + pow(moment.mu20 - moment.mu02, 2));
	const auto ratio_in    = denominator < Constants::FILTER_BY_INERTIA_EPS ? CalculateRatio(moment, denominator) : 1;

	if(ratio_in < params_.min_inertia_ratio || ratio_in >= params_.max_inertia_ratio)
		return false;

	ratio = ratio_in * ratio_in;

	return true;
}

bool BlobDetector::CheckBoundingRectangle(const cv::Mat& image,
                                          const cv::Rect& rectangle)
{
	return rectangle.x < 0 ||
		rectangle.y < 0 ||
		rectangle.x + rectangle.width > image.cols ||
		rectangle.y + rectangle.height > image.rows;
}

bool BlobDetector::IfBlobInsideBlackRing(const cv::Mat& contour_search_binary_image,
                                         const VectorCVPoint contours,
                                         const size_t contour_idx,
                                         const CircularMarker& center,
                                         const cv::Moments moment,
                                         const cv::Moments moment_parent) const
{
	auto scaled_contour = contours[contour_idx];
	const auto point    = 2.2 * cv::Point(int(center.raw_ellipse.center.x), int(center.raw_ellipse.center.y));

	//TODO Use generate instead
	for(auto i            = 0; i < scaled_contour.size(); ++i)
		scaled_contour[i] = scaled_contour[i] * 3.2 - point;

	const auto overlap_count = GetOverlapCount(contour_search_binary_image, scaled_contour);

	//If less than 60% is black, then the blob is probably not inside a black ring
	return IsBlobToBlackRingRatioOk(moment, moment_parent, scaled_contour, overlap_count);
}

size_t BlobDetector::GetOverlapCount(const cv::Mat& binary_image,
                                     const std::vector<cv::Point_<int>>& scaled_contour)
{
	size_t count = 0;

	for(const auto& point : scaled_contour)
	{
		if(IsPointOnImage(binary_image, point) && binary_image.at<char>(point) == 0)
		{
			count++;
		}
	}

	return count;
}

bool BlobDetector::IsBlobToBlackRingRatioOk(const cv::Moments moment,
                                            const cv::Moments moment_parent,
                                            const std::vector<cv::Point_<int>>& contour,
                                            const size_t count) const
{
	if(BlobToBlackRingRatio(contour, count))
	{
		//In case the scaledContour was not properly scaled, check if the blob has a parent with the right size
		if(!params_.filter_by_area)
			return true;

		if(moment_parent.m00 > Constants::MAX_PARENT_SIZE * moment.m00)
			return true;
	}

	return false;
}

bool BlobDetector::IsParentRightSize(const double area,
                                     const double area_parent) const
{
	//In case the scaledContour was not properly scaled, check if the blob has a parent with the right size
	return params_.filter_by_area && params_.coded_only &&
		params_.decode_ring_marker &&
		area_parent > Constants::MAX_PARENT_SIZE * area;
}

auto BlobDetector::BlobToBlackRingRatio(const std::vector<cv::Point_<int>>& contour,
                                        const size_t count) -> bool
{
	return float(count) / float(contour.size()) < Constants::BLOB_BLACK_RING_RATIO;
}

bool BlobDetector::IsPointOnImage(const cv::Mat& image,
                                  const std::vector<cv::Point_<int>>::value_type& point)
{
	return point.x > 0 &&
		point.y > 0 &&
		point.x < image.cols &&
		point.y < image.rows;
}
}
}
