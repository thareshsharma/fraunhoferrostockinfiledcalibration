#include "read_from_scope.h"

ReadFromScope::ReadFromScope(std::istream* input_stream,
                             ReadContextBase* read_context) :
	model_in_format_(input_stream)
{
	read_context_ = read_context ? read_context : new ReadContextBase();
	model_in_format_.SetFormat(read_context_->GetFormat());
}

ReadFromScope::~ReadFromScope() = default;

ReadContextBase* ReadFromScope::GetReadContext() const
{
	return read_context_;
}

ModelInFormat& ReadFromScope::GetModelInFormat()
{
	model_in_format_.SetFormat(GetReadContext()->GetFormat());
	return model_in_format_;
}

//ReadFromScope::ReadFromScope(const ReadFromScope&)
//{
//}

//ReadFromScope& ReadFromScope::operator=(const ReadFromScope&)
//{
//	return *this;
//}
