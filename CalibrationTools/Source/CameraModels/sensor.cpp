#include "sensor.h"
#include "helper.h"

namespace DMVS
{
namespace CameraCalibration
{
Sensor::Sensor() :
	rotation_matrix_(cv::Mat::eye(3, 3, CV_64FC1)),
	translation_vector_(cv::Mat::zeros(3, 1, CV_64FC1)),
	transformation_matrix_(cv::Mat::zeros(4, 4, CV_64FC1)),
	image_channel_(CHANNEL_DEFAULT)
{
	SyncTransformationMatrix();
}

Sensor::Sensor(const cv::Mat4d& transformation_matrix,
               const ImageChannel image_channel) :
	transformation_matrix_(transformation_matrix),
	image_channel_(image_channel)
{
	SyncExtrinsicParameters();
}

Sensor::Sensor(const cv::Mat& rotation_matrix,
               const cv::Mat& translation_vector,
               const ImageChannel image_channel):
	rotation_matrix_(rotation_matrix),
	translation_vector_(translation_vector),
	transformation_matrix_(cv::Mat::zeros(4, 4, CV_64FC1)),
	image_channel_(image_channel)
{
	SyncTransformationMatrix();
}

Sensor::Sensor(const Sensor& rhs)
	: enable_shared_from_this(rhs)
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;
	}
}

Sensor& Sensor::operator=(const Sensor& rhs)
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;
	}

	return *this;
}

Sensor::Sensor(Sensor&& rhs) noexcept
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;

		// Free rhs from its values
		rhs.rotation_matrix_       = cv::Mat::zeros(3, 3, 0);
		rhs.translation_vector_    = cv::Mat::zeros(3, 1, 0);
		rhs.transformation_matrix_ = cv::Mat::zeros(4, 4, 0);
		rhs.image_channel_         = CHANNEL_DEFAULT;
	}
}

Sensor& Sensor::operator=(Sensor&& rhs) noexcept
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;

		// Free rhs from its values
		rhs.rotation_matrix_       = cv::Mat::zeros(3, 3, 0);
		rhs.translation_vector_    = cv::Mat::zeros(3, 1, 0);
		rhs.transformation_matrix_ = cv::Mat::zeros(4, 4, 0);
		rhs.image_channel_         = CHANNEL_DEFAULT;
	}

	return *this;
}

Sensor::~Sensor() = default;

void Sensor::GetPlaneIntersectionPoints(cv::InputArray& points_2d,
                                        const cv::Vec3d& plane_position,
                                        const cv::Vec3d& plane_normal,
                                        cv::OutputArray intersection_points_coordinate_system) const
{
	// Plane position and plane normal are in camera coordinate system
	intersection_points_coordinate_system.create(points_2d.size(), intersection_points_coordinate_system.type(), -1, true);
	const auto out_matrix = intersection_points_coordinate_system.getMat();
	const auto data       = reinterpret_cast<cv::Point3f*>(out_matrix.data);

	//Camera position in Freestyle coordinates as ray origin
	const cv::Point3f origin(0., 0., 0.);
	const cv::Vec3d ray_location(origin.x, origin.y, origin.z);
	cv::Vec3d plane_normalized;
	cv::normalize(plane_normal, plane_normalized);
	const auto d_hesse = plane_normalized.dot(plane_position);

	//Calculate intersection Points of IR0 beams with plane detected by color camera
	std::vector<cv::Point3f> ray_directions;
	RayReconstructionWCS(points_2d, ray_directions, true);

	std::vector<cv::Point3f> points_3d;

	for(size_t idx = 0; idx < ray_directions.size(); ++idx)
	{
		cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
		const auto line_direction_scale = (d_hesse - plane_normalized.dot(ray_location)) / plane_normalized.dot(ray_direction);
		auto point                      = (line_direction_scale * ray_direction) + ray_location;
		cv::Point3f temp_point          = cv::Point3f(point[0], point[1], point[2]);
		data[idx] = FromSensorToWorldCoordinateSystem(temp_point);
		points_3d.push_back(data[idx]);
	}

	//second run if sensor supports depth dependent un-distortion
	if(HandlesDepthValues())
	{
		//ray_directions.clear();
		//RayReconstruction(points_2d, ray_directions, true, points_3d);
		//for(size_t idx = 0; idx < ray_directions.size(); ++idx)
		//{
		//	cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
		//	const auto line_direction_scale = (d_hesse - plane_normal.dot(ray_location)) / plane_normal.dot(ray_direction);
		//	auto point                      = line_direction_scale * ray_direction + ray_location;
		//	data[idx]                       = cv::Point3f(point[0], point[1], point[2]);
		//}
	}
}

cv::Point3f Sensor::FromSensorToWorldCoordinateSystem(const cv::Point3f& point) const
{
	// First move the point back
	const cv::Mat point_mat           = (cv::Mat_<double>(3, 1) << point.x, point.y, point.z);
	const auto translated_back_vector = point_mat - translation_vector_;

	// Multiply with inverse of rotation matrix
	const auto x = GetValue<float>(translated_back_vector, 0);
	const auto y = GetValue<float>(translated_back_vector, 1);
	const auto z = GetValue<float>(translated_back_vector, 2);

	return {x, y, z};
}

cv::Point3f Sensor::FromSensorToWorldCoordinateSystem(const cv::Point3f& point,
                                                      const cv::Mat& rotation_matrix,
                                                      const cv::Mat& translation_vector) const
{
	// First move the point back
	const cv::Mat point_mat           = (cv::Mat_<double>(3, 1) << point.x, point.y, point.z);
	const auto translated_back_vector = point_mat - translation_vector;

	// Multiply with inverse of rotation matrix
	const auto x = GetValue<float>(translated_back_vector, rotation_matrix, 0);
	const auto y = GetValue<float>(translated_back_vector, rotation_matrix, 1);
	const auto z = GetValue<float>(translated_back_vector, rotation_matrix, 2);

	return {x, y, z};
}

cv::Point3d Sensor::FromSensorToWorldCoordinateSystem(const cv::Point3d& point) const
{
	// First move the point back
	const cv::Mat point_mat           = (cv::Mat_<double>(3, 1) << point.x, point.y, point.z);
	const auto translated_back_vector = point_mat - translation_vector_;

	// Multiply with inverse of rotation matrix
	const auto x = GetValue<double>(translated_back_vector, 0);
	const auto y = GetValue<double>(translated_back_vector, 1);
	const auto z = GetValue<double>(translated_back_vector, 2);

	return {x, y, z};
}

Eigen::Vector3f Sensor::FromSensorToWorldCoordinateSystem(const Eigen::Vector3f& eigen_vector) const
{
	const Eigen::Vector3d point_double = eigen_vector.cast<double>();

	return FromSensorToWorldCoordinateSystem(point_double).cast<float>();
}

Eigen::Matrix3f Sensor::FromSensorToWorldCoordinateSystem(const Eigen::Matrix3f& eigen_matrix) const
{
	const Eigen::Matrix3d matrix_double = eigen_matrix.cast<double>();

	return FromSensorToWorldCoordinateSystem(matrix_double).cast<float>();
}

Eigen::Vector3d Sensor::FromSensorToWorldCoordinateSystem(const Eigen::Vector3d& eigen_vector) const
{
	Eigen::Matrix3d rotation;
	cv::cv2eigen(rotation_matrix_, rotation);

	//_R should be orthonormal. Inverse == transposed
	return rotation.transpose() * (eigen_vector - Eigen::Map<Eigen::Vector3d>(reinterpret_cast<double*>(translation_vector_.data)));
}

Eigen::Matrix3d Sensor::FromSensorToWorldCoordinateSystem(const Eigen::Matrix3d& eigen_matrix) const
{
	Eigen::Matrix3d rotation;
	cv::cv2eigen(rotation_matrix_, rotation);

	//_R should be orthonormal. Inverse == transposed
	return rotation.transpose() * eigen_matrix;
}

cv::Point3f Sensor::FromWorldCoordinateSystemToSensor(const cv::Point3f& point) const
{
	Eigen::Matrix3d rotation;
	cv::cv2eigen(rotation_matrix_, rotation);

	const Eigen::Map<Eigen::Vector3d> translation_projection(reinterpret_cast<double*>(translation_vector_.data));
	const Eigen::Vector3d p(point.x, point.y, point.z);

	const Eigen::Vector3d projection_new = (rotation * p + translation_projection);
	return cv::Point3f(projection_new[0], projection_new[1], projection_new[2]);
}

cv::Point3d Sensor::FromWorldCoordinateSystemToSensor(const cv::Point3d& point) const
{
	Eigen::Matrix3d rotation;
	cv::cv2eigen(rotation_matrix_, rotation);

	const Eigen::Map<Eigen::Vector3d> translation_projection(reinterpret_cast<double*>(translation_vector_.data));
	const Eigen::Vector3d eigen_point(point.x, point.y, point.z);
	Eigen::Vector3d projection_new = rotation * eigen_point + translation_projection;

	return {(projection_new[0], projection_new[1], projection_new[2])};
}

Eigen::Vector3f Sensor::FromWorldCoordinateSystemToSensor(const Eigen::Vector3f& eigen_vector) const
{
	const Eigen::Vector3d vector_double = eigen_vector.cast<double>();

	return FromWorldCoordinateSystemToSensor(vector_double).cast<float>();
}

Eigen::Matrix3f Sensor::FromWorldCoordinateSystemToSensor(const Eigen::Matrix3f& eigen_matrix) const
{
	const Eigen::Matrix3d matrix_double = eigen_matrix.cast<double>();

	return FromWorldCoordinateSystemToSensor(matrix_double).cast<float>();
}

Eigen::Vector3d Sensor::FromWorldCoordinateSystemToSensor(const Eigen::Vector3d& eigen_vector) const
{
	Eigen::Matrix3d rotation;
	cv::cv2eigen(rotation_matrix_, rotation);
	const Eigen::Map<Eigen::Vector3d> vector_double(reinterpret_cast<double*>(translation_vector_.data));

	return rotation * eigen_vector + vector_double;
}

Eigen::Matrix3d Sensor::FromWorldCoordinateSystemToSensor(const Eigen::Matrix3d& eigen_matrix) const
{
	Eigen::Matrix3d rotation;
	cv::cv2eigen(rotation_matrix_, rotation);

	return rotation * eigen_matrix;
}

cv::Vec2d Sensor::ProjectPoint(const cv::Vec3d& point_3d,
                               const bool apply_camera_extrinsics) const
{
	cv::Vec2d out;
	cv::Mat arg1(1, 1, CV_64FC3, (double*)&point_3d);
	cv::Mat arg2(1, 1, CV_64FC2, &out);
	ProjectPoints(arg1, arg2, apply_camera_extrinsics, false);

	return out;
}

cv::Vec2d Sensor::UndistortPoint(const cv::Vec2d& point_pixel,
                                 const cv::Vec3d& point_depth) const
{
	std::vector<cv::Vec2d> cv_pix;
	std::vector<cv::Vec2d> cv_pt;
	cv_pix.push_back(point_pixel);

	if(point_depth.channels != 0)
	{
		UndistortPoints(cv_pix, cv_pt);
	}
	else
	{
		std::vector<cv::Vec3d> cv_depth;
		cv_depth.push_back(point_depth);

		UndistortPoints(cv_pix, cv_pt, cv_depth);
	}

	return cv::Vec2d(cv_pt.front()[0], cv_pt.front()[1]);
}

cv::Mat4d Sensor::GetTransformationMatrix() const
{
	cv::Mat4d transformation = cv::Mat::zeros(4, 4, CV_64FC1);

	for(auto row = 0; row < 3; ++row)
	{
		for(auto col = 0; col < 3; ++col)
		{
			transformation(row, col) = rotation_matrix_.at<double>(row, col);
		}
	}

	transformation(0, 3) = translation_vector_.at<double>(0, 0);
	transformation(1, 3) = translation_vector_.at<double>(1, 0);
	transformation(2, 3) = translation_vector_.at<double>(2, 0);
	transformation(3, 3) = 1;

	return transformation;
}

void Sensor::SetTransformationMatrix(const cv::Mat4d& transformation_matrix)
{
	transformation_matrix_ = transformation_matrix;
	SyncExtrinsicParameters();
}

void Sensor::SetExtrinsicParameters(const cv::Mat& rotation_matrix,
                                    const cv::Mat& translation_vector)
{
	if(rotation_matrix.cols == 3)
	{
		rotation_matrix_ = rotation_matrix.clone();
	}
	else if(rotation_matrix.cols == 1 || rotation_matrix.rows == 1)
	{
		cv::Mat rot_mat;
		cv::Rodrigues(rotation_matrix, rot_mat);
		rotation_matrix_ = rot_mat.clone();
	}

	translation_vector_ = translation_vector.clone();
	SyncTransformationMatrix();
}

void Sensor::GetExtrinsicParameters(cv::Mat& rotation_matrix,
                                    cv::Mat& translation_vector) const
{
	/*cv::Mat rot_mat;
	if(rotation_matrix_.cols == 1 || rotation_matrix.rows == 1)
	{
		cv::Rodrigues(rotation_matrix, rot_mat);
	}
	else
	{
		assert(rotation_matrix.cols == 3);
		rotation_matrix = rot_mat.clone();
	}*/

	rotation_matrix    = rotation_matrix_.empty() ? cv::Mat::eye(3, 3, CV_64F) : rotation_matrix_.clone();
	translation_vector = translation_vector_.empty() ? cv::Mat::zeros(3, 1, CV_64F) : translation_vector_.clone();
}

ImageChannel Sensor::GetImageChannel() const
{
	return image_channel_;
}

bool Sensor::SolvePnP(cv::InputArray object_points,
                      cv::InputArray image_points,
                      cv::Mat& rotation_vector,
                      cv::Mat& translation_vector,
                      const bool if_use_extrinsic_guess,
                      const int flags) const
{
	// New approach: use pinhole focal length 1 coordinates, so solvePNP does not need the undistortion model
	if(if_use_extrinsic_guess && (rotation_vector.total() != 3 || translation_vector.total() != 3))
		return false;

	cv::Mat undistorted_points;
	UndistortPoints(image_points, undistorted_points);

	const auto first_solve_ok = cv::solvePnP(object_points,
	                                         undistorted_points,
	                                         cv::Mat::eye(3, 3, CV_64F),
	                                         cv::Mat(),
	                                         rotation_vector,
	                                         translation_vector,
	                                         if_use_extrinsic_guess,
	                                         cv::SOLVEPNP_ITERATIVE);

	if(first_solve_ok && HandlesDepthValues())
	{
		CvMat object_points_cvmat;
		Helper::ToCVMat(object_points.getMat(), object_points_cvmat);
		const auto data_float  = (CvPoint3D32f*)object_points_cvmat.data.ptr;
		const auto data_double = (CvPoint3D64f*)object_points_cvmat.data.ptr;
		const auto source_type = CV_MAT_TYPE(object_points_cvmat.type);
		const auto source_step = object_points_cvmat.rows == 1 ? 1 : object_points_cvmat.step / CV_ELEM_SIZE(source_type);
		auto total_size        = object_points_cvmat.rows + object_points_cvmat.cols - 1;

		std::vector<cv::Point3d> points_3d(total_size);

		if(source_type == CV_32FC3)
		{
			for(auto i = 0; i < total_size; ++i)
			{
				points_3d[i].x = data_float[i * source_step].x;
				points_3d[i].y = data_float[i * source_step].y;
				points_3d[i].z = data_float[i * source_step].z;
			}
		}
		else
		{
			for(auto i = 0; i < total_size; ++i)
			{
				points_3d[i].x = data_double[i * source_step].x;
				points_3d[i].y = data_double[i * source_step].y;
				points_3d[i].z = data_double[i * source_step].z;
			}
		}

		const auto local_points = Helper::ApplyTransformation(rotation_vector, translation_vector, points_3d);
		UndistortPoints(image_points, undistorted_points, local_points);

		return cv::solvePnP(object_points,
		                    undistorted_points,
		                    cv::Mat::eye(3, 3, CV_64F),
		                    cv::Mat(),
		                    rotation_vector,
		                    translation_vector,
		                    true,
		                    cv::SOLVEPNP_ITERATIVE);
	}

	//TODO second run with depth undistorted points if depth dependency supported by model
	return first_solve_ok;
}

size_t Sensor::StereoIntersect(std::shared_ptr<const Sensor> other,
                               const std::vector<cv::Vec2f>& this_px_coords,
                               const std::vector<cv::Vec2f>& other_px_coords,
                               std::vector<cv::Vec3d>& points_3d,
                               std::vector<cv::Vec2f>* error_2d,
                               bool ignore_depth) const
{
	//check corresponding pixel coordinate vectors size match
	if(other_px_coords.size() != this_px_coords.size())
		return Constants::DEFAULT_SIZE_T_MAX_VALUE;

	//if depth dependence supported and 3d data available:
	//copy 3d data to cv type structure for depth dependent undistortion later
	auto handles_depth_dependency = (this->HandlesDepthValues() || other->HandlesDepthValues()) && !ignore_depth;
	std::vector<cv::Point3d> points_3d_back_up;

	if(points_3d.size() == this_px_coords.size() && handles_depth_dependency)
	{
		for(auto point : points_3d)
		{
			points_3d_back_up.emplace_back(cv::Point3d(point));
		}
	}

	points_3d.clear();

	if(error_2d)
		error_2d->clear();

	cv::Mat rotation_this, translation_this, rotation_other, translation_other;
	this->GetExtrinsicParameters(rotation_this, translation_this);
	other->GetExtrinsicParameters(rotation_other, translation_other);

	Eigen::Map<Eigen::Vector3d> p_this((double*)(translation_this.data), 3, 1);
	Eigen::Map<Eigen::Vector3d> p_other((double*)(translation_other.data), 3, 1);
	Eigen::Map<Eigen::Matrix3d> r_this((double*)(rotation_this.data), 3, 3);
	Eigen::Map<Eigen::Matrix3d> r_other((double*)(rotation_other.data), 3, 3);

	Eigen::Vector3d rpThis  = r_this * p_this;
	Eigen::Vector3d rpOther = r_other * p_other;

	// if depth dependence supported and 3d data not yet available:
	// create initial 3d data for depth dependent undistortion (within ray reconstruction)
	if(handles_depth_dependency && points_3d_back_up.empty())
	{
		std::vector<cv::Vec3f> dir_this, dir_other;
		this->RayReconstructionWCS(this_px_coords, dir_this, true);
		other->RayReconstructionWCS(other_px_coords, dir_other, true);

		for(size_t i = 0; i < this_px_coords.size(); ++i)
		{
			Eigen::Matrix3d directions(Eigen::Matrix3d::Zero());
			Eigen::Vector3d positions(Eigen::Vector3d::Zero());

			Eigen::Vector3d w0(dir_this[i][0], dir_this[i][1], dir_this[i][2]);
			Eigen::Vector3d w1(dir_other[i][0], dir_other[i][1], dir_other[i][2]);

			Eigen::Matrix3d v0 = Eigen::Matrix3d::Identity() - w0 * w0.transpose();
			Eigen::Matrix3d v1 = Eigen::Matrix3d::Identity() - w1 * w1.transpose();

			directions += v0;
			directions += v1;

			positions -= v0 * rpThis;
			positions -= v1 * rpOther;

			Eigen::Vector3d t = directions.inverse() * positions;
			points_3d_back_up.emplace_back(cv::Point3d(t[0], t[1], t[2]));
		}
	}

	// Run final 3d point calculation based on depth dependent undistorted rays
	std::vector<cv::Vec3f> dir_this, dir_other;
	this->RayReconstructionWCS(this_px_coords, dir_this, true, points_3d_back_up);
	other->RayReconstructionWCS(other_px_coords, dir_other, true, points_3d_back_up);

	for(size_t i = 0; i < this_px_coords.size(); ++i)
	{
		Eigen::Matrix3d directions(Eigen::Matrix3d::Zero());
		Eigen::Vector3d positions(Eigen::Vector3d::Zero());

		Eigen::Vector3d w0(dir_this[i][0], dir_this[i][1], dir_this[i][2]);
		Eigen::Vector3d w1(dir_other[i][0], dir_other[i][1], dir_other[i][2]);

		Eigen::Matrix3d v0 = Eigen::Matrix3d::Identity() - (w0 * w0.transpose());
		Eigen::Matrix3d v1 = Eigen::Matrix3d::Identity() - (w1 * w1.transpose());

		directions += v0;
		directions += v1;

		positions -= v0 * rpThis;
		positions -= v1 * rpOther;

		Eigen::Vector3d t = directions.inverse() * positions;
		points_3d.emplace_back(cv::Vec3d(t[0], t[1], t[2]));
	}

	if(error_2d)
	{
		std::vector<cv::Point3d> points;

		for(size_t i = 0; i < points_3d.size(); ++i)
			points.emplace_back(cv::Point3d(points_3d[i][0], points_3d[i][1], points_3d[i][2]));

		std::vector<cv::Vec2d> points_2d;
		ProjectPoints(points, points_2d);

		for(size_t i = 0; i < points_2d.size(); ++i)
		{
			cv::Vec2f point_float(points_2d[i][0], points_2d[i][1]);
			error_2d->push_back(point_float - this_px_coords[i]);
		}
	}

	return points_3d.size();
}

void Sensor::RayReconstructionWCS(cv::InputArray points_2d,
                               cv::OutputArray out,
                               const bool normalize,
                               cv::InputArray points3d) const
{
	std::vector<cv::Point2f> undistorted;
	UndistortPoints(points_2d, undistorted, points3d);

	//Using opencv structures as described here:
	//http://stackoverflow.com/questions/25750041/pass-opencv-inputarray-and-use-it-as-stdvector
	out.create(points_2d.size(), out.type());

	cv::Mat out_mat = out.getMat();
	const cv::Mat out_double(out_mat.size(), out.type());
	const auto data = reinterpret_cast<cv::Point3f*>(out_double.data);
	const auto origin(FromSensorToWorldCoordinateSystem(cv::Point3f(0., 0., 0.)));
	size_t index = 0;

	for(const auto& point : undistorted)
	{
		//point in camera CS
		cv::Point3f point3(point.x, point.y, 1.0);

		//point in world CS
		point3 = FromSensorToWorldCoordinateSystem(point3);

		//direction from camera origin to point at 1m (in camera CS), this is the ray direction in world coordinates
		point3 = point3 - origin;

		if(normalize)
		{
			point3 = point3 * (1. / cv::norm<float>(point3));
		}

		data[index] = point3;
		++index;
	}

	out_double.convertTo(out_mat, out_mat.type());
}

void Sensor::SyncTransformationMatrix()
{
	const auto rows = rotation_matrix_.rows;
	const auto cols = rotation_matrix_.cols;

	for(auto row = 0; row < rows; ++row)
	{
		for(auto col = 0; col < cols; ++col)
		{
			transformation_matrix_.at<double>(row, col) = rotation_matrix_.at<double>(row, col);
		}
	}

	transformation_matrix_.at<double>(0, 3) = translation_vector_.at<double>(0, 0);
	transformation_matrix_.at<double>(1, 3) = translation_vector_.at<double>(1, 0);
	transformation_matrix_.at<double>(2, 3) = translation_vector_.at<double>(2, 0);
}

void Sensor::SyncExtrinsicParameters()
{
	const auto rows = transformation_matrix_.rows;
	const auto cols = transformation_matrix_.cols;

	for(auto row = 0; row < rows - 1; ++row)
	{
		for(auto col = 0; col < cols - 1; ++col)
		{
			rotation_matrix_.at<double>(row, col) = transformation_matrix_.at<double>(row, col);
		}
	}

	translation_vector_.at<double>(0, 0) = transformation_matrix_.at<double>(0, 3);
	translation_vector_.at<double>(1, 0) = transformation_matrix_.at<double>(1, 3);
	translation_vector_.at<double>(2, 0) = transformation_matrix_.at<double>(2, 3);
}
}
}
