#include "single_camera_calibration.h"
#include "constants.h"
#include "calibration_header.h"

namespace DMVS
{
namespace CameraCalibration
{
SingleCameraCalibration::SingleCameraCalibration(const CameraInfo& camera_info) :
	CameraSensor(),
	camera_info_(camera_info)
{
	InitParameterFixations();
	image_channel_ = CHANNEL_DEFAULT;
}

SingleCameraCalibration::SingleCameraCalibration() :
	CameraSensor()
{
}

SingleCameraCalibration::SingleCameraCalibration(cv::InputArray& camera_matrix_in,
                                                 cv::InputArray& distortion,
                                                 cv::InputArray& rotation_matrix,
                                                 cv::InputArray& translation_vector_in) :
	CameraSensor()
{
	InitParameterFixations();
	image_channel_ = CHANNEL_DEFAULT;

	camera_info_.intrinsics.camera_intrinsic_matrix = camera_matrix_in.getMat().clone();
	camera_info_.intrinsics.distortion_vector       = distortion.getMat().clone();
	camera_info_.extrinsics.rotation_matrix         = rotation_matrix.getMat().clone();
	camera_info_.extrinsics.translation_vector      = translation_vector_in.getMat().clone();
}

SingleCameraCalibration::SingleCameraCalibration(const std::shared_ptr<const SingleCameraCalibration> source) :
	CameraSensor(source->rotation_matrix_, source->translation_vector_, source->image_channel_)
{
	InitParameterFixations();
}

CameraInfo SingleCameraCalibration::GetCameraInfo() const
{
	return camera_info_;
}

bool SingleCameraCalibration::ParamVectorToCameraInfo(const std::vector<double>& params,
                                                      CameraInfo& camera_info)
{
	// Focal Point
	camera_info.intrinsics.camera_intrinsic_matrix.at<double>(0, 0) = params[0];
	camera_info.intrinsics.camera_intrinsic_matrix.at<double>(1, 1) = params[1];
	//cx and cy
	camera_info.intrinsics.camera_intrinsic_matrix.at<double>(0, 2) = params[2];
	camera_info.intrinsics.camera_intrinsic_matrix.at<double>(1, 2) = params[3];

	// distortion vector
	camera_info.intrinsics.distortion_vector.at<double>(0, 0) = params[4];
	camera_info.intrinsics.distortion_vector.at<double>(0, 1) = params[5];
	camera_info.intrinsics.distortion_vector.at<double>(0, 2) = params[6];
	camera_info.intrinsics.distortion_vector.at<double>(0, 3) = params[7];
	camera_info.intrinsics.distortion_vector.at<double>(0, 4) = params[8];

	// rotation matrix
	cv::Mat rotation_matrix_temp;
	cv::Mat rotation_vector          = cv::Mat(3, 1, CV_64F);
	rotation_vector.at<double>(0, 0) = params[9];
	rotation_vector.at<double>(1, 0) = params[9];
	rotation_vector.at<double>(2, 0) = params[9];
	cv::Rodrigues(rotation_vector, rotation_matrix_temp);
	camera_info.extrinsics.rotation_matrix = rotation_matrix_temp;

	//tranlation vector
	camera_info.extrinsics.translation_vector.at<double>(0, 0) = params[0];
	camera_info.extrinsics.translation_vector.at<double>(1, 0) = params[0];
	camera_info.extrinsics.translation_vector.at<double>(2, 0) = params[0];

	//depth dependent exp
	camera_info.intrinsics.depth_parameters.exponential(0) = params[0];
	camera_info.intrinsics.depth_parameters.exponential(1) = params[0];
	camera_info.intrinsics.depth_parameters.exponential(2) = params[0];

	return true;
}

bool SingleCameraCalibration::CameraInfoToParamVector(const CameraInfo& camera_info,
                                                      std::vector<double>& params)
{
	params.resize(Constants::PARAMS_SIZE);
	// Focal Point
	params.push_back(camera_info.intrinsics.camera_intrinsic_matrix.at<double>(0, 0));
	params.push_back(camera_info.intrinsics.camera_intrinsic_matrix.at<double>(1, 1));
	//cx and cy
	params.push_back(camera_info.intrinsics.camera_intrinsic_matrix.at<double>(0, 2));
	params.push_back(camera_info.intrinsics.camera_intrinsic_matrix.at<double>(1, 2));

	// distortion vector
	params.push_back(camera_info.intrinsics.distortion_vector.at<double>(0, 0));
	params.push_back(camera_info.intrinsics.distortion_vector.at<double>(0, 1));
	params.push_back(camera_info.intrinsics.distortion_vector.at<double>(0, 2));
	params.push_back(camera_info.intrinsics.distortion_vector.at<double>(0, 3));
	params.push_back(camera_info.intrinsics.distortion_vector.at<double>(0, 4));

	// rotation matrix
	cv::Mat rotation_vector;
	cv::Rodrigues(camera_info.extrinsics.rotation_matrix, rotation_vector);
	params.push_back(rotation_vector.at<double>(0, 0));
	params.push_back(rotation_vector.at<double>(1, 0));
	params.push_back(rotation_vector.at<double>(2, 0));

	//tranlation vector
	params.push_back(camera_info.extrinsics.translation_vector.at<double>(0, 0));
	params.push_back(camera_info.extrinsics.translation_vector.at<double>(1, 0));
	params.push_back(camera_info.extrinsics.translation_vector.at<double>(2, 0));

	//depth dependent exp
	params.push_back(camera_info.intrinsics.depth_parameters.exponential(0));
	params.push_back(camera_info.intrinsics.depth_parameters.exponential(1));
	params.push_back(camera_info.intrinsics.depth_parameters.exponential(2));

	return true;
}

std::shared_ptr<CameraSensor> SingleCameraCalibration::Duplicate() const
{
	auto out = std::make_shared<SingleCameraCalibration>(camera_info_);
	out->SetImageChannel(image_channel_);
	out->parameter_fixations_ = parameter_fixations_;
	return out;
}

std::shared_ptr<CameraSensor> SingleCameraCalibration::DuplicateLaser() const
{
	return nullptr;
}

void SingleCameraCalibration::LaserProjectPoints(cv::InputArray points_3d,
                                                 cv::OutputArray points_2d,
                                                 bool apply_camera_extrinsics,
                                                 bool ignore_depth) const
{
}

ImageChannel SingleCameraCalibration::GetImageChannel() const
{
	return image_channel_;
}

void SingleCameraCalibration::SetImageChannel(const ImageChannel image_channel)
{
	image_channel_ = image_channel;
}

size_t SingleCameraCalibration::InitFromParamsVector(const std::vector<double>& params)
{
	if(params.size() != Constants::PARAMS_COUNT)
	{
		assert(false);
	}

	// convert pure double vector to internal structure with meaningful names
	CameraInfo camera_info;

	if(!ParamVectorToCameraInfo(params, camera_info))
		return Constants::DEFAULT_SIZE_T_VALUE;

	//parameter_representation_ = params;
	return 0;
}

void SingleCameraCalibration::KeepExtrinsicParametersFix(const bool fix_extrinsic_parameters)
{
	for(auto i = 9; i < Constants::PARAMS_COUNT; ++i)
	{
		parameter_fixations_[i] = fix_extrinsic_parameters ? Constants::DEFAULT_SIZE_T_MAX_VALUE : i;
	}
}

void SingleCameraCalibration::KeepDistortionFix(const bool fix_distortion)
{
	for(auto i = 4; i < 9; ++i)
	{
		parameter_fixations_[i] = fix_distortion ? Constants::DEFAULT_SIZE_T_MAX_VALUE : i;
	}
}

void SingleCameraCalibration::KeepSameFocalLengthFix(const bool fix_same_focal_length)
{
	parameter_fixations_[1] = fix_same_focal_length ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 1;
}

void SingleCameraCalibration::KeepFocalLengthFix(const bool fix_focal_length)
{
	parameter_fixations_[0] = fix_focal_length ? Constants::DEFAULT_SIZE_T_MAX_VALUE : parameter_fixations_[0];
	parameter_fixations_[1] = fix_focal_length ? Constants::DEFAULT_SIZE_T_MAX_VALUE : parameter_fixations_[1];
}

void SingleCameraCalibration::KeepDepthDependentScalarParam1Fix(const bool fix_dd_scalar_param1_fix)
{
	parameter_fixations_[18] = fix_dd_scalar_param1_fix ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 18;
}

void SingleCameraCalibration::KeepDepthDependentScalarParam2Fix(const bool fix_dd_scalar_param2_fix)
{
	parameter_fixations_[19] = fix_dd_scalar_param2_fix ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 19;
}

void SingleCameraCalibration::KeepDepthDependentExpParam1Fix(const bool fix_dd_exp_param1_fix)
{
	parameter_fixations_[15] = fix_dd_exp_param1_fix ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 15;
}

void SingleCameraCalibration::KeepDepthDependentExpParam2Fix(const bool fix_dd_exp_param2_fix)
{
	parameter_fixations_[16] = fix_dd_exp_param2_fix ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 16;
}

void SingleCameraCalibration::KeepDepthDependentExpParam3Fix(const bool fix_dd_exp_param3_fix)
{
	parameter_fixations_[17] = fix_dd_exp_param3_fix ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 17;
}

//void SingleCameraCalibration2::UpdateInternalData()
//{
//	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED_IN_SCENE;
//}

void SingleCameraCalibration::InitParameterFixations()
{
	parameter_fixations_.resize(Constants::PARAMS_COUNT);

	for(auto i = 0; i < Constants::PARAMS_COUNT; ++i)
	{
		parameter_fixations_[i] = i;
	}
}

void SingleCameraCalibration::ProjectPoints(cv::InputArray& points3d,
                                            cv::OutputArray points2d,
                                            const bool if_apply_camera_extrinsics,
                                            const bool if_ignore_depth) const
{
	if(points3d.total() == 0)
	{
		points2d.clear();
		return;
	}
	cv::Mat rotation_matrix;
	cv::Mat translation_vector;

	if(if_apply_camera_extrinsics)
	{
		rotation_matrix    = rotation_matrix_.clone();
		translation_vector = translation_vector_.clone();
	}
	else
	{
		rotation_matrix    = cv::Mat::eye(3, 3, CV_64F);
		translation_vector = cv::Mat::zeros(3, 1, CV_64F);
	}

	if(points2d.depth() == cv::DataDepth<double>::value)
	{
		std::vector<cv::Point2d> points2d_local;

		cv::projectPoints(points3d,
		                  rotation_matrix,
		                  translation_vector,
		                  camera_info_.intrinsics.distortion_vector,
		                  camera_info_.intrinsics.distortion_vector,
		                  points2d_local);

		DistortToBaseModel(points2d_local, points2d);
	}
	else if(points2d.depth() == cv::DataDepth<float>::value)
	{
		std::vector<cv::Point2f> points2d_local;

		cv::projectPoints(points3d,
		                  rotation_matrix,
		                  translation_vector,
		                  camera_info_.intrinsics.distortion_vector,
		                  camera_info_.intrinsics.distortion_vector,
		                  points2d_local);
		DistortToBaseModel(points2d_local, points2d);
	}
	else
	{
		assert(false);
	}
}

cv::Point2f SingleCameraCalibration::ProjectPoint(const cv::Point3f& point,
                                                  const bool if_apply_camera_extrinsics) const
{
	cv::Mat rotation_matrix;
	cv::Mat translation_vector;

	if(if_apply_camera_extrinsics)
	{
		rotation_matrix    = rotation_matrix_.clone();
		translation_vector = translation_vector_.clone();
	}
	else
	{
		rotation_matrix    = cv::Mat::eye(3, 3, CV_64F);
		translation_vector = cv::Mat::zeros(3, 1, CV_64F);
	}

	std::vector<cv::Point3f> points3d(1, point);
	std::vector<cv::Point2f> points2d;
	std::vector<cv::Point2f> points2d_local;

	cv::projectPoints(points3d,
	                  rotation_matrix,
	                  translation_vector,
	                  camera_info_.intrinsics.distortion_vector,
	                  camera_info_.intrinsics.distortion_vector,
	                  points2d_local);
	DistortToBaseModel(points2d_local, points2d);
	return points2d.front();
}

void SingleCameraCalibration::GetInterpolatedRt(cv::Mat& rotation_vector_mean,
                                                cv::Mat& translation_vector_mean,
                                                SingleCameraCalibration& other) const
{
	// Depth system origin is in the middle of both IR cameras
	translation_vector_mean = (translation_vector_ + other.translation_vector_) * 0.5;

	// depth system orientation is half way SLERP interpolation between both camera orientations
	Eigen::Matrix3d r0, r1;

	for(auto row = 0; row < 3; ++row)
	{
		for(auto col = 0; col < 3; ++col)
		{
			r0(row, col) = rotation_matrix_.at<double>(row, col);
			r1(row, col) = other.rotation_matrix_.at<double>(row, col);
		}
	}

	Eigen::AngleAxisd a0, a1;
	Eigen::Quaterniond q0, q1;
	q0 = a0.fromRotationMatrix(r0);
	q1 = a1.fromRotationMatrix(r1);

	const auto q_depth           = q0.slerp(0.5, q1);
	const auto eigen_q_depth_rot = q_depth.toRotationMatrix();

	eigen2cv(eigen_q_depth_rot, rotation_vector_mean);
}

//void SingleCameraCalibration2::UndistortPoints(cv::InputArray points_2d,
//                                               cv::OutputArray out,
//                                               cv::InputArray points_3d) const
//{
//	UndistortPointsInternal(points_2d, out);
//}

void SingleCameraCalibration::UndistortPoints2(cv::InputArray points_2d,
                                               cv::OutputArray out,
                                               cv::InputArray points_3d) const
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
}

bool SingleCameraCalibration::HandlesDepthValues() const
{
	return false;
}

void SingleCameraCalibration::UndistortPointsInternal(cv::InputArray& points2d,
                                                      cv::OutputArray out,
                                                      cv::InputArray& rotation,
                                                      cv::InputArray& projection) const
{
	if(!(points2d.total() > 0))
		return;

	UndistortToBaseModel(points2d, out);
	const auto in_mat = out.getMat().clone();

	if(cv::norm(camera_info_.intrinsics.distortion_vector) < DBL_EPSILON)
		cv::undistortPoints(in_mat, out, camera_info_.intrinsics.distortion_vector, cv::Mat(), rotation, projection);
	else
		cv::undistortPoints(in_mat, out, camera_info_.intrinsics.distortion_vector, camera_info_.intrinsics.distortion_vector, rotation, projection);
}

void SingleCameraCalibration::UndistortToBaseModel(cv::InputArray& points2d,
                                                   cv::OutputArray out) const
{
	const auto points = points2d.getMat();

	if(!points.empty())
		points.copyTo(out);
}

void SingleCameraCalibration::DistortToBaseModel(cv::InputArray& points2d,
                                                 cv::OutputArray out) const
{
	const auto points = points2d.getMat();

	if(!points.empty())
		points.copyTo(out);
}

void SingleCameraCalibration::RayReconstruction(cv::InputArray points2d,
                                                cv::OutputArray out,
                                                const bool if_normalize) const
{
	std::vector<cv::Point2f> undistorted;
	UndistortPoints2(points2d, undistorted);

	//using opencv structures as described here: http://stackoverflow.com/questions/25750041/pass-opencv-inputarray-and-use-it-as-stdvector
	out.create(points2d.size(), out.type());
	cv::Mat out_mat = out.getMat();
	cv::Mat out_dbl(out_mat.size(), cv::DataType<cv::Point3f>::generic_type);
	cv::Point3f* data = (cv::Point3f*)out_dbl.data;
	cv::Point3f origin(0., 0., 0.);
	origin       = FromCameraToWorldCoordinateSystem(origin);
	size_t index = 0;

	for(const auto& point : undistorted)
	{
		// point in camera CS
		cv::Point3f point3(point.x, point.y, 1.0);

		// point in world CS
		point3 = FromCameraToWorldCoordinateSystem(point3);

		// direction From camera origin to point at 1m (in camera CS), this is the ray direction in world coordinates
		point3 = point3 - origin;

		if(if_normalize)
			point3 = point3 * (1. / cv::norm<float>(point3));

		data[index] = point3;

		index++;
	}

	out_dbl.convertTo(out_mat, out_mat.type());
}

void SingleCameraCalibration::GetCameraIntrinsicParams(cv::Mat& camera_matrix,
                                                       cv::Mat& distortion_vector) const
{
	camera_matrix = cv::Mat(3, 3, CV_64F);
	camera_matrix = camera_info_.intrinsics.camera_intrinsic_matrix.clone();

	distortion_vector = cv::Mat(5, 1, CV_64F);
	distortion_vector = camera_info_.intrinsics.distortion_vector.clone();
}

void SingleCameraCalibration::GetPlaneIntersectionPoints(cv::InputArray& pts2d,
                                                         const cv::Vec3d& plane_pos,
                                                         const cv::Vec3d& plane_normal,
                                                         cv::OutputArray intersection_pts_freestyle_cs) const
{
	intersection_pts_freestyle_cs.create(pts2d.size(), cv::DataType<cv::Point3f>::generic_type, -1, true);
	const auto out_mat = intersection_pts_freestyle_cs.getMat();
	cv::Point3f* data  = (cv::Point3f*)out_mat.data;

	// camera position in Freestyle coordinates as ray origin
	cv::Point3f origin(0., 0., 0.);
	origin = FromCameraToWorldCoordinateSystem(origin);
	const cv::Vec3d ray_location(origin.x, origin.y, origin.z);

	// 	Eigen::Vector3d egPlanePos( planePos.x, planePos.y, planePos.z );
	// 	Eigen::Vector3d egPlaneNormal( planeNormal.x, planeNormal.y, planeNormal.z );
	const auto d_hesse = plane_normal.dot(plane_pos);

	// calculate intersection Points of IR0 beams with plane detected by color camera
	std::vector<cv::Point3f> ray_directions;
	RayReconstruction(pts2d, ray_directions, true);

	for(auto idx = 0; idx < ray_directions.size(); ++idx)
	{
		cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
		const auto line_direction_scale = (d_hesse - plane_normal.dot(ray_location)) / plane_normal.dot(ray_direction);

		auto point = line_direction_scale * ray_direction + ray_location;
		data[idx]  = cv::Point3f(float(point(0)), float(point(1)), float(point(2)));
	}
}

bool SingleCameraCalibration::SolvePnP(cv::InputArray& object_points,
                                       cv::InputArray& image_points,
                                       cv::Mat& rotation_vector,
                                       cv::Mat& translation_vector,
                                       const bool if_use_extrinsic_guess,
                                       const int flags) const
{
	if(if_use_extrinsic_guess && (rotation_vector.total() != 3 || translation_vector.total() != 3))
		return false;

	assert(object_points.depth() == image_points.depth());
	assert((object_points.depth() == CV_32F) || (object_points.depth() == CV_64F));

	if(object_points.depth() == CV_32F)
	{
		std::vector<cv::Point2f> img_pts_local;
		UndistortToBaseModel(image_points, img_pts_local);
		return cv::solvePnP(object_points,
		                    img_pts_local,
		                    camera_info_.intrinsics.distortion_vector,
		                    camera_info_.intrinsics.distortion_vector,
		                    rotation_vector,
		                    translation_vector,
		                    if_use_extrinsic_guess,
		                    flags);
	}

	std::vector<cv::Point2d> img_pts_local;
	UndistortToBaseModel(image_points, img_pts_local);
	return cv::solvePnP(object_points,
	                    img_pts_local,
	                    camera_info_.intrinsics.distortion_vector,
	                    camera_info_.intrinsics.distortion_vector,
	                    rotation_vector,
	                    translation_vector,
	                    if_use_extrinsic_guess,
	                    flags);
}

double SingleCameraCalibration::StereoCalibrate(cv::InputArray object_points,
                                                cv::InputArrayOfArrays img_pts_this,
                                                cv::InputArrayOfArrays img_pts_other,
                                                std::shared_ptr<SingleCameraCalibration> other,
                                                const cv::Size size,
                                                cv::OutputArray rotation,
                                                cv::OutputArray translation,
                                                cv::OutputArray essential_matrix,
                                                cv::OutputArray fundamental_matrix,
                                                const cv::TermCriteria criteria,
                                                const int flags) const
{
	//we have to first correct the distorted points by the tabled based distortion term
	std::vector<std::vector<cv::Point2f>> image_points_this_local, image_points_other_local;
	std::vector<std::vector<cv::Point2f>> const* image_points_this_origin;
	std::vector<std::vector<cv::Point2f>> const* image_points_other_origin;

#if CV_MAJOR_VERSION == 3
	imgPtsThis_orig = (std::vector<std::vector<cv::Point2f>> const*)imgPtsThis.getObj();
	imgPtsOther_orig = (std::vector<std::vector<cv::Point2f>> const*)imgPtsOther.getObj();
#else
	image_points_this_origin  = (std::vector<std::vector<cv::Point2f>> const*)img_pts_this.getObj();
	image_points_other_origin = (std::vector<std::vector<cv::Point2f>> const*)img_pts_other.getObj();
#endif
	assert(image_points_this_origin->size() == image_points_other_origin->size());

	const auto image_points_size = image_points_this_origin->size();
	for(auto i = 0; i < image_points_size; ++i)
	{
		std::vector<cv::Point2f> points_this, points_other;
		UndistortToBaseModel(image_points_this_origin->at(i), points_this);
		other->UndistortToBaseModel(image_points_other_origin->at(i), points_other);
		image_points_this_local.push_back(points_this);
		image_points_other_local.push_back(points_other);
	}

	//we have to first correct the distorted points by the tabled based distortion term
	return cv::stereoCalibrate(object_points,
	                           image_points_this_local,
	                           image_points_other_local,
	                           camera_info_.intrinsics.distortion_vector,
	                           camera_info_.intrinsics.distortion_vector,
	                           other->camera_info_.intrinsics.distortion_vector,
	                           other->camera_info_.intrinsics.distortion_vector,
	                           size,
	                           rotation,
	                           translation,
	                           essential_matrix,
	                           fundamental_matrix,
	                           flags,
	                           criteria);
}

bool SingleCameraCalibration::GetBestFitCvModel(cv::Mat& camera_matrix,
                                                cv::Mat& distortion) const
{
	camera_info_.intrinsics.distortion_vector.copyTo(camera_matrix);
	camera_info_.intrinsics.distortion_vector.copyTo(distortion);
	return true;
}

void SingleCameraCalibration::SetIntrinsicParameters(const cv::Mat& camera_matrix,
                                                     const cv::Mat& distortion_vector)
{
	camera_info_.intrinsics.camera_intrinsic_matrix = camera_matrix;
	camera_info_.intrinsics.distortion_vector       = distortion_vector;
}

void SingleCameraCalibration::StereoRectify(const std::shared_ptr<const SingleCameraCalibration>& other,
                                            const cv::Size size,
                                            cv::InputArray& rotation,
                                            cv::InputArray& translation,
                                            cv::OutputArray rectification_transform1,
                                            cv::OutputArray rectification_transform2,
                                            cv::OutputArray projection_matrix1,
                                            cv::OutputArray projection_matrix2,
                                            cv::OutputArray disparity_to_depth_matrix,
                                            const int flags,
                                            const double alpha,
                                            const cv::Size new_image_size,
                                            cv::Rect* valid_pix_roi1,
                                            cv::Rect* valid_pix_roi2) const
{
	cv::stereoRectify(camera_info_.intrinsics.distortion_vector,
	                  camera_info_.intrinsics.distortion_vector,
	                  other->camera_info_.intrinsics.distortion_vector,
	                  other->camera_info_.intrinsics.distortion_vector,
	                  size,
	                  rotation,
	                  translation,
	                  rectification_transform1,
	                  rectification_transform2,
	                  projection_matrix1,
	                  projection_matrix2,
	                  disparity_to_depth_matrix,
	                  flags,
	                  alpha,
	                  new_image_size,
	                  valid_pix_roi1,
	                  valid_pix_roi2);
}

std::shared_ptr<const CameraSensor> SingleCameraCalibration::FromBinary(std::vector<char>::const_iterator& pos)
{
	CameraInfo camera_info = *(CameraInfo*)&(*pos);
	std::advance(pos, sizeof(camera_info));

	std::vector<double> params;
	CameraInfoToParamVector(camera_info, params);
	auto camera = std::make_shared<SingleCameraCalibration>();
	camera->InitFromParamsVector(params);

	return camera;
}

std::vector<double> SingleCameraCalibration::GetParamsVector() const
{
	std::vector<double> params(Constants::PARAMS_COUNT);
	/*auto basic_representation = GetBasicRepresentation();
	const auto start_value = &basic_representation.focal_length[0];

	for(auto i = 0; i < Constants::PARAMS_COUNT; ++i)
		params[i] = start_value[i];

	fixations = parameter_fixations_;*/

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	return params;
}

std::vector<size_t> SingleCameraCalibration::GetFixations() const
{
	return parameter_fixations_;
}

void SingleCameraCalibration::ToBinary(std::vector<char>& buffer) const
{
	std::vector<char> out;
	CalibrationHeader header{CALIBRATION_IDENT, 0, sizeof(CalibrationHeader)};
	buffer.insert(buffer.end(), (char*)&header, (char*)&header + sizeof(header));
	auto camera_info = GetCameraInfo();
	buffer.insert(buffer.end(), (char*)&camera_info, (char*)&camera_info + sizeof(camera_info));
}

size_t SingleCameraCalibration::StereoIntersect(std::shared_ptr<const SingleCameraCalibration> other,
                                                const std::vector<cv::Vec2f>& this_px_coords,
                                                const std::vector<cv::Vec2f>& other_px_coords,
                                                std::vector<cv::Vec3d>& points3d,
                                                std::vector<cv::Vec2f>* err2d) const
{
	// check corresponding pixel coordinate vectors size match
	if(other_px_coords.size() != this_px_coords.size())
		return -1;

	points3d.clear();

	if(err2d)
		err2d->clear();

	cv::Mat rotation_this, translation_this, rotation_other, translation_other;

	this->GetExtrinsicParameters(rotation_this, translation_this);
	other->GetExtrinsicParameters(rotation_other, translation_other);

	Eigen::Map<Eigen::Vector3d> p_translation_this((double*)(translation_this.data), 3, 1);
	Eigen::Map<Eigen::Vector3d> p_translation_other((double*)(translation_other.data), 3, 1);
	Eigen::Map<Eigen::Matrix3d> p_rotation_this((double*)(rotation_this.data), 3, 3);
	Eigen::Map<Eigen::Matrix3d> p_rotation_other((double*)(rotation_other.data), 3, 3);
	Eigen::Vector3d rotation_translation_this  = p_rotation_this * p_translation_this;
	Eigen::Vector3d rotation_translation_other = p_rotation_other * p_translation_other;

	std::vector<cv::Vec3f> direction_this, direction_other;
	this->RayReconstruction(this_px_coords, direction_this, true);
	other->RayReconstruction(other_px_coords, direction_other, true);
	const auto this_px_coords_size = this_px_coords.size();
	for(auto i = 0; i < this_px_coords_size; ++i)
	{
		Eigen::Matrix3d directions(Eigen::Matrix3d::Zero());
		Eigen::Vector3d positions(Eigen::Vector3d::Zero());

		Eigen::Vector3d w0(direction_this[i][0], direction_this[i][1], direction_this[i][2]);
		Eigen::Vector3d w1(direction_other[i][0], direction_other[i][1], direction_other[i][2]);

		Eigen::Matrix3d V0 = Eigen::Matrix3d::Identity() - (w0 * w0.transpose());
		Eigen::Matrix3d V1 = Eigen::Matrix3d::Identity() - (w1 * w1.transpose());

		directions += V0;
		directions += V1;

		positions -= V0 * rotation_translation_this;
		positions -= V1 * rotation_translation_other;

		Eigen::Vector3d t = directions.inverse() * positions;
		points3d.emplace_back(t[0], t[1], t[2]);
	}

	const auto points3d_size = points3d.size();

	if(err2d)
	{
		std::vector<cv::Point3f> points;
		for(auto i = 0; i < points3d_size; ++i)
		{
			points.emplace_back(float(points3d[i][0]), float(points3d[i][1]), float(points3d[i][2]));
		}

		std::vector<cv::Vec2f> pts2d;
		ProjectPoints(points, pts2d);

		for(auto i = 0; i < pts2d.size(); ++i)
			err2d->push_back(pts2d[i] - this_px_coords[i]);
	}

	return points3d_size;
}
}
}
