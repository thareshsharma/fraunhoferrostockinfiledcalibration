#include "camera_sensor.h"
#include "helper.h"
#include "transformation.h"

namespace DMVS
{
namespace CameraCalibration
{
CameraSensor::CameraSensor() :
	rotation_matrix_(cv::Mat::eye(3, 3, CV_64FC1)),
	translation_vector_(cv::Mat::zeros(3, 1, CV_64FC1)),
	transformation_matrix_(cv::Mat::zeros(4, 4, CV_64FC1)),
	image_channel_(CHANNEL_DEFAULT)
{
	UpdateTransformationMatrix(rotation_matrix_, translation_vector_);
}

CameraSensor::CameraSensor(const CameraInfo& camera_info) :
	rotation_matrix_(camera_info.extrinsics.rotation_matrix),
	translation_vector_(camera_info.extrinsics.translation_vector),
	transformation_matrix_(cv::Mat::zeros(4, 4, CV_64FC1)),
	camera_intrinsic_matrix_(camera_info.intrinsics.camera_intrinsic_matrix),
	image_channel_(camera_info.channel)
{
	UpdateTransformationMatrix(rotation_matrix_, translation_vector_);
}

CameraSensor::CameraSensor(const LaserInfo& laser_info) :
	rotation_matrix_(laser_info.extrinsics.rotation_matrix),
	translation_vector_(laser_info.extrinsics.translation_vector),
	transformation_matrix_(cv::Mat::zeros(4, 4, CV_64FC1)),
	camera_intrinsic_matrix_(laser_info.intrinsics.projector_intrinsic_matrix),
	image_channel_(laser_info.channel)
{
	UpdateTransformationMatrix(rotation_matrix_, translation_vector_);
}

CameraSensor::CameraSensor(const cv::Mat& transformation_matrix) :
	transformation_matrix_(transformation_matrix),
	image_channel_(CHANNEL_DEFAULT)
{
	UpdateExtrinsicParameters(transformation_matrix);
}

CameraSensor::CameraSensor(const cv::Mat& rotation_matrix,
                           const cv::Mat& translation_vector,
                           const ImageChannel image_channel):
	rotation_matrix_(rotation_matrix),
	translation_vector_(translation_vector),
	transformation_matrix_(cv::Mat::zeros(4, 4, CV_64FC1)),
	image_channel_(image_channel)
{
	UpdateTransformationMatrix(rotation_matrix_, translation_vector_);
}

CameraSensor::CameraSensor(const CameraSensor& rhs)
	: enable_shared_from_this(rhs)
{
	if(this != &rhs)
	{
		rotation_matrix_         = rhs.rotation_matrix_;
		translation_vector_      = rhs.translation_vector_;
		transformation_matrix_   = rhs.transformation_matrix_;
		camera_intrinsic_matrix_ = rhs.camera_intrinsic_matrix_;
		distortion_vector_       = rhs.distortion_vector_;
		image_channel_           = rhs.image_channel_;
		parameter_fixations_     = rhs.parameter_fixations_;
		parameters_              = rhs.parameters_;
	}
}

CameraSensor& CameraSensor::operator=(const CameraSensor& rhs)
{
	if(this != &rhs)
	{
		rotation_matrix_         = rhs.rotation_matrix_;
		translation_vector_      = rhs.translation_vector_;
		transformation_matrix_   = rhs.transformation_matrix_;
		camera_intrinsic_matrix_ = rhs.camera_intrinsic_matrix_;
		distortion_vector_       = rhs.distortion_vector_;
		image_channel_           = rhs.image_channel_;
		parameter_fixations_     = rhs.parameter_fixations_;
		parameters_              = rhs.parameters_;
	}

	return *this;
}

CameraSensor::CameraSensor(CameraSensor&& rhs) noexcept
{
	if(this != &rhs)
	{
		rotation_matrix_         = rhs.rotation_matrix_;
		translation_vector_      = rhs.translation_vector_;
		transformation_matrix_   = rhs.transformation_matrix_;
		camera_intrinsic_matrix_ = rhs.camera_intrinsic_matrix_;
		distortion_vector_       = rhs.distortion_vector_;
		image_channel_           = rhs.image_channel_;
		parameter_fixations_     = rhs.parameter_fixations_;
		parameters_              = rhs.parameters_;

		// Free rhs from its values
		rhs.rotation_matrix_         = cv::Mat::zeros(3, 3, 0);
		rhs.translation_vector_      = cv::Mat::zeros(3, 1, 0);
		rhs.transformation_matrix_   = cv::Mat::zeros(4, 4, 0);
		rhs.camera_intrinsic_matrix_ = cv::Mat::zeros(3, 3, 0);
		rhs.distortion_vector_       = cv::Mat::zeros(5, 1, 0);
		rhs.image_channel_           = CHANNEL_DEFAULT;
		rhs.parameter_fixations_.clear();
		rhs.parameters_.clear();
	}
}

CameraSensor& CameraSensor::operator=(CameraSensor&& rhs) noexcept
{
	if(this != &rhs)
	{
		rotation_matrix_         = rhs.rotation_matrix_;
		translation_vector_      = rhs.translation_vector_;
		transformation_matrix_   = rhs.transformation_matrix_;
		camera_intrinsic_matrix_ = rhs.camera_intrinsic_matrix_;
		distortion_vector_       = rhs.distortion_vector_;
		image_channel_           = rhs.image_channel_;
		parameter_fixations_     = rhs.parameter_fixations_;
		parameters_              = rhs.parameters_;

		// Free rhs from its values
		rhs.rotation_matrix_       = cv::Mat::zeros(3, 3, 0);
		rhs.translation_vector_    = cv::Mat::zeros(3, 1, 0);
		rhs.transformation_matrix_ = cv::Mat::zeros(4, 4, 0);
		rhs.image_channel_         = CHANNEL_DEFAULT;
		rhs.parameter_fixations_.clear();
		rhs.parameters_.clear();
	}

	return *this;
}

CameraSensor::~CameraSensor() = default;

cv::Point3f CameraSensor::FromCameraToWorldCoordinateSystem(const cv::Point3f& point) const
{
	// First move the point back
	const cv::Mat point_mat           = (cv::Mat_<double>(3, 1) << point.x, point.y, point.z);
	const auto translated_back_vector = point_mat - translation_vector_;

	// Multiply with inverse of rotation matrix
	const auto x = GetValue<float>(translated_back_vector, 0, 3, 6);
	const auto y = GetValue<float>(translated_back_vector, 1, 4, 7);
	const auto z = GetValue<float>(translated_back_vector, 2, 5, 8);

	return {x, y, z};
}

cv::Point3f CameraSensor::FromCameraToWorldCoordinateSystem(const cv::Point3f& point,
                                                            cv::Mat rotation_matrix,
                                                            cv::Mat translation_vector) const
{
	// First move the point back
	const cv::Mat point_mat           = (cv::Mat_<double>(3, 1) << point.x, point.y, point.z);
	const auto translated_back_vector = point_mat - translation_vector;

	// Multiply with inverse of rotation matrix
	const auto x = GetValue<float>(translated_back_vector, 0, 3, 6, rotation_matrix);
	const auto y = GetValue<float>(translated_back_vector, 1, 4, 7, rotation_matrix);
	const auto z = GetValue<float>(translated_back_vector, 2, 5, 8, rotation_matrix);

	return {x, y, z};
}

cv::Point3d CameraSensor::FromCameraToWorldCoordinateSystem(const cv::Point3d& point) const
{
	// First move the point back
	const cv::Mat point_mat           = (cv::Mat_<double>(3, 1) << point.x, point.y, point.z);
	const auto translated_back_vector = point_mat - translation_vector_;

	// Multiply with inverse of rotation matrix
	const auto x = GetValue<double>(translated_back_vector, 0, 3, 6);
	const auto y = GetValue<double>(translated_back_vector, 1, 4, 7);
	const auto z = GetValue<double>(translated_back_vector, 2, 5, 8);

	return {x, y, z};
}

Eigen::Vector3f CameraSensor::FromCameraToWorldCoordinateSystem(const Eigen::Vector3f& eigen_vector) const
{
	const Eigen::Vector3d point_double = eigen_vector.cast<double>();

	return FromCameraToWorldCoordinateSystem(point_double).cast<float>();
}

Eigen::Matrix3f CameraSensor::FromCameraToWorldCoordinateSystem(const Eigen::Matrix3f& eigen_matrix) const
{
	const Eigen::Matrix3d matrix_double = eigen_matrix.cast<double>();

	return FromCameraToWorldCoordinateSystem(matrix_double).cast<float>();
}

Eigen::Vector3d CameraSensor::FromCameraToWorldCoordinateSystem(const Eigen::Vector3d& eigen_vector) const
{
	Eigen::Matrix3d rotation;
	cv::cv2eigen(rotation_matrix_, rotation);

	//_R should be orthonormal. Inverse == transposed
	return rotation.transpose() * (eigen_vector - Eigen::Map<Eigen::Vector3d>(reinterpret_cast<double*>(translation_vector_.data)));
}

Eigen::Matrix3d CameraSensor::FromCameraToWorldCoordinateSystem(const Eigen::Matrix3d& eigen_matrix) const
{
	Eigen::Matrix3d rotation;
	cv::cv2eigen(rotation_matrix_, rotation);

	//_R should be orthonormal. Inverse == transposed
	return rotation.transpose() * eigen_matrix;
}

cv::Point3f CameraSensor::FromWorldCoordinateSystemToCamera(const cv::Point3f& point) const
{
	Eigen::Matrix3f rotation;
	cv::cv2eigen(rotation_matrix_, rotation);

	const Eigen::Map<Eigen::Vector3f> translation_projection(reinterpret_cast<float*>(translation_vector_.data));
	const Eigen::Vector3f p(point.x, point.y, point.z);

	const Eigen::Vector3f projection_new = rotation * p + translation_projection;
	return cv::Point3f(projection_new[0], projection_new[1], projection_new[2]);
}

cv::Point3d CameraSensor::FromWorldCoordinateSystemToCamera(const cv::Point3d& point,
                                                            const bool if_log) const
{
	Eigen::Matrix3d rotation;

	cv::cv2eigen(rotation_matrix_, rotation);
	const Eigen::Map<Eigen::Vector3d> translation_projection(reinterpret_cast<double*>(translation_vector_.data));
	const Eigen::Vector3d eigen_point(point.x, point.y, point.z);

	Eigen::Vector3d projection_new = rotation * eigen_point + translation_projection;

	//if(if_log)
	//{
	//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : projection_point " << projection_new;
	//}

	auto x = projection_new.x();
	auto y = projection_new.y();
	auto z = projection_new.z();

	cv::Point3d output_point(x,y,z);

	//return {(projection_new.x(), projection_new.y(), projection_new.z())};
	return output_point;
}

Eigen::Vector3f CameraSensor::FromWorldCoordinateSystemToCamera(const Eigen::Vector3f& eigen_vector) const
{
	const Eigen::Vector3d vector_double = eigen_vector.cast<double>();
	return FromWorldCoordinateSystemToCamera(vector_double).cast<float>();
}

Eigen::Matrix3f CameraSensor::FromWorldCoordinateSystemToCamera(const Eigen::Matrix3f& eigen_matrix) const
{
	const Eigen::Matrix3d matrix_double = eigen_matrix.cast<double>();
	return FromWorldCoordinateSystemToCamera(matrix_double).cast<float>();
}

Eigen::Vector3d CameraSensor::FromWorldCoordinateSystemToCamera(const Eigen::Vector3d& eigen_vector) const
{
	Eigen::Matrix3d rotation;
	cv::cv2eigen(rotation_matrix_, rotation);

	const Eigen::Map<Eigen::Vector3d> vector_double(reinterpret_cast<double*>(translation_vector_.data));
	return rotation * eigen_vector + vector_double;
}

Eigen::Matrix3d CameraSensor::FromWorldCoordinateSystemToCamera(const Eigen::Matrix3d& eigen_matrix) const
{
	Eigen::Matrix3d rotation;
	cv::cv2eigen(rotation_matrix_, rotation);
	return rotation * eigen_matrix;
}

cv::Vec2d CameraSensor::ProjectPoint(const cv::Vec3d& point_3d,
                                     const bool apply_camera_extrinsics) const
{
	cv::Vec2d out;
	cv::Mat arg1(1, 1, CV_64FC3, (double*)&point_3d);
	cv::Mat arg2(1, 1, CV_64FC2, &out);
	ProjectPoints(arg1, arg2, apply_camera_extrinsics, false);

	return out;
}

cv::Vec2d CameraSensor::UndistortPoint(const cv::Vec2d& point_pixel,
                                       const cv::Vec3d& point_depth) const
{
	std::vector<cv::Vec2d> cv_pix;
	std::vector<cv::Vec2d> cv_pt;
	cv_pix.push_back(point_pixel);

	if(point_depth.channels != 0)
	{
		UndistortPoints2(cv_pix, cv_pt);
	}
	else
	{
		std::vector<cv::Vec3d> cv_depth;
		cv_depth.push_back(point_depth);

		UndistortPoints2(cv_pix, cv_pt, cv_depth);
	}

	return cv::Vec2d(cv_pt.front()[0], cv_pt.front()[1]);
}

bool CameraSensor::HandlesDepthValues() const
{
	return false;
}

void CameraSensor::GetTransformationMatrix(cv::Mat& transformation) const
{
	transformation = cv::Mat::zeros(4, 4, CV_64FC1);

	for(auto row = 0; row < 3; ++row)
	{
		for(auto col = 0; col < 3; ++col)
		{
			transformation.at<double>(row, col) = rotation_matrix_.at<double>(row, col);
		}
	}

	transformation.at<double>(0, 3) = translation_vector_.at<double>(0, 0);
	transformation.at<double>(1, 3) = translation_vector_.at<double>(1, 0);
	transformation.at<double>(2, 3) = translation_vector_.at<double>(2, 0);
	transformation.at<double>(3, 3) = 1;
}

void CameraSensor::SetTransformationMatrix(const cv::Mat& transformation)
{
	assert(transformation.rows == 4 && transformation.cols == 4);

	for(auto row = 0; row < 3; ++row)
	{
		for(auto col = 0; col < 3; ++col)
		{
			rotation_matrix_.at<double>(row, col) = transformation.at<double>(row, col);
		}
	}

	translation_vector_.at<double>(0, 0) = transformation.at<double>(0, 3);
	translation_vector_.at<double>(1, 0) = transformation.at<double>(1, 3);
	translation_vector_.at<double>(2, 0) = transformation.at<double>(2, 3);
}


void CameraSensor::ResetOrigin(const cv::Mat& transformation)
{
	cv::Mat current_transformation;
	GetTransformationMatrix(current_transformation);
	const auto new_transformation = transformation.inv() * current_transformation;
	SetTransformationMatrix(new_transformation);
}

void CameraSensor::SetExtrinsicParameters(const cv::Mat& rotation_matrix,
                                          const cv::Mat& translation_vector)
{
	cv::Mat rot_mat;
	if(rotation_matrix.cols == 1 || rotation_matrix.rows == 1)
	{
		cv::Rodrigues(rotation_matrix, rot_mat);
	}
	{
		assert(rotation_matrix.cols == 3);
		rot_mat = rotation_matrix.clone();
	}

	rotation_matrix_    = rot_mat.clone();
	translation_vector_ = translation_vector.clone();
}

void CameraSensor::GetExtrinsicParameters(cv::Mat& rotation_matrix,
                                          cv::Mat& translation_vector) const
{
	rotation_matrix    = rotation_matrix_.empty() ? cv::Mat::eye(3, 3, CV_64F) : rotation_matrix_.clone();
	translation_vector = translation_vector_.empty() ? cv::Mat::zeros(3, 1, CV_64F) : translation_vector_.clone();
}

bool CameraSensor::GetBestFitCVModel(cv::Mat& camera_matrix,
                                     cv::Mat& distortion_vector) const
{
	return false;
}

bool CameraSensor::GetBestFitCVModelLaser(cv::Mat& camera_matrix,
                                          cv::Mat& distortion_vector) const
{
	return false;
}

ImageChannel CameraSensor::GetImageChannel() const
{
	return image_channel_;
}

void CameraSensor::SetImageChannel(const ImageChannel image_channel)
{
	image_channel_ = image_channel;
}

void CameraSensor::SetFreeParameter(const size_t free_parameters)
{
	if(free_parameters >= parameter_fixations_.size())
		return;

	parameter_fixations_[free_parameters] = free_parameters;
}

void CameraSensor::SetFixedParameter(const size_t fixed_param)
{
	if(fixed_param >= parameter_fixations_.size())
		return;

	parameter_fixations_[fixed_param] = fixed_param;
}

void CameraSensor::SetAllParametersFixed()
{
	std::fill(parameter_fixations_.begin(), parameter_fixations_.end(), Constants::DEFAULT_SIZE_T_MAX_VALUE);
}

std::shared_ptr<const CameraSensor> CameraSensor::CreateFromBinary(std::vector<char>::const_iterator& pos)
{
	//const auto header = (CalibrationHeader*)(&(*pos));

	//if(header->magic_number != CALIB_IDENT)
	//{
	//	return SingleCameraCalibration::FromBinary(pos);
	//}

	//std::advance(pos, header->static_data_offset);

	//switch(header->major_version)
	//{
	//case 0:
	//	return SingleCameraCalibration::FromBinary(pos);
	//case 1:
	//	return SingleCameraCalibration_01::FromBinary(pos);
	//case 2:
	//	return CameraSensor2DepthDependent::FromBinary(pos);

	//default:
	//	return nullptr;
	//}
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;

	return nullptr;
}

bool CameraSensor::SolvePnP(cv::InputArray object_points,
                            cv::InputArray image_points,
                            cv::Mat& rotation_vector,
                            cv::Mat& translation_vector,
                            const bool if_use_extrinsic_guess,
                            const int flags) const
{
	// New approach: use pinhole focal length 1 coordinates, so solvePNP does not need the undistortion model
	if(if_use_extrinsic_guess && (rotation_vector.total() != 3 || translation_vector.total() != 3))
		return false;

	cv::Mat undistorted_points;
	UndistortPoints2(image_points, undistorted_points);

	const auto first_solve_ok = cv::solvePnP(object_points,
	                                         undistorted_points,
	                                         cv::Mat::eye(3, 3, CV_64F),
	                                         cv::Mat(),
	                                         rotation_vector,
	                                         translation_vector,
	                                         if_use_extrinsic_guess,
	                                         cv::SOLVEPNP_ITERATIVE);

	if(first_solve_ok && HandlesDepthValues())
	{
		cv::Mat object_points_mat = object_points.getMat();
		const auto data_float     = (CvPoint3D32f*)object_points_mat.data;
		const auto data_double    = (CvPoint3D64f*)object_points_mat.data;
		const auto stype          = CV_MAT_TYPE(object_points_mat.type());
		const auto sstep          = object_points_mat.rows == 1 ? 1 : object_points_mat.step / CV_ELEM_SIZE(stype);
		auto n                    = object_points_mat.rows + object_points_mat.cols - 1;

		std::vector<cv::Point3d> points_3d(n);

		if(stype == CV_32FC3)
		{
			for(auto i = 0; i < n; ++i)
			{
				points_3d[i].x = data_float[i * sstep].x;
				points_3d[i].y = data_float[i * sstep].y;
				points_3d[i].z = data_float[i * sstep].z;
			}
		}
		else
		{
			for(auto i = 0; i < n; ++i)
			{
				points_3d[i].x = data_double[i * sstep].x;
				points_3d[i].y = data_double[i * sstep].y;
				points_3d[i].z = data_double[i * sstep].z;
			}
		}

		const auto local_points = Helper::ApplyTransformation(rotation_vector, translation_vector, points_3d);
		UndistortPoints2(image_points, undistorted_points, local_points);

		return cv::solvePnP(object_points,
		                    undistorted_points,
		                    cv::Mat::eye(3, 3, CV_64F),
		                    cv::Mat(),
		                    rotation_vector,
		                    translation_vector,
		                    true,
		                    cv::SOLVEPNP_ITERATIVE);
	}

	//TODO second run with depth undistorted points if depth dependency supported by model
	return first_solve_ok;
}

size_t CameraSensor::StereoIntersect(std::shared_ptr<const CameraSensor> other,
                                     const Dots& this_px_coords,
                                     const Dots& other_px_coords,
                                     std::vector<cv::Vec3d>& points_3d,
                                     std::vector<cv::Vec2f>* error_2d,
                                     bool ignore_depth) const
{
	//check corresponding pixel coordinate vectors size match
	if(other_px_coords.size() != this_px_coords.size())
	{
		return Constants::DEFAULT_SIZE_T_MAX_VALUE;
	}

	//if depth dependence supported and 3d data available:
	//copy 3d data to cv type structure for depth dependent undistortion later
	auto handles_depth_dependency = (this->HandlesDepthValues() || other->HandlesDepthValues()) && !ignore_depth;
	std::vector<cv::Point3d> points_3d_back_up;

	if(points_3d.size() == this_px_coords.size() && handles_depth_dependency)
	{
		for(auto point : points_3d)
		{
			points_3d_back_up.emplace_back(cv::Point3d(point));
		}
	}

	points_3d.clear();

	if(error_2d)
		error_2d->clear();

	cv::Mat rotation_this, translation_this, rotation_other, translation_other;

	this->GetExtrinsicParameters(rotation_this, translation_this);
	other->GetExtrinsicParameters(rotation_other, translation_other);

	Eigen::Map<Eigen::Vector3d> p_this((double*)(translation_this.data), 3, 1);
	Eigen::Map<Eigen::Vector3d> p_other((double*)(translation_other.data), 3, 1);
	Eigen::Map<Eigen::Matrix3d> r_this((double*)(rotation_this.data), 3, 3);
	Eigen::Map<Eigen::Matrix3d> r_other((double*)(rotation_other.data), 3, 3);

	Eigen::Vector3d rpThis  = r_this * p_this;
	Eigen::Vector3d rpOther = r_other * p_other;

	// if depth dependence supported and 3d data not yet available:
	// create initial 3d data for depth dependent undistortion (within ray reconstruction)
	if(handles_depth_dependency && points_3d_back_up.empty())
	{
		std::vector<cv::Vec3f> dir_this, dir_other;
		this->RayReconstruction2(this_px_coords, dir_this, true);
		other->RayReconstruction2(other_px_coords, dir_other, true);

		for(size_t i = 0; i < this_px_coords.size(); ++i)
		{
			Eigen::Matrix3d directions(Eigen::Matrix3d::Zero());
			Eigen::Vector3d positions(Eigen::Vector3d::Zero());

			Eigen::Vector3d w0(dir_this[i][0], dir_this[i][1], dir_this[i][2]);
			Eigen::Vector3d w1(dir_other[i][0], dir_other[i][1], dir_other[i][2]);

			Eigen::Matrix3d v0 = Eigen::Matrix3d::Identity() - w0 * w0.transpose();
			Eigen::Matrix3d v1 = Eigen::Matrix3d::Identity() - w1 * w1.transpose();

			directions += v0;
			directions += v1;

			positions -= v0 * rpThis;
			positions -= v1 * rpOther;

			Eigen::Vector3d t = directions.inverse() * positions;
			points_3d_back_up.emplace_back(cv::Point3d(t[0], t[1], t[2]));
		}
	}

	// Run final 3d point calculation based on depth dependent undistorted rays
	std::vector<cv::Vec3f> dir_this, dir_other;
	this->RayReconstruction2(this_px_coords, dir_this, true, points_3d_back_up);
	other->RayReconstruction2(other_px_coords, dir_other, true, points_3d_back_up);

	for(size_t i = 0; i < this_px_coords.size(); ++i)
	{
		Eigen::Matrix3d directions(Eigen::Matrix3d::Zero());
		Eigen::Vector3d positions(Eigen::Vector3d::Zero());

		Eigen::Vector3d w0(dir_this[i][0], dir_this[i][1], dir_this[i][2]);
		Eigen::Vector3d w1(dir_other[i][0], dir_other[i][1], dir_other[i][2]);

		Eigen::Matrix3d v0 = Eigen::Matrix3d::Identity() - (w0 * w0.transpose());
		Eigen::Matrix3d v1 = Eigen::Matrix3d::Identity() - (w1 * w1.transpose());

		directions += v0;
		directions += v1;

		positions -= v0 * rpThis;
		positions -= v1 * rpOther;

		Eigen::Vector3d t = directions.inverse() * positions;
		points_3d.emplace_back(cv::Vec3d(t[0], t[1], t[2]));
	}

	if(error_2d)
	{
		std::vector<cv::Point3d> points;

		for(size_t i = 0; i < points_3d.size(); ++i)
			points.emplace_back(cv::Point3d(points_3d[i][0], points_3d[i][1], points_3d[i][2]));

		Dots points_2d;
		ProjectPoints(points, points_2d);

		for(size_t i = 0; i < points_2d.size(); ++i)
		{
			cv::Point2f point_float(points_2d[i].x, points_2d[i].y);
			error_2d->push_back(point_float - this_px_coords[i]);
		}
	}

	return points_3d.size();
}

void CameraSensor::RayReconstruction(cv::InputArray points_2d,
                                     cv::OutputArray out,
                                     const bool normalize,
                                     cv::InputArray points3d) const
{
	std::vector<cv::Point2f> undistorted_points;
	UndistortPoints2(points_2d, undistorted_points, points3d);

	//Using opencv structures as described here:
	//http://stackoverflow.com/questions/25750041/pass-opencv-inputarray-and-use-it-as-stdvector
	out.create(points_2d.size(), out.type());
	cv::Mat out_mat = out.getMat();
	const cv::Mat out_double(out_mat.size(), out.type());
	const auto data = reinterpret_cast<cv::Point3f*>(out_double.data);
	//const auto origin(FromCameraToWorldCoordinateSystem(cv::Point3f(0., 0., 0.)));
	size_t index = 0;

	for(auto row = 0; row < undistorted_points.size(); ++row)
	{
		const auto point_x = undistorted_points[row].x;
		const auto point_y = undistorted_points[row].y;

		//point in camera CS
		cv::Point3f point3(point_x, point_y, 1.0);

		//point in world CS
		//point3 = FromCameraToWorldCoordinateSystem(point3);

		//direction from camera origin to point at 1m (in camera CS), this is the ray direction in world coordinates
		//point3 = point3 - origin;

		if(normalize)
		{
			point3 = point3 * (1. / cv::norm<float>(point3));
		}

		data[index] = point3;
		++index;
	}

	out_double.convertTo(out_mat, out_mat.type());
}

void CameraSensor::RayReconstruction2(cv::InputArray points_2d,
                                      cv::OutputArray out,
                                      const bool normalize,
                                      cv::InputArray points3d) const
{
	std::vector<cv::Point2f> undistorted;
	UndistortPoints2(points_2d, undistorted, points3d);

	//Using opencv structures as described here:
	//http://stackoverflow.com/questions/25750041/pass-opencv-inputarray-and-use-it-as-stdvector
	out.create(points_2d.size(), out.type());

	cv::Mat out_mat = out.getMat();
	const cv::Mat out_double(out_mat.size(), out.type());
	const auto data = reinterpret_cast<cv::Point3f*>(out_double.data);
	const auto origin(FromCameraToWorldCoordinateSystem(cv::Point3f(0., 0., 0.)));
	size_t index = 0;

	for(const auto& point : undistorted)
	{
		//point in camera CS
		cv::Point3f point3(point.x, point.y, 1.0);

		//point in world CS
		point3 = FromCameraToWorldCoordinateSystem(point3);

		//direction from camera origin to point at 1m (in camera CS), this is the ray direction in world coordinates
		point3 = point3 - origin;

		if(normalize)
		{
			point3 = point3 * (1. / cv::norm<float>(point3));
		}

		data[index] = point3;
		++index;
	}

	out_double.convertTo(out_mat, out_mat.type());
}

void CameraSensor::GetPlaneIntersectionPoints(cv::InputArray points_2d,
                                              const cv::Vec3d& plane_position,
                                              const cv::Vec3d& plane_normal,
                                              cv::OutputArray intersection_points_camera_cs) const
{
	// Plane position and plane normal are in camera coordinate system
	intersection_points_camera_cs.create(points_2d.size(), intersection_points_camera_cs.type(), -1, true);
	const auto out_matrix = intersection_points_camera_cs.getMat();
	const auto data       = reinterpret_cast<cv::Point3f*>(out_matrix.data);

	//Camera position in Freestyle coordinates as ray origin
	// cv::Point3f origin(0., 0., 0.);
	//origin = FromCameraToWorldCoordinateSystem(origin);
	// const cv::Vec3d ray_location(origin.x, origin.y, origin.z);

	////Eigen::Vector3d egPlanePos( planePos.x, planePos.y, planePos.z );
	////Eigen::Vector3d egPlaneNormal( planeNormal.x, planeNormal.y, planeNormal.z );
	//cv::Vec3d plane_normalized;
	//cv::normalize(plane_normal, plane_normalized);
	//const auto d_hesse = plane_normalized.dot(plane_position);

	////Calculate intersection Points of IR0 beams with plane detected by color camera
	//std::vector<cv::Point3f> ray_directions;
	//RayReconstruction(points_2d, ray_directions, true);

	//std::vector<cv::Point3f> points_3d;

	//for(size_t idx = 0; idx < ray_directions.size(); ++idx)
	//{
	//	cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
	//	const auto line_direction_scale = (d_hesse - plane_normalized.dot(ray_location)) / plane_normalized.dot(ray_direction);
	//	auto point                      = line_direction_scale * ray_direction + ray_location;
	//	data[idx]                       = cv::Point3f(point[0], point[1], point[2]);
	//	points_3d.push_back(data[idx]);
	//}

	cv::Point3f origin(0., 0., 0.);
	origin = FromCameraToWorldCoordinateSystem(origin);
	const cv::Vec3d ray_location(origin.x, origin.y, origin.z);
	cv::Vec3d plane_normalized;
	cv::normalize(plane_normal, plane_normalized);
	const auto d_hesse = plane_normalized.dot(plane_position);

	//Calculate intersection Points of IR0 beams with plane detected by color camera
	std::vector<cv::Point3f> ray_directions;
	RayReconstruction2(points_2d, ray_directions, true);

	std::vector<cv::Point3f> points_3d;

	for(size_t idx = 0; idx < ray_directions.size(); ++idx)
	{
		cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
		const auto line_direction_scale = (d_hesse - plane_normalized.dot(ray_location)) / plane_normalized.dot(ray_direction);
		auto point                      = (line_direction_scale * ray_direction) + ray_location;
		auto temp_point                 = cv::Point3f(point[0], point[1], point[2]);
		data[idx]                       = temp_point; // FromCameraToWorldCoordinateSystem(temp_point);
		points_3d.push_back(data[idx]);
	}

	//second run if sensor supports depth dependent un-distortion
	if(HandlesDepthValues())
	{
		//ray_directions.clear();
		//RayReconstruction(points_2d, ray_directions, true, points_3d);
		//for(size_t idx = 0; idx < ray_directions.size(); ++idx)
		//{
		//	cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
		//	const auto line_direction_scale = (d_hesse - plane_normal.dot(ray_location)) / plane_normal.dot(ray_direction);
		//	auto point                      = line_direction_scale * ray_direction + ray_location;
		//	data[idx]                       = cv::Point3f(point[0], point[1], point[2]);
		//}
	}
}

void CameraSensor::UpdateIntrinsicParameters(const cv::Mat& camera_matrix,
                                             const cv::Mat& distortion_vector)
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	/*camera_info_.intrinsics.camera_intrinsic_matrix = camera_matrix;
	camera_info_.intrinsics.distortion_vector = distortion_vector;*/
}

void CameraSensor::UpdateTransformationMatrix(const cv::Mat& rotation_matrix,
                                              const cv::Mat& translation_vector)
{
	const auto rows = rotation_matrix.rows;
	const auto cols = rotation_matrix.cols;

	for(auto row = 0; row < rows; ++row)
	{
		for(auto col = 0; col < cols; ++col)
		{
			transformation_matrix_.at<double>(row, col) = rotation_matrix.at<double>(row, col);
		}
	}

	transformation_matrix_.at<double>(0, 3) = translation_vector.at<double>(0, 0);
	transformation_matrix_.at<double>(1, 3) = translation_vector.at<double>(1, 0);
	transformation_matrix_.at<double>(2, 3) = translation_vector.at<double>(2, 0);
}

void CameraSensor::UpdateExtrinsicParameters(const cv::Mat4d& transformation_matrix)
{
	const auto rows = transformation_matrix.rows;
	const auto cols = transformation_matrix.cols;

	for(auto row = 0; row < rows - 1; ++row)
	{
		for(auto col = 0; col < cols - 1; ++col)
		{
			rotation_matrix_.at<double>(row, col) = transformation_matrix.at<double>(row, col);
		}
	}

	translation_vector_.at<double>(0, 0) = transformation_matrix.at<double>(0, 3);
	translation_vector_.at<double>(1, 0) = transformation_matrix.at<double>(1, 3);
	translation_vector_.at<double>(2, 0) = transformation_matrix.at<double>(2, 3);
}

//CameraSensor2::CameraSensor2(std::shared_ptr<const CameraSensor2> source)
//{
//	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << Constants::FUNCTION_NOT_IMPLEMENTED;
//}
}
}
