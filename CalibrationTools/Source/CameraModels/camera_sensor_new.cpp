#include "camera_sensor_new.h"
#include "helper.h"

namespace DMVS
{
namespace CameraCalibration
{
CameraSensorNew::CameraSensorNew() :
	Sensor(cv::Mat::eye(3, 3, CV_64FC1),
	       cv::Mat::zeros(3, 1, CV_64FC1),
	       CHANNEL_DEFAULT),
	intrinsic_matrix_(cv::Mat::zeros(3, 3, CV_64FC1)),
	distortion_vector_(cv::Mat::zeros(5, 1, CV_64FC1))
{
	SyncTransformationMatrix();
}

CameraSensorNew::CameraSensorNew(const CameraInfo& camera_info) :
	Sensor(camera_info.extrinsics.rotation_matrix,
	       camera_info.extrinsics.translation_vector,
	       camera_info.channel),
	intrinsic_matrix_(camera_info.intrinsics.camera_intrinsic_matrix),
	distortion_vector_(camera_info.intrinsics.distortion_vector)
{
	SyncTransformationMatrix();
}

CameraSensorNew::CameraSensorNew(const cv::Mat4d& transformation_matrix) :
	Sensor(transformation_matrix,
	       CHANNEL_DEFAULT)
{
	SyncExtrinsicParameters();
}

CameraSensorNew::CameraSensorNew(const cv::Mat& rotation_matrix,
                                 const cv::Mat& translation_vector,
                                 const ImageChannel image_channel):
	Sensor(rotation_matrix,
	       translation_vector,
	       image_channel)
{
	SyncTransformationMatrix();
}

CameraSensorNew::CameraSensorNew(const CameraSensorNew& rhs)
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;
	}
}

CameraSensorNew& CameraSensorNew::operator=(const CameraSensorNew& rhs)
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;
	}

	return *this;
}

CameraSensorNew::CameraSensorNew(CameraSensorNew&& rhs) noexcept
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;

		// Free rhs from its values
		rhs.rotation_matrix_       = cv::Mat::zeros(3, 3, 0);
		rhs.translation_vector_    = cv::Mat::zeros(3, 1, 0);
		rhs.transformation_matrix_ = cv::Mat::zeros(4, 4, 0);
		rhs.image_channel_         = CHANNEL_DEFAULT;
	}
}

CameraSensorNew& CameraSensorNew::operator=(CameraSensorNew&& rhs) noexcept
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;

		// Free rhs from its values
		rhs.rotation_matrix_       = cv::Mat::zeros(3, 3, 0);
		rhs.translation_vector_    = cv::Mat::zeros(3, 1, 0);
		rhs.transformation_matrix_ = cv::Mat::zeros(4, 4, 0);
		rhs.image_channel_         = CHANNEL_DEFAULT;
	}

	return *this;
}

CameraSensorNew::~CameraSensorNew() = default;

bool CameraSensorNew::HandlesDepthValues() const
{
	return false;
}

bool CameraSensorNew::GetBestFitCVModel(cv::Mat& camera_matrix,
                                        cv::Mat& distortion_vector) const
{
	return false;
}

void CameraSensorNew::RayReconstructionWCS(cv::InputArray points_2d,
                                           cv::OutputArray out,
                                           const bool normalize,
                                           cv::InputArray points3d) const
{
	std::vector<cv::Point2f> undistorted_points;
	UndistortPoints(points_2d, undistorted_points, points3d);

	//Using opencv structures as described here:
	//http://stackoverflow.com/questions/25750041/pass-opencv-inputarray-and-use-it-as-stdvector
	out.create(points_2d.size(), out.type());
	cv::Mat out_mat = out.getMat();
	const cv::Mat out_double(out_mat.size(), out.type());
	const auto data = reinterpret_cast<cv::Point3f*>(out_double.data);
	//const auto origin(FromCameraToWorldCoordinateSystem(cv::Point3f(0., 0., 0.)));
	size_t index = 0;

	for(auto row = 0; row < undistorted_points.size(); ++row)
	{
		const auto point_x = undistorted_points[row].x;
		const auto point_y = undistorted_points[row].y;

		//point in camera CS
		cv::Point3f point3(point_x, point_y, 1.0);

		//point in world CS
		//point3 = FromCameraToWorldCoordinateSystem(point3);

		//direction from camera origin to point at 1m (in camera CS), this is the ray direction in world coordinates
		//point3 = point3 - origin;

		if(normalize)
		{
			point3 = point3 * (1. / cv::norm<float>(point3));
		}

		data[index] = point3;
		++index;
	}

	out_double.convertTo(out_mat, out_mat.type());
}

void CameraSensorNew::RayReconstructionSCS(cv::InputArray points_2d,
                                           cv::OutputArray out,
                                           const bool normalize,
                                           cv::InputArray points3d) const
{
	std::vector<cv::Point2f> undistorted;
	UndistortPoints(points_2d, undistorted, points3d);

	//Using opencv structures as described here:
	//http://stackoverflow.com/questions/25750041/pass-opencv-inputarray-and-use-it-as-stdvector
	out.create(points_2d.size(), out.type());

	cv::Mat out_mat = out.getMat();
	const cv::Mat out_double(out_mat.size(), out.type());
	const auto data = reinterpret_cast<cv::Point3f*>(out_double.data);
	const auto origin(FromSensorToWorldCoordinateSystem(cv::Point3f(0., 0., 0.)));
	size_t index = 0;

	for(const auto& point : undistorted)
	{
		//point in camera CS
		cv::Point3f point3(point.x, point.y, 1.0);

		//point in world CS
		point3 = FromSensorToWorldCoordinateSystem(point3);

		//direction from camera origin to point at 1m (in camera CS), this is the ray direction in world coordinates
		point3 = point3 - origin;

		if(normalize)
		{
			point3 = point3 * (1. / cv::norm<float>(point3));
		}

		data[index] = point3;
		++index;
	}

	out_double.convertTo(out_mat, out_mat.type());
}

void CameraSensorNew::GetPlaneIntersectionPoints(cv::InputArray points_2d,
                                                 const cv::Vec3d& plane_position,
                                                 const cv::Vec3d& plane_normal,
                                                 cv::OutputArray intersection_points_coordinate_system) const
{
	// Plane position and plane normal are in camera coordinate system
	intersection_points_coordinate_system.create(points_2d.size(), intersection_points_coordinate_system.type(), -1, true);
	const auto out_matrix = intersection_points_coordinate_system.getMat();
	const auto data       = reinterpret_cast<cv::Point3f*>(out_matrix.data);

	//Camera position in Freestyle coordinates as ray origin
	const cv::Point3f origin(0., 0., 0.);
	const cv::Vec3d ray_location(origin.x, origin.y, origin.z);
	cv::Vec3d plane_normalized;
	cv::normalize(plane_normal, plane_normalized);
	const auto d_hesse = plane_normalized.dot(plane_position);

	//Calculate intersection Points of IR0 beams with plane detected by color camera
	std::vector<cv::Point3f> ray_directions;
	RayReconstructionWCS(points_2d, ray_directions, true);

	std::vector<cv::Point3f> points_3d;

	for(size_t idx = 0; idx < ray_directions.size(); ++idx)
	{
		cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
		const auto line_direction_scale = (d_hesse - plane_normalized.dot(ray_location)) / plane_normalized.dot(ray_direction);
		auto point                      = line_direction_scale * ray_direction + ray_location;
		auto temp_point                 = cv::Point3f(point[0], point[1], point[2]);
		data[idx]                       = FromSensorToWorldCoordinateSystem(temp_point);
		points_3d.push_back(data[idx]);
	}

	//second run if sensor supports depth dependent un-distortion
	if(HandlesDepthValues())
	{
		//ray_directions.clear();
		//RayReconstruction(points_2d, ray_directions, true, points_3d);
		//for(size_t idx = 0; idx < ray_directions.size(); ++idx)
		//{
		//	cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
		//	const auto line_direction_scale = (d_hesse - plane_normal.dot(ray_location)) / plane_normal.dot(ray_direction);
		//	auto point                      = line_direction_scale * ray_direction + ray_location;
		//	data[idx]                       = cv::Point3f(point[0], point[1], point[2]);
		//}
	}
}
}
}
