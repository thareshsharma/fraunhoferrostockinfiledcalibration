#include "camera_sensor_new.h"
#include "camera_sensor_depth_dependent_new.h"
#include "helper.h"

using namespace DMVS::CameraCalibration;

CameraSensorDepthDependentNew::CameraSensorDepthDependentNew() :
	CameraSensorNew()
{
}

CameraSensorDepthDependentNew::CameraSensorDepthDependentNew(const CameraInfo& camera_info) :
	CameraSensorNew(camera_info),
	camera_info_(camera_info)
{
}

CameraSensorDepthDependentNew::CameraSensorDepthDependentNew(const std::shared_ptr<const CameraSensorDepthDependentNew> rhs) :
	CameraSensorNew()
{
}

bool CameraSensorDepthDependentNew::HandlesDepthValues() const
{
	return true;
}

CameraSensorDepthDependentNew::CameraSensorDepthDependentNew(cv::InputArray& camera_matrix,
                                                             cv::InputArray& distortion,
                                                             cv::InputArray& rotation_matrix,
                                                             cv::InputArray& translation_vector) :
	CameraSensorNew()
{
	// set camera matrix
	camera_info_.intrinsics.camera_intrinsic_matrix      = camera_matrix.getMat();
	camera_info_.intrinsics.camera_intrinsic_matrix      = distortion.getMat();
	camera_info_.extrinsics.rotation_matrix              = rotation_matrix.getMat();
	camera_info_.extrinsics.translation_vector           = translation_vector.getMat();
	camera_info_.intrinsics.depth_parameters.exponential = cv::Vec3d(0.25, 3.0, 5.0);
	camera_info_.intrinsics.depth_parameters.scalar      = cv::Vec2d(0.4, 0.0065);

	//CameraInfoToParamVector(camera_info_, parameter_representation_);
	//CameraSensorDepthDependentNew::InitFromParamsVector(parameter_representation_);

	// all parameters are modifiable at calibration if not set fix later
	//parameter_fixations_.resize(20);

	//for(size_t i                = 0; i < parameter_fixations_.size(); ++i)
	//	parameter_fixations_[i] = i;

	//// fix exponential parameters of depth dependency
	//parameter_fixations_[15] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	//parameter_fixations_[16] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	//parameter_fixations_[17] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	//parameter_fixations_[18] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	//parameter_fixations_[19] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
}

CameraInfo CameraSensorDepthDependentNew::GetCameraInfo() const
{
	//CameraInfo camera_info;
	////ParamVectorToCameraInfo(parameter_representation_, camera_info);
	return camera_info_;
}

CameraSensorDepthDependentNew::~CameraSensorDepthDependentNew() = default;

std::shared_ptr<Sensor> CameraSensorDepthDependentNew::Duplicate() const
{
	auto out = std::make_shared<CameraSensorDepthDependentNew>(camera_info_);
	/*out->SetImageChannel(image_channel_);
	out->parameter_fixations_ = parameter_fixations_;*/

	return out;
}

void CameraSensorDepthDependentNew::ProjectPoints(cv::InputArray& points_3d,
                                                  cv::OutputArray points_2d,
                                                  const bool apply_camera_extrinsics,
                                                  const bool ignore_depth) const
{
	if(points_3d.total() == 0)
	{
		points_2d.clear();
		return;
	}

	cv::Mat rotation_matrix;
	cv::Mat translation_vector;

	if(apply_camera_extrinsics)
	{
		rotation_matrix    = camera_info_.extrinsics.rotation_matrix.clone();
		translation_vector = camera_info_.extrinsics.translation_vector.clone();
	}

	ProjectPointsInternal(points_3d, rotation_matrix, translation_vector, points_2d, ignore_depth);
}

void CameraSensorDepthDependentNew::UndistortPoints(cv::InputArray points_2d,
                                                    cv::OutputArray undistorted_points_2d,
                                                    cv::InputArray points_3d) const
{
	const auto source = points_2d.getMat();

	if(!(points_2d.total() > 0))
		return;

	CV_Assert(source.isContinuous() && (source.depth() == CV_32F || source.depth() == CV_64F) &&
		((source.rows == 1 && source.channels() == 2) || source.cols * source.channels() == 2));

	//auto depth        = source.depth();
	const auto points_count = source.checkVector(2);

	if(points_count == 0)
	{
		undistorted_points_2d.clear();
		return;
	}

	if(undistorted_points_2d.fixedType())
	{
		undistorted_points_2d.create(source.size(), undistorted_points_2d.type(), -1, true);
	}
	else
	{
		undistorted_points_2d.create(source.size(), source.type(), -1, true);
	}

	UndistortPointsDistanceDependent(source,
	                                 points_3d.getMat(),
	                                 undistorted_points_2d.getMat(),
	                                 camera_info_.intrinsics.camera_intrinsic_matrix,
	                                 camera_info_.intrinsics.distortion_vector);
}

bool CameraSensorDepthDependentNew::GetBestFitCVModel(cv::Mat& camera_matrix,
                                                      cv::Mat& distortion_vector) const
{
	camera_info_.intrinsics.camera_intrinsic_matrix.copyTo(camera_matrix);
	camera_info_.intrinsics.distortion_vector.copyTo(distortion_vector);

	return true;
}

void CameraSensorDepthDependentNew::KeepExtrinsicParametersFix(const bool fix_extrinsic_parameters)
{
	//for(size_t i = 9; i < Constants::PARAMS_COUNT; ++i)
	//{
	//	parameter_fixations_[i] = fix_extrinsic_parameters ? Constants::DEFAULT_SIZE_T_MAX_VALUE : i;
	//}
}

void CameraSensorDepthDependentNew::KeepSameFocalLengthFix(const bool fix_same_focal_length)
{
	//parameter_fixations_[1] = fix_same_focal_length ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 1;
}

void CameraSensorDepthDependentNew::KeepFocalLengthFix(const bool fix_focal_length)
{
	//parameter_fixations_[0] = fix_focal_length ? Constants::DEFAULT_SIZE_T_MAX_VALUE : parameter_fixations_[0];
	//parameter_fixations_[1] = fix_focal_length ? Constants::DEFAULT_SIZE_T_MAX_VALUE : parameter_fixations_[1];
}

void CameraSensorDepthDependentNew::KeepDistortionFix(const bool fix_distortion)
{
	//for(size_t i = 4; i < 9; ++i)
	//{
	//	parameter_fixations_[i] = fix_distortion ? Constants::DEFAULT_SIZE_T_MAX_VALUE : i;
	//}

	//for(size_t i = 16; i < 21; ++i)
	//{
	//	parameter_fixations_[i] = fix_distortion ? Constants::DEFAULT_SIZE_T_MAX_VALUE : i;
	//}
}

void CameraSensorDepthDependentNew::SetCameraInfo(const CameraInfo& camera_info)
{
	camera_info_ = camera_info;
}

void CameraSensorDepthDependentNew::UndistortPointsDistanceDependent(const cv::Mat& source,
                                                                     const cv::Mat& pts3d,
                                                                     const cv::Mat& destination,
                                                                     const cv::Mat& camera_matrix,
                                                                     const cv::Mat& distortion_coefficients) const
{
	int iteration = 1;

	if((source.type() == CV_32FC2 || source.type() == CV_64FC2) &&
		(destination.type() == CV_32FC2 || destination.type() == CV_64FC2) &&
		camera_matrix.rows == 3 && camera_matrix.cols == 3)
	{
		if(!distortion_coefficients.empty() && (distortion_coefficients.rows == 1 || distortion_coefficients.cols == 1) &&
			(distortion_coefficients.rows * distortion_coefficients.cols == 4 ||
				distortion_coefficients.rows * distortion_coefficients.cols == 5 ||
				distortion_coefficients.rows * distortion_coefficients.cols == 8))
		{
			iteration = 5;
		}

		// Pointers to access source and destination data with correct depth (float or double)
		// source: 2d image coordinates
		const CvPoint2D32f* source_data_float  = (const CvPoint2D32f*)source.data;
		const CvPoint2D64f* source_data_double = (const CvPoint2D64f*)source.data;

		// source: 3d point coordinates in camera coordinates
		const CvPoint3D32f* points_3d_data_float  = (const CvPoint3D32f*)pts3d.data;
		const CvPoint3D64f* points_3d_data_double = (const CvPoint3D64f*)pts3d.data;

		// destination: undistorted coordinates
		CvPoint2D32f* destination_data_float  = (CvPoint2D32f*)destination.data;
		CvPoint2D64f* destination_data_double = (CvPoint2D64f*)destination.data;

		const auto source_type      = source.type();
		const auto pts3d_type       = pts3d.type();
		const auto destination_type = destination.type();
		const auto source_step      = source.rows == 1 ? 1 : source.step / CV_ELEM_SIZE(source_type);
		const auto pts_3d_step      = pts3d.rows == 1 ? 1 : pts3d.step / CV_ELEM_SIZE(pts3d_type);
		const auto destination_step = destination.rows == 1 ? 1 : destination.step / CV_ELEM_SIZE(destination_type);

		const auto source_total_size = source.rows + source.cols - 1;
		const auto fx                = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(0, 0);
		const auto fy                = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(1, 1);
		/*auto ifx = 1. / fx;
		auto ify = 1. / fy;*/
		const auto cx = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(0, 2);
		const auto cy = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(1, 2);

		// parameters for depth compensation
		// distance for which radial distortion is calculated
		const auto s = camera_info_.intrinsics.depth_parameters.scalar[0];

		// distance focused image plane to lens
		const auto c = camera_info_.intrinsics.depth_parameters.scalar[1];

		// get the distances from 3d input points
		std::vector<double> distances(source_total_size, s);

		if((pts3d_type == CV_32FC3 || pts3d_type == CV_64FC3) && (pts3d.rows == source_total_size || pts3d.cols == source_total_size))
		{
			if(pts3d_type == CV_32FC3)
			{
				for(auto i = 0; i < source_total_size; ++i)
				{
					// transform device coordinate to camera local Z
					auto z_local = camera_info_.extrinsics.rotation_matrix.at<double>(2, 0) * points_3d_data_float[i * pts_3d_step].x;
					z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 1) * points_3d_data_float[i * pts_3d_step].y;
					z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 2) * points_3d_data_float[i * pts_3d_step].z;
					z_local += camera_info_.extrinsics.translation_vector.at<double>(2, 0);
					distances[i] = z_local;
				}
			}
			else
			{
				for(auto i = 0; i < source_total_size; ++i)
				{
					// transform device coordinate to camera local Z
					auto z_local = camera_info_.extrinsics.rotation_matrix.at<double>(2, 0) * points_3d_data_double[i * pts_3d_step].x;
					z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 1) * points_3d_data_double[i * pts_3d_step].y;
					z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 2) * points_3d_data_double[i * pts_3d_step].z;
					z_local += camera_info_.extrinsics.translation_vector.at<double>(2, 0);
					distances[i] = z_local;
				}
			}
		}

		//tbb::parallel_for(0,
		//                  source_total_size,
		//                  [&](const size_t i)
		for(int i = 0; i < source_total_size; i++)
		{
			double x, y, z;

			if(source_type == CV_32FC2)
			{
				x = source_data_float[i * source_step].x;
				y = source_data_float[i * source_step].y;
				z = distances[i];
			}
			else
			{
				x = source_data_double[i * source_step].x;
				y = source_data_double[i * source_step].y;
				z = distances[i];
			}

			// distance dependent radial compensation
			//	Note: Seems that log/exp is faster than sqrt/pow. Maybe we should rework this function
			//		using cv::Mat::forEach and/or SSE functions. Even a special version for the used
			//		exponents (1/4, 3, 5) is possible.
			//
			//double gamma_ss = (z*(s - c)) / (s*(z - c));
			//double focalScale = fabs(b.dd_exp[0] - 0.25) < DBL_EPSILON ? sqrt(sqrt(gamma_ss)) : pow(gamma_ss, b.dd_exp[0]);
			//int e1 = (int)b.dd_exp[1];
			//int e2 = (int)b.dd_exp[2];
			//double e1Fac = fabs(e1 - b.dd_exp[1]) < DBL_EPSILON ? pow(gamma_ss, e1) : pow(gamma_ss, b.dd_exp[1]);
			//double e2Fac = fabs(e2 - b.dd_exp[2]) < DBL_EPSILON ? pow(gamma_ss, e2) : pow(gamma_ss, b.dd_exp[2]);

			const auto gamma_ss_log = log((z * (s - c)) / (s * (z - c)));
			const auto focal_scale  = exp(camera_info_.intrinsics.depth_parameters.exponential[0] * gamma_ss_log);

			const auto if_x_corrected = 1.0 / (fx * focal_scale);
			const auto if_y_corrected = 1.0 / (fy * focal_scale);

			const auto e1_factor = exp(gamma_ss_log * camera_info_.intrinsics.depth_parameters.exponential[1]);
			const auto e2_factor = exp(gamma_ss_log * camera_info_.intrinsics.depth_parameters.exponential[2]);

			const auto k_0 = camera_info_.intrinsics.distortion_vector.at<double>(0) * e1_factor;
			const auto k_1 = camera_info_.intrinsics.distortion_vector.at<double>(1) / e2_factor;
			const auto k_2 = camera_info_.intrinsics.distortion_vector.at<double>(2);
			const auto k_3 = camera_info_.intrinsics.distortion_vector.at<double>(3);
			const auto k_4 = camera_info_.intrinsics.distortion_vector.at<double>(4);

			const auto x0 = x = (x - cx) * if_x_corrected;
			const auto y0 = y = (y - cy) * if_y_corrected;

			// compensate distortion iteratively
			for(auto j = 0; j < iteration; ++j)
			{
				const auto r2      = x * x + y * y;
				const auto ic_dist = 1.0 / (1 + ((k_4 * r2 + k_1) * r2 + k_0) * r2);
				const auto delta_x = 2 * k_2 * x * y + k_3 * (r2 + 2 * x * x);
				const auto delta_y = k_2 * (r2 + 2 * y * y) + 2 * k_3 * x * y;
				x                  = (x0 - delta_x) * ic_dist;
				y                  = (y0 - delta_y) * ic_dist;
			}

			if(destination_type == CV_32FC2)
			{
				destination_data_float[i * destination_step].x = float(x);
				destination_data_float[i * destination_step].y = float(y);
			}
			else
			{
				destination_data_double[i * destination_step].x = x;
				destination_data_double[i * destination_step].y = y;
			}
		}
	}
}

void CameraSensorDepthDependentNew::ProjectPointsInternal(cv::InputArray points3d,
                                                          const cv::Mat rotation_matrix,
                                                          const cv::Mat translation_vector,
                                                          cv::OutputArray points2d,
                                                          bool ignore_depth) const
{
	auto src     = points3d.getMat();
	auto depth   = src.depth();
	auto num_pts = src.checkVector(3);

	if(num_pts == 0)
	{
		points2d.clear();
		return;
	}

	points2d.create(num_pts, 1, CV_MAKETYPE(depth, 2), -1, true);
	CvMat destination, source;

	Helper::ToCVMat(points2d.getMat(), destination);
	Helper::ToCVMat(src, source);

	// get pointer based access to src and dst of different data types
	const CvPoint3D32f* source_float_ptr  = (const CvPoint3D32f*)source.data.ptr;
	const CvPoint3D64f* source_double_ptr = (const CvPoint3D64f*)source.data.ptr;
	CvPoint2D32f* destination_float_ptr   = (CvPoint2D32f*)destination.data.ptr;
	CvPoint2D64f* destination_double_ptr  = (CvPoint2D64f*)destination.data.ptr;

	auto source_type      = CV_MAT_TYPE(source.type);
	auto destination_type = CV_MAT_TYPE(destination.type);
	auto source_step      = source.rows == 1 ? 1 : source.step / CV_ELEM_SIZE(source_type);
	auto tran_vec         = translation_vector.empty()
		                        ? Eigen::Vector3d(0, 0, 0)
		                        : Eigen::Vector3d(translation_vector.at<double>(0, 0),
		                                          translation_vector.at<double>(1, 0),
		                                          translation_vector.at<double>(2, 0));

	// get the rotation matrix
	cv::Mat r_mat;
	if(!rotation_matrix.empty())
	{
		if(rotation_matrix.total() == 9)
		{
			r_mat = rotation_matrix;
		}
		else
		{
			cv::Rodrigues(rotation_matrix, r_mat);
		}
	}

	if(!(GetImageChannel() == CHANNEL_GREY0 || GetImageChannel() == CHANNEL_GREY1))
	{
		//
	}

	Eigen::Matrix3d rotation;
	//Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> rotation(r_mat.ptr<double>(), r_mat.rows, r_mat.cols);
	if(!r_mat.empty())
	{
		for(auto row = 0; row < 3; ++row)
		{
			for(auto col = 0; col < 3; ++col)
			{
				rotation(row, col) = r_mat.at<double>(row, col);
			}
		}
	}

	double k[5];
	for(auto i = 0; i < 5; ++i)
	{
		k[i] = 0.0; // camera_info_.intrinsics.distortion_vector.at<double>(i, 0);
	}

	auto fx = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(0, 0);
	auto fy = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(1, 1);
	auto cx = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(0, 2);
	auto cy = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(1, 2);

	for(auto i = 0; i < num_pts; ++i)
	{
		Eigen::Vector3d p_src;
		if(source_type == CV_32FC3)
		{
			p_src[0] = source_float_ptr[i * source_step].x;
			p_src[1] = source_float_ptr[i * source_step].y;
			p_src[2] = source_float_ptr[i * source_step].z;
		}
		else
		{
			p_src[0] = source_double_ptr[i * source_step].x;
			p_src[1] = source_double_ptr[i * source_step].y;
			p_src[2] = source_double_ptr[i * source_step].z;
		}

		auto p_cam = r_mat.empty() ? p_src : rotation * p_src + tran_vec;
		auto x     = p_cam[0] / p_cam[2];
		auto y     = p_cam[1] / p_cam[2];
		auto z     = p_cam[2];

		double r2, r4, r6, a1, a2, a3, c_distance;
		double xd, yd;
		double k_corrected[5];
		memcpy(k_corrected, k, 5 * sizeof(double));

		auto fx_corrected = fx;
		auto fy_corrected = fy;

		if(!ignore_depth)
		{
			// at z=s the term evaluates to 1,  i.e. no further correction
			auto gamma_ss = z * (camera_info_.intrinsics.depth_parameters.scalar[0] - camera_info_.intrinsics.depth_parameters.scalar[1]) / (
				camera_info_.intrinsics.depth_parameters.scalar[0] * (z - camera_info_.intrinsics.depth_parameters.scalar[1]));

			auto focal_scale = camera_info_.intrinsics.depth_parameters.exponential[0] == 0.25
				                   ? sqrt(sqrt(gamma_ss))
				                   : pow(gamma_ss, camera_info_.intrinsics.depth_parameters.exponential[0]);

			fx_corrected = fx * focal_scale;
			fy_corrected = fy * focal_scale;

			// calculate with integer exponents if possible
			auto e1 = int(camera_info_.intrinsics.depth_parameters.exponential[1]);
			auto e2 = int(camera_info_.intrinsics.depth_parameters.exponential[2]);

			auto e1_fac = e1 == camera_info_.intrinsics.depth_parameters.exponential[1]
				              ? pow(gamma_ss, e1)
				              : pow(gamma_ss, camera_info_.intrinsics.depth_parameters.exponential[1]);

			auto e2_fac = e2 == camera_info_.intrinsics.depth_parameters.exponential[2]
				              ? pow(gamma_ss, e2)
				              : pow(gamma_ss, camera_info_.intrinsics.depth_parameters.exponential[2]);

			k_corrected[0] = k[0] * e1_fac;
			k_corrected[1] = k[1] / e2_fac;
		}

		r2 = x * x + y * y;
		r4 = r2 * r2;
		r6 = r4 * r2;

		a1         = 2 * x * y;
		a2         = r2 + 2 * x * x;
		a3         = r2 + 2 * y * y;
		c_distance = 1 + k_corrected[0] * r2 + k_corrected[1] * r4 + k_corrected[4] * r6;

		xd = x * c_distance + k_corrected[2] * a1 + k_corrected[3] * a2;
		yd = y * c_distance + k_corrected[2] * a3 + k_corrected[3] * a1;

		if(destination_type == CV_32FC2)
		{
			destination_float_ptr[i * source_step].x = float(xd * fx_corrected + cx);
			destination_float_ptr[i * source_step].y = float(yd * fy_corrected + cy);
		}
		else
		{
			destination_double_ptr[i * source_step].x = xd * fx_corrected + cx;
			destination_double_ptr[i * source_step].y = yd * fy_corrected + cy;
		}
	}
}
