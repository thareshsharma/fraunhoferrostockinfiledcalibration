#include "camera_sensor_depth_dependent.h"
#include "constants.h"
#include "helper.h"

using namespace DMVS::CameraCalibration;

CameraSensorDepthDependent::CameraSensorDepthDependent() :
	CameraSensor()
{
	// All parameters are modifiable at calibration if not set fix later
	for(size_t i = 0; i < Constants::PARAMS_SIZE; ++i)
	{
		parameter_fixations_.push_back(i);
	}
}

CameraSensorDepthDependent::CameraSensorDepthDependent(const CameraInfo& camera_info) :
	CameraSensor(camera_info),
	camera_info_(camera_info)
{
	// Update parameters
	CameraSensorDepthDependent::SetIntrinsicParameters(camera_info_.intrinsics.camera_intrinsic_matrix, camera_info_.intrinsics.distortion_vector);
	SetExtrinsicParameters(camera_info_.extrinsics.rotation_matrix, camera_info_.extrinsics.translation_vector);
	CameraInfoToParamVector(camera_info_, parameters_);
	// All parameters are modifiable at calibration if not set fix later
	for(size_t i = 0; i < Constants::PARAMS_SIZE; ++i)
	{
		parameter_fixations_.push_back(i);
	}
}

CameraSensorDepthDependent::CameraSensorDepthDependent(const LaserInfo& laser_info) :
	CameraSensor(laser_info),
	laser_info_(laser_info)
{
	// Update Parameters
	CameraSensorDepthDependent::SetIntrinsicParameters(laser_info_.intrinsics.projector_intrinsic_matrix, laser_info_.intrinsics.distortion_vector);
	SetExtrinsicParameters(laser_info_.extrinsics.rotation_matrix, laser_info_.extrinsics.translation_vector);
	LaserInfoToParamVector(laser_info_, parameters_);
	// All parameters are modifiable at calibration if not set fix later
	for(size_t i = 0; i < Constants::PARAMS_SIZE; ++i)
	{
		parameter_fixations_.push_back(i);
	}
}

CameraSensorDepthDependent::CameraSensorDepthDependent(const std::shared_ptr<const CameraSensorDepthDependent> rhs) :
	CameraSensor()
{
	parameter_fixations_ = rhs->parameter_fixations_;
	CameraSensorDepthDependent::InitFromParamsVector(rhs->GetParamsVector());
	CameraSensorDepthDependent::SetImageChannel(rhs->GetImageChannel());
}

std::vector<double> CameraSensorDepthDependent::GetParamsVector() const
{
	return parameters_;
}

std::vector<size_t> CameraSensorDepthDependent::GetFixations() const
{
	return parameter_fixations_;
}

bool CameraSensorDepthDependent::HandlesDepthValues() const
{
	return true;
}

CameraSensorDepthDependent::CameraSensorDepthDependent(const std::string& name,
                                                       const ImageChannel channel,
                                                       const cv::Vec2d& depth_params,
                                                       const cv::Vec3d& depth_params_exp,
                                                       cv::InputArray& camera_matrix,
                                                       cv::InputArray& distortion,
                                                       cv::InputArray& rotation_matrix,
                                                       cv::InputArray& translation_vector) :
	CameraSensor()
{
	// set camera matrix
	camera_info_.name                                    = name;
	camera_info_.channel                                 = channel;
	camera_info_.intrinsics.camera_intrinsic_matrix      = camera_matrix.getMat();
	camera_info_.intrinsics.distortion_vector            = distortion.getMat();
	camera_info_.extrinsics.rotation_matrix              = rotation_matrix.getMat();
	camera_info_.extrinsics.translation_vector           = translation_vector.getMat();
	camera_info_.intrinsics.depth_parameters.exponential = depth_params_exp;
	camera_info_.intrinsics.depth_parameters.scalar      = depth_params;

	CameraSensorDepthDependent::SetIntrinsicParameters(camera_info_.intrinsics.camera_intrinsic_matrix, camera_info_.intrinsics.distortion_vector);
	SetExtrinsicParameters(camera_info_.extrinsics.rotation_matrix, camera_info_.extrinsics.translation_vector);
	CameraInfoToParamVector(camera_info_, parameters_);

	// All parameters are modifiable at calibration if not set fix later
	parameter_fixations_.clear();

	for(size_t i = 0; i < Constants::PARAMS_SIZE; ++i)
	{
		parameter_fixations_.push_back(i);
	}

	// fix exponential parameters of depth dependency
	parameter_fixations_[15] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	parameter_fixations_[16] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	parameter_fixations_[17] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	parameter_fixations_[18] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	parameter_fixations_[19] = 19;
}

bool CameraSensorDepthDependent::ParamVectorToCameraInfo(const std::vector<double>& params,
                                                         CameraInfo& camera_info)
{
	if(params.size() != Constants::PARAMS_SIZE)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Check parameters vector. Expected parameter count [" <<
			Constants::PARAMS_SIZE << "] Current parameter count [" << params.size() << "]";

		return false;
	}

	// Focal Point and cx, cy
	camera_info.intrinsics.camera_intrinsic_matrix = (cv::Mat_<double>(3, 3) << params[0], 0, params[2], 0, params[1], params[3], 0, 0, 1);

	// distortion vector
	camera_info.intrinsics.distortion_vector = (cv::Mat_<double>(5, 1) << params[4], params[5], params[6], params[7], params[8]);

	// rotation matrix
	cv::Mat rotation_matrix_temp;
	const cv::Mat rotation_vector = (cv::Mat_<double>(3, 1) << params[9], params[10], params[11]);
	cv::Rodrigues(rotation_vector, rotation_matrix_temp);
	camera_info.extrinsics.rotation_matrix = rotation_matrix_temp;

	//translation vector
	camera_info.extrinsics.translation_vector = (cv::Mat_<double>(3, 1) << params[12], params[13], params[14]);

	//depth dependent exp
	camera_info.intrinsics.depth_parameters.exponential = cv::Vec3d(params[15], params[16], params[17]);

	//depth dependent scalar
	camera_info.intrinsics.depth_parameters.scalar = cv::Vec2d(params[18], params[19]);

	return true;
}

bool CameraSensorDepthDependent::CameraInfoToParamVector(const CameraInfo& camera_info,
                                                         std::vector<double>& params)
{
	params.resize(Constants::PARAMS_SIZE);
	if(!camera_info.intrinsics.camera_intrinsic_matrix.empty())
	{
		// Focal Point
		params[0] = camera_info.intrinsics.camera_intrinsic_matrix.at<double>(0, 0);
		params[1] = camera_info.intrinsics.camera_intrinsic_matrix.at<double>(1, 1);

		//cx and cy
		params[2] = camera_info.intrinsics.camera_intrinsic_matrix.at<double>(0, 2);
		params[3] = camera_info.intrinsics.camera_intrinsic_matrix.at<double>(1, 2);
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Check intrinsic parameters (camera matrix) for Channel " << camera_info.channel;
		return false;
	}

	if(!camera_info.intrinsics.distortion_vector.empty())
	{
		// distortion vector
		params[4] = camera_info.intrinsics.distortion_vector.at<double>(0, 0);
		params[5] = camera_info.intrinsics.distortion_vector.at<double>(1, 0);
		params[6] = camera_info.intrinsics.distortion_vector.at<double>(2, 0);
		params[7] = camera_info.intrinsics.distortion_vector.at<double>(3, 0);
		params[8] = camera_info.intrinsics.distortion_vector.at<double>(4, 0);
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Check intrinsic parameters (distortion vector) for Channel " << camera_info.channel;
		return false;
	}

	if(!camera_info.extrinsics.rotation_matrix.empty())
	{
		// rotation matrix
		cv::Mat rotation_vector;
		cv::Rodrigues(camera_info.extrinsics.rotation_matrix, rotation_vector);
		params[9]  = rotation_vector.at<double>(0, 0);
		params[10] = rotation_vector.at<double>(1, 0);
		params[11] = rotation_vector.at<double>(2, 0);
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Check extrinsic parameters (rotation matrix) for Channel " << camera_info.channel;
		return false;
	}

	if(!camera_info.extrinsics.translation_vector.empty())
	{
		//tranlation vector
		params[12] = camera_info.extrinsics.translation_vector.at<double>(0, 0);
		params[13] = camera_info.extrinsics.translation_vector.at<double>(1, 0);
		params[14] = camera_info.extrinsics.translation_vector.at<double>(2, 0);
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Check extrinsic parameters (translation vector) for Channel " << camera_info.channel;
		return false;
	}

	//depth dependent exp
	params[15] = camera_info.intrinsics.depth_parameters.exponential(0);
	params[16] = camera_info.intrinsics.depth_parameters.exponential(1);
	params[17] = camera_info.intrinsics.depth_parameters.exponential(2);

	//depth dependent scalar
	params[18] = camera_info.intrinsics.depth_parameters.scalar(0);
	params[19] = camera_info.intrinsics.depth_parameters.scalar(1);

	return true;
}

bool CameraSensorDepthDependent::LaserInfoToParamVector(const LaserInfo& laser_info,
                                                        std::vector<double>& params)
{
	//params.resize(Constants::PARAMS_SIZE);
	// Focal Point
	params.push_back(laser_info.intrinsics.projector_intrinsic_matrix.at<double>(0, 0));
	params.push_back(laser_info.intrinsics.projector_intrinsic_matrix.at<double>(1, 1));
	//cx and cy
	params.push_back(laser_info.intrinsics.projector_intrinsic_matrix.at<double>(0, 2));
	params.push_back(laser_info.intrinsics.projector_intrinsic_matrix.at<double>(1, 2));

	// distortion vector
	params.push_back(laser_info.intrinsics.distortion_vector.at<double>(0, 0));
	params.push_back(laser_info.intrinsics.distortion_vector.at<double>(1, 0));
	params.push_back(laser_info.intrinsics.distortion_vector.at<double>(2, 0));
	params.push_back(laser_info.intrinsics.distortion_vector.at<double>(3, 0));
	params.push_back(laser_info.intrinsics.distortion_vector.at<double>(4, 0));

	// rotation matrix
	cv::Mat rotation_vector;
	cv::Rodrigues(laser_info.extrinsics.rotation_matrix, rotation_vector);
	params.push_back(rotation_vector.at<double>(0, 0));
	params.push_back(rotation_vector.at<double>(1, 0));
	params.push_back(rotation_vector.at<double>(2, 0));

	//tranlation vector
	params.push_back(laser_info.extrinsics.translation_vector.at<double>(0, 0));
	params.push_back(laser_info.extrinsics.translation_vector.at<double>(1, 0));
	params.push_back(laser_info.extrinsics.translation_vector.at<double>(2, 0));

	//depth dependent exp
	params.push_back(0);
	params.push_back(0);
	params.push_back(0);

	//depth dependent scalar
	params.push_back(0);
	params.push_back(0);

	return true;
}

size_t CameraSensorDepthDependent::InitFromParamsVector(const std::vector<double>& params)
{
	// convert pure double vector to internal structure with meaningful names
	parameters_ = params;


	if(!ParamVectorToCameraInfo(params, camera_info_))
		return Constants::DEFAULT_SIZE_T_VALUE;

	// Update parameters
	CameraSensorDepthDependent::SetIntrinsicParameters(camera_info_.intrinsics.camera_intrinsic_matrix, camera_info_.intrinsics.distortion_vector);
	SetExtrinsicParameters(camera_info_.extrinsics.rotation_matrix, camera_info_.extrinsics.translation_vector);

	return 0;
}

CameraInfo CameraSensorDepthDependent::GetCameraInfo() const
{
	return camera_info_;
}

void CameraSensorDepthDependent::GetCameraIntrinsicParams(cv::Mat& camera_matrix,
                                                          cv::Mat& distortion_vector) const
{
	camera_matrix = cv::Mat(3, 3, CV_64F);
	camera_matrix = camera_info_.intrinsics.camera_intrinsic_matrix.clone();

	distortion_vector = cv::Mat(5, 1, CV_64F);
	distortion_vector = camera_info_.intrinsics.distortion_vector.clone();
}

void CameraSensorDepthDependent::SetIntrinsicParameters(const cv::Mat& intrinsic_matrix,
                                                        const cv::Mat& distortion_vector)
{
	camera_intrinsic_matrix_ = intrinsic_matrix.clone();
	distortion_vector_       = distortion_vector.clone();
}

CameraSensorDepthDependent::~CameraSensorDepthDependent() = default;

std::shared_ptr<CameraSensor> CameraSensorDepthDependent::Duplicate() const
{
	auto out = std::make_shared<CameraSensorDepthDependent>(camera_info_);
	out->SetImageChannel(camera_info_.channel);
	out->parameter_fixations_ = parameter_fixations_;

	return out;
}

std::shared_ptr<CameraSensor> CameraSensorDepthDependent::DuplicateLaser() const
{
	//throw std::exception("DuplicateLaser should not be used");
	auto out = std::make_shared<CameraSensorDepthDependent>(laser_info_);
	out->SetExtrinsicParameters(laser_info_.extrinsics.rotation_matrix, laser_info_.extrinsics.translation_vector);
	out->SetIntrinsicParameters(laser_info_.intrinsics.projector_intrinsic_matrix, laser_info_.intrinsics.distortion_vector);
	out->SetImageChannel(CHANNEL_PROJECTOR0);
	out->parameter_fixations_ = parameter_fixations_;

	return out;
}

ImageChannel CameraSensorDepthDependent::GetImageChannel() const
{
	return image_channel_;
}

void CameraSensorDepthDependent::SetImageChannel(const ImageChannel image_channel)
{
	image_channel_ = image_channel;
}

void CameraSensorDepthDependent::ProjectPoints(cv::InputArray& points_3d,
                                               cv::OutputArray points_2d,
                                               const bool apply_camera_extrinsics,
                                               const bool ignore_depth) const
{
	if(points_3d.total() == 0)
	{
		points_2d.clear();
		return;
	}
	cv::Mat rotation_matrix;
	cv::Mat translation_vector;

	if(apply_camera_extrinsics)
	{
		rotation_matrix    = camera_info_.extrinsics.rotation_matrix.clone();
		translation_vector = camera_info_.extrinsics.translation_vector.clone();
	}

	ProjectPointsInternal(points_3d, rotation_matrix, translation_vector, points_2d, ignore_depth);
}

void CameraSensorDepthDependent::LaserProjectPoints(cv::InputArray& points_3d,
                                                    cv::OutputArray points_2d,
                                                    const bool apply_camera_extrinsics,
                                                    const bool ignore_depth) const
{
	if(points_3d.total() == 0)
	{
		points_2d.clear();
		return;
	}
	cv::Mat rotation_matrix;
	cv::Mat translation_vector;

	if(apply_camera_extrinsics)
	{
		rotation_matrix    = rotation_matrix_.clone();
		translation_vector = translation_vector_.clone();
	}

	ProjectPointsInternalLaser(points_3d, rotation_matrix, translation_vector, points_2d);
}

void CameraSensorDepthDependent::UndistortPoints2(cv::InputArray points_2d,
                                                  cv::OutputArray undistorted_points_2d,
                                                  cv::InputArray points_3d) const
{
	const auto source = points_2d.getMat();

	if(!(points_2d.total() > 0))
		return;

	CV_Assert(source.isContinuous() && (source.depth() == CV_32F || source.depth() == CV_64F) &&
		((source.rows == 1 && source.channels() == 2) || source.cols * source.channels() == 2));

	//auto depth        = source.depth();
	const auto points_count = source.checkVector(2);

	if(points_count == 0)
	{
		undistorted_points_2d.clear();
		return;
	}

	if(undistorted_points_2d.fixedType())
	{
		undistorted_points_2d.create(source.size(), undistorted_points_2d.type(), -1, true);
	}
	else
	{
		undistorted_points_2d.create(source.size(), source.type(), -1, true);
	}

	UndistortPointsDistanceDependent2(source,
	                                  points_3d.getMat(),
	                                  undistorted_points_2d.getMat(),
	                                  camera_info_.intrinsics.camera_intrinsic_matrix,
	                                  camera_info_.intrinsics.distortion_vector);
}

void CameraSensorDepthDependent::UndistortPointsLaser(cv::InputArray points_2d,
                                                      cv::OutputArray undistorted_points_2d,
                                                      cv::InputArray points_3d) const
{
	const auto source = points_2d.getMat();

	if(points_2d.empty())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : No points to process.";
		return;
	}

	if(source.isContinuous() && (source.depth() == CV_32F || source.depth() == CV_64F) &&
		(source.rows == 1 && source.channels() == 2 || source.cols * source.channels() == 2))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Points are not continuous or not enough channels .";
	}

	//auto depth        = source.depth();
	const auto points_count = source.checkVector(2);

	if(points_count == 0)
	{
		undistorted_points_2d.clear();
		return;
	}

	if(undistorted_points_2d.fixedType())
	{
		undistorted_points_2d.create(source.size(), undistorted_points_2d.type(), -1, true);
	}
	else
	{
		undistorted_points_2d.create(source.size(), source.type(), -1, true);
	}

	UndistortPointsDistanceDependentLaser(source,
	                                      points_3d.getMat(),
	                                      undistorted_points_2d.getMat(),
	                                      laser_info_.intrinsics.projector_intrinsic_matrix,
	                                      laser_info_.intrinsics.distortion_vector);
}

bool CameraSensorDepthDependent::GetBestFitCVModel(cv::Mat& camera_matrix,
                                                   cv::Mat& distortion_vector) const
{
	camera_info_.intrinsics.camera_intrinsic_matrix.copyTo(camera_matrix);
	camera_info_.intrinsics.distortion_vector.copyTo(distortion_vector);

	return true;
}

bool CameraSensorDepthDependent::GetBestFitCVModelLaser(cv::Mat& intrinsic_matrix,
                                                        cv::Mat& distortion_vector) const
{
	laser_info_.intrinsics.projector_intrinsic_matrix.copyTo(intrinsic_matrix);
	laser_info_.intrinsics.distortion_vector.copyTo(distortion_vector);
	return true;
}

void CameraSensorDepthDependent::KeepExtrinsicParametersFix(const bool fix_extrinsic_parameters)
{
	for(size_t i = 9; i < 15; ++i)
	{
		parameter_fixations_[i] = fix_extrinsic_parameters ? Constants::DEFAULT_SIZE_T_MAX_VALUE : i;
	}
}

void CameraSensorDepthDependent::KeepSameFocalLengthFix(const bool fix_same_focal_length)
{
	parameter_fixations_[1] = fix_same_focal_length ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 1;
}

void CameraSensorDepthDependent::KeepFocalLengthFix(const bool fix_focal_length)
{
	parameter_fixations_[0] = fix_focal_length ? Constants::DEFAULT_SIZE_T_MAX_VALUE : parameter_fixations_[0];
	parameter_fixations_[1] = fix_focal_length ? Constants::DEFAULT_SIZE_T_MAX_VALUE : parameter_fixations_[1];
}

void CameraSensorDepthDependent::KeepDepthDependentScalarParam1Fix(const bool fix_dd_scalar_param1_fix)
{
	parameter_fixations_[18] = fix_dd_scalar_param1_fix ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 18;
}

void CameraSensorDepthDependent::KeepDepthDependentScalarParam2Fix(const bool fix_dd_scalar_param2_fix)
{
	parameter_fixations_[19] = fix_dd_scalar_param2_fix ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 19;
}

void CameraSensorDepthDependent::KeepDepthDependentExpParam1Fix(const bool fix_dd_exp_param1_fix)
{
	parameter_fixations_[15] = fix_dd_exp_param1_fix ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 15;
}

void CameraSensorDepthDependent::KeepDepthDependentExpParam2Fix(const bool fix_dd_exp_param2_fix)
{
	parameter_fixations_[16] = fix_dd_exp_param2_fix ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 16;
}

void CameraSensorDepthDependent::KeepDepthDependentExpParam3Fix(const bool fix_dd_exp_param3_fix)
{
	parameter_fixations_[17] = fix_dd_exp_param3_fix ? Constants::DEFAULT_SIZE_T_MAX_VALUE : 17;
}

void CameraSensorDepthDependent::KeepDistortionFix(const bool fix_distortion)
{
	for(size_t i = 4; i < 9; ++i)
	{
		parameter_fixations_[i] = fix_distortion ? Constants::DEFAULT_SIZE_T_MAX_VALUE : i;
	}
}

std::shared_ptr<const CameraSensor> CameraSensorDepthDependent::FromBinary(std::vector<char>::const_iterator& pos)
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;

	CameraInfo camera_info = *(CameraInfo*)&(*pos);
	std::advance(pos, sizeof(camera_info));

	std::vector<double> params;
	CameraInfoToParamVector(camera_info, params);
	auto camera = std::make_shared<CameraSensorDepthDependent>();
	camera->InitFromParamsVector(params);

	return camera;
}

void CameraSensorDepthDependent::ToBinary(std::vector<char>& buffer) const
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;

	//std::vector<char> out;
	//SingleCalibrationContainer::CalibHeader header;
	//header.magicNumber      = CALIB_IDENT;
	//header.majorVersion     = 2;
	//header.staticDataOffset = sizeof(SingleCalibrationContainer::CalibHeader);
	//buffer.insert(buffer.end(), (char*)&header, (char*)&header + sizeof(header));
	//CameraSensorDepthDependent2::CameraInfo b = getBasicRepresentation();
	//buffer.insert(buffer.end(), (char*)&b, (char*)&b + sizeof(b));
}

void CameraSensorDepthDependent::SetCameraInfo(const CameraInfo& camera_info)
{
	camera_info_ = camera_info;
}

void CameraSensorDepthDependent::UndistortPointsDistanceDependent(const CvMat* source,
                                                                  const CvMat* pts3d,
                                                                  const CvMat* destination,
                                                                  const CvMat* camera_matrix,
                                                                  const CvMat* distortion_coefficients) const
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : USING CvMat " << Constants::CHECK_ME;

	double distortion_vector[5] = {0, 0, 0, 0, 0};
	CvMat cv_mat_camera_matrix  = cvMat(3, 3, CV_64F, camera_info_.intrinsics.camera_intrinsic_matrix.data), dk;

	auto iteration = 1;

	CV_Assert(CV_IS_MAT(source) && CV_IS_MAT(destination) &&
		(source->rows == 1 || source->cols == 1) &&
		(destination->rows == 1 || destination->cols == 1) &&
		source->cols + source->rows - 1 == destination->rows + destination->cols - 1 &&
		(CV_MAT_TYPE(source->type) == CV_32FC2 || CV_MAT_TYPE(source->type) == CV_64FC2) &&
		(CV_MAT_TYPE(destination->type) == CV_32FC2 || CV_MAT_TYPE(destination->type) == CV_64FC2));

	CV_Assert(CV_IS_MAT(camera_matrix) &&
		camera_matrix->rows == 3 && camera_matrix->cols == 3);

	cvConvert(camera_matrix, &cv_mat_camera_matrix);

	if(distortion_coefficients)
	{
		CV_Assert(CV_IS_MAT(distortion_coefficients) &&
			(distortion_coefficients->rows == 1 || distortion_coefficients->cols == 1) &&
			(distortion_coefficients->rows * distortion_coefficients->cols == 4 ||
				distortion_coefficients->rows * distortion_coefficients->cols == 5 ||
				distortion_coefficients->rows * distortion_coefficients->cols == 8));

		dk = cvMat(distortion_coefficients->rows,
		           distortion_coefficients->cols,
		           CV_MAKETYPE(CV_64F, CV_MAT_CN(distortion_coefficients->type)),
		           distortion_vector);

		cvConvert(distortion_coefficients, &dk);
		iteration = 5;
	}

	// Pointers to access source and destination data with correct depth (float or double)
	// source: 2d image coordinates
	const CvPoint2D32f* source_data_float  = (const CvPoint2D32f*)source->data.ptr;
	const CvPoint2D64f* source_data_double = (const CvPoint2D64f*)source->data.ptr;

	// source: 3d point coordinates in camera coordinates
	const CvPoint3D32f* points_3d_data_float  = (const CvPoint3D32f*)pts3d->data.ptr;
	const CvPoint3D64f* points_3d_data_double = (const CvPoint3D64f*)pts3d->data.ptr;

	// destination: undistorted coordinates
	CvPoint2D32f* destination_data_float  = (CvPoint2D32f*)destination->data.ptr;
	CvPoint2D64f* destination_data_double = (CvPoint2D64f*)destination->data.ptr;

	auto source_type      = CV_MAT_TYPE(source->type);
	auto pts3d_type       = CV_MAT_TYPE(pts3d->type);
	auto destination_type = CV_MAT_TYPE(destination->type);
	auto source_step      = source->rows == 1 ? 1 : source->step / CV_ELEM_SIZE(source_type);
	auto pts_3d_step      = pts3d->rows == 1 ? 1 : pts3d->step / CV_ELEM_SIZE(pts3d_type);
	auto destination_step = destination->rows == 1 ? 1 : destination->step / CV_ELEM_SIZE(destination_type);

	const auto source_total_size = source->rows + source->cols - 1;
	auto fx                      = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(0, 0);
	auto fy                      = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(1, 1);
	/*auto ifx = 1. / fx;
	auto ify = 1. / fy;*/
	auto cx = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(0, 2);
	auto cy = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(1, 2);

	// parameters for depth compensation
	// distance for which radial distortion is calculated
	const auto s = camera_info_.intrinsics.depth_parameters.scalar[0];

	// distance focused image plane to lens
	const auto c = camera_info_.intrinsics.depth_parameters.scalar[1];

	// get the distances from 3d input points
	std::vector<double> distances(source_total_size, s);

	if((pts3d_type == CV_32FC3 || pts3d_type == CV_64FC3) && (pts3d->rows == source_total_size || pts3d->cols == source_total_size))
	{
		if(pts3d_type == CV_32FC3)
		{
			for(auto i = 0; i < source_total_size; ++i)
			{
				// transform device coordinate to camera local Z
				auto z_local = camera_info_.extrinsics.rotation_matrix.at<double>(2, 0) * points_3d_data_float[i * pts_3d_step].x;
				z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 1) * points_3d_data_float[i * pts_3d_step].y;
				z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 2) * points_3d_data_float[i * pts_3d_step].z;
				z_local += camera_info_.extrinsics.translation_vector.at<double>(2, 0);
				distances[i] = z_local;
			}
		}
		else
		{
			for(auto i = 0; i < source_total_size; ++i)
			{
				// transform device coordinate to camera local Z
				auto z_local = camera_info_.extrinsics.rotation_matrix.at<double>(2, 0) * points_3d_data_double[i * pts_3d_step].x;
				z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 1) * points_3d_data_double[i * pts_3d_step].y;
				z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 2) * points_3d_data_double[i * pts_3d_step].z;
				z_local += camera_info_.extrinsics.translation_vector.at<double>(2, 0);
				distances[i] = z_local;
			}
		}
	}

	tbb::parallel_for(0,
	                  source_total_size,
	                  [&](const size_t i)
	                  // for( int i = 0; i < n; i++ )
	                  {
		                  double x, y, z;

		                  if(source_type == CV_32FC2)
		                  {
			                  x = source_data_float[i * source_step].x;
			                  y = source_data_float[i * source_step].y;
			                  z = distances[i];
		                  }
		                  else
		                  {
			                  x = source_data_double[i * source_step].x;
			                  y = source_data_double[i * source_step].y;
			                  z = distances[i];
		                  }

		                  // distance dependent radial compensation
		                  //	Note: Seems that log/exp is faster than sqrt/pow. Maybe we should rework this function
		                  //		using cv::Mat::forEach and/or SSE functions. Even a special version for the used
		                  //		exponents (1/4, 3, 5) is possible.
		                  //
		                  //double gamma_ss = (z*(s - c)) / (s*(z - c));
		                  //double focalScale = fabs(b.dd_exp[0] - 0.25) < DBL_EPSILON ? sqrt(sqrt(gamma_ss)) : pow(gamma_ss, b.dd_exp[0]);
		                  //int e1 = (int)b.dd_exp[1];
		                  //int e2 = (int)b.dd_exp[2];
		                  //double e1Fac = fabs(e1 - b.dd_exp[1]) < DBL_EPSILON ? pow(gamma_ss, e1) : pow(gamma_ss, b.dd_exp[1]);
		                  //double e2Fac = fabs(e2 - b.dd_exp[2]) < DBL_EPSILON ? pow(gamma_ss, e2) : pow(gamma_ss, b.dd_exp[2]);

		                  const auto gamma_ss_log = log((z * (s - c)) / (s * (z - c)));
		                  const auto focal_scale  = exp(camera_info_.intrinsics.depth_parameters.exponential[0] * gamma_ss_log);

		                  const auto if_x_corrected = 1.0 / (fx * focal_scale);
		                  const auto if_y_corrected = 1.0 / (fy * focal_scale);

		                  const auto e1_factor = exp(gamma_ss_log * camera_info_.intrinsics.depth_parameters.exponential[1]);
		                  const auto e2_factor = exp(gamma_ss_log * camera_info_.intrinsics.depth_parameters.exponential[2]);

		                  double k_corrected[5];
		                  memcpy(k_corrected, distortion_vector, 5 * sizeof(double));
		                  k_corrected[0] = distortion_vector[0] * e1_factor;
		                  k_corrected[1] = distortion_vector[1] / e2_factor;

		                  const auto x0 = x = (x - cx) * if_x_corrected;
		                  const auto y0 = y = (y - cy) * if_y_corrected;

		                  // compensate distortion iteratively
		                  for(auto j = 0; j < iteration; ++j)
		                  {
			                  const auto r2      = x * x + y * y;
			                  const auto ic_dist = 1.0 / (1 + ((k_corrected[4] * r2 + k_corrected[1]) * r2 + k_corrected[0]) * r2);
			                  const auto delta_x = 2 * k_corrected[2] * x * y + k_corrected[3] * (r2 + 2 * x * x);
			                  const auto delta_y = k_corrected[2] * (r2 + 2 * y * y) + 2 * k_corrected[3] * x * y;
			                  x                  = (x0 - delta_x) * ic_dist;
			                  y                  = (y0 - delta_y) * ic_dist;
		                  }

		                  if(destination_type == CV_32FC2)
		                  {
			                  destination_data_float[i * destination_step].x = float(x);
			                  destination_data_float[i * destination_step].y = float(y);
		                  }
		                  else
		                  {
			                  destination_data_double[i * destination_step].x = x;
			                  destination_data_double[i * destination_step].y = y;
		                  }
	                  });
}

void CameraSensorDepthDependent::UndistortPointsDistanceDependent2(const cv::Mat& source,
                                                                   const cv::Mat& pts3d,
                                                                   const cv::Mat& destination,
                                                                   const cv::Mat& camera_matrix,
                                                                   const cv::Mat& distortion_coefficients) const
{
	auto iteration = 1;

	if((source.type() == CV_32FC2 || source.type() == CV_64FC2) &&
		(destination.type() == CV_32FC2 || destination.type() == CV_64FC2) && camera_matrix.rows == 3 && camera_matrix.cols == 3)
	{
		if(!distortion_coefficients.empty() && (distortion_coefficients.rows == 1 || distortion_coefficients.cols == 1) &&
			(distortion_coefficients.rows * distortion_coefficients.cols == 4 ||
				distortion_coefficients.rows * distortion_coefficients.cols == 5 ||
				distortion_coefficients.rows * distortion_coefficients.cols == 8))
		{
			iteration = 5;
		}

		// Pointers to access source and destination data with correct depth (float or double)
		// source: 2d image coordinates
		const CvPoint2D32f* source_data_float  = (const CvPoint2D32f*)source.data;
		const CvPoint2D64f* source_data_double = (const CvPoint2D64f*)source.data;

		// source: 3d point coordinates in camera coordinates
		const CvPoint3D32f* points_3d_data_float  = (const CvPoint3D32f*)pts3d.data;
		const CvPoint3D64f* points_3d_data_double = (const CvPoint3D64f*)pts3d.data;

		// destination: undistorted coordinates
		CvPoint2D32f* destination_data_float  = (CvPoint2D32f*)destination.data;
		CvPoint2D64f* destination_data_double = (CvPoint2D64f*)destination.data;

		auto const source_type      = source.type();
		auto const pts3d_type       = pts3d.type();
		auto const destination_type = destination.type();
		auto const source_step      = source.rows == 1 ? 1 : source.step / CV_ELEM_SIZE(source_type);
		auto const pts_3d_step      = pts3d.rows == 1 ? 1 : pts3d.step / CV_ELEM_SIZE(pts3d_type);
		auto const destination_step = destination.rows == 1 ? 1 : destination.step / CV_ELEM_SIZE(destination_type);

		const auto source_total_size = source.rows + source.cols - 1;
		auto const fx                = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(0, 0);
		auto const fy                = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(1, 1);
		auto const cx                = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(0, 2);
		auto const cy                = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(1, 2);

		// parameters for depth compensation
		// distance for which radial distortion is calculated
		const auto s = camera_info_.intrinsics.depth_parameters.scalar[0];

		// distance focused image plane to lens
		const auto c = camera_info_.intrinsics.depth_parameters.scalar[1];

		// get the distances from 3d input points
		std::vector<double> distances(source_total_size, s);

		if((pts3d_type == CV_32FC3 || pts3d_type == CV_64FC3) && (pts3d.rows == source_total_size || pts3d.cols == source_total_size))
		{
			if(pts3d_type == CV_32FC3)
			{
				for(auto i = 0; i < source_total_size; ++i)
				{
					// transform device coordinate to camera local Z
					auto z_local = camera_info_.extrinsics.rotation_matrix.at<double>(2, 0) * points_3d_data_float[i * pts_3d_step].x;
					z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 1) * points_3d_data_float[i * pts_3d_step].y;
					z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 2) * points_3d_data_float[i * pts_3d_step].z;
					z_local += camera_info_.extrinsics.translation_vector.at<double>(2, 0);
					distances[i] = z_local;
				}
			}
			else
			{
				for(auto i = 0; i < source_total_size; ++i)
				{
					// transform device coordinate to camera local Z
					auto z_local = camera_info_.extrinsics.rotation_matrix.at<double>(2, 0) * points_3d_data_double[i * pts_3d_step].x;
					z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 1) * points_3d_data_double[i * pts_3d_step].y;
					z_local += camera_info_.extrinsics.rotation_matrix.at<double>(2, 2) * points_3d_data_double[i * pts_3d_step].z;
					z_local += camera_info_.extrinsics.translation_vector.at<double>(2, 0);
					distances[i] = z_local;
				}
			}
		}

		tbb::parallel_for(0,
		                  source_total_size,
		                  [&](const size_t i)
		                  //for(int i = 0; i < source_total_size; i++)
		                  {
			                  double x, y, z;

			                  if(source_type == CV_32FC2)
			                  {
				                  x = source_data_float[i * source_step].x;
				                  y = source_data_float[i * source_step].y;
				                  z = distances[i];
			                  }
			                  else
			                  {
				                  x = source_data_double[i * source_step].x;
				                  y = source_data_double[i * source_step].y;
				                  z = distances[i];
			                  }

			                  // distance dependent radial compensation
			                  //	Note: Seems that log/exp is faster than sqrt/pow. Maybe we should rework this function
			                  //		using cv::Mat::forEach and/or SSE functions. Even a special version for the used
			                  //		exponents (1/4, 3, 5) is possible.
			                  //
			                  //double gamma_ss = (z*(s - c)) / (s*(z - c));
			                  //double focalScale = fabs(b.dd_exp[0] - 0.25) < DBL_EPSILON ? sqrt(sqrt(gamma_ss)) : pow(gamma_ss, b.dd_exp[0]);
			                  //int e1 = (int)b.dd_exp[1];
			                  //int e2 = (int)b.dd_exp[2];
			                  //double e1Fac = fabs(e1 - b.dd_exp[1]) < DBL_EPSILON ? pow(gamma_ss, e1) : pow(gamma_ss, b.dd_exp[1]);
			                  //double e2Fac = fabs(e2 - b.dd_exp[2]) < DBL_EPSILON ? pow(gamma_ss, e2) : pow(gamma_ss, b.dd_exp[2]);

			                  const auto gamma_ss_log = log((z * (s - c)) / (s * (z - c)));
			                  const auto focal_scale  = exp(camera_info_.intrinsics.depth_parameters.exponential[0] * gamma_ss_log);

			                  const auto if_x_corrected = 1.0 / (fx * focal_scale);
			                  const auto if_y_corrected = 1.0 / (fy * focal_scale);

			                  const auto e1_factor = exp(gamma_ss_log * camera_info_.intrinsics.depth_parameters.exponential[1]);
			                  const auto e2_factor = exp(gamma_ss_log * camera_info_.intrinsics.depth_parameters.exponential[2]);

			                  const auto k_0 = camera_info_.intrinsics.distortion_vector.at<double>(0) * e1_factor;
			                  const auto k_1 = camera_info_.intrinsics.distortion_vector.at<double>(1) / e2_factor;
			                  const auto k_2 = camera_info_.intrinsics.distortion_vector.at<double>(2);
			                  const auto k_3 = camera_info_.intrinsics.distortion_vector.at<double>(3);
			                  const auto k_4 = camera_info_.intrinsics.distortion_vector.at<double>(4);

			                  const auto x0 = x = (x - cx) * if_x_corrected;
			                  const auto y0 = y = (y - cy) * if_y_corrected;

			                  // compensate distortion iteratively
			                  for(auto j = 0; j < iteration; ++j)
			                  {
				                  const auto r2      = x * x + y * y;
				                  const auto ic_dist = 1.0 / (1 + ((k_4 * r2 + k_1) * r2 + k_0) * r2);
				                  const auto delta_x = 2 * k_2 * x * y + k_3 * (r2 + 2 * x * x);
				                  const auto delta_y = k_2 * (r2 + 2 * y * y) + 2 * k_3 * x * y;
				                  x                  = (x0 - delta_x) * ic_dist;
				                  y                  = (y0 - delta_y) * ic_dist;
			                  }

			                  if(destination_type == CV_32FC2)
			                  {
				                  destination_data_float[i * destination_step].x = float(x);
				                  destination_data_float[i * destination_step].y = float(y);
			                  }
			                  else
			                  {
				                  destination_data_double[i * destination_step].x = x;
				                  destination_data_double[i * destination_step].y = y;
			                  }
		                  });
	}
}

void CameraSensorDepthDependent::UndistortPointsDistanceDependentLaser(const cv::Mat& source,
                                                                       const cv::Mat& pts3d,
                                                                       const cv::Mat& destination,
                                                                       const cv::Mat& laser_matrix,
                                                                       const cv::Mat& distortion_coefficients) const
{
	auto iteration = 1;

	if((source.type() == CV_32FC2 || source.type() == CV_64FC2) &&
		(destination.type() == CV_32FC2 || destination.type() == CV_64FC2) && laser_matrix.rows == 3 && laser_matrix.cols == 3)
	{
		if(!distortion_coefficients.empty() && (distortion_coefficients.rows == 1 || distortion_coefficients.cols == 1) &&
			(distortion_coefficients.rows * distortion_coefficients.cols == 4 ||
				distortion_coefficients.rows * distortion_coefficients.cols == 5 ||
				distortion_coefficients.rows * distortion_coefficients.cols == 8))
		{
			iteration = 5;
		}

		// Pointers to access source and destination data with correct depth (float or double)
		// source: 2d image coordinates
		const CvPoint2D32f* source_data_float  = (const CvPoint2D32f*)source.data;
		const CvPoint2D64f* source_data_double = (const CvPoint2D64f*)source.data;

		// source: 3d point coordinates in camera coordinates
		const CvPoint3D32f* points_3d_data_float  = (const CvPoint3D32f*)pts3d.data;
		const CvPoint3D64f* points_3d_data_double = (const CvPoint3D64f*)pts3d.data;

		// destination: undistorted coordinates
		CvPoint2D32f* destination_data_float  = (CvPoint2D32f*)destination.data;
		CvPoint2D64f* destination_data_double = (CvPoint2D64f*)destination.data;

		auto const source_type      = source.type();
		auto const pts3d_type       = pts3d.type();
		auto const destination_type = destination.type();
		auto const source_step      = source.rows == 1 ? 1 : source.step / CV_ELEM_SIZE(source_type);
		auto const pts_3d_step      = pts3d.rows == 1 ? 1 : pts3d.step / CV_ELEM_SIZE(pts3d_type);
		auto const destination_step = destination.rows == 1 ? 1 : destination.step / CV_ELEM_SIZE(destination_type);

		const auto source_total_size = source.rows + source.cols - 1;
		auto const fx                = laser_matrix.at<double>(0, 0);
		auto const fy                = laser_matrix.at<double>(1, 1);
		auto const cx                = laser_matrix.at<double>(0, 2);
		auto const cy                = laser_matrix.at<double>(1, 2);

		//// parameters for depth compensation
		//// distance for which radial distortion is calculated
		//const auto s1 = camera_info_.intrinsics.depth_parameters.scalar[0];

		//// distance focused image plane to lens
		//const auto c1 = camera_info_.intrinsics.depth_parameters.scalar[1];

		// get the distances from 3d input points
		std::vector<double> distances(source_total_size, 1);

		//if((pts3d_type == CV_32FC3 || pts3d_type == CV_64FC3) && (pts3d.rows == source_total_size || pts3d.cols == source_total_size))
		//{
		//	if(pts3d_type == CV_32FC3)
		//	{
		//		for(auto i = 0; i < source_total_size; ++i)
		//		{
		//			// transform device coordinate to camera local Z
		//			auto z_local = laser_info_.extrinsics.rotation_matrix.at<double>(2, 0) * points_3d_data_float[i * pts_3d_step].x;
		//			z_local += laser_info_.extrinsics.rotation_matrix.at<double>(2, 1) * points_3d_data_float[i * pts_3d_step].y;
		//			z_local += laser_info_.extrinsics.rotation_matrix.at<double>(2, 2) * points_3d_data_float[i * pts_3d_step].z;
		//			z_local += laser_info_.extrinsics.translation_vector.at<double>(2, 0);
		//			distances[i] = z_local;
		//		}
		//	}
		//	else
		//	{
		//		for(auto i = 0; i < source_total_size; ++i)
		//		{
		//			// transform device coordinate to camera local Z
		//			auto z_local = laser_info_.extrinsics.rotation_matrix.at<double>(2, 0) * points_3d_data_double[i * pts_3d_step].x;
		//			z_local += laser_info_.extrinsics.rotation_matrix.at<double>(2, 1) * points_3d_data_double[i * pts_3d_step].y;
		//			z_local += laser_info_.extrinsics.rotation_matrix.at<double>(2, 2) * points_3d_data_double[i * pts_3d_step].z;
		//			z_local += laser_info_.extrinsics.translation_vector.at<double>(2, 0);
		//			distances[i] = z_local;
		//		}
		//	}
		//}

		tbb::parallel_for(0,
		                  source_total_size,
		                  [&](const size_t i)
		                  //for(int i = 0; i < source_total_size; i++)
		                  {
			                  double x, y, z;

			                  if(source_type == CV_32FC2)
			                  {
				                  x = source_data_float[i * source_step].x;
				                  y = source_data_float[i * source_step].y;
				                  z = distances[i];
			                  }
			                  else
			                  {
				                  x = source_data_double[i * source_step].x;
				                  y = source_data_double[i * source_step].y;
				                  z = distances[i];
			                  }

			                  // distance dependent radial compensation
			                  //	Note: Seems that log/exp is faster than sqrt/pow. Maybe we should rework this function
			                  //		using cv::Mat::forEach and/or SSE functions. Even a special version for the used
			                  //		exponents (1/4, 3, 5) is possible.
			                  //
			                  //double gamma_ss = (z*(s - c)) / (s*(z - c));
			                  //double focalScale = fabs(b.dd_exp[0] - 0.25) < DBL_EPSILON ? sqrt(sqrt(gamma_ss)) : pow(gamma_ss, b.dd_exp[0]);
			                  //int e1 = (int)b.dd_exp[1];
			                  //int e2 = (int)b.dd_exp[2];
			                  //double e1Fac = fabs(e1 - b.dd_exp[1]) < DBL_EPSILON ? pow(gamma_ss, e1) : pow(gamma_ss, b.dd_exp[1]);
			                  //double e2Fac = fabs(e2 - b.dd_exp[2]) < DBL_EPSILON ? pow(gamma_ss, e2) : pow(gamma_ss, b.dd_exp[2]);

			                  const auto gamma_ss_log = 1/*log((z * (s - c)) / (s * (z - c)))*/;
			                  const auto focal_scale  = 1; /* exp(camera_info_.intrinsics.depth_parameters.exponential[0] * gamma_ss_log);*/

			                  const auto if_x_corrected = 1.0 / (fx * focal_scale);
			                  const auto if_y_corrected = 1.0 / (fy * focal_scale);

			                  /*const auto e1_factor = exp(gamma_ss_log * camera_info_.intrinsics.depth_parameters.exponential[1]);
			                  const auto e2_factor = exp(gamma_ss_log * camera_info_.intrinsics.depth_parameters.exponential[2]);*/

			                  const auto k_0 = 0; //camera_info_.intrinsics.distortion_vector.at<double>(0) * e1_factor;
			                  const auto k_1 = 0; //camera_info_.intrinsics.distortion_vector.at<double>(1) / e2_factor;
			                  const auto k_2 = 0; //camera_info_.intrinsics.distortion_vector.at<double>(2);
			                  const auto k_3 = 0; //camera_info_.intrinsics.distortion_vector.at<double>(3);
			                  const auto k_4 = 0; //camera_info_.intrinsics.distortion_vector.at<double>(4);

			                  const auto x0 = x = (x - cx) * if_x_corrected;
			                  const auto y0 = y = (y - cy) * if_y_corrected;

			                  // compensate distortion iteratively
			                  for(auto j = 0; j < iteration; ++j)
			                  {
				                  const auto r2      = x * x + y * y;
				                  const auto ic_dist = 1.0 / (1 + ((k_4 * r2 + k_1) * r2 + k_0) * r2);
				                  const auto delta_x = 2 * k_2 * x * y + k_3 * (r2 + 2 * x * x);
				                  const auto delta_y = k_2 * (r2 + 2 * y * y) + 2 * k_3 * x * y;
				                  x                  = (x0 - delta_x) * ic_dist;
				                  y                  = (y0 - delta_y) * ic_dist;
			                  }

			                  if(destination_type == CV_32FC2)
			                  {
				                  destination_data_float[i * destination_step].x = float(x);
				                  destination_data_float[i * destination_step].y = float(y);
			                  }
			                  else
			                  {
				                  destination_data_double[i * destination_step].x = x;
				                  destination_data_double[i * destination_step].y = y;
			                  }
		                  });
	}
}


void CameraSensorDepthDependent::ProjectPointsInternalLaser(cv::InputArray points3d,
                                                            const cv::Mat& rotation_matrix,
                                                            const cv::Mat& translation_vector,
                                                            cv::OutputArray points2d) const
{
	auto points_3d_mat = points3d.getMat();
	auto depth         = points_3d_mat.depth();
	auto num_pts       = points_3d_mat.checkVector(3);

	if(num_pts == 0)
	{
		points2d.clear();
		return;
	}

	points2d.create(num_pts, 1, CV_MAKETYPE(depth, 2), -1, true);
	cv::Mat destination = points2d.getMat();

	// get pointer based access to src and dst of different data types
	const CvPoint3D32f* source_float_ptr  = (const CvPoint3D32f*)points_3d_mat.data;
	const CvPoint3D64f* source_double_ptr = (const CvPoint3D64f*)points_3d_mat.data;
	CvPoint2D32f* destination_float_ptr   = (CvPoint2D32f*)destination.data;
	CvPoint2D64f* destination_double_ptr  = (CvPoint2D64f*)destination.data;

	auto source_type      = CV_MAT_TYPE(points_3d_mat.type());
	auto destination_type = CV_MAT_TYPE(destination.type());
	auto source_step      = points_3d_mat.rows == 1 ? 1 : points_3d_mat.step / CV_ELEM_SIZE(source_type);
	auto tran_vec         = translation_vector.empty()
		                        ? Eigen::Vector3d(0, 0, 0)
		                        : Eigen::Vector3d(translation_vector.at<double>(0, 0),
		                                          translation_vector.at<double>(1, 0),
		                                          translation_vector.at<double>(2, 0));

	// get the rotation matrix
	cv::Mat r_mat;
	if(!rotation_matrix.empty())
	{
		if(rotation_matrix.total() == 9)
			r_mat = rotation_matrix;
		else
			cv::Rodrigues(rotation_matrix, r_mat);
	}

	Eigen::Matrix3d rotation;

	if(!r_mat.empty())
	{
		for(auto row = 0; row < 3; ++row)
		{
			for(auto col = 0; col < 3; ++col)
			{
				rotation(row, col) = r_mat.at<double>(row, col);
			}
		}
	}

	double k[5];
	for(auto i = 0; i < 5; ++i)
	{
		k[i] = distortion_vector_.at<double>(i, 0);
	}

	auto fx = camera_intrinsic_matrix_.at<double>(0, 0);
	auto fy = camera_intrinsic_matrix_.at<double>(1, 1);
	auto cx = camera_intrinsic_matrix_.at<double>(0, 2);
	auto cy = camera_intrinsic_matrix_.at<double>(1, 2);

	for(auto i = 0; i < num_pts; ++i)
	{
		Eigen::Vector3d p_src;
		if(source_type == CV_32FC3)
		{
			p_src[0] = source_float_ptr[i * source_step].x;
			p_src[1] = source_float_ptr[i * source_step].y;
			p_src[2] = source_float_ptr[i * source_step].z;
		}
		else
		{
			p_src[0] = source_double_ptr[i * source_step].x;
			p_src[1] = source_double_ptr[i * source_step].y;
			p_src[2] = source_double_ptr[i * source_step].z;
		}

		// 3D points to camera coordinate system. here everything is transformed to respective cam co-sys
		auto p_cam = r_mat.empty() ? p_src : rotation * p_src + tran_vec;
		auto x     = p_cam[0] / p_cam[2];
		auto y     = p_cam[1] / p_cam[2];
		auto z     = p_cam[2];

		double r2, r4, r6, a1, a2, a3, c_distance;
		double xd, yd;
		double k_corrected[5];
		memcpy(k_corrected, k, 5 * sizeof(double));

		auto fx_corrected = fx;
		auto fy_corrected = fy;

		r2 = x * x + y * y;
		r4 = r2 * r2;
		r6 = r4 * r2;

		a1         = 2 * x * y;
		a2         = r2 + 2 * x * x;
		a3         = r2 + 2 * y * y;
		c_distance = 1 + k_corrected[0] * r2 + k_corrected[1] * r4 + k_corrected[4] * r6;

		xd = x * c_distance + k_corrected[2] * a1 + k_corrected[3] * a2;
		yd = y * c_distance + k_corrected[2] * a3 + k_corrected[3] * a1;

		if(destination_type == CV_32FC2)
		{
			destination_float_ptr[i * source_step].x = float(xd * fx_corrected + cx);
			destination_float_ptr[i * source_step].y = float(yd * fy_corrected + cy);
		}
		else
		{
			destination_double_ptr[i * source_step].x = xd * fx_corrected + cx;
			destination_double_ptr[i * source_step].y = yd * fy_corrected + cy;
		}
	}
}

void CameraSensorDepthDependent::ProjectPointsInternal(cv::InputArray points3d,
                                                       const cv::Mat& rotation_matrix,
                                                       const cv::Mat& translation_vector,
                                                       cv::OutputArray points2d,
                                                       bool ignore_depth) const
{
	auto points_3d_mat = points3d.getMat();
	auto depth         = points_3d_mat.depth();
	auto num_pts       = points_3d_mat.checkVector(3);

	if(num_pts == 0)
	{
		points2d.clear();
		return;
	}

	points2d.create(num_pts, 1, CV_MAKETYPE(depth, 2), -1, true);
	auto points_2d_mat = points2d.getMat();

	// get pointer based access to src and dst of different data types
	const CvPoint3D32f* source_float_ptr  = (const CvPoint3D32f*)points_3d_mat.data;
	const CvPoint3D64f* source_double_ptr = (const CvPoint3D64f*)points_3d_mat.data;
	CvPoint2D32f* destination_float_ptr   = (CvPoint2D32f*)points_2d_mat.data;
	CvPoint2D64f* destination_double_ptr  = (CvPoint2D64f*)points_2d_mat.data;

	auto source_type      = CV_MAT_TYPE(points_3d_mat.type());
	auto destination_type = CV_MAT_TYPE(points_2d_mat.type());
	auto source_step      = points_3d_mat.rows == 1 ? 1 : points_3d_mat.step / CV_ELEM_SIZE(source_type);
	//auto destination_step = destination.rows == 1 ? 1 : destination.step / CV_ELEM_SIZE(destination_type);

	auto tran_vec = translation_vector.empty()
		                ? Eigen::Vector3d(0, 0, 0)
		                : Eigen::Vector3d(translation_vector.at<double>(0, 0),
		                                  translation_vector.at<double>(1, 0),
		                                  translation_vector.at<double>(2, 0));

	// get the rotation matrix
	cv::Mat r_mat;
	if(!rotation_matrix.empty())
	{
		if(rotation_matrix.total() == 9)
			r_mat = rotation_matrix;
		else
			cv::Rodrigues(rotation_matrix, r_mat);
	}

	Eigen::Matrix3d rotation;
	if(!r_mat.empty())
	{
		for(auto row = 0; row < 3; ++row)
		{
			for(auto col = 0; col < 3; ++col)

			{
				rotation(row, col) = r_mat.at<double>(row, col);
			}
		}
	}

	double k[5];
	for(auto i = 0; i < 5; ++i)
	{
		k[i] = camera_info_.intrinsics.distortion_vector.at<double>(i, 0);
	}

	auto fx = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(0, 0);
	auto fy = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(1, 1);
	auto cx = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(0, 2);
	auto cy = camera_info_.intrinsics.camera_intrinsic_matrix.at<double>(1, 2);

	for(auto i = 0; i < num_pts; ++i)
	{
		Eigen::Vector3d p_src;
		if(source_type == CV_32FC3)
		{
			p_src[0] = source_float_ptr[i * source_step].x;
			p_src[1] = source_float_ptr[i * source_step].y;
			p_src[2] = source_float_ptr[i * source_step].z;
		}
		else
		{
			p_src[0] = source_double_ptr[i * source_step].x;
			p_src[1] = source_double_ptr[i * source_step].y;
			p_src[2] = source_double_ptr[i * source_step].z;
		}

		// 3D points to camera coordinate system. here everything is transformed to respective cam co-sys
		auto p_cam = r_mat.empty() ? p_src : rotation * p_src + tran_vec;
		auto x     = p_cam[0] / p_cam[2];
		auto y     = p_cam[1] / p_cam[2];
		auto z     = p_cam[2];

		double r2, r4, r6, a1, a2, a3, c_distance;
		double xd, yd;
		double k_corrected[5];
		memcpy(k_corrected, k, 5 * sizeof(double));

		auto fx_corrected = fx;
		auto fy_corrected = fy;

		if(!ignore_depth)
		{
			// at z=s the term evaluates to 1,  i.e. no further correction
			auto gamma_ss = z * (camera_info_.intrinsics.depth_parameters.scalar[0] - camera_info_.intrinsics.depth_parameters.scalar[1]) / (
				camera_info_.intrinsics.depth_parameters.scalar[0] * (z - camera_info_.intrinsics.depth_parameters.scalar[1]));

			auto focal_scale = camera_info_.intrinsics.depth_parameters.exponential[0] == 0.25
				                   ? sqrt(sqrt(gamma_ss))
				                   : pow(gamma_ss, camera_info_.intrinsics.depth_parameters.exponential[0]);

			fx_corrected = fx * focal_scale;
			fy_corrected = fy * focal_scale;

			// calculate with integer exponents if possible
			auto e1 = int(camera_info_.intrinsics.depth_parameters.exponential[1]);
			auto e2 = int(camera_info_.intrinsics.depth_parameters.exponential[2]);

			auto e1_fac = e1 == camera_info_.intrinsics.depth_parameters.exponential[1]
				              ? pow(gamma_ss, e1)
				              : pow(gamma_ss, camera_info_.intrinsics.depth_parameters.exponential[1]);

			auto e2_fac = e2 == camera_info_.intrinsics.depth_parameters.exponential[2]
				              ? pow(gamma_ss, e2)
				              : pow(gamma_ss, camera_info_.intrinsics.depth_parameters.exponential[2]);

			k_corrected[0] = k[0] * e1_fac;
			k_corrected[1] = k[1] / e2_fac;
		}

		r2 = x * x + y * y;
		r4 = r2 * r2;
		r6 = r4 * r2;

		a1         = 2 * x * y;
		a2         = r2 + 2 * x * x;
		a3         = r2 + 2 * y * y;
		c_distance = 1 + k_corrected[0] * r2 + k_corrected[1] * r4 + k_corrected[4] * r6;

		xd = x * c_distance + k_corrected[2] * a1 + k_corrected[3] * a2;
		yd = y * c_distance + k_corrected[2] * a3 + k_corrected[3] * a1;

		if(destination_type == CV_32FC2)
		{
			destination_float_ptr[i * source_step].x = float(xd * fx_corrected + cx);
			destination_float_ptr[i * source_step].y = float(yd * fy_corrected + cy);
		}
		else
		{
			destination_double_ptr[i * source_step].x = xd * fx_corrected + cx;
			destination_double_ptr[i * source_step].y = yd * fy_corrected + cy;
		}
	}
}
