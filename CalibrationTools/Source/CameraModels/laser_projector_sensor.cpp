#include "laser_projector_sensor.h"
#include "helper.h"

namespace DMVS
{
namespace CameraCalibration
{
LaserProjectorSensor::LaserProjectorSensor() :
	Sensor(cv::Mat::eye(3, 3, CV_64FC1),
	       cv::Mat::zeros(3, 1, CV_64FC1),
	       CHANNEL_DEFAULT)
{
	SyncTransformationMatrix();
}

LaserProjectorSensor::LaserProjectorSensor(const LaserInfo& laser_info) :
	Sensor(laser_info.extrinsics.rotation_matrix,
	       laser_info.extrinsics.translation_vector,
	       laser_info.channel)
{
	SyncTransformationMatrix();
}

LaserProjectorSensor::LaserProjectorSensor(const cv::Mat4d& transformation_matrix) :
	Sensor(transformation_matrix,
	       CHANNEL_DEFAULT)
{
	SyncExtrinsicParameters();
}

LaserProjectorSensor::LaserProjectorSensor(const cv::Mat& rotation_matrix,
                                           const cv::Mat& translation_vector,
                                           const ImageChannel image_channel):
	Sensor(rotation_matrix,
	       translation_vector,
	       image_channel)
{
	SyncTransformationMatrix();
}

LaserProjectorSensor::LaserProjectorSensor(const LaserProjectorSensor& rhs)
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;
	}
}

LaserProjectorSensor& LaserProjectorSensor::operator=(const LaserProjectorSensor& rhs)
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;
	}

	return *this;
}

LaserProjectorSensor::LaserProjectorSensor(LaserProjectorSensor&& rhs) noexcept
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;

		// Free rhs from its values
		rhs.rotation_matrix_       = cv::Mat::zeros(3, 3, 0);
		rhs.translation_vector_    = cv::Mat::zeros(3, 1, 0);
		rhs.transformation_matrix_ = cv::Mat::zeros(4, 4, 0);
		rhs.image_channel_         = CHANNEL_DEFAULT;
	}
}

LaserProjectorSensor& LaserProjectorSensor::operator=(LaserProjectorSensor&& rhs) noexcept
{
	if(this != &rhs)
	{
		rotation_matrix_       = rhs.rotation_matrix_;
		translation_vector_    = rhs.translation_vector_;
		transformation_matrix_ = rhs.transformation_matrix_;
		image_channel_         = rhs.image_channel_;

		// Free rhs from its values
		rhs.rotation_matrix_       = cv::Mat::zeros(3, 3, 0);
		rhs.translation_vector_    = cv::Mat::zeros(3, 1, 0);
		rhs.transformation_matrix_ = cv::Mat::zeros(4, 4, 0);
		rhs.image_channel_         = CHANNEL_DEFAULT;
	}

	return *this;
}

LaserProjectorSensor::~LaserProjectorSensor() = default;

bool LaserProjectorSensor::HandlesDepthValues() const
{
	return false;
}

void LaserProjectorSensor::RayReconstructionWCS(cv::InputArray points_2d,
                                                cv::OutputArray out,
                                                const bool normalize,
                                                cv::InputArray points3d) const
{
	std::vector<cv::Point2f> undistorted;
	UndistortPoints(points_2d, undistorted, points3d);

	//Using opencv structures as described here:
	//http://stackoverflow.com/questions/25750041/pass-opencv-inputarray-and-use-it-as-stdvector
	out.create(points_2d.size(), out.type());

	cv::Mat out_mat = out.getMat();
	const cv::Mat out_double(out_mat.size(), out.type());
	const auto data = reinterpret_cast<cv::Point3f*>(out_double.data);
	const auto origin(FromSensorToWorldCoordinateSystem(cv::Point3f(0., 0., 0.)));
	size_t index = 0;

	for(const auto& point : undistorted)
	{
		//point in camera CS
		cv::Point3f point3(point.x, point.y, 1.0);

		//point in world CS
		point3 = FromSensorToWorldCoordinateSystem(point3);

		//direction from camera origin to point at 1m (in camera CS), this is the ray direction in world coordinates
		point3 = point3 - origin;

		if(normalize)
		{
			point3 = point3 * (1. / cv::norm<float>(point3));
		}

		data[index] = point3;
		++index;
	}

	out_double.convertTo(out_mat, out_mat.type());
}

void LaserProjectorSensor::RayReconstructionSCS(cv::InputArray points_2d,
                                                cv::OutputArray out,
                                                const bool normalize,
                                                cv::InputArray points3d) const
{
	std::vector<cv::Point2f> undistorted;
	UndistortPoints(points_2d, undistorted, points3d);

	//Using opencv structures as described here:
	//http://stackoverflow.com/questions/25750041/pass-opencv-inputarray-and-use-it-as-stdvector
	out.create(points_2d.size(), out.type());

	cv::Mat out_mat = out.getMat();
	const cv::Mat out_double(out_mat.size(), out.type());
	const auto data = reinterpret_cast<cv::Point3f*>(out_double.data);
	const auto origin(FromSensorToWorldCoordinateSystem(cv::Point3f(0., 0., 0.)));
	size_t index = 0;

	for(const auto& point : undistorted)
	{
		//point in camera CS
		cv::Point3f point3(point.x, point.y, 1.0);

		//point in world CS
		point3 = FromSensorToWorldCoordinateSystem(point3);

		//direction from camera origin to point at 1m (in camera CS), this is the ray direction in world coordinates
		point3 = point3 - origin;

		if(normalize)
		{
			point3 = point3 * (1. / cv::norm<float>(point3));
		}

		data[index] = point3;
		++index;
	}

	out_double.convertTo(out_mat, out_mat.type());
}

void LaserProjectorSensor::GetPlaneIntersectionPoints(cv::InputArray points_2d,
                                                      const cv::Vec3d& plane_position,
                                                      const cv::Vec3d& plane_normal,
                                                      cv::OutputArray intersection_points_coordinate_system) const
{
	// Plane position and plane normal are in camera coordinate system
	intersection_points_coordinate_system.create(points_2d.size(), intersection_points_coordinate_system.type(), -1, true);
	const auto out_matrix = intersection_points_coordinate_system.getMat();
	const auto data       = reinterpret_cast<cv::Point3f*>(out_matrix.data);

	//Camera position in Freestyle coordinates as ray origin
	// cv::Point3f origin(0., 0., 0.);
	//origin = FromCameraToWorldCoordinateSystem(origin);
	// const cv::Vec3d ray_location(origin.x, origin.y, origin.z);

	////Eigen::Vector3d egPlanePos( planePos.x, planePos.y, planePos.z );
	////Eigen::Vector3d egPlaneNormal( planeNormal.x, planeNormal.y, planeNormal.z );
	//cv::Vec3d plane_normalized;
	//cv::normalize(plane_normal, plane_normalized);
	//const auto d_hesse = plane_normalized.dot(plane_position);

	////Calculate intersection Points of IR0 beams with plane detected by color camera
	//std::vector<cv::Point3f> ray_directions;
	//RayReconstruction(points_2d, ray_directions, true);

	//std::vector<cv::Point3f> points_3d;

	//for(size_t idx = 0; idx < ray_directions.size(); ++idx)
	//{
	//	cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
	//	const auto line_direction_scale = (d_hesse - plane_normalized.dot(ray_location)) / plane_normalized.dot(ray_direction);
	//	auto point                      = line_direction_scale * ray_direction + ray_location;
	//	data[idx]                       = cv::Point3f(point[0], point[1], point[2]);
	//	points_3d.push_back(data[idx]);
	//}

	cv::Point3f origin(0., 0., 0.);
	const cv::Vec3d ray_location(origin.x, origin.y, origin.z);
	cv::Vec3d plane_normalized;
	cv::normalize(plane_normal, plane_normalized);
	const auto d_hesse = plane_normalized.dot(plane_position);

	//Calculate intersection Points of IR0 beams with plane detected by color camera
	std::vector<cv::Point3f> ray_directions;
	RayReconstructionWCS(points_2d, ray_directions, true);

	std::vector<cv::Point3f> points_3d;

	for(size_t idx = 0; idx < ray_directions.size(); ++idx)
	{
		cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
		const auto line_direction_scale = (d_hesse - plane_normalized.dot(ray_location)) / plane_normalized.dot(ray_direction);
		auto point                      = (line_direction_scale * ray_direction) + ray_location;
		cv::Point3f temp_point          = cv::Point3f(point[0], point[1], point[2]);
		data[idx] = FromSensorToWorldCoordinateSystem(temp_point);
		points_3d.push_back(data[idx]);
	}

	//second run if sensor supports depth dependent un-distortion
	if(HandlesDepthValues())
	{
		//ray_directions.clear();
		//RayReconstruction(points_2d, ray_directions, true, points_3d);
		//for(size_t idx = 0; idx < ray_directions.size(); ++idx)
		//{
		//	cv::Vec3d ray_direction(ray_directions[idx].x, ray_directions[idx].y, ray_directions[idx].z);
		//	const auto line_direction_scale = (d_hesse - plane_normal.dot(ray_location)) / plane_normal.dot(ray_direction);
		//	auto point                      = line_direction_scale * ray_direction + ray_location;
		//	data[idx]                       = cv::Point3f(point[0], point[1], point[2]);
		//}
	}
}
}
}
