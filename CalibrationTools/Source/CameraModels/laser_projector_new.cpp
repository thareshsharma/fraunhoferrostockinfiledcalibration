#include "helper.h"
#include "laser_projector_new.h"

using namespace DMVS::CameraCalibration;

LaserProjectorNew::LaserProjectorNew() :
	LaserProjectorSensor()
{
	////parameter_representation_ = std::vector<double>(Constants::PARAMS_SIZE, 0);
	//parameter_fixations_.resize(Constants::PARAMS_SIZE);
	//// all parameters are modifiable at calibration if not set fix later
	//for(size_t i                = 0; i < Constants::PARAMS_SIZE; ++i)
	//	parameter_fixations_[i] = i;
}

LaserProjectorNew::LaserProjectorNew(const LaserInfo& laser_info) :
	LaserProjectorSensor(),
	laser_info_(laser_info)
{
	// Update Parameters
	const auto rot_mat = cv::Mat::eye(3, 3, CV_64F);
	const cv::Mat translate(cv::Vec3d(0.06842076778411865, 0.002079565078020096, -0.03215530514717102));
}

LaserProjectorNew::LaserProjectorNew(const std::shared_ptr<const LaserProjectorNew> rhs) :
	LaserProjectorSensor()
{
	////parameter_representation_ = std::vector<double>(Constants::PARAMS_SIZE, 0);
	//parameter_fixations_ = rhs->parameter_fixations_;
	//std::vector<size_t> fixations;
	////LaserProjectorNew::InitFromParamsVector(rhs->GetParamsVector(fixations));
	//LaserProjectorNew::SetImageChannel(rhs->GetImageChannel());
}

//std::vector<double> LaserProjectorNew::GetParamsVector(std::vector<size_t>& fixations) const
//{
//	fixations = parameter_fixations_;
//	return parameter_representation_;
//}

bool LaserProjectorNew::HandlesDepthValues() const
{
	return true;
}

LaserProjectorNew::LaserProjectorNew(cv::InputArray& camera_matrix,
                                     cv::InputArray& distortion,
                                     cv::InputArray& rotation_matrix,
                                     cv::InputArray& translation_vector) :
	LaserProjectorSensor()
{
	//parameter_representation_ = std::vector<double>(Constants::PARAMS_SIZE, 0);

	// set camera matrix
	laser_info_.intrinsics.projector_intrinsic_matrix = camera_matrix.getMat();
	laser_info_.intrinsics.distortion_vector = distortion.getMat();
	laser_info_.extrinsics.rotation_matrix         = rotation_matrix.getMat();
	laser_info_.extrinsics.translation_vector      = translation_vector.getMat();

	//CameraInfoToParamVector(laser_info_, parameter_representation_);
	//LaserProjectorNew::InitFromParamsVector(parameter_representation_);

	// all parameters are modifiable at calibration if not set fix later
	//parameter_fixations_.resize(20);

	//for(size_t i                = 0; i < parameter_fixations_.size(); ++i)
	//	parameter_fixations_[i] = i;

	//// fix exponential parameters of depth dependency
	//parameter_fixations_[15] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	//parameter_fixations_[16] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	//parameter_fixations_[17] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	//parameter_fixations_[18] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
	////parameter_fixations_[19] = Constants::DEFAULT_SIZE_T_MAX_VALUE;
}


LaserInfo LaserProjectorNew::GetLaserInfo() const
{
	//CameraInfo camera_info;
	////ParamVectorToCameraInfo(parameter_representation_, camera_info);
	return laser_info_;
}

LaserProjectorNew::~LaserProjectorNew() = default;

std::shared_ptr<Sensor> LaserProjectorNew::Duplicate() const
{
	auto out           = std::make_shared<LaserProjectorNew>(laser_info_);
	const auto rot_mat = cv::Mat::eye(3, 3, CV_64F);
	const cv::Mat translate(cv::Vec3d(0.06842076778411865, 0.002079565078020096, -0.03215530514717102));
	//out->SetExtrinsicParameters(rot_mat, translate);

	out->SetImageChannel(image_channel_);
	out->parameter_fixations_ = parameter_fixations_;*/

	return out;
}

void LaserProjectorNew::ProjectPoints(cv::InputArray& points_3d,
                                      cv::OutputArray points_2d,
                                      const bool apply_camera_extrinsics,
                                      const bool ignore_depth) const
{
	if(points_3d.total() == 0)
	{
		points_2d.clear();
		return;
	}
	cv::Mat rotation_matrix;
	cv::Mat translation_vector;
	GetExtrinsicParameters(rotation_matrix, translation_vector);
	if(apply_camera_extrinsics)
	{
		rotation_matrix    = rotation_matrix_.clone();
		translation_vector = translation_vector_.clone();
	}

	ProjectPointsInternal(points_3d, rotation_matrix, translation_vector, points_2d, ignore_depth);
}

void LaserProjectorNew::UndistortPoints(cv::InputArray points_2d,
                                        cv::OutputArray undistorted_points_2d,
                                        cv::InputArray points_3d) const
{
	auto source = points_2d.getMat();

	if(!(points_2d.total() > 0))
		return;

	CV_Assert(source.isContinuous() && (source.depth() == CV_32F || source.depth() == CV_64F) &&
		((source.rows == 1 && source.channels() == 2) || source.cols * source.channels() == 2));

	//auto depth        = source.depth();
	auto points_count = source.checkVector(2);

	if(points_count == 0)
	{
		undistorted_points_2d.clear();
		return;
	}

	if(undistorted_points_2d.fixedType())
	{
		undistorted_points_2d.create(source.size(), undistorted_points_2d.type(), -1, true);
	}
	else
	{
		undistorted_points_2d.create(source.size(), source.type(), -1, true);
	}

	UndistortPointsDistanceDependent(source,
	                                 points_3d.getMat(),
	                                 undistorted_points_2d.getMat(),
	                                 laser_info_.intrinsics.projector_intrinsic_matrix,
	                                 laser_info_.intrinsics.distortion_vector);
}


//bool LaserProjectorNew::GetBestFitCVModel(cv::Mat& camera_matrix,
//                                          cv::Mat& distortion_vector) const
//{
//	laser_info_.intrinsics.camera_intrinsic_matrix.copyTo(camera_matrix);
//	//laser_info_.intrinsics.distortion_vector.copyTo(distortion_vector);
//
//
//	return true;
//}

void LaserProjectorNew::SetLaserInfo(const LaserInfo& laser_info)
{
	laser_info_ = laser_info;
}

void LaserProjectorNew::UndistortPointsDistanceDependent(const cv::Mat& source,
                                                         const cv::Mat& pts3d,
                                                         const cv::Mat& destination,
                                                         const cv::Mat& camera_matrix,
                                                         const cv::Mat& distortion_coefficients) const
{	int iteration = 1;

	if( /*source.isContinuous() && destination.isContinuous() &&*/
			/*(source.rows == 1 || source.cols == 1) &&
			(destination.rows == 1 || destination.cols == 1) &&*/
			/*source.cols + source.rows - 1 == destination.rows + destination.cols - 1 &&*/
		(source.type() == CV_32FC2 || source.type() == CV_64FC2) &&
		(destination.type() == CV_32FC2 || destination.type() == CV_64FC2) && camera_matrix.rows == 3 && camera_matrix.cols == 3)
	{
		if(!distortion_coefficients.empty() && (distortion_coefficients.rows == 1 || distortion_coefficients.cols == 1) &&
			(distortion_coefficients.rows * distortion_coefficients.cols == 4 ||
				distortion_coefficients.rows * distortion_coefficients.cols == 5 ||
				distortion_coefficients.rows * distortion_coefficients.cols == 8))
		{
			iteration = 5;
		}

		// Pointers to access source and destination data with correct depth (float or double)
		// source: 2d image coordinates
		const CvPoint2D32f* source_data_float  = (const CvPoint2D32f*)source.data;
		const CvPoint2D64f* source_data_double = (const CvPoint2D64f*)source.data;

		// source: 3d point coordinates in camera coordinates
		const CvPoint3D32f* points_3d_data_float  = (const CvPoint3D32f*)pts3d.data;
		const CvPoint3D64f* points_3d_data_double = (const CvPoint3D64f*)pts3d.data;

		// destination: undistorted coordinates
		CvPoint2D32f* destination_data_float  = (CvPoint2D32f*)destination.data;
		CvPoint2D64f* destination_data_double = (CvPoint2D64f*)destination.data;

		auto source_type      = source.type();
		auto pts3d_type       = pts3d.type();
		auto destination_type = destination.type();
		auto source_step      = source.rows == 1 ? 1 : source.step / CV_ELEM_SIZE(source_type);
		auto pts_3d_step      = pts3d.rows == 1 ? 1 : pts3d.step / CV_ELEM_SIZE(pts3d_type);
		auto destination_step = destination.rows == 1 ? 1 : destination.step / CV_ELEM_SIZE(destination_type);

		const auto source_total_size = source.rows + source.cols - 1;
		auto fx                      = laser_info_.intrinsics.projector_intrinsic_matrix.at<double>(0, 0);
		auto fy                      = laser_info_.intrinsics.projector_intrinsic_matrix.at<double>(1, 1);
		/*auto ifx = 1. / fx;
		auto ify = 1. / fy;*/
		auto cx = laser_info_.intrinsics.projector_intrinsic_matrix.at<double>(0, 2);
		auto cy = laser_info_.intrinsics.projector_intrinsic_matrix.at<double>(1, 2);

		// parameters for depth compensation
		// distance for which radial distortion is calculated
		//const auto s = laser_info_.intrinsics.depth_parameters.scalar[0];

		// distance focused image plane to lens
		//const auto c = laser_info_.intrinsics.depth_parameters.scalar[1];

		// get the distances from 3d input points
		std::vector<double> distances(source_total_size, 0);

		if((pts3d_type == CV_32FC3 || pts3d_type == CV_64FC3) && (pts3d.rows == source_total_size || pts3d.cols == source_total_size))
		{
			if(pts3d_type == CV_32FC3)
			{
				for(auto i = 0; i < source_total_size; ++i)
				{
					// transform device coordinate to camera local Z
					auto z_local = laser_info_.extrinsics.rotation_matrix.at<double>(2, 0) * points_3d_data_float[i * pts_3d_step].x;
					z_local += laser_info_.extrinsics.rotation_matrix.at<double>(2, 1) * points_3d_data_float[i * pts_3d_step].y;
					z_local += laser_info_.extrinsics.rotation_matrix.at<double>(2, 2) * points_3d_data_float[i * pts_3d_step].z;
					z_local += laser_info_.extrinsics.translation_vector.at<double>(2, 0);
					distances[i] = z_local;
				}
			}
			else
			{
				for(auto i = 0; i < source_total_size; ++i)
				{
					// transform device coordinate to camera local Z
					auto z_local = laser_info_.extrinsics.rotation_matrix.at<double>(2, 0) * points_3d_data_double[i * pts_3d_step].x;
					z_local += laser_info_.extrinsics.rotation_matrix.at<double>(2, 1) * points_3d_data_double[i * pts_3d_step].y;
					z_local += laser_info_.extrinsics.rotation_matrix.at<double>(2, 2) * points_3d_data_double[i * pts_3d_step].z;
					z_local += laser_info_.extrinsics.translation_vector.at<double>(2, 0);
					distances[i] = z_local;
				}
			}
		}

		//tbb::parallel_for(0,
		//                  source_total_size,
		//                  [&](const size_t i)
		for(int i = 0; i < source_total_size; i++)
		{
			double x, y, z;

			if(source_type == CV_32FC2)
			{
				x = source_data_float[i * source_step].x;
				y = source_data_float[i * source_step].y;
				z = distances[i];
			}
			else
			{
				x = source_data_double[i * source_step].x;
				y = source_data_double[i * source_step].y;
				z = distances[i];
			}

			// distance dependent radial compensation
			//	Note: Seems that log/exp is faster than sqrt/pow. Maybe we should rework this function
			//		using cv::Mat::forEach and/or SSE functions. Even a special version for the used
			//		exponents (1/4, 3, 5) is possible.
			//
			//double gamma_ss = (z*(s - c)) / (s*(z - c));
			//double focalScale = fabs(b.dd_exp[0] - 0.25) < DBL_EPSILON ? sqrt(sqrt(gamma_ss)) : pow(gamma_ss, b.dd_exp[0]);
			//int e1 = (int)b.dd_exp[1];
			//int e2 = (int)b.dd_exp[2];
			//double e1Fac = fabs(e1 - b.dd_exp[1]) < DBL_EPSILON ? pow(gamma_ss, e1) : pow(gamma_ss, b.dd_exp[1]);
			//double e2Fac = fabs(e2 - b.dd_exp[2]) < DBL_EPSILON ? pow(gamma_ss, e2) : pow(gamma_ss, b.dd_exp[2]);

			const auto gamma_ss_log = 1;// log((z * (s - c)) / (s * (z - c)));
			const auto focal_scale = 1;// exp(laser_info_.intrinsics.depth_parameters.exponential[0] * gamma_ss_log);

			const auto if_x_corrected = 1.0 / (fx * focal_scale);
			const auto if_y_corrected = 1.0 / (fy * focal_scale);

			const auto e1_factor = 1;//exp(gamma_ss_log * laser_info_.intrinsics.depth_parameters.exponential[1]);
			const auto e2_factor = 1;//exp(gamma_ss_log * laser_info_.intrinsics.depth_parameters.exponential[2]);

			const auto k_0 = laser_info_.intrinsics.distortion_vector.at<double>(0) * e1_factor;
			const auto k_1 = laser_info_.intrinsics.distortion_vector.at<double>(1) / e2_factor;
			const auto k_2 = laser_info_.intrinsics.distortion_vector.at<double>(2);
			const auto k_3 = laser_info_.intrinsics.distortion_vector.at<double>(3);
			const auto k_4 = laser_info_.intrinsics.distortion_vector.at<double>(4);

			const auto x0 = x = (x - cx) * if_x_corrected;
			const auto y0 = y = (y - cy) * if_y_corrected;

			// compensate distortion iteratively
			for(auto j = 0; j < iteration; ++j)
			{
				const auto r2      = x * x + y * y;
				const auto ic_dist = 1.0 / (1 + ((k_4 * r2 + k_1) * r2 + k_0) * r2);
				const auto delta_x = 2 * k_2 * x * y + k_3 * (r2 + 2 * x * x);
				const auto delta_y = k_2 * (r2 + 2 * y * y) + 2 * k_3 * x * y;
				x                  = (x0 - delta_x) * ic_dist;
				y                  = (y0 - delta_y) * ic_dist;
			}

			if(destination_type == CV_32FC2)
			{
				destination_data_float[i * destination_step].x = float(x);
				destination_data_float[i * destination_step].y = float(y);
			}
			else
			{
				destination_data_double[i * destination_step].x = x;
				destination_data_double[i * destination_step].y = y;
			}
		}
	}
}

void LaserProjectorNew::ProjectPointsInternal(cv::InputArray points3d,
                                              const cv::Mat rotation_matrix,
                                              const cv::Mat translation_vector,
                                              cv::OutputArray points2d,
                                              bool ignore_depth) const
{
	/*CameraInfo*/ /*laser_info_*/
	/*ParamVectorToCameraInfo(parameter_representation_, camera_info);*/
	auto src     = points3d.getMat();
	auto depth   = src.depth();
	auto num_pts = src.checkVector(3);

	if(num_pts == 0)
	{
		points2d.clear();
		return;
	}

	points2d.create(num_pts, 1, CV_MAKETYPE(depth, 2), -1, true);
	cv::Mat destination, source;

	Helper::ToCVMat(points2d.getMat(), destination);
	Helper::ToCVMat(src, source);

	// get pointer based access to src and dst of different data types
	const CvPoint3D32f* source_float_ptr  = (const CvPoint3D32f*)source.data.ptr;
	const CvPoint3D64f* source_double_ptr = (const CvPoint3D64f*)source.data.ptr;
	CvPoint2D32f* destination_float_ptr   = (CvPoint2D32f*)destination.data.ptr;
	CvPoint2D64f* destination_double_ptr  = (CvPoint2D64f*)destination.data.ptr;

	auto source_type      = CV_MAT_TYPE(source.type);
	auto destination_type = CV_MAT_TYPE(destination.type);
	auto source_step      = source.rows == 1 ? 1 : source.step / CV_ELEM_SIZE(source_type);
	//auto destination_step = destination.rows == 1 ? 1 : destination.step / CV_ELEM_SIZE(destination_type);

	auto tran_vec = translation_vector.empty()
		                ? Eigen::Vector3d(0, 0, 0)
		                : Eigen::Vector3d(translation_vector.at<double>(0, 0),
		                                  translation_vector.at<double>(1, 0),
		                                  translation_vector.at<double>(2, 0));

	// get the rotation matrix
	cv::Mat r_mat;
	if(!rotation_matrix.empty())
	{
		if(rotation_matrix.total() == 9)
			r_mat = rotation_matrix;
		else
			cv::Rodrigues(rotation_matrix, r_mat);
	}

	Eigen::Matrix3d rotation;

	if(!r_mat.empty())
	{
		for(auto row = 0; row < 3; ++row)
		{
			for(auto col = 0; col < 3; ++col)

			{
				rotation(row, col) = r_mat.at<double>(row, col);
			}
		}
	}

	double k[5];
	for(auto i = 0; i < 5; ++i)
	{
		k[i] = 0.0; // laser_info_.intrinsics.distortion_vector.at<double>(i, 0);
	}

	auto fx = laser_info_.intrinsics.projector_intrinsic_matrix.at<double>(0, 0);
	auto fy = laser_info_.intrinsics.projector_intrinsic_matrix.at<double>(1, 1);
	auto cx = laser_info_.intrinsics.projector_intrinsic_matrix.at<double>(0, 2);
	auto cy = laser_info_.intrinsics.projector_intrinsic_matrix.at<double>(1, 2);

	for(auto i = 0; i < num_pts; ++i)
	{
		Eigen::Vector3d p_src;
		if(source_type == CV_32FC3)
		{
			p_src[0] = source_float_ptr[i * source_step].x;
			p_src[1] = source_float_ptr[i * source_step].y;
			p_src[2] = source_float_ptr[i * source_step].z;
		}
		else
		{
			p_src[0] = source_double_ptr[i * source_step].x;
			p_src[1] = source_double_ptr[i * source_step].y;
			p_src[2] = source_double_ptr[i * source_step].z;
		}

		auto p_cam = r_mat.empty() ? p_src : rotation * p_src + tran_vec;
		auto x = p_cam[0] / p_cam[2];
		auto y = p_cam[1] / p_cam[2];
		auto z = p_cam[2];

		double r2, r4, r6, a1, a2, a3, c_distance;
		double xd, yd;
		double k_corrected[5];
		memcpy(k_corrected, k, 5 * sizeof(double));

		auto fx_corrected = fx;
		auto fy_corrected = fy;

		if(!ignore_depth)
		{
			//// at z=s the term evaluates to 1,  i.e. no further correction
			//auto gamma_ss = z * (laser_info_.intrinsics.depth_parameters.scalar[0] - laser_info_.intrinsics.depth_parameters.scalar[1]) / (
			//	laser_info_.intrinsics.depth_parameters.scalar[0] * (z - laser_info_.intrinsics.depth_parameters.scalar[1]));

			//auto focal_scale = laser_info_.intrinsics.depth_parameters.exponential[0] == 0.25
			//	                   ? sqrt(sqrt(gamma_ss))
			//	                   : pow(gamma_ss, laser_info_.intrinsics.depth_parameters.exponential[0]);

			//fx_corrected = fx * focal_scale;
			//fy_corrected = fy * focal_scale;

			//// calculate with integer exponents if possible
			//auto e1 = int(laser_info_.intrinsics.depth_parameters.exponential[1]);
			//auto e2 = int(laser_info_.intrinsics.depth_parameters.exponential[2]);

			//auto e1_fac = e1 == laser_info_.intrinsics.depth_parameters.exponential[1]
			//	              ? pow(gamma_ss, e1)
			//	              : pow(gamma_ss, laser_info_.intrinsics.depth_parameters.exponential[1]);

			//auto e2_fac = e2 == laser_info_.intrinsics.depth_parameters.exponential[2]
			//	              ? pow(gamma_ss, e2)
			//	              : pow(gamma_ss, laser_info_.intrinsics.depth_parameters.exponential[2]);

			//k_corrected[0] = k[0] * e1_fac;
			//k_corrected[1] = k[1] / e2_fac;
		}

		r2 = x * x + y * y;
		r4 = r2 * r2;
		r6 = r4 * r2;

		a1         = 2 * x * y;
		a2         = r2 + 2 * x * x;
		a3         = r2 + 2 * y * y;
		c_distance = 1 + k_corrected[0] * r2 + k_corrected[1] * r4 + k_corrected[4] * r6;

		xd = x * c_distance + k_corrected[2] * a1 + k_corrected[3] * a2;
		yd = y * c_distance + k_corrected[2] * a3 + k_corrected[3] * a1;

		if(destination_type == CV_32FC2)
		{
			destination_float_ptr[i * source_step].x = float(xd * fx_corrected + cx);
			destination_float_ptr[i * source_step].y = float(yd * fy_corrected + cy);
		}
		else
		{
			destination_double_ptr[i * source_step].x = xd * fx_corrected + cx;
			destination_double_ptr[i * source_step].y = yd * fy_corrected + cy;
		}
	}
}
