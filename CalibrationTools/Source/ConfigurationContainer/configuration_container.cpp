#include "configuration_container.h"

using namespace DMVS::CameraCalibration;

ConfigurationContainer::ConfigurationContainer()
{
	Initialize();
}

ConfigurationContainer::~ConfigurationContainer() = default;

void ConfigurationContainer::Initialize()
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initializing Configuration Container  " << Constants::LOG_START;
	calibration_data_root_   = "C:\\";
	configuration_data_file_ = Helper::GetCalibrationsDirectory().append("calibrationConfig.xml");

	board_id_                     = 4;
	first_uncoded_id_             = 10;
	marker_index3d_               = 12;
	coordinate_system_definition_ = 14;
	calibration_boards_count_     = 18;
	factory_calibration_distance_ = 0.4f;

	// On-site calibration
	on_site_calibration_min_distance_  = 0.20f;
	on_site_calibration_step_distance_ = 0.02f;
	on_site_calibration_num_steps_     = 10;

	// Projector Calibration
	projector_calibration_min_bright_beams_          = 350;
	projector_calibration_center_beam_window_width_  = 200;
	projector_calibration_center_beam_window_height_ = 200;
	projector_calibration_min_points_                = 9000;

	projector_calibration_min_beam_length_               = 0.20f;
	projector_calibration_center_beam_window_position_x_ = 290.0f;
	projector_calibration_center_beam_window_position_y_ = 0.0f;
	projector_calibration_ir_exposure_at_1m_             = 1.0f;


	projector_calibration_center_beam_use_window_center_ = true;
	use_dmvs_projector_calibration_                      = true;

	// Scanning
	freestyle_min_distance_ = 0.3;

	if_force_beam_                    = false;
	show_symmetry_projection_         = false;
	min_projector_calibration_points_ = 6000;

	min_beam_length_            = 0.20f;
	force_beam_x_               = 311.0f;
	force_beam_y_               = 11.00f;
	max_distance_symmetry_test_ = 1.0f;

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initializing Configuration Container  " << Constants::LOG_END;
}

fs::path ConfigurationContainer::GetConfigurationDataFile() const
{
	return configuration_data_file_;
}

void ConfigurationContainer::SetConfigurationDataFile(const std::string& configuration_data_file)
{
	configuration_data_file_ = configuration_data_file;
}

int ConfigurationContainer::GetBoardId() const
{
	return board_id_;
}

void ConfigurationContainer::SetBoardId(const int board_id)
{
	board_id_ = board_id;
}

int ConfigurationContainer::GetFirstUncodedId() const
{
	return first_uncoded_id_;
}

void ConfigurationContainer::SetFirstUncodedId(const int uncoded_id)
{
	first_uncoded_id_ = uncoded_id;
}

int ConfigurationContainer::GetMarkerIndex3d() const
{
	return marker_index3d_;
}

void ConfigurationContainer::SetMarkerIndex3d(const int marker_index3d)
{
	marker_index3d_ = marker_index3d;
}

int ConfigurationContainer::GetCoordinateSystemDefinition() const
{
	return coordinate_system_definition_;
}

void ConfigurationContainer::SetCoordinateSystemDefinition(const int coordinate_system_definition)
{
	coordinate_system_definition_ = coordinate_system_definition;
}

std::string ConfigurationContainer::GetCalibrationDataRoot() const
{
	return calibration_data_root_;
}

void ConfigurationContainer::SetCalibrationDataRoot(const std::string& data_root)
{
	calibration_data_root_ = data_root;
}

int ConfigurationContainer::GetCalibrationBoardsCount() const
{
	return calibration_boards_count_;
}

void ConfigurationContainer::SetCalibrationBoardsCount(const int calibration_boards_count)
{
	calibration_boards_count_ = calibration_boards_count;
}

std::string ConfigurationContainer::GetPDFReportFileName() const
{
	return pdf_report_file_name_;
}

void ConfigurationContainer::SetPDFReportFileName(const std::string& file_name)
{
	pdf_report_file_name_ = file_name;
}

std::string ConfigurationContainer::GetInfieldReportLogoPath() const
{
	return infield_report_logo_path_;
}

void ConfigurationContainer::SetInfieldReportLogoPath(const std::string& logo_path)
{
	infield_report_logo_path_ = logo_path;
}

double ConfigurationContainer::GetColorCameraRMS() const
{
	return color_camera_rms_;
}

void ConfigurationContainer::SetColorCameraRMS(const double camera_rms)
{
	color_camera_rms_ = camera_rms;
}

double ConfigurationContainer::GetLeftCameraRMS() const
{
	return left_camera_rms_;
}

void ConfigurationContainer::SetLeftCameraRMS(const double left_camera_rms)
{
	left_camera_rms_ = left_camera_rms;
}

double ConfigurationContainer::GetRightCameraRMS() const
{
	return right_camera_rms_;
}

void ConfigurationContainer::SetRightCameraRMS(const double right_camera_rms)
{
	right_camera_rms_ = right_camera_rms;
}

double ConfigurationContainer::GetFactoryCalibrationMaxRMS() const
{
	return factory_calibration_max_rms_;
}

void ConfigurationContainer::SetFactoryCalibrationMaxRMS(const double factory_calibration_max_rms)
{
	factory_calibration_max_rms_ = factory_calibration_max_rms;
}

//bool ConfigurationContainer::GetFactoryCalibrationSuccessful() const
//{
//	return if_factory_calibration_successful_;
//}
//
//void ConfigurationContainer::SetFactoryCalibrationSuccessful(const bool if_successful)
//{
//	if_factory_calibration_successful_ = if_successful;
//}

std::string& ConfigurationContainer::GetContent()
{
	return content_;
}

void ConfigurationContainer::SetContent(const std::string& content)
{
	content_ = content;
}

float ConfigurationContainer::GetFactoryCalibrationDistance() const
{
	return factory_calibration_distance_;
}

void ConfigurationContainer::SetFactoryCalibrationDistance(const float factory_calibration_distance)
{
	factory_calibration_distance_ = factory_calibration_distance;
}

float ConfigurationContainer::GetOnsiteCalibrationMinDistance() const
{
	return on_site_calibration_min_distance_;
}

void ConfigurationContainer::SetOnsiteCalibrationMinDistance(const float onsite_calibration_min_distance)
{
	on_site_calibration_min_distance_ = onsite_calibration_min_distance;
}

float ConfigurationContainer::GetOnsiteCalibrationStepDistance() const
{
	return on_site_calibration_step_distance_;
}

void ConfigurationContainer::SetOnsiteCalibrationStepDistance(const float onsite_calibration_step_distance)
{
	on_site_calibration_step_distance_ = onsite_calibration_step_distance;
}

int ConfigurationContainer::GetOnsiteCalibrationNumSteps() const
{
	return on_site_calibration_num_steps_;
}

void ConfigurationContainer::SetOnsiteCalibrationNumSteps(const int onsite_calibration_num_steps)
{
	on_site_calibration_num_steps_ = onsite_calibration_num_steps;
}

float ConfigurationContainer::GetProjectorCalibrationMinBeamLength() const
{
	return projector_calibration_min_beam_length_;
}

void ConfigurationContainer::SetProjectorCalibrationMinBeamLength(const float projector_calibration_min_beam_length)
{
	projector_calibration_min_beam_length_ = projector_calibration_min_beam_length;
}

int ConfigurationContainer::GetProjectorCalibrationMinBrightBeams() const
{
	return projector_calibration_min_bright_beams_;
}

void ConfigurationContainer::SetProjectorCalibrationMinBrightBeams(const int projector_calibration_min_bright_beams)
{
	projector_calibration_min_bright_beams_ = projector_calibration_min_bright_beams;
}

float ConfigurationContainer::GetProjectorCalibrationCenterBeamWindowPositionX() const
{
	return projector_calibration_center_beam_window_position_x_;
}

void ConfigurationContainer::SetProjectorCalibrationCenterBeamWindowPositionX(const float projector_calibration_center_beam_window_position_x)
{
	projector_calibration_center_beam_window_position_x_ = projector_calibration_center_beam_window_position_x;
}

float ConfigurationContainer::GetProjectorCalibrationCenterBeamWindowPositionY() const
{
	return projector_calibration_center_beam_window_position_y_;
}

void ConfigurationContainer::SetProjectorCalibrationCenterBeamWindowPositionY(const float projector_calibration_center_beam_window_position_y)
{
	projector_calibration_center_beam_window_position_y_ = projector_calibration_center_beam_window_position_y;
}

int ConfigurationContainer::GetProjectorCalibrationCenterBeamWindowWidth() const
{
	return projector_calibration_center_beam_window_width_;
}

void ConfigurationContainer::SetProjectorCalibrationCenterBeamWindowWidth(const int projector_calibration_center_beam_window_width)
{
	projector_calibration_center_beam_window_width_ = projector_calibration_center_beam_window_width;
}

int ConfigurationContainer::GetProjectorCalibrationCenterBeamWindowHeight() const
{
	return projector_calibration_center_beam_window_height_;
}

void ConfigurationContainer::SetProjectorCalibrationCenterBeamWindowHeight(const int projector_calibration_center_beam_window_height)
{
	projector_calibration_center_beam_window_height_ = projector_calibration_center_beam_window_height;
}

int ConfigurationContainer::GetProjectorCalibrationMinPoints() const
{
	return projector_calibration_min_points_;
}

void ConfigurationContainer::SetProjectorCalibrationMinPoints(const int projector_calibration_min_points)
{
	projector_calibration_min_points_ = projector_calibration_min_points;
}

bool ConfigurationContainer::GetProjectorCalibrationCenterBeamUseWindowCenter() const
{
	return projector_calibration_center_beam_use_window_center_;
}

void ConfigurationContainer::SetProjectorCalibrationCenterBeamUseWindowCenter(const bool projector_calibration_center_beam_use_window_center)
{
	projector_calibration_center_beam_use_window_center_ = projector_calibration_center_beam_use_window_center;
}

float ConfigurationContainer::GetProjectorCalibrationIRExposureAt1m() const
{
	return projector_calibration_ir_exposure_at_1m_;
}

void ConfigurationContainer::SetProjectorCalibrationIRExposureAt1m(const float projector_calibration_ir_exposure_at_1m)
{
	projector_calibration_ir_exposure_at_1m_ = projector_calibration_ir_exposure_at_1m;
}

bool ConfigurationContainer::GetUseDMVSProjectorCalibration() const
{
	return use_dmvs_projector_calibration_;
}

void ConfigurationContainer::SetUseDMVSProjectorCalibration(const bool use_dmvs_projector_calibration)
{
	use_dmvs_projector_calibration_ = use_dmvs_projector_calibration;
}

double ConfigurationContainer::GetFreestyleMinDistance() const
{
	return freestyle_min_distance_;
}

void ConfigurationContainer::SetFreestyleMinDistance(const double freestyle_min_distance)
{
	freestyle_min_distance_ = freestyle_min_distance;
}

float ConfigurationContainer::GetProjectorCalMinBeamLength() const
{
	return min_beam_length_;
}

void ConfigurationContainer::SetProjectorCalMinBeamLength(const float min_beam_length)
{
	min_beam_length_ = min_beam_length;
}

bool ConfigurationContainer::GetForceBeam() const
{
	return if_force_beam_;
}

void ConfigurationContainer::SetForceBeam(const bool if_force_beam)
{
	if_force_beam_ = if_force_beam;
}

float ConfigurationContainer::GetForceBeamX() const
{
	return force_beam_x_;
}

void ConfigurationContainer::SetForceBeamX(const float force_beam_x)
{
	force_beam_x_ = force_beam_x;
}

float ConfigurationContainer::GetForceBeamY() const
{
	return force_beam_y_;
}

void ConfigurationContainer::SetForceBeamY(const float force_beam_y)
{
	force_beam_y_ = force_beam_y;
}

int ConfigurationContainer::GetMinProjectorCalibrationPointsToFinish() const
{
	return min_projector_calibration_points_;
}

void ConfigurationContainer::SetMinProjectorCalibrationPointsToFinish(const int min_projector_calibration_points = 6000)
{
	min_projector_calibration_points_ = min_projector_calibration_points;
}

float ConfigurationContainer::GetMaxDistSymmetryTest() const
{
	return max_distance_symmetry_test_;
}

void ConfigurationContainer::SetMaxDistSymmetryTest(const float max_distance_symmetry_test)
{
	max_distance_symmetry_test_ = max_distance_symmetry_test;
}

bool ConfigurationContainer::GetShowSymmetryProjection() const
{
	return show_symmetry_projection_;
}

void ConfigurationContainer::SetShowSymmetryProjection(const bool show_symmetry_projection)
{
	show_symmetry_projection_ = show_symmetry_projection;
}
