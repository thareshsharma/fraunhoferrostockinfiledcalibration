#include "model_in_format.h"
#include "data_in_format.h"
#include "thread_local_storage.h"
#include "thread_data_type.h"

ModelInFormat::ModelInFormat(std::istream* stream) :
	input_(*stream),
	keyword_index_(0),
	format_(FORMAT_XML_V2)
{
}

ModelInFormat::~ModelInFormat() = default;

ModelFormat::Format ModelInFormat::DetermineFormat()
{
	auto fm = FORMAT_FARO;
	input_ >> std::ws;
	const char c = input_.peek();

	// we know that we are not reading XML v.1 since it's only used in a few import methods where the fromXML() method of XMLSupport is called
	if(c == '<')
	{
		std::string line1;
		getline(input_, line1);
		const auto line1_string(line1);

		if(line1_string == XML_HEADER)
		{
			// The XML file looks like this:
			// <?xml version="1.0" encoding="UTF-8"?>
			// <!-- XML v. 2 -->
			// <!-- '/** iQscene: Main Model **/' -->
			// ...
			std::string line2;
			getline(input_, line2);
			const auto line2_string(line2);

			if(line2_string == XML_V2_COMMENT)
			{
				fm = FORMAT_XML_V2;
			}
			//else
			//{
			// an XML file which starts like this:
			//
			// <?xml version="1.0" encoding="utf-8"?>
			// <Root version="4.8.1001.0" format_="0">
			// ...
			// we could set
			// fm = ModelFormat::FormatXMLv1;
			// but we have already read the root element and the rest will fail because of this

			// \TODO OK Try to rescue this unfortunate situation
			//}
		}
		//else
		//{
		// a file which starts like this:
		//
		// <Keyword> {
		// Bla Bla Bla
		// }
		//
		// we should go back with seekg(0) which does not work at the moment

		// \TODO OK Try to rescue this unfortunate situation
		//}

		// input_.seekg(0, std::ios::beg); // <-- Does not work??? ( input_.seekg(0); does also not work )
	}
	// else do nothing

	SetFormat(fm);

	return fm;
}

std::string ModelInFormat::GetKeyword()
{
	if(keyword_index_ > 0)
	{
		keyword_       = keyword_.substr(keyword_index_);
		keyword_index_ = 0;
	}
	else if(keyword_index_ < 0)
		NextKeyword();

	return keyword_;
}

wchar_t ModelInFormat::CharToWchar_t(const char& c) const
{
	return std::use_facet<std::ctype<char>>(std::locale()).widen(c);
}

void ModelInFormat::NextKeyword()
{
	// check if we got a propagate keyword_:
	ThreadLocalStorage* tls = ThreadLocalStorage::The();
	ThreadDataType* data    = tls ? tls->GetData() : nullptr;

	if(data)
	{
		std::string& propagate_keyword = data->GetModelInFormatPropagateKeyword();

		if(propagate_keyword.empty() == false)
		{
			keyword_          = propagate_keyword;
			propagate_keyword = "";
			keyword_index_    = 0;
			return;
		}
	}

	input_ >> std::ws;

	if(format_ == ModelFormat::FORMAT_XML_V2)
	{
		NextKeywordFromXMLV2(); // call an assistant method since this method would become too long otherwise
	}
	else // read from structured storage format (no need for an extra method at the moment)
	{
		// read next token
		const auto c = input_.peek();

		if(c == START_OF_BLOCK || c == END_OF_BLOCK) // is a block identifier
		{
			const auto c0 = static_cast<char>(input_.get());

			// c0 = '{' or '}'
			assert(c0 == c);
			const char buff[] = {c0, '\0'};
			keyword_          = buff;
		}
		else // regular keyword_
		{
			input_ >> keyword_;
		}
	}

	// in both cases:
	if(input_.good())
	{
		keyword_index_ = 0;
	}
	else
	{
		keyword_index_ = -1;
		keyword_       = '0';
	}
}

bool ModelInFormat::SkipRestOfLine()
{
	return false;
}

bool ModelInFormat::SkipToEndOfBlock()
{
	return false;
}

void ModelInFormat::CleanPropagateKeyword()
{
}

void ModelInFormat::SetPropagateKeyword(const std::string& in)
{
}

bool ModelInFormat::IsEndOfBlock()
{
	/*const std::string& key = getKeyword();
	return key.characterCount() == 1 && key.at(0) == END_OF_BLOCK;*/
	return false;
}

int ModelInFormat::RawRead(const bool no_peek)
{
	return 0;
}

void ModelInFormat::ReadHeader(std::string& header) const
{
	DataInFormat<std::string> fs(&input_, format_);

	if(format_ == FORMAT_XML_V2)
	{
		// input_ looks like this:
		// seekg in determineFormat() does not work, so no longer in stream: // <?xml version="1.0" encoding="UTF-8"?>
		// seekg in determineFormat() does not work, so no longer in stream: // <!-- XML v. 2 -->
		// <!-- '/** iQscene: Main Model **/' -->
		// <!-- NEXT: Version Information -->
		// <!-- 'iQLib' 4.8.1001.0 -->
		// <FARO>
		//
		// or like this:
		//
		// seekg in determineFormat() does not work, so no longer in stream: // <?xml version="1.0" encoding="UTF-8"?>
		// seekg in determineFormat() does not work, so no longer in stream: // <!-- XML v. 2 -->
		// <!-- '/** FARO Scene: Shared Instances **/' -->
		// <!-- NEXT: XML Root Element -->
		// <FARO>
		// seekg in determineFormat() does not work, so no longer in stream: input_.ignore(INT_MAX, '\n'); // skip <?xml version="1.0" encoding="UTF-8"?>
		// seekg in determineFormat() does not work, so no longer in stream: input_.ignore(INT_MAX, '\n'); // skip <!-- XML v. 2 -->
		input_ >> std::ws;
		input_.ignore(4);             // skip <!--
		header = fs.Get();            // set the header
		input_.ignore(INT_MAX, '\n'); // skip the rest of the line

		// it is unknown if the next line contains the version information or the XML root element which needs to be skipped now since readVersion() is not called afterwards:
		std::string line;
		std::getline(input_, line);
		if(line == "<!-- NEXT: XML Root Element -->")
		{
			input_.ignore(INT_MAX, '\n'); // skip <FARO>
		}
		// otherwise readVersion() is called next and skips the line with the XML root element
	}
	else
	{
		// input_ looks like this:
		// "/** FARO Scene: Scan Data Repository Reference **/"
		header = fs.Get();
	}
}

void ModelInFormat::ReadVersion(Version& version) const
{
	DataInFormat<Version> fv(&input_, format_);
	version = fv.Get();
}

void ModelInFormat::ReadRootModelVersion(Version& version)
{
}

bool ModelInFormat::ReadFaroStart()
{
	return false;
}

bool ModelInFormat::ReadFaroEnd()
{
	return false;
}

void ModelInFormat::DecodeXMLEntities(std::string& to_decode)
{
}

void ModelInFormat::DecodeEscapedXMLEntities(std::string& to_decode)
{
}

ModelFormat::Format ModelInFormat::GetFormat() const
{
	return FORMAT_FARO;
}

void ModelInFormat::SetFormat(const Format format)
{
	format_ = format;
}

void ModelInFormat::ReadBeforeBuf()
{
}

void ModelInFormat::NextKeywordFromXMLV2()
{
	// One has to be very careful if a return; should be added anywhere
	char c;
	auto keyword_set = false; // needed to know if the keyword_ was already set at an earlier point of the following code
	keyword_         = "";    // make sure the keyword_ is empty before adding characters (not always true!)

	input_.get(c);

	if(c == '<') // e.g. at [<]Child> or at [<]/Child>
	{
		std::string key_main;
		input_.get(c);

		// we have reached the end of a block || FOR EXAMPLE <[/]Child>
		if(c == '/')
		{
			// we have to test if it's the closing tag of the XML root element: </FARO>
			// read e.g. "</FARO>" into typeString:
			std::string close_string;
			input_.get(c);

			while(c != '>' && input_.good())
			{
				close_string += CharToWchar_t(c);
				input_.get(c);
			}

			//dynamic_assert_msg(input_.good(), "While reading the file, an error was encountered when trying to read the [>] of the closing tag.");

			if(close_string == "FARO")
			{
				// reset the propagateKeyword so that the reading of </FARO> does not interfere with reading the next file:
				ThreadLocalStorage* tls = ThreadLocalStorage::The();
				ThreadDataType* data    = tls ? tls->GetData() : nullptr;

				if(data)
				{
					auto& propagate_keyword = data->GetModelInFormatPropagateKeyword();
					propagate_keyword       = '0';
				}

				keyword_index_ = -1;
				keyword_       = '0';
				return;
			}

			keyword_    = "}";
			keyword_set = true;
		}

		if(keyword_set == false) // e.g. it was a 'C': <[C]hild>
		{
			key_main += CharToWchar_t(c);
			char next = input_.peek();

			// read until the next special char we have to look at:
			while(next != ' ' && next != '>' && input_.good()) // while next ist not e.g. <Attr[ ]type="Vec3dd"> or <Child[>]
			{
				input_.get(c);
				key_main += CharToWchar_t(c);
				next = input_.peek();
			}

			//dynamic_assert_msg(input_.good(), "While reading the file, an error was encountered when trying to read a [white space] or a [>] after the tag name.");
			// next is now e.g. <Attr[ ]type="Vec3dd"> or <Child[>], in this cases c is <Att[r] type="std::string"> or <Chil[d]>
			// input_ is now at e.g. <Attr| type="Vec3dd"> or <Child|>
			// we are not allowed to call input_.get(c) here since the > has to remain in the stream

			if(next == ' ') // an attribute follows
			{
				// read until the start of the content of the attribute:
				input_ >> std::ws;

				// read "attribute=" into attrString, with attribute being "type", for example
				std::string attr_string;

				input_.get(c);

				while(c != '\"' && input_.good())
				{
					if(c != ' ') // ignore these white spaces: type__=_"
						attr_string += c;

					input_.get(c);
				}
				//dynamic_assert_msg(input_.good(), "While reading the file, an error was encountered when trying to read the [\"] after the attribute name.");
				// attrString is now e.g. type=
				// input_ is now at e.g. <Attr type="|Vec3dd">

				// at the moment there are only type and value attributes, and they do not appear together which makes everything easier:
				if(attr_string == "type=")
				{
					// read e.g. "Vec3dd" into typeString:
					std::string typeString;
					input_.get(c);
					while(c != '\"' && input_.good())
					{
						typeString += c;
						input_.get(c);
					}
					//dynamic_assert_msg(input_.good(), "While reading the file, an error was
					// encountered when trying to read the closing [\"] of the type attribute.");
					// SET KEYWORD || FOR EXAMPLE: Attr<Vec3dd>
					keyword_ += key_main + "<" + typeString + ">";

					// input_ is now at e.g. <Attr type="Vec3dd"|>
				}
				else if(attr_string == "value=")
				{
					keyword_ += key_main; // SET KEYWORD || FOR EXAMPLE: Name
					// input_ is now at e.g. <Name value="|'Transformation'" />
				}
			}
			else if(next == '>')
			{
				keyword_ += key_main; // SET KEYWORD || FOR EXAMPLE: Child
				// input_ is now at e.g. <Child|>
				// let the closing > stay so that the next call of nextKeyword returns the start of the block
			}
		}
	}
	else if(c == '>') // start of a block, e.g. <Child[>]
	{
		keyword_ = "{";
	}
	else if(c == '\"')
		// nextKeyword was called after a DataInFormat read the content of the "value" attribute of a value element || FOR EXAMPLE, c is <Value value="2000["] />
	{
		// precondition: the value attribute is always the last attribute in the attribute list
		input_ >> std::ws;
		input_.get(c);

		if(c == '/') // c is e.g. <Value value="2000" [/]>
		{
			input_.get(); // discard >

			// look at the next element for the keyword_:
			NextKeyword();
		}
		else if(c == '>') // c is e.g. <HorCalibData value="10"[>]
		{
			keyword_ = "{";
		}
	}
}
