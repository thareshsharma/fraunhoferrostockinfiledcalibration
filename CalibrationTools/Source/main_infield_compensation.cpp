#include "constants.h"
#include "observation.h"
#include "blob_detector.h"
#include "helper.h"
#include "dmvs.h"
#include "enum_calibration_exit_code.h"
#include "calibration_file_base.h"
#include "calibration_cache.h"
#include "calibration_calculator.h"
#include "configuration_container.h"
#include "kinematics_helpers.h"
#include "enum_camera_model.h"
#include "enum_image_channel_type.h"
#include "laser_dot_detector_2d.h"
#include "reconstructor_laser_dots_3d.h"
#include "projector_calibration_helper.h"
#include "projector.h"
#include "board_detector_loader.h"
#include "kdtree.h"
#include "work_flows.h"
#include "calibration_xml_parser.h"
#include "projector_calibration_fit.h"
#include "application_settings.h"
#include "image_class.h"
#include "dmvs_sdk.h"
#include "image_recorder_helper.h"
#include "marker_data.h"
#include "transformation.h"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include "calibration_json_writer.h"
#include "linear_table_frame_position.h"
#include "camera_sensor_depth_dependent.h"

using namespace dmvs_sdk::data;
using namespace DMVS::CameraCalibration;
using namespace ImageRecorder;
using namespace boost::log::trivial;
using namespace boost::accumulators;

DMVSDevice* dmvs_device     = nullptr;
size_t global_image_counter = 0;
boost::log::sources::severity_logger<severity_level> lg;

static void ImageReceived2D(unsigned char* image,
                            const long rows,
                            const long cols,
                            AdditionalImageInformation2D meta_data,
                            void* context)
{
	if(image)
	{
		ImageClass* this_image = static_cast<ImageClass*>(context);
		global_image_counter++;
		this_image->AddImage(image, rows, cols, global_image_counter);
	}
}

int ConnectDMVS(ImageClass* context,
                ApplicationSettings& settings)
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : FARO DMVS SDK Version [" << GetSDKVersion() << "]";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Detecting Devices...";

	// Creates a list of DMVSDeviceInfo structs.
	// The DMVSDeviceList object is later used to create instances of DMVSDevice* with the create
	// function.
	DMVSDeviceList* const devices = DetectAllDMVSDevices();

	if(devices->Size() == 0)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No DMVS Device detected.";
		system("pause");
		return -1;
	}

	// Iterate over device infos
	for(auto i = 0; i < devices->Size(); i++)
	{
		// Get a pointer to the DMVSDeviceInfo. This pointer should never be deleted.
		auto device_info = devices->At(i);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Device No. " << i << " DMVS:\t\t" << device_info->serial;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Device No. " << i << " IP:\t\t" << device_info->ip;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Device No. " << i << " Firmware:\t" << device_info->pc_version;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Device No. " << i << " DMVS Version:\t " << device_info->head_version;
	}

	// default: take first best DMVS device
	// Get chosen DMVS
	DMVSDeviceInfo* dmvs_device_info = devices->At(0);

	// Create a Instance with the DMVSDeviceInfo.
	// DMVSDevice* is now under your jurisdiction (you are responsible to delete the object)
	// DMVSDevice* dmvsDevice = devices->create_device_from_info(dmvsDeviceInfo);
	dmvs_device = devices->CreateDeviceFromSerial(dmvs_device_info->serial);

	if(!dmvs_device)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Device not found.";
		system("pause");
		return -1;
	}

	if(dmvs_device->Connect(true) != error::OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not connect to DMVS Device.";
		system("pause");
		return 1;
	}

	if(dmvs_device)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Connected to DMVS Device with IP Address [" << dmvs_device->GetIpAddress() << "]";
	}

	settings.serial_no    = dmvs_device ? std::string(dmvs_device->GetSerialNumber()) : "DMVS000000000";
	settings.storage_path = "./" + settings.serial_no + "/" + settings.time_buffer;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Images stored at [" << settings.storage_path << "]";
	boost::filesystem::create_directories(settings.storage_path);

	char calibration_filename[60];
	const auto error_code = dmvs_device->DownloadFactoryCalibration(settings.storage_path.c_str(), calibration_filename);

	if(error_code != error::OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Factory Calibration could not be downloaded";
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Downloaded Factory Calibration Filename : " << calibration_filename;
	settings.calibration_files_folder = settings.storage_path + "\\" + calibration_filename;

	dmvs_device->StopImageAcquisition();
	SaveSettings(dmvs_device, settings.storage_path);

	if(!boost::filesystem::exists(settings.path_to_setting_file))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not find setting file " << settings.path_to_setting_file;
	}

	dmvs_device->LoadParameters(settings.path_to_setting_file.c_str());
	dmvs_device->SetMode(Mode2D);

	double current_frame_rate;
	dmvs_device->GetFramerate(current_frame_rate);

	if(1 / current_frame_rate > settings.doublet_tolerance)
	{
		settings.doublet_tolerance = 2 / current_frame_rate;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Increased doublet tolerance to  " << settings.doublet_tolerance;
	}

	dmvs_device->Register2DImageCallBack(ImageReceived2D, context);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Starting in 3s... ";
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Starting in 2s... ";
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Starting in 1s... ";
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Recording Started ";

	if(dmvs_device->StartImageAcquisition() != error::OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not start image acquisition";
		dmvs_device->Disconnect();
		return -1;
	}

	//wait some time to be sure everything is set
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	return 0;
}

int DisconnectDMVS(ApplicationSettings& settings)
{
	if(dmvs_device)
	{
		dmvs_device->StopImageAcquisition();

		if(!dmvs_device->IsConnected())
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Reconnecting to Device...";
			dmvs_device->Connect();
		}

		LoadSettings(dmvs_device, settings.storage_path);

		dmvs_device->Disconnect();
	}
	return 0;
}

void ProcessLoop(ImageClass* raw_images,
                 const bool& record,
                 ApplicationSettings& settings)
{
	float last_image_at_time = 0;

	while(record || raw_images->GetSize() > 0)
	{
		if(raw_images->GetSize() > 0)
		{
			const auto count    = raw_images->ReturnCounter();
			const auto led_time = raw_images->ReturnLastDuration();
			auto pre_suffix     = settings.storage_path + "/Image_" + std::to_string(count) + "_";

			if(settings.store_every_image)
			{
				const auto is_laser_image = raw_images->IsLaserImage(settings.laser_dot_threshold);
				auto current_image        = raw_images->GrabImage();
				cv::imwrite(pre_suffix + std::to_string(is_laser_image) + Constants::FILE_PNG, current_image);
				//continue;
			}

			if(led_time - last_image_at_time >= settings.image_interval)
			{
				const auto is_laser_image = raw_images->IsLaserImage(settings.laser_dot_threshold);

				if(!is_laser_image &&
					raw_images->ReturnLastSaturation() <= settings.max_led_saturation &&
					raw_images->ReturnLastBrightFraction() >= settings.min_led_fraction)
				{
					//store current image
					auto led_image  = raw_images->GrabImage();
					auto loop_count = 0;
					while(raw_images->GetSize() == 0 && loop_count < 200)
					{
						std::this_thread::sleep_for(std::chrono::milliseconds(20));
						loop_count++;
					}

					//check next image
					const auto is_next_image_laser_image = raw_images->IsLaserImage(settings.laser_dot_threshold);

					if(is_next_image_laser_image && raw_images->ReturnLastDuration() - led_time < settings.doublet_tolerance)
					{
						cv::imwrite(pre_suffix + std::to_string(is_laser_image) + Constants::FILE_PNG, led_image);

						//reset for next image doublet
						last_image_at_time = led_time;

						auto laser_image = raw_images->GrabImage();
						cv::imwrite(pre_suffix + std::to_string(is_next_image_laser_image) + Constants::FILE_PNG, laser_image);
						settings.valid_image_count++;
					}
					else
					{
						//image after valid LED image is invalid-->remove from pipeline
						/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : 2nd image failed : " <<
							raw_images->ReturnLastDuration() - led_time << "    " << is_next_image_laser_image;*/

						raw_images->PopImage();
					}
				}
				else
				{
					//auto led_image = raw_images->GrabImage();
					//current image is not a valid LED image --> remove from pipeline
					//cv::imwrite(pre_suffix + std::to_string(raw_images->ReturnLastSaturation() * 1000) + "_"+std::to_string(raw_images->ReturnLastBrightFraction() * 1000) + Constants::FILE_PNG, led_image);
					raw_images->PopImage();
				}
			}
			else
			{
				raw_images->PopImage();
			}
		}
		else
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
	}
}

void InitializeLogging(const std::string& log_file)
{
	boost::log::add_file_log(log_file);

	boost::log::core::get()->set_filter(
		severity >= info
	);
}

bool GetCameraIds(const std::vector<std::shared_ptr<Observation>>& observations,
                  std::set<size_t>& camera_ids)
{
	for(const auto& observation : observations)
	{
		camera_ids.insert(observation->GetImageChannel());
	}

	return !camera_ids.empty();
}

bool GetDetectedBoard(std::vector<std::shared_ptr<Observation>> observations,
                      const std::vector<std::shared_ptr<BoardDetector>>& boards,
                      std::vector<std::shared_ptr<BoardDetector>>& detected_boards)

{
	std::set<size_t> detected_boards_set;
	for(const auto& observation : observations)
	{
		if(observation->GetBoardId() != Constants::DEFAULT_SIZE_T_VALUE)
		{
			detected_boards_set.insert(observation->GetBoardId());
		}
	}

	for(const auto& board : boards)
	{
		if(detected_boards_set.find(board->GetBoardId()) != detected_boards_set.end())
		{
			detected_boards.push_back(board);
		}
	}

	return !detected_boards.empty();
}

std::shared_ptr<const CameraSensor> InitializeSensor(const std::string& calibration_file,
                                                     const std::string& name,
                                                     const ImageChannel channel,
                                                     const ApplicationSettings& settings,
                                                     cv::InputArray transformation = cv::noArray())
{
	CalibrationXMLParser calibration_file_parser(calibration_file);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << calibration_file;
	cv::Mat distortion_vector, camera_matrix, translation_vector, rotation_matrix;
	calibration_file_parser.GetCameraMatrix(channel, camera_matrix);
	calibration_file_parser.GetDistortionVector(channel, distortion_vector);
	calibration_file_parser.GetRotationMatrix(channel, rotation_matrix);
	calibration_file_parser.GetTranslationVector(channel, translation_vector);
	cv::Vec2d depth_params;
	calibration_file_parser.GetDepthDependentScalarParameters(channel, depth_params);

	cv::Vec3d depth_params_exp;
	calibration_file_parser.GetDepthDependentExponentialParameters(channel, depth_params_exp);

	if(transformation.getMat().data)
	{
		auto transformation_mat = transformation.getMat();
		assert(transformation_mat.rows == 4 && transformation_mat.cols == 4);

		cv::Mat transformation_current = cv::Mat::zeros(4, 4, CV_64FC1);
		const auto rows                = rotation_matrix.rows;
		const auto cols                = rotation_matrix.cols;

		for(auto row = 0; row < rows; ++row)
		{
			for(auto col = 0; col < cols; ++col)
			{
				transformation_current.at<double>(row, col) = rotation_matrix.at<double>(row, col);
			}
		}

		transformation_current.at<double>(0, 3) = translation_vector.at<double>(0, 0);
		transformation_current.at<double>(1, 3) = translation_vector.at<double>(1, 0);
		transformation_current.at<double>(2, 3) = translation_vector.at<double>(2, 0);
		transformation_current.at<double>(3, 3) = 1;

		cv::Mat final_transformation = transformation_mat.inv() * transformation_current;

		for(auto row = 0; row < 3; ++row)
		{
			for(auto col = 0; col < 3; ++col)
			{
				rotation_matrix.at<double>(row, col) = final_transformation.at<double>(row, col);
			}
		}

		translation_vector.at<double>(0, 0) = final_transformation.at<double>(0, 3);
		translation_vector.at<double>(1, 0) = final_transformation.at<double>(1, 3);
		translation_vector.at<double>(2, 0) = final_transformation.at<double>(2, 3);
	}

	return DMVSSensor::GetDMVSCameraCameraCalibration(name,
	                                                  channel,
	                                                  DEPTH_DEPENDENT_MODEL,
	                                                  depth_params,
	                                                  depth_params_exp,
	                                                  settings,
	                                                  camera_matrix,
	                                                  distortion_vector,
	                                                  rotation_matrix,
	                                                  translation_vector);
}


void InitializeCameraSensorMap(std::map<size_t, std::shared_ptr<const CameraSensor>>& camera_sensor_map,
                               const ApplicationSettings& settings)
{
	camera_sensor_map[CHANNEL_GREY0] = InitializeSensor(settings.calibration_files_folder, "left", CHANNEL_GREY0, settings);
	//cv::Mat left_camera_transformation;
	//camera_sensor_map[CHANNEL_GREY0]->GetTransformationMatrix(left_camera_transformation);
	camera_sensor_map[CHANNEL_GREY1]      = InitializeSensor(settings.calibration_files_folder, "right", CHANNEL_GREY1, settings);
	camera_sensor_map[CHANNEL_PROJECTOR0] = InitializeSensor(settings.calibration_files_folder, "projector", CHANNEL_PROJECTOR0, settings);

	//// left_camera_transformation is used to bring right camera and project to left camera co-ordinate system
	//// also we transform left camera with left camera transformation so that we have
	//// identity rotation matrix and translation vector of [0,0,0]
	//camera_sensor_map[CHANNEL_GREY1]      = InitializeSensor(calibration_file, "right", 21, left_camera_transformation);
	//camera_sensor_map[CHANNEL_PROJECTOR0] = InitializeSensor(calibration_file, "projector", 31, left_camera_transformation);
	//camera_sensor_map[CHANNEL_GREY0]      = InitializeSensor(calibration_file, "left", 20, left_camera_transformation);
}

std::shared_ptr<const CameraSensor> InitializeSensorInHouse(const std::string& name,
                                                            const ImageChannel channel,
                                                            const ApplicationSettings& settings,
                                                            cv::InputArray transformation = cv::noArray())

{
	//cv::Mat distortion_vector, camera_matrix, translation_vector, rotation_matrix;

	cv::Mat camera_matrix          = cv::Mat::eye(3, 3, CV_64F);
	camera_matrix.at<double>(0, 0) = 1141.0;
	camera_matrix.at<double>(1, 1) = 1141.0;
	camera_matrix.at<double>(0, 2) = 640.0;
	camera_matrix.at<double>(1, 2) = 512.0;

	cv::Mat distortion_vector          = cv::Mat::zeros(5, 1, CV_64F);
	distortion_vector.at<double>(0, 0) = -0.10569587;
	distortion_vector.at<double>(1, 0) = 0.14983749;
	distortion_vector.at<double>(2, 0) = 0.00015819847;
	distortion_vector.at<double>(3, 0) = 0.001486752;
	distortion_vector.at<double>(4, 0) = -0.046035613;

	const cv::Vec2d depth_params(settings.depth_dependent_scalar1, settings.depth_dependent_scalar2);
	const cv::Vec3d depth_params_exp(settings.depth_dependent_exp1, settings.depth_dependent_exp2, settings.depth_dependent_exp3);

	cv::Mat rotation_matrix    = cv::Mat::eye(3, 3, CV_64F);
	cv::Mat translation_vector = cv::Mat::zeros(3, 1, CV_64F);

	if(channel == CHANNEL_GREY1)
	{
		rotation_matrix = (cv::Mat_<double>(3, 3) << 0.84667758, -0.013145687, -0.53194386,
			0.0053022114, 0.99985358, -0.016269571,
			0.53207985, 0.010954602, 0.84662331);

		translation_vector = (cv::Mat_<double>(3, 1) << 0.20346663, 0.0085657875, 0.058308118);
	}

	return DMVSSensor::GetDMVSCameraCameraCalibration(name,
	                                                  channel,
	                                                  DEPTH_DEPENDENT_MODEL,
	                                                  depth_params,
	                                                  depth_params_exp,
	                                                  settings,
	                                                  camera_matrix,
	                                                  distortion_vector,
	                                                  rotation_matrix,
	                                                  translation_vector);
}

void InitializeCameraSensorMapInHouse(std::map<size_t, std::shared_ptr<const CameraSensor>>& camera_sensor_map,
                                      const ApplicationSettings& settings)
{
	camera_sensor_map[CHANNEL_GREY0]      = InitializeSensorInHouse("left", CHANNEL_GREY0, settings);
	camera_sensor_map[CHANNEL_GREY1]      = InitializeSensorInHouse("right", CHANNEL_GREY1, settings);
	camera_sensor_map[CHANNEL_PROJECTOR0] = InitializeSensorInHouse("projector", CHANNEL_PROJECTOR0, settings);
}

void ProcessRecordedImages(DMVSSensor dmvs,
                           std::shared_ptr<CalibrationCache> calibration_cache,
                           std::vector<std::shared_ptr<BoardDetector>>& boards,
                           BlobDetector blob_detector,
                           Frame frame_images,
                           int& recorded_position_count,
                           const std::vector<cv::Mat>& recorded_images,
                           std::vector<std::shared_ptr<Observation>>& observations,
                           ApplicationSettings settings)
{
	// Add empty images for grey0, grey1 channel
	ChannelImageMap channel_image_map, channel_image_map_copy;
	channel_image_map.channel_image_map[CHANNEL_GREY0] = cv::Mat();
	channel_image_map.channel_image_map[CHANNEL_GREY1] = cv::Mat();

	for(auto& recorded_image : recorded_images)
	{
		// Split the image in two parts
		channel_image_map.channel_image_map[CHANNEL_GREY0] =
			recorded_image(cv::Rect(0, 0, Constants::DMVS_IMAGE_WIDTH, Constants::DMVS_IMAGE_HEIGHT));
		channel_image_map.channel_image_map[CHANNEL_GREY1] = recorded_image(
			cv::Rect(Constants::DMVS_IMAGE_WIDTH, 0, Constants::DMVS_IMAGE_WIDTH, Constants::DMVS_IMAGE_HEIGHT));

		CopyChannelImageMap(channel_image_map, channel_image_map_copy);
		Helper::CopyImages(channel_image_map_copy, frame_images, recorded_position_count);
		ChannelImageVectorMap channel_image_vector_map, image_series;
		GrabImageSeries2(dmvs, channel_image_map, image_series);
		AverageImageSeries(channel_image_map, channel_image_vector_map, recorded_position_count, 1, image_series, settings);

		ImageChannelCircularMarkerVectorMap circular_markers_in_current_frame, markers_combined;
		std::vector<ImageChannelCircularMarkerVectorMap> found_circular_markers_map;

		Helper::DetectMarkers(channel_image_map, blob_detector, circular_markers_in_current_frame);
		Helper::RemoveLowQualityMarker(circular_markers_in_current_frame, channel_image_map);
		found_circular_markers_map.push_back(circular_markers_in_current_frame);

		PushImagesToCalibrationCache(calibration_cache, channel_image_map, recorded_position_count);

		std::map<ImageChannel, std::vector<CircularMarkerPair>> unsaturated_markers_map;
		RemoveSaturatedMarkers(found_circular_markers_map, unsaturated_markers_map);
		DrawMarkers(channel_image_vector_map, unsaturated_markers_map);
		CombineMarkers(unsaturated_markers_map, markers_combined);
		auto coded_marker_count = Helper::AddCodedMarkers(boards, markers_combined, recorded_position_count, observations, channel_image_map_copy);

		if(coded_marker_count < 3)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not enough coded markers found, trying again. Found only [" << coded_marker_count <<
				"] coded markers";
			continue;
		}

		CreateAndShowCollage(channel_image_map_copy, recorded_position_count);
		recorded_position_count++;
	}
}

void GetPosition(const DMVS::CameraCalibration::Transformation& transformation,
                 Eigen::Vector3d& position)
{
	const auto translational_vector = transformation.GetTranslationalTransformation();
	cv::cv2eigen(translational_vector, position);
}

void GetNormal(const DMVS::CameraCalibration::Transformation& transformation,
               Eigen::Vector3d& normal)
{
	const auto rotation_matrix = transformation.GetRotationalTransformation();
	//cv::Rodrigues(rotation_matrix, rotation_vector);
	const cv::Mat rotation_vector = (cv::Mat_<double>(3, 1) << rotation_matrix.at<double>(0, 2), rotation_matrix.at<double>(1, 2), rotation_matrix.at<
		double>(2, 2));

	cv::cv2eigen(rotation_vector, normal);
}

void GetSecondaryBoardMask(cv::Mat& src,
                           std::vector<cv::Point2d> vertices,
                           cv::Mat& destination)
{
	// create a mask
	cv::Mat mask(src.rows, src.cols, CV_8UC1, cv::Scalar(125));
	// Copy the input image to temp
	cv::Mat temp, output;
	src.copyTo(temp);

	// Create contour on mask image
	std::vector<std::vector<cv::Point>> co_ordinates;
	co_ordinates.emplace_back();
	co_ordinates[0].push_back(vertices[0]);
	co_ordinates[0].push_back(vertices[1]);
	co_ordinates[0].push_back(vertices[2]);
	co_ordinates[0].push_back(vertices[3]);
	cv::drawContours(mask, co_ordinates, 0, cv::Scalar(255), CV_FILLED, 8);
	bitwise_not(mask, mask);
	temp.copyTo(output, mask);
	destination = src - output;
}

void GetBrightDotDataVector(const cv::Mat& input,
                            cv::Mat& output)
{
	const auto rows = input.rows;

	for(auto row = 0; row < rows; ++row)
	{
		// Bright dots filter
		if(std::abs(input.at<double>(row, 0) - 1.00) < 0.00001)
		{
			output.push_back(input.row(row));
		}
	}
}

void RecordImage(int ac,
                 char* av[])
{
	ApplicationSettings settings(ac, av);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Image Recording Tool " << Constants::LOG_START;
	ImageClass raw_images(Constants::BRIGHT_PIXEL_THRESHOLD);

	if(ConnectDMVS(&raw_images, settings) != 0)
	{
		return;
	}

	auto record = true;
	cv::Mat show_frame;
	cv::namedWindow("Current Frame", cv::WINDOW_NORMAL);
	std::thread process_thread(ProcessLoop, &raw_images, std::ref(record), std::ref(settings));

	while(record)
	{
		while(raw_images.GetSize() < 1)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(20));
		}

		//Do not display dark images
		if(raw_images.ReturnLastBrightFraction() <= Constants::MIN_LED_FRACTION && raw_images.ReturnLastSaturation() > Constants::MAX_LED_SATURATION)
		{
			continue;
		}

		raw_images.GrabCurrentImage(&show_frame);

		if(show_frame.empty())
		{
			continue;
		}

		auto y_spacing = 60;
		auto y_offset  = 40;
		auto font_size = 1.5;

		//display only images which have "no" laser sots
		auto mean_image_value = cv::mean(show_frame);
		if(mean_image_value[0] < Constants::MIN_LED_BRIGHTNESS) //should be obsolete!!
		{
			continue;
		}

		if(show_frame.channels() < 3)
			cv::cvtColor(show_frame, show_frame, cv::COLOR_GRAY2RGB);

		std::ostringstream frames_recorded_string;
		frames_recorded_string << global_image_counter << "  Frames recorded ";
		cv::putText(show_frame,
		            frames_recorded_string.str(),
		            cv::Point(30, y_offset + 0 * y_spacing),
		            cv::FONT_HERSHEY_TRIPLEX,
		            0.7 * font_size,
		            cv::Scalar(0, 255, 10),
		            2);

		std::ostringstream frames_stored_string;
		frames_stored_string << settings.valid_image_count << "  Frames stored ";
		cv::putText(show_frame,
		            frames_stored_string.str(),
		            cv::Point(30, y_offset + 1 * y_spacing),
		            cv::FONT_HERSHEY_TRIPLEX,
		            1.5 * font_size,
		            cv::Scalar(0, 255, 10),
		            2);
		cv::imshow("Current Frame", show_frame);
		auto c = char(cv::waitKey(10));

		switch(c)
		{
		case 27:
			record = false;
			break; //esc
		}

		if(settings.valid_image_count > Constants::MAX_VALID_IMAGES)
		{
			record = false;
		}
	}

	DisconnectDMVS(settings);
	process_thread.join();
}

bool SearchCorrespondingMarkerId(std::vector<std::shared_ptr<Observation>>& observations,
                                 const size_t frame_id,
                                 const size_t searched_marker_id,
                                 const ImageChannel channel,
                                 std::shared_ptr<Observation>& found_marker_observation)
{
	auto if_marker_found = false;

	for(const auto observation : observations)
	{
		if(observation->GetImageChannel() == channel && observation->GetFrameId() == frame_id && !observation->GetIsProcessed())
		{
			if(observation->GetMarkerId() == searched_marker_id)
			{
				found_marker_observation = observation;
				found_marker_observation->SetIsProcessed(true);
				if_marker_found = true;
				break;
			}
		}
	}

	return if_marker_found;
}

void CreateLists(std::vector<std::tuple<size_t, std::shared_ptr<Observation>, std::shared_ptr<Observation>>> paired_observations,
                 const size_t marker_id,
                 const size_t frame_id,
                 std::vector<cv::Point2d>& channel_grey0_positions,
                 std::vector<cv::Point2d>& channel_grey1_positions)
{
	for(auto paired_observation : paired_observations)
	{
		const auto current_marker_id = std::get<0>(paired_observation);

		if(current_marker_id == marker_id)
		{
			const auto grey0 = std::get<1>(paired_observation);
			const auto grey1 = std::get<2>(paired_observation);

			if(grey0->GetFrameId() == frame_id && !grey0->GetIsProcessed() && grey1->GetFrameId() == frame_id && !grey1->GetIsProcessed())
			{
				channel_grey0_positions.push_back(grey0->GetRawEllipseCenter());
				channel_grey1_positions.push_back(grey1->GetRawEllipseCenter());

				// Set processed to true so it is not included again
				grey0->SetIsProcessed(true);
				grey1->SetIsProcessed(true);
			}
		}
	}

	/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : channel_grey0_positions [" << channel_grey0_positions.size() << "]";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : channel_grey1_positions [" << channel_grey1_positions.size() << "]";*/
}

void WriteToStream(DMVS::CameraCalibration::Transformation& transformation,
                   std::stringstream& string_stream)
{
	for(auto value : transformation.Serialized())
	{
		string_stream << value << " ";
	}
}

void Calculate3DCalibrationError(std::vector<std::shared_ptr<Observation>>& observations,
                                 CaptureCalibration& capture_calibration,
                                 std::map<size_t, DMVS::CameraCalibration::Transformation>& frame_transformation_map,
                                 ApplicationSettings& settings)
{
	// Get all observations
	std::vector<MarkerData> paired_observations;

	// Create paired markers
	for(auto& observation : observations)
	{
		const auto frame_id  = observation->GetFrameId();
		const auto marker_id = observation->GetMarkerId();
		const auto channel   = observation->GetImageChannel();
		std::shared_ptr<Observation> found_marker_observation;

		if(channel != CHANNEL_GREY1 && SearchCorrespondingMarkerId(observations,
		                                                           frame_id,
		                                                           marker_id,
		                                                           CHANNEL_GREY1,
		                                                           found_marker_observation))
		{
			MarkerData marker_pair;
			marker_pair.frame_id          = frame_id;
			marker_pair.marker_id         = marker_id;
			marker_pair.position_2d_grey0 = observation->GetRawEllipseCenter();
			marker_pair.position_2d_grey1 = found_marker_observation->GetRawEllipseCenter();
			marker_pair.is_valid          = false;

			paired_observations.push_back(marker_pair);
		}
	}

	// Get List of markers from observation
	std::vector<size_t> frame_ids;

	for(const auto& paired_observation : paired_observations)
	{
		frame_ids.push_back(paired_observation.frame_id);
	}

	std::sort(frame_ids.begin(), frame_ids.end());
	frame_ids.erase(std::unique(frame_ids.begin(), frame_ids.end()), frame_ids.end());

	std::map<size_t, Dots> positions_channel_grey0_map;
	std::map<size_t, Dots> positions_channel_grey1_map;

	std::ofstream paired_observations_stream;
	auto paired_observations_path = settings.storage_path + R"(\paired_observations.txt)";
	paired_observations_stream.open(paired_observations_path);

	for(auto frame_id : frame_ids)
	{
		Dots channel_grey0_positions;
		Dots channel_grey1_positions;

		for(auto& paired_observation : paired_observations)
		{
			const auto current_frame_id = paired_observation.frame_id;

			if(current_frame_id == frame_id)
			{
				if(!paired_observation.is_valid)
				{
					channel_grey0_positions.emplace_back(paired_observation.position_2d_grey0);
					channel_grey1_positions.emplace_back(paired_observation.position_2d_grey1);
				}
			}
		}

		std::vector<cv::Vec3d> reconstructed_marker_position_3d;
		capture_calibration.GetCalibration(CHANNEL_GREY0)->StereoIntersect(capture_calibration.GetCalibration(CHANNEL_GREY1),
		                                                                   channel_grey0_positions,
		                                                                   channel_grey1_positions,
		                                                                   reconstructed_marker_position_3d,
		                                                                   nullptr,
		                                                                   false);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Frame Id [" << frame_id << "] reconstructed_3d_points [" <<
			reconstructed_marker_position_3d.size() << "]";

		auto index = 0;
		auto size  = frame_transformation_map.size();

		for(auto& paired_observation : paired_observations)
		{
			const auto current_frame_id = paired_observation.frame_id;

			if(current_frame_id == frame_id && !paired_observation.is_valid)
			{
				auto& current_frame_transformation = frame_transformation_map.at(current_frame_id);
				cv::Mat camera_transformation_matrix;
				capture_calibration.GetCalibration(CHANNEL_GREY0)->GetTransformationMatrix(camera_transformation_matrix);
				DMVS::CameraCalibration::Transformation camera_transformation(camera_transformation_matrix);

				//auto final_transformation = current_frame_transformation.GetInverse().Multiply(camera_transformation);
				auto final_transformation = current_frame_transformation.GetInverse();
				auto rotation             = final_transformation.GetRotationalTransformation();
				auto translation          = final_transformation.GetTranslationalTransformation();

				cv::Mat marker_position_3d_mat = rotation * reconstructed_marker_position_3d[index] + translation;
				paired_observation.position_3d = cv::Point3d(marker_position_3d_mat);

				std::stringstream frame_transformation_str, camera_transformation_str, final_transformation_str;
				WriteToStream(current_frame_transformation, frame_transformation_str);
				WriteToStream(camera_transformation, camera_transformation_str);
				WriteToStream(final_transformation, final_transformation_str);

				if(paired_observations_stream.is_open())
				{
					paired_observations_stream << paired_observation.frame_id << " " <<
						paired_observation.marker_id << " " <<
						reconstructed_marker_position_3d[index][0] << " " << reconstructed_marker_position_3d[index][1] << " " <<
						reconstructed_marker_position_3d[index][2] << " " << paired_observation.position_3d.x << " " <<
						paired_observation.position_3d.y << " " << paired_observation.position_3d.z << " " <<
						frame_transformation_str.str() << " " <<
						camera_transformation_str.str() << " " <<
						final_transformation_str.str() << " " << '\n';
				}

				// Set processed to true so it is not included again
				paired_observation.is_valid = true;
				index++;
			}
		}

		positions_channel_grey0_map[frame_id] = channel_grey0_positions;
		positions_channel_grey1_map[frame_id] = channel_grey1_positions;
	}

	/*std::vector<cv::Vec3d> reconstructed_3d_points;*/
	//accumulator_set<cv::Point3d, features<boost::accumulators::tag::mean, boost::accumulators::tag::median>> error3d;
	//error3d(cv::Point3d(0, 0, 0));
	//error3d(cv::Point3d(2, 4, 3));
	//error3d(cv::Point3d(1, 4, 5));
	//error3d(cv::Point3d(3, 7, 9));
	// Separate the list in two list of channels
	// Then go through each frame and fine the markers which exists in each of the channel image
	// Perform stereo intersect and get 3d points
	// Create a list of these unique markers and then add calculated 3d points
	// Something like std::map<MarkerId, std::Vector<cv::Points3d>>

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Processing paired_observations [" << paired_observations.size() << "]";
}

int InfieldCompensation(ApplicationSettings& settings)
{
	//auto record_images = false;

	if(settings.record)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Image Recording Tool " << Constants::LOG_START;
		ImageClass raw_images(Constants::BRIGHT_PIXEL_THRESHOLD);

		if(ConnectDMVS(&raw_images, settings) != 0)
		{
			return CALIBRATION_SENSOR_SETUP_ERROR;
		}

		auto record = true;
		cv::Mat show_frame;
		cv::namedWindow("Current Frame", cv::WINDOW_NORMAL);
		std::thread process_thread(ProcessLoop, &raw_images, std::ref(record), std::ref(settings));

		while(record)
		{
			while(raw_images.GetSize() < 1)
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(20));
			}

			//Do not display dark images
			if(raw_images.ReturnLastBrightFraction() <= settings.min_led_fraction && raw_images.ReturnLastSaturation() >
				settings.max_led_saturation)
			{
				continue;
			}

			raw_images.GrabCurrentImage(&show_frame);

			if(show_frame.empty())
			{
				continue;
			}

			const auto x_offset   = 30;
			const auto y_spacing  = 60;
			const auto y_offset   = 40;
			const auto y_row_1    = y_offset;
			const auto y_row_2    = y_offset + y_spacing;
			const auto font_scale = 2;

			//display only images which have "no" laser sots
			auto mean_image_value = cv::mean(show_frame);
			if(mean_image_value[0] < Constants::MIN_LED_BRIGHTNESS) //should be obsolete!!
			{
				continue;
			}

			if(show_frame.channels() < 3)
				cv::cvtColor(show_frame, show_frame, cv::COLOR_GRAY2RGB);

			std::ostringstream frames_recorded_string;
			frames_recorded_string << global_image_counter << "  Frames recorded ";
			cv::putText(show_frame,
			            frames_recorded_string.str(),
			            cv::Point(x_offset, y_row_1),
			            cv::FONT_HERSHEY_TRIPLEX,
			            font_scale * 0.7,
			            cv::Scalar(0, 255, 10),
			            2);

			std::ostringstream frames_stored_string;
			frames_stored_string << settings.valid_image_count << "  Frames stored ";
			cv::putText(show_frame,
			            frames_stored_string.str(),
			            cv::Point(x_offset, y_row_2),
			            cv::FONT_HERSHEY_TRIPLEX,
			            font_scale,
			            cv::Scalar(0, 255, 10),
			            2);
			cv::imshow("Current Frame", show_frame);
			auto c = char(cv::waitKey(10));

			if(c == 27 || settings.valid_image_count > settings.images_to_record)
			{
				record = false;
			}
		}

		DisconnectDMVS(settings);
		process_thread.join();
		cv::destroyAllWindows();
	}
	else
	{
		//settings.storage_path          = R"(D:\repos\BitBucket\farolabs\infield-compensation\InfieldCompensation\DMVS011900031\20201125-1745)";
		settings.calibration_files_folder = settings.storage_path + settings.calibration_filename
			/*R"(\Factory_FREESTYLE_DMVS011900031_S1184086_S1181822.xml)"*/;
		settings.serial_no = "DMVS011900031";
	}

	auto configuration_container = std::make_shared<ConfigurationContainer>();
	DMVSSensor dmvs;
	const auto file_base = CalibrationFileBase::GetFileBase(settings.serial_no, configuration_container);

	if(!file_base)
	{
		return IOMessages::IO_UNDEFINED;
	}

	std::string file_name;
	file_base->GetCameraCalibrationFSVOutputFile(file_name);
	auto calibration_cache = std::make_shared<CalibrationCache>(configuration_container, settings.serial_no, file_name);

	std::vector<std::shared_ptr<BoardDetector>> boards;
	const BlobDetector::BlobDetectorParams blob_detection_params;
	BlobDetector blob_detector(blob_detection_params);

	if(!BoardDetector::LoadDetectorBoardsInfield(settings.board_files_path, boards))
	{
		return CALIBRATION_BOARD_NOT_LOADED;
	}

	// Collect all marker detections called observations: (frame/board/marker/camera/coordinates)
	// Data structure to keep the images of all channels in all frames
	// e.g. all_images.frame[nFrame].channel[CHANNEL_GREY0] = cv::Mat()
	Frame frame_images;

	// Counter for representing recorded positions
	auto recorded_position_count = 0;
	std::vector<std::shared_ptr<Observation>> observations;
	/*auto log_file = settings.storage_path + "\\InfieldCompensation.log";
	InitializeLogging(log_file);
	boost::log::add_common_attributes();*/

	std::vector<cv::Mat> recorded_led_images, recorded_laser_images;
	LoadRecordedImages(settings.storage_path, recorded_led_images, recorded_laser_images);
	FilterRecordedImages(boards, blob_detector, recorded_led_images, recorded_laser_images);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finding Observations " << Constants::LOG_START;

	// Add empty images for grey0, grey1 channel
	ChannelImageMap channel_image_map, channel_image_map_copy;
	channel_image_map.channel_image_map[CHANNEL_GREY0] = cv::Mat();
	channel_image_map.channel_image_map[CHANNEL_GREY1] = cv::Mat();

	for(auto index = 0; index < recorded_led_images.size(); index++)
	{
		auto current_image = recorded_led_images[index];
		if(current_image.rows != 1024 && current_image.cols != 2 * Constants::DMVS_IMAGE_WIDTH)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Dimension mismatch rows [" << current_image.rows << "] cols [" << current_image.cols <<
 "]";
			return CALIBRATION_UNKNOWN_ERROR;
		}

		// Split the image in two parts
		channel_image_map.channel_image_map[CHANNEL_GREY0] = current_image(cv::Rect(0, 0, Constants::DMVS_IMAGE_WIDTH, Constants::DMVS_IMAGE_HEIGHT));
		channel_image_map.channel_image_map[CHANNEL_GREY1] = current_image(
			cv::Rect(Constants::DMVS_IMAGE_WIDTH, 0, Constants::DMVS_IMAGE_WIDTH, Constants::DMVS_IMAGE_HEIGHT));

		CopyChannelImageMap(channel_image_map, channel_image_map_copy);
		Helper::CopyImages(channel_image_map_copy, frame_images, index);
		ChannelImageVectorMap channel_image_vector_map, image_series;
		GrabImageSeries2(dmvs, channel_image_map, image_series);
		AverageImageSeries(channel_image_map, channel_image_vector_map, index, 1, image_series, settings);

		ImageChannelCircularMarkerVectorMap circular_markers_in_current_frame, markers_combined;
		std::vector<ImageChannelCircularMarkerVectorMap> found_circular_markers_map;

		Helper::DetectMarkers(channel_image_map, blob_detector, circular_markers_in_current_frame);
		Helper::RemoveLowQualityMarker(circular_markers_in_current_frame, channel_image_map);
		found_circular_markers_map.push_back(circular_markers_in_current_frame);

		PushImagesToCalibrationCache(calibration_cache, channel_image_map, index);

		std::map<ImageChannel, std::vector<CircularMarkerPair>> unsaturated_markers_map;
		RemoveSaturatedMarkers(found_circular_markers_map, unsaturated_markers_map);
		DrawMarkers(channel_image_vector_map, unsaturated_markers_map);
		CombineMarkers(unsaturated_markers_map, markers_combined);
		auto coded_marker_count = Helper::AddCodedMarkers(boards, markers_combined, index, observations, channel_image_map_copy);

		if(coded_marker_count < 3)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not enough coded markers found, trying again. Found only [" << coded_marker_count <<
 "] coded markers";
			continue;
		}

		CreateAndShowCollage(channel_image_map_copy, index);
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finding Observations " << Constants::LOG_END;
	cv::destroyAllWindows();

	CalibrationCalculator calibration_calculator(observations);
	calibration_calculator.InitializeCameraSystemTransformation(recorded_led_images.size());
	std::vector<std::shared_ptr<BoardDetector>> detected_boards;

	if(!GetDetectedBoard(observations, boards, detected_boards))
	{
		return CALIBRATION_BOARD_NOT_DETECTED;
	}

	// Set used calibration boards
	std::map<size_t, std::shared_ptr<const CameraSensor>> camera_sensor_map;
	InitializeCameraSensorMap(camera_sensor_map, settings);
	calibration_calculator.SetBoards(detected_boards);
	calibration_calculator.SetRelativeFixedCamera(camera_sensor_map, CHANNEL_GREY0);

	if(calibration_calculator.ProcessObservationsInfield())
	{
		// This has to be done only for offline mode. generally this
		CaptureCalibration capture_calibration;
		calibration_calculator.ReplaceWithNewCalibration(capture_calibration);
		auto relative_fixed_camera_param_optimizer = calibration_calculator.GetRelativeFixedCamerasParameterOptimizer();
		auto relative_board_transformations        = calibration_calculator.GetCalibrationBoardsTransformation();
		auto camera_system_transformations         = calibration_calculator.GetCameraSystemTransformation();
		auto frame_transformation_map              = camera_system_transformations->GetFrameTransformationMap();
		auto board_transformation_map              = relative_board_transformations->GetBoardTransformationMap();

		auto board_count = 0;
		std::map<BoardID, std::vector<cv::Point3d>> board_vertices_map;
		for(const auto& board : boards)
		{
			std::vector<cv::Point3d> board_vertices;
			board->GetProjectPlaneContour(board_vertices);
			board_vertices_map[board_count] = board_vertices;
			board_count++;
		}

		CalculateStatistics(file_base, frame_images, calibration_calculator);

		// Steps for Projector calibration
		// 1. Use Frame position and relative board position form last step
		// 2. Find Laser Dots with the possibility of separating bright and dark dots.

		// Device images (color, IR0, IR1)
		cv::Mat pattern_image_left, pattern_image_right;

		// Channel images for user feedback
		cv::Mat channel_image, projector_image_left, projector_image_right, projector_image;
		std::vector<cv::Point2d> projected_points_all_frames;

		std::ofstream projected_points_2d_stream, calibrated_points_2d_stream, correspondences_0_stream, correspondences_1_stream;

		auto projected_points_2d_path = settings.storage_path + R"(\projected_points_2d.txt)";
		projected_points_2d_stream.open(projected_points_2d_path);

		auto calibrated_points_2d_path = settings.storage_path + R"(\calibrated_points_2d.txt)";
		calibrated_points_2d_stream.open(calibrated_points_2d_path);

		auto correspondences_0_path = settings.storage_path + R"(\correspondences_0.txt)";
		correspondences_0_stream.open(correspondences_0_path);

		auto correspondences_1_path = settings.storage_path + R"(\correspondences_1.txt)";
		correspondences_1_stream.open(correspondences_1_path);

		// read the calibration points from the xml file for comparison
		CalibrationXMLParser calibration_file_parser(settings.calibration_files_folder);
		auto calibrated_laser_dots_mat = calibration_file_parser.GetCustomMatrices();

		// fill these custom matrix to get the only the bright points.
		cv::Mat calibrated_bright_laser_dots_mat;
		GetBrightDotDataVector(calibrated_laser_dots_mat, calibrated_bright_laser_dots_mat);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibrated bright laser dots size " << calibrated_bright_laser_dots_mat.size();

		// Create pointVec with projected intersection points for neighborhood search
		pointVec calibrated_bright_laser_dots_points_vec;

		for(auto row = 0; row < calibrated_bright_laser_dots_mat.rows; ++row)
		{
			calibrated_bright_laser_dots_points_vec.push_back({
				calibrated_bright_laser_dots_mat.at<double>(row, 1),
				calibrated_bright_laser_dots_mat.at<double>(row, 2)
			});
		}

		point_t q2(2);
		auto calibrated_laser_dots_search_radius = 12.0;
		KDTree calibrated_bright_laser_dots_kd_tree(calibrated_bright_laser_dots_points_vec);
		std::map<FrameID, std::vector<cv::Point3d>> correspondences_3d_points_map;
		std::map<FrameID, std::vector<cv::Point2d>> correspondences_calibrated_2d_points_map;
		std::vector<cv::Point3d> correspondences_3d_points_all;
		std::vector<cv::Point2d> correspondences_calibrated_2d_points_all;

		for(auto index = 0; index < recorded_laser_images.size(); index++)
		{
			//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finding Laser Dots" << Constants::LOG_START;
			// Transfer the board coordinates to World coordinates and then camera coordinates
			Eigen::Vector3d plane_position_reference_board, plane_normal_reference_board;
			auto current_frame_transformation = frame_transformation_map.at(index);

			GetPosition(current_frame_transformation, plane_position_reference_board);
			GetNormal(current_frame_transformation, plane_normal_reference_board);
			auto current_frame_translation = current_frame_transformation.GetTranslationalTransformation();
			auto current_frame_rotation    = current_frame_transformation.GetRotationalTransformation();

			std::map<BoardID, std::vector<cv::Point2d>> polygon_vertices_map;
			for(const auto& board_vertices : board_vertices_map)
			{
				std::vector<cv::Point3d> polygon_vertices_3d;
				for(const auto& board_vertex : board_vertices.second)
				{
					if(relative_board_transformations->GetReferenceBoardID() != board_vertices.first)
					{
						auto secondary_board_transformation = current_frame_transformation.Multiply(board_transformation_map[1]);
						current_frame_translation           = secondary_board_transformation.GetTranslationalTransformation();
						current_frame_rotation              = secondary_board_transformation.GetRotationalTransformation();
					}

					cv::Mat board_vertex_mat(board_vertex);
					cv::Mat vertex = current_frame_rotation * board_vertex_mat + current_frame_translation;
					polygon_vertices_3d.emplace_back(vertex.at<double>(0, 0),
					                                 vertex.at<double>(1, 0),
					                                 vertex.at<double>(2, 0));
				}

				std::vector<cv::Point2d> polygon_vertices_2d;
				capture_calibration.GetCalibration(CHANNEL_GREY0)->ProjectPoints(polygon_vertices_3d, polygon_vertices_2d, true);
				polygon_vertices_map[board_vertices.first] = polygon_vertices_2d;
			}

			Eigen::Vector3d plane_position_secondary_board, plane_normal_secondary_board;
			auto secondary_board_transformation = current_frame_transformation.Multiply(board_transformation_map[1]);
			GetPosition(secondary_board_transformation, plane_position_secondary_board);
			GetNormal(secondary_board_transformation, plane_normal_secondary_board);

			pattern_image_left  = recorded_laser_images[index](cv::Rect(0, 0, Constants::DMVS_IMAGE_WIDTH, Constants::DMVS_IMAGE_HEIGHT));
			pattern_image_right = recorded_laser_images[index](cv::Rect(Constants::DMVS_IMAGE_WIDTH,
			                                                            0,
			                                                            Constants::DMVS_IMAGE_WIDTH,
			                                                            Constants::DMVS_IMAGE_HEIGHT));


			// For dot brightness, to separate bright from dark dots
			Dots dots0, dots1, dots0_all, dots1_all;
			DotInfos dots_infos0, dots_infos1;
			LaserDotDetector2D::Params params;
			params.bg_limit = 10;

			LaserDotDetector2D ldd0(pattern_image_left, params);
			LaserDotDetector2D ldd1(pattern_image_right, params);

			ldd0.FindLaserDots(dots0, &dots_infos0);
			ldd1.FindLaserDots(dots1, &dots_infos1);
			dots0_all = dots0;
			dots1_all = dots1;

			// Copy to visualization image
			cv::cvtColor(pattern_image_left, projector_image_left, cv::COLOR_GRAY2BGR);
			cv::cvtColor(pattern_image_right, projector_image_right, cv::COLOR_GRAY2BGR);

			if(dots0.empty() || dots1.empty())
			{
				cv::Mat collage;
				cv::putText(projector_image,
				            "Not Enough Laser Dots",
				            cv::Point(100, 100),
				            cv::FONT_HERSHEY_SIMPLEX,
				            2.0,
				            Constants::DARK_BLUE,
				            3);

				Helper::CreateCollageImage3And1(collage,
				                                cv::Mat::zeros(1, 1, CV_8UC3),
				                                projector_image_left,
				                                projector_image_right,
				                                projector_image);

				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No laser dots in at least one camera";
				continue;
			}

			Dots filtered_dots0, filtered_dots1;
			DotInfos filtered_dots0_infos, filtered_dots1_infos;

			// If there are more dots visible than bright pattern beams -> we also see dark beams.
			// Then we can try to distinguish both groups by analysis of relative dots brightness
			// and thus identify the bright beams.
			if(dots0_all.size() > Constants::PROJECTOR_CALIBRATION_MIN_NUMBER_OF_DOTS)
			{
				// Get only bright points
				std::vector<size_t> dots0_masked;
				std::vector<size_t> dots1_masked;

				size_t index_in = 0;
				std::for_each(dots0.begin(),
				              dots0.end(),
				              [&index_in, &dots0_masked](const cv::Vec2f&)
				              {
					              dots0_masked.push_back(index_in++);
				              });

				index_in = 0;
				std::for_each(dots1.begin(),
				              dots1.end(),
				              [&index_in, &dots1_masked](const cv::Vec2f&)
				              {
					              dots1_masked.push_back(index_in++);
				              });

				// Analyze dot intensities and find bright dots
				std::vector<size_t> bright_dots0_indices, bright_dots1_indices;
				auto if_bright_laser_dots0_found = ProjectorCalibrationHelper::GetBrightLaserDotIndices(
					dots_infos0,
					dots0_masked,
					bright_dots0_indices);

				auto if_bright_laser_dots1_found = ProjectorCalibrationHelper::GetBrightLaserDotIndices(
					dots_infos1,
					dots1_masked,
					bright_dots1_indices);

				if(if_bright_laser_dots0_found && if_bright_laser_dots1_found)
				{
					// All good, use points
					for(auto bright_dot0_idx : bright_dots0_indices)
					{
						filtered_dots0.push_back(dots0[bright_dot0_idx]);
						filtered_dots0_infos.push_back(dots_infos0[bright_dot0_idx]);
					}

					for(auto bright_dot1_idx : bright_dots1_indices)
					{
						filtered_dots1.push_back(dots1[bright_dot1_idx]);
						filtered_dots1_infos.push_back(dots_infos1[bright_dot1_idx]);
					}
				}
				else
				{
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ <<
 "() : Not enough bright laser dots found or separation of bright/dark points not possible";
					continue;
				}
			}
			else
			{
				filtered_dots0       = dots0;
				filtered_dots0_infos = dots_infos0;

				filtered_dots1       = dots1;
				filtered_dots1_infos = dots_infos1;
			}

			// Draw dots to IR frame
			for(const auto& dot0_filtered : filtered_dots0)
			{
				circle(projector_image_left, cv::Point2f(dot0_filtered), 2, Constants::RED, 1, CV_AA);
			}

			for(const auto& dot1_filtered : filtered_dots1)
			{
				circle(projector_image_right, cv::Point2f(dot1_filtered), 2, Constants::RED, 1, CV_AA);
			}

			if(filtered_dots0.empty())
			{
				continue;
			}

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Found laser dots in Image [" << index << "] CHANNEL_GREY0 [" << dots_infos0.
size() << "] filtered [" << filtered_dots0.size() << "], CHANNEL_GREY1 [" << dots_infos1.size() << "] filtered [" << filtered_dots1.
size() << "]";

			/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finding Laser Dots " << Constants::LOG_END;*/

			// 3. Reconstruct Laser dots in 3D for the beams intersecting with the board plane.
			// Calculate plane to origin distance for Hesse Normal Form
			//auto distance_hesse = std::fabs(plane_normal_reference_board.dot(plane_position_reference_board));
			//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : distance_hesse " << distance_hesse;

			// Calculate intersection Points of IR0 beams with plane detected by color camera
			std::vector<cv::Point3f> reference_board_intersection_points_ir0, secondary_board_intersection_points_ir0;

			cv::Vec3d cv_plane_position_reference_board(plane_position_reference_board[0],
			                                            plane_position_reference_board[1],
			                                            plane_position_reference_board[2]);

			cv::Vec3d cv_plane_normal_reference_board(plane_normal_reference_board[0],
			                                          plane_normal_reference_board[1],
			                                          plane_normal_reference_board[2]);

			cv::Vec3d cv_plane_position_secondary_board(plane_position_secondary_board[0],
			                                            plane_position_secondary_board[1],
			                                            plane_position_secondary_board[2]);

			cv::Vec3d cv_plane_normal_secondary_board(plane_normal_secondary_board[0],
			                                          plane_normal_secondary_board[1],
			                                          plane_normal_secondary_board[2]);

			// Ensure the order here is same as the order for intersection_points_ir0_plane
			// filtered_dots0_new = filtered_dots0_plane_master + filtered_dots0_plane_Secondary;
			cv::Mat reference_board_mask, secondary_board_mask;
			cv::Mat filtered_dots0_image(pattern_image_left.rows, pattern_image_left.cols, CV_64F, cv::Scalar::all(0));

			for(auto filtered_dot0 : filtered_dots0)
			{
				const auto col                            = int(std::round(filtered_dot0.x));
				const auto row                            = int(std::round(filtered_dot0.y));
				filtered_dots0_image.at<double>(row, col) = 255;
			}

			GetSecondaryBoardMask(filtered_dots0_image, polygon_vertices_map[0], reference_board_mask);
			GetSecondaryBoardMask(filtered_dots0_image, polygon_vertices_map[1], secondary_board_mask);

			Dots reference_board_filtered_dots0, secondary_board_filtered_dots0;
			DotInfos reference_board_filtered_dots0_infos, secondary_board_filtered_dots0_infos;

			// Add the second board elements to  filtered_dots0_board2 and filtered_dots0_infos_board2
			for(auto idx = 0; idx < filtered_dots0.size(); idx++)
			{
				const auto col = int(std::round(filtered_dots0[idx].x));
				const auto row = int(std::round(filtered_dots0[idx].y));

				if(int(reference_board_mask.at<double>(row, col)) == 255)
				{
					reference_board_filtered_dots0.push_back(filtered_dots0[idx]);
					reference_board_filtered_dots0_infos.push_back(filtered_dots0_infos[idx]);
					filtered_dots0.erase(filtered_dots0.begin() + idx);
					filtered_dots0_infos.erase(filtered_dots0_infos.begin() + idx);
				}
				else if(int(secondary_board_mask.at<double>(row, col)) == 255)
				{
					secondary_board_filtered_dots0.push_back(filtered_dots0[idx]);
					secondary_board_filtered_dots0_infos.push_back(filtered_dots0_infos[idx]);
					filtered_dots0.erase(filtered_dots0.begin() + idx);
					filtered_dots0_infos.erase(filtered_dots0_infos.begin() + idx);
				}
			}

			// Removed the elements that have been moved to filtered_dots0_board2 and filtered_dots0_infos_board2
			// This was not done in last step as it is not a good idea to change the vector when it is getting iterated
			// Removed these filtered_dots0_board2 and filtered_dots0_infos board2 from original collection
			capture_calibration.GetCalibration(CHANNEL_GREY0)->GetPlaneIntersectionPoints(reference_board_filtered_dots0,
			                                                                              cv_plane_position_reference_board,
			                                                                              cv_plane_normal_reference_board,
			                                                                              reference_board_intersection_points_ir0);

			capture_calibration.GetCalibration(CHANNEL_GREY0)->GetPlaneIntersectionPoints(secondary_board_filtered_dots0,
			                                                                              cv_plane_position_secondary_board,
			                                                                              cv_plane_normal_secondary_board,
			                                                                              secondary_board_intersection_points_ir0);

			std::vector<cv::Point3f> all_board_intersection_points_ir0;
			all_board_intersection_points_ir0.insert(all_board_intersection_points_ir0.end(),
			                                         reference_board_intersection_points_ir0.begin(),
			                                         reference_board_intersection_points_ir0.end());
			all_board_intersection_points_ir0.insert(all_board_intersection_points_ir0.end(),
			                                         secondary_board_intersection_points_ir0.begin(),
			                                         secondary_board_intersection_points_ir0.end());

			Dots all_intersection_points_ir0;
			all_intersection_points_ir0.insert(all_intersection_points_ir0.end(),
			                                   reference_board_filtered_dots0.begin(),
			                                   reference_board_filtered_dots0.end());

			all_intersection_points_ir0.insert(all_intersection_points_ir0.end(),
			                                   secondary_board_filtered_dots0.begin(),
			                                   secondary_board_filtered_dots0.end());

			DotInfos all_intersection_points_infos_ir0;
			all_intersection_points_infos_ir0.insert(all_intersection_points_infos_ir0.end(),
			                                         reference_board_filtered_dots0_infos.begin(),
			                                         reference_board_filtered_dots0_infos.end());

			all_intersection_points_infos_ir0.insert(all_intersection_points_infos_ir0.end(),
			                                         secondary_board_filtered_dots0_infos.begin(),
			                                         secondary_board_filtered_dots0_infos.end());

			//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : All intersection points Infos IR0 [" << all_intersection_points_infos_ir0.size() << "]";

			// Ensure the order here is same as the order for filtered_dots0_new
			// intersection_points_ir0_plane = intersection_points_ir0_plane_master + intersection_points_ir0_plane_secondary
			// Calculate the projections of intersection Points into IR1 image and draw them
			std::vector<cv::Point2f> all_ir0_points_projected_to_ir1;
			capture_calibration.GetCalibration(CHANNEL_GREY1)->ProjectPoints(all_board_intersection_points_ir0,
			                                                                 all_ir0_points_projected_to_ir1);
			auto debug_image = projector_image_right.clone();

			std::for_each(all_ir0_points_projected_to_ir1.begin(),
			              all_ir0_points_projected_to_ir1.end(),
			              [&debug_image](const cv::Point2f& point)
			              {
				              cv::circle(debug_image, point, 5, Constants::BRIGHT_GREEN);
			              });

			auto debug_image_path = settings.storage_path + "\\Debug\\debug_image_" + std::to_string(recorded_position_count) + ".png";

			cv::imwrite(debug_image_path, debug_image);

			// 4. Find the correspondences for laser dots of left camera in right camera.
			// Find correspondences by distance of projection of intersection IR0 and IR1 measurements
			Dots correspondences0, correspondences1;

			// Create cv::Mat with projected intersection points for neighborhood search
			pointVec projected_points_vec;

			for(const auto& ir0_point_projected_to_ir1 : all_ir0_points_projected_to_ir1)
			{
				projected_points_vec.push_back({ir0_point_projected_to_ir1.x, ir0_point_projected_to_ir1.y});
			}

			if(filtered_dots0.empty() || filtered_dots1.empty())
			{
				continue;
			}

			point_t q(2);
			auto radius = 12.0;
			KDTree kd_tree(projected_points_vec);
			std::vector<Projector::DotInfoType> reconstructed_dot_infos;

			for(size_t dot1_idx = 0; dot1_idx < filtered_dots1.size(); ++dot1_idx)
			{
				q[0] = double(filtered_dots1[dot1_idx].x);
				q[1] = double(filtered_dots1[dot1_idx].y);

				auto nearest_neighbor_index = kd_tree.neighborhood_indices(q, radius);

				if(nearest_neighbor_index.size() == 1)
				{
					correspondences0.push_back(all_intersection_points_ir0[nearest_neighbor_index[0]]);
					correspondences1.push_back(filtered_dots1[dot1_idx]);

					Projector::DotInfoType dot_info_type{};
					dot_info_type.beam_idx   = -1;
					dot_info_type.brightness = (all_intersection_points_infos_ir0[nearest_neighbor_index[0]][4] + filtered_dots1_infos[
							dot1_idx][4]) /
						2.0f;
					dot_info_type.dia = 1.0;
					reconstructed_dot_infos.push_back(dot_info_type);
				}
				else
				{
					if(!nearest_neighbor_index.empty())
					{
						BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Found neighbors [" << nearest_neighbor_index.size() << "] " <<
 Constants::CHECK_ME;
					}
				}
			}

			// Reconstruct 3d points from correspondences above
			if(correspondences0.empty())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : correspondences0 is Empty " << Constants::CHECK_ME;
				continue;
			}

			if(correspondences_0_stream.is_open())
			{
				for(auto idx = 0; idx < correspondences0.size(); idx++)
				{
					auto correspondence_point = correspondences0[idx];
					correspondences_0_stream << correspondence_point.x << " " << correspondence_point.y << '\n';
				}
			}

			if(correspondences_1_stream.is_open())
			{
				for(auto idx = 0; idx < correspondences1.size(); idx++)
				{
					auto correspondence_point = correspondences1[idx];
					correspondences_1_stream << correspondence_point.x << " " << correspondence_point.y << '\n';
				}
			}

			// 5. Triangulate laser dots between the cameras for refining the 3d position.
			auto ir0_sensor = capture_calibration.GetCalibration(CHANNEL_GREY0);
			if(!ir0_sensor)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot access ir0_sensor calibration";
				return CALIBRATION_FAILED;
			}

			auto ir1_sensor = capture_calibration.GetCalibration(CHANNEL_GREY1);
			if(!ir1_sensor)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot access ir1_sensor calibration";
				return CALIBRATION_FAILED;
			}

			// calculate 3d points
			std::vector<cv::Vec3d> reconstructed_3d_points;
			ir0_sensor->StereoIntersect(ir1_sensor, correspondences0, correspondences1, reconstructed_3d_points, nullptr, false);

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : correspondences0 " << correspondences0.size() << " correspondences1 " <<
				correspondences1.size() << " reconstructed_3d_points " << reconstructed_3d_points.size();

			// Draw reconstructed 3d point projections in IR Images
			for(size_t idx = 0; idx < reconstructed_3d_points.size(); ++idx)
			{
				cv::circle(projector_image_left, cv::Point2f(correspondences0[idx]), 15, Constants::BRIGHT_GREEN);
				cv::circle(projector_image_right, cv::Point2f(correspondences1[idx]), 15, Constants::BRIGHT_GREEN);
			}

			/*	auto path0 = settings.storage_path + "\\Debug\\projector_image0_" + std::to_string(recorded_position_count) + ".png";
				auto path1 = settings.storage_path + "\\Debug\\projector_image1_" + std::to_string(recorded_position_count) + ".png";

				cv::imwrite(path0, projector_image_left);
				cv::imwrite(path1, projector_image_right);*/
			recorded_position_count++;

			//Project the reconstructed 3d points into projector to get 2d points
			const auto projector_calibration = capture_calibration.GetCalibration(CHANNEL_PROJECTOR0);
			if(!projector_calibration)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot access projector calibration";
				return CALIBRATION_FAILED;
			}

			std::vector<cv::Point2d> projected_points_2d;
			projector_calibration->LaserProjectPoints(reconstructed_3d_points, projected_points_2d);

			if(projected_points_2d.size() != reconstructed_3d_points.size())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : projected_points_2d.size() != reconstructed_3d_points.size() ???? " << index;
			}

			if(projected_points_2d_stream.is_open())
			{
				for(auto idx = 0; idx < projected_points_2d.size(); idx++)
				{
					auto reconstructed_3d_point = reconstructed_3d_points[idx];
					auto projected_point_2d     = projected_points_2d[idx];
					projected_points_2d_stream << reconstructed_3d_point[0] << " " << reconstructed_3d_point[1] << " " <<
						reconstructed_3d_point[2] << " " << projected_point_2d.x << " " << projected_point_2d.y << " 1 " << index
						<< '\n';
				}
			}

			std::vector<cv::Point3d> correspondences_reconstructed_3d_points;
			std::vector<cv::Point2d> correspondences_calibrated_points;
			//Compare these 2d Points with calibrated 2D points.
			for(size_t idx = 0; idx < projected_points_2d.size(); ++idx)
			{
				q2[0] = double(projected_points_2d[idx].x);
				q2[1] = double(projected_points_2d[idx].y);

				auto nearest_neighbor_index = calibrated_bright_laser_dots_kd_tree.neighborhood_indices(q2, calibrated_laser_dots_search_radius);

				if(nearest_neighbor_index.size() == 1)
				{
					correspondences_reconstructed_3d_points.emplace_back(reconstructed_3d_points[idx]);
					correspondences_calibrated_points.emplace_back(calibrated_bright_laser_dots_mat.at<double>(nearest_neighbor_index[0], 1),
					                                               calibrated_bright_laser_dots_mat.at<double>(nearest_neighbor_index[0], 2));

					correspondences_3d_points_all.emplace_back(reconstructed_3d_points[idx]);
					correspondences_calibrated_2d_points_all.emplace_back(
						calibrated_bright_laser_dots_mat.at<double>(nearest_neighbor_index[0], 1),
						calibrated_bright_laser_dots_mat.at<double>(nearest_neighbor_index[0], 2));
				}
				else
				{
					if(!nearest_neighbor_index.empty())
					{
						BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Found neighbors " << nearest_neighbor_index.size();
					}
				}
			}

			//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : correspondences_projected_points [" << correspondences_reconstructed_3d_points.size() << "] index [" << index << "]";
			correspondences_3d_points_map[index]            = correspondences_reconstructed_3d_points;
			correspondences_calibrated_2d_points_map[index] = correspondences_calibrated_points;
			//			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : correspondences_calibrated_2d_points_map [" << correspondences_calibrated_2d_points_map.
			//size() << "] index [" << index << "]";

			if(calibrated_points_2d_stream.is_open())
			{
				for(auto idx = 0; idx < correspondences_calibrated_points.size(); idx++)
				{
					//auto reconstructed_3d_point = reconstructed_3d_points[idx];
					auto calibrated_point_2d = correspondences_calibrated_points[idx];
					calibrated_points_2d_stream << calibrated_point_2d.x << " " << calibrated_point_2d.y << " 1 " << index << '\n';
				}
			}
		}

		// 6. Project back these 3d points (remains constant) in the projector for all the images. (do it in minimize loop).
		auto projector_calibration = capture_calibration.GetCalibration(CHANNEL_PROJECTOR0);
		if(!projector_calibration)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot access projector calibration";
			return CALIBRATION_FAILED;
		}

		std::vector<double> start_params;
		cv::Mat rotation_matrix, rotation_vector, translation_vector;
		projector_calibration->GetExtrinsicParameters(rotation_matrix, translation_vector);
		cv::Rodrigues(rotation_matrix, rotation_vector);
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : original rotation_vector " << rotation_vector.at<double>(0, 0) << " " <<
				rotation_vector.at<double>(1, 0) << " " << rotation_vector.at<double>(2, 0);
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : original translation_vector " << translation_vector.at<double>(0, 0) << " " <<
				translation_vector.at<double>(1, 0) << " " << translation_vector.at<double>(2, 0);

		cv::Mat projector_matrix, distortion;
		projector_calibration->GetCameraIntrinsicParams(projector_matrix, distortion);

		start_params.push_back(rotation_vector.at<double>(0, 0));
		start_params.push_back(rotation_vector.at<double>(1, 0));
		start_params.push_back(rotation_vector.at<double>(2, 0));
		start_params.push_back(translation_vector.at<double>(0, 0));
		start_params.push_back(translation_vector.at<double>(1, 0));
		start_params.push_back(translation_vector.at<double>(2, 0));
		Eigen::VectorXd x = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(start_params.data(), start_params.size());

		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : correspondences_calibrated_2d_points_map " << correspondences_calibrated_2d_points_map.size() << " " << Constants::CHECK_ME;
		calibration_calculator.OptimizeProjectorSystem(x, correspondences_3d_points_map, correspondences_calibrated_2d_points_map);
		auto projector_calibration_new = calibration_calculator.GetRelativeFixedCamerasParameterOptimizer()->GetCalibration(CHANNEL_PROJECTOR0);
		cv::Mat rotation_matrix_optimizer, rotation_vector_optimizer, translation_vector_optimizer;
		projector_calibration_new->GetExtrinsicParameters(rotation_matrix_optimizer, translation_vector_optimizer);
		cv::Rodrigues(rotation_matrix_optimizer, rotation_vector_optimizer);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : rotation_vector_optimizer " << rotation_vector_optimizer.at<double>(0, 0) << " " <<
 rotation_vector_optimizer.at<double>(1, 0) << " " << rotation_vector_optimizer.at<double>(2, 0);
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : translation_vector_optimizer " << translation_vector_optimizer.at<double>(0, 0) << " " <<
 translation_vector_optimizer.at<double>(1, 0) << " " << translation_vector_optimizer.at<double>(2, 0);

		calibration_file_parser.SetRotationMatrix(31, rotation_matrix_optimizer);
		calibration_file_parser.SetTranslationVector(31, translation_vector_optimizer);

		cv::Mat rotation_matrix_20, translation_vector_20, camera_matrix_20, distortion_vector_20;
		capture_calibration.GetCalibration(CHANNEL_GREY0)->GetExtrinsicParameters(rotation_matrix_20, translation_vector_20);
		capture_calibration.GetCalibration(CHANNEL_GREY0)->GetCameraIntrinsicParams(camera_matrix_20, distortion_vector_20);

		calibration_file_parser.SetRotationMatrix(20, rotation_matrix_20);
		calibration_file_parser.SetTranslationVector(20, translation_vector_20);
		calibration_file_parser.SetCameraMatrix(20, camera_matrix_20);

		cv::Mat rotation_matrix_21, translation_vector_21, camera_matrix_21, distortion_vector_21;
		capture_calibration.GetCalibration(CHANNEL_GREY1)->GetExtrinsicParameters(rotation_matrix_21, translation_vector_21);
		capture_calibration.GetCalibration(CHANNEL_GREY1)->GetCameraIntrinsicParams(camera_matrix_21, distortion_vector_21);
		calibration_file_parser.SetRotationMatrix(21, rotation_matrix_21);
		calibration_file_parser.SetTranslationVector(21, translation_vector_21);
		calibration_file_parser.SetCameraMatrix(21, camera_matrix_21);

		calibration_file_parser.SaveCalibrationFile();

		projected_points_2d_stream.close();
		calibrated_points_2d_stream.close();

		Calculate3DCalibrationError(observations, capture_calibration, frame_transformation_map, settings);
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calculated RMS error was greater than " << Constants::CALIBRATION_MAX_PERMITTED_RMS_ERROR;
		return CALIBRATION_FAILED_RMS;
	}

	return 0;
}

//int IsDoeOrthogonalDirection(const cv::Vec2d direction)
//{
//	const auto doe_skew_deg = 6;
//	const auto tolerance = 7;
//
//	const auto y = -tan(doe_skew_deg / 180.0 * 3.14159);
//	const auto x = -y;
//
//	const cv::Vec2d skew_x(1.0, y);
//	const cv::Vec2d skew_y(x, 1.0);
//
//	const auto delta_x = Helper::AngleTo(direction, skew_x);
//	const auto ang_x = delta_x / 3.14159 * 180.0;
//
//	const auto delta_y = Helper::AngleTo(direction, skew_y);
//	const auto ang_y = delta_y / 3.14159 * 180.0;
//
//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : ang_y " << ang_y << " delta_y " << delta_y;
//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : ang_x " << ang_x << " delta_x " << delta_x;
//
//	if(ang_x < tolerance || ang_x > 180 - tolerance)
//		return 1;
//
//	if(ang_y < tolerance || ang_y > 180 - tolerance)
//		return 2;
//
//	return 0;
//}

int IsDiagonal(const cv::Vec2d& direction)
{
	const auto doe_skew_deg = 6;
	const auto tolerance    = 7;

	const auto a = -tan((doe_skew_deg + 45) / 180.0 * 3.14159);
	const auto b = -tan((doe_skew_deg - 45) / 180.0 * 3.14159);

	const cv::Vec2d skew_x(1.0, a);
	const cv::Vec2d skew_y(1.0, b);

	const auto delta_x = Helper::AngleTo(direction, skew_x);
	const auto ang_x   = delta_x / 3.14159 * 180.0;

	const auto delta_y = Helper::AngleTo(direction, skew_y);
	const auto ang_y   = delta_y / 3.14159 * 180.0;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : ang_y " << ang_y << " delta_y " << delta_y;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : ang_x " << ang_x << " delta_x " << delta_x;

	if(ang_x < tolerance || ang_x > 180 - tolerance)
		return 1;

	if(ang_y < tolerance || ang_y > 180 - tolerance)
		return 2;

	return 0;
}

int FactoryCalibration(ApplicationSettings& settings)
{
	auto exit_code             = CALIBRATION_OK;
	auto configuration_options = std::make_shared<ConfigurationContainer>();
	DMVSSensor dmvs;

	if(settings.record && dmvs.SetUpSensor(configuration_options) != CALIBRATION_SENSOR_SETUP_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration Sensor setup failed ";
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Trying to process the images offline";
		return CALIBRATION_SENSOR_SETUP_ERROR;
	}

	fs::path calibration_file_path;
	auto camera_path      = Helper::GetCamerasDirectory();
	auto filename         = settings.record ? dmvs.GetCalibrationFilename() : settings.calibration_filename;
	calibration_file_path = camera_path / fs::path(filename);

	if(fs::exists(calibration_file_path) && fs::is_regular_file(calibration_file_path))
	{
		settings.calibration_files_folder = calibration_file_path.string();
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Default calibration file " << calibration_file_path << " does not exist";
		return CALIBRATION_CAMERA_CALIBRATION_TEMPLATE_FILE_MISSING;
	}

	CalibrationXMLParser calibration_file_parser(calibration_file_path);

	if(settings.record)
	{
		settings.storage_path = settings.storage_path + "\\" + dmvs.serial_number + "\\" + Helper::GetTimeStamp();
	}

	if(settings.record && !fs::exists(settings.storage_path))
	{
		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Created folder for storing files ";
		fs::create_directories(settings.storage_path);
	}

	IKinematics* linear_axis;
	if(!SetUpLinearAxis(linear_axis) && settings.record)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Linear Axis setup failed ";
		return CALIBRATION_NO_LINEAR_AXIS_FOUND;
	}

	if(!linear_axis)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Linear Axis cannot be used ";
		return CALIBRATION_NO_LINEAR_AXIS_FOUND;
	}

	const std::string serial_number = settings.record ? dmvs.GetConnectedDMVS()->GetSerialNumber() : "DMVS_DMVS011900000";
	const auto file_base            = CalibrationFileBase::GetFileBase(serial_number, configuration_options);

	if(!file_base)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : File base creation failed ";
		return IOMessages::IO_UNDEFINED;
	}

	std::string file_name;
	file_base->GetCameraCalibrationFSVOutputFile(file_name);
	auto calibration_cache = std::make_shared<CalibrationCache>();
	SetUpCalibrationCache(configuration_options, serial_number, file_name, calibration_cache);

	////====================================================================
	// Add empty images for grey0, grey1 channel
	ChannelImageMap channel_image_map, channel_image_map_copy;
	channel_image_map.channel_image_map[CHANNEL_GREY0] = cv::Mat();
	channel_image_map.channel_image_map[CHANNEL_GREY1] = cv::Mat();

	BlobDetector blob_detector;
	std::vector<std::shared_ptr<BoardDetector>> boards;
	if(!BoardDetector::LoadDetectorBoards(file_base, boards, configuration_options))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration board loading failed";
		return CALIBRATION_BOARD_NOT_LOADED;
	}

	// Collect all marker detections called observations: (frame/board/marker/camera/coordinates)
	// Data structure to keep the images of all channels in all frames
	// e.g. all_images.frame[frame_id].channel[CHANNEL_GREY0] = cv::Mat()
	Frame frame_images{};

	// Flag for indicating if the calibration is done
	auto is_data_acquisition_complete = false;

	// Counter for frame per position
	int position_idx = 0;

	// Index of the current frame
	int frame_index = 0;

	if(settings.record)
	{
		ChangeLedPower(dmvs, position_idx);
	}

	std::vector<std::shared_ptr<Observation>> observations;
	std::map<size_t, double> linear_axis_positions;
	std::vector<cv::Mat> laser_images;
	std::map<size_t, std::vector<cv::Mat>> recorded_images_map;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finding Observations " << Constants::LOG_START;

	while(!is_data_acquisition_complete)
	{
		if(settings.record)
		{
			SetPositionBasedExposureTime(dmvs, position_idx);

			if(!dmvs.PushChannelImageToCache())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration image grabbing failed ";
				return CALIBRATION_IMAGE_GRABBING_ERROR;
			}

			// Retrieve images of all Freestyle channels
			if(RetrieveChannelImages(dmvs, channel_image_map, frame_index) != CALIBRATION_IMAGE_RETRIEVED)
			{
				BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::LOG_NO_IMAGE_RETRIEVED;
				continue;
			}

			CopyChannelImageMap(channel_image_map, channel_image_map_copy);
			Helper::CopyImages(channel_image_map_copy, frame_images, position_idx);

			std::vector<ImageChannelCircularMarkerVectorMap> found_circular_markers_map;
			found_circular_markers_map.reserve(Constants::FRAMES_PER_POSITION);
			ChannelImageVectorMap image_vector_channel_map;

			FrameProcessing(dmvs,
			                channel_image_map,
			                blob_detector,
			                found_circular_markers_map,
			                image_vector_channel_map,
			                frame_index,
			                position_idx,
			                settings);

			PushImagesToCalibrationCache(calibration_cache, channel_image_map, position_idx);
			std::map<ImageChannel, std::vector<CircularMarkerPair>> markers_combined_map;
			RemoveSaturatedMarkers(found_circular_markers_map, markers_combined_map);
			DrawMarkers(image_vector_channel_map, markers_combined_map);
			std::map<ImageChannel, CircularMarkerVector> markers_combined;
			CombineMarkers(markers_combined_map, markers_combined);
			auto coded_marker_count = Helper::AddCodedMarkers(boards, markers_combined, position_idx, observations, channel_image_map_copy);

			if(coded_marker_count < Constants::MIN_COUNT_CODED_MARKER)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not enough coded markers found, trying again. Found only [" << coded_marker_count <<
	                    "] marker";
				continue;
			}

			Helper::AddUncodedMarkers(markers_combined, observations, position_idx);
			CreateAndShowCollage(channel_image_map_copy, position_idx);

			position_idx++;

			ChangeLedPower(dmvs, position_idx);

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Frame Count [" << position_idx << "] Number of Calibration Positions [" << Constants::
	                NUMBER_CALIBRATION_POSITIONS << "]";

			is_data_acquisition_complete = position_idx >= int(Constants::NUMBER_CALIBRATION_POSITIONS);

			if(settings.record && linear_axis && dmvs.IsLive())
			{
				if(!dmvs.Stop())
				{
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Stop acquisition error ";
				}

				Helper::Sleeping(1, " while dmvs stops acquisition");
				auto current_position             = linear_axis->GetPosition();
				auto const current_position_value = current_position->at<double>(0, 3);
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Current Position [" << current_position_value << "] Frame Index [" << frame_index <<
	                    "] Position Idx [" << position_idx << "]";

				// Save the actual position from the linear axis
				linear_axis_positions[position_idx - 1] = 0.6 - current_position_value;

				if(!is_data_acquisition_complete)
				{
					auto next_position_value = current_position_value - Constants::LINEAR_STAGE_STEPPING;
					linear_axis->MoveXAxis(next_position_value);
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Moving to next Position [" << next_position_value << "]";
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Frame Count [" << position_idx << "] Calibration Position Count [" <<
	                        Constants::NUMBER_CALIBRATION_POSITIONS << "]";

					// Check if the image is same or different what is getting store in the cache and what is being read
					linear_axis->WaitForMovementToFinish();
				}

				delete current_position;
			}

			linear_axis->WaitForMovementToFinish();
		}
		else
		{
			LoadRecordedImages(settings.storage_path, recorded_images_map);

			// Add empty images for grey0, grey1 channel
			ChannelImageMap channel_image_map_offline, channel_image_map_copy_offline;
			//for(auto index = 0; index < recorded_images_map.size(); index++)
			for(auto index = 0; index < 3; index++)
			{
				ImageChannelCircularMarkerVectorMap circular_markers_in_current_frame, markers_combined;
				std::vector<ImageChannelCircularMarkerVectorMap> found_circular_markers_map;

				for(const auto& recorded_image : recorded_images_map[index])
				{
					// Split the image in two parts
					channel_image_map_offline.channel_image_map[CHANNEL_GREY0] = recorded_image(
						cv::Rect(0, 0, Constants::DMVS_IMAGE_WIDTH, Constants::DMVS_IMAGE_HEIGHT));
					channel_image_map_offline.channel_image_map[CHANNEL_GREY1] = recorded_image(
						cv::Rect(Constants::DMVS_IMAGE_WIDTH, 0, Constants::DMVS_IMAGE_WIDTH, Constants::DMVS_IMAGE_HEIGHT));

					Helper::DetectMarkers(channel_image_map_offline, blob_detector, circular_markers_in_current_frame);
					Helper::RemoveLowQualityMarker(circular_markers_in_current_frame, channel_image_map_offline);
					found_circular_markers_map.push_back(circular_markers_in_current_frame);
				}

				auto& current_image = recorded_images_map[index][0];
				CopyChannelImageMap(channel_image_map_offline, channel_image_map_copy_offline);
				Helper::CopyImages(channel_image_map_copy_offline, frame_images, index);
				PushImagesToCalibrationCache(calibration_cache, channel_image_map_offline, index);

				std::map<ImageChannel, std::vector<CircularMarkerPair>> unsaturated_markers_map;
				RemoveSaturatedMarkers(found_circular_markers_map, unsaturated_markers_map);

				//DrawMarkers(channel_image_vector_map, unsaturated_markers_map);
				CombineMarkers(unsaturated_markers_map, markers_combined);
				auto coded_marker_count = Helper::AddCodedMarkers(boards, markers_combined, index, observations, channel_image_map_copy_offline);

				if(coded_marker_count < 3)
				{
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not enough coded markers found, trying again. Found only [" << coded_marker_count
						<< "] coded markers";
					continue;
				}

				Helper::AddUncodedMarkers(markers_combined, observations, index);
				//CreateAndShowCollage(channel_image_map_copy_offline, index);
			}

			is_data_acquisition_complete = true;
		}
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finding Observations " << Constants::LOG_END;

	if(settings.record && !dmvs.Stop())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Stop Acquisition error ";
	}

	//std::ofstream observations_stream;
	//auto paired_observations_path = settings.storage_path + R"(\observations_new.txt)";
	//observations_stream.open(paired_observations_path);
	//for(auto& observation : observations)
	//{
	//	observations_stream << observation->GetMarkerId() << " " << observation->GetFrameId() << " " << observation->GetImageChannel() << " " <<
	//		observation->GetBoardId() << "\n";
	//}
	//observations_stream.close();

	CalibrationCalculator calibration_calculator(observations);

	// Write stepping to position file
	std::string linear_table_position_file;
	file_base->GetCameraCalibrationLinearStagePositionOutputFile(linear_table_position_file);
	//for(auto i = 0; i < Constants::NUMBER_CALIBRATION_POSITIONS; ++i)
	for(auto i = 0; i < 3; ++i)
	{
		linear_axis_positions[i] = 0.03 * i;
	}

	std::shared_ptr<CameraSystemTransformation> linear_table_frame_transformation = std::make_shared<LinearTableFramePosition>(linear_axis_positions);
	calibration_calculator.SetCameraSystemTransformation(linear_table_frame_transformation);
	//calibration_calculator.InitializeCameraSystemTransformation(recorded_images.size());
	std::set<size_t> detected_boards_set;

	std::vector<std::shared_ptr<BoardDetector>> detected_boards;
	if(!GetDetectedBoard(observations, boards, detected_boards))
	{
		return CALIBRATION_BOARD_NOT_DETECTED;
	}

	// Set used calibration boards
	calibration_calculator.SetBoards(detected_boards);
	std::map<size_t, std::shared_ptr<const CameraSensor>> camera_sensor_map;
	InitializeCameraSensorMapInHouse(camera_sensor_map, settings);

	// Why is this done for only one CHANNEL_GREY0
	calibration_calculator.SetRelativeFixedCamera(camera_sensor_map, CHANNEL_GREY0);
	if(calibration_calculator.ProcessObservationsInHouse())
	{
		//CalculateStatistics(file_base, frame_images, calibration_calculator);
		auto left_camera_calibration = std::dynamic_pointer_cast<CameraSensorDepthDependent>(
			calibration_calculator.GetRelativeFixedCamerasParameterOptimizerLens()->GetCalibration(CHANNEL_GREY0));
		auto right_camera_calibration = std::dynamic_pointer_cast<CameraSensorDepthDependent>(
			calibration_calculator.GetRelativeFixedCamerasParameterOptimizerLens()->GetCalibration(CHANNEL_GREY1));

		if(left_camera_calibration && right_camera_calibration)
		{
			// get a Freestyle default calibration and update with new Freestyle calibration data as calculated
			cv::Mat rotation_matrix_20, translation_vector_20, camera_matrix_20, distortion_vector_20;
			calibration_file_parser.SetDepthDependentScalarParameters(CHANNEL_GREY0,
			                                                          left_camera_calibration->GetCameraInfo().intrinsics.depth_parameters.scalar);
			calibration_file_parser.SetDepthDependentExponentialParameters(CHANNEL_GREY0,
			                                                               left_camera_calibration->GetCameraInfo().intrinsics.depth_parameters.
			                                                               exponential);

			left_camera_calibration->GetExtrinsicParameters(rotation_matrix_20, translation_vector_20);
			left_camera_calibration->GetCameraIntrinsicParams(camera_matrix_20, distortion_vector_20);
			calibration_file_parser.SetRotationMatrix(20, rotation_matrix_20);
			calibration_file_parser.SetTranslationVector(20, translation_vector_20);
			calibration_file_parser.SetCameraMatrix(20, camera_matrix_20);
			calibration_file_parser.SetDistortionVector(20, distortion_vector_20);

			cv::Mat rotation_matrix_21, translation_vector_21, camera_matrix_21, distortion_vector_21;
			calibration_file_parser.SetDepthDependentScalarParameters(CHANNEL_GREY1,
			                                                          right_camera_calibration->GetCameraInfo().intrinsics.depth_parameters.scalar);
			calibration_file_parser.SetDepthDependentExponentialParameters(CHANNEL_GREY1,
			                                                               right_camera_calibration->GetCameraInfo().intrinsics.depth_parameters.
			                                                               exponential);
			right_camera_calibration->GetExtrinsicParameters(rotation_matrix_21, translation_vector_21);
			right_camera_calibration->GetCameraIntrinsicParams(camera_matrix_21, distortion_vector_21);
			calibration_file_parser.SetRotationMatrix(21, rotation_matrix_21);
			calibration_file_parser.SetTranslationVector(21, translation_vector_21);
			calibration_file_parser.SetCameraMatrix(21, camera_matrix_21);
			calibration_file_parser.SetDistortionVector(21, distortion_vector_21);
			calibration_file_parser.SaveCalibrationFile(true, "DMVS_" + settings.record ? dmvs.serial_number : settings.serial_no);

			CalculateStatistics(file_base, frame_images, calibration_calculator);
		}

		//====================================================================
		// Projector Calibration
		if(linear_axis->MoveToPosition(cv::Vec3d(Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START, 0.0, 0.0)))
		{
			linear_axis->SetVelocity(0.8);
		}

		auto* position = linear_axis->GetPosition();
		if(std::abs(position->at<double>(0, 3) - Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START) > Constants::DOUBLE_EPSILON)
		{
			if(linear_axis->MoveToPosition(cv::Vec3d(Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START, 0.0, 0.0)))
			{
				linear_axis->WaitForMovementToFinish();
			}
		}

#pragma region Preloop

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : PRE LOOP " << Constants::LOG_START;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Setting up Device and Data";

		// Active log. Save old log settings first.
		std::string log_file;
		file_base->GetProjectorCalibrationLogOutputFile(log_file);

		// device images (color, IR0, IR1)
		cv::Mat pattern_image0, pattern_image1;

		// channel images for user feedback
		cv::Mat channel_image, projector_image_grey0, projector_image_grey1, projector_image;

		if(!settings.calibration_files_folder.empty())
		{
			// This is just a temporary file path, usually the file name of the path should come from settings file.
			fs::path calibration_xml_file_path(R"(C:\ProgramData\FARO\Freestyle\Cameras\DMVS_DMVS011900031_1705.xml)");
			dmvs.capture_calibration.LoadCalibrationFromXmlFile(calibration_xml_file_path);
		}
		else
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration file path is empty. Please use valid xml filepath";
			return CALIBRATION_REQUIRED_FILE_MISSING;
		}

		//if(!process_offline && dmvs.cache && dmvs.cache->Get(0, "deviceOptions", configuration_options) != IOMessages::IO_OK)
		//{
		//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : There are no device options found in cache ";
		//	return CALIBRATION_REQUIRED_FILE_MISSING;
		//}

		if(settings.record && dmvs.configuration_options)
		{
			dmvs.configuration_options = configuration_options;
			dmvs.configuration_options->SetProjectorCalMinBeamLength(Constants::PROJECTOR_CALIBRATION_MINIMUM_BEAM_LENGTH);
		}

		// Projector object to keep all recorded points and do beam calculations
		const auto projector_calibration = dmvs.capture_calibration.GetCalibration(CHANNEL_PROJECTOR0);

		if(!projector_calibration)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot access projector calibration";
			return CALIBRATION_PROJECTOR_CALIBRATION_MISSING;
		}

		Projector projector(projector_calibration, configuration_options);
		projector.SetOutputPath(log_file);

		std::string doe_yml;
		file_base->GetProjectorCalibrationDoeYML(doe_yml);

		if(!fs::exists(doe_yml))
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DOE definition file not found. Expected it at " << doe_yml;

			return CALIBRATION_DOE_FILE_MISSING;
		}

		projector.SetDoeYmlPath(doe_yml);
		projector.InitCalibration();
		auto is_projector_calibrated   = false;
		auto ir_exposure_time_at_1m_ms = Constants::PROJECTOR_CALIBRATION_DEFAULT_IR_EXPOSURE_TIME_AT_1M_MS;

		if(!dmvs.SetExposureTime(ir_exposure_time_at_1m_ms) && settings.record)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Exposure time could not be set";
		}

		// Load 6dof target configuration ( ring marker positions )
		const auto reference_board_filename = file_base->GetInputPath()->string() + R"(\)" + Constants::FILE_CALIBRATION_BOARD_TEMPLATE;
		BoardDetectorLoader board_detector_loader;

		if(!board_detector_loader.ReadFile(reference_board_filename))
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not read reference calibration board file " << reference_board_filename;
			return CALIBRATION_REFERENCE_CALIBRATION_BOARD_FILE_MISSING;
		}

		// Create a 6dof target of coded ring marker board
		auto board_detector_grey0 = board_detector_loader.CreateDetector(dmvs.capture_calibration.GetCalibration(CHANNEL_GREY0));
		auto board_detector_grey1 = board_detector_loader.CreateDetector(dmvs.capture_calibration.GetCalibration(CHANNEL_GREY1));

		// Copy file to calibration folder
		std::string calibration_base_folder;
		file_base->GetProjectorCalibrationOutputFolder(calibration_base_folder);
		fs::copy_file(reference_board_filename, calibration_base_folder);

		auto initialized_step4 = false;

		// Try to find the Board with IR System
		cv::Vec3d plane_position_start, plane_position_end;
		cv::Vec3d plane_normal_start, plane_normal_end;
		std::map<size_t, CircularMarker> start_markers, end_markers;

		if(dmvs.IsLive())
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Determining Calibration Plate Position";

			if(!dmvs.FindTablePosition(linear_axis,
			                           plane_position_start,
			                           plane_position_end,
			                           plane_normal_start,
			                           plane_normal_end,
			                           board_detector_grey0,
			                           start_markers,
			                           end_markers))
			{
				return CALIBRATION_FAILED;
			}
		}
		else
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS is not LIVE. It is dead. Bring it back to LIFE by turning it OFF and ON again";
			throw std::exception("DMVS is not LIVE. It is dead. Bring it back to LIFE by turning it OFF and ON again");

			/*cv::Mat saved_plane_position_end(3, 1, CV_64F);
			cv::Mat saved_plane_position_start(3, 1, CV_64F);
			cv::Mat saved_plane_normal_end(3, 1, CV_64F);
			if(dmvs.cache)
			{
				dmvs.cache->Get(0, "PlateCalibrationStart", saved_plane_position_start, CalibrationCacheItem::CV_MATRIX);
				dmvs.cache->Get(0, "PlateCalibrationEnd", saved_plane_position_end, CalibrationCacheItem::CV_MATRIX);
				dmvs.cache->Get(0, "PlateCalibrationNormalEnd", saved_plane_normal_end, CalibrationCacheItem::CV_MATRIX);
			}
			else
			{
				BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not reference board file " << reference_board_filename;
				return NO_PARAMETERS_GIVEN;
			}
			for(auto i = 0; i < 3; ++i)
			{
				plane_position_start[i] = saved_plane_position_start.at<double>(i, 0);
				plane_position_end[i]   = saved_plane_position_end.at<double>(i, 0);
				plane_normal_end[i]     = saved_plane_normal_end.at<double>(i, 0);
			}*/
		}

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : PRE LOOP " << Constants::LOG_END;

		std::vector<cv::Point3f> all_3d_points;
		unsigned int frame_index_sfm = 0;

		// Save calibration boards
		//auto recording_info1 = std::make_shared<ConfigurationContainer>();
		//recording_info->Initialize();

		// reset stop
		dmvs.stop = false;

		if(!dmvs.SetScanSettings())
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Scan Settings could not be set ";
		}

		auto successful_frames_per_position = 0;
		auto initial_exposure_test_frame    = 0;
		auto image_counter                  = 0;

		std::string log_path = R"(D:\repos\BitBucket\farolabs\calibration-tools\CalibrationTools\DMVS011900031\20210426-101158\Logs)";

		std::ofstream output_ir0plane;
		auto output_path2 = log_path + R"(\output_ir0plane.txt)";
		output_ir0plane.open(output_path2);

		std::ofstream output_reconstructed_3d_points;
		auto output_path_output_reconstructed_3d_points = log_path + R"(\reconstructed_3d_point.txt)";
		output_reconstructed_3d_points.open(output_path_output_reconstructed_3d_points);

		std::ofstream output;
		auto output_path = log_path + R"(\reconstructed_3d_point_camera_cs.txt)";
		output.open(output_path);

		// At the beginning we try a really low exposure time to detect the central beam in case it is too bright
		while(!dmvs.stop)
		{
			if(initial_exposure_test_frame < Constants::PROJECTOR_CALIBRATION_INITIAL_EXPOSURE_TEST_FRAMES)
			{
				if(!dmvs.SetExposureTime(0.3))
				{
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Exposure time could not be set ";
				}

				initial_exposure_test_frame++;
			}

#pragma region captureAndCountour

			// save last and grab new frame
			// Only grab and save an image if grab(bool synced = true) is called
			if(!dmvs.PushChannelImageToCache())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : PushChannelImageToCache failed ";
				exit_code = CALIBRATION_NO_DEVICE;
				break;
			}

			// Get images
			auto retrieve_ok = true;

			for(auto i = 0; i < 3; ++i)
			{
				retrieve_ok &= dmvs.RetrieveImageFromCalibrationCache(dmvs.frame_index, CHANNEL_GREY0, pattern_image0);
				retrieve_ok &= dmvs.RetrieveImageFromCalibrationCache(dmvs.frame_index, CHANNEL_GREY1, pattern_image1);
			}

			if(!retrieve_ok)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Images from Cache could not be retrieved";
				continue;
			}

			// Info data is needed for each frame
			//auto config_container = std::make_shared<ConfigurationContainer>();
			//config_container->Initialize();
			projector_image    = projector.GetFeedbackImage();
			auto images_folder = settings.storage_path + R"(\Images)";

			if(!boost::filesystem::is_directory(images_folder))
			{
				boost::filesystem::create_directory(images_folder);
			}

			//cv::imwrite(images_folder + R"(\projector_image)" + std::to_string(image_counter) + ".png", projector_image);

			// Calculate the Board Position
			auto current_position = 0.0;

			if(dmvs.IsLive())
			{
				current_position = Helper::GetLinearStagePosition(linear_axis);
			}
			else
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS is not LIVE. It is dead. Bring it back to LIFE by turning it OFF and ON again";
				throw std::exception("DMVS is not LIVE. It is DEAD. Bring it back to LIFE by turning it OFF and ON again");
				std::string current_stag_position_string;
				dmvs.cache->Get(frame_index_sfm, "PlatePosition", current_stag_position_string);
				current_position = std::stold(current_stag_position_string);
			}

			frame_index_sfm++;

			auto interpolation_factor = (current_position - Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START)
				/ (Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_END - Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START);

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Interpolation Factor [" << interpolation_factor << "] Current Position [" <<
 current_position << "]";

			auto plane_position = plane_position_start - (plane_position_start - plane_position_end) * interpolation_factor;
			auto plane_normal   = plane_normal_end;
			//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Plane Normal " << plane_normal << " Plane Position " << plane_position;

			Eigen::Vector3d eigen_plane_location(plane_position[0], plane_position[1], plane_position[2]);
			Eigen::Vector3d eigen_plane_normal(plane_normal[0], plane_normal[1], plane_normal[2]);

			// calculate plane to origin distance for Hesse Normal Form
			auto distance_hesse = std::fabs(eigen_plane_normal.dot(eigen_plane_location));

#pragma endregion captureAndCountour

#pragma region detectAndFilter2Ddots
			// detect dots in raw image coordinates
			Dots dots0, dots1, dots0_all;

			// For dot brightness, to separate bright from dark dots
			DotInfos dots_infos0, dots_infos1;

			LaserDotDetector2D::Params params;
			params.bg_limit = 10;

			LaserDotDetector2D ldd0(pattern_image0, params);
			LaserDotDetector2D ldd1(pattern_image1, params);
			ldd0.FindLaserDots(dots0, &dots_infos0);
			ldd1.FindLaserDots(dots1, &dots_infos1);
			dots0_all = dots0;

			// Copy to visualization image
			cv::cvtColor(pattern_image0, projector_image_grey0, cv::COLOR_GRAY2BGR);
			cv::cvtColor(pattern_image1, projector_image_grey1, cv::COLOR_GRAY2BGR);

			//cv::Mat pattern_image;
			//Helper::CreateCollage(pattern_image0, pattern_image1, pattern_image);
			cv::imwrite(images_folder + R"(\pattern_image1_)" + std::to_string(image_counter) + ".png", pattern_image1);
			cv::imwrite(images_folder + R"(\pattern_image_)" + std::to_string(image_counter) + ".png", pattern_image0);

			if(dots0.empty() && dots1.empty())
			{
				cv::Mat collage;
				cv::putText(projector_image, "NOT ENOUGH DOTS", cv::Point(100, 100), cv::FONT_HERSHEY_SIMPLEX, 2.0, Constants::DARK_BLUE, 3);
				Helper::CreateCollageImage3And1(collage,
				                                cv::Mat::zeros(1, 1, CV_8UC3),
				                                projector_image_grey0,
				                                projector_image_grey1,
				                                projector_image);

				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No dots in at least one camera";
				continue;
			}

			Dots dots0_filtered, dots1_filtered;
			DotInfos dots0_filtered_infos, dots1_filtered_infos;

			// if there are more dots visible than bright pattern beams this implies that we also see dark beams.
			// Then we can try to distinguish both groups by analysis of relative dots brightness
			// and thus identify the bright beams.
			if((projector.GetCalibrationStep() == 1 || projector.GetCalibrationStep() == 2 || projector.GetCalibrationStep() == 3) &&
				(initial_exposure_test_frame < Constants::PROJECTOR_CALIBRATION_INITIAL_EXPOSURE_TEST_FRAMES ||
					dots0_all.size() > Constants::PROJECTOR_CALIBRATION_MIN_NUMBER_OF_DOTS))
			{
				// Get only bright points
				std::vector<size_t> dots0_masked(dots0.size());
				std::vector<size_t> dots1_masked(dots1.size());

				for(auto i = 0; i < dots0.size(); ++i)
				{
					dots0_masked[i] = i;
				}

				for(auto i = 0; i < dots1.size(); ++i)
				{
					dots1_masked[i] = i;
				}

				// Analyze dot intensities and find bright dots
				std::vector<size_t> dots0_idx_bright, dots1_idx_bright;
				auto found_dots0_bright_idx = ProjectorCalibrationHelper::GetBrightLaserDotIndices(dots_infos0, dots0_masked, dots0_idx_bright);
				auto found_dots1_bright_idx = ProjectorCalibrationHelper::GetBrightLaserDotIndices(dots_infos1, dots1_masked, dots1_idx_bright);

				if(found_dots0_bright_idx && found_dots1_bright_idx)
				{
					// All good, use points
					for(auto dots0_idx = 0; dots0_idx < dots0_idx_bright.size(); ++dots0_idx)
					{
						dots0_filtered.push_back(dots0[dots0_idx_bright[dots0_idx]]);
						dots0_filtered_infos.push_back(dots_infos0[dots0_idx_bright[dots0_idx]]);
					}

					for(auto dots1_idx = 0; dots1_idx < dots1_idx_bright.size(); ++dots1_idx)
					{
						dots1_filtered.push_back(dots1[dots1_idx_bright[dots1_idx]]);
						dots1_filtered_infos.push_back(dots_infos1[dots1_idx_bright[dots1_idx]]);
					}
				}
				else
				{
					// Not enough points found or separation of bright/dark points not possible
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not enough points found or separation of bright/dark points not possible " <<
 Constants::CHECK_ME;
					continue;
				}
			}
			else
			{
				dots0_filtered       = dots0;
				dots0_filtered_infos = dots_infos0;

				dots1_filtered       = dots1;
				dots1_filtered_infos = dots_infos1;
			}

			// Draw dots to IR frame
			for(const auto& dot0_filtered : dots0_filtered)
			{
				circle(projector_image_grey0, cv::Point2f(dot0_filtered), 10, Constants::RED, 1, CV_AA);
			}

			for(const auto& dot1_filtered : dots1_filtered)
			{
				circle(projector_image_grey1, cv::Point2f(dot1_filtered), 10, Constants::RED, 1, CV_AA);
			}

			if(dots0_filtered.empty())
			{
				continue;
			}

#pragma endregion detectAndFilter2Ddots

#pragma region calculate3Ddata

			// Calculate intersection Points of IR0 beams with plane detected by color camera
			std::vector<cv::Point3f> intersection_points_ir0_plane;
			//cv::imwrite(images_dir + R"(\debug_projector_image0_)" + std::to_string(image_counter) + ".png", projector_image0);

			dmvs.capture_calibration.GetCalibration(CHANNEL_GREY0)->GetPlaneIntersectionPoints(dots0_filtered,
			                                                                                   plane_position,
			                                                                                   plane_normal,
			                                                                                   intersection_points_ir0_plane);

			if(output_ir0plane.is_open())
			{
				for(auto intersection_point_ir0_plane : intersection_points_ir0_plane)
				{
					output_ir0plane << intersection_point_ir0_plane.x << " "
						<< intersection_point_ir0_plane.y << " "
						<< intersection_point_ir0_plane.z << '\n';
				}
			}

			// calculate the projections of intersection Points into IR1 image and draw them
			std::vector<cv::Point2f> ir0_points_projected_to_ir1;
			dmvs.capture_calibration.GetCalibration(CHANNEL_GREY1)->ProjectPoints(intersection_points_ir0_plane, ir0_points_projected_to_ir1);
			//auto debug_image = projector_image_grey1.clone();

			//for(auto& ir0_point_projected_to_ir1 : ir0_points_projected_to_ir1)
			//{
			//	cv::circle(debug_image, ir0_point_projected_to_ir1, 5, Constants::BRIGHT_GREEN);
			//}

			//cv::imshow("Debug Image", debug_image);
			//cv::imwrite(images_folder + R"(\debug_image_)" + std::to_string(image_counter) + ".png", debug_image);

			// Find correspondences by distance of projection of intersection IR0 and IR1 measurements
			Dots correspondences0, correspondences1;

			// Create cv::Mat with projected intersection points for neighborhood search
			pointVec projected_pts_mat(ir0_points_projected_to_ir1.size());

			for(auto idx = 0; idx < ir0_points_projected_to_ir1.size(); ++idx)
			{
				point_t temp              = {ir0_points_projected_to_ir1[idx].x, ir0_points_projected_to_ir1[idx].y};
				projected_pts_mat.at(idx) = temp;
			}

			if(dots0_filtered.empty() || dots1_filtered.empty())
			{
				continue;
			}

			// Create KD tree for neighborhood search
			point_t q(2);
			auto radius = 7.0f;
			KDTree kd_tree(projected_pts_mat);
			std::vector<Projector::DotInfoType> reconstructed_dot_infos;

			for(size_t dot1_idx = 0; dot1_idx < dots1_filtered.size(); ++dot1_idx)
			{
				q[0] = double(dots1_filtered[dot1_idx].x);
				q[1] = double(dots1_filtered[dot1_idx].y);

				auto nearest_neighbor_index = kd_tree.neighborhood_indices(q, radius);

				if(nearest_neighbor_index.size() == 1)
				{
					correspondences0.push_back(dots0_filtered[nearest_neighbor_index[0]]);
					correspondences1.push_back(dots1_filtered[dot1_idx]);

					Projector::DotInfoType dot_info_type{};
					dot_info_type.beam_idx   = -1;
					dot_info_type.brightness = (dots0_filtered_infos[nearest_neighbor_index[0]][4] + dots1_filtered_infos[dot1_idx][4]) / 2.0f;
					dot_info_type.dia        = 1.0;
					reconstructed_dot_infos.push_back(dot_info_type);
				}
				else
				{
					if(!nearest_neighbor_index.empty())
					{
						//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Found neighbors " << nearest_neighbor_index.size();
					}
				}
			}

			// Reconstruct 3d points from correspondences above
			if(correspondences0.empty())
			{
				// create and update user feedback image
				cv::Mat collage2;
				cv::putText(projector_image,
				            "NO CORRESPONDENCES",
				            cv::Point(100, 100),
				            cv::FONT_HERSHEY_SIMPLEX,
				            2.0,
				            Constants::DARK_BLUE,
				            3);

				Helper::CreateCollageImage3And1(collage2,
				                                cv::Mat::zeros(1, 1, CV_8UC3),
				                                projector_image_grey0,
				                                projector_image_grey1,
				                                projector_image);

				continue;
			}

			auto ir0_sensor = dmvs.capture_calibration.GetCalibration(CHANNEL_GREY0);
			auto ir1_sensor = dmvs.capture_calibration.GetCalibration(CHANNEL_GREY1);

			// calculate 3d points
			std::vector<cv::Vec3d> reconstructed_3d_points;
			ir0_sensor->StereoIntersect(ir1_sensor, correspondences0, correspondences1, reconstructed_3d_points, nullptr, false);

			// Draw reconstructed 3d point projections in IR Images
			for(size_t idx = 0; idx < reconstructed_3d_points.size(); ++idx)
			{
				cv::circle(projector_image_grey0, cv::Point2f(correspondences0[idx]), 15, Constants::BRIGHT_GREEN);
				cv::circle(projector_image_grey1, cv::Point2f(correspondences1[idx]), 15, Constants::BRIGHT_GREEN);
			}

			cv::imwrite(images_folder + R"(\projector_image0_)" + std::to_string(image_counter) + ".png", projector_image_grey0);
			cv::imwrite(images_folder + R"(\projector_image1_)" + std::to_string(image_counter) + ".png", projector_image_grey1);

			std::vector<cv::Vec3d> reconstructed_3d_point_camera_cs;


			if(output_reconstructed_3d_points.is_open())
			{
				for(auto reconstructed_3d_point : reconstructed_3d_points)
				{
					output_reconstructed_3d_points << reconstructed_3d_point[0] << " "
						<< reconstructed_3d_point[1] << " "
						<< reconstructed_3d_point[2] << '\n';
				}
			}

			// Bring back the reconstructed points back to left camera coordinate - not required
			for(auto reconstructed_3d_point : reconstructed_3d_points)
			{
				cv::Mat cv_rotation_matrix, cv_translation_vector;
				ir0_sensor->GetExtrinsicParameters(cv_rotation_matrix, cv_translation_vector);
				Eigen::Matrix3d eigen_rotation;
				cv::cv2eigen(cv_rotation_matrix, eigen_rotation);

				const Eigen::Map<Eigen::Vector3d> translation_projection(reinterpret_cast<double*>(cv_translation_vector.data));
				const Eigen::Vector3d p(reconstructed_3d_point[0], reconstructed_3d_point[1], reconstructed_3d_point[2]);
				const Eigen::Vector3d projection_new = eigen_rotation * p + translation_projection;
				reconstructed_3d_point_camera_cs.emplace_back(cv::Vec3d(projection_new[0], projection_new[1], projection_new[2]));
			}

			// Bring back the reconstructed points back to left camera coordinate - not required
			// check distance to detected plane
			std::vector<cv::Point3f> reconstructed_3d_points_near_plane;
			std::vector<Projector::DotInfoType> reconstructed_3d_point_infos_near_plane;

			for(size_t idx = 0; idx < reconstructed_3d_point_camera_cs.size(); ++idx)
			{
				Eigen::Vector3d point(reconstructed_3d_point_camera_cs[idx][0],
				                      reconstructed_3d_point_camera_cs[idx][1],
				                      reconstructed_3d_point_camera_cs[idx][2]);

				// Max distance to board
				auto max_distance_to_board = std::max(distance_hesse * distance_hesse * 0.01, 0.01);

				// Calculate point to plane distance with Hesse Normal Form
				auto point_to_plane_distance = fabs(fabs(eigen_plane_normal.dot(point)) - distance_hesse);

				// Ignore points that are further than 1 cm away from the plane
				if(fabs(point_to_plane_distance) < max_distance_to_board)
				{
					reconstructed_3d_points_near_plane.emplace_back(float(reconstructed_3d_point_camera_cs[idx][0]),
					                                                float(reconstructed_3d_point_camera_cs[idx][1]),
					                                                float(reconstructed_3d_point_camera_cs[idx][2]));

					reconstructed_3d_point_infos_near_plane.push_back(reconstructed_dot_infos[idx]);
				}
			}

#pragma endregion calculate3Ddata

			projector.Add3dPoints(reconstructed_3d_points_near_plane, reconstructed_3d_point_infos_near_plane);

			if(output.is_open())
			{
				for(size_t i = 0; i < reconstructed_3d_points_near_plane.size(); i++)
				{
					output << reconstructed_3d_point_camera_cs[i][0] << " "
						<< reconstructed_3d_point_camera_cs[i][1] << " "
						<< reconstructed_3d_point_camera_cs[i][2] << '\n';
				}
			}

#pragma region loopControlFlow
			// Update status message
			size_t min_bright_beams = 350;
			auto min_beam_length    = 0.20;

			if(projector.GetCalibrationStep() == 1)
			{
				successful_frames_per_position++;
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Detecting bright points to calculate projector position. Min depth difference = "
					<< min_beam_length << " m. " << projector.GetStep1GreenPointsCount() << " of " << min_bright_beams << " points found.";

				if(successful_frames_per_position == Constants::FRAMES_PER_POSITION)
				{
					if(dmvs.IsLive())
					{
						Helper::JogForward(linear_axis, Constants::LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION);
					}
					successful_frames_per_position = 0;
				}
			}
			else if(projector.GetCalibrationStep() == 2)
			{
				successful_frames_per_position++;

				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Identifying central dot to calculate projector rotation. Min depth difference = " <<
					min_beam_length << "m. " << projector.GetStep1GreenPointsCount() << " of 1125 points found.";

				if(successful_frames_per_position == Constants::FRAMES_PER_POSITION)
				{
					if(dmvs.IsLive())
					{
						Helper::JogForward(linear_axis, Constants::LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION);
					}
					successful_frames_per_position = 0;
				}
			}
			else if(projector.GetCalibrationStep() == 3)
			{
				successful_frames_per_position++;

				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Matching DOE pattern to dots. Min depth difference = " << min_beam_length << "m. " <<
					projector.GetStep1GreenPointsCount() << " of 1125 points ";

				if(successful_frames_per_position == Constants::FRAMES_PER_POSITION)
				{
					if(dmvs.IsLive())
					{
						Helper::JogForward(linear_axis, Constants::LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION);
					}
					successful_frames_per_position = 0;
				}
			}
			else if(projector.GetCalibrationStep() == 4)
			{
				if(!initialized_step4)
				{
					if(dmvs.IsLive())
					{
						linear_axis->MoveXAxis(0.2);
						linear_axis->WaitForMovementToFinish();
					}

					initialized_step4 = true;
				}

				successful_frames_per_position++;

				if(successful_frames_per_position == Constants::FRAMES_PER_POSITION)
				{
					if(dmvs.IsLive())
					{
						Helper::JogForward(linear_axis, Constants::LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION);
					}
					successful_frames_per_position = 0;
				}

				if(projector.GetStep4PointsCount() < projector.GetMinDotNumberToFinish())
				{
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Pie -> " << projector.GetStep4PointsCount() << " of "
						<< projector.GetMinDotNumberToFinish() << " minimum points found.";
				}
				else
				{
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Pie -> " << projector.GetStep4PointsCount() << " of " << 11665
						<< " points found.";

					//if(dmvs.use_frame)
					projector.ForceNextCalibrationStep();
					dmvs.stop = true;
				}
			}

			//std::shared_ptr<CameraSensor> projector_calibration_result;

			if(!is_projector_calibrated && projector.GetStep4PointsCount() > projector.GetMinDotNumberToFinish())
			{
				is_projector_calibrated = true;
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  Calibration done";
				//dmvs.capture_calibration.SetCalibration(CHANNEL_PROJECTOR0, projector_calibration_result);
			}

			// Create and update user feedback image
			cv::Mat collage;
			Helper::CreateCollageImage3And1(collage,
			                                cv::Mat::zeros(1, 1, CV_8UC3),
			                                projector_image_grey0,
			                                projector_image_grey1,
			                                projector_image);

			cv::imwrite(images_folder + R"(\collage_)" + std::to_string(image_counter) + ".png", collage);

			image_counter++;
#pragma endregion loopControlFlow
		} // end of capture loop

		output_ir0plane.close();
		output_reconstructed_3d_points.close();
		output.close();

		projector.ForceNextCalibrationStep();
		projector.CheckCalibrationStep();

		////calibration_cache2.Put(0, "nframes", std::to_string(frame_index_sfm + 1));
		////calibration_cache2.Close();
		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  dmvs.m_stop " << dmvs.m_stop;
		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  is_projector_calibrated " << is_projector_calibrated;

		if(dmvs.stop && !is_projector_calibrated || !is_projector_calibrated)
		{
			return CALIBRATION_STOPPED_BY_USER;
		}

#pragma region createApplyAndSaveCalibration

		// Write back result to current calibration data and save
		std::shared_ptr<CameraSensor> new_projector_calibration;
		auto result = projector.GetCalibrationResult(new_projector_calibration);

		if(result == Projector::MORE_DATA)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : More data is needed";
		}

		dmvs.capture_calibration.SetCalibration(CHANNEL_PROJECTOR0, new_projector_calibration);

		cv::Mat laser_dots = cv::Mat::zeros(4, 11165, CV_64F);
		projector.GetLaserDotMatrix(laser_dots);
		dmvs.capture_calibration.SetCustomMatrix("laser_beams", laser_dots);
		calibration_file_parser.SetCustomMatrices(laser_dots);

		// transform Coordinate system to projector
		cv::Mat rotation_matrix, translation_matrix;
		new_projector_calibration->GetExtrinsicParameters(rotation_matrix, translation_matrix);
		calibration_file_parser.SetRotationMatrix(31, rotation_matrix);
		calibration_file_parser.SetTranslationVector(31, translation_matrix);

		// Why this is being done!!!!!!!!!!!!!!
		// First we move to the projector coordinate system
		dmvs.capture_calibration.MoveToCoordinateSystemAdjusted(rotation_matrix, translation_matrix);

		// Save here
		calibration_file_parser.SetRotationMatrix(31, rotation_matrix);
		calibration_file_parser.SetTranslationVector(31, translation_matrix);
		calibration_file_parser.SaveCalibrationFile(false, dmvs.serial_number + "_ProjectorCalibration2");

		//dmvs.CalculateNewCoordinateSystem(rotation_matrix, translation_matrix, dmvs.capture_calibration);

		// Move to the final system
		//dmvs.capture_calibration.MoveToCoordinateSystemAdjusted(rotation_matrix, translation_matrix);
		//calibration_file_parser.SetRotationMatrix(31, rotation_matrix);
		//calibration_file_parser.SetTranslationVector(31, translation_matrix);
		//calibration_file_parser.SaveCalibrationFile(false, dmvs.serial_number + "_ProjCalib2");

		// use new calibration immediately within current reconstructor
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Setting calibration for current Scene instance.";

		if(dmvs.IsLive())
		{
			if(!dmvs.reconstructor)
			{
				dmvs.InitReconstructor();
			}

			dmvs.reconstructor->SetLaserBeamsFromCalibration(laser_dots);
			dmvs.reconstructor->SetCalibrationAndPrepare(dmvs.capture_calibration);

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Moving to Test position";

			linear_axis->MoveXAxis(0.4);
			linear_axis->WaitForMovementToFinish();

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Preparing Test loop";

			dmvs.TestLoop(false);
		}

		// de-register all commands

		//// Save Json file
		//std::string calibration_folder;

		//if(file_base->GetProjectorCalibrationOutputFolder(calibration_folder) == IOMessages::IO_OK)
		//{
		//	auto projector_calibration_json_file = calibration_folder + "/ProjectorCalibrationInformation.json";
		//	CalibrationJSONWriter::WriteProjectorCalibrationJSON(projector_calibration_json_file,
		//	                                                     projector.Step4PointsNumber() >= projector.GetMinDotNumberToFinish(),
		//	                                                     projector.GetParaboloidCompensationX(),
		//	                                                     projector.GetParaboloidCompensationY(),
		//	                                                     projector.Step4PointsNumber(),
		//	                                                     dmvs.serial_number);
		//}

		//// Save to calibration results folder
		//std::string calibration_xml_output_file;
		//file_base->GetProjectorCalibrationXMLOutputFile(calibration_xml_output_file);
		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Saving calibration to " << calibration_xml_output_file;

		//if(false/*dmvs.capture_calibration1.SaveFile(calibration_xml_output_file)*/)
		//{
		//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Saved calibration to " << calibration_xml_output_file;
		//}
		//else
		//{
		//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not save calibration to " << calibration_xml_output_file;
		//}

		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Save calibrations to cameras folder ?";


#pragma endregion createApplyAndSaveCalibration
	}
	/*else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calculated RMS error was greater than " << Constants::CALIBRATION_MAX_PERMITTED_RMS_ERROR;
		return CALIBRATION_FAILED_RMS;
	}*/

	delete linear_axis;

	return exit_code;
}

int LensTestingApp(ApplicationSettings& settings)
{
	auto exit_code             = CALIBRATION_OK;
	auto configuration_options = std::make_shared<ConfigurationContainer>();
	DMVSSensor dmvs;

	if(settings.record && dmvs.SetUpSensor(configuration_options) != CALIBRATION_SENSOR_SETUP_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration Sensor setup failed ";
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Trying to process the images offline";
		settings.record = false;
	}

	auto camera_path = Helper::GetCamerasDirectory();
	fs::path calibration_file_path;
	if(fs::is_directory(camera_path))
	{
		auto filename         = settings.record ? dmvs.GetCalibrationFilename() : "dmvs_calibration_template.xml";
		calibration_file_path = camera_path / fs::path(filename);
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Directory " << camera_path << " does not exist";
		return CALIBRATION_CAMERA_CALIBRATION_TEMPLATE_FILE_MISSING;
	}

	if(!fs::exists(calibration_file_path))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Default calibration file " << calibration_file_path << " does not exist";
		return CALIBRATION_CAMERA_CALIBRATION_TEMPLATE_FILE_MISSING;
	}

	CalibrationXMLParser calibration_file_parser(calibration_file_path);

	if(settings.record)
	{
		settings.storage_path = settings.storage_path + "\\" + dmvs.serial_number + "\\" + Helper::GetTimeStamp();
	}

	if(settings.record && !fs::exists(settings.storage_path))
	{
		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Created folder for storing files ";
		fs::create_directories(settings.storage_path);
	}

	IKinematics* linear_axis = nullptr;
	if(settings.record && !SetUpLinearAxis(linear_axis))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Linear Axis setup failed ";
		return CALIBRATION_NO_LINEAR_AXIS_FOUND;
	}

	if(settings.record && !linear_axis)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Linear Axis cannot be used ";
		return CALIBRATION_NO_LINEAR_AXIS_FOUND;
	}

	const std::string serial_number = settings.record ? dmvs.GetConnectedDMVS()->GetSerialNumber() : "DMVS_DMVS011900000";
	const auto file_base            = CalibrationFileBase::GetFileBase(serial_number, configuration_options);

	if(!file_base)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : File base creation failed ";
		return IOMessages::IO_UNDEFINED;
	}

	std::string fsv_file_name;
	file_base->GetCameraCalibrationFSVOutputFile(fsv_file_name);
	auto calibration_cache = std::make_shared<CalibrationCache>();
	SetUpCalibrationCache(configuration_options, serial_number, fsv_file_name, calibration_cache);

	// Add empty images for grey0, grey1 channel
	ChannelImageMap channel_image_map, channel_image_map_copy;
	channel_image_map.channel_image_map[CHANNEL_GREY0] = cv::Mat();
	channel_image_map.channel_image_map[CHANNEL_GREY1] = cv::Mat();

	BlobDetector blob_detector;
	std::vector<std::shared_ptr<BoardDetector>> boards;
	if(!BoardDetector::LoadDetectorBoards(file_base, boards, configuration_options))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration board loading failed ";
		return CALIBRATION_BOARD_NOT_LOADED;
	}

	// Collect all marker detections called observations: (frame/board/marker/camera/coordinates)
	// Data structure to keep the images of all channels in all frames
	// e.g. all_images.frame[frame_id].channel[CHANNEL_GREY0] = cv::Mat()
	Frame frame_images{};

	// Flag for indicating if the calibration is done
	auto is_data_acquisition_complete = false;

	// Counter for frame per position
	auto position_idx = 0;

	// Index of the current frame
	auto frame_index = 0;

	if(settings.record)
	{
		ChangeLedPower(dmvs, position_idx);
	}

	std::vector<std::shared_ptr<Observation>> observations;
	std::map<size_t, double> linear_axis_positions;
	std::vector<cv::Mat> recorded_images, laser_images;
	std::map<size_t, std::vector<cv::Mat>> recorded_images_map;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finding Observations " << Constants::LOG_START;

	while(!is_data_acquisition_complete)
	{
		if(settings.record)
		{
			SetPositionBasedExposureTime(dmvs, position_idx);

			if(!dmvs.PushChannelImageToCache())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration image grabbing failed ";
				return CALIBRATION_IMAGE_GRABBING_ERROR;
			}

			// Retrieve images of all Freestyle channels
			if(RetrieveChannelImages(dmvs, channel_image_map, frame_index) != CALIBRATION_IMAGE_RETRIEVED)
			{
				BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << Constants::LOG_NO_IMAGE_RETRIEVED;
				continue;
			}

			CopyChannelImageMap(channel_image_map, channel_image_map_copy);
			Helper::CopyImages(channel_image_map_copy, frame_images, position_idx);

			std::vector<ImageChannelCircularMarkerVectorMap> found_circular_markers_map;
			found_circular_markers_map.reserve(Constants::FRAMES_PER_POSITION);
			ChannelImageVectorMap image_vector_channel_map;

			FrameProcessing(dmvs,
			                channel_image_map,
			                blob_detector,
			                found_circular_markers_map,
			                image_vector_channel_map,
			                frame_index,
			                position_idx,
			                settings);

			PushImagesToCalibrationCache(calibration_cache, channel_image_map, position_idx);
			std::map<ImageChannel, std::vector<CircularMarkerPair>> unsaturated_markers_map;
			RemoveSaturatedMarkers(found_circular_markers_map, unsaturated_markers_map);
			DrawMarkers(image_vector_channel_map, unsaturated_markers_map);
			std::map<ImageChannel, CircularMarkerVector> markers_combined;
			CombineMarkers(unsaturated_markers_map, markers_combined);
			auto coded_marker_count = Helper::AddCodedMarkers(boards, markers_combined, position_idx, observations, channel_image_map_copy);

			if(coded_marker_count < Constants::MIN_COUNT_CODED_MARKER)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not enough coded markers found, trying again. Found only [" << coded_marker_count <<
					"] marker";
				continue;
			}

			Helper::AddUncodedMarkers(markers_combined, observations, position_idx);
			CreateAndShowCollage(channel_image_map_copy, position_idx);

			position_idx++;

			ChangeLedPower(dmvs, position_idx);

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Frame Count [" << position_idx << "] Number of Calibration Positions [" << Constants::
				NUMBER_CALIBRATION_POSITIONS << "]";

			is_data_acquisition_complete = position_idx >= int(Constants::NUMBER_CALIBRATION_POSITIONS);

			if(settings.record && dmvs.IsLive())
			{
				if(!dmvs.Stop())
				{
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Stop acquisition error ";
				}

				Helper::Sleeping(1, " while dmvs stops acquisition");
				auto current_position = linear_axis->GetPosition()->at<double>(0, 3);
				//auto const current_position_value = current_position->at<double>(0, 3);
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Current Position [" << current_position << "] Frame Index [" << frame_index <<
					"] Position Idx [" << position_idx << "]";

				// Save the actual position from the linear axis
				linear_axis_positions[position_idx - 1] = 0.6 - current_position;

				if(!is_data_acquisition_complete)
				{
					auto next_position_value = current_position - Constants::LINEAR_STAGE_STEPPING;
					linear_axis->MoveXAxis(next_position_value);
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Moving to next Position [" << next_position_value << "]";
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Frame Count [" << position_idx << "] Calibration Position Count [" <<
						Constants::NUMBER_CALIBRATION_POSITIONS << "]";

					// Check if the image is same or different what is getting store in the cache and what is being read
					linear_axis->WaitForMovementToFinish();
				}
			}
		}
		else
		{
			LoadRecordedImages(settings.storage_path, recorded_images_map);

			// Add empty images for grey0, grey1 channel
			ChannelImageMap channel_image_map_offline, channel_image_map_copy_offline;
			for(auto index = 0; index < recorded_images_map.size(); index++)
				//for(auto index = 0; index < 4; index++)
			{
				ImageChannelCircularMarkerVectorMap circular_markers_in_current_frame, markers_combined;
				std::vector<ImageChannelCircularMarkerVectorMap> found_circular_markers_map;

				for(const auto& recorded_image : recorded_images_map[index])
				{
					// Split the image in two parts
					channel_image_map_offline.channel_image_map[CHANNEL_GREY0] = recorded_image(
						cv::Rect(0, 0, Constants::DMVS_IMAGE_WIDTH, Constants::DMVS_IMAGE_HEIGHT));
					channel_image_map_offline.channel_image_map[CHANNEL_GREY1] = recorded_image(
						cv::Rect(Constants::DMVS_IMAGE_WIDTH, 0, Constants::DMVS_IMAGE_WIDTH, Constants::DMVS_IMAGE_HEIGHT));

					Helper::DetectMarkers(channel_image_map_offline, blob_detector, circular_markers_in_current_frame);
					Helper::RemoveLowQualityMarker(circular_markers_in_current_frame, channel_image_map_offline);
					found_circular_markers_map.push_back(circular_markers_in_current_frame);
				}

				auto& current_image = recorded_images_map[index][0];
				CopyChannelImageMap(channel_image_map_offline, channel_image_map_copy_offline);
				Helper::CopyImages(channel_image_map_copy_offline, frame_images, index);
				PushImagesToCalibrationCache(calibration_cache, channel_image_map_offline, index);

				std::map<ImageChannel, std::vector<CircularMarkerPair>> unsaturated_markers_map;
				RemoveSaturatedMarkers(found_circular_markers_map, unsaturated_markers_map);

				//DrawMarkers(channel_image_vector_map, unsaturated_markers_map);
				CombineMarkers(unsaturated_markers_map, markers_combined);
				auto coded_marker_count = Helper::AddCodedMarkers(boards, markers_combined, index, observations, channel_image_map_copy_offline);

				if(coded_marker_count < 3)
				{
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not enough coded markers found, trying again. Found only [" << coded_marker_count
						<< "] coded markers";
					continue;
				}

				Helper::AddUncodedMarkers(markers_combined, observations, index);
				//CreateAndShowCollage(channel_image_map_copy_offline, index);
			}

			is_data_acquisition_complete = true;
		}
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finding Observations " << Constants::LOG_END;

	if(!dmvs.Stop() && settings.record)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Stop Acquisition error ";
	}

	std::string filename = "\\observations";

	filename += settings.record ? "_online_" : "_offline_";
	std::ofstream observations_stream;
	auto paired_observations_path = settings.storage_path + filename + Helper::GetTimeStamp() + ".txt";
	observations_stream.open(paired_observations_path);

	for(auto& observation : observations)
	{
		observations_stream << observation->GetMarkerId() << " " << observation->GetFrameId() << " " << observation->GetImageChannel() << " " <<
			observation->GetBoardId() << " " << observation->GetRawEllipseCenter().x << " " << observation->GetRawEllipseCenter().y << "\n";
	}

	observations_stream.close();

	CalibrationCalculator calibration_calculator(observations);

	// Write stepping to position file
	std::string linear_table_position_file;
	file_base->GetCameraCalibrationLinearStagePositionOutputFile(linear_table_position_file);
	for(auto i = 0; i < Constants::NUMBER_CALIBRATION_POSITIONS; ++i)
		//for(auto i = 0; i < 4; ++i)
	{
		linear_axis_positions[i] = 0.03 * i;
	}

	std::shared_ptr<CameraSystemTransformation> linear_table_frame_transformation = std::make_shared<LinearTableFramePosition>(linear_axis_positions);
	calibration_calculator.SetCameraSystemTransformation(linear_table_frame_transformation);
	//calibration_calculator.InitializeCameraSystemTransformation(recorded_images.size());
	std::set<size_t> detected_boards_set;

	std::vector<std::shared_ptr<BoardDetector>> detected_boards;
	if(!GetDetectedBoard(observations, boards, detected_boards))
	{
		return CALIBRATION_BOARD_NOT_DETECTED;
	}

	// Set used calibration boards
	calibration_calculator.SetBoards(detected_boards);
	std::map<size_t, std::shared_ptr<const CameraSensor>> camera_sensor_map;
	InitializeCameraSensorMapInHouse(camera_sensor_map, settings);

	// Why is this done for only one CHANNEL_GREY0
	calibration_calculator.SetRelativeFixedCamera(camera_sensor_map, CHANNEL_GREY0);

	auto res = calibration_calculator.ProcessObservationsInHouse();
	{
		auto left_camera_calibration = std::dynamic_pointer_cast<CameraSensorDepthDependent>(
			calibration_calculator.GetRelativeFixedCamerasParameterOptimizerLens()->GetCalibration(CHANNEL_GREY0));
		auto right_camera_calibration = std::dynamic_pointer_cast<CameraSensorDepthDependent>(
			calibration_calculator.GetRelativeFixedCamerasParameterOptimizerLens()->GetCalibration(CHANNEL_GREY1));

		if(left_camera_calibration && right_camera_calibration)
		{
			// get a Freestyle default calibration and update with new Freestyle calibration data as calculated
			cv::Mat rotation_matrix_20, translation_vector_20, camera_matrix_20, distortion_vector_20;
			calibration_file_parser.SetDepthDependentScalarParameters(CHANNEL_GREY0,
			                                                          left_camera_calibration->GetCameraInfo().intrinsics.depth_parameters.scalar);
			calibration_file_parser.SetDepthDependentExponentialParameters(CHANNEL_GREY0,
			                                                               left_camera_calibration->GetCameraInfo().intrinsics.depth_parameters.
			                                                               exponential);

			left_camera_calibration->GetExtrinsicParameters(rotation_matrix_20, translation_vector_20);
			left_camera_calibration->GetCameraIntrinsicParams(camera_matrix_20, distortion_vector_20);
			calibration_file_parser.SetRotationMatrix(20, rotation_matrix_20);
			calibration_file_parser.SetTranslationVector(20, translation_vector_20);
			calibration_file_parser.SetCameraMatrix(20, camera_matrix_20);
			calibration_file_parser.SetDistortionVector(20, distortion_vector_20);

			cv::Mat rotation_matrix_21, translation_vector_21, camera_matrix_21, distortion_vector_21;
			calibration_file_parser.SetDepthDependentScalarParameters(CHANNEL_GREY1,
			                                                          right_camera_calibration->GetCameraInfo().intrinsics.depth_parameters.scalar);
			calibration_file_parser.SetDepthDependentExponentialParameters(CHANNEL_GREY1,
			                                                               right_camera_calibration->GetCameraInfo().intrinsics.depth_parameters.
			                                                               exponential);
			right_camera_calibration->GetExtrinsicParameters(rotation_matrix_21, translation_vector_21);
			right_camera_calibration->GetCameraIntrinsicParams(camera_matrix_21, distortion_vector_21);
			calibration_file_parser.SetRotationMatrix(21, rotation_matrix_21);
			calibration_file_parser.SetTranslationVector(21, translation_vector_21);
			calibration_file_parser.SetCameraMatrix(21, camera_matrix_21);
			calibration_file_parser.SetDistortionVector(21, distortion_vector_21);
			calibration_file_parser.SaveCalibrationFile();

			CalculateStatistics(file_base, frame_images, calibration_calculator);
		}
	}
	/*else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calculated RMS error was greater than " << Constants::CALIBRATION_MAX_PERMITTED_RMS_ERROR;
		return CALIBRATION_FAILED_RMS;
	}*/

	delete linear_axis;

	return exit_code;
}

int main(int ac,
         char* av[])
{
	ApplicationSettings settings(ac, av);

	if(settings.application_name == "FactoryCalibration")
	{
		return FactoryCalibration(settings);
	}

	if(settings.application_name == "InfieldCompensation")
	{
		return InfieldCompensation(settings);
	}

	if(settings.application_name == "LensTestingApp")
	{
		return LensTestingApp(settings);
	}

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Please check application name in config file. You have entered [" << settings.application_name
 << "] . You should use FactoryCalibration, InfieldCompensation or LensTestingApp ";
}
