#include "calibration_file_base.h"
#include "configuration_container.h"
#include "helper.h"

using namespace DMVS::CameraCalibration;

CalibrationFileBase::~CalibrationFileBase()
= default;

std::shared_ptr<CalibrationFileBase> CalibrationFileBase::GetFileBase(const std::string& serial_number,
                                                                      const std::shared_ptr<ConfigurationContainer> options)
{
	auto file_base                     = std::make_shared<CalibrationFileBase>();
	const auto configuration_file_path = options->GetConfigurationDataFile();

	if(IOMessages::IO_OK != file_base->Init(serial_number, configuration_file_path))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not initialize or read or create configuration file in folder : " << configuration_file_path;
		return nullptr;
	}

	return file_base;
}

std::string ToString(const std::wstring& input)
{
	return Helper::Narrow(input.c_str());
}

IOMessages::IOResult CalibrationFileBase::Init(const std::string& serial_number,
                                               const fs::path& config_file_path)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Configuration file path : " << config_file_path;

	// We need the initializations below for calibration only.
	if(!config_file_path.empty())
	{
		config_file_path_ = std::make_shared<fs::path>(config_file_path);

		// Check if configFile exists. If not, create default config file.
		if(!config_file_path_ || !is_regular_file(*config_file_path_))
		{
			return CreateDefaultConfigFile();
		}

		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Configuration file being read.";
		//return ReadConfigFile();
	}

	// Create file name for fsv recording
	// C run-time time (defined in <time.h>)
	time_t os_binary_time;

	// Get the current time from the
	time(&os_binary_time);

	const CTime ct_time(os_binary_time);
	const std::wstring time_stamp = static_cast<LPCTSTR>(ct_time.Format(_T("%Y-%m-%d_%H-%M-%S")));
	const auto time_stamp_string  = ToString(time_stamp);

	serial_number_               = serial_number.empty() ? "Temp" : serial_number;
	root_path_                   = std::make_shared<fs::path>(Constants::ROOT_DEFAULT_PATH);
	input_path_                  = std::make_shared<fs::path>(root_path_->append(Constants::INPUT_FOLDER));
	root_path_calibration_       = std::make_shared<fs::path>(root_path_->append(serial_number_));
	camera_calibration_path_     = std::make_shared<fs::path>(root_path_calibration_->append(time_stamp_string + " CameraCalibration"));
	projection_calibration_path_ = std::make_shared<fs::path>(root_path_calibration_->append(time_stamp_string + " ProjectorCalibration"));
	fqc_path_                    = std::make_shared<fs::path>(root_path_calibration_->append(time_stamp_string + " FQC"));
	infield_calibration_path_    = std::make_shared<fs::path>(
		Helper::GetCalibrationsDirectory().append(serial_number_ + time_stamp_string + " InfieldCalibration"));

	return IOMessages::IO_OK;
}

std::shared_ptr<const fs::path> CalibrationFileBase::GetRootPath() const
{
	return root_path_;
}

IOMessages::IOResult CalibrationFileBase::CreateFolderIfNotExist(const std::shared_ptr<fs::path>& file) const
{
	return CreateFolderIfNotExist(file ? std::make_shared<const fs::path>(file->parent_path()) : nullptr);
}

IOMessages::IOResult CalibrationFileBase::CreateFolderIfNotExist(const std::shared_ptr<const fs::path>& directory)
{
	if(!directory)
	{
		return IOMessages::IO_FALSE;
	}

	if(exists(*directory))
	{
		return IOMessages::IO_OK;
	}

	if(!fs::create_directories(*directory))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not create directory " << directory->string();
		return IOMessages::IO_FILE_NOT_FOUND;
	}

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::CreateDefaultConfigFile() const
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;

	const auto config_file_dir = config_file_path_->parent_path();

	if(!exists(config_file_dir))
	{
		// Try to create
		if(!create_directories(config_file_dir))
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Failed to create directory " << config_file_dir;
			return IOMessages::IO_FAILED_CREATE_DIRECTORY;
		}
	}

	// Directory now exists -> Create default config file
	// Calculate the new length of the header
	std::ofstream config_file_stream;
	config_file_stream.open(config_file_path_->string(), std::ios_base::out);

	//TODO
	////Use a custom attribute to root folder name
	//CustomAttribute<std::string>* rootAttr = new CustomAttribute<std::string>();
	//this->AddAttribute(rootAttr, "CalibrationDataRoot");
	//std::string defaultRootPath = "C:\\CalibrationDataFS";
	//rootAttr->Assign(defaultRootPath);

	////Use a custom attribute to network storage folder name
	//auto netStorage = new CustomAttribute<std::string>();
	//this->AddAttribute(netStorage, "CalibrationDataNetwork");
	//netStorage->Assign(defaultNetPath);

	//WriteContextBase* writeContext = new WriteContextBase;
	//this->WriteTo(configFileStream, writeContext);

	return IOMessages::IO_NOT_IMPLEMENTED;
}

IOMessages::IOResult CalibrationFileBase::ReadConfigFile() const
{
	// Read config file
	std::ifstream config_file_stream;
	config_file_stream.open(config_file_path_->string().c_str(), std::ios_base::in);

	if(!config_file_stream.is_open())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Configuration file stream is not open ";
	}

	/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Started reading configuration file : " << *config_file_path_;*/
	//const auto read_context = new ReadContextBase;
	//this->ReadFrom(config_file_stream, read_context);
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finished reading configuration file : " << *config_file_path_;
	return IOMessages::IO_NOT_IMPLEMENTED;
}

IOMessages::IOResult CalibrationFileBase::GetDefaultCameraCalibration(fs::path& filename,
                                                                      const std::string& sensor) const
{
	auto calibration_file(*input_path_);
	calibration_file.append("DMVS_default.xml");
	filename = calibration_file;

	return IOMessages::IO_OK;
}

std::shared_ptr<const fs::path> CalibrationFileBase::GetInputPath() const
{
	return input_path_;
}

IOMessages::IOResult CalibrationFileBase::GetCalibrationParametersYML(std::string& filename) const
{
	auto calibration_file(*input_path_);
	calibration_file.append(Constants::FILE_CALIBRATION_PARAMS);
	filename = calibration_file.string();

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCalibrationBoardsPath(std::string& boards_file_path) const
{
	fs::path dir = input_path_->append(Constants::FILE_REF_BOARDS);

	if(!exists(dir))
	{
		boards_file_path = "";
		return IOMessages::IO_FILE_NOT_FOUND;
	}

	boards_file_path = dir.string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCalibrationParametersTemplateYML(std::string& filename) const
{
	auto calibration_file(*input_path_);
	calibration_file.append(Constants::FILE_IN_HOUSE_BOARD_TEMPLATE);
	filename = calibration_file.string();

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationBoardTemplate(std::string& filename) const
{
	auto calibration_file(*input_path_);
	calibration_file.append(Constants::FILE_CALIBRATION_BOARD_TEMPLATE);
	filename = calibration_file.string();

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationFolder(std::string& filename) const
{
	filename = camera_calibration_path_->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationFSVOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(serial_number_ + Constants::FILE_CAMERA_CALIBRATION_FSV);

	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationLinearStagePositionOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(Constants::FILE_LINEAR_TABLE_POSITIONS);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationSequenceOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(Constants::FILE_CAMERA_CALIBRATION_SEQ);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationPDFOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(Constants::FILE_CAMERA_CALIBRATION_PDF);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationLogOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(Constants::FILE_CAMERA_CALIBRATION_LOG);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationYMLOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(serial_number_ + Constants::FILE_CAMERA_CALIBRATION_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationReportOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(Constants::FILE_CAMERA_CALIBRATION_REPORT_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationStatisticsOutputDirectory(std::shared_ptr<fs::path>& directory) const
{
	directory = std::make_shared<fs::path>(camera_calibration_path_->string());
	directory->append(Constants::FILE_STATISTICS);

	if(!exists(*directory))
		create_directories(*directory);

	if(!exists(*directory))
		return IOMessages::IO_FAILED_CREATE_DIRECTORY;

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationImageOutputDirectory(const int channel,
                                                                                   std::shared_ptr<fs::path>& directory) const
{
	const auto sub_dir_name = std::to_string(channel);
	directory               = std::make_shared<fs::path>(camera_calibration_path_->string());
	directory->append(sub_dir_name);

	if(!exists(*directory))
		create_directories(*directory);

	if(!exists(*directory))
		return IOMessages::IO_FAILED_CREATE_DIRECTORY;

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetCameraCalibrationSubDirectory(const std::string& sub_directory_name,
                                                                           fs::path& directory) const
{
	if(!exists(*camera_calibration_path_))
		create_directories(*camera_calibration_path_);

	directory = *camera_calibration_path_;
	directory.append(sub_directory_name);
	create_directories(directory);

	if(!exists(directory))
		return IOMessages::IO_FAILED_CREATE_DIRECTORY;

	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetProjectorCalibrationOutputFolder(std::string& filename) const
{
	if(!projection_calibration_path_)
		return IOMessages::IO_FILE_NOT_FOUND;

	filename = projection_calibration_path_->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetProjectorCalibrationDoeYML(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*input_path_);
	calibration_file->append(Constants::FILE_DOE_YML);
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetProjectorCalibrationFSVOutputFile(std::string& filename) const
{
	const auto calibration_file = camera_calibration_path_;
	calibration_file->append(Constants::FILE_PROJECTOR_CALIBRATION_FSV);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetProjectorCalibrationLogOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(Constants::FILE_PROJECTOR_CALIBRATION_LOG);
	CreateFolderIfNotExist(calibration_file);
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetProjectorCalibrationDataJSONFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(serial_number_ + Constants::FILE_JSON);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetProjectorCalibrationXMLOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(serial_number_ + Constants::FILE_PROJECTOR_CALIBRATION_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetProjectorCalibrationReportOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*camera_calibration_path_);
	calibration_file->append(Constants::FILE_PROJ_CALIBRATION_REPORT_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetNumCalibrationFoldersOnLocalDrive(int& folder_num) const
{
	/*std::vector<std::shared_ptr<const fs::path>> dirs = root_path_->CollectDirectories("Freestyle_*", false);
	folder_num                                         = (int)(dirs.size() - 1);*/
	return IOMessages::IO_OK;
}

//std::shared_ptr<const fs::path> CalibrationFileBase::getNetworkStoragePath(bool toQPRFolder)
//{
//	const std::string path = this->GetCustomAttrValue("CalibrationDataNetwork", defaultNetPath);
//	auto dir               = Directory::fromString(path);
//
//	if(toQPRFolder)
//		dir = dir->appendLevel("QPRs");
//
//	// Use full sensor name to create the directory name
//	dir = dir->appendLevel(m_serialNumber);
//
//	return (dir);
//}

bool FindFile(const std::shared_ptr<const fs::path> directory,
              const std::string& basename,
              std::string& fullname)
{
	if(!directory)
		return false;

	if(!exists(*directory))
		return false;

	auto file(*directory);
	file.append(basename);

	if(exists(file))
	{
		fullname = file.string();
		return true;
	}

	return false;
}

/**
 * \brief Case insensitive version of iQString::startsWith
 * \param input_string
 * \param pattern
 * \return bool
*/
static bool StartsWith(const std::string& input_string,
                       const std::string& pattern)
{
	if(pattern.empty())
		return true;

	if(input_string.empty())
		return false;

	if(input_string.length() < pattern.length())
		return false;

	return boost::iequals(pattern, input_string.substr(0, pattern.length()));
}

/**
 * \brief Gets name of a configuration file for tolerances,
 * marker positions, etc.Takes care for the sensor type.
 */
static std::string GetBaseName(const std::string& sensor_id,
                               const std::string& def,
                               const std::string& ext)
{
	auto name = def + ext;

	//std::string sensorID = seq->getCustomAttrValue("SensorID", std::string("unknown"));
	if(StartsWith(sensor_id, "FREESTYLE_0"))
		name = "FREESTYLE_0" + ext;
	else if(StartsWith(sensor_id, "FREESTYLE_1"))
		name = "FREESTYLE_1" + ext;
	else if(StartsWith(sensor_id, "FREESTYLE_9"))
		name = "FREESTYLE_9" + ext;
	else if(StartsWith(sensor_id, "FREESTYLE2_"))
		name = "FREESTYLE_2" + ext;
	else if(StartsWith(sensor_id, "Detail3D_"))
		name = "FREESTYLE_2" + ext;

	return name;
}

//
//IOMessages::IOResult CalibrationFileBase::getFQCMarkerFile(std::shared_ptr<FileDataObject> video,
//                                                           const std::string& sensorID,
//                                                           std::string& fileName)
//{
//	if(!video)
//		return IOMessages::IO_FALSE;
//	//if (!Videogrammetry::getFolder<RecordingFile>(seq))	return IOMessages::IO_FALSE;
//
//	// Get directory of video file
//	std::shared_ptr<const fs::path> videoDir = File::fromString(video->getFileInfo().fileName)->getDirectory();
//
//	// Marker file name
//	//	Depends on sensor ID
//	std::string markers = getBaseName(sensorID, "FQC markers", ".mdf");
//
//	// Search in video and default folders
//	if(findFile(videoDir, markers, fileName))
//		return IOMessages::IO_OK;
//	if(findFile(videoDir, "FQC markers.mdf", fileName))
//		return IOMessages::IO_OK;
//	if(findFile(m_inputPath, markers, fileName))
//		return IOMessages::IO_OK;
//	if(findFile(m_inputPath, "FQC markers.mdf", fileName))
//		return IOMessages::IO_OK;
//
//	return IOMessages::IO_FALSE;
//}

//IOMessages::IOResult CalibrationFileBase::getFQCPlaneFile(std::shared_ptr<FileDataObject> video,
//                                                          const std::string& sensorID,
//                                                          std::string& fileName)
//{
//	if(!video)
//		return IOMessages::IO_FALSE;
//	//if (!Videogrammetry::getFolder<RecordingFile>(seq))	return IOMessages::IO_FALSE;
//
//	// Get directory of video file
//	//std::shared_ptr<RecordingFile> video = Videogrammetry::getFolder<RecordingFile>(seq);
//	std::shared_ptr<const fs::path> videoDir = File::fromString(video->getFileInfo().fileName)->getDirectory();
//
//	// Marker file name
//	//	Depends on sensor ID
//	std::string markers = getBaseName(sensorID, "", ".fqc");
//
//	// Search in video and default folders
//	if(findFile(videoDir, markers, fileName))
//		return IOMessages::IO_OK;
//	if(findFile(m_inputPath, markers, fileName))
//		return IOMessages::IO_OK;
//
//	return IOMessages::IO_FALSE;
//}

//IOMessages::IOResult CalibrationFileBase::getFQCToleranceFile(std::shared_ptr<FileDataObject> video,
//                                                              const std::string& sensorID,
//                                                              std::string& fileName)
//{
//	if(!video)
//		return IOMessages::IO_FALSE;
//	//if (!Videogrammetry::getFolder<RecordingFile>(seq))	return IOMessages::IO_FALSE;
//
//	// Get directory of video file
//	//std::shared_ptr<RecordingFile> video = Videogrammetry::getFolder<RecordingFile>(seq);
//	std::shared_ptr<const fs::path> videoDir = File::fromString(video->getFileInfo().fileName)->getDirectory();
//
//	// Marker file name
//	//	Depends on sensor ID
//	std::string markers = getBaseName(sensorID, "", ".tol");
//
//	// Search in video and default folders
//	if(findFile(videoDir, markers, fileName))
//		return IOMessages::IO_OK;
//	if(findFile(m_inputPath, markers, fileName))
//		return IOMessages::IO_OK;
//
//	return IOMessages::IO_FALSE;
//}


IOMessages::IOResult CalibrationFileBase::GetFQCReportOutput(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*fqc_path_);
	calibration_file->append(Constants::FILE_FQC_REPORT_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetFQCLogOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*fqc_path_);
	calibration_file->append(Constants::FILE_FQC_LOG);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

std::shared_ptr<const fs::path> CalibrationFileBase::GetFQCPath() const
{
	auto temp = std::make_shared<fs::path>(*fqc_path_);
	temp->append(Constants::FILE_TMP);
	CreateFolderIfNotExist(temp);
	return fqc_path_;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationFSVOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_FSV);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationAllLogOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_ALL_LOG);

	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationBrightLogOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_BRIGHT_LOG);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationProjLogOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_PROJECTION_LOG);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationBrightXML(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_BRIGHT_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationProjXML(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_PROJECTION_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationTransShiftLogOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_TRANS_SHIFT_LOG);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationTransShiftXML(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_TRANS_SHIFT_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationAllXML(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_ALL_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationReportFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_CALIBRATION_REPORT_PDF);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationOriginalXML(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_ORIGINAL_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::GetInfieldCalibrationCompleteLogOutputFile(std::string& filename) const
{
	const auto calibration_file = std::make_shared<fs::path>(*infield_calibration_path_);
	calibration_file->append(Constants::FILE_INFIELD_CALIBRATION_COMPLETE_LOG_XML);
	RETURN_IF_NOT_OK(CreateFolderIfNotExist(calibration_file));
	filename = calibration_file->string();
	return IOMessages::IO_OK;
}

IOMessages::IOResult CalibrationFileBase::RemoveOldInFieldFolders(const int num_to_keep) const
{
	bool success = false;
	//std::shared_ptr<const fs::path> inFieldDir (DEFAULT_ROOT_IN_FIELD)->append(serial_number_);

	//if(!inFieldDir || !inFieldDir->Create())
	//	return IOMessages::IO_UNKNOWN;

	//std::vector<std::shared_ptr<const fs::path>> directories = inFieldDir->CollectDirectories();

	//if(directories.size() <= (size_t)num_to_keep)
	//	return IOMessages::IO_OK;

	//// Sort the directories
	//std::vector<std::string> paths;
	//for(auto dirIt = 0; dirIt < directories.size(); ++dirIt)
	//{
	//	paths.push_back(directories[dirIt]->string().c_str());
	//}

	//std::sort(paths.begin(), paths.end());

	//bool success = true;

	//for(auto dirIt = 0; dirIt < paths.size() - (size_t)num_to_keep; ++dirIt)
	//{
	//	if(!Directory::FromString(std::string(paths[dirIt].c_str()))->Delete(true))
	//		success = false;
	//}

	return success ? IOMessages::IO_OK : IOMessages::IO_UNKNOWN;
}

IOMessages::IOResult CalibrationFileBase::SetRootPathPermanent(const std::string& new_root_path)
{
	//this->SetCustomAttrValue(Constants::ATTRIBUTE_ROOT_FOLDER, new_root_path);

	//// Directory now exists -> Create default config file
	//// Calculate the new length of the header
	//std::ofstream config_file_stream;
	//config_file_stream.open(config_file_path_->string(), std::ios_base::out);
	//const auto write_context = new WriteContextBase;
	//auto result              = this->WriteTo(config_file_stream, write_context);
	return IOMessages::IOResult(true);
}

std::shared_ptr<const fs::path> CalibrationFileBase::GetRootPathCalibration() const
{
	return root_path_calibration_;
}
