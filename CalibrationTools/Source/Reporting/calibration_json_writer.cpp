#include "calibration_json_writer.h"
#include "helper.h"

const char* CalibrationJSONWriter::DMVS_APP_FOLDER = "/results/";

const char* CalibrationJSONWriter::PROJECTOR_CALIBRATION_FILE_NAME = "/ProjectorCalibrationInformation.json";
const char* CalibrationJSONWriter::CAMERA_CALIBRATION_FILE_NAME    = "/CameraCalibrationInformation.json";

const char* CalibrationJSONWriter::PROJECTOR_CALIBRATION_SUCCESS_STRING = "ProjectorCalibrationSuccess";
const char* CalibrationJSONWriter::PARABOLOID_RMS_X_STRING              = "ParaboloidFitRMSX";
const char* CalibrationJSONWriter::PARABOLOID_RMS_Y_STRING              = "ParaboloidFitRMSY";
const char* CalibrationJSONWriter::PROJECTOR_CALIBRATION_DATE_STRING    = "ProjectorCalibrationDate";
const char* CalibrationJSONWriter::PROJECTOR_POINTS_DETECTED_STRING     = "ProjectorPointsDetected";
const char* CalibrationJSONWriter::PROJECTOR_CALIBRATION_FOLDER_STRING  = "ProjectorCalibrationFolder";

const char* CalibrationJSONWriter::CAMERA_CALIBRATION_SUCCESS_STRING = "CameraCalibrationSuccess";
const char* CalibrationJSONWriter::RMS_LEFT_STRING                   = "OverallRMSLeft";
const char* CalibrationJSONWriter::RMS_RIGHT_STRING                  = "OverallRMSRight";
const char* CalibrationJSONWriter::TANGENTIAL_RMS_LEFT_STRING        = "TangentialRMSLeft";
const char* CalibrationJSONWriter::TANGENTIAL_RMS_RIGHT_STRING       = "TangentialRMSRight";
const char* CalibrationJSONWriter::RADIAL_RMS_LEFT_STRING            = "RadialRMSLeft";
const char* CalibrationJSONWriter::RADIAL_RMS_RIGHT_STRING           = "RadialRMSRight";
const char* CalibrationJSONWriter::CAMERA_CALIBRATION_DATE_STRING    = "CameraCalibrationDate";
const char* CalibrationJSONWriter::CAMERA_CALIBRATION_FOLDER_STRING  = "CameraCalibrationFolder";

const char* CalibrationJSONWriter::SERIAL_STRING      = "serial";
const char* CalibrationJSONWriter::TESTS_STRING       = "tests";
const char* CalibrationJSONWriter::TEST_STRING        = "test";
const char* CalibrationJSONWriter::MESSAGE_STRING     = "message";
const char* CalibrationJSONWriter::NAME_STRING        = "name";
const char* CalibrationJSONWriter::KEY_STRING         = "key";
const char* CalibrationJSONWriter::VALUE_STRING       = "value";
const char* CalibrationJSONWriter::DESCRIPTION_STRING = "description";
const char* CalibrationJSONWriter::LEVEL_STRING       = "level";
const char* CalibrationJSONWriter::STATUS_STRING      = "status";
const char* CalibrationJSONWriter::VALUES_STRING      = "values";

// Values
const char* CalibrationJSONWriter::CALIBRATION_DESCRIPTION_STRING    = "Camera Calibration results of device";
const char* CalibrationJSONWriter::CAMERA_CALIBRATION_NAME_STRING    = "Camera Calibration";
const char* CalibrationJSONWriter::CAMERA_CALIBRATION_MESSAGE_STRING = "Details values of Camera Calibration";

const char* CalibrationJSONWriter::PROJECTOR_CALIBRATION_DESCRIPTION_STRING = "Projector Calibration results of device";
const char* CalibrationJSONWriter::PROJECTOR_CALIBRATION_NAME_STRING        = "Projector Calibration";
const char* CalibrationJSONWriter::PROJECTOR_CALIBRATION_MESSAGE_STRING     = "Details values of Projector Calibration";

const char* CalibrationJSONWriter::CALIBRATION_PATH_DESCRIPTION_STRING = "Calibration folders";
const char* CalibrationJSONWriter::CAMERA_CALIBRATION_PATH_STRING      = "Camera Export Path";
const char* CalibrationJSONWriter::PROJECTOR_CALIBRATION_PATH_STRING   = "Projector Export Path";

CalibrationJSONWriter::CalibrationJSONWriter()
= default;

void CalibrationJSONWriter::Write(const std::string& calibration_folder,
                                  double overall_rms_left,
                                  double overall_rms_right,
                                  double tangential_rms_left,
                                  double tangential_rms_right,
                                  double radial_rms_left,
                                  double radial_rms_right,
                                  bool success,
                                  const std::string& serial_number)
{
	nlohmann::json json_file;
	json_file[SERIAL_STRING] = serial_number;

	auto camera_calibration_values = GenerateCameraCalibrationReport(overall_rms_left,
	                                                                 overall_rms_right,
	                                                                 tangential_rms_left,
	                                                                 tangential_rms_right,
	                                                                 radial_rms_left,
	                                                                 radial_rms_right,
	                                                                 success);

	auto calibration_folder_values = GenerateCameraCalibrationPathExport(calibration_folder);

	// Now put into array
	json_file[TESTS_STRING] = nlohmann::json::array({calibration_folder_values, camera_calibration_values});

	// Save in calibration folder
	std::ofstream file(calibration_folder + CAMERA_CALIBRATION_FILE_NAME);
	file << json_file;
	file.close();
	WriteToConsole(json_file);
	CheckAndCreateDMVSAppDirectory(serial_number);

	// Write to DMVS APP
	std::string results_file(GetCurrentAppPath() + std::string(DMVS_APP_FOLDER) + serial_number + "/" + GetTimeCurrent() + ".json");
	std::ofstream file_dmvs_app(results_file);
	file_dmvs_app << json_file;
	file_dmvs_app.close();
}

void CalibrationJSONWriter::WriteProjectorCalibrationJSON(const std::string& calibration_folder,
                                                          bool success,
                                                          double paraboloid_rms_x,
                                                          double paraboloid_rms_y,
                                                          size_t overall_points_detected,
                                                          const std::string& serial_number)
{
	nlohmann::json json_file;
	json_file[PROJECTOR_CALIBRATION_SUCCESS_STRING] = success;
	json_file[SERIAL_STRING]                        = serial_number;

	nlohmann::json projectorCalibration;
	projectorCalibration[TESTS_STRING]       = PROJECTOR_CALIBRATION_NAME_STRING;
	projectorCalibration[DESCRIPTION_STRING] = PROJECTOR_CALIBRATION_DESCRIPTION_STRING;
	nlohmann::json paraboloid_rms_x_json;
	paraboloid_rms_x_json[KEY_STRING]   = PARABOLOID_RMS_X_STRING;
	paraboloid_rms_x_json[VALUE_STRING] = std::to_string(paraboloid_rms_x);

	nlohmann::json paraboloid_rms_y_json;
	paraboloid_rms_y_json[KEY_STRING]   = PARABOLOID_RMS_Y_STRING;
	paraboloid_rms_y_json[VALUE_STRING] = std::to_string(paraboloid_rms_y);

	nlohmann::json projector_calibration_date;
	projector_calibration_date[KEY_STRING]   = PROJECTOR_POINTS_DETECTED_STRING;
	projector_calibration_date[VALUE_STRING] = std::to_string(overall_points_detected);

	nlohmann::json projector_points_detected;
	projector_points_detected[KEY_STRING]   = PROJECTOR_CALIBRATION_DATE_STRING;
	projector_points_detected[VALUE_STRING] = GetTimeCurrent();
	projectorCalibration[TEST_STRING]       = PROJECTOR_CALIBRATION_NAME_STRING;

	nlohmann::json status_details;
	status_details[LEVEL_STRING]   = success ? 1 : 3;
	status_details[NAME_STRING]    = PROJECTOR_CALIBRATION_NAME_STRING;
	status_details[MESSAGE_STRING] = PROJECTOR_CALIBRATION_MESSAGE_STRING;
	status_details[VALUES_STRING]  = nlohmann::json::array({
		paraboloid_rms_x_json, paraboloid_rms_y_json, projector_calibration_date, projector_points_detected
	});

	nlohmann::json status_projector_calibration_values = nlohmann::json::array({status_details});
	projectorCalibration[STATUS_STRING]                = status_projector_calibration_values;
	json_file[TESTS_STRING]                            = status_projector_calibration_values;

	CheckAndCreateDMVSAppDirectory(serial_number);
	std::string resultsFolder(GetCurrentAppPath() + DMVS_APP_FOLDER + serial_number + "/");
	fs::path dir(resultsFolder);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : The function has not been tested";
	if(exists(dir))
	{
		std::vector<fs::path> found_files;
		for(const auto& entry : fs::directory_iterator(dir))
		{
			if(entry.path().extension().string() == ".json")
				found_files.push_back(entry);
		}

		if(!found_files.empty())
		{
			//Search for the last file
			auto last_file = *found_files.end();

			//Write last file if possible
			if(exists(last_file))
			{
				std::ifstream ifs(last_file.filename().c_str());
				auto j = nlohmann::json::parse(ifs);
				ifs.close();
				AddProjectorCalibrationPathExport(j[TESTS_STRING].front(), calibration_folder);

				//Erase old projector calibration
				while(j[TESTS_STRING].size() > 2)
				{
					j[TESTS_STRING].erase(2);
				}

				j[TESTS_STRING].push_back(projectorCalibration);
				std::ofstream outfile(last_file.filename().c_str());
				outfile << j;
				outfile.close();
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << j << std::endl;
			}
		}
	}

	std::ofstream file(calibration_folder + PROJECTOR_CALIBRATION_FILE_NAME);
	file << json_file;
	file.close();
	WriteToConsole(json_file);
}

void CalibrationJSONWriter::WriteCameraCalibration(CalibrationData& calibration_object,
                                                   const std::string& full_file_path)
{
	std::ifstream input(full_file_path, std::ofstream::in);
	nlohmann::json calibration_json;
	input >> calibration_json;

	calibration_json["DMVS"]["value"]                    = calibration_object.hardware_version;
	calibration_json["DMVS"]["CalibrationDate"]["value"] = Helper::GetNowTime();
	calibration_json["DMVS"]["SerialNumber"]["value"]    = calibration_object.serial_number;
	calibration_json["DMVS"]["FirmwareVersion"]["value"] = calibration_object.firmware_version;
	auto environmental_json                                                  = calibration_json["DMVS"]["Environmental"];
	auto temperature_json                                                    = environmental_json["Temperature"];
	temperature_json["LaserTemperature"]["calibrationBeginValue"]            = calibration_object.laser_temperature_begin;
	temperature_json["LaserTemperature"]["calibrationEndValue"]              = calibration_object.laser_temperature_end;
	temperature_json["Camera1Temperature"]["calibrationBeginValue"]          = calibration_object.camera1_temperature_begin;
	temperature_json["Camera1Temperature"]["calibrationEndValue"]            = calibration_object.camera1_temperature_end;
	temperature_json["Camera2Temperature"]["calibrationBeginValue"]          = calibration_object.camera2_temperature_begin;
	temperature_json["Camera2Temperature"]["calibrationEndValue"]            = calibration_object.camera1_temperature_end;
	temperature_json["BackPlateSensor1Temperature"]["calibrationBeginValue"] = calibration_object.back_plate_sensor1_temperature_begin;
	temperature_json["BackPlateSensor1Temperature"]["calibrationEndValue"]   = calibration_object.back_plate_sensor1_temperature_end;
	temperature_json["BackPlateSensor2Temperature"]["calibrationBeginValue"] = calibration_object.back_plate_sensor2_temperature_begin;
	temperature_json["BackPlateSensor2Temperature"]["calibrationEndValue"]   = calibration_object.back_plate_sensor2_temperature_end;
	temperature_json["BackPlateSensor3Temperature"]["calibrationBeginValue"] = calibration_object.back_plate_sensor3_temperature_begin;
	temperature_json["BackPlateSensor3Temperature"]["calibrationEndValue"]   = calibration_object.back_plate_sensor3_temperature_end;

	auto pressure_json                     = environmental_json["Pressure"];
	pressure_json["calibrationBeginValue"] = calibration_object.pressure_begin;
	pressure_json["calibrationEndValue"]   = calibration_object.pressure_end;

	auto humidity_json                                  = environmental_json["Humidity"];
	humidity_json["calibrationBeginValue"]              = calibration_object.humidity_begin;
	humidity_json["calibrationEndValue"]                = calibration_object.humidity_end;
	calibration_json["DMVS"]["DetectedPoints"]["value"] = calibration_object.detected_points;
	calibration_json["DMVS"]["FinalRMS"]["value"]       = calibration_object.final_rms;

	auto camera_left                         = calibration_json["DMVS"]["Cameras"]["Camera"][0];
	camera_left["channel"]                   = calibration_object.camera_left.channel;
	camera_left["ImageResolution"]["height"] = calibration_object.camera_left.height;
	camera_left["ImageResolution"]["width"]  = calibration_object.camera_left.width;

	cv::Vec2d depth_param_left = calibration_object.camera_left.intrinsics.depth_parameters.scalar;
	std::vector<double> depth_param_vec_left;
	Helper::ToVector(depth_param_left, depth_param_vec_left);
	camera_left["Parameters"]["IntrinsicParams"]["DepthDependent"]["Scalar"]["value"] = depth_param_vec_left;

	cv::Vec3d exponential_left = calibration_object.camera_left.intrinsics.depth_parameters.exponential;
	std::vector<double> depth_param_exp_left;
	Helper::ToVector(exponential_left, depth_param_exp_left);
	camera_left["Parameters"]["IntrinsicParams"]["DepthDependent"]["Exponential"]["value"] = depth_param_exp_left;

	cv::Mat camera_matrix_left = calibration_object.camera_left.intrinsics.camera_intrinsic_matrix;
	std::vector<double> camera_matrix_vector_left;
	Helper::ToVector(camera_matrix_left, camera_matrix_vector_left);
	camera_left["Parameters"]["IntrinsicParams"]["CameraMatrix"]["value"] = camera_matrix_vector_left;

	/*cv::Mat extrinsic_matrix_left = calibration_object.camera_left.extrinsic_params;
	std::vector<double> extrinsics_left;
	Helper::ToVector(extrinsic_matrix_left, extrinsics_left);
	camera_left["Parameters"]["ExtrinsicParams"]["value"] = extrinsics_left;*/

	auto camera_right                         = calibration_json["DMVS"]["Cameras"]["Camera"][0];
	camera_right["channel"]                   = calibration_object.camera_left.channel;
	camera_right["ImageResolution"]["height"] = calibration_object.camera_left.height;
	camera_right["ImageResolution"]["width"]  = calibration_object.camera_left.width;

	cv::Vec2d depth_param_right = calibration_object.camera_left.intrinsics.depth_parameters.scalar;
	std::vector<double> depth_param_vec_right;
	Helper::ToVector(depth_param_right, depth_param_vec_right);
	camera_left["Parameters"]["IntrinsicParams"]["DepthDependent"]["Scalar"]["value"] = depth_param_vec_right;

	cv::Vec3d exponential_right = calibration_object.camera_left.intrinsics.depth_parameters.exponential;
	std::vector<double> depth_param_exp_right;
	Helper::ToVector(exponential_right, depth_param_exp_right);
	camera_left["Parameters"]["IntrinsicParams"]["DepthDependent"]["Exponential"]["value"] = depth_param_exp_right;

	cv::Mat camera_matrix_right = calibration_object.camera_left.intrinsics.camera_intrinsic_matrix;
	std::vector<double> camera_matrix_vector_right;
	Helper::ToVector(camera_matrix_right, camera_matrix_vector_right);
	camera_left["Parameters"]["IntrinsicParams"]["CameraMatrix"]["value"] = camera_matrix_vector_right;

	/*cv::Mat extrinsic_matrix_right = calibration_object.camera_left.extrinsic_params;
	std::vector<double> extrinsics_right;
	Helper::ToVector(extrinsic_matrix_right, extrinsics_right);
	camera_left["Parameters"]["ExtrinsicParams"]["value"] = extrinsics_right;*/

	auto projector       = calibration_json["DMVS"]["Projectors"]["Projector"];
	projector["channel"] = calibration_object.projector.channel;
	projector["name"]    = calibration_object.projector.name;

	cv::Mat projector_camera_matrix = calibration_object.camera_right.intrinsics.camera_intrinsic_matrix;
	std::vector<double> projector_camera_matrix_vector;
	Helper::ToVector(projector_camera_matrix, projector_camera_matrix_vector);
	projector["Parameters"]["IntrinsicParams"]["CameraMatrix"]["value"] = projector_camera_matrix_vector;

	/*cv::Mat projector_extrinsic_matrix = calibration_object.camera_right.extrinsic_params;
	std::vector<double> projector_extrinsics;
	Helper::ToVector(projector_extrinsic_matrix, projector_extrinsics);
	projector["Parameters"]["ExtrinsicParams"]["value"] = projector_extrinsics;*/

	std::ofstream out(full_file_path, std::ofstream::out);
	out << std::setw(4) << calibration_json << std::endl;
}

nlohmann::json CalibrationJSONWriter::GenerateCameraCalibrationReport(double overall_rms_left,
                                                                      double overall_rms_right,
                                                                      double tangential_rms_left,
                                                                      double tangential_rms_right,
                                                                      double radial_rms_left,
                                                                      double radial_rms_right,
                                                                      bool success)
{
	nlohmann::json camera_calibration_values;

	// Camera Calibration Data
	camera_calibration_values[LEVEL_STRING]   = success ? 1 : 3;
	camera_calibration_values[NAME_STRING]    = CAMERA_CALIBRATION_NAME_STRING;
	camera_calibration_values[MESSAGE_STRING] = CAMERA_CALIBRATION_MESSAGE_STRING;

	// Actual values
	nlohmann::json rms_left;
	rms_left[KEY_STRING]    = RMS_LEFT_STRING;
	rms_left[VALUES_STRING] = std::to_string(overall_rms_left);

	nlohmann::json rms_right;
	rms_right[KEY_STRING]   = RMS_LEFT_STRING;
	rms_right[VALUE_STRING] = std::to_string(overall_rms_right);

	nlohmann::json rms_tangential_left;
	rms_tangential_left[KEY_STRING]   = TANGENTIAL_RMS_LEFT_STRING;
	rms_tangential_left[VALUE_STRING] = std::to_string(tangential_rms_left);

	nlohmann::json rms_tangential_right;
	rms_tangential_right[KEY_STRING]   = TANGENTIAL_RMS_RIGHT_STRING;
	rms_tangential_right[VALUE_STRING] = std::to_string(tangential_rms_right);

	nlohmann::json rms_radial_left;
	rms_radial_left[KEY_STRING]   = RADIAL_RMS_LEFT_STRING;
	rms_radial_left[VALUE_STRING] = std::to_string(radial_rms_left);

	nlohmann::json rms_radial_right;
	rms_radial_right[KEY_STRING]   = RADIAL_RMS_RIGHT_STRING;
	rms_radial_right[VALUE_STRING] = std::to_string(radial_rms_right);

	nlohmann::json camera_calibration_date;
	camera_calibration_date[KEY_STRING]   = CAMERA_CALIBRATION_DATE_STRING;
	camera_calibration_date[VALUE_STRING] = GetTimeCurrent();

	camera_calibration_values[VALUES_STRING] = nlohmann::json::array({
		rms_left,
		rms_right,
		rms_tangential_left,
		rms_tangential_right,
		rms_radial_left,
		rms_radial_right,
		camera_calibration_date
	});

	nlohmann::json status_camera_calibration_values = nlohmann::json::array({camera_calibration_values});

	nlohmann::json camera_calibration;
	camera_calibration[DESCRIPTION_STRING] = CALIBRATION_DESCRIPTION_STRING;
	camera_calibration[TEST_STRING]        = CAMERA_CALIBRATION_NAME_STRING;
	camera_calibration[STATUS_STRING]      = status_camera_calibration_values;

	return camera_calibration;
}

nlohmann::json CalibrationJSONWriter::GenerateCameraCalibrationPathExport(const std::string& calibration_folder)
{
	nlohmann::json calibration_folders;
	calibration_folders[DESCRIPTION_STRING] = CALIBRATION_PATH_DESCRIPTION_STRING;
	calibration_folders[TEST_STRING]        = CALIBRATION_PATH_DESCRIPTION_STRING;

	nlohmann::json camera_calibration_path;
	camera_calibration_path[LEVEL_STRING]   = 1;
	camera_calibration_path[MESSAGE_STRING] = calibration_folder;
	camera_calibration_path[NAME_STRING]    = CAMERA_CALIBRATION_PATH_STRING;

	calibration_folders[STATUS_STRING] = nlohmann::json::array({camera_calibration_path});
	return calibration_folders;
}

void CalibrationJSONWriter::CheckAndCreateDMVSAppDirectory(const std::string& serial_number)
{
	const auto path_to_create = GetCurrentAppPath() + DMVS_APP_FOLDER;
	const fs::path results_directory(path_to_create);

	if(!exists(results_directory))
	{
		if(!(CreateDirectory(results_directory.c_str(), nullptr) || ERROR_ALREADY_EXISTS == GetLastError()))
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "Directory " << results_directory.string() << " could not be created.";
		}
	}

	const fs::path serial_directory = GetCurrentAppPath() + DMVS_APP_FOLDER + serial_number + "/";

	if(!exists(serial_directory))
	{
		if(!(CreateDirectory(serial_directory.c_str(), nullptr) || ERROR_ALREADY_EXISTS == GetLastError()))
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Directory " << serial_directory.string() << "  could not be created.";
		}
	}
}

std::string CalibrationJSONWriter::GetCurrentAppPath()
{
	wchar_t result[MAX_PATH];
	GetModuleFileName(nullptr, result, MAX_PATH);
	std::wstring ws(result);
	const std::string str(ws.begin(), ws.end());
	return str.empty() ? "./" : str;
}

bool CalibrationJSONWriter::AddProjectorCalibrationPathExport(nlohmann::json& calibration_path_test,
                                                              const std::string& calibration_folder)
{
	try
	{
		nlohmann::json projector_calibration_path;
		projector_calibration_path[LEVEL_STRING]   = 1;
		projector_calibration_path[MESSAGE_STRING] = calibration_folder;
		projector_calibration_path[NAME_STRING]    = PROJECTOR_CALIBRATION_PATH_STRING;

		// Erase old projector calibration
		while(calibration_path_test[STATUS_STRING].size() > 1)
		{
			calibration_path_test[STATUS_STRING].erase(1);
		}

		calibration_path_test[STATUS_STRING].push_back(projector_calibration_path);
	}
	catch(nlohmann::json::exception& exception)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << exception.what();
		return false;
	}

	return true;
}

void CalibrationJSONWriter::WriteToConsole(nlohmann::json& json_to_write)
{
	std::stringstream ss;
	ss << json_to_write << std::endl;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << ss.str();
}

std::string CalibrationJSONWriter::GetTimeCurrent()
{
	struct tm new_time;
	time_t now;
	time(&now);
	localtime_s(&new_time, &now);
	char buf[1024];
	strftime(buf, sizeof(buf) - 1, "%Y%m%d-%H%M", &new_time);
	return std::string(buf);
}
