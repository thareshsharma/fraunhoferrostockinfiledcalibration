#include "calibration_json_reader.h"
#include "helper.h"

CalibrationJSONReader::CalibrationJSONReader() = default;

void CalibrationJSONReader::ReadCameraCalibration(const std::string& json_file_path,
                                                  CalibrationData& calibration_data)
{
	//const std::string json_file_path = R"(C:\ProgramData\FARO\Freestyle\Cameras\DMVS_DMVS011900031.json)";
	// read a JSON file
	std::ifstream stream(json_file_path);
	json j = json::parse(stream);

	calibration_data.hardware_version = j["DMVS"]["value"].get<std::string>();
	calibration_data.calibration_date = j["DMVS"]["CalibrationDate"]["value"].get<std::string>();
	calibration_data.serial_number    = j["DMVS"]["SerialNumber"]["value"].get<std::string>();
	calibration_data.firmware_version = j["DMVS"]["FirmwareVersion"]["value"].get<std::string>();

	auto environmental_json                  = j["DMVS"]["Environmental"];
	auto temperature_json                    = environmental_json["Temperature"];
	calibration_data.laser_temperature_begin = temperature_json["LaserTemperature"]["calibrationBeginValue"].get<double>();
	calibration_data.laser_temperature_end   = temperature_json["LaserTemperature"]["calibrationEndValue"].get<double>();

	calibration_data.camera1_temperature_begin            = temperature_json["Camera1Temperature"]["calibrationBeginValue"].get<double>();
	calibration_data.camera1_temperature_end              = temperature_json["Camera1Temperature"]["calibrationEndValue"].get<double>();
	calibration_data.camera2_temperature_begin            = temperature_json["Camera2Temperature"]["calibrationBeginValue"].get<double>();
	calibration_data.camera1_temperature_end              = temperature_json["Camera2Temperature"]["calibrationEndValue"].get<double>();
	calibration_data.back_plate_sensor1_temperature_begin = temperature_json["BackPlateSensor1Temperature"]["calibrationBeginValue"].get<double>();
	calibration_data.back_plate_sensor1_temperature_end   = temperature_json["BackPlateSensor1Temperature"]["calibrationEndValue"].get<double>();
	calibration_data.back_plate_sensor2_temperature_begin = temperature_json["BackPlateSensor2Temperature"]["calibrationBeginValue"].get<double>();
	calibration_data.back_plate_sensor2_temperature_end   = temperature_json["BackPlateSensor2Temperature"]["calibrationEndValue"].get<double>();
	calibration_data.back_plate_sensor3_temperature_begin = temperature_json["BackPlateSensor3Temperature"]["calibrationBeginValue"].get<double>();
	calibration_data.back_plate_sensor3_temperature_end   = temperature_json["BackPlateSensor3Temperature"]["calibrationEndValue"].get<double>();

	auto pressure_json              = environmental_json["Pressure"];
	calibration_data.pressure_begin = pressure_json["calibrationBeginValue"].get<double>();
	calibration_data.pressure_end   = pressure_json["calibrationEndValue"].get<double>();

	auto humidity_json               = environmental_json["Humidity"];
	calibration_data.humidity_begin  = humidity_json["calibrationBeginValue"].get<double>();
	calibration_data.humidity_end    = humidity_json["calibrationEndValue"].get<double>();
	calibration_data.detected_points = j["DMVS"]["DetectedPoints"]["value"].get<int>();
	calibration_data.final_rms       = j["DMVS"]["FinalRMS"]["value"].get<double>();

	auto camera_left                     = j["DMVS"]["Cameras"]["Camera"][0];
	calibration_data.camera_left.channel = ImageChannel(camera_left["channel"].get<size_t>());
	calibration_data.camera_left.height  = camera_left["ImageResolution"]["height"].get<int>();
	calibration_data.camera_left.width   = camera_left["ImageResolution"]["width"].get<int>();

	cv::Vec2d depth_param_left;
	const auto depth_param_vec_left = camera_left["Parameters"]["IntrinsicParams"]["DepthDependent"]["Scalar"]["value"].get<std::vector<double>>();
	Helper::ToVec2D(depth_param_vec_left, depth_param_left);
	calibration_data.camera_left.intrinsics.depth_parameters.scalar = depth_param_left;

	cv::Vec3d exponential_left;
	auto depth_param_exp_left = camera_left["Parameters"]["IntrinsicParams"]["DepthDependent"]["Exponential"]["value"].get<std::vector<double>>();
	Helper::ToVec3D(depth_param_exp_left, exponential_left);
	calibration_data.camera_left.intrinsics.depth_parameters.exponential = exponential_left;

	cv::Mat camera_matrix_left;
	const auto camera_matrix_vector_left = camera_left["Parameters"]["IntrinsicParams"]["CameraMatrix"]["value"].get<std::vector<double>>();
	Helper::ToMatrix3x3(camera_matrix_vector_left, camera_matrix_left);
	calibration_data.camera_left.intrinsics.camera_intrinsic_matrix = camera_matrix_left.clone();

	cv::Mat distortion_left;
	const auto distortion_vector_left = camera_left["Parameters"]["IntrinsicParams"]["Distortion"]["value"].get<std::vector<double>>();
	Helper::ToDistortionMatrix(distortion_vector_left, distortion_left);
	calibration_data.camera_left.intrinsics.distortion_vector = distortion_left.clone();

	cv::Mat camera_left_extrinsic_rotation_matrix;
	cv::Mat camera_left_extrinsic_translation_vector;
	const auto transformation_left = camera_left["Parameters"]["ExtrinsicParams"]["TransformationMatrix"]["value"].get<std::vector<double>>();
	Helper::ToExtrinsicsParameters(transformation_left, camera_left_extrinsic_rotation_matrix, camera_left_extrinsic_translation_vector);
	calibration_data.camera_left.extrinsics.rotation_matrix    = camera_left_extrinsic_rotation_matrix.clone();
	calibration_data.camera_left.extrinsics.translation_vector = camera_left_extrinsic_translation_vector.clone();

	auto camera_right                     = j["DMVS"]["Cameras"]["Camera"][1];
	calibration_data.camera_right.channel = ImageChannel(camera_right["channel"].get<size_t>());
	calibration_data.camera_right.height  = camera_right["ImageResolution"]["height"].get<int>();
	calibration_data.camera_right.width   = camera_right["ImageResolution"]["width"].get<int>();

	cv::Vec2d depth_param_right;
	const auto depth_param_vector_right = camera_right["Parameters"]["IntrinsicParams"]["DepthDependent"]["Scalar"]["value"].get<std::vector<double>
	>();
	Helper::ToVec2D(depth_param_vector_right, depth_param_right);
	calibration_data.camera_right.intrinsics.depth_parameters.scalar = depth_param_right;

	cv::Vec3d exponential_right;
	auto depth_param_exp_right = camera_right["Parameters"]["IntrinsicParams"]["DepthDependent"]["Exponential"]["value"].get<std::vector<double>>();
	Helper::ToVec3D(depth_param_exp_right, exponential_right);
	calibration_data.camera_right.intrinsics.depth_parameters.exponential = exponential_right;

	cv::Mat camera_matrix_right;
	const auto camera_vector_right = camera_right["Parameters"]["IntrinsicParams"]["CameraMatrix"]["value"].get<std::vector<double>>();
	Helper::ToMatrix3x3(camera_vector_right, camera_matrix_right);
	calibration_data.camera_right.intrinsics.camera_intrinsic_matrix = camera_matrix_right.clone();

	cv::Mat distortion_right;
	const auto distortion_vector_right = camera_right["Parameters"]["IntrinsicParams"]["Distortion"]["value"].get<std::vector<double>>();
	Helper::ToDistortionMatrix(distortion_vector_right, distortion_right);
	calibration_data.camera_right.intrinsics.distortion_vector = distortion_right.clone();

	cv::Mat camera_right_extrinsic_rotation_matrix;
	cv::Mat camera_right_extrinsic_translation_vector;
	const auto transformation_right = camera_right["Parameters"]["ExtrinsicParams"]["TransformationMatrix"]["value"].get<std::vector<double>>();
	Helper::ToExtrinsicsParameters(transformation_right, camera_right_extrinsic_rotation_matrix, camera_right_extrinsic_translation_vector);
	calibration_data.camera_right.extrinsics.rotation_matrix    = camera_right_extrinsic_rotation_matrix.clone();
	calibration_data.camera_right.extrinsics.translation_vector = camera_right_extrinsic_translation_vector.clone();

	auto projector                     = j["DMVS"]["Projectors"]["Projector"];
	calibration_data.projector.channel = ImageChannel(projector["channel"].get<size_t>());
	calibration_data.projector.name    = projector["name"].get<std::string>();

	cv::Mat projector_intrinsic_matrix;
	const auto projector_camera_matrix_vector = projector["Parameters"]["IntrinsicParams"]["ProjectorMatrix"]["value"].get<std::vector<double>>();
	Helper::ToMatrix3x3(projector_camera_matrix_vector, projector_intrinsic_matrix);
	calibration_data.projector.intrinsics.projector_intrinsic_matrix = projector_intrinsic_matrix.clone();

	cv::Mat distortion_projector;
	const auto distortion_vector_projector = projector["Parameters"]["IntrinsicParams"]["Distortion"]["value"].get<std::vector<double>>();
	Helper::ToDistortionMatrix(distortion_vector_projector, distortion_projector);
	calibration_data.projector.intrinsics.distortion_vector = distortion_projector.clone();

	cv::Mat projector_extrinsic_rotation_matrix;
	cv::Mat projector_extrinsic_translation_vector;
	const auto transformation_extrinsics = projector["Parameters"]["ExtrinsicParams"]["TransformationMatrix"]["value"].get<std::vector<double>>();
	Helper::ToExtrinsicsParameters(transformation_extrinsics, projector_extrinsic_rotation_matrix, projector_extrinsic_translation_vector);
	calibration_data.projector.extrinsics.rotation_matrix    = projector_extrinsic_rotation_matrix.clone();
	calibration_data.projector.extrinsics.translation_vector = projector_extrinsic_translation_vector.clone();

	cv::Mat projector_pattern_definition;
	const auto laser_beams = projector["PatternDefinition"]["value"].get<std::vector<double>>();
	Helper::ToLaserBeamCustomMatrix(laser_beams, projector_pattern_definition);
	calibration_data.projector.pattern_definition = projector_pattern_definition.clone();
}
