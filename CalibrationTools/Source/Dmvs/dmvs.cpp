#include "dmvs.h"

#include "application_settings.h"
#include "constants.h"
#include "calibration_cache.h"
#include "configuration_container.h"
#include "helper.h"
#include "capture_calibration.h"
#include "capture_grabber.h"
#include "enum_image_channel_type.h"
#include "laser_dot_detector_2d.h"
#include "reconstructor_laser_dots_3D_back_projection.h"
#include "projector_calibration_helper.h"
#include "single_camera_calibration.h"
#include "camera_sensor_depth_dependent.h"
#include "BoardDetector/board_detector.h"
#include "fit_plane.h"
#include "projector.h"

namespace fs = boost::filesystem;

namespace DMVS
{
namespace CameraCalibration
{
CriticalSection DMVSSensor::critical_section_lock;

DMVSSensor::DMVSSensor() :
	grabber(nullptr),
	open_mode(DeviceCurrentMode::CLOSED),
	frame_index(0),
	configuration_options(nullptr),
	stop(false),
	m_snapshot(false),
	auto_exposure_(Constants::DMVS_DEFAULT_AUTO_EXPOSURE),
	set_laser_(Constants::DMVS_DEFAULT_SET_LASER),
	set_led_flash_constant_(Constants::DMVS_DEFAULT_SET_LED_FLASH_CONSTANT),
	position_interface_active_(Constants::DMVS_DEFAULT_POSITION_INTERFACE_ACTIVE),
	led_power_0_(Constants::DMVS_DEFAULT_LED_POWER), //URANGE 0 to 100 (%)
	led_power_1_(Constants::DMVS_DEFAULT_LED_POWER), //RANGE 0 to 100 (%)
	mode_(Constants::DMVS_DEFAULT_MODE),
	exposure_time_(Constants::DMVS_DEFAULT_EXPOSURE_TIME),
	frame_rate_(Constants::DMVS_DEFAULT_FRAME_RATE),
	timestamp_(0.0),
	dmvs_device_(nullptr),
	frame_grabber_count_(0),
	frame_count_(0),
	skipped_frame_count_(0),
	rgb_width_(Constants::DMVS_IMAGE_WIDTH),
	rgb_height_(Constants::DMVS_IMAGE_HEIGHT)
{
}

DMVSSensor::~DMVSSensor()
{
	DMVS_DELETE(dmvs_device_);
}

void DMVSSensor::ImageReceived2D(unsigned char* image,
                                 const long width,
                                 const long height,
                                 data::AdditionalImageInformation2D additional_image_information,
                                 void* context)
{
	auto dmvs = static_cast<DMVSSensor*>(context);
	CriticalSection::Access lock(critical_section_lock, CriticalSection::read);
	const cv::Mat temp(height, width, CV_8U, image);
	const auto io_message1 = dmvs->cache->Put(dmvs->frame_grabber_count_ + 1,
	                                          Constants::GREY0,
	                                          temp.colRange(0,
	                                                        Constants::DMVS_IMAGE_WIDTH),
	                                          CalibrationCacheItem::CV_RAW_MATRIX);

	const auto io_message2 = dmvs->cache->Put(dmvs->frame_grabber_count_ + 1,
	                                          Constants::GREY1,
	                                          temp.colRange(Constants::DMVS_IMAGE_WIDTH,
	                                                        Constants::DMVS_DOUBLE_CAMERA_IMAGE_WIDTH),
	                                          CalibrationCacheItem::CV_RAW_MATRIX);

	if(io_message1 == IOMessages::IO_OK && io_message2 == IOMessages::IO_OK)
	{
		++dmvs->frame_grabber_count_;
	}
	else if(io_message1 == IOMessages::IO_FALSE || io_message2 == IOMessages::IO_FALSE)
	{
		cv::Mat test;
		// why it is reading this image in grey although test is not being used.??????????
		while(dmvs->cache->Get(dmvs->frame_grabber_count_ + 1, Constants::GREY0, test) == IOMessages::IO_OK)
			++dmvs->frame_grabber_count_;
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::LOG_CACHE_ERROR << Constants::LOG_ERROR_IO_MESSAGE1 << io_message1 <<
 Constants::
LOG_ERROR_IO_MESSAGE1 << io_message2;
	}
}

void DMVSSensor::ImageReceived3D(data::Point3D* image,
                                 const long points_count,
                                 data::AdditionalImageInformation3D additional_image_information,
                                 void* context)
{
	auto dmvs = static_cast<DMVSSensor*>(context);
	const cv::Mat temp(points_count, 3, CV_32F, (float*)image);
	const auto io_message = dmvs->cache->Put(dmvs->frame_grabber_count_ + 1, Constants::POINT_CLOUD, temp);

	if(io_message == IOMessages::IO_OK)
	{
		++dmvs->frame_grabber_count_;
	}
	else if(io_message == IOMessages::IO_FALSE)
	{
		cv::Mat test;

		while(dmvs->cache->Get(dmvs->frame_grabber_count_ + 1, Constants::POINT_CLOUD, test) == IOMessages::IO_OK)
			++dmvs->frame_grabber_count_;
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::LOG_CACHE_ERROR << Constants::LOG_ERROR_IO_MESSAGE1 << io_message;
	}
}

CalibrationExitCode DMVSSensor::SetUpSensor(const std::shared_ptr<ConfigurationContainer>& options)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Sensor Setup " << Constants::LOG_START;
	configuration_options = options;

	//Connect to the DMVS
	if(!Connect())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : could not get connected ";
		return CALIBRATION_DMVS_CONNECT_ERROR;
	}

	if(!dmvs_device_)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No device ";
		return CALIBRATION_NO_DEVICE;
	}

	//Open the device
	if(!Open())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not open the device ";
		return CALIBRATION_DMVS_OPEN_ERROR;
	}

	if(!SetCameraCalibrationSettings(false))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Camera Calibration settings setup failed ";
		return CALIBRATION_SETTINGS_ERROR;
	}

	// Wait for 3 seconds for the first frame to arrive
	Helper::Sleeping(0.1, "while first frame arrive");

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Sensor Setup " << Constants::LOG_END;

	return CALIBRATION_SENSOR_SETUP_OK;
}

DeviceDescription DMVSSensor::DeviceState()
{
	if(!IsSupportedSensor() || !ConnectToDMVS())
		return {DeviceType(), NOT_CONNECTED};

	const auto res = GetDeviceCalibration(capture_calibration);

	const auto state = res && calibration_filename_.empty() ? CALIBRATED : CONNECTED;

	DeviceDescription device_description(DeviceType(), state, serial_number);

	device_description.minimum_z_value = Constants::DEVICE_DESCRIPTION_MINIMUM_Z_VALUE;
	device_description.maximum_z_value = Constants::DEVICE_DESCRIPTION_MAXIMUM_Z_VALUE;
	device_description.default_z_value = Constants::DEVICE_DESCRIPTION_DEFAULT_Z_VALUE;
	device_description.components.push_back(FLASH);
	device_description.components.push_back(LEDS);

	// Calibration modes
	device_description.capabilities.push_back(FS_CAMERA_FACTORY_CAL);
	device_description.capabilities.push_back(FS_PROJECTOR_FACTORY_CAL);
	device_description.capabilities.push_back(FS_CAMERA_INFIELD_CAL);
	device_description.capabilities.push_back(FS_TESTLOOP_CAL);

	return device_description;
}

DeviceType DMVSSensor::DeviceType() const
{
	return DMVS_CAPTURE;
}

bool DMVSSensor::Init(const std::string& input_filename,
                      const std::string& output_filename,
                      const std::shared_ptr<ConfigurationContainer> options)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initializing DMVS Device [" << dmvs_device_->GetSerialNumber() << "]" << Constants::LOG_START;

	configuration_options = options;
	input_file_           = input_filename;
	frame_count_          = ULONG_MAX;
	frame_index           = 0;
	timestamp_            = 0.0;

	if(!output_filename.empty())
	{
		const fs::path file(output_filename);

		if(is_regular_file(file))
			output_file_ = file;
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initializing DMVS Device [" << dmvs_device_->GetSerialNumber() << "]" << Constants::LOG_END;
	return true;
}

std::string DMVSSensor::GetDeviceType() const
{
	return device_type_;
}

std::string DMVSSensor::GetSerialNumber() const
{
	return serial_number;
}

std::string DMVSSensor::GetCaptureDeviceIdent() const
{
	// DMVS may not have serial number sometimes. Just use the device id
	return GetDeviceType() + (GetSerialNumber().empty() ? "" : "_" + GetSerialNumber());
}

bool DMVSSensor::InitCapture()
{
	// Sanity checks
	if(!configuration_options)
		return false;

	// Create new cache
	cache = std::make_shared<CalibrationCache>();
	cache->MaximumItems(400);

	if(!output_file_.empty())
		cache->Attach(output_file_.string(),
		              CalibrationCache::CACHE_WRITE | CalibrationCache::CACHE_ASYNC,
		              CalibrationCache::MagicCode(DeviceType()));

	// Store sensor infos in the recording file
	// Note: Don't use getCaptureDeviceIdent to get the string. This doesn't work.
	if(cache->Put(0, Constants::DMVS_SENSOR, GetCaptureDeviceIdent()) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device Identification could not be saved in cache";
	}

	if(cache->Put(0, Constants::DMVS_DEVICE_TYPE, GetDeviceType()) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device Type could not be saved in cache";
	}

	if(cache->Put(0, Constants::DMVS_SERIAL_NO, GetSerialNumber()) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device Serial Number could not be saved in cache";
	}

	//if(!(rgb_height_ > 0 && rgb_height_ > 0))
	//	return false;

	if(cache->Put(0, Constants::DMVS_RGB_WIDTH, std::to_string(rgb_width_)) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Height could not be saved in cache";
	}

	if(cache->Put(0, Constants::DMVS_RGB_HEIGHT, std::to_string(rgb_height_)) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device Identification could not be saved in cache";
	}

	if(cache->Put(0, Constants::DMVS_SKIPPED, std::to_string(skipped_frame_count_)) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device Identification could not be saved in cache";
	}

	if(cache->Put(0, Constants::DMVS_OPTIONS, configuration_options) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device Identification could not be saved in cache";
	}

	return true;
}

bool InitReplay()
{
	/// Sanity checks
	//if(cache_)				return false;

	/// Create a new cache
	//cache_ = new SFMCache;
	//cache_->maxItems(800);
	//if(cache_->attach(m_inputFile, SFMCache::SFMCacheRead) != IOMessages::IO_OK)
	//	return false;

	/// Load calibration and other sensor parameter
	////	Ignore errors for optional parameters.
	//std::shared_ptr<AttrContainer> cal = new AttrContainer;
	//cal->init();

	//bool calibOK = (cache_->get(0, "calibration", cal) == IOMessages::IO_OK) &&
	//	m_calibration.fillFromContainer(LS::smart_cast<CalibrationContainer>(cal));

	//if(!calibOK)
	//{
	//	std::shared_ptr<AttrContainer> recordingInfo = new AttrContainer;
	//	recordingInfo->init();
	//	if(cache_->get(0, "recordingInfo", recordingInfo) != IOMessages::IO_OK)
	//		calibOK = false;
	//	else
	//	{
	//		std::string content = recordingInfo->getCustomAttrValue("Content", std::string());
	//		if(content.compareEquality("FactoryCalibration"))
	//		{
	//			calibOK = getDefaultCalibration(m_calibration);
	//		}
	//	}
	//}

	//if(!calibOK)
	//	return false;


	/// Old files store the device identifier. New ones the device type and serial.
	//std::string val;
	//if(cache_->get(0, "width", val) != IOMessages::IO_OK || !val.stringToNumber(m_rgbWidth))					return false;
	//if(cache_->get(0, "height", val) != IOMessages::IO_OK || !val.stringToNumber(m_rgbHeight))				return false;
	//if(cache_->get(0, "skipped", val) == IOMessages::IO_OK) 	val.stringToNumber(m_skippedFrames);

	//if(cache_->get(0, "sensor", val) == IOMessages::IO_OK)
	//{
	//	int ul = val.findLastOf(L'_');
	//	strcpy_s(m_deviceType, val.substr(0, ul).c_str());

	//	if(ul > 1)
	//		strcpy_s(m_serial, val.substr(ul + 1, -1).c_str());
	//	else
	//		memset(m_serial, 0x0, sizeof(m_serial));
	//}
	//else
	//{
	//	if(cache_->get(0, "type", val) != IOMessages::IO_OK || strcpy_s(m_deviceType, val.c_str()) != 0)	return false;
	//	if(cache_->get(0, "serial", val) != IOMessages::IO_OK || strcpy_s(m_serial, val.c_str()) != 0)		return false;
	//}

	/// Try to set the processing options
	//std::shared_ptr<AttrContainer> opts = new AttrContainer;
	//opts->init();

	//if(cache_->get(0, "options", opts) == IOMessages::IO_OK)
	//{
	//	int trackmode = SFMCore::get(opts, "FeatureTrackMode", (size_t)VG_UNKNOWN_TRACK_MODE);
	//	if(trackmode != VG_UNKNOWN_TRACK_MODE)
	//	{
	//		SFMCore::set(m_options, "FeatureTrackMode", trackmode);
	//	}
	//}

	/// Count frames
	////	Store the number of frames in the file. Since recording
	////	is done, it is not possible to change the number of frames
	////	anymore.
	//if(SFMCore::get(m_options, "SFMForceFrameCounting", false) ||	// explicit counting for developers to process files with wrong nFrames value
	//	cache_->get(0, "nframes", val) != IOMessages::IO_OK ||		// explicit counting if "nframes" does not exist
	//	!val.stringToNumber(m_frames))									// explicit counting if "nframes" value exist but not a valid number
	//{
	//	//	Frames have an ID and not an index
	//	//	Frames may not start with 0 or may not be continuously enumerated
	//	//	Stop after MAX_INFO_ENTRY_GAP not existing frames
	//	std::shared_ptr<AttrContainer> info = new AttrContainer;
	//	info->init();

	//	int nFailed = 0;
	//	m_frames = 0;
	//	for(auto i = 0; nFailed < MAX_INFO_ENTRY_GAP; ++i)
	//	{
	//		if(!cache_->get(i, "info", info) == IOMessages::IO_OK)
	//		{
	//			if(m_frames == 0 && i < MAX_INFO_ENTRY_WARMUP_GAP)
	//				// Ignore missing frames at startup.
	//				//	Freestyle 2 laser needs a long warmup time. During
	//				//	this time, the sensor captures frames and increases the
	//				//	frame counter.
	//				;
	//			else
	//				nFailed++;
	//		}
	//		else
	//		{
	//			nFailed = 0;
	//			m_frames = i;
	//		}
	//	}

	//	cache_->put(0, "nframes", std::string::stringFromNumber(m_frames));
	//}

	return true;
}

bool DMVSSensor::Open()
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Opening DMVS Device [" << dmvs_device_->GetSerialNumber() << "] " << Constants::LOG_START;

	// Sanity checks
	if(DeviceState().state < CONNECTED)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS Device state is not connected";
		return false;
	}

	if(IsOpened())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is already opened";
		return true;
	}

	// Setup recording
	if(!Init(input_file_.string(), "", configuration_options))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not initialize DMVS Device";
		return false;
	}

	// Configure tracking parameters
	if(!ConfigureTracking())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not bring DMVS Device to Tracking State!";
		return false;
	}

	//// Get calibration
	//if(!GetDeviceCalibration(capture_calibration))
	//{
	//	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Calibration could not be downloaded DMVS Device!";
	//	return false;
	//}

	if(!Configure())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Error in configuring DMVS Device. Probably, DMVS Parameter Error";
	}

	if(!InitCapture())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not initialise Capture!";
		return false;
	}

	if(dmvs_device_->Register2DImageCallBack(ImageReceived2D, this) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not register 2D Image Callback";
		return false;
	}

	if(dmvs_device_->Register3DImageCallBack(ImageReceived3D, this) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not register 3D Image Callback";
		return false;
	}

	open_mode = DeviceCurrentMode::GRABBING;

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Opening DMVS Device [" << dmvs_device_->GetSerialNumber() << "] " << Constants::LOG_END;

	return IsOpened();
}

void DMVSSensor::GetCalibration(CaptureCalibration& calibration) const
{
	calibration = capture_calibration;
}

void DMVSSensor::SetCalibration(CaptureCalibration& calibration)
{
	capture_calibration = calibration;
}

bool DMVSSensor::GetDefaultCalibration(CaptureCalibration& calibration)
{
	// NOT IMPLEMENTED IN DMVSSDK
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : GET DMVS DEFAULT CALIBRATION NOT IMPLEMENTED IN DMVS SDK ";
	return false;
}

bool DMVSSensor::GetDeviceCalibration(CaptureCalibration& calibration)
{
	return CreateDeviceCalibrationFile() && calibration.Load(calibration_filename_);
}

bool DMVSSensor::ManageCalibration(const ManageCalibrationMode calibration_mode)
{
	// Store the current calibration on device
	if(calibration_mode == STORE_CALIBRATION)
	{
		return UploadCalibration();
	}

	// Restore a calibration form device
	// Stores the calibration in the SCENECT/cameras folder
	if(calibration_mode == RESTORE_CALIBRATION)
	{
		return CreateDeviceCalibrationFile();
	}

	return false;
}

bool DMVSSensor::PushChannelImageToCache(bool synced)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_GRAB_IMAGE << Constants::LOG_START;
	auto if_grabbed = false;

	if(IsLive() && dmvs_device_)
		if_grabbed = Start();

	//Helper::Sleeping(0.1, "while waiting for startImageAcquisition() to finish");
	if_grabbed &= GrabFrame();

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_GRAB_IMAGE << Constants::LOG_END;

	return if_grabbed;
}

std::string DMVSSensor::GetExtension()
{
	// TODO
	return "DMVS";
}

LSFitPlane::PlaneParam FitPlane(const std::vector<cv::Point3f>& pts,
                                CaptureCalibration& capture_calibration,
                                std::vector<int>& plane_points,
                                std::shared_ptr<ConfigurationContainer> options)
{
	plane_points.clear();
	std::vector<int> plane_points_all;
	std::vector<cv::Vec3d> plane_pts3d;

	std::vector<cv::Point2f> projector_projected_points;
	const auto cal_color = capture_calibration.GetCalibration(CHANNEL_PROJECTOR0);
	cal_color->ProjectPoints(pts, projector_projected_points);
	const auto& laser_beams = capture_calibration.GetCustomMatrix("laser_beams");

	// Get the points lying around the center of the image (150x100 pixels)
	for(size_t i = 0; i < pts.size(); i++)
	{
		auto proj_coordinate = projector_projected_points[i];

		if(cv::norm(proj_coordinate) < 150)
		{
			plane_points_all.push_back(int(i));
			plane_pts3d.emplace_back(cv::Vec3d(pts[i].x, pts[i].y, pts[i].z));
		}
	}

	return Projector::FitPlane2(plane_pts3d, plane_points, plane_points_all);
}

int DMVSSensor::TestLoop(bool reload_calibration)
{
	// init the loop exit code
	auto exit_code = CALIBRATION_OK;
	fs::path calibration_data_root(R"(C:\ProgramData\FARO\Freestyle\Cameras\DMVS_DMVS011900031_original.xml)");

	if(reload_calibration && !calibration_data_root.empty())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Loading Calibration file from [" << calibration_data_root.string() << "]";
		capture_calibration.LoadCalibrationFromXmlFile(calibration_data_root);
	}

	if(!InitReconstructor())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : InitReconstructor failed.";
		return false;
	}

	// Configure tracking parameters
	if(!ConfigureTracking())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not bring DMVS Device to Tracking State!";
		return false;
	}

	auto plane_fit_radius = 1200;

	// Set calibration values and rectify cameras 3d reconstructor
	reconstructor->use_projector = true;
	reconstructor->SetCalibrationAndPrepare(capture_calibration);
	const auto& laser = capture_calibration.GetCustomMatrix("laser_beams");

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Run Freestyle with " << laser.rows << " dots pattern";
	reconstructor->SetLaserBeamsFromCalibration(laser);

	std::vector<cv::Point3f> triangulated_points;
	std::vector<double> rms;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Starting test loop";

	unsigned long index = 0;
	unsigned long n_frm = 0;
	auto ir_exp_time    = 1.0;

	if(ir_exp_time > 0 && SetExposureTime(ir_exp_time))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Exposure time could not be set";
	}

	//auto val2_color = [](const float val)
	//{
	//	cv::Vec3b color;

	//	if(val < -1.0f)
	//	{
	//		color = cv::Vec3b(255, 0, 0);
	//	}

	//	if(val > 1.0f)
	//	{
	//		color = cv::Vec3b(0, 0, 255);
	//	}

	//	if(val >= 0.0f)
	//	{
	//		color[2] = std::min(int(val * 512.0f), 255);
	//		color[1] = std::min(int((1.0f - val) * 512.0f), 255);
	//		color[0] = 0;
	//	}
	//	else
	//	{
	//		color[0] = std::min(int(-val * 512.0f), 255);
	//		color[1] = std::min(int((1.0f + val) * 512.0f), 255);
	//		color[2] = 0;
	//	}

	//	return color;
	//};

	//m_stop = false;

	//while(!m_stop)
	{
		// update ir exposure time if changed
		if(1.0 != ir_exp_time)
		{
			ir_exp_time = ir_exp_time < 0.07 ? ir_exp_time : 0.07;
			if(!SetExposureTime(ir_exp_time))
			{
				BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set to IrExposureTime " << ir_exp_time;
			}
		}

		// save last and grab new frame
		if(!PushChannelImageToCache())
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : ERROR Grab failed";
			exit_code = !IsLive() ? END_OF_FILE : CALIBRATION_NO_DEVICE;
		}

		//if useAutoIRNormal is false, exposure times are always in search mode, leading to continuous ramping.
		// const bool useAutoIRNormal = SFMCore::get(m_options, L"UseAutoIRInDevLoop", true);
		//const auto useAutoIRNormal = true;

		// get images
		auto retrieve_ok = true;

		// device images (color, IR0, IR1)
		cv::Mat pattern_img0, pattern_img1, color_image;
		retrieve_ok &= RetrieveImageFromCalibrationCache(frame_index, CHANNEL_GREY0, pattern_img0);
		retrieve_ok &= RetrieveImageFromCalibrationCache(frame_index, CHANNEL_GREY1, pattern_img1);

		if(!retrieve_ok)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Retrieve image from Calibration failed, Frame skipped";
			return CALIBRATION_STOPPED_BY_USER;
		}

		color_image = cv::Mat(pattern_img0.rows, pattern_img0.cols, CV_8UC3);

		// find 2d dots
		Dots dots0, dots1;
		DotInfos dots_infos0, dots_infos1;

		if(!FindDots(pattern_img0, pattern_img1, dots0, dots1, !IsLive(), &dots_infos0, &dots_infos1))
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Find Dots Method return false.";
			return CALIBRATION_STOPPED_BY_USER;
		}

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Dots0 count [" << dots0.size() << "] Dots1 count [" << dots1.size() << "]";

		//for use in auto ir, without the circles drawn.
		const auto pattern_img0_orig = pattern_img0.clone();
		cv::Mat pattern_img_0draw, pattern_img_1draw;
		cv::cvtColor(pattern_img0, pattern_img_0draw, cv::COLOR_GRAY2BGR);
		cv::cvtColor(pattern_img1, pattern_img_1draw, cv::COLOR_GRAY2BGR);

		// draw detected IR dots in IR images
		auto if_show_dots = true;

		//if(SFMCore::get(m_options, L"ShowDots", 1) == 1)
		if(if_show_dots)
		{
			for(auto& dot0 : dots0)
			{
				//auto brightness = 1.0 + fabs(dots_infos0[i][0]) / 30.0f;
				circle(pattern_img_0draw, dot0, 1, Constants::DARK_BLUE, 1, CV_AA);
			}

			for(auto& dot1 : dots1)
			{
				circle(pattern_img_1draw, dot1, 1, Constants::DARK_BLUE, 1, CV_AA);
			}

			std::stringstream str0;
			str0 << "Dots0 count[" << dots0.size() << "]";
			std::stringstream str1;
			str1 << "Dots0 count[" << dots1.size() << "]";
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Dots0 count [" << dots0.size() << "] Dots1 count [" << dots1.size() << "]";
			cv::putText(pattern_img_0draw, str0.str(), cv::Point(20, 80), CV_FONT_HERSHEY_SIMPLEX, 0.5, Constants::DARK_BLUE, 1, CV_AA);
			cv::putText(pattern_img_1draw, str1.str(), cv::Point(20, 80), CV_FONT_HERSHEY_SIMPLEX, 0.5, Constants::DARK_BLUE, 1, CV_AA);
			cv::imwrite(Helper::GetCalibrationImagesDirectory().string() + "/pattern_img_0draw.png", pattern_img_0draw);
			cv::imwrite(Helper::GetCalibrationImagesDirectory().string() + "/pattern_img_1draw.png", pattern_img_1draw);
		}

		//cv::Mat current_image;

		// calculate 3d points
		std::vector<int> point_flags;
		if(!reconstructor->TriangulatePoints(dots0, dots1, triangulated_points, point_flags, false, 10, 4))
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No triangulated 3D dots ";
			return CALIBRATION_STOPPED_BY_USER;
		}

		//if(SFMCore::get(m_options, L"ShowDots", 1) == 1)
		if(if_show_dots)
		{
			for(auto& triangulated_point : triangulated_points)
			{
				//auto brightness = 1.0 + fabs(dots_infos0[i][0]) / 30.0f;
				circle(pattern_img0, cv::Point2f(triangulated_point.x, triangulated_point.y), 1, Constants::DARK_BLUE, 1, CV_AA);
			}

			std::stringstream str0;
			str0 << "Triangulated points count[" << triangulated_points.size() << "]";

			cv::putText(pattern_img0, str0.str(), cv::Point(20, 80), CV_FONT_HERSHEY_SIMPLEX, 0.5, Constants::DARK_BLUE, 1, CV_AA);
			cv::imwrite(Helper::GetCalibrationImagesDirectory().string() + "/Triangulated.png", pattern_img0);
		}

		std::ofstream output;
		auto output_path = R"(D:\TestData\output\triangulated_points_)" + Helper::GetTimeStamp() + ".txt";
		output.open(output_path);
		if(output.is_open())
		{
			for(size_t i = 0; i < triangulated_points.size(); i++)
			{
				output << triangulated_points.at(i).x << " "
					<< triangulated_points.at(i).y << " "
					<< triangulated_points.at(i).z << "\n";
			}

			output.close();
		}
		/*std::stringstream stream;
		stream << "Triangulated Dots : " << triangulated_points.size();
		cv::putText(color_image, stream.str(), cv::Point(20, 80), CV_FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255), 1, CV_AA);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Number of found 3D points : " << triangulated_points.size();*/

		//	// Fit a Plane to measured 3d points - rectangle defined in fitPlane
		//	LSFitPlane::PlaneParam plane_params;
		//	std::vector<int> plane_points;
		//	plane_params   = fitPlane(triangulated_points, capture_calibration, plane_points, configuration_options);
		//	auto distance  = (-plane_params.position).dot(plane_params.normal);
		//	auto plane_rms = plane_params.ptDist;
		//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : PlaneFit with " << plane_points.size() << " Points Rms " << plane_rms << " at " << distance
		//<< " m (" <<
		//plane_rms / (distance * distance) << " )";

		//	// projection of 3d points into color camera
		//	std::vector<cv::Point2f> projColor;
		//	auto calibration_color = capture_calibration.GetCalibration(CHANNEL_BGR0);
		//	calibration_color->ProjectPoints(triangulated_points, projColor);

		//	auto& bright_beams = m_reconstructor->getBrightPoints();
		//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Number of bright beams " << bright_beams.size();

		//	bool if_show_dots2 = true;
		//	//if(SFMCore::get(m_options, L"ShowDots", 1) == 1)
		//	if(if_show_dots2)
		//	{
		//		for(size_t i = 0; i < plane_points.size(); i++)
		//		{
		//			auto plane_point_idx    = plane_points[i];
		//			auto triangulated_point = triangulated_points[plane_point_idx];

		//			//Distance to plane
		//			auto distance_to_plane = (cv::Vec3d(triangulated_point.x, triangulated_point.y, triangulated_point.z) - plane_params.position).dot(
		//				plane_params.normal);
		//			auto point_size = 2;

		//			if(fabs(distance_to_plane) > 1.0)
		//			{
		//				point_size = int(2.0 * fabs(distance_to_plane));
		//			}

		//			cv::Vec3b c = val2_color(float(distance_to_plane / 0.003));
		//			circle(color_image, projColor[plane_point_idx], 1, cv::Scalar(c[0], c[1], c[2]), point_size);
		//		}
		//	}

		//	cv::Mat ir0_img, ir1_img, proj_img;
		//	std::vector<cv::Vec4f> ir0_errors, ir1_errors, proj_errors;
		//	auto rms_ir0  = m_reconstructor->getProjectionError(ir0_errors, 0);
		//	auto rms_ir1  = m_reconstructor->getProjectionError(ir1_errors, 1);
		//	auto rms_proj = m_reconstructor->getProjectionError(proj_errors, 2);

		//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : RMS IR0 [" << rms_ir0 << "] IR1 [" << rms_ir1 << "] Proj [" << rms_proj << "]";
		//	rms.push_back((rms_ir0 + rms_ir1) / 2.);

		//	StatisticValSet<double> stats;

		//	for(const auto& val : rms)
		//	{
		//		stats.add(val);
		//	}

		//	stats.finish();
		//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : RMS mean : " << stats.meanVal;

		//	//std::string txt = std::to_string(-1) + " fps - IR0";
		//	//ErrVis::createErrorImg(ir0Img, ir0errors, rmsIr0, 100.0, txt);

		//	//txt = std::to_string(-1) + " fps - IR1";
		//	//ErrVis::createErrorImg(ir1Img, ir1errors, rmsIr1, 100.0, txt);

		//	//txt = std::to_string(-1) + " fps - Projector";
		//	//ErrVis::createErrorImg(projImg, projErrors, rmsProj, 100.0, txt, cv::Point2f(800, 600));
		//	nFrm++;

		//	//switch(SFMCore::get(m_options, L"CurrentImage", 0))
		//	switch(0)
		//	{
		//	case 0: current_image = color_image;
		//		break;
		//	case 1: current_image = pattern_img_0draw;
		//		break;
		//	case 2: current_image = pattern_img_1draw;
		//		break;
		//		//	case 1: cv::resize( ir0Img, currentImg, cv::Size(640, 480) ); break;
		//		//	case 2: cv::resize( ir1Img, currentImg, cv::Size(640, 480) ) ; break;
		//		// case 2: currentImg = projImg ; break;
		//	case 3: current_image = proj_img;
		//		break;
		//	case 4: current_image = ir0_img;
		//		break;
		//	case 5: current_image = ir1_img;
		//		break;
		//	default: current_image = pattern_img_0draw;
		//		break;
		//	}

		//	// Draw the current frame
		//	/*if(false)
		//	{
		//		std::string irStatus = "";
		//		cv::putText(pattern_img_0draw, irStatus, cv::Point(20, 80), CV_FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255), 1, CV_AA);
		//	}*/

		//	//video->update(currentImg);
	}

	if(stop)
	{
		exit_code = CALIBRATION_STOPPED_BY_USER;
	}

	return exit_code;
}

bool DMVSSensor::GrabFromSensor() const
{
	// Sanity checks
	if(!IsOpened())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS Device is not opened ";
		return false;
	}

	if(!IsLive())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS Device is not live ";
		return false;
	}

	if(!cache)
		return false;

	if(!dmvs_device_)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Either no DMVS Device or DMVS Device is not connected ";
		return false;
	}

	if(!dmvs_device_->IsConnected())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS Device is not connected ";
		return false;
	}

	return true;
}

bool DMVSSensor::SetExposureTime(const double exposure) const
{
	if(!IsLive())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS Device is not live ";
		return true;
	}

	if(!dmvs_device_)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Either No DMVS Device or DMVS Device not connected ";
		return false;
	}

	auto const result = dmvs_device_->SetExposureTime(exposure * 1000);

	const auto if_set = result == error::OK;
	if(!if_set)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Error " << result << "Error string " << Helper::GetErrorString(result);
	}
	return if_set;
}

std::string DMVSSensor::GetCalibrationFilename() const
{
	return calibration_filename_;
}

bool DMVSSensor::ConnectToDMVS()
{
	if(dmvs_device_)
	{
		if(dmvs_device_->IsConnected())
		{
			serial_number = dmvs_device_->GetSerialNumber();
			device_type_  = "DMVS";
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Currently connected to : " << dmvs_device_->GetSerialNumber();
			return true;
		}

		Release();
	}

	// If there is also dmvs connected, then no need to execute following code.
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No device is currently connected. Attempting to connect....";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : FARO DMVS SDK Version: " << GetSDKVersion();
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Detecting Devices...";

	// Creates a list of DMVSDeviceInfo structs.
	// The DMVSDeviceList object is later used to create instances of DMVSDevice* with the create
	// function.
	DMVSDeviceList* dmvs_device_list = DetectAllDMVSDevices();

	if(dmvs_device_list->Size() == 0)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No DMVS Device detected ";
		return false;
	}

	if(dmvs_device_list->Size())
	{
		// We use this to get the DMVS specified by the command line
		//auto connections = SearchConnectionsForSpecificDMVS();

		auto connection_flag = false;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << dmvs_device_list->Size() << " DMVS  Device(s) detected ";

		for(auto i = 0; i < dmvs_device_list->Size(); ++i)
		{
			auto current_dmvs = dmvs_device_list->At(i);
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Try to connect to DMVS at position [" << i << "]";
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Device Name [" << current_dmvs->serial << "]";
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Version [" << current_dmvs->hardware_version << "]";
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : IP Address [" << current_dmvs->ip << "]";
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Serial Number [" << current_dmvs->serial << "]";
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Head Version [" << current_dmvs->head_version << "]";
			std::string searched_serial;

			/*if(connections.size() == 1)
				searchedSerial = connections.begin()->serial;*/

			if(!searched_serial.empty())
			{
				if(strcmp(current_dmvs->serial, searched_serial.c_str()) != 0)
				{
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Avoid Connecting on other Devices";
					continue;
				}
			}

			// Create first DMVSDevice in DMVSDeviceList
			dmvs_device_ = dmvs_device_list->CreateDeviceFromInfo(current_dmvs);

			// verify creation of DMVSDevice
			if(!dmvs_device_)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Device with serial not found ";
				continue;
			}

			// Connects to the actual device. The parameter true resets the device parameters.
			if(dmvs_device_->Connect() != error::OK)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not connect to DMVS Device";
				continue;
			}

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Connection established to " << dmvs_device_->GetSerialNumber();
			connection_flag = true;
			break;
		}

		if(!connection_flag)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Connection Failed";
			Release();
			return false;
		}
	}
	delete dmvs_device_list;

	serial_number = dmvs_device_->GetSerialNumber();
	device_type_  = "DMVS";

	if(dmvs_device_->IsConnected())
		return true;

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS is not connected properly it seems!";

	return false;
}

bool DMVSSensor::ConfigureTracking() const
{
	// configure tracking params here
	return SetScanSettings();
}

bool DMVSSensor::Configure() const
{
	if(!dmvs_device_)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Either No DMVS Device or DMVS Device not connected ";
		return false;
	}

	// Common Settings
	if(mode_ == data::Mode2D)
	{
		if(dmvs_device_->StopImageAcquisition() && dmvs_device_->SetMode(data::Mode2D) != error::OK)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: Mode2D";
			return false;
		}
	}
	else if(mode_ == data::Mode3D)
	{
		if(dmvs_device_->StopImageAcquisition() && dmvs_device_->SetMode(data::Mode3D) != error::OK)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: Mode3D";
			return false;
		}
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : INVALID MODE " << mode_;
		return false;
	}

	if(dmvs_device_->SetExposureTime(exposure_time_) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: EXPOSURE_TIME";
		return false;
	}

	if(dmvs_device_->SetFramerate(frame_rate_) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: FRAME_RATE";
		return false;
	}

	if(dmvs_device_->SetLEDPower0(led_power_0_) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: LED_POWER_0";
		return false;
	}

	if(dmvs_device_->SetLEDPower1(led_power_1_) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: LED_POWER_1";
		return false;
	}

	if(dmvs_device_->SetPositionInterfaceActive(position_interface_active_) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: POSITION_INTERFACE_ACTIVE";
		return false;
	}

	// if (dmvs_device_->setInputTrigger(INPUT_TRIGGER) != error::OK)
	//	BOOST_LOG_TRIVIAL(info) <<("");
	// if (dmvs_device_->setInputTriggerStatus(INPUT_TRIGGER_STATUS) != error::OK)
	//	BOOST_LOG_TRIVIAL(info) <<("");
	// if (dmvs_device_->setOutputTriggerStatus(OUTPUT_TRIGGER_STATUS) != error::OK)
	//	BOOST_LOG_TRIVIAL(info) <<("");
	// if (dmvs_device_->setSequencerExposureTime(SEQUENCER_EXPOSURE_TIME) != error::OK)
	//	BOOST_LOG_TRIVIAL(info) <<("");
	// if (dmvs_device_->setSequencerMode(SEQUENCER_MODE) != error::OK)
	//	BOOST_LOG_TRIVIAL(info) <<("");
	// if (dmvs_device_->setSequencerSetCount(SEQUENCER__SET_COUNT) != error::OK)
	//	BOOST_LOG_TRIVIAL(info) <<("");

	// Exclusive 2D Settings
	data::DeviceMode current_mode;
	const auto if_ok = dmvs_device_->GetMode(current_mode) == error::OK;

	if(if_ok && current_mode == data::Mode2D)
	{
		if(dmvs_device_->SetAutoExposure(auto_exposure_) != error::OK)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: AUTO_EXPOSURE";
			return false;
		}

		if(dmvs_device_->SetLaser(set_laser_) != error::OK)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: SET_LASER";
			return false;
		}

		if(dmvs_device_->SetLEDFlashConstant(set_led_flash_constant_) != error::OK)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: SET_LED_FLASH_CONSTANT";
			return false;
		}
	}

	return true;
}

bool DMVSSensor::RetrieveGrey(size_t channel,
                              cv::Mat& img)
{
	return false;
}

bool DMVSSensor::UploadCalibration()
{
	if(!dmvs_device_)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Either no DMVS Device or DMVS Device not connected ";
		return false;
	}

	// Determine current calibration timestamp
	auto camera_path = Helper::GetCamerasDirectory();

	if(CreateDirectory(camera_path.wstring().c_str(), nullptr) || ERROR_ALREADY_EXISTS == GetLastError())
	{
		fs::path current_filename("DMVS_");
		current_filename.append(dmvs_device_->GetSerialNumber());
		current_filename.append(".xml");

		const auto error = dmvs_device_->UploadFactoryCalibration(camera_path.append(current_filename.c_str()).string().c_str());

		if(error == error::OK)
		{
			calibration_filename_ = current_filename.string();
			return true;
		}

		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not upload calibration for DMVS device";
	}

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not create directory";

	return false;
}

bool DMVSSensor::CreateDeviceCalibrationFile()
{
	// Determine current calibration timestamp
	auto camera_path = Helper::GetCamerasDirectory();
	if(fs::is_directory(camera_path))
	{
		const auto calibration_filename         = "DMVS_" + std::string(dmvs_device_->GetSerialNumber()) + ".xml";
		const auto calibration_filepath         = camera_path / fs::path(calibration_filename);
		const auto default_calibration_filepath = camera_path / fs::path("dmvs_calibration_template.xml");

		if(fs::exists(default_calibration_filepath))
		{
			fs::copy_file(default_calibration_filepath, camera_path / calibration_filename, fs::copy_option::overwrite_if_exists);

			if(fs::exists(camera_path / calibration_filename))
			{
				calibration_filename_ = calibration_filename;
				return true;
			}

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not rename calibration file " << default_calibration_filepath << " to " <<
 calibration_filename;

			return false;
		}


		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Default calibration file " << default_calibration_filepath << " does not exist";
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Directory " << camera_path << " does not exist";
	}

	return false;
}

//bool DMVSSensor::DownloadCalibration()
//{
//	if(!dmvs_device_)
//	{
//		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is not available";
//		return false;
//	}
//
//	// Determine current calibration timestamp
//	auto camera_path             = Helper::GetCamerasDirectory();
//	const auto camera_path_exits = fs::is_directory(camera_path) ? true : fs::create_directories(camera_path);
//
//	if(camera_path_exits)
//	{
//		const auto calibration_filename = "DMVS_" + std::string(dmvs_device_->GetSerialNumber()) + ".xml";
//		const auto calibration_filepath = camera_path / fs::path(calibration_filename);
//		const auto temp_directory_path  = camera_path / fs::path("temp");
//
//		if(!fs::is_directory(temp_directory_path))
//		{
//			fs::create_directory(temp_directory_path);
//		}
//
//		// Create Temp Directory to check current Device calibration Timestamp
//		if(fs::is_directory(temp_directory_path))
//		{
//			char out_filename[80];
//			const auto error = dmvs_device_->DownloadCalibration(temp_directory_path.string().c_str(), out_filename);
//
//			if(error == error::OK)
//			{
//				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Downloaded Calibration file [" << out_filename << "] to temp directory path [" <<
// temp_directory_path << "]";
//
//				device_calibration_filename_ = fs::path(out_filename);
//
//				const auto& file_path(temp_directory_path);
//				fs::rename(temp_directory_path / out_filename, camera_path / calibration_filename);
//
//				if(fs::exists(camera_path / calibration_filename))
//				{
//					calibration_filename_ = calibration_filename;
//					return true;
//				}
//
//				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not rename calibration file " << out_filename << " to " <<
// calibration_filename;
//				return false;
//			}
//
//			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not download calibration file for DMVS device";
//
//			return false;
//		}
//	}
//
//	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not create directory " << camera_path;
//
//	return false;
//}

bool DMVSSensor::SetBrightPointSettings() const
{
	return SetScanSettings();
}

std::shared_ptr<const CameraSensor> DMVSSensor::GetDMVSCameraCameraCalibration(const std::string& name,
                                                                               ImageChannel channel,
                                                                               const CameraModel camera_model,
                                                                               const cv::Vec2d& depth_params,
                                                                               const cv::Vec3d& depth_params_exp,
                                                                               const ApplicationSettings& settings,
                                                                               const cv::Mat& camera_matrix,
                                                                               const cv::Mat& distortion_vector,
                                                                               const cv::Mat& rotation_matrix,
                                                                               const cv::Mat& translation_vector)
{
	auto depth_dependent_calibration = std::make_shared<CameraSensorDepthDependent>(name,
	                                                                                channel,
	                                                                                depth_params,
	                                                                                depth_params_exp,
	                                                                                camera_matrix,
	                                                                                distortion_vector,
	                                                                                rotation_matrix,
	                                                                                translation_vector);

	depth_dependent_calibration->KeepExtrinsicParametersFix(settings.extrinsic);
	depth_dependent_calibration->KeepFocalLengthFix(settings.focal_length);
	depth_dependent_calibration->KeepDistortionFix(settings.distortion);
	depth_dependent_calibration->KeepDepthDependentExpParam1Fix(settings.dd_exp1);
	depth_dependent_calibration->KeepDepthDependentExpParam2Fix(settings.dd_exp2);
	depth_dependent_calibration->KeepDepthDependentExpParam3Fix(settings.dd_exp3);
	depth_dependent_calibration->KeepDepthDependentScalarParam1Fix(settings.dd_scalar1);
	depth_dependent_calibration->KeepDepthDependentScalarParam2Fix(settings.dd_scalar2);

	//std::shared_ptr<const CameraSensor> default_calibration;

	//switch(camera_model)
	//{
	//case 0:
	//	default_calibration = depth_dependent_calibration;
	//	break;
	//case 1:
	//	default_calibration = std::make_shared<SingleCameraCalibration>(camera_matrix, distortion_vector, rotation_matrix, translation_vector);
	//	break;
	//case 2:
	//	//defaultCal = scvCal;
	//	//break;
	//	//	case 3: defaultCal = ddCal01; break;
	//default:
	//	default_calibration = std::make_shared<CameraSensorDepthDependent>(name,
	//	                                                                   channel,
	//	                                                                   depth_params,
	//	                                                                   depth_params_exp,
	//	                                                                   camera_matrix,
	//	                                                                   distortion_vector,
	//	                                                                   rotation_matrix,
	//	                                                                   translation_vector);
	//}

	return depth_dependent_calibration;
}

void DMVSSensor::UseFrameCB(size_t type,
                            void* data)
{
	static_cast<DMVSSensor*>(data)->use_frame_ = true;
}

bool DMVSSensor::IsSupportedSensor()
{
	auto* const dmvs_list = dmvs_sdk::DetectAllDMVSDevices();

	if(dmvs_list->Size() == 0)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No DMVS Devices found";
		delete dmvs_list;
		return false;
	}

	for(auto i = 0; i < dmvs_list->Size(); ++i)
	{
		if(strcmp(dmvs_list->At(i)->serial, "N/A") != 0)
		{
			delete dmvs_list;
			return true;
		}
	}

	delete dmvs_list;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Invalid serial number as DMVS Serial Number contains N/A";
	return false;
}

void DMVSSensor::UpdateReconstructor()
{
	if(!reconstructor)
	{
		reconstructor = std::make_shared<ReconstructorLaserDots3DBackProjection>();
	}

	if(!reconstructor)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() reconstructor NULL pointer ";
		return;
	}

	const auto& laser = capture_calibration.GetCustomMatrix("laser_beams");

	reconstructor->SetCalibrationAndPrepare(capture_calibration);
	if(!laser.empty())
	{
		reconstructor->use_projector = true;
		reconstructor->SetLaserBeamsFromCalibration(laser);
	}
}

bool DMVSSensor::InitReconstructor()
{
	UpdateReconstructor();

	if(!reconstructor)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : reconstructor NULL pointer";
		return false;
	}

	if(!reconstructor->Initialize(configuration_options))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : reconstructor->Initialize() failed";
		return false;
	}

	if(IsLive())
	{
		reconstructor->SetAutoCompensation(1);
		reconstructor->SetProjectorAutoCompensationLimit(0.01);
	}
	else
	{
		reconstructor->SetAutoCompensation(3);
		reconstructor->SetProjectorAutoCompensationLimit(0.001);
	}

	return reconstructor->Preparation();
}

bool DMVSSensor::FindDots(const cv::Mat& pattern_img0,
                          const cv::Mat& pattern_img1,
                          Dots& dots0,
                          Dots& dots1,
                          const bool use_edge_improvement,
                          DotInfos* dots0_infos,
                          DotInfos* dots1_infos)
{
	// Needed to take parameter initialization with SFM::getOption out off tbb parallelization
	// because  SFM::Plugin::getOption is not thread-safe
	LaserDotDetector2D::Params params;
	params.peak_neighbor_step   = 0;
	params.bg_limit             = 1;
	params.mean_peak_factor     = 2.5;
	params.neighbor_peak_factor = 1.0;
	params.edge_dot_detection   = use_edge_improvement;
	params.sub_pixel_patch_size = 5;
	params.dot_blur             = 5;
	params.sigma_dot_blur       = 0.5;
	params.inner_ring           = 3;
	params.outer_ring           = 4;
	params.use_all_candidates   = false;

	// find_dots itself is already partially tbb parallelized
	// experiment has shown that another parallelization at this level
	// leads to a significant speed-up at least at 4(8HT)core CPU of (approx 35-40%)
	// We can get some more speed-up by splitting both images (GH)
	tbb::parallel_invoke([&]
	                     {
		                     LaserDotDetector2D ldd0(pattern_img0, params);
		                     ldd0.FindLaserDots(dots0, dots0_infos);
	                     },
	                     [&]
	                     {
		                     LaserDotDetector2D ldd1(pattern_img1, params);
		                     ldd1.FindLaserDots(dots1, dots1_infos);
	                     });

	return !(dots0.empty() && dots1.empty());
}

void DMVSSensor::CaptureImageAndFindPlaneByMarkers(cv::Vec3d& plane_position,
                                                   cv::Vec3d& plane_normal,
                                                   ProjectorBoardDetector& board_detector,
                                                   std::map<size_t, CircularMarker>& detected_markers)
{
	cv::Mat pattern_image0, pattern_image12;
	size_t max_marker_count = 0;
	auto last_rms           = 0.0;
	const auto trial_count  = 10;
	std::map<size_t, CircularMarker> detected_markers_map;

	for(auto trial_idx = 0; trial_idx < trial_count; ++trial_idx)
	{
		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Detecting Marker Images [" << trial_idx << "] of [" << trial_count << "]";

		// VgFreestyle2 synced grab. Only grab and save an image if grab(bool synced = true) is called
		if(!PushChannelImageToCache())
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Image not pushed to cache";
			break;
		}

		// get images
		if(!RetrieveImageFromCalibrationCache(frame_index, CHANNEL_GREY0, pattern_image0))
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : CHANNEL [" << CHANNEL_GREY0 << "] Image could not be retrieved. Frame Index [" <<
 frame_index << "]";
		}

		//if(!RetrieveImageFromCalibrationCache(frame_index, CHANNEL_GREY1, pattern_image1))
		//{
		//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : CHANNEL [" << CHANNEL_GREY1 << "] image could not be retrieved. Frame Index [" << frame_index << "]";
		//}

		// Grey0 is now reference use this for marker Detection
		auto detected_board_markers = board_detector.Detect(pattern_image0);

		if(!detected_board_markers)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No markers detected. Going to next iteration...";
			continue;
		}

		std::map<size_t, CircularMarker> marker_map;
		board_detector.GetBoardMarkerMap(marker_map);
		const auto marker_size_current = marker_map.size();

		if(marker_size_current < Constants::MIN_REQUIRED_MARKERS)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Detected only [" << marker_map.size() << "] markers. At least [ 4 ] markers required.";
			continue;
		}

		const auto rms_current = board_detector.GetRMS();

		if(marker_size_current > max_marker_count || marker_size_current == max_marker_count && rms_current < last_rms)
		{
			cv::Mat translation_vector;
			cv::Mat rotation_vector;
			board_detector.GetOrientation(rotation_vector, translation_vector);

			//plane_position = translation_vector.reshape(3).at<cv::Vec3d>();
			//plane_normal = rotation_vector.reshape(3).at<cv::Vec3d>();

			cv::Mat rotation_matrix(3, 3, CV_64F);
			cv::Rodrigues(rotation_vector, rotation_matrix);
			plane_position       = cv::Vec3d(translation_vector.at<double>(0), translation_vector.at<double>(1), translation_vector.at<double>(2));
			plane_normal         = cv::Vec3d(rotation_matrix.at<double>(2), rotation_matrix.at<double>(5), rotation_matrix.at<double>(8));
			detected_markers_map = marker_map;
			max_marker_count     = marker_size_current;
			last_rms             = board_detector.GetRMS();
		}

		marker_map.clear();
	}

	detected_markers = detected_markers_map;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Successfully Detected Plate Position. Detected [" << detected_markers_map.size() << "] Markers";
}


std::string DMVSSensor::GetPositionString(const TablePosition position)
{
	switch(position)
	{
	case START:
		return "imagestart_";
	case CENTER:
		return "imagemiddle_";
	case END:
		return "imageend_";
	default:
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Table position is not known";
		return "-1";
	}
}

void DMVSSensor::CaptureImageAndFindPlaneByMarkersOffline(cv::Vec3d& plane_position,
                                                          cv::Vec3d& plane_normal,
                                                          ProjectorBoardDetector& board_detector,
                                                          std::map<size_t, CircularMarker>& detected_markers,
                                                          TablePosition position) const
{
	cv::Mat pattern_image0, pattern_image12;
	size_t max_marker_count = 0;
	auto last_rms           = 0.0;
	const auto trial_count  = 10;
	std::map<size_t, CircularMarker> detected_markers_map;

	auto position_idx = GetPositionString(position);

	for(auto idx = 0; idx < trial_count; ++idx)
	{
		auto folder = Helper::GetCamerasDirectory() / "FindTablePosition";
		if(!fs::is_directory(folder))
			fs::create_directories(folder);

		auto file_path = folder.string() + R"(\)" + GetPositionString(position) + std::to_string(idx) + ".png";

		if(!fs::is_regular_file(file_path))
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : File does not exist " << file_path;
			continue;
		}

		pattern_image0 = cv::imread(file_path);

		// Grey0 is now reference use this for marker Detection
		auto detected_board_markers = board_detector.Detect(pattern_image0);

		if(!detected_board_markers)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No markers detected. Going to next iteration...";
			continue;
		}

		std::map<size_t, CircularMarker> marker_map;
		board_detector.GetBoardMarkerMap(marker_map);
		const auto marker_size_current = marker_map.size();

		if(marker_size_current < Constants::MIN_REQUIRED_MARKERS)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Detected only [" << marker_map.size() << "] markers. At least [ 4 ] markers required.";
			continue;
		}

		const auto rms_current = board_detector.GetRMS();

		if(marker_size_current > max_marker_count || marker_size_current == max_marker_count && rms_current < last_rms)
		{
			cv::Mat translation_vector;
			cv::Mat rotation_vector;
			board_detector.GetOrientation(rotation_vector, translation_vector);
			cv::Mat rotation_matrix(3, 3, CV_64F);
			cv::Rodrigues(rotation_vector, rotation_matrix);
			plane_position       = cv::Vec3d(translation_vector.at<double>(0), translation_vector.at<double>(1), translation_vector.at<double>(2));
			plane_normal         = cv::Vec3d(rotation_matrix.at<double>(2), rotation_matrix.at<double>(5), rotation_matrix.at<double>(8));
			detected_markers_map = marker_map;
			max_marker_count     = marker_size_current;
			last_rms             = board_detector.GetRMS();
		}

		marker_map.clear();
	}

	detected_markers = detected_markers_map;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Successfully Detected Plate Position. Detected [" << detected_markers_map.size() << "] Markers";
}

void DMVSSensor::FindTablePositionRep(DMVSSensor* capture_device,
                                      IKinematics* linear_stage,
                                      ProjectorBoardDetector& board_detector,
                                      std::map<size_t, CircularMarker>& first_image_marker,
                                      std::map<size_t, CircularMarker>& second_image_marker,
                                      const double step,
                                      std::vector<cv::Vec3d>& plane_positions,
                                      std::vector<cv::Vec3d>& plane_normals)
{
	cv::Vec3d plane_position, plane_normal;
	bool enough_identical_markers_found = false;

	plane_positions.reserve(10);
	plane_normals.reserve(10);

	for(size_t i = 0; i < 10; i++)
	{
		if(capture_device->IsLive())
		{
			if(linear_stage->MoveXAxis(step * i))
			{
				linear_stage->WaitForMovementToFinish();
			}

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Linear Stage  position at " << *linear_stage->GetPosition();
		}

		while(!enough_identical_markers_found)
		{
			CaptureImageAndFindPlaneByMarkers(plane_position,
			                                  plane_normal,
			                                  board_detector,
			                                  second_image_marker);

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_normal_middle " << plane_normal;
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_position_middle " << plane_position;

			// Now check if we have at least 4 Identical Markers
			auto number_same_marker = 0;
			for(auto it = second_image_marker.begin(); it != second_image_marker.end(); ++it)
			{
				if(first_image_marker.find(it->second.id) != first_image_marker.end())
				{
					number_same_marker++;
				}
			}

			if(number_same_marker > 3)
			{
				enough_identical_markers_found = true;
			}
		}
		second_image_marker.clear();
		enough_identical_markers_found = false;
		plane_positions[i]             = plane_position;
		plane_normals[i]               = plane_normal;

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Detecting first position" << plane_positions[i];
	}
}

bool DMVSSensor::FindTablePosition(IKinematics* linear_stage,
                                   cv::Vec3d& plane_position_start,
                                   cv::Vec3d& plane_position_end,
                                   cv::Vec3d& plane_normal_start,
                                   cv::Vec3d& plane_normal_end,
                                   ProjectorBoardDetector& board_detector,
                                   std::map<size_t, CircularMarker>& image1_markers,
                                   std::map<size_t, CircularMarker>& image2_markers)
{
	if(IsLive())
	{
		if(linear_stage->MoveXAxis(Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START))
		{
			linear_stage->WaitForMovementToFinish();
		}

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Table Position - Start " << linear_stage->GetPosition()->at<double>(0, 3);
		// Todo get and save position to Frame
		//linear_table_position_start = Helper::GetLinearStagePosition(linear_stage);
		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Linear Stage Position Start " << linear_table_position_start;
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS is not live ";

		return false;
	}

	if(!Stop())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not stop Grab";
		return false;
	}

	if(!SetCameraCalibrationSettings(false))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : SetCameraCalibrationSettings() failed";
		return false;
	}

	// Take Picture and identify Marker
	CaptureImageAndFindPlaneByMarkers(plane_position_start,
	                                  plane_normal_start,
	                                  board_detector,
	                                  image1_markers);

	if(image1_markers.size() < 4)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Insufficient markers found [" << image1_markers.size() << "]";
		return false;
	}

	if(IsLive())
	{
		if(linear_stage->MoveXAxis(Constants::LINEAR_STAGE_PLATE_DETECTION_MIDDLE_POSITION))
		{
			linear_stage->WaitForMovementToFinish();
		}

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Table Position - Middle " << linear_stage->GetPosition()->at<double>(0, 3);
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS is not live ";

		return false;
	}

	cv::Vec3d plane_position_middle, plane_normal_middle;
	auto enough_identical_markers_found = false;

	while(!enough_identical_markers_found)
	{
		CaptureImageAndFindPlaneByMarkers(plane_position_middle,
		                                  plane_normal_middle,
		                                  board_detector,
		                                  image2_markers);

		// Now check if we have at least 4 Identical Markers
		auto number_same_marker = 0;

		for(const auto& image2_marker : image2_markers)
		{
			if(image1_markers.find(image2_marker.second.id) != image1_markers.end())
			{
				number_same_marker++;
			}
		}

		if(number_same_marker > 3)
		{
			enough_identical_markers_found = true;
		}
	}

	if(IsLive())
	{
		if(linear_stage->MoveXAxis(Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_END))
		{
			linear_stage->WaitForMovementToFinish();
		}

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Table Position - End " << linear_stage->GetPosition()->at<double>(0, 3);
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS is not live ";

		return false;
	}

	image2_markers.clear();
	enough_identical_markers_found = false;

	while(!enough_identical_markers_found)
	{
		CaptureImageAndFindPlaneByMarkers(plane_position_end,
		                                  plane_normal_end,
		                                  board_detector,
		                                  image2_markers);

		// Now check if we have at least 4 Identical Markers
		auto number_same_marker = 0;

		for(const auto& image2_marker : image2_markers)
		{
			if(image1_markers.find(image2_marker.second.id) != image1_markers.end())
			{
				number_same_marker++;
			}
		}

		if(number_same_marker > 3)
		{
			enough_identical_markers_found = true;
		}
	}

	// Calculate Plate Position
	if(IsLive())
	{
		// Now move back to origin
		if(linear_stage->MoveXAxis(Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START))
		{
			linear_stage->WaitForMovementToFinish();
		}
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS is not live ";

		return false;
	}

	/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_position_start " << plane_position_start;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_position_middle " << plane_position_middle;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_position_end " << plane_position_end;*/

	std::vector<cv::Point3d> positions{cv::Point3d(plane_position_start), cv::Point3d(plane_position_middle), cv::Point3d(plane_position_end)};
	ProjectorCalibrationHelper::AveragePositions(positions, plane_position_start, plane_position_end);

	if(!Stop())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not stop Grab";
		return false;
	}

	if(!SetScanSettings())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : SetScanSettings() failed";
		return false;
	}

	return true;
}


bool DMVSSensor::FindTablePositionOffline(IKinematics* linear_stage,
                                          cv::Vec3d& plane_position_start,
                                          cv::Vec3d& plane_position_end,
                                          cv::Vec3d& plane_normal_start,
                                          cv::Vec3d& plane_normal_end,
                                          ProjectorBoardDetector& board_detector,
                                          std::map<size_t, CircularMarker>& image1_markers,
                                          std::map<size_t, CircularMarker>& image2_markers)
{
	// Take Picture and identify Marker
	CaptureImageAndFindPlaneByMarkersOffline(plane_position_start,
	                                         plane_normal_start,
	                                         board_detector,
	                                         image1_markers,
	                                         START);

	if(image1_markers.size() < 4)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Insufficient markers found [" << image1_markers.size() << "]";
		return false;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_normal_start " << plane_normal_start << "  " << Constants::CHECK_ME;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_position_start " << plane_position_start << "  " << Constants::CHECK_ME;

	cv::Vec3d plane_position_middle, plane_normal_middle;
	auto enough_identical_markers_found = false;

	while(!enough_identical_markers_found)
	{
		CaptureImageAndFindPlaneByMarkersOffline(plane_position_middle,
		                                         plane_normal_middle,
		                                         board_detector,
		                                         image2_markers,
		                                         CENTER);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_normal_middle " << plane_normal_middle << "  " << Constants::CHECK_ME;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_position_middle " << plane_position_middle << "  " << Constants::CHECK_ME;

		// Now check if we have at least 4 Identical Markers
		auto number_same_marker = 0;

		for(const auto& image2_marker : image2_markers)
		{
			if(image1_markers.find(image2_marker.second.id) != image1_markers.end())
			{
				number_same_marker++;
			}
		}

		if(number_same_marker > 3)
		{
			enough_identical_markers_found = true;
		}
	}

	image2_markers.clear();
	enough_identical_markers_found = false;

	while(!enough_identical_markers_found)
	{
		CaptureImageAndFindPlaneByMarkersOffline(plane_position_end,
		                                         plane_normal_end,
		                                         board_detector,
		                                         image2_markers,
		                                         END);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_normal_end " << plane_normal_end << "  " << Constants::CHECK_ME;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_position_end " << plane_position_end << "  " << Constants::CHECK_ME;

		// Now check if we have at least 4 Identical Markers
		auto number_same_marker = 0;

		for(const auto& image2_marker : image2_markers)
		{
			if(image1_markers.find(image2_marker.second.id) != image1_markers.end())
			{
				number_same_marker++;
			}
		}

		if(number_same_marker > 3)
		{
			enough_identical_markers_found = true;
		}
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_position_start " << plane_position_start;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_position_middle " << plane_position_middle;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : plane_position_end " << plane_position_end;

	std::vector<cv::Point3d> positions{cv::Point3d(plane_position_start), cv::Point3d(plane_position_middle), cv::Point3d(plane_position_end)};
	ProjectorCalibrationHelper::AveragePositions(positions, plane_position_start, plane_position_end);

	return true;
}

bool DMVSSensor::SetLedPower(const unsigned int power) const
{
	if(!IsLive())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS Device is not live ";
		return true;
	}

	if(!dmvs_device_)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Either No DMVS Device or DMVS Device not connected ";
		return false;
	}

	const auto percent_cut = std::min(power, 100U);

	if(dmvs_device_->SetLEDPower0(percent_cut) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: LED_POWER_0";
		return false;
	}

	if(dmvs_device_->SetLEDPower1(percent_cut) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS could not set parameter: LED_POWER_1";
		return false;
	}

	return true;
}

void DMVSSensor::CalculateNewCoordinateSystem(cv::Mat& rotation,
                                              cv::Mat& translation,
                                              CaptureCalibration& calibration) const
{
	// Now we adjust our rotations
	cv::Mat r_grey0, r_grey1, t_grey0, t_grey1, r_proj, t_proj;
	capture_calibration.GetCalibration(CHANNEL_GREY0)->GetExtrinsicParameters(r_grey0, t_grey0);
	capture_calibration.GetCalibration(CHANNEL_GREY1)->GetExtrinsicParameters(r_grey1, t_grey1);
	capture_calibration.GetCalibration(CHANNEL_PROJECTOR0)->GetExtrinsicParameters(r_proj, t_proj);
	cv::Mat direction = t_grey1 - t_grey0;

	cv::Vec3d direction_vector(direction.at<double>(0, 0),
	                           direction.at<double>(1, 0),
	                           direction.at<double>(2, 0));

	auto test = r_proj.col(2);
	cv::Vec3d projector_z_direction(r_proj.col(2).at<double>(0, 0), r_proj.col(2).at<double>(1, 0), r_proj.col(2).at<double>(2, 0));
	projector_z_direction = cv::normalize(projector_z_direction);

	auto projector_x_direction = direction_vector - direction_vector.dot(projector_z_direction) * projector_z_direction;
	projector_x_direction      = cv::normalize(projector_x_direction);
	auto projector_y_direction = projector_x_direction.cross(projector_z_direction);
	projector_y_direction      = cv::normalize(projector_y_direction);

	cv::Vec3d camera0_updirection(r_grey0.col(1).at<double>(0, 0), r_grey0.col(1).at<double>(1, 0), r_grey0.col(1).at<double>(2, 0));

	// We want to look in the same direction upwards as the camera
	if(std::acos(camera0_updirection.dot(projector_y_direction)) > 0.8 * M_PI)
	{
		projector_y_direction = - projector_y_direction;
	}

	rotation = r_proj;
	for(auto i = 0; i < 3; ++i)
	{
		rotation.at<double>(i, 0) = projector_x_direction[i];
		rotation.at<double>(i, 1) = projector_y_direction[i];
	}

	translation = t_proj;
}

void DMVSSensor::IncreaseImageCounter(size_t type,
                                      void* data)
{
	auto& options = static_cast<DMVSSensor*>(data)->configuration_options;
	auto current  = 0;
	current       = ++current % 6;
}

bool DMVSSensor::Connect()
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Connect " << Constants::LOG_START;

	if(dmvs_device_)
	{
		return dmvs_device_->IsConnected() ? true : Release();
	}

	const auto dmvs_list = DetectAllDMVSDevices();

	if(dmvs_list->Size() == 0)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : No Device(s) Found ";
		return false;
	}

	auto is_connected         = false;
	const auto dmvs_list_size = dmvs_list->Size();

	for(auto idx = 0; idx < dmvs_list_size; ++idx)
	{
		DMVSDeviceInfo* current_dmvs = dmvs_list->At(idx);

		if(current_dmvs /*&& current_dmvs-> == "2A000058"*/)
		{
			dmvs_device_ = dmvs_list->CreateDeviceFromInfo(current_dmvs);
			if(dmvs_device_ && dmvs_device_->Connect() == error::OK)
			{
				is_connected = true;
				break;
			}
		}
	}

	if(!is_connected)
	{
		Release();
	}

	delete dmvs_list;

	if(is_connected)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Connected with DMVS Device [" << dmvs_device_->GetSerialNumber() << "]";
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not get connected with any DMVS Device ";
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Connect " << Constants::LOG_END;

	return is_connected;
}

bool DMVSSensor::Release()
{
	if(!dmvs_device_)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No DMVS Device available ";
		return true;
	}

	if(dmvs_device_->IsConnected())
	{
		if(dmvs_device_->StopImageAcquisition() != error::OK)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not stop Image Acquisition";
		}

		if(dmvs_device_->Unregister2DImageCallBack() != error::OK)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not un-register 2D Image Callback";
		}

		if(dmvs_device_->Unregister3DImageCallBack() != error::OK)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not un-register 3D Image Callback";
		}

		if(dmvs_device_->Disconnect() != error::OK)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not disconnect DMVS Device - retry";
		}
		else
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Device disconnected";
		}
	}

	delete dmvs_device_;
	dmvs_device_ = nullptr;

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Connect " << Constants::LOG_END;
	return true;
}

bool DMVSSensor::Start() const
{
	if(!IsLive())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is not live ";
		return true;
	}

	const auto error_message = dmvs_device_->StartImageAcquisition();

	if(error_message != error::OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Error Starting Acquisition " << error_message;
		return false;
	}

	return true;
}

bool DMVSSensor::Run() const
{
	if(!IsLive())
	{
		return true;
	}

	const auto error_message = dmvs_device_->StartImageAcquisition();

	if(error_message != error::OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Error starting Acquisition " << error_message;
	}

	return error_message == error::OK;
}

bool DMVSSensor::Stop() const
{
	if(!dmvs_device_)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Either No DMVS Device or DMVS Device not connected ";
		return false;
	}

	if(!IsLive())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is not live ";
		return true;
	}

	const auto error_message = dmvs_device_->StopImageAcquisition();

	if(error_message != error::OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Error stopping acquisition " << error_message;
		return false;
	}

	return true;
}

bool DMVSSensor::GrabFrame()
{
	//Sanity checks
	if(!IsOpened())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is not opened ";
		return false;
	}

	//if(!IsLive())
	//{
	//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Grabbing from File ";
	//	return GrabFromFile();
	//}

	if(IsLive())
	{
		// Wait for a new frame. The grabbing is always synchronized.
		for(auto i = 0; i < 100 && frame_index >= frame_grabber_count_; ++i)
			SleepEx(Constants::DMVS_GRAB_WAIT_TIME_MILLISECONDS, TRUE);

		//No new frame in 1/2 second
		if(frame_index >= frame_grabber_count_)
			return false;

		//Set the new frame number
		frame_index = frame_grabber_count_;

		return true;
	}

	//auto if_grabbed = false;
	//while(IsLive() && !(frame_index >= frame_grabber_count_))
	//{
	//	// Wait for a new frame. The grabbing is always synchronized.
	//	SleepEx(Constants::DMVS_GRAB_WAIT_TIME_MILLISECONDS, TRUE);

	//	//Set the new frame number
	//	if(frame_index < frame_grabber_count_)
	//	{
	//		frame_index = frame_grabber_count_;
	//		if_grabbed = true;
	//	}
	//}

	return false;
}

bool DMVSSensor::RetrieveImageFromCalibrationCache(const size_t frame,
                                                   const size_t channel,
                                                   cv::Mat& image) const
{
	if(!IsOpened())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is not opened";
		return false;
	}

	auto if_retrieved = false;

	switch(channel)
	{
	case CHANNEL_GREY0:
		if_retrieved = cache->Get(frame, Constants::GREY0, image, CalibrationCacheItem::CV_RAW_MATRIX) == IOMessages::IO_OK;
		break;

	case CHANNEL_GREY1:
		if_retrieved = cache->Get(frame, Constants::GREY1, image, CalibrationCacheItem::CV_RAW_MATRIX) == IOMessages::IO_OK;
		break;

	case CHANNEL_POINT_CLOUD:
		if_retrieved = cache->Get(frame, Constants::POINT_CLOUD, image) == IOMessages::IO_OK;
		break;

	default:
		if_retrieved = false;
		break;
	}

	return if_retrieved;
}

bool DMVSSensor::IsRunning() const
{
	if(!IsLive())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is not live ";
		return true;
	}

	return dmvs_device_ && dmvs_device_->IsConnected() == error::OK;
}

bool DMVSSensor::IsConnected() const
{
	return dmvs_device_->IsConnected() == error::OK;
}

bool DMVSSensor::IfFullAccess()
{
	return false;
}

size_t DMVSSensor::ImageWidth(const size_t channel)
{
	switch(channel)
	{
	case CHANNEL_GREY0:
		return Constants::DMVS_IMAGE_WIDTH;

	case CHANNEL_GREY1:
		return Constants::DMVS_IMAGE_WIDTH;

	case CHANNEL_POINT_CLOUD:
		return Constants::DMVS_IMAGE_WIDTH;

	case CHANNEL_SPARSE_CLOUD:
		return Constants::DMVS_IMAGE_WIDTH;

	default:
		return Constants::DEFAULT_SIZE_T_MAX_VALUE;
	}
}

size_t DMVSSensor::ImageHeight(const size_t channel)
{
	switch(channel)
	{
	case CHANNEL_GREY0:
		return Constants::DMVS_IMAGE_HEIGHT;

	case CHANNEL_GREY1:
		return Constants::DMVS_IMAGE_HEIGHT;

	case CHANNEL_POINT_CLOUD:
		return Constants::DMVS_IMAGE_HEIGHT;

	case CHANNEL_SPARSE_CLOUD:
		return Constants::DMVS_IMAGE_HEIGHT;

	default:
		return Constants::DEFAULT_SIZE_T_MAX_VALUE;
	}
}

bool DMVSSensor::GrabFromFile()
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;

	//Sanity checks
	//if(!IsOpened())
	//{
	//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is not opened ";
	//	return false;
	//}

	//if(IsLive())
	//	return false;

	//if(!cache_)
	//	return false;

	////Find next existing frame based on "info" entry
	//while(++frame_ <= frames_)
	//{
	//	if(cache_->Get(frame_, "info", infoContainer) == IOMessages::IO_OK)
	//		return true;
	//}

	//frame_ > m_frames: //no next frame -   grab failed
	return false;
}

bool DMVSSensor::IsLive() const
{
	return open_mode > DeviceCurrentMode::CONNECTED;
}

bool DMVSSensor::IsOpened() const
{
	return open_mode == DeviceCurrentMode::FROM_FILE || open_mode > DeviceCurrentMode::CONNECTED;
}

DMVSDevice* DMVSSensor::GetConnectedDMVS() const
{
	return dmvs_device_;
}

bool DMVSSensor::StartCapture(CapabilityType type)
{
	if(!IsOpened())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is not opened ";
		return false;
	}

	if(open_mode == DeviceCurrentMode::GRABBING)
	{
		// Read some frames before start to initialize
		// the automatic exposure settings
		frame_grabber_count_ = 0UL;

		for(unsigned long i = 0; i < skipped_frame_count_; ++i)
		{
			GrabFromSensor();
		}

		/*auto grabber = new CaptureGrabber(this);
		 * m_grabber = std::make_shared<CaptureGrabber>(this);*/
		grabber->InitThread();
	}

	return true;
}

bool DMVSSensor::StopCapture()
{
	if(!IsOpened())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS Device is already in closed state";
		return true;
	}

	if(open_mode == DeviceCurrentMode::GRABBING)
	{
		grabber->TerminateThread();
		grabber              = nullptr;
		frame_grabber_count_ = 0UL;
	}

	return true;
}

bool DMVSSensor::SetCameraCalibrationSettings(const bool is_auto_exposure) const
{
	if(!IsLive())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS Device is not live ";
		return false;
	}

	if(!dmvs_device_)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Either no DMVS Device or DMVS Device not connected ";
		return false;
	}

	if(dmvs_device_->SetAutoExposure(false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Auto Exposure ";
		return false;
	}

	if(dmvs_device_->SetSequencerMode(false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Sequencer Mode ";
		return false;
	}

	if(dmvs_device_->SetInputTrigger(data::TriggerLine1, false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Input Trigger Line 1";
		return false;
	}

	if(dmvs_device_->SetInputTrigger(data::TriggerLine2, false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Input Trigger Line 2 ";
		return false;
	}

	if(dmvs_device_->SetPositionInterfaceActive(false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Position Interface Active ";
		return false;
	}

	if(dmvs_device_->SetMode(data::Mode3D) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set the mode to 3D Mode ";
		return false;
	}

	if(dmvs_device_->SetLaser(false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set the laser ";
		return false;
	}

	if(dmvs_device_->SetLEDFlashConstant(true) != error::OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not set LED Flash Constant ";
		return false;
	}

	if(dmvs_device_->SetExposureTime(2000) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Exposure Time ";
		return false;
	}

	if(dmvs_device_->SetFramerate(Constants::DMVS_DEFAULT_FRAME_RATE) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Frame Rate";
		return false;
	}

	if(dmvs_device_->SetMode(data::Mode2D) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set mode to 2D Mode ";
		return false;
	}

	if(dmvs_device_->SetLEDPower0(Constants::DMVS_DEFAULT_LED_POWER) != error::OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not set Led 0 Power ";
		return false;
	}

	if(dmvs_device_->SetLEDPower1(Constants::DMVS_DEFAULT_LED_POWER) != error::OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not set Led 1 Power ";
		return false;
	}

	if(dmvs_device_->SetAutoExposure(is_auto_exposure) != error::OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not set Auto Exposure ";
		return false;
	}

	return true;
}

bool DMVSSensor::SetScanSettings() const
{
	if(!IsLive())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is not live ";
		return true;
	}

	if(!dmvs_device_)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Either No DMVS Device or DMVS Device not connected ";
		return false;
	}
	if(dmvs_device_->StopImageAcquisition() != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not Stop Acquisition ";
		return false;
	}

	if(dmvs_device_->SetAutoExposure(false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Auto Exposure ";
		return false;
	}

	if(dmvs_device_->SetSequencerMode(false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Sequencer Mode ";
		return false;
	}

	if(dmvs_device_->SetInputTrigger(data::TriggerLine1, false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Input Trigger Line 1";
		return false;
	}

	if(dmvs_device_->SetInputTrigger(data::TriggerLine2, false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Input Trigger Line 2 ";
		return false;
	}

	if(dmvs_device_->SetPositionInterfaceActive(false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Position Interface Active ";
		return false;
	}

	if(dmvs_device_->SetMode(data::Mode3D) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set mode to 3D Mode ";
		return false;
	}

	if(dmvs_device_->SetLaser(true) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Laser ";
		return false;
	}

	if(dmvs_device_->SetLEDFlashConstant(false) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Led Flash Constant ";
		return false;
	}

	if(dmvs_device_->SetExposureTime(Constants::DMVS_DEFAULT_EXPOSURE_TIME) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Exposure Time ";
		return false;
	}

	if(dmvs_device_->SetFramerate(Constants::DMVS_DEFAULT_FRAME_RATE) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set Frame Rate";
		return false;
	}

	if(dmvs_device_->SetMode(data::Mode2D) != error::OK)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not set mode to 2D Mode ";
		return false;
	}

	return true;
}
}
}
