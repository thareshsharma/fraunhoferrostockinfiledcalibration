#include "dmvs_exception.h"

namespace DMVS
{
DMVSException::~DMVSException() noexcept
= default;

DMVSException::DMVSException(const char* msg,
                             const char* file,
                             const size_t line,
                             const char* func,
                             const char* info)
	: std::exception(msg),
	  file_(file),
	  line_number_(line),
	  function_(func),
	  info_(info)
{
}

char const* DMVSException::what() const
{
	std::ostringstream out_stream;
	out_stream << file_ << ":" << line_number_ << ": " << info_;
	return out_stream.str().c_str();
}

const char* DMVSException::GetFile() const
{
	return file_;
}

size_t DMVSException::GetLineNumber() const
{
	return line_number_;
}

const char* DMVSException::GetFunction() const
{
	return function_;
}

const char* DMVSException::GetInfo() const
{
	return info_;
}
}
