#include "dmvs_camera_calibration.h"
#include "constants.h"
#include "calibration_cache.h"
#include "configuration_container.h"
#include "capture_calibration.h"
#include "enum_image_channel_type.h"
#include "single_camera_calibration.h"
#include "camera_sensor_depth_dependent.h"

namespace fs = boost::filesystem;

namespace DMVS
{
namespace CameraCalibration
{
DMVSCameraCalibration::DMVSCameraCalibration() :
	configuration_options(nullptr),
	timestamp_(0.0),
	device_type_("DMVS")

{
}

DMVSCameraCalibration::~DMVSCameraCalibration() = default;

DeviceDescription DMVSCameraCalibration::DeviceState()
{
	/*if(!IsSupportedSensor() || !ConnectToDMVS())
		return {DeviceType(), NOT_CONNECTED};*/

	const auto state = calibration_filename_.empty() && GetDeviceCalibration(capture_calibration) ? CALIBRATED : CONNECTED;

	DeviceDescription device_description(DeviceType(), state, serial_number);

	device_description.minimum_z_value = Constants::DEVICE_DESCRIPTION_MINIMUM_Z_VALUE;
	device_description.maximum_z_value = Constants::DEVICE_DESCRIPTION_MAXIMUM_Z_VALUE;
	device_description.default_z_value = Constants::DEVICE_DESCRIPTION_DEFAULT_Z_VALUE;
	device_description.components.push_back(FLASH);
	device_description.components.push_back(LEDS);

	// Calibration modes
	device_description.capabilities.push_back(FS_CAMERA_FACTORY_CAL);
	device_description.capabilities.push_back(FS_PROJECTOR_FACTORY_CAL);
	device_description.capabilities.push_back(FS_CAMERA_INFIELD_CAL);
	device_description.capabilities.push_back(FS_TESTLOOP_CAL);

	return device_description;
}

DeviceType DMVSCameraCalibration::DeviceType() const
{
	return DMVS_CAPTURE;
}

bool DMVSCameraCalibration::Init(const std::string& input_filename,
                                 const std::string& output_filename,
                                 const std::shared_ptr<ConfigurationContainer> options)
{
	configuration_options = options;
	timestamp_            = 0.0;

	if(!output_filename.empty())
	{
		output_file_ = fs::path(output_filename);
	}

	return true;
}

std::string DMVSCameraCalibration::GetSerialNumber() const
{
	return serial_number;
}

std::string DMVSCameraCalibration::GetCaptureDeviceIdent() const
{
	// DMVS may not have serial number sometimes. Just use the device id
	return GetDeviceType() + (GetSerialNumber().empty() ? "" : "_" + GetSerialNumber());
}

bool DMVSCameraCalibration::InitCapture()
{
	// Sanity checks
	if(!configuration_options)
		return false;

	// Create new cache
	cache = std::make_shared<CalibrationCache>();
	cache->MaximumItems(400);

	if(!output_file_.empty())
		cache->Attach(output_file_.string(),
		              CalibrationCache::CACHE_WRITE | CalibrationCache::CACHE_ASYNC,
		              CalibrationCache::MagicCode(DeviceType()));

	// Store sensor infos in the recording file
	// Note: Don't use getCaptureDeviceIdent to get the string. This doesn't work.
	if(cache->Put(0, Constants::DMVS_SENSOR, GetCaptureDeviceIdent()) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device Identification could not be saved in cache";
	}

	if(cache->Put(0, Constants::DMVS_DEVICE_TYPE, GetDeviceType()) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device Type could not be saved in cache";
	}

	if(cache->Put(0, Constants::DMVS_SERIAL_NO, GetSerialNumber()) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device Serial Number could not be saved in cache";
	}

	return true;
}

std::string DMVSCameraCalibration::GetDeviceType() const
{
	return device_type_;
}

bool DMVSCameraCalibration::Open()
{
	// Sanity checks
	if(DeviceState().state < CONNECTED)
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS Device state is not connected";
		return false;
	}

	if(IsOpened())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is already opened";
		return true;
	}

	// Setup recording
	if(!Init(input_file_.string(), "", configuration_options))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not initialize DMVS Device";
		return false;
	}

	// Configure tracking parameters
	//if(!ConfigureTracking())
	//{
	//	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not bring DMVS Device to Tracking State!";
	//	return false;
	//}

	// Get calibration
	if(!GetDeviceCalibration(capture_calibration))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Calibration could not be downloaded DMVS Device!";
		return false;
	}

	//if(!Configure())
	//{
	//	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Error in configuring DMVS Device. Probably, DMVS Parameter Error";
	//}

	if(!InitCapture())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not initialise Capture!";
		return false;
	}

	//if(dmvs_device_->Register2DImageCallBack(ImageReceived2D, this) != error::OK)
	//{
	//	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not register 2D Image Callback";
	//	return false;
	//}

	//if(dmvs_device_->Register3DImageCallBack(ImageReceived3D, this) != error::OK)
	//{
	//	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not register 3D Image Callback";
	//	return false;
	//}

	//open_mode = DeviceCurrentMode::GRABBING;

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Opening DMVS Device [" << dmvs_device_->GetSerialNumber() << "] " << Constants::LOG_END;

	return IsOpened();
}

void DMVSCameraCalibration::GetCalibration(CaptureCalibration& calibration) const
{
	calibration = capture_calibration;
}

void DMVSCameraCalibration::SetCalibration(CaptureCalibration& calibration)
{
	capture_calibration = calibration;
}

bool DMVSCameraCalibration::GetDefaultCalibration(CaptureCalibration& calibration)
{
	// NOT IMPLEMENTED IN DMVSSDK
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : GET DMVS DEFAULT CALIBRATION NOT IMPLEMENTED IN DMVS SDK ";
	return false;
}

bool DMVSCameraCalibration::GetDeviceCalibration(CaptureCalibration& calibration)
{
	return false;// DownloadCalibration() && calibration.Load(calibration_filename_);
}

bool DMVSCameraCalibration::ManageCalibration(const ManageCalibrationMode calibration_mode)
{
	// Store the current calibration on device
	if(calibration_mode == STORE_CALIBRATION)
	{
		return false;// UploadCalibration();
	}

	// Restore a calibration form device
	// Stores the calibration in the SCENECT/cameras folder
	if(calibration_mode == RESTORE_CALIBRATION)
	{
		return false;// DownloadCalibration();
	}

	return false;
}

bool DMVSCameraCalibration::PushChannelImageToCache(bool synced)
{
	auto if_grabbed = false;

	//TODO
	//TODO
	//TODO
	//TODO
	/*if(IsLive() && dmvs_device_)
		if_grabbed = Start();

	Helper::Sleeping(0.1, "while waiting for startImageAcquisition() to finish");
	if_grabbed &= GrabFrame();*/

	return if_grabbed;
}

std::string DMVSCameraCalibration::GetExtension()
{
	// TODO
	return "DMVS";
}

std::shared_ptr<const CameraSensor> DMVSCameraCalibration::GetDMVSCameraCameraCalibration(std::string name,
                                                                                          ImageChannel channel,
                                                                                          const CameraModel camera_model,
                                                                                          const cv::Vec2d depth_params,
                                                                                          const cv::Vec3d depth_params_exp,
                                                                                          const cv::Mat& camera_matrix,
                                                                                          const cv::Mat& distortion_vector,
                                                                                          const cv::Mat& rotation_matrix,
                                                                                          const cv::Mat& translation_vector)
{
	auto depth_dependent_calibration = std::make_shared<CameraSensorDepthDependent>(name,
	                                                                                channel,
	                                                                                depth_params,
	                                                                                depth_params_exp,
	                                                                                camera_matrix,
	                                                                                distortion_vector,
	                                                                                rotation_matrix,
	                                                                                translation_vector);

	//depth_dependent_calibration->KeepDistortionFix(false);
	//depth_dependent_calibration->KeepExtrinsicParametersFix(true);

	std::shared_ptr<const CameraSensor> default_calibration;

	switch(camera_model)
	{
	case 0:
		default_calibration = depth_dependent_calibration;
		break;
	case 1:
		default_calibration = std::make_shared<SingleCameraCalibration>(camera_matrix, distortion_vector, rotation_matrix, translation_vector);
		break;
	case 2:
		//defaultCal = scvCal;
		//break;
		//	case 3: defaultCal = ddCal01; break;
	default:
		default_calibration = std::make_shared<CameraSensorDepthDependent>(name,
		                                                                   channel,
		                                                                   depth_params,
		                                                                   depth_params_exp,
		                                                                   camera_matrix,
		                                                                   distortion_vector,
		                                                                   rotation_matrix,
		                                                                   translation_vector);
	}

	return depth_dependent_calibration;
}


bool DMVSCameraCalibration::RetrieveImageFromCalibrationCache(const size_t frame,
                                                              const size_t channel,
                                                              cv::Mat& image) const
{
	if(!IsOpened())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Device is not opened";
		return false;
	}

	auto if_retrieved = false;

	switch(channel)
	{
	case CHANNEL_GREY0:
		if_retrieved = cache->Get(frame, Constants::GREY0, image, CalibrationCacheItem::CV_RAW_MATRIX) == IOMessages::IO_OK;
		break;

	case CHANNEL_GREY1:
		if_retrieved = cache->Get(frame, Constants::GREY1, image, CalibrationCacheItem::CV_RAW_MATRIX) == IOMessages::IO_OK;
		break;

	case CHANNEL_POINT_CLOUD:
		if_retrieved = cache->Get(frame, Constants::POINT_CLOUD, image) == IOMessages::IO_OK;
		break;

	default:
		if_retrieved = false;
		break;
	}

	return if_retrieved;
}

bool DMVSCameraCalibration::IsOpened() const
{
	return false; // open_mode == DeviceCurrentMode::FROM_FILE || open_mode > DeviceCurrentMode::CONNECTED;
}
}
}
