#include "thread_base.h"

// ThreadBase is a base class for threads.
// - Derive from this class to implement a custom thread.
// - Implement the abstract runThreadProc function to define the thread behavior.
// - Optionally override initThreadProc and exitThreadProc to define
//   custom initialization and finalization within the thread.
// - CAUTION:
//       -> Call initThread() immediately after construction!
//       -> Call terminateThread() immediately before destruction!

ThreadBase::ThreadBase()
{
	thread_terminate_    = false;
	thread_running_      = false;
	thread_sleep_        = false;
	thread_activated_    = false;
	set_thread_to_sleep_ = false;
	thread_id_           = 0;

	// init the handles
	activate_thread_          = nullptr;
	initialize_thread_        = nullptr;
	thread_handle_            = nullptr;
	terminated_thread_signal_ = nullptr;
	started_thread_signal_    = nullptr;
	thread_name_              = "ThreadBase";
}

ThreadBase::~ThreadBase()
{
	if(!thread_running_ && thread_handle_ != 0)
	{
		Join();
		return;
	}

	// Check that the thread is already terminated!
	assert(!thread_running_);
	if(thread_running_ || thread_handle_ != 0)
	{
		// Write to log if log is enabled
		auto err = L"ThreadBase::~ThreadBase Error: Destructor was called before thread was terminated";
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << err;

		// Write to cout for team city
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << err;

		//_XSTD terminate();
	}
}

//
// Close a system handle
//	For windows: Check if the handle is valid and
//	may be closed.
//
template <typename HANDLE_TYPE>
static void closeSystemHandle(HANDLE_TYPE& hdl,
                              const std::string& name)
{
	HANDLE_TYPE cleanupHandle = hdl;
	hdl                       = 0;

	if(!cleanupHandle)
		return;

#ifdef WINDOWS
	DWORD flgs;
	if(!GetHandleInformation(cleanupHandle, &flgs) ||		// Must return TRUE
		(flgs & HANDLE_FLAG_PROTECT_FROM_CLOSE) ||			// Flag should be not set
		!CloseHandle(cleanupHandle))						// Must return TRUE
	{
		std::string err;
		err.printf(L"ThreadBase::reset %ls: error %d", name.stringNeverNull(), GetLastError());
		LOG_INFO(err);
	}
#else
	CloseHandle(cleanupHandle);
#endif

	cleanupHandle = 0;
}

void ThreadBase::Reset(bool cleanup)
{
#ifdef ThreadBase_LOG
	LOG_INFO(L"ThreadBase::reset: begin");
#endif

	if(cleanup)
	{
		// Cleanup handles
		closeSystemHandle(activate_thread_, thread_name_);
		closeSystemHandle(initialize_thread_, thread_name_);
		closeSystemHandle(terminated_thread_signal_, thread_name_);
		closeSystemHandle(started_thread_signal_, thread_name_);
		closeSystemHandle(thread_handle_, thread_name_);

		thread_id_ = 0;
	}

	// Thread parameter
	thread_terminate_    = false;
	thread_running_      = false;
	thread_sleep_        = false;
	thread_activated_    = false;
	set_thread_to_sleep_ = false;

#ifdef ThreadBase_LOG
	LOG_INFO(L"ThreadBase::reset: end");
#endif
}

typedef unsigned int (__stdcall* threadStaticProc)(void*);


/// Returns handle of this thread.
HANDLE ThreadBase::GetThreadHandle() const
{
	return thread_handle_;
}

inline HANDLE _beginthreadex_named(
	void* security,
	int stacksize,
	threadStaticProc start_routine,
	void* arglist,
	unsigned int initflag,
	unsigned int* thread_attr,
	const char* threadName)
{
	return (HANDLE)_beginthreadex(
		security,
		stacksize,
		start_routine,
		arglist,
		initflag,
		thread_attr);
}


void ThreadBase::InitThread()
{
	if(thread_handle_ != 0)
	{
		throw(std::runtime_error("Thread already was initialized."));
	}

	// Start the thread
	{
		activate_thread_ = CreateEvent(nullptr,
		                               /*manual reset*/
		                               FALSE,
		                               /*initial state*/
		                               FALSE,
		                               /*name*/
		                               nullptr);
		initialize_thread_ = CreateEvent(nullptr,
		                                 /*manual reset*/
		                                 FALSE,
		                                 /*initial state*/
		                                 FALSE,
		                                 /*name*/
		                                 nullptr);
		terminated_thread_signal_ = CreateEvent(nullptr,
		                                        /*manual reset*/
		                                        FALSE,
		                                        /*initial state*/
		                                        FALSE,
		                                        /*name*/
		                                        nullptr);
		started_thread_signal_ = CreateEvent(nullptr,
		                                     /*manual reset*/
		                                     FALSE,
		                                     /*initial state*/
		                                     FALSE,
		                                     /*name*/
		                                     nullptr);


		// Actually start thread
		{
			// use CRT for thread creation for proper initialization of CRT
			thread_id_ = 0L;

			thread_handle_ = //reinterpret_cast<THREAD_HANDLE>
				(HANDLE)(_beginthreadex_named(nullptr,
				                              0,
				                              StaticThreadProc,
				                              this,
				                              0,
				                              reinterpret_cast<unsigned int*>(&thread_id_),
				                              thread_name_.c_str()));
			if(!thread_handle_)
			{
				thread_id_ = 0; // creation failed
			}
#ifdef _LINUX
			// Define priority given by the calling thread
			setPriority(GetThreadPriority(GetCurrentThread()));
#else
			// Lowest thread priority
			SetPriority(THREAD_PRIORITY_LOWEST);
#endif // _LINUX

			// Initialize and start the thread
			SetEvent(initialize_thread_);
		}
	}

	// Wait for the thread to be started.
	if(thread_handle_)
	{
		// wait until the thread has finished starting
		// bool threadStarting is needed because of the possibility of spurious wakeups
		WaitForSingleObject(started_thread_signal_, INFINITE);
	}

#ifdef _DEBUG
	// for debugging only. By default thread should not be blocked.
	thread_blocked = false;
#endif
}

void ThreadBase::Join()
{
	DWORD exitcode = 0;
	if(GetExitCodeThread(thread_handle_, &exitcode))
	{
		// Thread already returned?
		// Must be checked before waiting on terminatedThreadSignal in case
		// of threads forcibly terminated by Windows upon unloading a dll.
		// These threads never signalled terminatedThreadSignal.
		if(exitcode == (DWORD)0)
		{
			Reset();
			return;
		}
	}

	if(thread_id_ == GetCurrentThreadId())
	{
		if(thread_running_)
		{
			// Thread tries to delete itself but is still running. That should not happen!
			throw(std::runtime_error("A thread can not wait for terminating itself!"));
		}
		else
		{
			// Thread is trying to delete itself but has finished and this is the last thing it does.
			// It is safe to free its handles.
			Reset();
			return;
		}
	}

	//	if(!AllocMain::isThreadLocked(Models::theThreadContext(threadID)) && !GlobalAllocScene::isThreadLocked())
	//	{
	//		// we don't have the lock
	//		// it's safe to wait
	//		if(GetCurrentThreadId() == getMainThreadId())
	//		{
	//			// Process messages while waiting in main thread
	//			do {
	//				iQProcessMessages();
	//
	//				// check if the thread is really still running
	//				if(GetExitCodeThread(threadHandle, &exitcode))
	//				{
	//					if(exitcode != STILL_ACTIVE && exitcode != 0)
	//					{
	//						// These threads never signalled terminatedThreadSignal.
	//						// the application is most probably in an undefined state.
	//						// Write the dump directly instead of throwing, since a throw in a
	//						// destructor will bypass the exit handler and call abort.
	//#ifdef WIN32
	//						writeMiniDumpSpecifiedPath(nullptr, SmartPtr<const Directory>(new Directory()));
	//#endif
	//						std::exit(exitcode);
	//					}
	//				}
	//			} while(WAIT_TIMEOUT == WaitForSingleObject(terminatedThreadSignal, 10)); // Wait for thread to terminate
	//		}
	//		else
	//		{
	//			// Wait until thread was terminated
	//			WaitForSingleObject(terminatedThreadSignal, INFINITE);  // Wait for thread to terminate
	//		}
	//
	//		// Close the thread handles and reset all members
	//		reset();
	//#ifdef ThreadBase_LOG
	//		{
	//			std::string logMsg(L"ThreadBase::terminateThread: Successfully terminated thread");
	//			LOG_INFO(logMsg);
	//		}
	//#endif
	//	}
	//	else
	//	{
	//		// you are trying to wait while you're still owning a lock!!!
	//		assert(false);
	//#ifdef DEBUG_LOG_EXT
	//		{
	//			std::string logMsg(L"ThreadBase::terminateThread: Unexpectedely terminating while owning the lock: ");
	//			if(AllocMain::isThreadLocked(Models::theThreadContext(threadID)))
	//				logMsg << L"AllocMain";
	//			else if(GlobalAllocScene::isThreadLocked())
	//				logMsg << L"AllocScene";
	//			else
	//				logMsg << L"Unknown";
	//			LOG_INFO(logMsg);
	//			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << std::string(logMsg.stringAnsi()); // Make error visible in TeamCity
	//			throw(new std::runtime_error("Waiting for thread completion under a lock!"));
	//		}
	//#endif
	//		return;
	//	}
}


void ThreadBase::TerminateThread()
{
	// Only wait if there exists a thread
#ifdef _MSC_VER
	if(thread_handle_ != nullptr)
#else
	if(threadHandle > 0)
#endif
	{
		// Make sure the thread can terminate as soon as possible
		SetPriority(THREAD_PRIORITY_NORMAL);

		// Tell the thread to stop running its internal loop
		thread_terminate_ = true;
		// Wakeup the thread in case it is sleeping
		if(thread_sleep_)
			SetEvent(activate_thread_);

		// Wait until the thread is terminated
		Join();
	}
}


// setToSleep: Set running thread to sleep, wait for sleep state
// depending on wait.
bool ThreadBase::SetToSleep(bool wait)
{
	{
		LockIntern lock(*this);
		if(!thread_running_)
			return false;
		if(thread_sleep_)
			return true;

		set_thread_to_sleep_ = true;
	}

#ifdef ThreadBase_LOG
	LOG_INFO(L"ThreadBase::setToSleep: begin");
#endif

	if(wait && thread_id_ != GetCurrentThreadId())
	{
		//VERIFY(::ResetEvent(activateThread));
		int curPrio = GetPriority();
		SetPriority(THREAD_PRIORITY_NORMAL);
		do
		{
			// wait for execution
			set_thread_to_sleep_ = true; // thread could be woken up by timer already
			Sleep(10);
		}
		while(!thread_sleep_);
		set_thread_to_sleep_ = false;
		SetPriority(curPrio);
	}

#ifdef ThreadBase_LOG
	LOG_INFO(L"ThreadBase::setToSleep: end");
#endif

	return true;
}


/// waitForFinish: Waits until the first time process finished the job.
void ThreadBase::WaitForFinish()
{
	{
		LockIntern lock(*this);
		if(!thread_running_)
			return;
		if(thread_sleep_)
			return;
	}

#ifdef ThreadBase_LOG
	LOG_INFO(L"ThreadBase::waitForFinish: begin");
#endif

	/// Waits for the thread to finish the job
	{
		assert(!thread_id_ || thread_id_ != GetCurrentThreadId());
		int curPrio = GetPriority();
		// Give the thread a higher priority in order to make it finish as fast as possible
		SetPriority(THREAD_PRIORITY_NORMAL);

		auto isJoinable = [&]()
		{
			LockIntern lock(*this);
			return thread_sleep_ || !thread_running_;
		};

		while(!isJoinable()) // wait for execution
			Sleep(10);

		// Reset the thread's priority
		SetPriority(curPrio);
	}

#ifdef ThreadBase_LOG
	LOG_INFO(L"ThreadBase::waitForFinish: end");
#endif

	return;
}


// Implement this function to define the thread behaviour. return true to
// continue with another loop, false to stop execution.
// Default implementation: Returns false and set thread to sleep.
bool ThreadBase::RunThreadProc()
{
	LockIntern lock(*this);
	thread_sleep_ = true;
	return false;
}


// activate: Activates the thread: Returns false, if  thread
// doesn't need to be activated
bool ThreadBase::Activate(bool wait)
{
	// The activate method needs to be synchronized to avoid a possible deadlock
	// (due to setting the threadActivated flag) when it is called concurrently
	// from multiple threads
	CriticalSection::Access lockInternal(lock_internal_access_, CriticalSection::write);

	{
		LockIntern lock(*this);
		if(!thread_sleep_ || !thread_running_)
			return false;
	}

	thread_activated_ = false;
	SetEvent(activate_thread_);

#ifdef ThreadBase_LOG
	LOG_INFO(L"ThreadBase::activate: begin");
#endif

	if(wait)
	{
		// Wait for activation
		while(!thread_activated_)
			Sleep(10);

		thread_activated_ = false;
	}

#ifdef ThreadBase_LOG
	LOG_INFO(L"ThreadBase::activate: end");
#endif

	return true;
}


/// To be called before going to sleep, to be overwritten by derived class,
/// if different behavior is wanted.
bool ThreadBase::IsReadyForSleep() const
{
	// In base implementation we always are ready for sleep.
	return true;
}


int ThreadBase::GetPriority() const
{
	if(!thread_handle_)
		return -1;

	return GetThreadPriority(thread_handle_);
}


bool ThreadBase::SetPriority(int in)
{
	if(!thread_handle_)
		return false;

	return SetThreadPriority(thread_handle_, in) ? true : false;
}


DWORD ThreadBase::GetThreadId()
{
	return thread_id_;
}

/// staticThreadProc is called as a starting point for a new thread.
unsigned int WINAPI ThreadBase::StaticThreadProc(void* user_data)
{
	//#ifdef _WINDOWS
	//	// Dirty hack to set the correct module state if the library is loaded
	//	// JNI (Java Native Interface)
	//	if(getTheModuleState() != nullptr)
	//	{
	//		AFX_MANAGE_STATE(getTheModuleState());
	//		ThreadBase* thread = (ThreadBase*)user_data;
	//		return thread->threadProcBase();
	//	}
	//#endif
	ThreadBase* thread = (ThreadBase*)user_data;
	return thread->ThreadProcBase();
}

/// threadProc is called by staticThreadProc and runs
/// the ThreadBase.
unsigned int ThreadBase::ThreadProcBase()
{
	thread_running_ = true;
	SetEvent(started_thread_signal_);

	struct SignalThreadStopAtExit
	{
		decltype(terminated_thread_signal_)& m_terminateSignal;

		// Special case. Used for FunctorThreads only!
		// The terminatedThreadSignal member must not be called
		// after calling exitThreadProc() as the ThreadBase destructor
		// can be called within exitThreadProc().
		// Copy the member to a local variable because destructor can be called from within exitThreadProc()
		decltype(calls_destructor_in_thread_) m_callsDesctructor;

		~SignalThreadStopAtExit()
		{
			if(!m_callsDesctructor)
			{
				// Signal the terminateThread() method
				SetEvent(m_terminateSignal);
			}
		}
	} signalStopAtExit{terminated_thread_signal_, calls_destructor_in_thread_};

	// Start do the work
	// TODO [MT]: Implement the functionality to add custom additional events which cause
	// the execution of certain functionality based on given function pointers
	HANDLE buf[] = {
		initialize_thread_,
		activate_thread_
	};
	//	while(!threadTerminate)
	//	{
	//		// check whether there are any events waiting to be processed
	//		DWORD retVal = SafeWait::WaitForMultipleObjects(sizeof(buf) / sizeof(buf[0]), buf, FALSE, 0);
	//		if(retVal == WAIT_TIMEOUT)
	//		{
	//			// No events waiting -> Fall asleep immediately, wait until a new event arrives
	//			{
	//				// Lock change of threadSleep.
	//				LockIntern lock(*this);
	//				threadSleep = true;
	//			}
	//			setThreadToSleep = false;
	//			if(isReadyForSleep())
	//			{
	//				retVal = SafeWait::WaitForMultipleObjects(sizeof(buf) / sizeof(buf[0]), buf, FALSE, INFINITE);
	//			}
	//			else
	//			{
	//				// Usually we want thread activated.
	//				retVal = 1 + WAIT_OBJECT_0;
	//			}
	//		}
	//		threadSleep = false;
	//		threadActivated = true;
	//		if(threadTerminate)   // Thread is supposed to terminate
	//			break;
	//
	//		switch(retVal - WAIT_OBJECT_0)	// Type of event
	//		{
	//			// InitializeThread: Initialize thread
	//		case 0:
	//		{
	//			initThreadProc();
	//			SetEvent(activateThread);
	//		} break;
	//		// ActivateThread: Run message loop
	//		case 1:
	//		{
	//			bool runAgain = false;
	//			do
	//			{
	//				// call the defined thread procedure
	//				try
	//				{
	//					runAgain = runThreadProc();
	//				}
	//				catch(const LockingException_AllocSceneUnderAllocMain&)
	//				{
	//#ifdef _DEBUG
	//#ifdef _WINDOWS
	//					DebugBreak();
	//#endif
	//#endif
	//
	//#ifdef _WINDOWS
	//					if(!DlgBaseBlock::isBlocked())
	//					{
	//						// using AfxMessageBox instead of SyncMessageBox because
	//						// SyncMessageBox makes use of the functor mechanism
	//						// which might already be corrupt at this point
	//						const std::string msg(std::string::loadFromResource(IDS_THREAD_SYNC_EXCEPTION));
	//						AfxMessageBox(msg.stringNeverNull(), MB_OK, MB_ICONEXCLAMATION);
	//						LOG_ERROR(msg.stringNeverNull());
	//					}
	//					else
	//					{
	//						// --> Create an exception
	//						ExceptionManager::addException<Exception>(IOMessages::IO_LOCK_ALLOCSCENE_UNDER_ALLOCMAIN);
	//					}
	//#endif
	//				}
	//				catch(const LockingException_GlobalAllocSceneUnderAllocScene&)
	//				{
	//#ifdef _DEBUG
	//#ifdef _WINDOWS
	//					DebugBreak();
	//#endif
	//#endif
	//
	//#ifdef _WINDOWS
	//					if(!DlgBaseBlock::isBlocked())
	//					{
	//						// using AfxMessageBox instead of SyncMessageBox because
	//						// SyncMessageBox makes use of the functor mechanism
	//						// which might already be corrupt at this point
	//						const std::string msg(std::string::loadFromResource(IDS_THREAD_SYNC_EXCEPTION));
	//						AfxMessageBox(msg.stringNeverNull(), MB_OK, MB_ICONEXCLAMATION);
	//						LOG_ERROR(msg.stringNeverNull());
	//					}
	//					else
	//					{
	//						// --> Create an exception
	//						ExceptionManager::addException<Exception>(IOMessages::IO_LOCK_GOLBAL_ALLOCSCENE_UNDER_ALLOCSCENE);
	//					}
	//#endif
	//				}
	//
	//				catch(const LockingException_AllocMainAllUnderAllocMain&)
	//				{
	//#ifdef _DEBUG
	//#ifdef _WINDOWS
	//					DebugBreak();
	//#endif
	//#endif
	//
	//#ifdef _WINDOWS
	//					if(!DlgBaseBlock::isBlocked())
	//					{
	//						// using AfxMessageBox instead of SyncMessageBox because
	//						// SyncMessageBox makes use of the functor mechanism
	//						// which might already be corrupt at this point
	//						const std::string msg(std::string::loadFromResource(IDS_THREAD_SYNC_EXCEPTION));
	//						AfxMessageBox(msg.stringNeverNull(), MB_OK, MB_ICONEXCLAMATION);
	//						LOG_ERROR(msg.stringNeverNull());
	//					}
	//					else
	//					{
	//						// --> Create an exception
	//						ExceptionManager::addException<Exception>(IOMessages::IO_LOCK_ALLOCMAINALL_UNDER_ALLOCMAIN);
	//					}
	//#endif
	//				} /* catch */
	//				catch(const LockingException_WaitUnderLock&)
	//				{
	//#ifdef _DEBUG
	//#ifdef _WINDOWS
	//					DebugBreak();
	//#endif
	//#endif
	//
	//					if(!DlgBaseBlock::isBlocked())
	//					{
	//#ifdef _WINDOWS
	//						// using AfxMessageBox instead of SyncMessageBox because
	//						// SyncMessageBox uses the functor mechanism which may
	//						// already be corrupt at this point
	//						AfxMessageBox(std::string::loadFromResource(IDS_LOCKING_EXCEPTION).stringNeverNull(), MB_OK, MB_ICONEXCLAMATION);
	//#endif
	//					}
	//					else
	//					{
	//						// --> Create an exception
	//						ExceptionManager::addException<Exception>(IOMessages::IO_LOCK_WAIT_UNDER_LOCK);
	//					}
	//					LOG_ERROR(std::string::loadFromResource(IDS_LOCKING_EXCEPTION).stringNeverNull());
	//				}/* catch(const LockingException_WaitUnderLock&) */
	//			} while(runAgain && !threadTerminate && !setThreadToSleep);
	//			// If runAgain is false (by ThreadBase::runThreadProc), threadSleep
	//			// should already be set to true, but for threadTerminate or setThreadToSleep
	//			// better set threadSleep flag as soon as possible.
	//			LockIntern lock(*this);
	//			threadSleep = true;
	//		} break;
	//		}	// switch event
	//	}

	// Clean up system
	thread_running_ = false;

	ExitThreadProc();

	// signalStopAtExit goes out of scope and signals that the thread has stopped
	return 0;
}
