#include "thread_local_storage.h"
#include "thread_data_type.h"

ThreadLocalStorage* ThreadLocalStorage::singleton_ = nullptr;
CriticalSection ThreadLocalStorage::LockInternal::critical_section_;

void ThreadLocalStorage::Init()
{
	LockInternal lock;

	if(singleton_)
		return;

	assert(!singleton_);
	singleton_ = new ThreadLocalStorage();
	atexit(Cleanup);
}

void ThreadLocalStorage::Cleanup() noexcept
{
	ThreadLocalStorage* instance = nullptr;
	{
		LockInternal lock;
		std::swap(singleton_, instance);
	}
	delete instance;
}

ThreadLocalStorage::ThreadLocalStorage()
#ifdef _WINDOWS
	: tls_index_(TlsAlloc())
#elif defined(_LINUX)
	: tls_index_(0)
#endif
{
#ifdef _WINDOWS
	InitializeCriticalSection(&critical_section_);
	assert(tls_index_ != TLS_OUT_OF_INDEXES);
	//TODO
	//atexit_thread(threadTerminating);
#elif defined(_LINUX)
	int res = pthread_key_create((pthread_key_t*)(&tls_index_), &threadTerminating);
	assert(res == 0);
	isIndexValid = (res == 0);
	InitializeCriticalSection(&cs_);
#endif
}

ThreadLocalStorage::~ThreadLocalStorage() noexcept
{
	// if register/unregister was done properly the should
	// be only data from current thread (the main thread) inside the map
	DeleteCriticalSection(&critical_section_);

#ifdef _WINDOWS
	if(tls_index_ != TLS_OUT_OF_INDEXES)
		TlsFree(tls_index_);
#elif defined(_LINUX)
	if(isIndexValid)
		pthread_key_delete(tls_index_);
#endif

	// map cleanup, should not be necessary,
	// however worker threads are not even required to terminate...
	for(MapType::const_iterator iterator = tls_map_.begin(); iterator != tls_map_.end(); ++iterator)
		delete iterator->second;
}

#ifdef _WINDOWS
void ThreadLocalStorage::ThreadTerminating() noexcept
#elif defined(_LINUX)
void ThreadLocalStorage::threadTerminating(void* key) noexcept
#endif
{
	if(!singleton_)
		return; // nothing to do as no instance exists.
	singleton_->UnregisterCurrentThread();
}

void ThreadLocalStorage::RegisterCurrentThread() noexcept
{
	// need to synchronize access to the map.
	ScopeSyncGuard sync(&critical_section_);

	// create entry in tls_map_
	const auto key = GetCurrentThreadId(); // the key is the current thread id
	auto& entry    = tls_map_[key];
	assert(!entry);
	entry = new ThreadDataType();

	// set Tls value
#ifdef _WINDOWS
	if(tls_index_ != TLS_OUT_OF_INDEXES)
	{
		const auto res = TlsSetValue(tls_index_, entry);
		assert(res);
	}
#elif defined(_LINUX)
	if(isIndexValid) {
		const int res = pthread_setspecific(tls_index_, entry);
		assert(res == 0);
	}
#endif
}

void ThreadLocalStorage::UnregisterCurrentThread() noexcept
{
	// need to synchronize access to the map.
	ScopeSyncGuard sync(&critical_section_);

	// remove lookup entry
	const auto key      = GetCurrentThreadId(); // the key is the current thread id
	const auto iterator = tls_map_.find(key);
	std::unique_ptr<ThreadDataType> data;
	if(iterator != tls_map_.end())
	{
		std::unique_ptr<ThreadDataType> swap_data(iterator->second);
		std::swap(data, swap_data);
		tls_map_.erase(iterator);
	}

	// clear Tls variable for safety
#ifdef _WINDOWS
	if(tls_index_ != TLS_OUT_OF_INDEXES)
		TlsSetValue(tls_index_, nullptr);
#elif defined(_LINUX)
	if(isIndexValid)
		pthread_setspecific(tls_index_, 0);
#endif
}

ThreadDataType* ThreadLocalStorage::GetDataFallback()
{
	// a thread called that did not register itself, thus it is registered on
	// first call. The thread is unregistered by taking advantage of DllMain.
	RegisterCurrentThread();

#ifdef _WINDOWS
	if(tls_index_ != TLS_OUT_OF_INDEXES)
	{
#elif defined(_LINUX)
	if(isIndexValid) {
#endif
#ifdef _WINDOWS
		auto* data = static_cast<ThreadDataType*>(TlsGetValue(tls_index_));
#elif defined(_LINUX)
		ThreadDataType* data = static_cast<ThreadDataType*>(pthread_getspecific(tls_index_));
#endif
		assert(data);
		return data;
	}

	// there is no tls index available
	// need to synchronize access to the map.
	ScopeSyncGuard sync(&critical_section_);
	const auto key = GetCurrentThreadId(); // the key is the current thread id
	return tls_map_[key];
}
