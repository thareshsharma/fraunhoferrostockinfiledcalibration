#include "critical_section.h"

inline void CriticalSection::Access::aquire(CriticalSection& csIn,
                                            const AccessMode& modeIn)
{
	switch(modeIn)
	{
	case read: csIn.enter();
		break;
	case write: csIn.enter();
		break;
	default: assert(false);
	}
}

inline void CriticalSection::Access::release(CriticalSection& csIn,
                                             const AccessMode& modeIn)
{
	switch(modeIn)
	{
	case read: csIn.leave();
		break;
	case write: csIn.leave();
		break;
	default: assert(false);
	}
}

CriticalSection::CriticalSection(const CriticalSection&)
{
}

CriticalSection& CriticalSection::operator=(const CriticalSection&)
{
	return *this;
}

CriticalSection::CriticalSection(const DWORD SpinCount):
	lockCounter(0u)
{
	InitializeCriticalSectionAndSpinCount(&cs, SpinCount);
}

CriticalSection::Access::Access(CriticalSection& csIn,
                                const AccessMode& modeIn) : cs(csIn), mode(modeIn), hasLockFlg(0)
{
	aquire();
}

CriticalSection::Access::~Access()
{
	release();
}

inline void CriticalSection::Access::aquire()
{
	if(hasLockFlg)
		return;
	aquire(cs, mode);
	hasLockFlg = true;
}

inline void CriticalSection::Access::release()
{
	if(!hasLockFlg)
		return;
	release(cs, mode);
	hasLockFlg = false;
}

inline bool CriticalSection::TryAccess::tryAquire(CriticalSection& csIn,
                                                  const AccessMode& modeIn)
{
	switch(modeIn)
	{
	case read:
		return (csIn.tryEnter() != FALSE);
	case write:
		return (csIn.tryEnter() != FALSE);
	default:
		// Forgot one case?
		(false);
	}
	return false;
}

inline void CriticalSection::TryAccess::release(CriticalSection& csIn,
                                                const AccessMode& modeIn)
{
	switch(modeIn)
	{
	case read:
		{
			csIn.leave();
		}
		break;
	case write:
		{
			csIn.leave();
		}
		break;
	default:
		// Forgot one case?
		assert(false);
	}
}

inline CriticalSection::TryAccess::TryAccess(CriticalSection& csIn,
                                             const AccessMode& modeIn) :
	cs(csIn),
	mode(modeIn),
	gotLock(false)
{
	tryAquire();
}

inline CriticalSection::TryAccess::~TryAccess()
{
	release();
}

/// Returns, if getting access has worked.
inline bool CriticalSection::TryAccess::hasAccess() const
{
	return gotLock;
}

inline bool CriticalSection::TryAccess::tryAquire()
{
	if(!gotLock)
		gotLock = tryAquire(cs, mode);
	return gotLock;
}

inline void CriticalSection::TryAccess::release()
{
	if(hasAccess())
		release(cs, mode);
	gotLock = false;
}
