#include "work_flows.h"
#include "constants.h"
#include "observation.h"
#include "blob_detector.h"
#include "board_detector.h"
#include "helper.h"
#include "dmvs.h"
#include "enum_calibration_exit_code.h"
#include "calibration_file_base.h"
#include "calibration_cache.h"
#include "calibration_calculator.h"
#include "linear_table_frame_position.h"
#include "calibration_json_writer.h"
#include "configuration_container.h"
#include "ikinematics.h"
#include "kinematics_helpers.h"
#include "enum_camera_model.h"
#include "enum_image_channel_type.h"
#include "laser_dot_detector_2d.h"
#include "reconstructor_laser_dots_3d.h"
#include "projector_calibration_helper.h"
#include <opencv2/imgproc/imgproc_c.h>
#include "projector.h"
#include "board_detector_loader.h"
#include "KdTree/kdtree.h"
#include <regex>

using namespace DMVS::CameraCalibration;
using namespace LABS::MotionControl;

bool SetUpLinearAxis(IKinematics*& linear_axis)
{
	linear_axis = GetNextBestLinearStage();

	// Ensuring that linear stage is found and connected
	if(!linear_axis->Connect())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : No Linear Axis found";
		return false;
	}

	Helper::Sleeping(0.1, " to connect to Linear Axis ");

	// Move to first position
	const cv::Vec3d position(Constants::LINEAR_STAGE_LENGTH, 0, 0);

	//const cv::Vec3d position(0.005, 0, 0);
	linear_axis->MoveToPosition(position);
	linear_axis->WaitForMovementToFinish();

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Moving Linear Axis to " << Constants::LINEAR_STAGE_LENGTH << " as Initial Position";

	return true;
}

void SetUpCalibrationCache(const std::shared_ptr<ConfigurationContainer>& configuration_container,
                           const std::string& serial_number,
                           const std::string& file_name,
                           std::shared_ptr<CalibrationCache>& calibration_cache)
{
	calibration_cache->MaximumItems(100);
	calibration_cache->Attach(file_name, CalibrationCache::CACHE_WRITE | CalibrationCache::CACHE_ASYNC, CalibrationCache::MagicCode(DMVS_CAPTURE));

	if(calibration_cache->Put(0, Constants::DMVS_SERIAL_NO, serial_number) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Serial Number could not be saved in calibration cache";
	}

	if(calibration_cache->Put(0, Constants::DMVS_WIDTH, std::to_string(Constants::DMVS_IMAGE_HEIGHT)) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Image Width could not be saved in calibration cache";
	}

	if(calibration_cache->Put(0, Constants::DMVS_HEIGHT, std::to_string(Constants::DMVS_IMAGE_WIDTH)) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Image Height could not be saved in calibration cache";
	}

	if(calibration_cache->Put(0, Constants::DMVS_OPTIONS, configuration_container) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS Configuration options could not be saved in calibration cache";
	}
}

void PushImagesToCalibrationCache(std::shared_ptr<CalibrationCache>& calibration_cache,
                                  ChannelImageMap& channel_image_map,
                                  const int index)
{
	for(auto& channel_image_pair : channel_image_map.channel_image_map)
	{
		const auto channel = channel_image_pair.first;
		switch(channel)
		{
		case CHANNEL_GREY0:
			if(calibration_cache->Put(index, Constants::GREY0, channel_image_pair.second, CalibrationCacheItem::CV_RAW_MATRIX) != IOMessages::IO_OK)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::GREY0 << " Image could not be saved";
			}
			break;

		case CHANNEL_GREY1:
			if(calibration_cache->Put(index, Constants::GREY1, channel_image_pair.second, CalibrationCacheItem::CV_RAW_MATRIX) != IOMessages::IO_OK)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::GREY1 << " Image could not be saved";
			}
			break;

		case CHANNEL_BAYBG0:
			if(calibration_cache->Put(index, Constants::RAW_BAYER_BG, channel_image_pair.second, CalibrationCacheItem::CV_RAW_MATRIX) != IOMessages::
				IO_OK)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::POINT_CLOUD << " Image could not be saved";
			}
			break;

		default:
			break;
		}
	}
}

void RemoveSaturatedMarkers(const std::vector<ImageChannelCircularMarkerVectorMap>& circular_markers_maps,
                            std::map<ImageChannel, std::vector<CircularMarkerPair>>& unsaturated_markers_map)
{
	//frame counter
	auto counter = 0;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Ignoring over saturated markers [ threshold = " << Constants::MAX_AVERAGE_SURROUNDING_AREA << "]";

	for(const auto& circular_markers_map : circular_markers_maps)
	{
		for(const auto& circular_marker_pair : circular_markers_map)
		{
			auto channel = circular_marker_pair.first;

			for(const auto& circular_marker : circular_marker_pair.second)
			{
				//Ignore markers with too high surrounding -> likely to be over saturated
				if(circular_marker.average_surrounding_area > Constants::MAX_AVERAGE_SURROUNDING_AREA)
				{
					continue;
				}

				auto found = false;
				size_t pos = 0;
				CircularMarkerPair marker_pair_temp;

				for(const auto& found_marker_pair : unsaturated_markers_map[channel])
				{
					if(cv::norm(found_marker_pair.second.CorrectedCenter() - circular_marker.CorrectedCenter()) <
						Constants::TOLERANCE_CORRECTED_CENTER)
					{
						found            = true;
						marker_pair_temp = found_marker_pair;
						break;
					}

					pos++;
				}

				if(found)
				{
					//If contrast is better take this one
					const auto marker_temp_contrast = marker_pair_temp.second.average_surrounding_area - marker_pair_temp.second.average_inner_area;
					const auto marker_contrast      = circular_marker.average_surrounding_area - circular_marker.average_inner_area;

					if(marker_temp_contrast - marker_contrast < Constants::DOUBLE_EPSILON)
					{
						unsaturated_markers_map[channel][pos].first  = counter;
						unsaturated_markers_map[channel][pos].second = circular_marker;
					}
				}
				else
				{
					unsaturated_markers_map[channel].push_back(CircularMarkerPair(counter, circular_marker));
				}
			}
		}

		counter++;
	}
}

void DrawMarkers(ChannelImageVectorMap& image_vector_channel_map,
                 std::map<ImageChannel, std::vector<CircularMarkerPair>>& markers_combined_map)
{
	//Draw into images for debugging
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Displaying Images " << Constants::LOG_START;

	for(const auto& marker_pair : markers_combined_map)
	{
		// Marker pair consists of channel 20 or 21 and vector of images.
		auto& channel_images_map = image_vector_channel_map[marker_pair.first];

		for(const auto& marker : marker_pair.second)
		{
			cv::circle(channel_images_map[marker.first],
			           marker.second.CorrectedCenter(),
			           Constants::CIRCLE_RADIUS,
			           Constants::PURE_BLUE);
		}
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Displaying Images" << Constants::LOG_END;
}

void CombineMarkers(std::map<ImageChannel, std::vector<CircularMarkerPair>>& markers_combined_map,
                    std::map<ImageChannel, CircularMarkerVector>& combined_markers)
{
	for(auto& marker_pair : markers_combined_map)
	{
		CircularMarkerVector marker_vector;
		marker_vector.reserve(marker_pair.second.size());

		for(const auto& marker : marker_pair.second)
			marker_vector.emplace_back(marker.second);

		combined_markers[marker_pair.first] = marker_vector;
	}
}

void ChangeLedPower(DMVSSensor& dmvs,
                    const int frame_count)
{
	if(dmvs.IsLive())
	{
		auto power = Helper::CalculateLedPower(frame_count);

		if(power > 100)
			power = 100;

		if(!dmvs.SetLedPower(power))
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Led Power could not be set";
		}

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Led Power successfully set to [" << std::to_string(power) << "]";
	}
}

double FixExposureTime(const double exposure_time)
{
	auto fixed_exposure_time = exposure_time;

	if(!Helper::InRange(exposure_time, Constants::CAMERA_MIN_EXPOSURE_TIME_SEC, Constants::CAMERA_MAX_EXPOSURE_TIME_SEC))
	{
		if(Constants::CAMERA_MIN_EXPOSURE_TIME_SEC - exposure_time >= Constants::DOUBLE_EPSILON)
		{
			fixed_exposure_time = Constants::CAMERA_MIN_EXPOSURE_TIME_SEC;
		}
		else if(exposure_time - Constants::CAMERA_MAX_EXPOSURE_TIME_SEC >= Constants::DOUBLE_EPSILON)
		{
			fixed_exposure_time = Constants::CAMERA_MAX_EXPOSURE_TIME_SEC;
		}
	}
	// Trick to ensure 3 decimal place rounding
	fixed_exposure_time = std::round(fixed_exposure_time * 1000.0) / 1000.0;

	return fixed_exposure_time;
}

void ChangeExposureTime(DMVSSensor& dmvs,
                        const size_t frame_index)
{
	if(dmvs.IsLive())
	{
		auto exposure_time = Constants::CAMERA_MIN_EXPOSURE_TIME_SEC + frame_index * 0.3;
		exposure_time      = FixExposureTime(exposure_time);

		if(!dmvs.SetExposureTime(exposure_time))
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Exposure time could not be set ";
		}

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Exposure time successfully set to [" << exposure_time << "]";
	}
}

void SetPositionBasedExposureTime(DMVSSensor& dmvs,
                                  const int frame_count)
{
	if(dmvs.IsLive())
	{
		auto const position_factor = Helper::PositionFactor(frame_count, Constants::NUMBER_CALIBRATION_POSITIONS);
		auto exposure_time         = Constants::CAMERA_MIN_EXPOSURE_TIME_SEC + position_factor * Constants::CAMERA_MAX_EXPOSURE_TIME_SEC;
		exposure_time              = FixExposureTime(exposure_time);

		if(!dmvs.SetExposureTime(exposure_time))
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Exposure time could not be set";
		}

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Exposure time successfully set to [" << exposure_time << "]";
	}
}

void GrabImageSeries2(DMVSSensor& dmvs,
                      const ChannelImageMap& channel_image_map,
                      ChannelImageVectorMap& image_series)
{
	// Grab a series of images for every image channel
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Grabbing series of images [" << Constants::NUMBER_OF_IMAGES_IN_SERIES << " images ] " <<
	//		Constants::LOG_START;

	for(size_t image_series_index = 0; image_series_index < Constants::NUMBER_OF_IMAGES_IN_SERIES; ++image_series_index)
	{
		//Retrieve all channels and add to the image series
		for(const auto& image_channel_pair : channel_image_map.channel_image_map)
		{
			const auto channel         = image_channel_pair.first;
			const auto retrieved_image = image_channel_pair.second;

			//IR channel, Bayer channel just copy image (one channel images)
			//assert(retrieved_image.channels() == 1);

			image_series[channel].push_back(retrieved_image.clone());
		}
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Grabbing series of images [" << Constants::NUMBER_OF_IMAGES_IN_SERIES << " images ] " <<
 //Constants::LOG_END;
}

void GrabImageSeries(DMVSSensor& dmvs,
                     const ChannelImageMap& channel_image_map,
                     ChannelImageVectorMap& image_series)
{
	// Grab a series of images for every image channel
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Grabbing series of images [" << Constants::NUMBER_OF_IMAGES_IN_SERIES << " images ] " <<
	//	Constants::LOG_START;

	for(size_t index = 0; index < Constants::NUMBER_OF_IMAGES_IN_SERIES; ++index)
	{
		dmvs.GrabFrame();

		//Retrieve all channels and add to the image series
		for(const auto& image_channel_pair : channel_image_map.channel_image_map)
		{
			const auto channel = image_channel_pair.first;
			cv::Mat retrieved_image;
			dmvs.RetrieveImageFromCalibrationCache(dmvs.frame_index, channel, retrieved_image);

			//IR channel, Bayer channel just copy image (one channel images)
			if(retrieved_image.channels() == 1)
			{
				image_series[channel].push_back(retrieved_image.clone());
			}
		}
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Grabbing series of images [" << Constants::NUMBER_OF_IMAGES_IN_SERIES << " images ] " <<
	//	Constants::LOG_END;
}

void WriteAverageImage(const ApplicationSettings& settings,
                       const int position_index,
                       const size_t frame_index_at_position,
                       const cv::Mat& average_image)
{
	const auto file_name = "AverageImage_Pos_" + std::to_string(position_index).append("_Frame_").append(std::to_string(frame_index_at_position)).
	                                                                            append(".png");

	const auto average_filepath = settings.storage_path + "\\" + file_name;

	cv::imwrite(average_filepath, average_image);
}

void AverageImageSeries(ChannelImageMap& channel_image_map,
                        ChannelImageVectorMap& image_vector_channel_map,
                        const int position_index,
                        const size_t frame_index_at_position,
                        ChannelImageVectorMap& image_series,
                        const ApplicationSettings& settings)
{
	// Average image series
	const auto rows = channel_image_map.channel_image_map[CHANNEL_GREY0].rows;
	const auto cols = channel_image_map.channel_image_map[CHANNEL_GREY0].cols;

	cv::Mat combined_average_image(rows, cols, CV_8UC3);
	cv::Mat average_images[] = {cv::Mat(rows, cols, CV_8UC3), cv::Mat(rows, cols, CV_8UC3)};

	for(const auto& image_channel_pair : channel_image_map.channel_image_map)
	{
		const auto image_channel = image_channel_pair.first;

		// Does not work for color images because it is BGR here and m_output saves Bayer pattern
		auto average_image = Helper::AverageImages(image_series[image_channel]);

		// Add the averaged images to the image collection
		channel_image_map.channel_image_map[image_channel] = average_image.clone();
		image_vector_channel_map[image_channel].push_back(average_image.clone());

		const auto idx = image_channel == CHANNEL_GREY0 ? 0 : 1;
		average_image.copyTo(average_images[idx]);
	}

	cv::hconcat(average_images, 2, combined_average_image);

	if(settings.record)
	{
		WriteAverageImage(settings, position_index, frame_index_at_position, combined_average_image);
	}
}

void FrameProcessing(DMVSSensor& dmvs,
                     ChannelImageMap& channel_image_map,
                     BlobDetector& blob_detector,
                     std::vector<ImageChannelCircularMarkerVectorMap>& found_circular_markers_map,
                     ChannelImageVectorMap& image_vector_channel_map,
                     const int current_frame_index,
                     const int recorded_position_count,
                     const ApplicationSettings& settings)
{
	for(size_t frame_index_at_position = 0; frame_index_at_position < Constants::FRAMES_PER_POSITION; ++frame_index_at_position)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Frame Index [" << frame_index_at_position << "] at Position Index [" <<
 recorded_position_count << "]";

		if(dmvs.IsLive())
		{
			ChangeExposureTime(dmvs, frame_index_at_position);
			ChannelImageVectorMap image_series;
			GrabImageSeries(dmvs, channel_image_map, image_series);
			AverageImageSeries(channel_image_map, image_vector_channel_map, recorded_position_count, frame_index_at_position, image_series, settings);
		}
		else
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DMVS is not live";

			for(auto it = channel_image_map.channel_image_map.begin(); it != channel_image_map.channel_image_map.end(); ++it)
			{
				auto image_channel = it->first;
				cv::Mat retrieved_image;
				dmvs.RetrieveImageFromCalibrationCache(current_frame_index + 1, it->first, retrieved_image);

				//add the averaged images to the image collection
				channel_image_map.channel_image_map[image_channel] = retrieved_image.clone();
				image_vector_channel_map[image_channel].push_back(retrieved_image.clone());
			}
		}

		ImageChannelCircularMarkerVectorMap circular_markers_current_frame;
		Helper::DetectMarkers(channel_image_map, blob_detector, circular_markers_current_frame);
		Helper::RemoveLowQualityMarker(circular_markers_current_frame, channel_image_map);
		found_circular_markers_map.push_back(circular_markers_current_frame);
	}
}

void FrameProcessing2(DMVSSensor& dmvs,
                      ChannelImageMap& channel_image_map,
                      BlobDetector& blob_detector,
                      std::vector<ImageChannelCircularMarkerVectorMap>& found_circular_markers_map,
                      ChannelImageVectorMap& channel_image_vector_map,
                      const int recorded_position_count,
                      const ApplicationSettings& settings)
{
	ChannelImageVectorMap image_series;
	GrabImageSeries2(dmvs, channel_image_map, image_series);
	AverageImageSeries(channel_image_map, channel_image_vector_map, recorded_position_count, 1, image_series, settings);

	ImageChannelCircularMarkerVectorMap circular_markers_current_frame;
	Helper::DetectMarkers(channel_image_map, blob_detector, circular_markers_current_frame);
	Helper::RemoveLowQualityMarker(circular_markers_current_frame, channel_image_map);

	found_circular_markers_map.push_back(circular_markers_current_frame);
}

void CopyChannelImageMap(const ChannelImageMap& source,
                         ChannelImageMap& destination)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Copying Channel Image Map" << Constants::LOG_START;

	for(const auto& channel_image_pair : source.channel_image_map)
	{
		const auto channel = channel_image_pair.first;

		// Create a color image as copy to draw feedback into the image
		if(channel_image_pair.second.channels() == Constants::NUMBER_OF_CHANNELS)
		{
			destination.channel_image_map[channel] = channel_image_pair.second.clone();
		}
		else
		{
			// First sensor line starts with BG for some reason OpenCV uses
			// 2nd line, 2nd 3rd column to define the Bayer code -> RG
			if(channel == CHANNEL_BAYBG0)
			{
				cv::cvtColor(channel_image_pair.second,
				             destination.channel_image_map[CHANNEL_BGR0],
				             cv::COLOR_BayerRG2BGR_EA);
			}

			cv::Mat image_copy;
			cv::cvtColor(channel_image_pair.second, image_copy, cv::COLOR_GRAY2RGB);

			destination.channel_image_map[channel] = image_copy;
		}
	}
}

CalibrationExitCode RetrieveChannelImages(DMVSSensor& dmvs,
                                          ChannelImageMap& channel_image_map,
                                          const int frame_index)
{
	auto is_retrieved = true;

	for(auto& image_channel_pair : channel_image_map.channel_image_map)
	{
		cv::Mat image;
		const auto channel = image_channel_pair.first;
		is_retrieved &= dmvs.IsLive()
			                ? dmvs.RetrieveImageFromCalibrationCache(dmvs.frame_index, channel, image)
			                : dmvs.RetrieveImageFromCalibrationCache(frame_index + 1, channel, image);

		image_channel_pair.second = image.clone();
	}

	return is_retrieved ? CALIBRATION_IMAGE_RETRIEVED : CALIBRATION_IMAGE_NOT_RETRIEVED;
}

void WriteCollage(const int position_idx,
                  const cv::Mat& collage)
{
	const auto file_name         = "Collage_" + std::to_string(position_idx).append(".png");
	const auto collage_file_path = Helper::GetCalibrationImagesCollageDirectory().append(file_name);
	cv::imwrite(collage_file_path.string(), collage);
}

void CreateAndShowCollage(ChannelImageMap image_channel,
                          const int position_idx)
{
	cv::Mat collage;
	Helper::CreateCollage(image_channel.channel_image_map[CHANNEL_GREY0],
	                      image_channel.channel_image_map[CHANNEL_GREY1],
	                      collage);

	cv::namedWindow("Collage", cv::WINDOW_NORMAL);
	cv::imshow("Collage", collage);
	WriteCollage(position_idx, collage);
	cv::waitKey(100);
}

void CalculateStatistics(const std::shared_ptr<CalibrationFileBase>& file_base,
                         Frame& all_frame_images,
                         CalibrationCalculator& calibration_calculator,
                         ApplicationSettings* settings)
{
	// Create the output directory for statistics and open output file for overall result data
	std::ofstream out_file;
	std::shared_ptr<fs::path> stats_dir;

	if(file_base->GetCameraCalibrationStatisticsOutputDirectory(stats_dir) != IOMessages::IO_OK)
	{
		stats_dir = nullptr;
	}
	else
	{
		Helper::CopyReportData(stats_dir->string());
		auto out_file_name = stats_dir->append("detailedResults.txt");
		out_file.open(out_file_name.c_str(), std::ofstream::out);
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : out_file_name " << out_file_name;
	}

	calibration_calculator.AppendFrameAndBoardPositions(out_file);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Observations Total [" << calibration_calculator.GetObservationSize() << "]";
	std::stringstream stream;
	stream << "BoardID   " << "FrID " << " CamID " << "Er_X" << " Er_Y " << " Er_Rad  " << " Er_Tan " <<
		" MeanX" << " MeanY " << "CamLocal X " << " Y  " << " Z " << " MarkerID \n";
	out_file << stream.str();

	for(auto& image_frame : all_frame_images.frame_images)
	{
		auto frame_id = image_frame.first;
		for(auto& image_channel_map_in : image_frame.second.channel_image_map)
		{
			auto channel = image_channel_map_in.first == CHANNEL_BAYBG0
				               ? CHANNEL_BGR0
				               : image_channel_map_in.first;

			if(out_file.is_open())
			{
				calibration_calculator.AppendErrors(out_file, frame_id, channel);
			}

			auto image = image_channel_map_in.second.clone();
			cv::cvtColor(image_channel_map_in.second, image, cv::COLOR_GRAY2BGR);
			calibration_calculator.DrawErrors(image, frame_id, channel);

			if(settings)
			{
				auto debug_dir = settings->storage_path + R"(\Debug)";

				if(!boost::filesystem::is_directory(debug_dir))
				{
					boost::filesystem::create_directory(debug_dir);
				}

				auto file_name = debug_dir + R"(\error_)" + std::to_string(frame_id) + "_" + std::to_string(channel) + ".png";
				cv::imwrite(file_name, image);
			}

			// Create directory for images with projection errors
			std::shared_ptr<fs::path> output;
			if(file_base->GetCameraCalibrationImageOutputDirectory(channel, output) == IOMessages::IO_OK)
			{
				auto filename = Constants::FILE_FRAME + std::to_string(frame_id)
				                                        .append(Constants::FILE_CHANNEL).append(std::to_string(channel)).append(Constants::FILE_PNG);
				auto full_file_name = output->append(filename);
				std::vector<int> compression_params;
				compression_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
				compression_params.push_back(3);
				cv::imwrite(full_file_name.string(), image, compression_params);
			}
			else
			{
				output = nullptr;
			}
		}
	}

	out_file.close();
}

int FactoryCameraCalibration(int ac,
                             char* av[])
{
	ApplicationSettings settings(ac, av);
	auto exit_code               = CALIBRATION_OK;
	auto configuration_container = std::make_shared<ConfigurationContainer>();
	DMVSSensor dmvs;

	if(dmvs.SetUpSensor(configuration_container) != CALIBRATION_SENSOR_SETUP_OK)
	{
		return CALIBRATION_SENSOR_SETUP_ERROR;
	}

	IKinematics* linear_axis;
	if(!SetUpLinearAxis(linear_axis))
	{
		return CALIBRATION_NO_LINEAR_AXIS_FOUND;
	}

	const std::string serial_number = dmvs.GetConnectedDMVS()->GetSerialNumber();
	const auto file_base            = CalibrationFileBase::GetFileBase(serial_number, configuration_container);

	if(!file_base)
		return IOMessages::IO_UNDEFINED;

	std::string file_name;
	file_base->GetCameraCalibrationFSVOutputFile(file_name);
	auto calibration_cache = std::make_shared<CalibrationCache>();
	SetUpCalibrationCache(configuration_container, serial_number, file_name, calibration_cache);

	// Add empty images for grey0, grey1 channel
	ChannelImageMap channel_image_map, image_channel_map_copy;
	channel_image_map.channel_image_map[CHANNEL_GREY0] = cv::Mat();
	channel_image_map.channel_image_map[CHANNEL_GREY1] = cv::Mat();

	BlobDetector blob_detector;
	std::vector<std::shared_ptr<DMVS::CameraCalibration::BoardDetector>> boards;
	if(dmvs.IsLive() && !DMVS::CameraCalibration::BoardDetector::LoadDetectorBoards(file_base, boards, configuration_container))
		return CALIBRATION_BOARD_NOT_LOADED;

	// Collect all marker detections called observations: (frame/board/marker/camera/coordinates)
	// Data structure to keep the images of all channels in all frames
	// e.g. all_images.frame[nFrame].channel[CHANNEL_GREY0] = cv::Mat()
	Frame frame_images;

	// Flag for indicating if the calibration is done
	auto is_calibrated = false;

	// Counter for frame per position
	int position_idx = 0;

	// Index of the current frame
	int frame_index = 0;

	ChangeLedPower(dmvs, position_idx);

	std::vector<std::shared_ptr<Observation>> observations;
	std::map<size_t, double> linear_axis_positions;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finding Observations " << Constants::LOG_START;

	while(!is_calibrated)
	{
		//dmvs.Stop();
		SetPositionBasedExposureTime(dmvs, position_idx);

		if(!dmvs.PushChannelImageToCache())
		{
			exit_code = CALIBRATION_IMAGE_GRABBING_ERROR;
			break;
		}

		// Retrieve images of all Freestyle channels
		if(RetrieveChannelImages(dmvs, channel_image_map, frame_index) != CALIBRATION_IMAGE_RETRIEVED)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::LOG_NO_IMAGE_RETRIEVED;
			continue;
		}

		CopyChannelImageMap(channel_image_map, image_channel_map_copy);
		Helper::CopyImages(image_channel_map_copy, frame_images, position_idx);

		std::vector<ImageChannelCircularMarkerVectorMap> found_circular_markers_map;
		found_circular_markers_map.reserve(Constants::FRAMES_PER_POSITION);
		ChannelImageVectorMap image_vector_channel_map;

		FrameProcessing(dmvs,
		                channel_image_map,
		                blob_detector,
		                found_circular_markers_map,
		                image_vector_channel_map,
		                frame_index,
		                position_idx,
		                settings);

		PushImagesToCalibrationCache(calibration_cache, channel_image_map, position_idx);
		std::map<ImageChannel, std::vector<CircularMarkerPair>> markers_combined_map;
		RemoveSaturatedMarkers(found_circular_markers_map, markers_combined_map);
		DrawMarkers(image_vector_channel_map, markers_combined_map);
		std::map<ImageChannel, CircularMarkerVector> markers_combined;
		CombineMarkers(markers_combined_map, markers_combined);
		auto coded_marker_count = Helper::AddCodedMarkers(boards, markers_combined, position_idx, observations, image_channel_map_copy);

		if(coded_marker_count < Constants::MIN_COUNT_CODED_MARKER)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Not enough coded markers found, trying again. Found only [" << coded_marker_count <<
					"] marker";
			continue;
		}

		Helper::AddUncodedMarkers(markers_combined, observations, position_idx);
		CreateAndShowCollage(image_channel_map_copy, position_idx);

		position_idx++;

		ChangeLedPower(dmvs, position_idx);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Frame Count [" << position_idx << "] Number of Calibration Positions [" << Constants::
				NUMBER_CALIBRATION_POSITIONS << "]";

		if(position_idx >= int(Constants::NUMBER_CALIBRATION_POSITIONS))
			is_calibrated = true;

		if(dmvs.IsLive())
		{
			if(!dmvs.Stop())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Stop Acquisition error ";
			}

			//Helper::Sleeping(1, "while dmvs stops acquisition";
			auto position_current             = linear_axis->GetPosition();
			auto const current_position_value = position_current->at<double>(0, 3);
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Current Position [" << current_position_value << "] Frame Index [" << frame_index <<
					"] Position Idx [" << position_idx << "]";

			// Save the actual position from the linear axis
			size_t idx_positions                 = position_idx == 0 ? position_idx : position_idx - 1;
			linear_axis_positions[idx_positions] = current_position_value;

			//if(calibration_cache->Put(frame_index, "LinearStagePosition", *position_current, CalibrationCacheItem::CV_RAW_MATRIX) != IOMessages::IO_OK)
			//{
			//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Linear Stage Position could not be saved in Calibration Cache";
			//}

			if(!is_calibrated)
			{
				auto next_position_value = current_position_value - Constants::LINEAR_STAGE_STEPPING;
				linear_axis->MoveToPosition(next_position_value);
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Moving to next Position [" << next_position_value << "]";
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Frame Count [" << position_idx << "] Calibration position count [" << Constants::
						NUMBER_CALIBRATION_POSITIONS << "]";

				// Check if the image is same or different what is getting store in the cache and what is being read
				linear_axis->WaitForMovementToFinish();
			}

			delete position_current;
		}
	}

	linear_axis->MoveToPosition(0.20);
	linear_axis->WaitForMovementToFinish();
	delete linear_axis;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Finding Observations " << Constants::LOG_END;

	if(!dmvs.Stop())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Stop Acquisition error ";
	}

	CalibrationCalculator calibration_calculator(observations);

	// Write stepping to position file
	std::string linear_table_position_file;
	file_base->GetCameraCalibrationLinearStagePositionOutputFile(linear_table_position_file);

	//cv::FileStorage fs(linear_table_position_file, cv::FileStorage::WRITE);
	//cv::Mat position_new(1, 1, CV_64F);
	//position_new.at<double>(0, 0) = Constants::LINEAR_STAGE_STEPPING;
	//fs << "linearTablePositions" << position_new;
	//fs.release();

	std::shared_ptr<CameraSystemTransformation> linear_table_frame_position = std::make_shared<LinearTableFramePosition>(linear_axis_positions);
	calibration_calculator.SetCameraSystemTransformation(linear_table_frame_position);

	std::set<size_t> detected_boards_set;

	// CamsModifier: Add all channel IDs as Camera IDs
	std::set<size_t> camera_ids;

	for(const auto& observation : observations)
	{
		if(observation->GetBoardId() != Constants::DEFAULT_SIZE_T_VALUE)
			detected_boards_set.insert(observation->GetBoardId());

		camera_ids.insert(observation->GetImageChannel());
	}

	if(camera_ids.empty())
		return CALIBRATION_STOPPED_BY_USER;

	std::vector<std::shared_ptr<DMVS::CameraCalibration::BoardDetector>> detected_boards;
	for(const auto& board : boards)
	{
		if(detected_boards_set.find(board->GetBoardId()) != detected_boards_set.end())
			detected_boards.push_back(board);
	}

	// Set used calibration boards
	calibration_calculator.SetBoards(detected_boards);
	std::map<size_t, std::shared_ptr<const CameraSensor>> camera_sensor_map;
	//camera_sensor_map[CHANNEL_GREY0] = DMVSSensor::GetDMVSCameraCameraCalibration("left",
	//                                                                              CHANNEL_GREY0,
	//                                                                              DEPTH_DEPENDENT_MODEL,
	//                                                                              0.40,

	//                                                                              cv::Mat(3, 3, CV_64F));
	//camera_sensor_map[CHANNEL_GREY1] = DMVSSensor::GetDMVSCameraCameraCalibration("right",
	//                                                                              CHANNEL_GREY1,
	//                                                                              DEPTH_DEPENDENT_MODEL,
	//                                                                              0.40,
	//                                                                              cv::Mat(3, 3, CV_64F));

	// why is this done for only one CHANNEL_GREY0
	calibration_calculator.SetRelativeFixedCamera(camera_sensor_map, CHANNEL_GREY0);
	calibration_calculator.ProcessObservationsInfield();

	CalculateStatistics(file_base, frame_images, calibration_calculator);

	// get a Freestyle default calibration and update with new Freestyle calibration data as calculated
	fs::path default_cal_file_name;
	file_base->GetDefaultCameraCalibration(default_cal_file_name);
	CaptureCalibration freestyle_default_calibration;

	if(!freestyle_default_calibration.LoadFile(default_cal_file_name))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot load default Calibration data";

		return CALIBRATION_REQUIRED_FILE_MISSING;
	}

	auto rms_right         = calibration_calculator.GetChannelRMS(CHANNEL_GREY0);
	auto rms_left          = calibration_calculator.GetChannelRMS(CHANNEL_GREY1);
	auto rms_right_tan_rad = calibration_calculator.GetChannelRMSTanRad(CHANNEL_GREY0);
	auto rms_left_tan_rad  = calibration_calculator.GetChannelRMSTanRad(CHANNEL_GREY1);

	// Define file name for PDF report
	std::string pdf_report;
	file_base->GetCameraCalibrationPDFOutputFile(pdf_report);

	// define image for report
	std::string logo_path = "/companyLogo.png";
	configuration_container->SetInfieldReportLogoPath(logo_path);
	configuration_container->SetColorCameraRMS(0.0);
	configuration_container->SetLeftCameraRMS(rms_left);
	configuration_container->SetRightCameraRMS(rms_right);
	configuration_container->SetFactoryCalibrationMaxRMS(Constants::CALIBRATION_MAX_PERMITTED_RMS_ERROR);
	auto success = rms_right < Constants::CALIBRATION_MAX_PERMITTED_RMS_ERROR && rms_left < Constants::CALIBRATION_MAX_PERMITTED_RMS_ERROR;
	//configuration_container->SetFactoryCalibrationSuccessful(success);

	if(!success)
		exit_code = CALIBRATION_FAILED_RMS;

	// Write out camera calibration info to JSON
	std::string camera_calibration_folder;

	if(file_base->GetCameraCalibrationFolder(camera_calibration_folder) == IOMessages::IO_OK)
	{
		CalibrationJSONWriter::Write(camera_calibration_folder,
		                             rms_left,
		                             rms_right,
		                             rms_left_tan_rad.first,
		                             rms_right_tan_rad.first,
		                             rms_left_tan_rad.second,
		                             rms_right_tan_rad.second,
		                             success,
		                             Constants::JSON_SERIAL_NUMBER);
	}

	calibration_calculator.GetChannelResult(freestyle_default_calibration, CHANNEL_GREY0);
	calibration_calculator.GetChannelResult(freestyle_default_calibration, CHANNEL_GREY1);
	calibration_cache->Close();

	// Write new calibration data to output file
	std::string result_file_name;
	file_base->GetCameraCalibrationYMLOutputFile(result_file_name);
	auto save_success = false;

	const auto suffix = save_success ? Constants::LOG_OK : Constants::LOG_FAILED;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Save calibration to file " << result_file_name << "() : " << suffix;

	freestyle_default_calibration.Save(Helper::GetCaptureDeviceFile(), "");

	return exit_code;
}

int ProjectorCalibration()
{
	//auto exit_code = CALIBRATION_OK;
	auto configuration_container = std::make_shared<ConfigurationContainer>();
	DMVSSensor dmvs;

	if(dmvs.SetUpSensor(configuration_container) != CALIBRATION_SENSOR_SETUP_OK)
	{
		return CALIBRATION_SENSOR_SETUP_ERROR;
	}

	// Init linear stage
	IKinematics* linear_stage;

	if(!SetUpLinearAxis(linear_stage) && !linear_stage)
	{
		return CALIBRATION_NO_LINEAR_AXIS_FOUND;
	}

	if(linear_stage->MoveToPosition(cv::Vec3d(Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START, 0.0, 0.0)))
	{
		linear_stage->SetVelocity(0.8);
	}

	auto position = linear_stage->GetPosition();

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Position " << position;

	if(position->at<double>(0, 3) - Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START > 0.0000001)
	{
		if(linear_stage->MoveToPosition(cv::Vec3d(Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START, 0.0, 0.0)))
		{
			linear_stage->WaitForMovementToFinish();
		}
	}

	const std::string serial_number = dmvs.GetConnectedDMVS()->GetSerialNumber();

	// Initialize directory and filename management
	const auto file_base = CalibrationFileBase::GetFileBase(serial_number, configuration_container);

	if(!file_base)
	{
		return IOMessages::IO_UNDEFINED;
	}

#pragma region Preloop

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : PRE LOOP " << Constants::LOG_START;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Setting up Device and Data";

	//auto file_base     = std::shared_ptr<CalibrationFileBase>();
	/*std::string sensor;

	if(dmvs.IsLive())
	{
		sensor = dmvs.GetCaptureDeviceIdent();
	}*/

	//if(IOMessages::IO_OK != file_base->Init(sensor, ""))
	//{
	//	//TODO Check This
	//	auto read_config_error = std::string(
	//		R"(Could neither read nor create config xml file. Filename = C:\ProgramData\FARO\Scenect\calibrationConfig.xml)");
	//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << read_config_error;

	//	return IOMessages::IO_UNDEFINED;
	//}

	// Active log. Save old log settings first.
	std::string log_file;
	file_base->GetProjectorCalibrationLogOutputFile(log_file);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DMVS " << serial_number;

	// device images (color, IR0, IR1)
	cv::Mat pattern_image0, pattern_image1;

	// init the loop exit code
	auto exit_code = CALIBRATION_OK;

	// channel images for user feedback
	cv::Mat channel_image, projector_image0, projector_image1, projector_image;

	std::string calibration_json_file = R"(C:\ProgramData\FARO\Freestyle\Cameras\DMVS_DMVS011900031.json)";

	if(!calibration_json_file.empty())
	{
		fs::path filename(calibration_json_file);
		dmvs.capture_calibration.LoadCalibrationFromJsonFile(filename);
	}
	else
	{
		if(dmvs.cache->Get(0, "calibration", configuration_container) == IOMessages::IO_OK)
		{
			dmvs.capture_calibration.FillFromContainer(configuration_container->GetConfigurationDataFile());
		}
	}

	//auto device_type_option = std::make_shared<ConfigurationContainer>();
	if(dmvs.cache->Get(0, "deviceOptions", configuration_container) == IOMessages::IO_OK)
	{
		dmvs.configuration_options = configuration_container;
	}

	dmvs.configuration_options->SetProjectorCalMinBeamLength(Constants::PROJECTOR_CALIBRATION_MINIMUM_BEAM_LENGTH);

	// Projector object to keep all recorded points and do beam calculations
	const auto projector_calibration = dmvs.capture_calibration.GetCalibration(CHANNEL_PROJECTOR0);

	if(!projector_calibration)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot access projector calibration";
		return CALIBRATION_FAILED;
	}

	Projector projector(projector_calibration, dmvs.configuration_options);
	projector.SetOutputPath(log_file);

	std::string doe_yml;
	file_base->GetProjectorCalibrationDoeYML(doe_yml);

	if(!fs::exists(doe_yml))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : DOE definition file not found. Expected it at " << doe_yml;

		return IOMessages::IO_UNDEFINED;
	}

	projector.SetDoeYmlPath(doe_yml);
	projector.InitCalibration();

	// main capture loop
	auto is_projector_calibrated = false;

	// ExposureTime in ms
	auto ir_exposure_time_at_1m_ms = Constants::PROJECTOR_CALIBRATION_DEFAULT_IR_EXPOSURE_TIME_AT_1M_MS;

	if(!dmvs.SetExposureTime(ir_exposure_time_at_1m_ms))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Exposure time could not be set";
	}

	// Load 6dof target configuration ( ring marker positions )
	//const auto reference_board_filename = R"(C:\ProgramData\FARO\Freestyle\Calibrations\Boards\board_02.cpd)";
	const auto reference_board_filename = R"(C:\CalibrationDataFS\10 input\CalibBoardTemplate.cpd)";
	//file_base->GetInfieldCalibrationBoardTemplate(reference_board_filename);
	BoardDetectorLoader board_detector_loader;

	if(!board_detector_loader.ReadFile(reference_board_filename))
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Could not reference board file " << reference_board_filename;
		return NO_PARAMETERS_GIVEN;
	}

	// Create a 6dof target of coded ring marker board
	auto board_detector_grey0 = board_detector_loader.CreateDetector(dmvs.capture_calibration.GetCalibration(CHANNEL_GREY0));
	auto board_detector_grey1 = board_detector_loader.CreateDetector(dmvs.capture_calibration.GetCalibration(CHANNEL_GREY1));

	// Copy file to calibration folder
	std::string calibration_base_folder;
	file_base->GetProjectorCalibrationOutputFolder(calibration_base_folder);

	//TODO
	//ConfigurationContainer configuration;
	fs::copy_file(reference_board_filename, calibration_base_folder);

	auto initialized_step4 = false;

	// Try to find the Board with IR System
	cv::Vec3d plane_position_start, plane_position_end;
	cv::Vec3d plane_normal_start, plane_normal_end;
	std::map<size_t, CircularMarker> start_markers, end_markers;
	//reference_board_filename = R"(C:\ProgramData\FARO\Freestyle\Calibrations\Boards\board_02.cpd)";

	if(dmvs.IsLive())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Determining Calibration Plate Position";

		if(!dmvs.FindTablePosition(linear_stage,
		                           plane_position_start,
		                           plane_position_end,
		                           plane_normal_start,
		                           plane_normal_end,
		                           board_detector_grey0,
		                           start_markers,
		                           end_markers))
		{
			return CALIBRATION_FAILED;
		}
	}
	else
	{
		cv::Mat plane_position_end_cv_saved(3, 1, CV_64F);
		cv::Mat plane_position_start_cv_saved(3, 1, CV_64F);
		cv::Mat plane_normal_end_cv_saved(3, 1, CV_64F);
		dmvs.cache->Get(0, "PlateCalibrationStart", plane_position_start_cv_saved, CalibrationCacheItem::CV_MATRIX);
		dmvs.cache->Get(0, "PlateCalibrationEnd", plane_position_end_cv_saved, CalibrationCacheItem::CV_MATRIX);
		dmvs.cache->Get(0, "PlateCalibrationNormalEnd", plane_normal_end_cv_saved, CalibrationCacheItem::CV_MATRIX);

		for(auto i = 0; i < 3; ++i)
		{
			plane_position_start[i] = plane_position_start_cv_saved.at<double>(i, 0);
			plane_position_end[i]   = plane_position_end_cv_saved.at<double>(i, 0);
			plane_normal_end[i]     = plane_normal_end_cv_saved.at<double>(i, 0);
		}
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration Plate Position End" << plane_position_end;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration Plate Position Start" << plane_position_start;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration Plate Normal End" << plane_normal_end;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration Plate Normal Start" << plane_normal_start;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : PRE LOOP " << Constants::LOG_END;

	//Helper::Sleeping(0.1, "Wait for Pre - Loop to end");

	std::vector<cv::Point3f> all_3d_points;
	CalibrationCache calibration_cache;
	std::string fsv_output_file;
	file_base->GetProjectorCalibrationFSVOutputFile(fsv_output_file);
	calibration_cache.Attach(fsv_output_file,
	                         CalibrationCache::CACHE_WRITE | CalibrationCache::CACHE_ASYNC,
	                         CalibrationCache::MagicCode(DMVS_CAPTURE));

	// Store sensor infos in the recording file
	// Note: Don't use getCaptureDeviceIdent to get the string. This doesn't work.
	if(calibration_cache.Put(0, "sensor", serial_number) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : sensor serial number could not be pushed to cache";
	}

	if(calibration_cache.Put(0, "width", std::to_string(Constants::DMVS_DOUBLE_CAMERA_IMAGE_WIDTH)) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Width could not be pushed to cache ";
	}

	if(calibration_cache.Put(0, "height", std::to_string(Constants::DMVS_DOUBLE_CAMERA_IMAGE_HEIGHT)) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Height could not be pushed to cache ";
	}

	if(calibration_cache.Put(0, "deviceOptions", dmvs.configuration_options) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Device options could not be pushed to cache ";
	}

	unsigned int frame_index_sfm = 0;
	cv::Mat plane_position_start_cv(3, 1, CV_64F);
	cv::Mat plane_position_end_cv(3, 1, CV_64F);
	cv::Mat plane_normal_end_cv(3, 1, CV_64F);

	for(auto i = 0; i < 3; ++i)
	{
		plane_position_start_cv.at<double>(i, 0) = plane_position_start[i];
		plane_position_end_cv.at<double>(i, 0)   = plane_position_end[i];
		plane_normal_end_cv.at<double>(i, 0)     = plane_normal_end[i];
	}

	if(calibration_cache.Put(0, "PlateCalibrationStart", plane_position_start_cv, CalibrationCacheItem::CV_MATRIX) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Plate Calibration start could not be pushed to cache ";
	}

	if(calibration_cache.Put(0, "PlateCalibrationEnd", plane_position_end_cv, CalibrationCacheItem::CV_MATRIX) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Plate Calibration end could not be pushed to cache ";
	}

	if(calibration_cache.Put(0, "PlateCalibrationNormalEnd", plane_normal_end_cv, CalibrationCacheItem::CV_MATRIX) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Plate Calibration Normal end could not be pushed to cache ";
	}

	// Save calibration boards
	auto recording_info = std::make_shared<ConfigurationContainer>();
	recording_info->Initialize();
	/*recordingInfo->setCustomAttrValue("Content", std::string("FactoryCalibration"));*/

	if(calibration_cache.Put(0, "recordingInfo", recording_info) != IOMessages::IO_OK)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Recording info could not be pushed to cache ";
	}

	// reset stop
	dmvs.stop = false;

	if(!dmvs.SetScanSettings())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Scan Settings could not be set ";
	}

	unsigned int successful_frames_per_position = 0;
	unsigned int initial_exposure_test_frame    = 0;

	// At the beginning we try a really low exposure time to detect the central beam in case it is too bright
	while(!dmvs.stop)
	{
		if(initial_exposure_test_frame < Constants::PROJECTOR_CALIBRATION_INITIAL_EXPOSURE_TEST_FRAMES)
		{
			if(!dmvs.SetExposureTime(0.3))
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Exposure time could not be set ";
			}

			initial_exposure_test_frame++;
		}

#pragma region captureAndCountour

		// save last and grab new frame
		// Only grab and save an image if grab(bool synced = true) is called
		if(!dmvs.PushChannelImageToCache())
		{
			exit_code = CALIBRATION_NO_DEVICE;
			break;
		}

		// Get images
		auto retrieve_ok = true;

		for(auto i = 0; i < 3; ++i)
		{
			retrieve_ok &= dmvs.RetrieveImageFromCalibrationCache(dmvs.frame_index, CHANNEL_GREY0, pattern_image0);
			retrieve_ok &= dmvs.RetrieveImageFromCalibrationCache(dmvs.frame_index, CHANNEL_GREY1, pattern_image1);
		}

		if(!retrieve_ok)
			continue;

		// Info data is needed for each frame
		auto info = std::make_shared<ConfigurationContainer>();
		info->Initialize();

		/*SFMCore::set(info, "hubTemp", 12.345);
		SFMCore::set(info, "grabts", GetTickCount64() / 1000.0);*/
		if(calibration_cache.Put(frame_index_sfm, "info", info) != IOMessages::IO_OK)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Info could not be pushed to cache ";
		}

		if(calibration_cache.Put(frame_index_sfm, Constants::GREY0, pattern_image0, CalibrationCacheItem::CV_RAW_MATRIX) != IOMessages::IO_OK)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Pattern image gray 0 could not be pushed to cache ";
		}

		if(calibration_cache.Put(frame_index_sfm, Constants::GREY1, pattern_image1, CalibrationCacheItem::CV_RAW_MATRIX) != IOMessages::IO_OK)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Pattern image gray 1 could not be pushed to cache ";
		}

		projector_image = projector.GetFeedbackImage();

		cv::imwrite(R"(C:\ProgramData\FARO\Freestyle\Calibrations\Images\projector_image.png)", projector_image);
		// Calculate the Board Position
		auto current_position = 0.0;

		if(dmvs.IsLive())
		{
			current_position = Helper::GetLinearStagePosition(linear_stage);
		}
		else
		{
			std::string current_stag_position_string;
			dmvs.cache->Get(frame_index_sfm, "PlatePosition", current_stag_position_string);
			current_position = std::stold(current_stag_position_string);
		}

		calibration_cache.Put(frame_index_sfm, "PlatePosition", std::to_string(current_position));
		frame_index_sfm++;

		auto interpolation_factor = (current_position - Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START)
			/ (Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_END - Constants::LINEAR_STAGE_PLATE_DETECTION_POSITION_START);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : interpolation_factor " << interpolation_factor;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : current_position " << current_position;

		auto plane_position = plane_position_start - (plane_position_start - plane_position_end) * interpolation_factor;
		auto plane_normal   = plane_normal_end;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Plane Normal " << plane_normal;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Plane Position " << plane_position;

		Eigen::Vector3d eigen_plane_location(plane_position[0], plane_position[1], plane_position[2]);
		Eigen::Vector3d eigen_plane_normal(plane_normal[0], plane_normal[1], plane_normal[2]);

		// calculate plane to origin distance for Hesse Normal Form
		auto distance_hesse = std::fabs(eigen_plane_normal.dot(eigen_plane_location));

#pragma endregion captureAndCountour

#pragma region detectAndFilter2Ddots
		// detect dots in raw image coordinates
		Dots dots0, dots1, dots0_all, dots1_all;

		// For dot brightness, to separate bright from dark dots
		DotInfos dots_infos0, dots_infos1;

		LaserDotDetector2D::Params params;
		params.bg_limit = 10;

		LaserDotDetector2D ldd0(pattern_image0, params);
		LaserDotDetector2D ldd1(pattern_image1, params);
		ldd0.FindLaserDots(dots0, &dots_infos0);
		ldd1.FindLaserDots(dots1, &dots_infos1);
		dots0_all = dots0;
		dots1_all = dots1;

		// Copy to visualization image
		cv::cvtColor(pattern_image0, projector_image0, cv::COLOR_GRAY2BGR);
		cv::cvtColor(pattern_image1, projector_image1, cv::COLOR_GRAY2BGR);
		cv::Mat pattern_image;
		Helper::CreateCollage(pattern_image0, pattern_image1, pattern_image);
		cv::imwrite(R"(C:\ProgramData\FARO\Freestyle\Calibrations\Images\pattern_image1.png)", pattern_image1);
		cv::imwrite(R"(C:\ProgramData\FARO\Freestyle\Calibrations\Images\pattern_image0.png)", pattern_image0);

		if(dots0.empty() && dots1.empty())
		{
			cv::Mat collage;
			cv::putText(projector_image, "NOT ENOUGH DOTS", cv::Point(100, 100), cv::FONT_HERSHEY_SIMPLEX, 2.0, Constants::DARK_BLUE, 3);
			Helper::CreateCollageImage3And1(collage,
			                                cv::Mat::zeros(1, 1, CV_8UC3),
			                                projector_image0,
			                                projector_image1,
			                                projector_image);

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No dots in at least one camera";
			continue;
		}

		Dots dots0_filtered, dots1_filtered;
		DotInfos dots0_filtered_infos, dots1_filtered_infos;

		// if there are more dots visible than bright pattern beams -> we also see dark beams.
		// Then we can try to distinguish both groups by analysis of relative dots brightness
		// and thus identify the bright beams.
		if((projector.GetCalibrationStep() == 1 || projector.GetCalibrationStep() == 2 || projector.GetCalibrationStep() == 3) &&
			(initial_exposure_test_frame < Constants::PROJECTOR_CALIBRATION_INITIAL_EXPOSURE_TEST_FRAMES ||
				dots0_all.size() > Constants::PROJECTOR_CALIBRATION_MIN_NUMBER_OF_DOTS))
		{
			// Get only bright points
			std::vector<size_t> dots0_masked(dots0.size());
			std::vector<size_t> dots1_masked(dots1.size());

			for(auto i = 0; i < dots0.size(); ++i)
			{
				dots0_masked[i] = i;
			}

			for(auto i = 0; i < dots1.size(); ++i)
			{
				dots1_masked[i] = i;
			}

			// Analyze dot intensities and find bright dots
			std::vector<size_t> dots0_idx_bright, dots1_idx_bright;
			auto found_dots0_bright_idx = ProjectorCalibrationHelper::GetBrightLaserDotIndices(dots_infos0, dots0_masked, dots0_idx_bright);
			auto found_dots1_bright_idx = ProjectorCalibrationHelper::GetBrightLaserDotIndices(dots_infos1, dots1_masked, dots1_idx_bright);

			if(found_dots0_bright_idx && found_dots1_bright_idx)
			{
				// All good, use points
				for(auto dots0_idx = 0; dots0_idx < dots0_idx_bright.size(); ++dots0_idx)
				{
					dots0_filtered.push_back(dots0[dots0_idx_bright[dots0_idx]]);
					dots0_filtered_infos.push_back(dots_infos0[dots0_idx_bright[dots0_idx]]);
				}

				for(auto dots1_idx = 0; dots1_idx < dots1_idx_bright.size(); ++dots1_idx)
				{
					dots1_filtered.push_back(dots1[dots1_idx_bright[dots1_idx]]);
					dots1_filtered_infos.push_back(dots_infos1[dots1_idx_bright[dots1_idx]]);
				}
			}
			else
			{
				// Not enough points found or separation of bright/dark points not possible
				//video->update(pImg0);
				continue;
			}
		}
		else
		{
			dots0_filtered       = dots0;
			dots0_filtered_infos = dots_infos0;

			dots1_filtered       = dots1;
			dots1_filtered_infos = dots_infos1;
		}

		// Draw dots to IR frame
		for(size_t i = 0; i < dots0_filtered.size(); ++i)
		{
			circle(projector_image0, cv::Point2f(dots0_filtered[i]), 10, Constants::RED, 1, CV_AA);
		}

		for(size_t i = 0; i < dots1_filtered.size(); ++i)
		{
			circle(projector_image1, cv::Point2f(dots1_filtered[i]), 10, Constants::RED, 1, CV_AA);
		}

		if(dots0_filtered.empty())
		{
			//video->update(pImg0);
			continue;
		}

#pragma endregion detectAndFilter2Ddots

#pragma region calculate3Ddata

		// Calculate intersection Points of IR0 beams with plane detected by color camera
		std::vector<cv::Point3f> intersection_points_ir0_plane;
		cv::imwrite(R"(C:\ProgramData\FARO\Freestyle\Calibrations\Images\debugimage0.png)", projector_image0);

		dmvs.capture_calibration.GetCalibration(CHANNEL_GREY0)->GetPlaneIntersectionPoints(dots0_filtered,
		                                                                                   plane_position,
		                                                                                   plane_normal,
		                                                                                   intersection_points_ir0_plane);

		std::ofstream output_ir0plane;
		auto output_path2 = "D:\\TestData\\output_ir0plane" + std::to_string(int(distance_hesse * 100)) + ".txt";
		output_ir0plane.open(output_path2);

		if(output_ir0plane.is_open())
		{
			for(size_t i = 0; i < intersection_points_ir0_plane.size(); i++)
			{
				output_ir0plane << intersection_points_ir0_plane[i].x << " "
					<< intersection_points_ir0_plane[i].y << " "
					<< intersection_points_ir0_plane[i].z << '\n';
			}

			output_ir0plane.close();
		}


		// calculate the projections of intersection Points into IR1 image and draw them
		std::vector<cv::Point2f> ir0_projected_to_ir1;
		dmvs.capture_calibration.GetCalibration(CHANNEL_GREY1)->ProjectPoints(intersection_points_ir0_plane, ir0_projected_to_ir1);
		auto debug_image = projector_image1.clone();

		for(auto& point : ir0_projected_to_ir1)
		{
			cv::circle(debug_image, point, 5, Constants::BRIGHT_GREEN);
		}

		//cv::imshow("Debug Image", debug_image);
		cv::imwrite(R"(C:\ProgramData\FARO\Freestyle\Calibrations\Images\debug_image.png)", debug_image);

		// Find correspondences by distance of projection of intersection IR0 and IR1 measurements
		Dots correspondences0, correspondences1;

		// Create cv::Mat with projected intersection points for neighborhood search
		pointVec projected_pts_mat(ir0_projected_to_ir1.size());

		for(auto idx = 0; idx < ir0_projected_to_ir1.size(); ++idx)
		{
			point_t temp              = {ir0_projected_to_ir1[idx].x, ir0_projected_to_ir1[idx].y};
			projected_pts_mat.at(idx) = temp;
		}

		if(dots0_filtered.empty() || dots1_filtered.empty())
		{
			//video->update(pImg0);
			continue;
		}

		// Create KD tree for neighborhood search
		//cv::flann::KDTreeIndexParams index_params(1);
		//cv::flann::Index kd_tree(projected_pts_mat, index_params);
		//cv::flann::SearchParams cv_radius_params(-1, 0.0f, false);

		//// run neighborhood search
		//std::vector<float> distances(2);
		//std::vector<int> indices(2);
		point_t q(2);
		auto radius = 7.0f;
		KDTree kd_tree(projected_pts_mat);
		std::vector<Projector::DotInfoType> reconstructed_dot_infos;

		for(size_t dot1_idx = 0; dot1_idx < dots1_filtered.size(); ++dot1_idx)
		{
			q[0] = dots1_filtered[dot1_idx].x;
			q[1] = dots1_filtered[dot1_idx].y;

			auto nearest_neighbor_index = kd_tree.neighborhood_indices(q, radius);

			if(nearest_neighbor_index.size() == 1)
			{
				correspondences0.push_back(dots0_filtered[nearest_neighbor_index[0]]);
				correspondences1.push_back(dots1_filtered[dot1_idx]);

				Projector::DotInfoType dot_info_type{};
				dot_info_type.beam_idx   = -1;
				dot_info_type.brightness = (dots0_filtered_infos[nearest_neighbor_index[0]][4] + dots1_filtered_infos[dot1_idx][4]) / 2.0f;
				dot_info_type.dia        = 1.0;
				reconstructed_dot_infos.push_back(dot_info_type);
			}
			else
			{
				if(!nearest_neighbor_index.empty())
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Found neighbors " << nearest_neighbor_index.size();
			}
		}

		// Reconstruct 3d points from correspondences above
		if(correspondences0.empty())
		{
			// create and update user feedback image
			cv::Mat collage2;
			cv::putText(projector_image,
			            "NO CORRESPONDENCES",
			            cv::Point(100, 100),
			            cv::FONT_HERSHEY_SIMPLEX,
			            2.0,
			            Constants::DARK_BLUE,
			            3);

			Helper::CreateCollageImage3And1(collage2,
			                                cv::Mat::zeros(1, 1, CV_8UC3),
			                                projector_image0,
			                                projector_image1,
			                                projector_image);

			//video->update(collage2);
			continue;
		}

		auto ir0_sensor = dmvs.capture_calibration.GetCalibration(CHANNEL_GREY0);
		auto ir1_sensor = dmvs.capture_calibration.GetCalibration(CHANNEL_GREY1);

		///////////////////////////////////////////////////////////////////////
		// calculate 3d points
		std::vector<cv::Vec3d> reconstructed_3d_points;
		ir0_sensor->StereoIntersect(ir1_sensor, correspondences0, correspondences1, reconstructed_3d_points, nullptr, false);


		// Draw reconstructed 3d point projections in IR Images
		for(size_t idx = 0; idx < reconstructed_3d_points.size(); ++idx)
		{
			cv::circle(projector_image0, cv::Point2f(correspondences0[idx]), 15, Constants::BRIGHT_GREEN);
			cv::circle(projector_image1, cv::Point2f(correspondences1[idx]), 15, Constants::BRIGHT_GREEN);
		}

		cv::imwrite(R"(C:\ProgramData\FARO\Freestyle\Calibrations\Images\projector_image0.png)", projector_image0);
		cv::imwrite(R"(C:\ProgramData\FARO\Freestyle\Calibrations\Images\projector_image1.png)", projector_image1);

		std::vector<cv::Vec3d> reconstructed_3d_point_camera_cs;
		// Bring back the reconstructed points back to left camera coordinate - not required
		for(auto reconstructed_3d_point : reconstructed_3d_points)
		{
			cv::Mat cv_rotation_matrix, cv_translation_vector;
			ir0_sensor->GetExtrinsicParameters(cv_rotation_matrix, cv_translation_vector);
			Eigen::Matrix3d eigen_rotation;
			cv::cv2eigen(cv_rotation_matrix, eigen_rotation);

			const Eigen::Map<Eigen::Vector3d> translation_projection(reinterpret_cast<double*>(cv_translation_vector.data));
			const Eigen::Vector3d p(reconstructed_3d_point[0], reconstructed_3d_point[1], reconstructed_3d_point[2]);
			const Eigen::Vector3d projection_new = eigen_rotation * p + translation_projection;
			reconstructed_3d_point_camera_cs.emplace_back(cv::Vec3d(projection_new[0], projection_new[1], projection_new[2]));
		}

		// Bring back the reconstructed points back to left camera coordinate - not required

		// check distance to detected plane
		std::vector<cv::Point3f> reconstructed_3d_points_near_plane;
		std::vector<Projector::DotInfoType> reconstructed_3d_point_infos_near_plane;

		for(size_t idx = 0; idx < reconstructed_3d_point_camera_cs.size(); ++idx)
		{
			Eigen::Vector3d point(reconstructed_3d_point_camera_cs[idx][0],
			                      reconstructed_3d_point_camera_cs[idx][1],
			                      reconstructed_3d_point_camera_cs[idx][2]);

			// Max distance to board
			auto max_distance_to_board = std::max(distance_hesse * distance_hesse * 0.01, 0.01);
			// Calculate point to plane distance with Hesse Normal Form
			auto point_to_plane_distance = fabs(fabs(eigen_plane_normal.dot(point)) - distance_hesse);

			// Ignore points that are further than 1 cm away from the plane
			if(fabs(point_to_plane_distance) < max_distance_to_board)
			{
				reconstructed_3d_points_near_plane.push_back(cv::Point3f(float(reconstructed_3d_point_camera_cs[idx][0]),
				                                                         float(reconstructed_3d_point_camera_cs[idx][1]),
				                                                         float(reconstructed_3d_point_camera_cs[idx][2])));
				reconstructed_3d_point_infos_near_plane.push_back(reconstructed_dot_infos[idx]);
			}
		}

		/*for(size_t i = 0; i < reconstructed_3d_points_near_plane.size(); i++)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : reconstructed_3d_points_near_plane " << reconstructed_3d_points_near_plane[i].x << " "
				<< reconstructed_3d_points_near_plane[i].y << " "
				<< reconstructed_3d_points_near_plane[i].z;
		}

		if(reconstructed_3d_points_near_plane.empty())
		{
			for(size_t i = 0; i < reconstructed_3d_point_camera_cs.size(); i++)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : reconstructed_3d_point_camera_cs " << reconstructed_3d_point_camera_cs[i][0] << " "
					<< reconstructed_3d_point_camera_cs[i][1] << " "
					<< reconstructed_3d_point_camera_cs[i][2];
			}
		}

		dmvs.Stop();

		if(!dmvs.SetExposureTime(ir_exposure_time_at_1m_ms * distance_hesse))
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Exposure time " << ir_exposure_time_at_1m_ms * distance_hesse << " could not be set";
		}
		dmvs.Start();*/

#pragma endregion calculate3Ddata

		projector.Add3dPoints(reconstructed_3d_points_near_plane, reconstructed_3d_point_infos_near_plane);

		std::ofstream output;
		auto output_path = "D:\\TestData\\output" + std::to_string(int(distance_hesse * 10000)) + ".txt";
		output.open(output_path);

		if(output.is_open())
		{
			for(size_t i = 0; i < reconstructed_3d_point_camera_cs.size(); i++)
			{
				output << reconstructed_3d_point_camera_cs[i][0] << " "
					<< reconstructed_3d_point_camera_cs[i][1] << " "
					<< reconstructed_3d_point_camera_cs[i][2] << '\n';
			}

			output.close();
		}

#pragma region loopControlFlow
		// Update status message
		size_t min_bright_beams = 350;
		auto min_beam_length    = 0.20;

		if(projector.GetCalibrationStep() == 1)
		{
			successful_frames_per_position++;
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Detecting bright points to calculate projector position. Min depth difference = "
					<< min_beam_length << "=." << projector.GetStep1GreenPointsCount() << " " << min_bright_beams << " points found.";

			if(successful_frames_per_position == Constants::FRAMES_PER_POSITION)
			{
				if(dmvs.IsLive())
				{
					Helper::JogForward(linear_stage, Constants::LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION);
				}
				successful_frames_per_position = 0;
			}
		}
		else if(projector.GetCalibrationStep() == 2)
		{
			successful_frames_per_position++;

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Identifying central dot to calculate projector rotation. Min depth  difference = " <<
					min_beam_length << "m. " << projector.GetStep1GreenPointsCount() << " of " << 1125 << "points found.";

			if(successful_frames_per_position == Constants::FRAMES_PER_POSITION)
			{
				if(dmvs.IsLive())
				{
					Helper::JogForward(linear_stage, Constants::LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION);
				}
				successful_frames_per_position = 0;
			}
		}
		else if(projector.GetCalibrationStep() == 3)
		{
			successful_frames_per_position++;

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Matching DOE pattern to dots. Min depth difference = " << min_beam_length << "m. " <<
					projector.GetStep1GreenPointsCount() << " of " << 1125 << " points ";

			if(successful_frames_per_position == Constants::FRAMES_PER_POSITION)
			{
				if(dmvs.IsLive())
				{
					Helper::JogForward(linear_stage, Constants::LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION);
				}
				successful_frames_per_position = 0;
			}
		}
		else if(projector.GetCalibrationStep() == 4)
		{
			if(!initialized_step4)
			{
				if(dmvs.IsLive())
				{
					linear_stage->MoveXAxis(0.2);
					linear_stage->WaitForMovementToFinish();
				}

				initialized_step4 = true;
			}

			successful_frames_per_position++;

			if(successful_frames_per_position == Constants::FRAMES_PER_POSITION)
			{
				if(dmvs.IsLive())
				{
					Helper::JogForward(linear_stage, Constants::LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION);
				}
				successful_frames_per_position = 0;
			}

			if(projector.GetStep4PointsCount() < projector.GetMinDotNumberToFinish())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Pie : " << projector.GetStep4PointsCount() << " of "
						<< projector.GetMinDotNumberToFinish() << " Detecting points. " << projector.GetStep4PointsCount()
						<< " of " << projector.GetMinDotNumberToFinish() << " minimum points found.";
			}
			else
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Pie : " << projector.GetStep4PointsCount() << " of " << 11665
						<< " Detecting all possible points. " << projector.GetStep4PointsCount() << " of " << 11665
						<< " points found. Press big button to save calibration.";

				//if(dmvs.m_use_frame)
				projector.ForceNextCalibrationStep();
				dmvs.stop = true;
			}
		}

		std::shared_ptr<CameraSensor> projector_calibration_result;

		if(!is_projector_calibrated && projector.GetStep4PointsCount() > projector.GetMinDotNumberToFinish())
		{
			is_projector_calibrated = true;
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  Calibration done";
			dmvs.capture_calibration.SetCalibration(CHANNEL_PROJECTOR0, projector_calibration_result);
		}

		// Create and update user feedback image
		cv::Mat collage2;
		Helper::CreateCollageImage3And1(collage2,
		                                cv::Mat::zeros(1, 1, CV_8UC3),
		                                projector_image0,
		                                projector_image1,
		                                projector_image);

		cv::imwrite(R"(C:\ProgramData\FARO\Freestyle\Calibrations\Images\collage2.png)", collage2);
		//video->update(collage2);
		continue;
#pragma endregion loopControlFlow
	} // end of capture loop

	projector.ForceNextCalibrationStep();
	projector.CheckCalibrationStep();

	calibration_cache.Put(0, "nframes", std::to_string(frame_index_sfm + 1));
	calibration_cache.Close();

	if((dmvs.stop && !is_projector_calibrated) || !is_projector_calibrated)
	{
#pragma region calibrationFailedActions
		// Copy data to server into QPRs folder.
		// Write sync log messages in other log file.
		//std::shared_ptr<const Directory> rootPath = fileBase->getRootPath();
		//std::shared_ptr<File> syncLogFile         = File::fromString(*rootPath, String("syncLog.log"));
		//Models::the()->setLogFile(syncLogFile->getFullName());

		//Models::the()->setLogFile(origLogFile);
		//Models::the()->switchLogTo(origLogFileOn);
		//Models::the()->setLoggingLevel(origLogLevel);
#pragma endregion calibrationFailedActions

		return CALIBRATION_STOPPED_BY_USER;
	}

#pragma region createApplyAndSaveCalibration

	// Write back result to current calibration data and save
	std::shared_ptr<CameraSensor> new_projector_calibration;
	projector.GetCalibrationResult(new_projector_calibration);
	dmvs.capture_calibration.SetCalibration(CHANNEL_PROJECTOR0, new_projector_calibration);

	cv::Mat laser_dots = cv::Mat::zeros(4, 11165, CV_64F);
	projector.GetLaserDotMatrix(laser_dots);
	dmvs.capture_calibration.SetCustomMatrix("laser_beams", laser_dots);

	// transform Coordinate system to projector
	cv::Mat rotation_matrix, translation_matrix;
	new_projector_calibration->GetExtrinsicParameters(rotation_matrix, translation_matrix);

	// First we move to the projector coordinate system
	dmvs.capture_calibration.MoveToCoordinateSystemAdjusted(rotation_matrix, translation_matrix);
	dmvs.CalculateNewCoordinateSystem(rotation_matrix, translation_matrix, dmvs.capture_calibration);

	// Move to the final system
	dmvs.capture_calibration.MoveToCoordinateSystemAdjusted(rotation_matrix, translation_matrix);

	// use new calibration immediately within current reconstructor
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Setting calibration for current Scene instance.";

	if(!dmvs.reconstructor)
	{
		dmvs.InitReconstructor();
	}

	dmvs.reconstructor->SetLaserBeamsFromCalibration(laser_dots);
	dmvs.reconstructor->SetCalibrationAndPrepare(dmvs.capture_calibration);

	if(dmvs.IsLive())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Moving to Test position";
		linear_stage->MoveXAxis(0.4);
		linear_stage->WaitForMovementToFinish();
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Preparing Test loop";

		dmvs.TestLoop(false);
	}

	// de-register all commands

	// Save Json file
	std::string calibration_folder;

	if(file_base->GetProjectorCalibrationOutputFolder(calibration_folder) == IOMessages::IO_OK)
	{
		auto projector_calibration_json_file = calibration_folder + "/ProjectorCalibrationInformation.json";
		CalibrationJSONWriter::WriteProjectorCalibrationJSON(projector_calibration_json_file,
		                                                     projector.GetStep4PointsCount() >= projector.GetMinDotNumberToFinish(),
		                                                     projector.GetParaboloidCompensationX(),
		                                                     projector.GetParaboloidCompensationY(),
		                                                     projector.GetStep4PointsCount(),
		                                                     dmvs.serial_number);
	}

	// Save to calibration results folder
	std::string calibration_xml_output_file;
	file_base->GetProjectorCalibrationXMLOutputFile(calibration_xml_output_file);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Saving calibration to " << calibration_xml_output_file;

	if(false/*dmvs.capture_calibration.SaveFile(calibration_xml_output_file)*/)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Saved calibration to " << calibration_xml_output_file;
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not save calibration to " << calibration_xml_output_file;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Save calibrations to cameras folder ?";

	//if(SFMCore::get(m_options, "SaveCalibrations", true))
	//{
	//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Save calibrations: Yes.";

	//	// Save to ProgramData
	//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Saving calibration to cameras folder as " << sensor;
	//	if(dmvs.capture_calibration_.Save(GetCaptureDeviceFile(), quiet ? String::NIL : "Calibration for %ls already exists. Do you want to replace it ?"))
	//		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Saved calibration to " << sensor;
	//	else
	//		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not save calibration to " << sensor;
	//}
	//else
	//{
	//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Save calibrations: No.";
	//}

	// Write calibration onto device?
	//if(!quiet)
	//{
	//	if(SyncMessageBoxUserDecide("Do you wish to save the calibration onto the device?", MB_YESNO | MB_ICONQUESTION) == IDYES)
	//	{
	//		// Save calibration
	//		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Saving calibration to device..";
	//		if(manageCalibration(ManageCalibrationMode::STORE_CALIBRATION, progress))
	//			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration was written to device successfully.";
	//		else
	//			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration could not be written to device!";
	//	}
	//}

	//if(!quiet)
	//{
	//	// Delete the calibration file in cameras folder for TQ check?
	//	if(SyncMessageBoxUserDecide("Do you wish to delete the calibration file from cameras folder?", MB_YESNO | MB_ICONQUESTION) == IDYES)
	//	{
	//		// Now delete calibration file
	//		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Trying to delete calibration xml file in cameras folder..";
	//		auto camerasFolder(Helper::GetDataFolder(CALIBRATIONS_DIRECTORY));

	//		// Create path to store calibrations
	//		auto dir = fs::path(camerasFolder);

	//		// check if directory exists on file system, create if not
	//		if(!exists(dir))
	//		{
	//			if(!create(dir))
	//			{
	//				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot create cameras directory";
	//				return false;
	//			}
	//		}

	//		// Create full path for calibration file
	//		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  try to delete to " << dir->getFullName();
	//		std::shared_ptr<fs::path> file = fs::path(*dir, GetCaptureDeviceFile());

	//		if(file->remove())
	//			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << file->getFullName() << " was removed.";
	//		else
	//			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << file->getFullName() << " was NOT removed.";
	//	}
	//}
#pragma endregion createApplyAndSaveCalibration


	////Models::the()->setLogFile(origLogFile);
	////Models::the()->setLoggingLevel(origLogLevel);
	////Models::the()->switchLogTo(false);

	return int(exit_code);
}

bool IsLedOnInImage(ChannelImageMap channel_image_map,
                    BlobDetector& blob_detector)
{
	ImageChannelCircularMarkerVectorMap circular_markers_current_frame;
	Helper::DetectMarkers(channel_image_map, blob_detector, circular_markers_current_frame);

	return !circular_markers_current_frame.empty();
}

bool PathComparator(fs::path& path1,
                    fs::path& path2)
{
	const std::regex search_regex("[^0-9]*([0-9]+).*");
	const std::string format("$1");

	const auto filename1        = path1.filename().string();
	const auto filename2        = path2.filename().string();
	const auto filename1_regex  = std::regex_replace(filename1, search_regex, format);
	const auto filename2_regex  = std::regex_replace(filename2, search_regex, format);
	const auto filename1_suffix = filename1_regex != filename1 ? std::stoi(filename1_regex) : 0;
	const auto filename2_suffix = filename2_regex != filename2 ? std::stoi(filename2_regex) : 0;

	return filename1_suffix < filename2_suffix;
}

bool EndsWith(const std::string& input,
              const std::string& suffix)
{
	return input.size() >= suffix.size() && input.compare(input.size() - suffix.size(), suffix.size(), suffix) == 0;
}

bool StartsWith(const std::string& input,
                const std::string& prefix)
{
	return input.size() >= prefix.size() && input.compare(0, prefix.size(), prefix) == 0;
}

int ExtractPositionIndex(const std::string& name)
{
	const std::string matches("0123456789");
	const auto index = name.find_first_of(matches);
	std::stringstream int_stream(name.substr(index));
	int position_index;
	int_stream >> position_index;

	return position_index;
}

void LoadRecordedImages(const std::string& images_folder,
                        std::map<size_t, std::vector<cv::Mat>>& images_map)
{
	// Get all the images from the folder
	if(fs::exists(images_folder) && fs::is_directory(images_folder))
	{
		std::vector<fs::path> image_paths;
		copy(fs::directory_iterator(images_folder), fs::directory_iterator(), std::back_inserter(image_paths));

		// sort, since directory iteration is not ordered on some file systems
		std::sort(image_paths.begin(), image_paths.end(), PathComparator);

		for(auto& image_path : image_paths)
		{
			if(image_path.extension() == Constants::FILE_PNG && fs::is_regular_file(image_path))
			{
				auto image = cv::imread(image_path.string(), cv::IMREAD_GRAYSCALE);

				auto name          = image_path.filename().string();
				const auto pos_idx = ExtractPositionIndex(name);

				if(name.find("_Pos_") && name.find("_Frame_"))
				{
					images_map[pos_idx].push_back(image);
				}
			}
		}
	}
}

void LoadRecordedImages(const std::string& images_folder,
                        std::vector<cv::Mat>& led_images,
                        std::vector<cv::Mat>& laser_images)
{
	// Get all the images from the folder
	if(fs::exists(images_folder) && fs::is_directory(images_folder))
	{
		std::vector<fs::path> image_paths;
		copy(fs::directory_iterator(images_folder), fs::directory_iterator(), std::back_inserter(image_paths));

		// sort, since directory iteration is not ordered on some file systems
		std::sort(image_paths.begin(), image_paths.end(), PathComparator);

		for(auto& image_path : image_paths)
		{
			if(image_path.extension() == Constants::FILE_PNG && fs::is_regular_file(image_path))
			{
				auto image = cv::imread(image_path.string(), cv::IMREAD_GRAYSCALE);

				cv::Mat rotated_image;
				cv::rotate(image, rotated_image, cv::ROTATE_180);
				auto name = image_path.filename().string();

				if(EndsWith(name, "_0.png"))
				{
					led_images.push_back(image);
				}
				else
				{
					laser_images.push_back(image);
				}
			}
		}
	}
}

//bool CreateChannelImagesMap(std::string images_folder,
//                            const int size,
//                            std::map<int, std::string>& channel20_images,
//                            std::map<int, std::string>& channel21_images)
//{
//	for(int i = 0; i < size; ++i)
//	{
//		channel20_images[i] = images_folder + "Channel_20_" + std::to_string(i) + ".png";
//		channel21_images[i] = images_folder + "Channel_21_" + std::to_string(i) + ".png";
//	}
//}

void JoinImages(const std::string& images_folder,
                std::vector<cv::Mat>& recorded_joined_images)
{
	// Get all the images from the folder
	if(fs::exists(images_folder) && fs::is_directory(images_folder))
	{
		//std::vector<fs::path> image_paths;
		//copy(fs::directory_iterator(images_folder), fs::directory_iterator(), std::back_inserter(image_paths));

		//// sort, since directory iteration is not ordered on some file systems
		//std::sort(image_paths.begin(), image_paths.end(), Comparator);

		std::vector<cv::String> fn;
		const auto pattern = images_folder + R"(\*.png)";

		cv::glob(pattern, fn, false);

		for(auto i = 0; i < fn.size() / 2; ++i)
		{
			auto filename1 = images_folder + R"(\Channel_21_)" + std::to_string(i) + ".png";
			auto filename2 = images_folder + R"(\Channel_20_)" + std::to_string(i) + ".png";

			if(fs::exists(filename1) && fs::is_regular_file(filename1) && fs::exists(filename2) && fs::is_regular_file(filename2))
			{
				auto channel_20_image = cv::imread(filename1);
				auto channel_21_image = cv::imread(filename2);

				cv::Mat result;
				cv::hconcat(channel_20_image, channel_21_image, result);
				auto filename = images_folder + R"(\merged_)" + std::to_string(i) + ".png";
				cv::imwrite(filename, result);
				recorded_joined_images.push_back(result);
			}
		}
	}
}

void FilterRecordedImages(std::vector<std::shared_ptr<DMVS::CameraCalibration::BoardDetector>>& boards,
                          const BlobDetector& blob_detector,
                          std::vector<cv::Mat>& led_images,
                          std::vector<cv::Mat>& laser_images)
{
	if(led_images.size() != laser_images.size())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << " () : laser Image count is not same as led image count. TAKING NO ACTION";
		return;
	}

	ImageChannelCircularMarkerVectorMap markers, markers_combined;

	//Create this beforehand
	markers[CHANNEL_GREY0] = CircularMarkerVector();
	markers[CHANNEL_GREY1] = CircularMarkerVector();

	std::vector<int> indices;
	// filter images for code stability
	for(auto idx = 0; idx < led_images.size(); idx++)
	{
		auto image = led_images.at(idx);
		ChannelImageMap channel_image_map, channel_image_map_copy;
		channel_image_map.channel_image_map[CHANNEL_GREY0] = image(cv::Rect(0, 0, Constants::DMVS_IMAGE_WIDTH, Constants::DMVS_IMAGE_HEIGHT));
		channel_image_map.channel_image_map[CHANNEL_GREY1] = image(cv::Rect(Constants::DMVS_IMAGE_WIDTH,
		                                                                    0,
		                                                                    Constants::DMVS_IMAGE_WIDTH,
		                                                                    Constants::DMVS_IMAGE_HEIGHT));
		blob_detector.DetectBlob(channel_image_map.channel_image_map[CHANNEL_GREY0], markers[CHANNEL_GREY0]);
		blob_detector.DetectBlob(channel_image_map.channel_image_map[CHANNEL_GREY1], markers[CHANNEL_GREY1]);

		Helper::RemoveLowQualityMarker(markers, channel_image_map);

		std::map<ImageChannel, std::vector<CircularMarkerPair>> unsaturated_markers;
		std::vector<ImageChannelCircularMarkerVectorMap> found_circular_markers_map;
		found_circular_markers_map.push_back(markers);
		RemoveSaturatedMarkers(found_circular_markers_map, unsaturated_markers);
		CombineMarkers(unsaturated_markers, markers_combined);

		auto sufficient_marker_found = true;

		for(auto& marker_pair : markers_combined)
		{
			//for(auto idx1 = 0; idx < board_size; ++idx)
			for(auto board : boards)
			{
				//const auto current_board = board;
				const auto board_id   = board->Detect(marker_pair.second);
				auto detected_markers = board->GetDetectedMarkers();

				if(detected_markers.size() < 7)
				{
					sufficient_marker_found = false;
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << " () : found_coded_markers " << detected_markers.size();
				}
			}
		}

		if(sufficient_marker_found)
		{
			indices.push_back(idx);
		}
	}

	std::vector<cv::Mat> led_images_temp;
	std::vector<cv::Mat> laser_images_temp;

	for(auto index : indices)
	{
		led_images_temp.push_back(led_images.at(index));
		laser_images_temp.push_back(laser_images.at(index));
	}

	led_images.clear();
	laser_images.clear();

	led_images   = led_images_temp;
	laser_images = laser_images_temp;
}

void FilterRecordedImages(const std::vector<cv::Mat>& images,
                          std::vector<cv::Mat>& led_on_images,
                          std::vector<cv::Mat>& laser_on_images)
{
	const BlobDetector::BlobDetectorParams blob_detection_params;
	BlobDetector blob_detector(blob_detection_params);

	for(const auto& image : images)
	{
		// split the image in two parts
		cv::Rect region_image_left(0, 0, 1280, 1024);
		cv::Rect region_image_right(1280, 0, 1280, 1024);
		const auto image_left  = image(region_image_left);
		const auto image_right = image(region_image_right);
		ChannelImageMap channel_image_map;
		channel_image_map.channel_image_map[CHANNEL_GREY0] = image_left;
		channel_image_map.channel_image_map[CHANNEL_GREY1] = image_right;

		// check if it is a Led on image add to led on images
		// else check if it is laser dots image if yes then add to laser dot
		if(IsLedOnInImage(channel_image_map, blob_detector))
			led_on_images.push_back(image);
		else
			laser_on_images.push_back(image);
	}
}

bool IfImageIsSharp(cv::Mat image)
{
	// Methods are available.. this will just act as Wrapper method.
	// shall be implemented if required.
	return false;
}

double ModifiedLaplacian(const cv::Mat& src)
{
	const cv::Mat kernel_x = (cv::Mat_<double>(3, 1) << -1, 2, -1);
	const cv::Mat kernel_y = cv::getGaussianKernel(3, -1, CV_64F);
	cv::Mat filtered_x;
	cv::Mat filtered_y;
	cv::sepFilter2D(src, filtered_x, CV_64F, kernel_x, kernel_y);
	cv::sepFilter2D(src, filtered_y, CV_64F, kernel_y, kernel_x);
	const cv::Mat fm = cv::abs(filtered_x) + cv::abs(filtered_y);

	return cv::mean(fm).val[0];
}

double VarianceOfLaplacian(const cv::Mat& src)
{
	cv::Mat laplacian_image;
	cv::Laplacian(src, laplacian_image, CV_64F);
	cv::Scalar mu, sigma;
	cv::meanStdDev(laplacian_image, mu, sigma);

	return sigma.val[0] * sigma.val[0];
}

double Tenengrad(const cv::Mat& src,
                 const int kernel_size)
{
	cv::Mat sobel_x;
	cv::Mat sobel_y;
	cv::Sobel(src, sobel_x, CV_64F, 1, 0, kernel_size);
	cv::Sobel(src, sobel_y, CV_64F, 0, 1, kernel_size);
	const cv::Mat fm = sobel_x.mul(sobel_x) + sobel_y.mul(sobel_y);

	return cv::mean(fm).val[0];
}

double NormalizedGrayLevelVariance(const cv::Mat& src)
{
	cv::Scalar mu, sigma;
	cv::meanStdDev(src, mu, sigma);

	return (sigma.val[0] * sigma.val[0]) / mu.val[0];
}
