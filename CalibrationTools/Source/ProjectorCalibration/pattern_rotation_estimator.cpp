#include "pattern_rotation_estimator.h"
#include "constants.h"
#include "helper.h"

using namespace Reconstruct;

static bool FitLineAndUpdateRMS(std::vector<cv::Point2f>& points,
                                double& best_rms,
                                cv::Vec4f& l)
{
	if(points.size() < 4)
	{
		return false;
	}

	cv::fitLine(points, l, cv::DIST_L2, 0, 0.01, 0.01);

	// Create Eigen line for distance calculations
	const Eigen::ParametrizedLine<float, 2> line(Eigen::Vector2f(l[2], l[3]), Eigen::Vector2f(l[0], l[1]));

	auto dist = 0.0;

	for(auto& point : points)
	{
		dist += line.squaredDistance(Eigen::Vector2f(point.x, point.y));
	}

	const auto rms = sqrt(dist / double(points.size()));

	if(rms < best_rms)
	{
		best_rms = rms;
		return true;
	}

	return false;
}

static void CVPointsToLine(Eigen::ParametrizedLine<float, 2>& line,
                           const cv::Point2f& start_point,
                           const cv::Point2f& end_point)
{
	// Create line in Eigen format
	Eigen::Vector2f origin, direction;
	origin[0]    = start_point.x;
	origin[1]    = start_point.y;
	direction[0] = end_point.x - start_point.x;
	direction[1] = end_point.y - start_point.y;
	direction.normalize();

	line = Eigen::ParametrizedLine<float, 2>(origin, direction);
}

static double GetRMStoLine(std::vector<cv::Point2f>& points,
                           const cv::Point2f& start_point,
                           const cv::Point2f& end_point)
{
	// Create line in Eigen format
	Eigen::ParametrizedLine<float, 2> line;
	CVPointsToLine(line, start_point, end_point);

	// sum up distances
	auto distances = 0.0;

	for(auto& point : points)
	{
		double d = line.squaredDistance(Eigen::Vector2f(point.x, point.y));
		distances += d * d;
	}

	return sqrt(distances / double(points.size()));
}

PatternRotationDetector::PatternRotationDetector():
	log_file_(nullptr),
	center_(0)
{
}

int PatternRotationDetector::LoadOriginalPattern(const char* file)
{
	if(!file)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Original pattern file does not exist";
		return -1;
	}

	cv::FileStorage fs(file, cv::FileStorage::READ);

	if(!fs.isOpened())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Original pattern file could not be loaded ";
		return -1;
	}

	fs["doe"] >> doe_design_data_;
	if(doe_design_data_.cols != 4)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DOE invalid number of rows [" << doe_design_data_.cols << "] - expected 4 rows ";
		return -1;
	}

	doe_image_coords_bright_.clear();
	doe_image_coords_all_.clear();

	for(auto i = 0; i < doe_design_data_.rows; ++i)
	{
		const auto x = doe_design_data_.at<double>(i, 1);
		const auto y = doe_design_data_.at<double>(i, 2);
		const auto z = doe_design_data_.at<double>(i, 3);

		const auto img_x = float(x / z * 1000.0);

		// DOE is used DOE towards laser,
		const auto img_y = float(-y / z * 1000.0);

		doe_image_coords_all_.emplace_back(img_x, img_y);

		if(doe_design_data_.at<double>(i, 0) > 0.5)
		{
			doe_image_coords_bright_.emplace_back(img_x, img_y);
		}
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Found Bright dots [" << doe_image_coords_bright_.size() << "] Overall dots [" <<
 doe_image_coords_all_.size() << "]";

	const cv::Point2f offset(800, 600);
	cv::Mat img = cv::Mat::zeros(1200, 1600, CV_8U);

	for(int i = 0; i < int(doe_image_coords_all_.size()); ++i)
	{
		const auto radius = doe_design_data_.at<double>(i, 0) > 0.5 ? 4 : 2;
		circle(img, (cv::Point2f(doe_image_coords_all_[i]) + offset) * 0.8f, radius, Constants::DARK_BLUE);
	}

	// get the original pattern beam indices of the cross expanded from the central beam
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Get the original pattern beam indices of the cross expanded from the central beam";
	ExpandCross(cv::Vec2f(0, 0), doe_image_coords_bright_);

	for(auto i = 0; i < 4; ++i)
	{
		doe_urdl_points_[i] = GetExpandedPoints(Direction(i));
	}

	return 0;
}

void PatternRotationDetector::SetPoints(std::vector<cv::Vec2f>& points)
{
	pattern_points_ = points;
}

cv::Mat& PatternRotationDetector::GetAlignedDOE()
{
	return aligned_design_data_;
}

void PatternRotationDetector::CalculateTransformation()
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calculating DOE transformation..";

	// corrected 3d target positions for scale correction with Eigen umeyama
	Eigen::Matrix<double, 2, Eigen::Dynamic> doe_design;
	Eigen::Matrix<double, 2, Eigen::Dynamic> doe_mea;

	int n_pts = 0;
	int mins[4];

	for(auto i = 0; i < 4; ++i)
	{
		//mins[i] = ( m_doeURDLPts[(i+shift)%4].size() < m_doeURDLPts[i].size() ) ?  m_doeURDLPts[(i+shift)%4].size() : m_doeURDLPts[i].size() ;
		mins[i] = (urdl_pts_[i].size() < doe_urdl_points_[i].size()) ? (int)urdl_pts_[i].size() : (int)doe_urdl_points_[i].size();
		n_pts += mins[i];
	}

	doe_design.resize(2, n_pts);
	doe_mea.resize(2, n_pts);

	int count = 0;

	for(int i = 0; i < 4; ++i)
	{
		for(int j = 0; j < mins[i]; ++j)
		{
			doe_design(0, count) = doe_image_coords_bright_[doe_urdl_points_[i][j]][0];
			doe_design(1, count) = doe_image_coords_bright_[doe_urdl_points_[i][j]][1];

			doe_mea(0, count) = pattern_points_[urdl_pts_[i][j]][0];
			doe_mea(1, count) = pattern_points_[urdl_pts_[i][j]][1];
			count++;
		}
	}

	// get optimal trafo between calibration model point clouds including scale correction (true flag)
	Eigen::Matrix<double, -1, -1> transformation = Eigen::umeyama(doe_design, doe_mea, true);
	const auto scale                             = transformation.col(0).norm();

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DOE pattern transformation dimensions: Rows [" << transformation.rows()
											<< "] Cols [" << transformation.cols() << "] Count [" << count << "]";

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DOE pattern scale correction " << scale;

	Eigen::Rotation2Dd rotation(0.0);
	const Eigen::Matrix2d eigen_matrix = transformation.topLeftCorner(2, 2);
	rotation.fromRotationMatrix(eigen_matrix);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DOE pattern rotation correction: " << rotation.angle();

	count    = 0;
	auto rms = 0.0;

	for(auto i = 0; i < 4; ++i)
	{
		for(int j = 0; j < mins[i]; ++j)
		{
			Eigen::Vector3d p(doe_design(0, count), doe_design(1, count), 1.0);
			Eigen::Vector2d p_trafo = transformation.topRightCorner(2, 3) * p;
			Eigen::Vector2d p_mea(doe_mea(0, count), doe_mea(1, count));
			rms += (p_trafo - p_mea).squaredNorm();
			count++;
		}
	}

	doe_design_data_.copyTo(aligned_design_data_);

	for(auto i = 0; i < int(doe_image_coords_all_.size()); ++i)
	{
		Eigen::Vector3d p(doe_image_coords_all_[i][0], doe_image_coords_all_[i][1], 1.0);
		Eigen::Vector2d p_trafo               = transformation.topRightCorner(2, 3) * p;
		aligned_design_data_.at<double>(i, 1) = p_trafo[0];
		aligned_design_data_.at<double>(i, 2) = p_trafo[1];
		aligned_design_data_.at<double>(i, 3) = 1000.0;
	}

	rms = sqrt(rms / (double)count);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DOE Pattern -> Fit measurement to DOE Data RMS : " << rms;
}

int PatternRotationDetector::ExpandCross(const cv::Vec2f center)
{
	return ExpandCross(center, pattern_points_);
}

int IsDoeOrthogonalDirection(const cv::Vec2d direction)
{
	const auto doe_skew_deg = 6;
	const auto tolerance    = 7;

	const auto y = -tan(doe_skew_deg / 180.0 * 3.14159);
	const auto x = -y;

	const cv::Vec2d skew_x(1.0, y);
	const cv::Vec2d skew_y(x, 1.0);

	const auto delta_x = Helper::AngleTo(direction, skew_x);
	const auto ang_x   = delta_x / 3.14159 * 180.0;

	const auto delta_y = Helper::AngleTo(direction, skew_y);
	const auto ang_y   = delta_y / 3.14159 * 180.0;

	if(ang_x < tolerance || ang_x > 180 - tolerance)
	{
		return 1;
	}

	if(ang_y < tolerance || ang_y > 180 - tolerance)
	{
		return 2;
	}

	return 0;
}

std::string GetOrientationString(const int index)
{
	std::string str;

	switch(index)
	{
	case 0: str = "up";
		break;
	case 1: str = "right";
		break;
	case 2: str = "down";
		break;
	case 3: str = "left";
		break;
	default: ;
	}

	return str;
}

int PatternRotationDetector::ExpandCross(cv::Vec2f center,
                                         std::vector<cv::Vec2f> points)
{
	// Create kd tree for neighbor search
	cv::Mat samples(int(points.size()), 2, CV_32F);

	for(auto i = 0; i < points.size(); ++i)
	{
		samples.at<float>(i, 0) = points[i][0];
		samples.at<float>(i, 1) = points[i][1];
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Create kd tree with [" << points.size() << "] points ";

	const cv::flann::KDTreeIndexParams index_params(1);
	cv::flann::Index kd_tree(samples, index_params);
	const cv::flann::SearchParams search_params(-1, 0.0f, false);

	// The radius in which we expect to look for the next neighbor points
	// in each direction brightest dots. Distance between two dots is 27 pixels
	const auto radius = 50.0;

	// find index of start points in origin
	std::vector<float> q(2);
	q[0] = center[0];
	q[1] = center[1];
	std::vector<int> indices;     // (1000);
	std::vector<float> distances; //(1000);

	// find neighbors of central point. Keep in mind, that distances here are squared.
	const auto num_neighbors = kd_tree.radiusSearch(q, indices, distances, radius * radius, 100, search_params);

	// Less than 4 points around central point + central point found
	if(num_neighbors < 5)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : [" << num_neighbors << "] neighbor points found. Should be at least 4 points ";
		return 0;
	}

	const auto start_idx = indices[0];

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cross center at [" << points[start_idx][0] << " "
											<< points[start_idx][1] << "]. Should be near (0,0) within 5 pixels.";

	// index of up right down left in that order
	cv::Vec2f urdl_difference[4];
	cv::Vec2f urdl_points[4];
	int urdl_point_idx[4];

	// set origin seed point as starting point for all directions
	for(auto i = 0; i < 4; ++i)
	{
		urdl_points[i] = cv::Point2f(points[start_idx]);
	}

	// classify 4 nearest neighbors to top, right, down and left neighbor
	for(auto i = 1; i < 5; ++i)
	{
		auto neighbor_point = points[indices[i]];
		auto difference     = neighbor_point - points[start_idx];

		if(!IsDoeOrthogonalDirection(difference))
		{
			continue;
		}

		auto orientation = UNKNOWN;

		if(fabs(difference[0]) > fabs(difference[1]))
		{
			orientation = difference[0] > 0.0 ? RIGHT : LEFT;
		}
		else
		{
			orientation = difference[1] > 0.0 ? UP : DOWN;
		}

		urdl_points[int(orientation)]     = neighbor_point;
		urdl_difference[int(orientation)] = difference;
		urdl_point_idx[int(orientation)]  = indices[i];
	}

	// Check distances, right and left should be similar, upper and lower should be similar
	if(cv::norm(urdl_difference[int(UP)] + urdl_difference[int(DOWN)]) > 4.0 ||
		cv::norm(urdl_difference[int(RIGHT)] + urdl_difference[int(LEFT)]) > 4.0)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cross cannot be expanded, distance from center are huge. Probably no center point exist";

		for(auto i = 0; i < urdl_points->rows; ++i)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : urdl_points[" << i << "] [" << urdl_points[i]
													<< "] urdl_difference[" << i << "] [" << urdl_difference[i]
													<< "] urdl_point_idx[" << i << "] [" << urdl_point_idx[i] << "]";
		}

		return 0;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cross can be expanded in every direction";

	// Expand to every direction
	for(auto i = 0; i < 4; ++i)
	{
		urdl_pts_[i].clear();
		urdl_pts_[i].push_back(urdl_point_idx[i]);

		// end expansion in one direction
		auto expansion_running = true;

		while(expansion_running)
		{
			auto next_point = urdl_points[i] + urdl_difference[i];
			std::vector<float> q(2);
			q[0] = next_point[0];
			q[1] = next_point[1];

			// Search radius sqrt(50)=7 pixels
			const auto neighbors_count = kd_tree.radiusSearch(q, indices, distances, 100.0, 10, search_params);

			//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Found " << neighbors_count << " close neighbor points during expansion";

			if(neighbors_count != 1)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Expansion " << GetOrientationString(i) << " stopped at " << urdl_pts_[i].size();
				expansion_running = false;
			}

			if(indices[0] == urdl_pts_[i].back())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Found last cross entry [" << indices[0] << "]";
				expansion_running = false;
			}

			// add point to current direction expansion vector
			urdl_pts_[i].push_back(indices[0]);

			// update expansion point and step for next point search
			urdl_difference[i] = points[indices[0]] - urdl_points[i];
			urdl_points[i]     = points[indices[0]];
		}
	}

	auto expansion_points_count = 0;
	for(auto& urdl_pt : urdl_pts_)
	{
		expansion_points_count += int(urdl_pt.size());
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Expansion points count [" << expansion_points_count << "]";

	return expansion_points_count;
}

std::vector<int> PatternRotationDetector::GetExpandedPoints(const Direction direction)
{
	return urdl_pts_[int(direction)];
}

cv::Vec4f PatternRotationDetector::GetEndpointsVertical() const
{
	cv::Vec4f end_point_vertical;
	end_point_vertical[0] = vertical_end_points_[0].x;
	end_point_vertical[1] = vertical_end_points_[0].y;
	end_point_vertical[2] = vertical_end_points_[1].x;
	end_point_vertical[3] = vertical_end_points_[1].y;

	return end_point_vertical;
}

cv::Vec4f PatternRotationDetector::GetEndpointsHorizontal() const
{
	cv::Vec4f end_point_horizontal;
	end_point_horizontal[0] = horizontal_end_points_[0].x;
	end_point_horizontal[1] = horizontal_end_points_[0].y;
	end_point_horizontal[2] = horizontal_end_points_[1].x;
	end_point_horizontal[3] = horizontal_end_points_[1].y;

	return end_point_horizontal;
}

cv::Mat PatternRotationDetector::CrossImage() const
{
	return cross_image_;
}

int PatternRotationDetector::GetNumPointsOnEachCrossDirection()
{
	return Constants::NEIGHBOR_POINTS_IN_EACH_DIRECTION;
}

int PatternRotationDetector::GetCenter() const
{
	return center_;

	// 		Eigen::ParametrizedLine< float, 2> lh,lv;
	// 		cvPtsToLine( lh, m_horEndPts[0], m_horEndPts[1]);
	// 		cvPtsToLine( lv, m_vertEndPts[0], m_vertEndPts[1]);
	//
	// 		Eigen::Hyperplane<float,2> hp(lv);
	// 		double intersection = lh.intersection(hp) ;
	// 		Eigen::Vector2f pos = lh.origin() + (float)intersection * lh.direction() ;
	// 		return cv::Vec2f(  pos[0],  pos[1] );
}
