#include "projector_calibration_helper.h"

void ProjectorCalibrationHelper::RemovePointsOutsideContour(std::vector<cv::Vec2f>& pts,
                                                            std::vector<cv::Point>& contour,
                                                            DotInfos& dots_infos)
{
	size_t i = 0;
	while(i < pts.size())
	{
		// remove point if it is outside contour
		cv::Vec2f p(pts[i][0], pts[i][1]);
		if(cv::pointPolygonTest(contour, p, false) < 0.0)
		{
			pts[i]        = pts.back();
			dots_infos[i] = dots_infos.back();
			pts.pop_back();
			dots_infos.pop_back();
		}
			// keep point test next one
		else
		{
			i++;
		}
	}
}

bool ProjectorCalibrationHelper::GetBrightLaserDotIndices(const DotInfos& dots_infos,
                                                          const std::vector<size_t>& dots,
                                                          std::vector<size_t>& bright_dots_indices)
{
	if(dots.size() < 50)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Returning as laser dots count [" << dots.size() << "] <  [ 50 ]";
		return false;
	}

	const auto cols = 1;
	cv::Mat brightness_data(int(dots.size()), cols, CV_32FC1);

	// [4] is background brightness, [0] is maximum brightness
	for(auto i = 0; i < dots.size(); ++i)
	{
		brightness_data.at<float>(i, 0) = dots_infos[dots[i]][4];
	}

	const cv::TermCriteria term_criteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 1000, 0.001);

	cv::Mat labels, centers;
	cv::kmeans(brightness_data, 2, labels, term_criteria, 3, cv::KMEANS_PP_CENTERS, centers);
	const auto bright_cluster = centers.at<float>(1, 0) > centers.at<float>(0, 0) ? 1 : 0;

	for(auto row = 0; row < brightness_data.rows; ++row)
	{
		if(labels.at<int>(row, 0) == bright_cluster)
		{
			bright_dots_indices.push_back(dots[row]);
		}
	}

	return !bright_dots_indices.empty();
}

void ProjectorCalibrationHelper::AveragePositions(std::vector<cv::Point3d>& positions,
                                                  cv::Vec3d& start_position,
                                                  cv::Vec3d& end_position)
{
	cv::Vec6d line_params;
	cv::fitLine(positions, line_params, cv::DIST_HUBER, 0.5, 0.001, 0.001);

	// Now find nearest point on the line for start and end so we can interpolate from there
	const auto line_vector     = cv::Vec3d(line_params[0], line_params[1], line_params[2]);
	const auto line_base_point = cv::Vec3d(line_params[3], line_params[4], line_params[5]);

	const auto diff_vector_start      = start_position - line_base_point;
	const auto length_of_vector_start = diff_vector_start.dot(line_vector);

	// now save to startPoint
	start_position                  = length_of_vector_start * line_vector + line_base_point;
	const auto diff_vector_end      = end_position - line_base_point;
	const auto length_of_vector_end = diff_vector_end.dot(line_vector);

	// now save to startPoint
	end_position = length_of_vector_end * line_vector + line_base_point;
}
