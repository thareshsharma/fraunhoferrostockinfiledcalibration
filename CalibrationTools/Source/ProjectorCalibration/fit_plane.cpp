﻿#include "fit_plane.h"
#include "statistics_value_median_set.h"

LSFitPlane::PlaneParam LSFitPlane::fit(const std::vector<cv::Vec3d>& points,
                                       bool calculateAxisLengths)
{
	// A plane is described by its position and a normal.
	// To fit a plane into a point cloud means to determine thess parameters in a way to
	// minimize the distance of the points to the resulting plane.
	// The quality of the fit is measured by the remaining distances between points and plane.
	// There are several ways to calculate the overall quality:
	// - the sum of the remaining distances (linear)
	// - the sum of the squares of the remaining distances (least squares)

	// The distance between a point and the plane is given by multiplying the normal of
	// the plane with a vector connecting the point with an arbitrary point of the plane.
	// So if the position of the plane is known, its normal can be calculated by a
	// linear equation, for example with SVD - no iteration is required.

	// For a linear fit, the plane has to go through the mean center of the points - so
	// it is easy to calculate the position.
	// Unfortunately, SVD performs a least square fit!

	// It is unclear what the consequences are of combining a position based on
	// a linear fit and a normal based on a least square fit.

	// create matrix A for which Ap=0, p is the vector describing the plane
	int n = static_cast<int>(points.size());

	PlaneParam plane;
	plane.position = cv::Vec3d(0, 0, 0);
	plane.normal   = cv::Vec3d(0, 0, 0);
	plane.ptDrift  = 0;
	plane.ptDist   = 0;

	// insufficient number of points
	if(n < 3)
		return plane;

	// position is the mean value of the points
	for(int i = 0; i < n; ++i)
		plane.position += points.at(i);

	plane.position /= n;

	CvMat* A;
	A = cvCreateMat(n, 3, CV_64FC1);

	for(int j = 0; j < n; ++j)
	{
		const cv::Vec3d& pt = points.at(j);
		cvSetReal2D(A, j, 0, pt[0] - plane.position[0]);
		cvSetReal2D(A, j, 1, pt[1] - plane.position[1]);
		cvSetReal2D(A, j, 2, pt[2] - plane.position[2]);
	}

	// find p using singular value decomposition of A
	// p is the last column of V
	CvMat* V;
	V = cvCreateMat(3, 3, CV_64FC1);

	CvMat* W;
	W = cvCreateMat(n, 3, CV_64FC1);

	cvSVD(A, W, 0, V, CV_SVD_MODIFY_A);

	plane.normal[0] = cvGetReal2D(V, 0, 2);
	plane.normal[1] = cvGetReal2D(V, 1, 2);
	plane.normal[2] = cvGetReal2D(V, 2, 2);

	plane.majorAxis[0] = cvGetReal2D(V, 0, 0);
	plane.majorAxis[1] = cvGetReal2D(V, 1, 0);
	plane.majorAxis[2] = cvGetReal2D(V, 2, 0);

	plane.minorAxis[0] = cvGetReal2D(V, 0, 1);
	plane.minorAxis[1] = cvGetReal2D(V, 1, 1);
	plane.minorAxis[2] = cvGetReal2D(V, 2, 1);

	plane.semiAxesRatio = cvGetReal2D(W, 1, 1) / cvGetReal2D(W, 0, 0);

	if(calculateAxisLengths)
	{
		StatisticValLimitsSet<double> majAxis, minAxis;
		for(auto point : points)
		{
			point -= plane.position;
			majAxis.add(fabs(point.dot(plane.majorAxis)));
			minAxis.add(fabs(point.dot(plane.minorAxis)));
		}

		majAxis.finish();
		minAxis.finish();
		plane.majorAxisLength = majAxis.getValueMatchingToSum(0.8);
		plane.minorAxisLength = minAxis.getValueMatchingToSum(0.8);
	}

	// normalize the normal
	plane.normal = cv::normalize(plane.normal);

	cvReleaseMat(&A);
	cvReleaseMat(&V);
	cvReleaseMat(&W);

	// calculate statistics
	double ptDrift = 0;
	double ptDist  = 0;
	for(int k = 0; k < n; ++k)
	{
		const cv::Vec3d& pt = points.at(k) - plane.position;
		const double d      = pt.dot(plane.normal);
		ptDrift += d;
		ptDist += d * d;
	}
	plane.ptDrift = ptDrift / n;
	plane.ptDist  = sqrt(ptDist / n);

	return plane;
}
