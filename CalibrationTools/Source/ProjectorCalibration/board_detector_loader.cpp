#include "board_detector_loader.h"
#include "helper.h"

BoardDetectorLoader::BoardDetectorLoader(const bool transform_on_load):
	board_id_(-1),
	origin_id_(-1),
	x_id_(-1),
	y_id_(-1),
	transform_on_load_(transform_on_load)
{
}

BoardDetectorLoader::~BoardDetectorLoader() noexcept
= default;

bool BoardDetectorLoader::ReadFile(const std::string& file,
                                   int uncoded_marker_start_id)
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Try to open board coordinate file " << file;
	cv::FileStorage fs(file, cv::FileStorage::READ);

	if(!fs.isOpened())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot open board coordinate file " << file;
		return false;
	}

	// Load and set the calibration wall plane contour coordinates
	// Try to load exact board coordinates as calculated by PhotoModeler with high end camera images
	// Load and set the calibration wall plane coded marker coordinates
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Load coded marker coordinates ";
	cv::Mat coded_marker_coordinates;
	fs["codedMarkerCoordinates"] >> coded_marker_coordinates;
	//fs["projectorPlanePts"] >> coded_marker_coordinates;

	if(coded_marker_coordinates.rows < 1)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot open coded marker coordinates matrix from " << file;
		return false;
	}

	// set the original board points from the file
	for(auto row = 0; row < coded_marker_coordinates.rows; ++row)
	{
		cv::Point3f pos(float(coded_marker_coordinates.at<double>(row, 1)),
		                float(coded_marker_coordinates.at<double>(row, 2)),
		                float(coded_marker_coordinates.at<double>(row, 3)));

		// first column contains coded marker id
		coded_markers_[int(coded_marker_coordinates.at<double>(row, 0))] = pos;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Loading additional marker coordinates ";
	cv::Mat additional_markers_coordinates;
	fs["additionalMarkerCoordinates"] >> additional_markers_coordinates;

	if(additional_markers_coordinates.rows < 1)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot find any additional marker coordinates matrix from " << file;
		//return -1;
	}

	for(auto row = 0; row < additional_markers_coordinates.rows; ++row)
	{
		cv::Point3d pos(additional_markers_coordinates.at<double>(row, 0),
		                additional_markers_coordinates.at<double>(row, 1),
		                additional_markers_coordinates.at<double>(row, 2));

		auto id = uncoded_marker_start_id + row;

		uncoded_markers_[id] = pos;
	}

	if(transform_on_load_)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Loading coordinate system definition ";
		cv::Mat coordinate_system_definition;
		fs["coordinateSystemDefinition"] >> coordinate_system_definition;

		if(coordinate_system_definition.rows < 1)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot find any coordinate system definition matrix from " << file;
			return false;
		}

		if(coordinate_system_definition.rows == 3)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Coordinate system definition does not have 3 rows";
			origin_id_ = coordinate_system_definition.at<int>(0, 0);
			x_id_      = coordinate_system_definition.at<int>(1, 0);
			y_id_      = coordinate_system_definition.at<int>(2, 0);
		}
		else
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Updating origin XZ ";
			UpdateOXZ();
		}

		RedefineCoordinateSystemOXY();
	}

	// get board ID from board ID node if available, otherwise extract from filename
	cv::FileNode board_id = fs["boardID"];

	if(board_id.isInt())
	{
		board_id >> board_id_;
	}
	else
	{
		const auto length   = file.length();
		auto number_sub_str = file.substr(length - 12, length - 4);
		auto id             = std::stoi(number_sub_str);

		if(id > 0)
		{
			board_id_ = id;
		}
	}

	//load plane contour
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Loading projector plane contour ";
	cv::Mat projector_plane_contour;
	fs["projectorPlaneContour"] >> projector_plane_contour;

	if(projector_plane_contour.rows < 1)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot open projector plane contour matrix from " << file;
		return false;
	}

	// contour
	std::vector<cv::Vec3f> plane_contour;
	for(auto row = 0; row < projector_plane_contour.rows; ++row)
	{
		cv::Vec3f pos(float(projector_plane_contour.at<double>(row, 0)),
		              float(projector_plane_contour.at<double>(row, 1)),
		              float(projector_plane_contour.at<double>(row, 2)));
		plane_contour.push_back(pos);
	}

	plane_contour_ = plane_contour;

	return true;
}

ProjectorBoardDetector BoardDetectorLoader::CreateDetector(const std::shared_ptr<const CameraSensor>& calibration,
                                                           const bool detect_only_markers) const
{
	ProjectorBoardDetector detector_out(calibration,
	                                    detect_only_markers,
	                                    board_id_,
	                                    origin_id_,
	                                    x_id_,
	                                    y_id_,
	                                    coded_markers_,
	                                    uncoded_markers_);
	detector_out.SetPlaneContour(plane_contour_);
	return detector_out;
}

void BoardDetectorLoader::RedefineCoordinateSystemOXY()
{
	const auto origin = Eigen::Vector3d(coded_markers_[origin_id_].x, coded_markers_[origin_id_].y, coded_markers_[origin_id_].z);
	const auto x      = Eigen::Vector3d(coded_markers_[x_id_].x, coded_markers_[x_id_].y, coded_markers_[x_id_].z);
	const auto y      = Eigen::Vector3d(coded_markers_[y_id_].x, coded_markers_[y_id_].y, coded_markers_[y_id_].z);

	// create the rotation matrix
	Eigen::Matrix3d rotation_matrix;
	rotation_matrix.col(0) = (x - origin).normalized();
	rotation_matrix.col(1) = (y - origin).normalized();
	rotation_matrix.col(2) = rotation_matrix.col(0).cross(rotation_matrix.col(1)).normalized();
	rotation_matrix.col(1) = rotation_matrix.col(2).cross(rotation_matrix.col(0)).normalized();

	// apply transformation to all markers
	for(auto it = coded_markers_.begin(); it != coded_markers_.end(); ++it)
	{
		Eigen::Vector3d point(it->second.x, it->second.y, it->second.z);
		Eigen::Vector3d point_new = rotation_matrix.inverse() * (point - origin);
		it->second.x              = float(point_new[0]);
		it->second.y              = float(point_new[1]);
		it->second.z              = float(point_new[2]);
		/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Point New [" << it->first << "] " << point_new[0] << " " << point_new[1] << " " <<
 point_new[2];*/
	}

	for(auto it = uncoded_markers_.begin(); it != uncoded_markers_.end(); ++it)
	{
		Eigen::Vector3d point(it->second.x, it->second.y, it->second.z);
		Eigen::Vector3d point_new = rotation_matrix.inverse() * (point - origin);
		it->second.x              = float(point_new[0]);
		it->second.y              = float(point_new[1]);
		it->second.z              = float(point_new[2]);
		/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Uncoded Marker Id [" << it->first << "] " << point_new[0] << " " << point_new[1] << " " <<
 point_new[2];*/
	}
}

void BoardDetectorLoader::UpdateOXZ()
{
	std::vector<std::pair<int, int>> farthest_away_pairs;
	do
	{
		auto distance = 0.;
		std::pair<int, int> best_pair(0, 0);
		for(const auto& coded_marker_pair : coded_markers_)
		{
			for(const auto& coded_marker_pair_clone : coded_markers_)
			{
				const std::pair<int, int> current_pair1(coded_marker_pair.first, coded_marker_pair_clone.first);
				const std::pair<int, int> current_pair2(coded_marker_pair_clone.first, coded_marker_pair.first);
				const auto current_distance = cv::norm(coded_marker_pair.second - coded_marker_pair_clone.second);

				if(current_distance > distance &&
					std::find(farthest_away_pairs.begin(), farthest_away_pairs.end(), current_pair1) == farthest_away_pairs.end() &&
					std::find(farthest_away_pairs.begin(), farthest_away_pairs.end(), current_pair2) == farthest_away_pairs.end())
				{
					best_pair = std::pair<int, int>(coded_marker_pair.first, coded_marker_pair_clone.first);
					distance  = current_distance;
				}
			}
		}

		farthest_away_pairs.push_back(best_pair);
	}
	while(farthest_away_pairs.size() < 2);

	origin_id_ = farthest_away_pairs.front().first;
	{
		const auto id1 = farthest_away_pairs.back().first;
		const auto id2 = farthest_away_pairs.back().second;

		//take the longer distance as x direction
		if(cv::norm(coded_markers_[origin_id_] - coded_markers_[id1]) > cv::norm(coded_markers_[origin_id_] - coded_markers_[id2]))
		{
			x_id_ = id1;
			y_id_ = id2;
		}
		else
		{
			x_id_ = id2;
			y_id_ = id1;
		}
	}
}
