#include "projector_board_detector.h"

#include <utility>
#include "blob_detector.h"
#include "ring_code_decoder.h"
#include "ellipse_detection_distortion_correction.h"
#include "math_helper.h"
#include "ellipse_optimizer.h"
#include "coded_ring_center_detector.h"
#include "helper.h"

namespace Reconstruct
{
ProjectorBoardDetector::ProjectorBoardDetector(std::shared_ptr<const CameraSensor> calibration,
                                               const bool detect_only_markers) : calibration_(std::move(calibration)),
                                                                                 origin_id_(-1),
                                                                                 x_id_(-1),
                                                                                 y_id_(-1),
                                                                                 board_id_(-1),
                                                                                 num_unknown_marker_(0),
                                                                                 min_grey_(0),
                                                                                 max_grey_(250),
                                                                                 grey_step_(10),
                                                                                 detected_(false),
                                                                                 detect_only_markers_(detect_only_markers),
                                                                                 on_site_calibration_mode_(true)
{
	BlobDetector::BlobDetectorParams params;
	min_grey_  = params.min_threshold;
	max_grey_  = params.max_threshold;
	grey_step_ = params.threshold_step;
}

ProjectorBoardDetector::ProjectorBoardDetector(const ProjectorBoardDetector& in) :
	rotation_vector_(in.rotation_vector_.clone()),
	translation_vector_(in.translation_vector_.clone()),
	calibration_(in.calibration_),
	image_(in.image_.clone()),
	position_(in.position_),
	normal_(in.normal_),
	coded_markers_(in.coded_markers_),
	uncoded_markers_(in.uncoded_markers_),
	detected_board_markers_(in.detected_board_markers_),
	plane_contour_(in.plane_contour_),
	current_contour_points_(in.current_contour_points_),
	origin_id_(in.origin_id_),
	x_id_(in.x_id_),
	y_id_(in.y_id_),
	board_id_(in.board_id_),
	num_unknown_marker_(in.num_unknown_marker_),
	min_grey_(in.min_grey_),
	max_grey_(in.max_grey_),
	grey_step_(in.grey_step_),
	detected_(in.detected_),
	detect_only_markers_(in.detect_only_markers_),
	on_site_calibration_mode_(in.on_site_calibration_mode_)
{
}

ProjectorBoardDetector::ProjectorBoardDetector(const std::shared_ptr<const CameraSensor> calibration,
                                               const bool detect_only_markers_in,
                                               const int board_id,
                                               const int origin_id,
                                               const int x_id,
                                               const int y_id,
                                               const std::map<int, cv::Point3f>& coded_markers,
                                               const std::map<int, cv::Point3f>& uncoded_markers) :
	calibration_(calibration),
	coded_markers_(coded_markers),
	uncoded_markers_(uncoded_markers),
	origin_id_(origin_id),
	x_id_(x_id),
	y_id_(y_id),
	board_id_(board_id),
	num_unknown_marker_(0),
	min_grey_(0),
	max_grey_(250),
	grey_step_(10),
	detected_(false),
	detect_only_markers_(detect_only_markers_in),
	on_site_calibration_mode_(true)
{
	const BlobDetector::BlobDetectorParams params;
	min_grey_  = params.min_threshold;
	max_grey_  = params.max_threshold;
	grey_step_ = params.threshold_step;
}

int ProjectorBoardDetector::GetBoardId() const
{
	return board_id_;
}

ProjectorBoardDetector& ProjectorBoardDetector::operator =(const ProjectorBoardDetector& rhs)
{
	min_grey_                 = rhs.min_grey_;
	max_grey_                 = rhs.max_grey_;
	grey_step_                = rhs.grey_step_;
	detect_only_markers_      = rhs.detect_only_markers_;
	on_site_calibration_mode_ = rhs.on_site_calibration_mode_;
	calibration_              = rhs.calibration_;
	origin_id_                = rhs.origin_id_;
	y_id_                     = rhs.y_id_;
	x_id_                     = rhs.x_id_;
	num_unknown_marker_       = rhs.num_unknown_marker_;
	board_id_                 = rhs.board_id_;
	detected_                 = rhs.detected_;
	rotation_vector_          = rhs.rotation_vector_.clone();
	translation_vector_       = rhs.translation_vector_.clone();
	image_                    = rhs.image_.clone();
	position_                 = rhs.position_;
	normal_                   = rhs.normal_;
	coded_markers_            = rhs.coded_markers_;
	uncoded_markers_          = rhs.uncoded_markers_;
	detected_board_markers_         = rhs.detected_board_markers_;
	plane_contour_            = rhs.plane_contour_;
	current_contour_points_   = rhs.current_contour_points_;

	return *this;
}

int ProjectorBoardDetector::NumUnknownMarkers() const
{
	return num_unknown_marker_;
}

void ProjectorBoardDetector::SetImage(const cv::Mat& image)
{
	if(image.channels() == 3)
	{
		if(image.depth() == CV_8U)
		{
			image_ = cv::Mat::zeros(image.size(), CV_8U);

			for(auto row = 0; row < image.rows; ++row)
			{
				for(auto col = 0; col < image.cols; ++col)
				{
					image_.at<uchar>(row, col) = image.at<cv::Vec3b>(row, col)[1];
				}
			}
		}
	}
	else
	{
		image.copyTo(image_);
	}

	cv::normalize(image_, image_, 0, 0xFF, cv::NORM_MINMAX, -1);
	cv::Mat bilateral_filtered;
	cv::bilateralFilter(image_, bilateral_filtered, 3, 40.0, 40.0);
	image_ = bilateral_filtered;
}

void ProjectorBoardDetector::SetPlaneContour(const std::vector<cv::Vec3f>& plane_contour)
{
	plane_contour_ = plane_contour;
}

int ProjectorBoardDetector::Detect(const cv::Mat& image,
                                   bool use_outer_circles,
                                   const bool refine_with_theoretical_centers,
                                   bool if_mirrored_image,
                                   const int min_grey_in,
                                   const int max_grey_in,
                                   const int grey_step_in)
{
	SetImage(image);
	detected_board_markers_.clear();
	num_unknown_marker_ = 0;
	BlobDetector::BlobDetectorParams params;
	params.refine_ellipses = 1;

	// For on-site calibration we are looking for coded markers only
	// Use area limits taking care for this. We will use the board detection
	// for calibration of other sensors too. Therefore take care for the
	// image size.
	if(on_site_calibration_mode_)
	{
		params.min_area          = image_.size().area() > 640 * 480 ? 50.0f : 15.0f;
		params.max_area          = image_.size().area() > 640 * 480 ? 10000.0f : 2500.0f;
		params.min_repeatability = 1;
		params.min_threshold     = 0;
		params.max_threshold     = 1;
	}
	else
	{
		params.min_area = 10.0f;
		params.max_area = 10000.0f;
		params.min_repeatability = 2;
		params.min_threshold     = min_grey_in != 0 ? min_grey_in : min_grey_;
		params.max_threshold     = max_grey_in != 0 ? max_grey_in : max_grey_;
		params.threshold_step    = grey_step_in != 0 ? grey_step_in : grey_step_;
	}

	BlobDetector blob_detector(params);

	// detect all circular blobs (as ellipses)
	std::vector<CircularMarker> circular_markers;
	blob_detector.DetectBlob(image_, circular_markers);

	// RingCode decoder for coded ID search outer ring scale 6.0, 12bit codes
	RingCodeDecoder ring_decoder(image_, 6.0f, 12, if_mirrored_image);

	// Used for center refinement by outer ring
	// -> can be integrated to ring code decoder
	CodedRingCenterDetector coded_ring_detector(image_);
	StatisticValSet<double> stats_min, stats_max;

	// #pragma omp parallel for
	for(auto& circular_marker : circular_markers)
	{
		// try to decode ellipse as coded ring marker
		double outer_ring_scale, angle, middle_outer_ring_scale;
		auto marker_id = ring_decoder.GetMarkerId(circular_marker.raw_ellipse, outer_ring_scale, middle_outer_ring_scale, angle);

		// check if marker is detected as coded ring marker
		if(marker_id < 0)
		{
			circular_marker.inner_radius_scale = 1.0;
			circular_marker.outer_radius_scale = 1.0;
			circular_marker.angle              = 0.0;

			//in case of no uncoded markers, there is no need to assign unassigned markers later on.
			if(uncoded_markers_.empty())
			{
				continue;
			}
		}
		else
		{
			//may be an uncoded marker
			circular_marker.outer_radius_scale = outer_ring_scale;
			circular_marker.angle              = angle;
			circular_marker.inner_radius_scale = 1.0;

			// coded ring marker start with id 1000
			marker_id += 1000;
			circular_marker.id = marker_id;

			// skip marker that are not part of the board
			if(coded_markers_.find(marker_id) == coded_markers_.end())
			{
				num_unknown_marker_++;
				continue;
			}
		}

		if(!(circular_marker.confidence > 0.0))
		{
			continue;
		}

		bool add       = true;
		double min_val = -1;
		double max_val = -1;

		//#pragma omp critical
		if(add)
		{
			if(marker_id < 0)
			{
				detected_markers_without_id_.push_back(circular_marker);
			}
			else
			{
				detected_board_markers_[marker_id] = circular_marker;
			}

			if(min_val >= 0.0 && max_val != 0.0)
			{
				stats_min.add(min_val);
				stats_max.add(max_val);
			}
		}
	}

	if(detect_only_markers_)
	{
		return int(detected_board_markers_.size());
	}

	AssignIDsToDetectedMarkers();
	ClearDetectedMarkersWithoutID();

	//set new grey thresholds for ellipse detection
	{
		stats_min.finish();
		stats_max.finish();
		BlobDetector::BlobDetectorParams params_2;
		if(detected_board_markers_.size() > 5 && stats_max.minVal - stats_min.maxVal > params_2.threshold_step)
		{
			min_grey_  = (stats_max.minVal - stats_min.maxVal) / 4 + stats_min.maxVal;
			max_grey_  = stats_max.maxVal;
			grey_step_ = (stats_max.minVal - stats_min.maxVal) / 4;
		}
		else
		{
			//no ellipses found, or min max values too close at each other -> reset to start value
			min_grey_  = params_2.min_threshold;
			max_grey_  = params_2.max_threshold;
			grey_step_ = params_2.threshold_step;
		}
	}

	// Apply ellipse correction to detected ellipses based on known size and position
	if(!CalculateOrientation())
	{
		return -1;
	}

	//correct distortion and perspective distortion
	auto corrector = std::make_shared<EllipseDetectionDistortionCorrection>(rotation_vector_, translation_vector_, calibration_);
	std::vector<EllipseDetectionDistortionCorrection::Circle> model;

	// get Z=1 normed diameters of circles only from 2d ellipses
	std::vector<cv::RotatedRect> ellipses;
	std::vector<size_t> used_ids;

	for(const auto& marker_pair : detected_board_markers_)
	{
		ellipses.push_back(marker_pair.second.raw_ellipse);
		used_ids.push_back(marker_pair.first);
	}

	auto diameters = corrector->GetNormedRealWorldDiameters(ellipses);

	// scale normed diameters to real world diameters by IR system measured distance
	// later we retry detection of undetected marker if we know the expected position
	// we need to know the radius of the inner circle which is given by the mean radius of the detected markers
	auto mean_inner_circle_diameter = 0.0;

	for(size_t idx= 0; idx < used_ids.size(); ++idx)
	{
		// scale the Z=1 normalized diameter to real world diameter
		diameters[idx] *= FrameCoordinates(used_ids[idx])[2];
		mean_inner_circle_diameter += diameters[idx] * detected_board_markers_[used_ids[idx]].inner_radius_scale;
		EllipseDetectionDistortionCorrection::Circle circle;
		GetCodedMarker(used_ids[idx], circle.center);

		// this should be the normal of the plane fit to the board markers ?!
		circle.normal = cv::Vec3d(0., 0., 1.0);
		circle.radius = float(diameters[idx] * 0.5);
		model.push_back(circle);
	}

	mean_inner_circle_diameter /= double(used_ids.size());

	// calculate the corrections based on board detection normal and known circle size
	auto corrections = corrector->GetCorrections(model, nullptr, nullptr);

	for(size_t i = 0; i < used_ids.size(); ++i)
	{
		detected_board_markers_[used_ids[i]].center_correction = cv::Point2f(corrections[i][0], corrections[i][1]);
		// In this case it is not required, when everything works you can come and waste your time here.
		//detected_markers_[used_ids[i]].center_correction = cv::Point2f(0, 0);
	}

	// recalculate camera orientation to board with new marker coordinates
	if(!CalculateOrientation())
		return -1;

	// do a refinement with all ellipses where the circles should have been detected.
	//VGDebug(L" try to find additional Board marker at known positions with diameter %f",  meanInnerCircleDiameter * 0.5 );

	// We retry detection of undetected marker for which we know the expected position now
	if(refine_with_theoretical_centers)
	{
		EllipseFitOptimizer gradient_optimizer(image_);
		auto ellipse_detection_distortion_correction = std::make_shared<EllipseDetectionDistortionCorrection>(
			rotation_vector_,
			translation_vector_,
			calibration_);
		std::vector<EllipseDetectionDistortionCorrection::Circle> circles;
		std::vector<int> circle_indices;

		for(const auto& coded_marker_pair : coded_markers_)
		{
			//did we already find this marker?
			if(detected_board_markers_.find(coded_marker_pair.first) != detected_board_markers_.end())
				continue;

			EllipseDetectionDistortionCorrection::Circle circle;
			circle.center = cv::Vec3d(coded_marker_pair.second.x, coded_marker_pair.second.y, coded_marker_pair.second.z);
			// this should be the normal of the plane fit to the board markers ?!
			circle.normal = cv::Vec3d(0., 0., 1.0);
			circle.radius = float(mean_inner_circle_diameter * 0.5);
			circles.push_back(circle);
			circle_indices.push_back(coded_marker_pair.first);
		}

		// create the expected ellipse based on board position and known marker position/size
		std::vector<cv::RotatedRect> theoretical_rects;
		ellipse_detection_distortion_correction->GetCorrections(circles, 0, &theoretical_rects);

		// #pragma omp parallel for
		for(size_t i = 0; i < circle_indices.size(); ++i)
		{
			// try to decode the marker at the theoretically expected position
			CircularMarker center;
			center.raw_ellipse = gradient_optimizer.optimize(theoretical_rects[i]);
			if(!gradient_optimizer.success())
			{
				continue;
			}

			// try to decode ring code
			double outer_ring_scale, angle, middle_outer_ring_scale;
			auto marker_id = ring_decoder.GetMarkerId(center.raw_ellipse, outer_ring_scale, angle, middle_outer_ring_scale);

			// check if marker is detected as coded ring marker
			if(marker_id < 0)
			{
				continue;
			}

			center.outer_radius_scale = outer_ring_scale;
			center.angle              = angle;
			center.confidence         = 1.0;

			marker_id += 1000;
			center.id = marker_id;

			//test whether we really detected the right circle
			if(marker_id != circle_indices[i])
			{
				continue;
			}

			bool add = true;

			// circle to apply lens correction to ellipse position and normal equal for inner and outer ellipse
			EllipseDetectionDistortionCorrection::Circle circle;
			GetCodedMarker(marker_id, circle.center);

			circle.normal = cv::Vec3d(0., 0., 1.0); // this should be the normal of the plane fit to the board markers ?!
			{
				if(params.refine_ellipses)
				{
					center.outer_radius_scale = 1.0;
					center.confidence         = 1.0;
				}

				// update the circle diameter for lens correction of ellipse of inner circle
				auto diameters_in = ellipse_detection_distortion_correction->GetNormedRealWorldDiameters(
					std::vector<cv::RotatedRect>(1, center.raw_ellipse));
				circle.radius = float(diameters_in[0] * FrameCoordinates(int(marker_id))[2] * 0.5);
			}

			// #pragma omp critical
			if(add)
			{
				// calculate lens correction for ellipse
				auto distortion_corrections = ellipse_detection_distortion_correction->GetCorrections(
					std::vector<EllipseDetectionDistortionCorrection::Circle>(1, circle));
				center.center_correction = cv::Point2f(distortion_corrections[i]);/* [0] , distortion_corrections[i][1]);*/
				detected_board_markers_[marker_id] = center;
			}
		}

		// recalculate camera orientation to board with new marker coordinates
		if(!CalculateOrientation())
			return -1;
	}

	// Calculate the plane parameters in Freestyle coordinates from the plane parameters
	// in board coordinates and the transformation between Freestyle coordinate system and
	// board coordinate system (rvec, tvec)
	{
		cv::Mat translation_vector, rotation_vector;
		auto success = GetOrientation(rotation_vector, translation_vector);

		cv::Mat m(3, 3, CV_64F);
		cv::Rodrigues(rotation_vector, m, cv::noArray());
		Eigen::Map<Eigen::Matrix3d> rotation((double*)m.data);
		Eigen::Map<Eigen::Vector3d> loc_pattern((double*)translation_vector.data);

		// Translate the plane contour points with current pose
		current_contour_points_.clear();

		for(size_t idx = 0; idx < plane_contour_.size(); ++idx)
		{
			Eigen::Vector3d p(plane_contour_[idx][0], plane_contour_[idx][1], plane_contour_[idx][2]);
			Eigen::Vector3d p_new = rotation.inverse() * p + loc_pattern;
			current_contour_points_.emplace_back(cv::Point3f(float(p_new[0]), float(p_new[1]), float(p_new[2])));
		}
	}

	return int(detected_board_markers_.size());
}

int ProjectorBoardDetector::FitMarker()
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	return -1;
}

void ProjectorBoardDetector::GetBoardMarkerMap(std::map<size_t, CircularMarker>& markers) const
{
	markers = detected_board_markers_;
}

std::vector<CircularMarker> ProjectorBoardDetector::GetDetectedMarkersWithoutID() const
{
	return detected_markers_without_id_;
}

void ProjectorBoardDetector::AddCodedMarker(const int marker_id,
                                            const cv::Point3f& marker_position)
{
	coded_markers_[marker_id] = marker_position;
}

void ProjectorBoardDetector::AddUncodedMarker(const int marker_id,
                                              const cv::Point3f marker_position)
{
	uncoded_markers_[marker_id] = marker_position;
}

bool ProjectorBoardDetector::GetCodedMarker(const size_t id,
                                            cv::Vec3d& vector3d) const
{
	auto outcome        = false;
	const auto iterator = coded_markers_.find(id);

	if(iterator != coded_markers_.end())
	{
		vector3d = cv::Vec3d(iterator->second.x, iterator->second.y, iterator->second.z);
		outcome  = true;
	}

	return outcome;
}

bool ProjectorBoardDetector::GetMarker(const int id,
                                       cv::Vec3d& vec) const
{
	bool outcome              = false;
	auto all_marker_positions = GetAllMarkerCoordinates();
	const auto iterator       = all_marker_positions.find(id);

	if(iterator != all_marker_positions.end())
	{
		vec     = cv::Vec3d(iterator->second.x, iterator->second.y, iterator->second.z);
		outcome = true;
	}

	return outcome;
}

double ProjectorBoardDetector::DetectColorPlane(cv::Vec3d& position,
                                                cv::Vec3d& normal)
{
	///* fit a plane through all board marker on the plate because the points are
	// measured with Photo Modeler and not exactly on a plane*/
	//std::vector<cv::Vec3d> planePts;
	//std::map<int, cv::Point3f>::iterator it;
	//for(it = m_codedMarkers.begin(); it != m_codedMarkers.end(); ++it)
	//{
	//	cv::Point3f p = it->second;
	//	planePts.push_back(cv::Vec3d(p.x, p.y, p.z));
	//}

	//// Fit plane to IR depth 3d points calc distance of Freestyle (0,0,0) to plane
	//std::shared_ptr<ScanPtListIterator> ptsIt              = new ScanPtListIterator(planePts);
	//std::shared_ptr<ExtractPlaneParam> planeFitCodedMarker = std::make_shared<ExtractPlaneParam>();
	//planeFitCodedMarker->calcPlaneParam(ExtractPlaneParam::CalcRemoveOutliers);
	//cv::Vec3d planeNormal   = planeFitCodedMarker->getNormal();
	//cv::Vec3d planePosition = planeFitCodedMarker->getPosition();
	//cv::Mat tvec, rvec;
	//bool success = getOrientation(rvec, tvec);

	//// Parameters of the plane in board coordinates from plane fit to marker coordinates
	//Eigen::Vector3d normalVec(planeNormal[0], planeNormal[1], planeNormal[2]);
	//Eigen::Vector3d posVec(planePosition[0], planePosition[1], planePosition[2]);

	//// Calculate the plane parameters in Freestyle coordinates from the plane parameters
	//// in board coordinates and the transformation between Freestyle coordinate sytem and
	//// board coordinate system (rvec, tvec)
	//cv::Mat m(3, 3, CV_64F);
	//cv::Rodrigues(rvec, m, cv::noArray());
	//Eigen::Map<Eigen::Matrix3d> rot((double*)m.data);
	//Eigen::Map<Eigen::Vector3d> locPattern((double*)tvec.data);

	//// plane orientation, normal points toward Freestyle device ( negative Z )
	//Eigen::Vector3d newNormal = rot.inverse() * normalVec;
	//if(newNormal[2] > 0)
	//	newNormal *= -1.0;

	//// plane position in Freestyle coordinates
	//Eigen::Vector3d newPos = rot.inverse() * posVec + locPattern;
	//double d               = -newPos.dot(newNormal);

	//normal   = cv::Vec3d(newNormal[0], newNormal[1], newNormal[2]);
	//position = cv::Vec3d(newPos[0], newPos[1], newPos[2]);

	//m_normal   = normal;
	//m_position = position;

	// To see if calculated plane jumps.
	//VGDebug(L"Colorplane translation=(%5f,%5f,%5f", position.x(), position.y(), position.z());
	//VGDebug(L"Colorplane normal=(%5f,%5f,%5f", normal.x(), normal.y(), normal.z());

	/*return d;*/

	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	return 0.0;
}

std::vector<cv::Point3f> ProjectorBoardDetector::GetCurrentContourPoints() const
{
	return current_contour_points_;
}

void ProjectorBoardDetector::GetPointsOnBoard(const std::vector<cv::Point3f>& points_3d,
                                              std::vector<int>& on_board_idx,
                                              const double max_dist) const
{
	on_board_idx.clear();

	for(size_t idx = 0; idx < points_3d.size(); ++idx)
	{
		if(fabs(GetDistance(points_3d[idx])) < max_dist)
		{
			on_board_idx.push_back(int(idx));
		}
	}
}

void ProjectorBoardDetector::DrawDetectionResult(cv::Mat& result_img)
{
	// Draw all detected circular coded markers of the board
	for(const auto& detected_marker : detected_board_markers_)
	{
		// draw IDs
		//RenderTools::drawText(resultImg, String::stringFromNumber(m_detectedMarker[iMarker].id), m_detectedMarker[iMarker].ellipse.center, RenderTools::Blue));

		// draw fitted ellipse
		const auto& current_marker = detected_marker.second;
		cv::ellipse(result_img, current_marker.CorrectedEllipse(), Constants::BRIGHT_BLUE);

		// mark zero of outer orientation ring
		const auto angle = detected_marker.second.angle;
		const auto dx    = sin(angle) * current_marker.raw_ellipse.size.width * current_marker.outer_radius_scale * 0.5f;
		const auto dy    = cos(angle) * current_marker.raw_ellipse.size.height * current_marker.outer_radius_scale * 0.5f;
		cv::Point2f c(current_marker.raw_ellipse.center.x + float(dx), current_marker.raw_ellipse.center.y - float(dy));
		cv::circle(result_img, c, 2, Constants::CYAN);
	}
}

int ProjectorBoardDetector::GetMarkerPositions3d(std::vector<cv::Point3f>& marker3d)
{
	marker3d.clear();
	for(auto it = coded_markers_.begin(); it != coded_markers_.end(); ++it)
		marker3d.push_back(it->second);

	return int(marker3d.size());
}

std::vector<cv::Vec3f> ProjectorBoardDetector::GetPlaneContourPts() const
{
	return plane_contour_;
}

double ProjectorBoardDetector::GetDistance(const cv::Point3f& pt3d) const
{
	const auto difference = cv::Vec3d(pt3d.x, pt3d.y, pt3d.z) - position_;
	return difference.dot(normal_);
}

bool ProjectorBoardDetector::GetOrientation(cv::Mat& rotation_vector,
                                            cv::Mat& translation_vector) const
{
	rotation_vector_.copyTo(rotation_vector);
	translation_vector_.copyTo(translation_vector);
	return detected_;
}

bool ProjectorBoardDetector::CalculateOrientation(const bool filter_markers)
{
	detected_ = false;

	// In case there are no uncoded markers, there is no hope that we still
	// detect some uncoded markers
	//if(detected_markers_.size() < 4 /*&& m_uncodedMarkers.empty()*/)
	//	return false;

	//if(detected_markers_.size() == 3 && m_uncodedMarkers.empty())
	//{
	//	// Try to identify additional detected markers here so that we reach
	//	// the required 4 correspondences
	//	if(!identifyUncodedMarkerToReach4DetectedMarkers())
	//	{
	//		return false;
	//	}
	//}
	//else if(detected_markers_.size() < 3)
	//{
	//	// With less than 3 markers, no hope to identify uncoded markers
	//	return false;
	//}

	if(detected_board_markers_.size() < 4)
	{
		//assert(false);
		return false;
	}

	detected_ = SolvePnP(detected_board_markers_, rotation_vector_, translation_vector_);

	if(!detected_ || !filter_markers)
		return detected_;

	const auto calculated_centers             = GetProjectedMarkerCenters(detected_board_markers_, rotation_vector_, translation_vector_);
	const auto average_difference_all_markers = std::accumulate(detected_board_markers_.begin(),
	                                                            detected_board_markers_.end(),
	                                                            0.0,
	                                                            [&calculated_centers](const double& val,
	                                                                                  const std::pair<int, CircularMarker>& marker)
	                                                            {
		                                                            const auto detected_center    = marker.second.CorrectedCenter();
		                                                            const auto& calculated_center = calculated_centers.at(marker.first);
		                                                            return val + cv::norm(detected_center - calculated_center);
	                                                            }) / detected_board_markers_.size();

	std::map<size_t, std::tuple<double, cv::Mat, cv::Mat>> results;

	// Process detected markers to see if there is an outlier and if
	// there is one (and exactly one can be there) then remove this
	// marker from detected marker. Generally, there are small number
	// of marker which are place on the calibration plate.
	if(detected_board_markers_.size() > 4)
	{
		for(auto detected_marker1 : detected_board_markers_)
		{
			auto detected_markers_copy = detected_board_markers_;
			detected_markers_copy.erase(detected_marker1.first);
			cv::Mat rotation_vector, translation_vector;

			const auto detected = SolvePnP(detected_markers_copy, rotation_vector, translation_vector);

			if(!detected)
				continue;

			const auto calculated_centers2 = GetProjectedMarkerCenters(detected_markers_copy, rotation_vector, translation_vector);
			// subset = all markers - 1
			auto average_difference_markers_subset = std::accumulate(detected_markers_copy.begin(),
			                                                         detected_markers_copy.end(),
			                                                         0.0,
			                                                         [&calculated_centers2](const double& val,
			                                                                                const std::pair<int, CircularMarker>& marker)
			                                                         {
				                                                         const auto detected_center    = marker.second.CorrectedCenter();
				                                                         const auto& calculated_center = calculated_centers2.at(marker.first);
				                                                         return val + cv::norm(detected_center - calculated_center);
			                                                         }) / detected_markers_copy.size();

			results[detected_marker1.first] = std::make_tuple(average_difference_markers_subset, rotation_vector, translation_vector);
		}
	}

	// find and remove the element without which the sum of the calculated deltas is the smallest:
	auto min_el = std::min_element(results.begin(),
	                               results.end(),
	                               [](const std::pair<int, std::tuple<double, cv::Mat, cv::Mat>>& p1,
	                                  const std::pair<int, std::tuple<double, cv::Mat, cv::Mat>>& p2) -> bool
	                               {
		                               return std::get<0>(p1.second) < std::get<0>(p2.second); ///  compare the deltas
	                               });

	if(min_el != results.end() && average_difference_all_markers > std::get<0>(min_el->second) * 50.0)
	{
		if(filter_markers)
			detected_board_markers_.erase(min_el->first);

		rotation_vector_    = std::get<1>(min_el->second);
		translation_vector_ = std::get<2>(min_el->second);
		detected_           = true;
	}

	return detected_;
}

bool ProjectorBoardDetector::IdentifyUncodedMarkerToReach4DetectedMarkers()
{
	//we assume here linear relation between 3d base vectors and 2d base vectors, as we assume that the markers all lie in a plane.
	if(detected_board_markers_.size() != 3)
	{
		return false;
	}

	const auto point_idx_0 = detected_board_markers_.begin();
	const auto point_idx_1 = std::next(point_idx_0, 1);
	const auto point_idx_2 = std::next(point_idx_0, 2);
	const auto base2d_1    = point_idx_1->second.raw_ellipse.center - point_idx_0->second.raw_ellipse.center;
	const auto base2d_2    = point_idx_2->second.raw_ellipse.center - point_idx_0->second.raw_ellipse.center;
	const auto base3d_1    = coded_markers_.at(int(point_idx_1->first)) - coded_markers_.at(int(point_idx_0->first));
	const auto base3d_2    = coded_markers_.at(int(point_idx_2->first)) - coded_markers_.at(int(point_idx_0->first));

	//iterate over all uncoded markers and predict where they should appear in the image
	//look whether at that position an uncoded marker is found

	//first is distance to predicted position in 2d
	std::vector<std::pair<double, CircularMarker>> closest_uncoded_markers;

	//we solve for: n*base1 + m*base2 = uncodedPos - basePos, looking for n and m

	Eigen::MatrixXf mat = Eigen::MatrixXf::Zero(3, 2);
	mat(0, 0)           = base3d_1.x;
	mat(1, 0)           = base3d_1.y;
	mat(2, 0)           = base3d_1.z;

	mat(0, 1) = base3d_2.x;
	mat(1, 1) = base3d_2.y;
	mat(2, 1) = base3d_2.z;

	std::vector<std::pair<double, std::pair<int, CircularMarker>>> close_markers;

	for(const auto& uncoded_marker3d : uncoded_markers_)
	{
		Eigen::VectorXf eigen_point = Eigen::VectorXf::Zero(3);
		const auto point            = uncoded_marker3d.second - coded_markers_.at(int(point_idx_0->first));
		eigen_point(0)              = point.x;
		eigen_point(1)              = point.y;
		eigen_point(2)              = point.z;

		//this is the least square solution that gives the marker position as linear combination of the two base directions in 3d
		Eigen::VectorXf least_square = mat.colPivHouseholderQr().solve(eigen_point);

		//the predicted position in the image is then
		const double n         = least_square(0);
		const double m         = least_square(1);
		auto predicted_img_pos = point_idx_0->second.raw_ellipse.center + n * base2d_1 + m * base2d_2;

		//find the coded marker that is closest
		auto closest = std::min_element(detected_markers_without_id_.begin(),
		                                detected_markers_without_id_.end(),
		                                [&predicted_img_pos](const CircularMarker& first,
		                                                     const CircularMarker& second) -> bool
		                                {
			                                return cv::norm(first.raw_ellipse.center - predicted_img_pos) < cv::norm(
				                                second.raw_ellipse.center - predicted_img_pos);
		                                });

		//add to the closeMarkers list if distance is smaller than 3 pixels
		if(closest == detected_markers_without_id_.end())
		{
			continue;
		}


		const auto dist_to_prediction = cv::norm((*closest).raw_ellipse.center - predicted_img_pos);

		if(dist_to_prediction < 3.0)
		{
			close_markers.emplace_back(dist_to_prediction, std::make_pair(uncoded_marker3d.first, *closest));
		}
	}

	//no close marker has been found.
	if(close_markers.empty())
	{
		return false;
	}

	//add to list of detected markers if predicted position deviates to measured position by less than 3 pixel
	for(const auto& marker : close_markers)
	{
		if(marker.first < 3.0)
		{
			detected_board_markers_[marker.second.first] = marker.second.second;
		}
	}

	return true;
}


std::map<int, cv::Point2f> ProjectorBoardDetector::GetProjectedMarkerCenters(const std::map<size_t, CircularMarker>& detected_marker,
                                                                             const cv::Mat& rotation_vector,
                                                                             const cv::Mat& translation_vector) const
{
	std::vector<cv::Point3f> marker3d_pts;
	std::vector<int> marker_ids;
	auto marker_positions = GetAllMarkerCoordinates();

	for(const auto& marker : detected_marker)
	{
		marker3d_pts.push_back(marker_positions.at(int(marker.first)));
		marker_ids.push_back(int(marker.first));
	}

	const auto marker3d_pts_local = MathHelper::ApplyTransformation(rotation_vector, translation_vector, marker3d_pts);
	std::vector<cv::Vec2f> projected;
	calibration_->ProjectPoints(marker3d_pts_local, projected);

	std::map<int, cv::Point2f> out;

	for(size_t i = 0; i < marker_ids.size(); ++i)
	{
		out[marker_ids[i]] = projected[i];
	}

	return out;
}

StatisticVal<double> ProjectorBoardDetector::GetStats() const
{
	StatisticValSet<double> stats;
	auto calculated_centers = GetProjectedMarkerCenters(detected_board_markers_, rotation_vector_, translation_vector_);
	std::map<int, cv::Point2f> delta;
	std::vector<cv::Vec2f> detected_centers;

	for(const auto& marker : detected_board_markers_)
	{
		const auto detected_center    = marker.second.CorrectedCenter();
		const auto& calculated_center = calculated_centers[int(marker.first)];
		stats.add(cv::norm(detected_center - calculated_center));
		delta[int(marker.first)] = detected_center - calculated_center;
	}

	stats.finish();

	return stats;
}

double ProjectorBoardDetector::GetRMS() const
{
	if(!detected_)
		return -1.0;

	const auto stats = GetStats();

	return stats.meanVal;
}

std::map<int, cv::Point3f> ProjectorBoardDetector::GetMarkerCoordinatesInFile() const
{
	return coded_markers_;
}

void ProjectorBoardDetector::RedefineCoordinateSystemOxy()
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
}

std::map<int, cv::Point3f> ProjectorBoardDetector::GetCodedMarkerCoordinates() const
{
	return coded_markers_;
}

std::map<int, cv::Point3f> ProjectorBoardDetector::GetUncodedMarkerCoordinates() const
{
	return uncoded_markers_;
}

cv::Mat ProjectorBoardDetector::GetImage() const
{
	return image_;
}

std::pair<int, cv::Point3f> ProjectorBoardDetector::GetOrigin() const
{
	return std::make_pair(origin_id_, coded_markers_.at(origin_id_));
}

std::pair<int, cv::Point3f> ProjectorBoardDetector::GetX() const
{
	return std::make_pair(x_id_, coded_markers_.at(x_id_));
}

std::pair<int, cv::Point3f> ProjectorBoardDetector::GetY() const
{
	return std::make_pair(y_id_, coded_markers_.at(y_id_));
}

void ProjectorBoardDetector::ClearDetectedMarkersWithoutID()
{
	detected_markers_without_id_.clear();
}

void ProjectorBoardDetector::SetDetectedMarkersWithoutID(const std::vector<CircularMarker>& in)
{
	detected_markers_without_id_ = in;
}

void ProjectorBoardDetector::SetOnsiteCalibrationMode(const bool value)
{
	on_site_calibration_mode_ = value;
}

void ProjectorBoardDetector::UpdateOXZ()
{
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
}

std::map<int, cv::Point3f> ProjectorBoardDetector::GetAllMarkerCoordinates() const
{
	std::map<int, cv::Point3f> marker_coordinates;
	marker_coordinates.insert(coded_markers_.begin(), coded_markers_.end());
	marker_coordinates.insert(uncoded_markers_.begin(), uncoded_markers_.end());
	return marker_coordinates;
}

cv::Vec3d ProjectorBoardDetector::FrameCoordinates(const int id) const
{
	// get 3d position in board coordinates of detected coded marker on board
	cv::Vec3d pt;
	GetCodedMarker(id, pt);

	// transform circle center (world coordinates) to frame coordinates (pFrame)
	cv::Mat r_mat;
	cv::Rodrigues(rotation_vector_, r_mat);
	Eigen::Matrix3d rot;
	cv2eigen(r_mat, rot);

	const Eigen::Vector3d translation(translation_vector_.at<double>(0, 0),
	                                  translation_vector_.at<double>(1, 0),
	                                  translation_vector_.at<double>(2, 0));
	Eigen::Vector3d point(pt[0], pt[1], pt[2]);
	Eigen::Vector3d point_frame = rot * point + translation;
	return cv::Vec3d(point_frame[0], point_frame[1], point_frame[2]);

	//Get 3d position in board coordinates of detected coded marker on board
	//cv::Vec3d pt_board;
	//GetCodedMarker(id, pt_board);

	////Get the 3d position in camera coordinates
	//auto points = std::vector<cv::Point3d>(1, pt_board);
	//auto pt_cam = Helper::ApplyTransformation(rotation_vector_, translation_vector_, points);
	//return pt_cam[0];
}

double ProjectorBoardDetector::GetOrientationTestQuality() const
{
	//1. find the 8 markers that are left-most in the color picture (lowest x coordinate)
	//2. solve pose for these markers (system 1)
	//3. solve pose for the remaining markers (at least 7) (system 2)
	//4. translate mean 3d coordinate of system 1 into system 2 --> determine 3d distance, should be zero in perfect registration

	if(detected_board_markers_.size() < 8 + 7)
		return -1.;

	std::map<size_t, CircularMarker> markers1;
	std::map<size_t, CircularMarker> markers2;

	//determine pose for system 1 and 2
	{
		std::vector<CircularMarker> all_markers(detected_board_markers_.size());
		std::transform(detected_board_markers_.begin(),
		               detected_board_markers_.end(),
		               all_markers.begin(),
		               [](const std::pair<int, CircularMarker>& in) -> CircularMarker
		               {
			               return in.second;
		               });

		std::sort(all_markers.begin(),
		          all_markers.end(),
		          [](const CircularMarker& first,
		             const CircularMarker& second)
		          {
			          return first.raw_ellipse.center.x < second.raw_ellipse.center.x;
		          });

		for(size_t i = 0; i < 8; ++i)
		{
			markers1[int(all_markers[i].id)] = all_markers[i];
		}

		for(size_t i = 8; i < all_markers.size(); ++i)
		{
			markers2[int(all_markers[i].id)] = all_markers[i];
		}
	}

	ProjectorBoardDetector board1(calibration_);
	ProjectorBoardDetector board2(calibration_);

	board1.coded_markers_ = coded_markers_;
	board2.coded_markers_ = coded_markers_;

	board1.detected_board_markers_ = markers1;
	board2.detected_board_markers_ = markers2;

	if(!board1.CalculateOrientation() || !board2.CalculateOrientation())
	{
		return -1.;
	}

	//now take mean point of markers of system 1 and transform it to system 2
	cv::Point3f mean(0., 0., 0.);
	for(const auto& pt : markers1)
	{
		mean += coded_markers_.at(int(pt.first));
	}
	mean *= 1.f / markers1.size();

	//TODO
	/*const std::vector<cv::Point3f> meanV(1, mean);
	const std::vector<cv::Point3f> mean1V = Helper::ApplyTransformation(board1.rotation_vector_, board1.translation_vector_, meanV);
	const std::vector<cv::Point3f> mean2V = Helper::applyTrafoInv(board2.rotation_vector_, board2.translation_vector_, mean1V);*/

	//const double dist = cv::norm(mean - mean2V[0]);

	/*return dist;*/
	BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	return 0.0;
}

bool ProjectorBoardDetector::SolvePnP(const std::map<size_t, CircularMarker>& markers,
                                      cv::Mat& rotation_vector,
                                      cv::Mat& translation_vector) const
{
	std::vector<cv::Point2f> image_points;
	std::vector<cv::Point3f> object_points;
	auto all_marker_positions = GetAllMarkerCoordinates();

	for(const auto& marker : markers)
	{
		// use only valid coded ring marker
		if(marker.second.confidence < 0.0)
			continue;

		// already tested at detection time
		// use only ring marker id that is part of the board
		image_points.push_back(marker.second.CorrectedCenter());
		object_points.push_back(all_marker_positions.at(marker.first));
	}

	return calibration_->SolvePnP(object_points, image_points, rotation_vector, translation_vector);
}

void ProjectorBoardDetector::SetDetectedMarkerCoordinate(const int id,
                                                         const CircularMarker& marker)
{
	detected_board_markers_[id] = marker;
}

void ProjectorBoardDetector::Reset()
{
	detected_board_markers_.clear();
	detected_ = false;
}

void ProjectorBoardDetector::AssignIDsToDetectedMarkers()
{
	AssignIDsToDetectedMarkers(detected_markers_without_id_);
}

void ProjectorBoardDetector::AssignIDsToDetectedMarkers(std::vector<CircularMarker> detected_markers_without_id)
{
	if(detected_markers_without_id.empty())
	{
		return;
	}

	// Calculate perspective transformation between board coordinates and image coordinates.
	// Here, we assume that the boards are flat and the z coordinates of the markers are zero.
	std::vector<cv::Point2d> coded_marker_positions2d;
	std::vector<cv::Point2d> coded_marker_positions_in_image;

	for(auto it = coded_markers_.begin(); it != coded_markers_.end(); ++it)
	{
		auto id    = it->first;
		auto pos3d = it->second;

		if(detected_board_markers_.count(id) != 0)
		{
			auto marker = detected_board_markers_.at(id);
			coded_marker_positions2d.push_back(cv::Point2f(pos3d.x, pos3d.y));
			coded_marker_positions_in_image.push_back(cv::Point2f(marker.CorrectedCenter().x, marker.CorrectedCenter().y));
		}
	}
	// we need at least four points to calculate the transformation
	if(coded_marker_positions_in_image.size() < 4)
	{
		return;
	}

	auto transformation_matrix = cv::findHomography(coded_marker_positions2d, coded_marker_positions_in_image);

	// use perspective transformation to obtain expected image coordinates of uncoded markers
	std::map<int, cv::Point2f> expected_marker_positions_in_image;

	for(auto it = uncoded_markers_.begin(); it != uncoded_markers_.end(); ++it)
	{
		auto id    = it->first;
		auto pos3d = it->second;
		cv::Point2f pos2d(pos3d.x, pos3d.y);
		std::vector<cv::Point2f> v_in(1, pos2d);
		std::vector<cv::Point2f> v_out;
		cv::perspectiveTransform(v_in, v_out, transformation_matrix);

		expected_marker_positions_in_image[id] = v_out[0];
	}

	// check if the detected position matches an estimated marker position
	for(const auto& detected_marker : detected_markers_without_id)
	{
		cv::Point2f position_in_image(detected_marker.CorrectedCenter().x, detected_marker.CorrectedCenter().y);

		for(auto it = expected_marker_positions_in_image.begin(); it != expected_marker_positions_in_image.end(); ++it)
		{
			auto id                         = it->first;
			auto expected_position_in_image = it->second;
			auto distance_in_pixels         = float(cv::norm(position_in_image - expected_position_in_image));

			if(distance_in_pixels < 1)
			{
				detected_board_markers_[id] = detected_marker;
				break;
			}
		}
	}
}
}
