#include "projector.h"
#include "statistics_value_median_set.h"
#include "fit_plane.h"

using namespace Reconstruct;

Projector::Projector(const std::shared_ptr<const CameraSensor>& calibration,
                     const std::shared_ptr<ConfigurationContainer>& options) :
	search_window_x_{options->GetProjectorCalibrationCenterBeamWindowPositionX()},
	search_window_y_{options->GetProjectorCalibrationCenterBeamWindowPositionY()},
	min_beam_length_{options->GetProjectorCalMinBeamLength()},
	search_window_width_{options->GetProjectorCalibrationCenterBeamWindowWidth()},
	search_window_height_{options->GetProjectorCalibrationCenterBeamWindowHeight()},
	min_num_beams_{options->GetProjectorCalibrationMinBrightBeams()},
	use_dmvs_projector_calibration_{options->GetUseDMVSProjectorCalibration()},
	calibration_{calibration->DuplicateLaser()},
	calibration_step_{-1},
	next_step_forced_{false},
	calibration_done_{false},
	step1_green_points_(0),
	green_points_(0),
	capture_mode_(),
	max_beam_point_projection_distance_{3.0},
	no_new_points_{0},
	step2_center_point_added_as_bright_{false},
	options_(options)
{
	// beam projection point is new cluster if no existing cluster is in 3.0 distance
	// value is in projector image plane pixel ( defined to scale as the used IR camera)
}

Projector::Projector(const std::vector<cv::Point2f>& beams,
                     const std::vector<cv::Point2f>& bright_beams) :
	search_window_x_(0),
	search_window_y_(0),
	min_beam_length_(0),
	search_window_width_(0),
	search_window_height_(0),
	min_num_beams_(500),
	use_dmvs_projector_calibration_(false),
	calibration_step_(-1),
	next_step_forced_(false),
	calibration_done_(false),
	step1_green_points_(0),
	green_points_(0),
	capture_mode_(),
	max_beam_point_projection_distance_(3.0),
	no_new_points_(0),
	step2_center_point_added_as_bright_(false)
{
}

Projector::~Projector() = default;

bool Projector::GetCalibrationResult(std::shared_ptr<CameraSensor>& result) const
{
	if(calibration_done_)
	{
		result = calibration_;
	}

	return calibration_done_;
}

void Projector::UpdateProjections(std::vector<LaserBeam>& dots,
                                  std::shared_ptr<const CameraSensor> calibration /*= 0 */) const
{
	if(!calibration)
	{
		calibration = calibration_;
	}

	if(!calibration)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration object is NULL";
	}

	// BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  update 2d mean positions");
	for(size_t i = 0; i < dots.size(); ++i)
	{
		std::vector<cv::Point2f> projected_points_2d;
		calibration->LaserProjectPoints(dots[i].beam_3d_points, projected_points_2d);

		cv::Point2f mean_pt2d(0.0, 0.0);

		for(const auto idx : projected_points_2d)
		{
			mean_pt2d += idx;
		}

		dots[i].mean_point       = mean_pt2d * (1.0f / float(projected_points_2d.size()));
		dots[i].number_of_points = int(projected_points_2d.size());
	}

	// BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  update 2d mean positions ... done");
}

int Projector::GetUseCount(const int beam_idx)
{
	// Get reference to beams to be used for current Step
	auto& beams = GetUsedBeams();
	return beam_idx < int(beams.size()) ? beams[beam_idx].number_of_points : 0;
}

int Projector::GetCalibrationStep() const
{
	return calibration_step_;
}

int Projector::GetStep4PointsCount() const
{
	return green_points_;
}

int Projector::GetStep1GreenPointsCount() const
{
	return green_points_;
}

void Projector::SetDoeYmlPath(const std::string path)
{
	doe_yml_path_ = path;
}

int Projector::GetMinDotNumberToFinish() const
{
	return options_->GetMinProjectorCalibrationPointsToFinish();
}

cv::Mat Projector::CrossHairImage() const
{
	return cross_hair_image_;
}

void Projector::AddPoints3dRecalibration(const std::vector<cv::Point3f>& points_3d,
                                         const std::vector<DotInfoType>& infos)
{
	if(points_3d.size() != infos.size())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() :  ERROR - Projector indices for recalibration";
		return;
	}

	// Get reference to beams to be used for current Step
	auto& beams = GetUsedBeams();
	std::vector<cv::Point2f> pts2d;
	calibration_->ProjectPoints(points_3d, pts2d);

	for(size_t i = 0; i < points_3d.size(); ++i)
	{
		// ignore points closer than 1m, it may be a dark point
		if(calibration_step_ == 100 && (points_3d[i].z < 1.0f))
			continue;

		Add3dPoint(points_3d[i], infos[i], pts2d[i], beams, infos[i].beam_idx);
	}

	CheckCalibrationStep();
}

void Projector::Add3dPoint(const cv::Point3f point_3d,
                           const DotInfoType info,
                           const cv::Point2f point_2d,
                           std::vector<LaserBeam>& beams,
                           const int beam_idx)
{
	// incremental update of mean value
	const auto number_of_points = float(beams[beam_idx].number_of_points);
	beams[beam_idx].mean_point  = (number_of_points * beams[beam_idx].mean_point + point_2d) * (1 / (number_of_points + 1.0));

	// no div operator for cv::Point2f!
	beams[beam_idx].number_of_points++;

	// add 3d point to the 3d point vector that corresponds to the cluster
	beams[beam_idx].beam_3d_points.push_back(point_3d);
	beams[beam_idx].dot_infos.push_back(info);

	// update min/max Z values of 3d point in the cluster
	if(point_3d.z > beams[beam_idx].farthest_3d_point_to_beam)
	{
		beams[beam_idx].farthest_3d_point_to_beam = point_3d.z;
	}

	if(point_3d.z < beams[beam_idx].nearest_3d_point_to_beam)
	{
		beams[beam_idx].nearest_3d_point_to_beam = point_3d.z;
	}

	if(!beams[beam_idx].is_beam && beams[beam_idx].number_of_points > 10
		&& fabs(beams[beam_idx].farthest_3d_point_to_beam - beams[beam_idx].nearest_3d_point_to_beam) > min_beam_length_)
	{
		beams[beam_idx].is_beam = true;
		stable_beams_indexes_.push_back(beam_idx);
	}
}

void Projector::Add3dPoints(const std::vector<cv::Point3f>& reconstructed_3d_points,
                            const std::vector<DotInfoType>& infos)
{
	if(reconstructed_3d_points.empty())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No reconstructed points.";
		return;
	}

	if(calibration_step_ >= 100)
	{
		AddPoints3dRecalibration(reconstructed_3d_points, infos);
		return;
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Adding " << reconstructed_3d_points.size() << " points to step " << calibration_step_;

	std::vector<DotInfoType> filtered_dot_infos;
	std::vector<cv::Point3f> filtered_points;

	// Get reference to beams to be used for current Step
	auto& beams = GetUsedBeams();

	if(calibration_step_ < 3)
	{
		// Calc mean brightness of all points
		auto mean_brightness = 0.0f;

		for(auto info : infos)
		{
			mean_brightness += info.brightness;
		}

		/*	const auto brightness = std::accumulate(infos.begin(),
			                                        infos.end(),
			                                        0.0,
			                                        [](const DotInfoType info)
			                                        {
				                                        return info.brightness;
			                                        }) / infos.size();*/

		mean_brightness /= float(infos.size());

		for(size_t i = 0; i < infos.size(); ++i)
		{
			if(infos[i].brightness > 0.5f * mean_brightness || infos[i].brightness < 2.0f * mean_brightness)
			{
				filtered_points.push_back(reconstructed_3d_points[i]);
				filtered_dot_infos.push_back(infos[i]);
			}
		}

		AddPointsToBeams(filtered_points, filtered_dot_infos, beams);
	}
	else
	{
		AddPointsToBeams(reconstructed_3d_points, infos, beams);
	}

	CheckCalibrationStep();
}

cv::Mat Projector::GetFeedbackImage()
{
	// clear image draw axis
	const cv::Point2f offset(800, 600);
	feedback_image_ = cv::Mat::zeros(1200, 1600, CV_8UC3);
	line(feedback_image_, cv::Point2f(0, offset.y), cv::Point2f(1600, offset.y), Constants::COLOR2);
	line(feedback_image_, cv::Point2f(offset.x, 0), cv::Point2f(offset.x, 1200), Constants::COLOR2);

	// draw central beam search window
	const auto upper_left  = cv::Point2f(search_window_x_ - search_window_width_ / 2, search_window_y_ - search_window_height_ / 2) + offset;
	const auto upper_right = cv::Point2f(search_window_x_ - search_window_width_ / 2, search_window_y_ + search_window_height_ / 2) + offset;
	const auto lower_left  = cv::Point2f(search_window_x_ + search_window_width_ / 2, search_window_y_ - search_window_height_ / 2) + offset;
	const auto lower_right = cv::Point2f(search_window_x_ + search_window_width_ / 2, search_window_y_ + search_window_height_ / 2) + offset;

	line(feedback_image_, upper_left, upper_right, Constants::COLOR3);
	line(feedback_image_, upper_right, lower_right, Constants::COLOR3);
	line(feedback_image_, lower_right, lower_left, Constants::COLOR3);
	line(feedback_image_, lower_left, upper_left, Constants::COLOR3);

	// get current vector to add new 3d points and cluster
	auto& beams = GetUsedBeams();

	// also shows step 1 points in step 2 (bright points)
	if(calibration_step_ == 2)
	{
		for(auto beam : beams)
		{
			cv::circle(feedback_image_,
			           cv::Point2f(beam.mean_point) + offset,
			           2,
			           Constants::MAGENTA,
			           1);
		}
	}

	if(calibration_step_ == 2 && aligned_doe_.rows > 0)
	{
		for(auto i = 0; i < aligned_doe_.rows; ++i)
		{
			cv::Point2f circle_center(float(aligned_doe_.at<double>(i, 1)), float(aligned_doe_.at<double>(i, 2)));
			const auto radius = aligned_doe_.at<double>(i, 0) < 0.5 ? 2 : 4;
			cv::circle(feedback_image_, circle_center + offset, radius, Constants::WHITE);
		}
	}

	for(auto beam : beams)
	{
		const auto beam_points_count = beam.number_of_points;

		// drawing color : <6: RED    <15 yellow  >= 15 green
		cv::Scalar color;
		//beam_points_count < 15 ? (beam_points_count < 6 ? Constants::DARK_BLUE : Constants::COLOR1) : Constants::BRIGHT_GREEN;

		if(beam_points_count < 6)
			color = Constants::DARK_BLUE;

		else if(beam_points_count < 15)
			color = Constants::YELLOW;

		else
			color = Constants::BRIGHT_GREEN;

		if(beam_points_count > 1)
		{
			cv::circle(feedback_image_, cv::Point2f(beam.mean_point) + offset, 2, color, 0);
		}

		if(beam.is_beam && calibration_step_ < 2)
		{
			cv::circle(feedback_image_, cv::Point2f(beam.mean_point) + offset, 6, Constants::PURE_BLUE, 0);
		}
	}

	return feedback_image_;
}

void Projector::ForceNextCalibrationStep()
{
	next_step_forced_ = true;
}

bool Projector::CalculateProjectorPosition(cv::Point3f& position,
                                           const std::vector<LaserBeam>& beams) const
{
	if(beams.empty())
		return false;

	// 	Calculate all beam lines
	DotInfos lines;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  step 1 calc new projector position with " << beams.size() << " beams";

	std::vector<std::vector<cv::Point3f>>::iterator it;

	for(auto beam : beams)
	{
		auto pts(beam.beam_3d_points);

		if(pts.size() < 3)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Could not calculate projector position. Given beam has less than 3 points.";
			continue;
		}

		// calculate line
		cv::Mat l;
		cv::fitLine(pts, l, cv::DIST_HUBER, 0.5, 0.001, 0.001);
		cv::Vec6f line(l.at<float>(0, 0),
		               l.at<float>(1, 0),
		               l.at<float>(2, 0),
		               l.at<float>(3, 0),
		               l.at<float>(4, 0),
		               l.at<float>(5, 0));

		lines.push_back(line);
	}

	if(lines.empty())
	{
		return false;
	}

	// Calculate intersection of all lines
	Eigen::Matrix3d directions(Eigen::Matrix3d::Zero());
	Eigen::Vector3d positions(Eigen::Vector3d::Zero());

	for(size_t i = 0; i < lines.size(); ++i)
	{
		// Get ray direction
		Eigen::Vector3d w(lines[i][0], lines[i][1], lines[i][2]);
		Eigen::Vector3d p(lines[i][3], lines[i][4], lines[i][5]);
		w.normalize();
		Eigen::Matrix3d V = Eigen::Matrix3d::Identity() - (w * w.transpose());
		directions += V;
		positions += V * p;
	}

	// t is intersection point of all beams
	Eigen::Vector3d t = directions.inverse() * positions;

	// get all distances of lines to intersection point and RMS
	std::vector<double> line_distances;

	double sqDistMax = 0.0, rms = 0.0;
	for(size_t i = 0; i < lines.size(); ++i)
	{
		Eigen::Vector3d dir(lines[i][0], lines[i][1], lines[i][2]);
		Eigen::Vector3d pos(lines[i][3], lines[i][4], lines[i][5]);
		Eigen::ParametrizedLine<double, 3> line(pos, dir);

		double sqDist = line.squaredDistance(t);
		line_distances.push_back(sqrt(sqDist));
		rms += sqDist;

		if(sqDist > sqDistMax)
			sqDistMax = sqDist;
	}
	rms = sqrt(rms / (double)lines.size());

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : projector position with beams: num beams " << lines.size() << " rms : " << rms * 1000.0 <<
 "mm max :" << sqrt(sqDistMax) * 1000 << "mm ";

	// collect  1,5 sigma inliers
	int goodLinesCount = 0;
	directions.setZero();
	positions.setZero();
	for(size_t i = 0; i < lines.size(); ++i)
	{
		if(line_distances[i] > 1.5 * rms)
			continue;

		// Get ray direction
		Eigen::Vector3d w(lines[i][0], lines[i][1], lines[i][2]);
		Eigen::Vector3d p(lines[i][3], lines[i][4], lines[i][5]);
		w.normalize();
		Eigen::Matrix3d V = Eigen::Matrix3d::Identity() - (w * w.transpose());
		directions += V;
		positions += V * p;
		goodLinesCount++;
	}

	if(goodLinesCount == 0)
		return false;
	// t is intersection point of all 1.5 sigma lines
	Eigen::Vector3d tGoodLines = directions.inverse() * positions;

	// get all maxDist and RMS of good lines only
	double sqDistMaxGoodLines = 0.0;
	double rmsGoodLines       = 0.0;

	for(size_t i = 0; i < lines.size(); ++i)
	{
		if(line_distances[i] > 1.5 * rms)
			continue;

		Eigen::Vector3d dir(lines[i][0], lines[i][1], lines[i][2]);
		Eigen::Vector3d pos(lines[i][3], lines[i][4], lines[i][5]);
		Eigen::ParametrizedLine<double, 3> line(pos, dir);

		double sqDist = line.squaredDistance(tGoodLines);
		rmsGoodLines += sqDist;
		if(sqDist > sqDistMaxGoodLines)
			sqDistMaxGoodLines = sqDist;
	}

	rmsGoodLines = sqrt(rmsGoodLines / double(goodLinesCount));

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector position with 1.5 sigma beams only : num " <<
		goodLinesCount << " rms : " << rmsGoodLines * 1000.0 << "mm max : " << sqrt(sqDistMaxGoodLines) * 1000 << "mm";

	//VgCalibrationReport::trySet(L"Step1ProjectorPositionRMS-mm", rmsGoodLines * 1000.0);
	//VgCalibrationReport::trySet(L"Step1ProjectorPositionMaxErr-mm", sqrt(sqDistMaxGoodLines) * 1000.0);
	//VgCalibrationReport::trySet(L"Step1ProjectorPosition1.5SigmaBeams", (double)goodLinesCount);

	// Calibration of Camera/Projector is given in World to Camera direction
	// i.e. the position is the position of World origin (color cam at Freestyle) in Camera coordinate system
	Eigen::Matrix3d rotation_eigen;
	cv::Mat rotation_matrix, translation_vector;
	calibration_->GetExtrinsicParameters(rotation_matrix, translation_vector);
	cv::cv2eigen(rotation_matrix, rotation_eigen);
	Eigen::Vector3d p = -rotation_eigen * tGoodLines;
	position          = cv::Point3f(float(p[0]), float(p[1]), float(p[2]));

	return true;
}

Projector::CaptureMode Projector::GetCaptureMode() const
{
	return capture_mode_;
}

static cv::Vec2f GetCoG(std::vector<cv::Vec2f> vec)
{
	cv::Vec2f cog(0.0, 0.0);
	for(size_t i = 0; i < vec.size(); ++i)
		cog += vec[i];

	cog /= (float)vec.size();
	return cog;
}

void Projector::CheckCalibrationStep()
{
	if(calibration_step_ == -1)
	{
		calibration_step_ = 1;
		InitStep1();
		return;
	}

	if(calibration_step_ == 1 && Step1Done())
	{
		calibration_step_++;
		InitStep2();
		return;
	}

	if(calibration_step_ == 2 && Step2Done())
	{
		calibration_step_++;
		InitStep3();
		return;
	}

	if(calibration_step_ == 3 && Step3Done())
	{
		calibration_step_++;
		InitStep4();
		return;
	}

	if(calibration_step_ == 4 && Step4Done())
	{
		calibration_step_++;
		calibration_done_ = true;
		return;
	}

	if(calibration_step_ == 100 && Step100Done())
	{
		size_t s          = GetUsedBeams().size();
		calibration_step_ = 101;
		GetUsedBeams().clear();
		GetUsedBeams().resize(s);
		InitStep101();
		return;
	}

	if(calibration_step_ == 101 && Step101Done())
	{
		calibration_step_ = 102;
		calibration_done_ = true;
		return;
	}

	return;
}

void Projector::SetOutputPath(const std::string& path)
{
	calibration_output_path_ = path;
}

double Projector::GetParaboloidCompensationX() const
{
	return doe_correction_rms_x_;
}

double Projector::GetParaboloidCompensationY() const
{
	return doe_correction_rms_y_;
}

bool Projector::PairCmp2nd(std::pair<int, double> a,
                           std::pair<int, double> b)
{
	return (a.second < b.second);
}

bool Projector::InitStep1()
{
	// In step 0, the projector position is to be found by intersecting
	// the rays of bright dots.
	// Condition: Enough rays of bright points have to be detected.
	// The rays must be at least BEAM_MIN_LENGTH m long (max z - min z)
	green_points_     = 0;
	calibration_step_ = 1;
	no_new_points_    = 0;

	cv::Mat rotation_matrix, rotation_matrix_transpose, translation_vector;
	calibration_->GetExtrinsicParameters(rotation_matrix, translation_vector);

	// Use translation from default camera calibration xml file
	cv::transpose(rotation_matrix, rotation_matrix_transpose);
	translation_vector = rotation_matrix_transpose * translation_vector;

	rotation_matrix = cv::Mat::eye(3, 3, CV_64F);
	calibration_->SetExtrinsicParameters(rotation_matrix, translation_vector);

	GetUsedBeams().clear();
	stable_beams_indexes_.clear();

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initialized STEP 1, set laser power to bright only";

	capture_mode_ = CALIBRATION_PATTERN_BRIGHT_ONLY;

	return true;
}

bool Projector::InitStep2()
{
	// In step 1, the projector rotation is to be found by
	// identifying the central dot.
	// For every dot around the center of the projector feedback image,
	// the projector is rotated towards that point and all rays are projected to
	// a temporary 2D image. If enough dots around the center are point symmetric to the
	// temporary central dot, this dot is chosen to be the correct central dot.
	green_points_ = 0;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initialise step 2, set laser power to bright only";
	capture_mode_ = CALIBRATION_PATTERN_BRIGHT_ONLY;
	return true;
}

bool Projector::InitStep3()
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initialise step 3, set laser power to bright only";

	if(rotation_detector_step1_.LoadOriginalPattern(doe_yml_path_.c_str()) != 0)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DOE yml file " << doe_yml_path_ << " not found! Aborting Calibration";
		return false;
	}

	capture_mode_ = CALIBRATION_PATTERN_BRIGHT_ONLY;
	return true;
}

bool Projector::InitStep4()
{
	// In step 4, all dark dots are to be identified by matching them against the original doe pattern.
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Initialise step 4, set laser power to all points";
	capture_mode_ = CALIBRATION_PATTERN_ALL;
	green_points_ = 0;
	auto& beams   = GetUsedBeams();
	std::vector<cv::Point3f> pts3d;
	std::vector<DotInfoType> dot_infos;
	DotInfoType dummy;
	dummy.brightness = 1.0f;
	dummy.dia        = 1.0;

	for(auto i = 0; i < aligned_doe_.rows; ++i)
	{
		cv::Point3f pProj((float)aligned_doe_.at<double>(i, 1), (float)aligned_doe_.at<double>(i, 2), (float)aligned_doe_.at<double>(i, 3));
		cv::Point3f pWorld = calibration_->FromCameraToWorldCoordinateSystem(pProj);
		pts3d.push_back(pWorld);
		dummy.beam_idx = i;
		dot_infos.push_back(dummy);
	}

	max_beam_point_projection_distance_ = 4.0f;
	stable_beams_indexes_.clear();

	// Copy already found points to add them to the DOE pattern beams later
	std::vector<cv::Point3f> old_pts;
	std::vector<DotInfoType> old_dot_infos;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : beams.size()  " << beams.size();

	for(auto beam_idx = 0; beam_idx < beams.size(); ++beam_idx)
	{
		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : beams[beamsIt].beam_3d_points.size()  " << beams[beam_idx].beam_3d_points.size();

		for(auto beam_points_idx = 0; beam_points_idx < beams[beam_idx].beam_3d_points.size(); ++beam_points_idx)
		{
			auto point_3d = beams[beam_idx].beam_3d_points[beam_points_idx];
			old_pts.push_back(point_3d);
			auto info = beams[beam_idx].dot_infos[beam_points_idx];
			old_dot_infos.push_back(info);
		}

		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : oldPts.size()  " << old_pts.size();
	}

	beams.clear();
	Add3dPoints(pts3d, dot_infos);

	// Add already found points to pattern DOE points
	Add3dPoints(old_pts, old_dot_infos);
	no_new_points_ = 1;

	// clean current point collection
	// init point collection with 1 point for each pattern dot
	return true;
}

bool Projector::InitStep100()
{
	capture_mode_ = CALIBRATION_PATTERN_BRIGHT_ONLY;
	return true;
}

bool Projector::InitStep101()
{
	capture_mode_ = TRACKING;
	return true;
}

double Projector::GetPointSymmetryRmsByBeam(const std::shared_ptr<CameraSensor> projector_calibration,
                                            const LaserBeam& beam,
                                            int& num_matches)
{
	return GetPointSymmetryRmsByCoordinate(projector_calibration, beam.mean_point, num_matches);
}

double Projector::GetPointSymmetryRmsByCoordinate(const std::shared_ptr<CameraSensor>& projector_calibration,
                                                  cv::Point2f center_candidate,
                                                  int& num_matches)
{
	static int counter     = 0;
	auto proj_cal_tmp_copy = projector_calibration; /* ->DuplicateLaser();*/
	auto point_step1_copy  = GetUsedBeams();
	RotateProjectorToCenter(center_candidate, proj_cal_tmp_copy);
	UpdateProjections(point_step1_copy, proj_cal_tmp_copy);

	// Get all beams longer than BEAM_MIN_LENGTH (max z - min z)
	std::vector<cv::Vec2f> bright_only_pts;
	{
		for(auto point_step1 : point_step1_copy)
		{
			if(!point_step1.is_beam)
				continue;

			bright_only_pts.push_back(point_step1.mean_point);
		}

		green_points_ = int(bright_only_pts.size());
	}

	if(bright_only_pts.size() < 100)
	{
		return -1;
	}

	// create kd tree for neighbor search
	cv::Mat samples(int(bright_only_pts.size()), 2, CV_32F);
	for(int i = 0; i < int(bright_only_pts.size()); ++i)
	{
		samples.at<float>(i, 0) = bright_only_pts[i][0];
		samples.at<float>(i, 1) = bright_only_pts[i][1];
	}
	// BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  create kd tree with %d points", brightOnlyPts.size() );

	cv::flann::KDTreeIndexParams indexParams(1);

	// Create the Index
	cv::flann::Index kdtree(samples, indexParams);
	cv::flann::SearchParams cv_radius_params(-1, 0.0f, false);

	// BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  created kd tree with %d points .. done", brightOnlyPts.size() );

	double errorSquaredSum = 0;
	int neighboursFound    = 0;
	double maxDist         = options_->GetMaxDistSymmetryTest();

	// Point must lie within 1 pixels of symmetry point

	std::vector<int> neighbourIds;
	std::vector<float> neighboursDists;

	for(int beamsIt = 0; beamsIt < bright_only_pts.size(); ++beamsIt)
	{
		// Only process points having x <= 0.
		if(bright_only_pts[beamsIt][0] > 0)
			continue;

		std::vector<float> symmetryPoint(2);
		symmetryPoint[0] = -bright_only_pts[beamsIt][0]; // Center in 0,0
		symmetryPoint[1] = -bright_only_pts[beamsIt][1]; // Center in 0,0

		int numNeighbors = kdtree.radiusSearch(symmetryPoint,
		                                       neighbourIds,
		                                       neighboursDists,
		                                       maxDist * maxDist,
		                                       10,
		                                       cv_radius_params);

		if(numNeighbors == 1)
		{
			cv::Vec2f symmetryP(symmetryPoint[0], symmetryPoint[1]);
			cv::Vec2f symmetryFound(bright_only_pts[neighbourIds[0]][0], bright_only_pts[neighbourIds[0]][1]);
			cv::Vec2f diff = symmetryP - symmetryFound;
			errorSquaredSum += (diff).dot(diff);
			neighboursFound++;
		}
		else if(numNeighbors > 1)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : More than one neighbor at symmetry test found. Should not happen!";
		}
	}
	if(options_->GetShowSymmetryProjection())
	{
		// Write debug image
		cv::Point2f offset(800, 600);
		cv::Mat feedbackImg = cv::Mat::zeros(1200, 1600, CV_8UC3);
		line(feedbackImg, cv::Point2f(0, offset.y), cv::Point2f(1600, offset.y), Constants::COLOR2);
		line(feedbackImg, cv::Point2f(offset.x, 0), cv::Point2f(offset.x, 1200), Constants::COLOR2);

		// get current vector to add new 3d points and cluster
		std::vector<LaserBeam>& beams = point_step1_copy;

		for(size_t i = 0; i < beams.size(); ++i)
		{
			int n = beams[i].number_of_points;
			// drawing color : <6: RED    <15 yellow  >= 15 green
			cv::Scalar color = (n < 15) ? ((n < 6) ? Constants::DARK_BLUE : Constants::COLOR1) : Constants::BRIGHT_GREEN;

			if(n > 1)
				cv::circle(feedbackImg, cv::Point2f(beams[i].mean_point) + offset, 2, color, 0);

			if(beams[i].is_beam && calibration_step_ < 2)
			{
				cv::circle(feedbackImg,
				           cv::Point2f(beams[i].mean_point) + offset,
				           4,
				           Constants::PURE_BLUE,
				           0);
			}
		}

		std::stringstream ss;
		ss << "C:\\temp\\proj" << counter++ << ".jpg";

		std::stringstream ss1;
		ss1 << "RMS : " << errorSquaredSum / neighboursFound << " , Symmetry matches found : " << neighboursFound;

		// CHECK
		//SFMCore::drawText(feedbackImg, rmsText, cv::Point(80, 80), SFMCore::Gray, 1.0, 2);
		cv::putText(feedbackImg,
		            ss1.str(),
		            cv::Point(80, 80),
		            cv::FONT_HERSHEY_SIMPLEX,
		            1.2,
		            Constants::GRAY,
		            1.0,
		            2);
		cv::imwrite(ss.str(), feedbackImg);
	}
	if(neighboursFound < 1)
	{
		return -1;
	}

	num_matches = neighboursFound;
	return sqrt(errorSquaredSum / neighboursFound);
}

bool Projector::RotateProjectorToBeamAndProject()
{
	if(int(GetUsedBeams().size()) < min_num_beams_)
	{
		return false;
	}

	auto projector_calibration_temp = calibration_/*->DuplicateLaser()*/;
	auto& beams                     = GetUsedBeams();

	std::vector<cv::Point2f> candidate_sym_pts;
	std::vector<std::pair<int, double>> candidate_brightness;
	std::vector<std::pair<int, double>> candidate_occurrences;
	StatisticValMedianSet<double> brightness_statistics;

	for(auto& beam : beams)
	{
		//LaserBeam& beam = beams[beamsIt];
		if(!beam.is_beam || beam.mean_point.x < search_window_x_ - float(search_window_width_ / 2.0)
			|| beam.mean_point.x > search_window_x_ + float(search_window_width_ / 2.0)
			|| beam.mean_point.y < search_window_y_ - float(search_window_height_ / 2.0)
			|| beam.mean_point.y > search_window_y_ + float(search_window_height_ / 2.0))

		{
			/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Skipping point [" << beam.mean_point.x << " "
				<< beam.mean_point.y << "] Search window size [" << search_window_width_ << " " << search_window_height_ <<
				"] Search Window [" << search_window_x_ << " " << search_window_y_ << "]";*/

			continue;
		}

		candidate_sym_pts.push_back(beam.mean_point);
		StatisticValMedianSet<double> beam_brightness;

		for(auto beam_dot_info : beam.dot_infos)
		{
			beam_brightness.add(double(beam_dot_info.brightness));
		}

		beam_brightness.finish();

		// add this beams brightness to the overall beam brightness statistics
		brightness_statistics.add(beam_brightness.medianVal);
		candidate_brightness.push_back(std::make_pair((int)candidate_sym_pts.size() - 1, beam_brightness.medianVal));
		candidate_occurrences.push_back(std::make_pair((int)candidate_sym_pts.size() - 1, (int)beam.beam_3d_points.size()));

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Beam mean point at [" << beam.mean_point.x << " "
			<< beam.mean_point.y << "] Brightness Mean [" << beam_brightness.meanVal << "] Median Value [" << beam_brightness.medianVal <<
			"] Number of points [" << beam_brightness.numPoints << "]";
	}

	std::sort(candidate_brightness.begin(), candidate_brightness.end(), PairCmp2nd);
	std::sort(candidate_occurrences.begin(), candidate_occurrences.end(), PairCmp2nd);

	/* Try to detect central beam by several criteria:
	 * 1.) No beam was missing and added in the candidates: Analyze brightness, and symmetry and try to
	 * find one different to all others
	 * 2.) Exactly one beam was missing and added in the candidates: that is the central beam !
	 * 3.) More than one beam was missing and added: clear distinction by
	 * only symmetry is requested (added points do not have brightness as distinction)
	*/

	// when the center beam index has been detected it is set here
	// any other attempt to identify it will be skipped
	auto center_beam_idx      = -1;
	auto n_before             = (int)candidate_sym_pts.size();
	auto add_candidate_result = TryFillMissingCentralBeam(candidate_sym_pts, false);
	auto n_added              = (int)candidate_sym_pts.size() - n_before;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector calibration added " << n_added << " symmetric points";

	/*for(auto& pt : candidate_sym_pts)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Candidate symmetry at [" << pt.x << " , " << pt.y << "]";
	}*/

	auto min_dist = DBL_MAX;

	if(options_->GetProjectorCalibrationCenterBeamUseWindowCenter()) // false
	{
		//auto x = options_->GetProjectorCalibrationCenterBeamWindowPositionX(); // , 0.0);
		//auto y = options_->GetProjectorCalibrationCenterBeamWindowPositionY(); // ", 0.0);
		auto x = 0.0f;
		auto y = 0.0f;

		// if one of the added coordinates is close to the expected central beam position
		// mark that a we added the center point in this step 2 and there is no real beam measurement
		auto nearest_added_pt_idx  = -1;
		auto nearest_added_pt_dist = 30.0f;

		// Find the added point nearest to the expected center beam
		for(auto i_added = 0; i_added < n_added; ++i_added)
		{
			auto idx   = (int)candidate_sym_pts.size() - 1 - i_added;
			float dist = cv::norm(candidate_sym_pts[idx] - cv::Point2f(x, y));

			if(dist < nearest_added_pt_dist)
			{
				nearest_added_pt_dist               = dist;
				nearest_added_pt_idx                = idx;
				step2_center_point_added_as_bright_ = true;
			}
		}

		// A missing beam close to the expected center beam position is a very strong
		// indicator to be the center beam.
		// if there is a beam added close to the expected center beam use it as center beam
		if(nearest_added_pt_idx > -1)
		{
			center_beam_idx = nearest_added_pt_idx;
		}
		else // use the point nearest to the expected center beam.
		{
			for(size_t candidate_idx = 0; candidate_idx < candidate_sym_pts.size(); ++candidate_idx)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector Calibration Check Candidate " << candidate_idx << " at [" <<
 candidate_sym_pts[candidate_idx].x << " " <<
 candidate_sym_pts[candidate_idx].y << "]";

				auto d = cv::norm(candidate_sym_pts[candidate_idx] - cv::Point2f(x, y));

				if(d < min_dist)
				{
					center_beam_idx = int(candidate_idx);
					min_dist        = d;
				}
			}
		}

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector Calibration Debug set center beam to [" << candidate_sym_pts[center_beam_idx].x <<
			" " << candidate_sym_pts[center_beam_idx].y << "]";
	}

	// centerBeamIndex not yet identified and no dots have been added by filling missing dots
	// Do statistics about symmetry, brightness and number of appearances -> score value
#pragma region NO_POINTS_ADDED__DO_STATISTICS
	if((center_beam_idx < 0) && (n_added == 0))
	{
		std::map<int, CentralBeamScore> central_beam_scores;

		// Statistics over beam brightness
		StatisticValSet<double> stats_all;

		for(size_t i = 1; i < candidate_brightness.size(); ++i)
			stats_all.add(candidate_brightness[i].second);

		stats_all.finish();

		double sum_rel_dists = 0;

		for(size_t i = 0; i < candidate_brightness.size(); ++i)
		{
			double dist       = fabs(candidate_brightness[i].second - stats_all.meanVal);
			double relRmsDist = dist / stats_all.deviation;
			sum_rel_dists += relRmsDist;
			int idx                             = candidate_brightness[i].first;
			central_beam_scores[idx].brightness = relRmsDist;
		}

		// normalize probabilities
		for(size_t i = 0; i < candidate_brightness.size(); ++i)
		{
			int idx = candidate_brightness[i].first;
			central_beam_scores[idx].brightness /= sum_rel_dists;
			central_beam_scores[idx].brightness *= (double)candidate_brightness.size();

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : idx " << idx << " x : " << candidate_sym_pts[idx].x << " y: " << candidate_sym_pts[idx].y
			<< " brightness : " << candidate_brightness[i].second << " relative dist : " << central_beam_scores[idx].brightness;
		}

		// statistics over number of beam detections
		{
			StatisticValSet<double> stats_all_2;

			for(size_t i = 1; i < candidate_occurrences.size(); ++i)
				stats_all_2.add(candidate_occurrences[i].second);

			stats_all_2.finish();

			double sum_rel_dists_2 = 0;
			for(size_t i = 0; i < candidate_occurrences.size(); ++i)
			{
				double dist = stats_all_2.meanVal / candidate_occurrences[i].second;

				if(dist < 1)
					dist = 1. / dist;

				sum_rel_dists += dist;
				int idx                              = candidate_occurrences[i].first;
				central_beam_scores[idx].num_matches = dist;
			}

			// normalize probabilities
			for(size_t i = 0; i < candidate_occurrences.size(); ++i)
			{
				int idx = candidate_occurrences[i].first;
				central_beam_scores[idx].num_matches /= sum_rel_dists;
				central_beam_scores[idx].num_matches *= (double)candidate_occurrences.size();
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Index [" << idx << "] Point [" <<
					candidate_sym_pts[idx].x << " " <<
					candidate_sym_pts[idx].y <<
					"] Occurrences [" << candidate_occurrences[i].second <<
					"] Relative occurrence [" << central_beam_scores[idx].num_matches << "]";
			}
		}

		// statistics over symmetry check
		{
			double sum_matches = 0;

			for(size_t candidate_idx = 0; candidate_idx < candidate_sym_pts.size(); ++candidate_idx)
			{
				auto proj_cal_tmp_thread                         = projector_calibration_temp/*->DuplicateLaser()*/;
				auto num_matches                                 = 0;
				auto& candidate                                  = candidate_sym_pts[candidate_idx];
				auto rms                                         = GetPointSymmetryRmsByCoordinate(proj_cal_tmp_thread, candidate, num_matches);
				central_beam_scores[(int)candidate_idx].symmetry = double(num_matches);
				sum_matches += (double)num_matches;
			}

			// normalize probabilities
			for(size_t candidate_idx = 0; candidate_idx < candidate_sym_pts.size(); ++candidate_idx)
			{
				auto& candidate = candidate_sym_pts[candidate_idx];
				auto num_match  = (int)central_beam_scores[(int)candidate_idx].symmetry;
				central_beam_scores[(int)candidate_idx].symmetry /= sum_matches;
				central_beam_scores[(int)candidate_idx].symmetry *= (double)candidate_sym_pts.size();

				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Candidate Idx [" << candidate_idx << "] Symmetry check [" << candidate.x << " " <<
 candidate.y << "] Matches ["
 << num_match << "] Relative Symmetry Matches [" << central_beam_scores[int(candidate_idx)].symmetry << "]";
			}
		}

		// combine all checks overall
		std::vector<std::pair<int, double>> overall_scores;
		double sum_score = 0.0;
		for(size_t candidate_idx = 0; candidate_idx < candidate_sym_pts.size(); ++candidate_idx)
		{
			auto score         = central_beam_scores[(int)candidate_idx];
			auto overall_score = score.brightness * score.brightness * +score.num_matches * score.num_matches + score.symmetry * score.symmetry;
			sum_score += overall_score;
			overall_scores.push_back(std::make_pair((int)candidate_idx, overall_score));
		}
		std::sort(overall_scores.begin(), overall_scores.end(), PairCmp2nd);

		for(size_t i = 0; i < overall_scores.size(); ++i)
		{
			auto idx = overall_scores[i].first;
			overall_scores[i].second /= sum_score;
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Score Idx [" << idx << "] Sym Points [" << candidate_sym_pts[idx].x << " "
			<< candidate_sym_pts[idx].y << "] Overall Score [" << overall_scores[i].second << "]";
		}

		auto best = (int)overall_scores.size() - 1;

		if(overall_scores[best].second > overall_scores[best - 1].second * 1.33)
		{
			center_beam_idx = overall_scores[best].first;
		}
	}

#pragma endregion

	std::vector<SymRms> candidate_to_rms(candidate_sym_pts.size());
	// candidate added but not yet save that it was the center beam

#pragma region TRY_WITH_SYMMETRY
	if(center_beam_idx < 0 && add_candidate_result != 2)
	{
		auto test_ok = true;

		// Get beam with 2nd min rms
		int max_matches_2nd                 = 0;
		int max_matches_candidate_index2_nd = -1;

		// Get beam with max matches
		int max_matches                 = 0;
		int max_matches_candidate_index = -1;

		for(size_t candidate_idx = 0; candidate_idx < candidate_sym_pts.size(); ++candidate_idx)
		{
			auto proj_cal_tmp_thread = projector_calibration_temp/*->DuplicateLaser()*/;
			auto num_matches         = 0;
			auto& candidate          = candidate_sym_pts[candidate_idx];
			auto rms                 = GetPointSymmetryRmsByCoordinate(proj_cal_tmp_thread, candidate, num_matches);

			candidate_to_rms[candidate_idx].index       = (int)candidate_idx;
			candidate_to_rms[candidate_idx].rms         = rms;
			candidate_to_rms[candidate_idx].num_matches = num_matches;

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : New symmetry candidate index [" << candidate_idx
			<< "] Candidate [" << candidate.x << " " << candidate.y << "] RMS [ " << rms << "] Matches [" << num_matches << "]";

			if(num_matches >= max_matches)
			{
				max_matches_2nd                 = max_matches;
				max_matches_candidate_index2_nd = max_matches_candidate_index;
				max_matches                     = num_matches;
				max_matches_candidate_index     = (int)candidate_idx;
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : New symmetry candidate index [" << candidate_idx
					<< "] Candidate [" << candidate.x << " " << candidate.y << "] RMS [ " << rms << "] Matches [" << num_matches << "]";
			}
		}

		// Reporting
		double min_ratio              = 2.0;
		double best_to_2nd_best_ratio = (float)max_matches / (float)max_matches_2nd;

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Num matches best beam is [" << max_matches
		<< "], Num matches 2nd best is [" << max_matches_2nd << "]. Ratio is [" << best_to_2nd_best_ratio << "]";

		//if(addCandidateResult == 2)
		//VgCalibrationReport::trySet(L"CenterBeamIdentification", LS::String(L"By missing beam, symmetry checks will be ignored"));
		//else
		//VgCalibrationReport::trySet(L"CenterBeamIdentification", LS::String(L"By symmetry check as following:"));

		//VgCalibrationReport::trySet(L"Step2NumSymmetriesBest", maxMatches);
		//VgCalibrationReport::trySet(L"Step2RmsSymmetriesBest", candToRms[maxMatchesCandidateIndex].rms);
		//VgCalibrationReport::trySet(L"Step2NumSymmetries2ndBest", maxMatches2nd);
		//VgCalibrationReport::trySet(L"Step2RmsSymmetries2ndBest", candToRms[maxMatchesCandidateIndex].rms);
		//VgCalibrationReport::trySet(L"Step2CentralDotPosX", (double)candidateSymPts[maxMatchesCandidateIndex].x);
		//VgCalibrationReport::trySet(L"Step2CentralDotPosY", (double)candidateSymPts[maxMatchesCandidateIndex].y);
		//VgCalibrationReport::trySet(L"Step2BestTo2ndRatio", bestTo2ndBestRatio);
		//VgCalibrationReport::trySet(L"Step2BestTo2ndRatioMin", minRatio);

		//TODO  CHECK THIS
		if(!calibration_output_path_.empty())
		{
			std::shared_ptr<const fs::path> outputDir = std::make_shared<fs::path>(calibration_output_path_);
			fs::path pointsLeftFilename               = calibration_output_path_ + "\\symmetryRmsDistribution.csv";

			std::ofstream outLeft(pointsLeftFilename.string());

			outLeft << "beamId;x;y;rms;matches" << std::endl;

			for(size_t dotIt = 0; dotIt < candidate_to_rms.size(); ++dotIt)
			{
				int iBeam      = candidate_to_rms[dotIt].index;
				int numMatches = candidate_to_rms[dotIt].num_matches;
				double rmsBeam = candidate_to_rms[dotIt].rms;
				if(rmsBeam < 0)
					continue;

				std::stringstream line;
				line << iBeam << ";" <<
					beams[iBeam].mean_point.x << ";" <<
					beams[iBeam].mean_point.y << ";" <<
					rmsBeam << ";" <<
					numMatches;

				//TODO CHECK THIS
				//line.replace(L".", L",");
				outLeft << line.str() << std::endl;
			}

			outLeft.close();
		}

		if((max_matches_candidate_index < 0 || max_matches < 80) && add_candidate_result < 2)
		{
			BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Beam index is [" << max_matches_candidate_index << "]. Number of symmetry matches is ["
 <<
 max_matches << "] Error!";
			test_ok = false;
		}

		if((best_to_2nd_best_ratio < min_ratio) && add_candidate_result < 2)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Ratio [" << max_matches << "/" << max_matches_2nd << "] = " << best_to_2nd_best_ratio <<
 " smaller than [" << min_ratio << "]. ";
			test_ok = false;
		}

		if(test_ok && add_candidate_result < 2)
		{
			center_beam_idx = max_matches_candidate_index;

			// if the new center did not exist before, we need to add (0,0) to the beams for cross detection
			BOOST_LOG_TRIVIAL(info) << "() : step2_center_point_added_as_bright_ before" << step2_center_point_added_as_bright_;
			step2_center_point_added_as_bright_ = max_matches_candidate_index >= n_before;
			BOOST_LOG_TRIVIAL(info) << "() : step2_center_point_added_as_bright_ after" << step2_center_point_added_as_bright_;
		}
	} // end try detect central beam with symmetry criterion
#pragma endregion // try with symmetry

#pragma region USE_UNIQUE_FILL_POINT
	// 2: A beam has been added which was already identified as center beam
	if(center_beam_idx < 0 && add_candidate_result == 2)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector calibration debug use unique fill point";
		BOOST_LOG_TRIVIAL(info) << "() : step2_center_point_added_as_bright_ before" << step2_center_point_added_as_bright_;
		step2_center_point_added_as_bright_ = true;
		BOOST_LOG_TRIVIAL(info) << "() : step2_center_point_added_as_bright_ after" << step2_center_point_added_as_bright_;
		center_beam_idx = (int)candidate_sym_pts.size() - 1;
	}
#pragma endregion

	if(center_beam_idx < 0)
	{
		return false;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Debug Projector calibration rotated to [" <<
		candidate_sym_pts[center_beam_idx].x << " " <<
		candidate_sym_pts[center_beam_idx].y << "]";

	RotateProjectorToCenter(candidate_sym_pts[center_beam_idx]);
	UpdateProjections(beams);
	central_beam_ = candidate_sym_pts[center_beam_idx];

	return true;
}

// Calculate the projector position
// by intersecting all rays, clearing the beams vector and adding all
// 3D points to new beams again.
bool Projector::Step1Done()
{
	auto& beams = GetUsedBeams();

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : get used beam size " << beams.size() << " m_minNumBeams " << min_num_beams_ << " m_beams.size "
 <<
 stable_beams_indexes_.size();

	if(beams.size() < min_num_beams_)
		return false;

	green_points_ = int(stable_beams_indexes_.size());

	if(stable_beams_indexes_.size() < min_num_beams_)
		return false;

	UpdateProjectionsAndBeams(beams);

	return true;
}

bool Projector::Step2Done()
{
	std::vector<LaserBeam>& beams = GetUsedBeams();

	if(beams.size() < min_num_beams_)
		return false;

	green_points_      = (int)stable_beams_indexes_.size();
	const auto success = use_dmvs_projector_calibration_ ? RotateProjectorToBeamAndProjectDMVS() : RotateProjectorToBeamAndProject();

	if(success)
	{
		UpdateProjectionsAndBeams(beams);
	}

	return success;
}

bool Projector::Step3Done()
{
	auto& beams   = GetUsedBeams();
	green_points_ = int(stable_beams_indexes_.size());

	if(stable_beams_indexes_.size() < min_num_beams_)
	{
		return false;
	}

	auto contains_center_beam = false;
	std::vector<cv::Vec2f> bright_only_points(stable_beams_indexes_.size());

	for(size_t i = 0; i < stable_beams_indexes_.size(); ++i)
	{
		bright_only_points[i] = beams[stable_beams_indexes_[i]].mean_point;

		if(cv::norm(bright_only_points[i]) < 5.0)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector calibration contains center beam " << bright_only_points[i];
			contains_center_beam = true;
		}

		//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Beam added to beams list. x,y = " << beams[stable_beams_indexes_[i]].mean_point.x << " "
		//<< beams[stable_beams_indexes_[i]].mean_point.y;
	}

	// if step 2 added the center beam and there is no real measured center
	// beam meanwhile just add (0,0) as center beam
	/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : contains_center_beam " << contains_center_beam << Constants::CHECK_ME;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : step2_center_point_added_as_bright_ " << step2_center_point_added_as_bright_ <<
 Constants::CHECK_ME;*/

	if(!contains_center_beam)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Add (0,0) as center beam";
		bright_only_points.emplace_back(0, 0);
	}

	//set all bright laser beam points
	rotation_detector_step1_.SetPoints(bright_only_points);
	const auto expansions_count = rotation_detector_step1_.ExpandCross(cv::Vec2f(0, 0));

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector calibration expansions count " << expansions_count;

	// Exactly 6 points into every direction (up, down, left, right)
	if(expansions_count >= 4 * Constants::NEIGHBOR_POINTS_IN_EACH_DIRECTION)
	{
		if(!rotation_detector_step1_.CrossImage().empty())
		{
			cross_hair_image_ = rotation_detector_step1_.CrossImage();
		}

		// set all bright laser beam points
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector calibration start rotation calculation transform ";

		rotation_detector_step1_.CalculateTransformation();
		rotation_detector_step1_.GetAlignedDOE().copyTo(aligned_doe_);
		GetUndistortDOE(aligned_doe_, bright_only_points);
		return true;
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector calibration skip as expansion count is not greater than 28 ";
		return false;
	}
}

bool Projector::Step4Done()
{
	auto& beams = GetUsedBeams();

	// Count good beams
	green_points_ = 0;

	for(int beamsIt = 0; beamsIt < beams.size(); ++beamsIt)
	{
		if(beams[beamsIt].number_of_points >= 15)
			green_points_++;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Detected " << green_points_ << " laser dots ";

	// Total points of DOE pattern=11665
	if(green_points_ >= GetMinDotNumberToFinish() && next_step_forced_)
	{
		next_step_forced_ = false;

		// Try to detect central beam with more accuracy
		if(use_dmvs_projector_calibration_)
		{
			RetryCentralBeam();
		}

		std::vector<cv::Point2f> measured_points2d;
		std::vector<int> doe_indices;

		for(int i = 0; i < int(beams.size()); ++i)
		{
			if(beams[i].beam_3d_points.size() > 1)
			{
				std::vector<cv::Point2f> projected_points2d;
				calibration_->LaserProjectPoints(beams[i].beam_3d_points, projected_points2d);

				cv::Point2f mean_measured2d(0.0, 0.0);

				// pts3d[0] is initial value from DOE data in this calibration step
				// and has to be ignored for new mean value
				for(size_t idx = 1; idx < projected_points2d.size(); ++idx)
				{
					mean_measured2d += projected_points2d[idx];
				}

				auto mean_measured_point(mean_measured2d * (1.0f / float(projected_points2d.size() - 1)));
				measured_points2d.push_back(mean_measured_point);
				doe_indices.push_back(i);
			}
		}

		std::string log_path = R"(D:\repos\BitBucket\farolabs\calibration-tools\CalibrationTools\DMVS011900031\20210426-101158\Logs)";

		std::ofstream aligned_doe_stream;
		auto aligned_doe_path = log_path + R"(\aligned_doe.txt)";
		aligned_doe_stream.open(aligned_doe_path);

		if(aligned_doe_stream.is_open())
		{
			aligned_doe_stream << aligned_doe_;
		}

		std::ofstream measured_points2d_stream;
		auto measured_point2d_stream_path = log_path + R"(\meaPts2d.txt)";
		measured_points2d_stream.open(measured_point2d_stream_path);

		if(measured_points2d_stream.is_open())
		{
			for(auto measured_points2d : measured_points2d)
			{
				measured_points2d_stream << measured_points2d.x << " " << measured_points2d.y << '\n';
			}
		}

		std::ofstream doe_indices_stream;
		auto doe_indices_stream_path = log_path + R"(\doeIndices.txt)";
		doe_indices_stream.open(doe_indices_stream_path);

		if(doe_indices_stream.is_open())
		{
			for(auto doe_index : doe_indices)
			{
				doe_indices_stream << doe_index << '\n';
			}
		}

		aligned_doe_stream.close();
		doe_indices_stream.close();
		measured_points2d_stream.close();

		ApplyParaboloidCompensation(aligned_doe_, measured_points2d, doe_indices);

		std::ofstream aligned_doe_stream_corrected;
		auto aligned_doe_corrected_path = log_path + R"(\aligned_doe_corrected.txt)";
		aligned_doe_stream_corrected.open(aligned_doe_corrected_path);

		if(aligned_doe_stream_corrected.is_open())
		{
			aligned_doe_stream_corrected << aligned_doe_;
		}

		aligned_doe_stream_corrected.close();

		for(size_t j = 0; j < doe_indices.size(); ++j)
		{
			if(doe_indices[j] < aligned_doe_.rows)
			{
				aligned_doe_.at<double>(doe_indices[j], 1) = measured_points2d[j].x;
				aligned_doe_.at<double>(doe_indices[j], 2) = measured_points2d[j].y;
			}
		}

		std::ofstream aligned_doe_stream_corrected2;
		auto aligned_doe_corrected_path2 = log_path + R"(\aligned_doe_corrected2.txt)";
		aligned_doe_stream_corrected2.open(aligned_doe_corrected_path2);

		if(aligned_doe_stream_corrected2.is_open())
		{
			for(auto row = 0; row < aligned_doe_.rows; row++)
			{
				for(auto col = 0; col < aligned_doe_.cols; col++)
				{
					aligned_doe_stream_corrected2 << std::to_string(aligned_doe_.at<double>(row, col)) + " ";
				}
			}
		}

		aligned_doe_stream_corrected.close();
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Enough laser dots detected! GreenDotsCount = " << green_points_;

		return true;
	}

	return false;
}

void Projector::RetryCentralBeam()
{
	std::vector<LaserBeam>& beams = GetUsedBeams();
	std::vector<LaserBeam> m_green_beams;
	m_green_beams.reserve(beams.size());
	for(int beamsIt = 0; beamsIt < beams.size(); ++beamsIt)
	{
		if(beams[beamsIt].number_of_points >= 15)
			m_green_beams.push_back(beams[beamsIt]);
	}
	// Search if we have point close to estimated center beam close means closer than 1.5 pixel here
	// Create the Index
	cv::Mat samplesDOE((int)m_green_beams.size(), 2, CV_32F);
	for(int i = 0; i < (int)m_green_beams.size(); ++i)
	{
		samplesDOE.at<float>(i, 0) = m_green_beams[i].mean_point.x;
		samplesDOE.at<float>(i, 1) = m_green_beams[i].mean_point.y;
	}

	cv::flann::KDTreeIndexParams indexParams(1);
	cv::flann::Index kdtreeDOE(samplesDOE, indexParams);
	cv::flann::SearchParams cv_radius_params(-1, 0.0f, false);
	double radius = 1.5; // Distance between two bright points in x/y direction is 30 px.
	// find index of start points in origin
	std::vector<float> q(2);
	q[0] = 0.0;
	q[1] = 0.0;

	std::vector<int> idxDOE;
	std::vector<float> distsDOE;
	int numNeighbors =
		kdtreeDOE.radiusSearch(q, idxDOE, distsDOE, radius * radius, 100, cv_radius_params);
	if(numNeighbors == 1)
	{
		central_beam_   = m_green_beams[idxDOE[0]].mean_point;
		auto projCalTmp = calibration_;

		cv::Mat rotation_matrix, translation_vector;
		projCalTmp->GetExtrinsicParameters(rotation_matrix, translation_vector);
		// We need a Relative transformation
		RotateProjectorToCenter(central_beam_, nullptr, rotation_matrix);
		UpdateProjections(beams);
	}
}

bool Projector::Step100Done()
{
	std::vector<LaserBeam>& beams = GetUsedBeams();
	if(next_step_forced_)
	{
		std::vector<cv::Vec2f> pts;

		// 		for ( size_t i=0 ; i< beams.size(); i++ )
		// 		{
		// 			if (beams[i].maxZ3d < 1.0 || beams[i].num < 15 )
		// 				continue;
		// 			pts.push_back( beams[i].meanPt );
		// 		}
		//
		// 		SFM::Tools::PatternRotationDetector rotDetect ;
		// 		rotDetect.loadOriginalPattern( "doe.yml");
		// 		rotDetect.setPoints( pts );
		//  		int numExpansions = rotDetect.expandCross( cv::Vec2f(0,0) );
		// 		rotDetect.calcTrafo();
		// 		rotDetect.getAlignedDOE().copyTo( m_alignedDOE );

		//	cv::Point2f offset( 800, 600 );
		// 		cv::Mat img = cv::Mat::zeros(1200,1600, CV_8U );
		//
		// 		for ( size_t i=0; i< pts.size(); i++ )
		// 		{
		// 			circle( img, cv::Point2f( pts[i] ) + offset , 4.0 , cv::Scalar( 255 ) );
		// 		}
		//
		// 		for (int i=0; i<4; i++ )
		// 		{
		// 			std::vector< int > dots = rotDetect.getExpandedPts( i );
		// 			for (int j=1; j<dots.size(); j++ )
		// 			{
		// 				cv::Point2f from( pts[ dots[j-1] ] );
		// 				cv::Point2f to( pts[ dots[j] ] );
		// 				cv::line( img,from + offset, to + offset, cv::Scalar(255) );
		// 			}
		// 		}

		next_step_forced_ = false;
		return true;
	}

	return false;
}

bool Projector::Step101Done()
{
	std::vector<LaserBeam>& beams = GetUsedBeams();

	if(next_step_forced_)
	{
		next_step_forced_ = false;
		cv::Point3f oldPos;
		cv::Mat rotation_matrix, translation_vector;
		calibration_->GetExtrinsicParameters(rotation_matrix, translation_vector);
		oldPos.x = (float)translation_vector.at<double>(0, 0);
		oldPos.y = (float)translation_vector.at<double>(1, 0);
		oldPos.z = (float)translation_vector.at<double>(2, 0);
		calibration_->SetExtrinsicParameters(rotation_matrix, translation_vector);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : old pos " << oldPos.x << " " << oldPos.y << " " << oldPos.z;

		std::vector<LaserBeam>& beams = GetUsedBeams();

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  used " << beams.size() << " beams ";

		cv::Point3f newPos;
		CalculateProjectorPosition(newPos, beams);

		calibration_->GetExtrinsicParameters(rotation_matrix, translation_vector);

		translation_vector.at<double>(0, 0) = newPos.x;
		translation_vector.at<double>(1, 0) = newPos.y;
		translation_vector.at<double>(2, 0) = newPos.z;

		calibration_->SetExtrinsicParameters(rotation_matrix, translation_vector);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  position correction " << cv::norm(oldPos - newPos);

		// update the 2d projections because projector position has been changed
		UpdateProjections(beams, calibration_);

		std::vector<cv::Point2f> meaPts2d;
		std::vector<int> doeIndices;

		for(size_t i = 0; i < beams.size(); ++i)
		{
			if(beams[i].beam_3d_points.size() > 1)
			{
				std::vector<cv::Point2f> proj2d;
				calibration_->LaserProjectPoints(beams[i].beam_3d_points, proj2d);

				cv::Point2f meanMeasured2d(0.0, 0.0);
				// pts3d[0] is initial value from DOE data in this calibration step
				// and has to be ignored for new mean value
				for(size_t iPts2d = 1; iPts2d < proj2d.size(); ++iPts2d)
				{
					meanMeasured2d += proj2d[iPts2d];
				}

				cv::Point2f p(meanMeasured2d * (1.0f / (float)(proj2d.size() - 1)));
				meaPts2d.push_back(p);
				doeIndices.push_back((int)i);
			}
		}

		ApplyParaboloidCompensation(aligned_doe_, meaPts2d, doeIndices);

		// BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  update doe matrix with %d rows", m_alignedDOE.rows );
		for(size_t i = 0; i < doeIndices.size(); ++i)
		{
			if(doeIndices[i] < aligned_doe_.rows)
			{
				aligned_doe_.at<double>(doeIndices[i], 1) = meaPts2d[i].x;
				aligned_doe_.at<double>(doeIndices[i], 2) = meaPts2d[i].y;
			}
		}

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  Laser position correction: %e mm", cv::norm(oldPos - newPos) * 1000.0;
		return true;
	}

	return false;
}

int Projector::GetLaserDotMatrix(cv::Mat& laser_dots) const
{
	aligned_doe_.copyTo(laser_dots);
	return aligned_doe_.rows;
}

int Projector::UpdateLaserDotMatrix(cv::Mat& laser_dots)
{
	if(laser_dots.rows != laser_beams_step2_.size())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() :  Cannot update laser beam matrix, different sizes!";
		return 0;
	}

	auto num_updated_beams = 0;

	for(int i = 0; i < int(laser_beams_step2_.size()); ++i)
	{
		if(laser_beams_step2_[i].number_of_points > 5)
		{
			laser_dots.at<double>(i, 1) = double(laser_beams_step2_[i].mean_point.x);
			laser_dots.at<double>(i, 2) = double(laser_beams_step2_[i].mean_point.y);
			num_updated_beams++;
		}
	}

	return num_updated_beams;
}

void Projector::InitCalibration()
{
	InitStep1();
}

void Projector::InitRecalibration(const cv::Mat& aligned_doe)
{
	aligned_doe.copyTo(aligned_doe_);

	LaserBeam lb;
	lb.is_beam                   = false;
	lb.farthest_3d_point_to_beam = 0.0;
	lb.nearest_3d_point_to_beam  = FLT_MAX;
	lb.number_of_points          = 0;
	lb.mean_point                = cv::Point2f(0.0f, 0.0f);
	calibration_step_            = 101;

	InitStep101();
	auto& used_beams = GetUsedBeams();
	used_beams.clear();
	used_beams.resize(aligned_doe_.rows, lb);
}

int Projector::RotateProjectorToCenter(cv::Point2f& zero_order_beam,
                                       std::shared_ptr<CameraSensor> calibration,
                                       cv::Mat rotation_matrix) const
{
	if(!calibration)
	{
		calibration = calibration_;
	}

	if(!calibration)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration object is NULL";
		return -1;
	}

	cv::Mat rotation_mat, translation_vec;
	calibration->GetExtrinsicParameters(rotation_mat, translation_vec);
	/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : rotation_mat " << rotation_mat;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : translation_vec " << translation_vec;*/

	Eigen::Vector3d orig_y(0.0, 1.0, 0.0);

	// get the focal length
	cv::Mat camera, distortion;
	calibration->GetBestFitCVModelLaser(camera, distortion);
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : camera " << camera;
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : distortion " << distortion;

	auto fl = (camera.at<double>(0, 0) + camera.at<double>(1, 1)) / 2.0;

	Eigen::Matrix3d rot;
	Eigen::Vector3d z(zero_order_beam.x, zero_order_beam.y, fl);
	z.normalize();

	auto x = orig_y.cross(z);
	x.normalize();

	auto y = z.cross(x);
	y.normalize();

	rot.row(0) = x;
	rot.row(1) = y;
	rot.row(2) = z;

	cv::Mat rotation_correction(3, 3, CV_64F);
	cv::Mat rotation_new(3, 3, CV_64F);
	//cv::Mat r_new2(3, 3, CV_64F);
	//cv::eigen2cv(rot, r_new2);

	for(auto i = 0; i < 3; ++i)
	{
		for(auto j = 0; j < 3; ++j)
		{
			rotation_correction.at<double>(i, j) = rot(i, j);
		}
	}

	rotation_new = rotation_matrix * rotation_correction;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Original rotation matrix " << rotation_matrix;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Rotation correction " << rotation_correction;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : After rotation correction " << rotation_new;

	calibration->SetExtrinsicParameters(rotation_new, translation_vec);

	return 0;
}

int Projector::EstimateCenterByCentralBeam(cv::Point3f& pos,
                                           const std::vector<cv::Point3f>& beam_pts) const
{
	if(beam_pts.size() < 2)
	{
		return -1;
	}

	// calculate line
	cv::Mat l;
	cv::fitLine(beam_pts, l, cv::DIST_HUBER, 0.5, 0.001, 0.001);
	cv::Vec3f lineDir(l.at<float>(0, 0), l.at<float>(1, 0), l.at<float>(2, 0));
	cv::Vec3f linePos(l.at<float>(3, 0), l.at<float>(4, 0), l.at<float>(5, 0));

	// get scalar for line at z = pos.z
	const double scale = (pos.z - linePos[2]) / lineDir[2];
	auto p             = linePos + scale * lineDir;

	cv::Mat pM(3, 1, CV_64F);
	pM.at<double>(0, 0) = p[0];
	pM.at<double>(1, 0) = p[1];
	pM.at<double>(2, 0) = p[2];

	// cv::transpose( getProjCal().R, Rt );
	cv::Mat rotation_matrix, translation_vector;
	calibration_->GetExtrinsicParameters(rotation_matrix, translation_vector);
	cv::Mat pp = -(rotation_matrix * pM);
	pos.x      = (float)pp.at<double>(0, 0);
	pos.y      = (float)pp.at<double>(1, 0);
	pos.z      = (float)pp.at<double>(2, 0);

	return 0;
}

std::shared_ptr<CameraSensor> Projector::GetProjectorCalibration() const
{
	return calibration_;
}

std::vector<Projector::LaserBeam>& Projector::GetUsedBeams()
{
	switch(calibration_step_)
	{
		// 3 steps initial calibration
		// case 0: return beams;
		// case 1: return m_ptsStep1;
		// case 2: return m_ptsStep2;

		// 2 steps recalibration
	case 100:
		return laser_beams_step0_;

	case 101:
		return laser_beams_step2_;

	default:
		return laser_beams_step0_;
	}
}

void Projector::ApplyParaboloidCompensation(cv::Mat& doe,
                                            std::vector<cv::Point2f>& pts2d,
                                            std::vector<int>& doe_correspondences)
{
	if(pts2d.size() != doe_correspondences.size())
	{
		BOOST_LOG_TRIVIAL(error) << __FUNCTION__ << "() : Index vector and point vector sizes do not match";
		return;
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Paraboloid compensation with " << pts2d.size() << " correspondences";

	Eigen::Matrix<double, Eigen::Dynamic, 6> A;
	Eigen::Matrix<double, Eigen::Dynamic, 1> b0, b1;

	A.resize(pts2d.size(), 6);
	b0.resize(pts2d.size(), 1);
	b1.resize(pts2d.size(), 1);

	for(size_t i = 0; i < pts2d.size(); ++i)
	{
		double x = doe.at<double>(doe_correspondences[i], 1);
		double y = doe.at<double>(doe_correspondences[i], 2);

		A(i, 0) = x * x;
		A(i, 1) = y * y;
		A(i, 2) = x;
		A(i, 3) = y;
		A(i, 4) = x * y;
		A(i, 5) = 1;

		b0(i, 0) = pts2d[i].x - x; // difference measured to DOE in X
		b1(i, 0) = pts2d[i].y - y; // difference measured to DOE in Y
	}

	Eigen::MatrixXd p0 = A.colPivHouseholderQr().solve(b0);
	Eigen::MatrixXd p1 = A.colPivHouseholderQr().solve(b1);

	for(int i = 0; i < doe.rows; ++i)
	{
		double x = doe.at<double>(i, 1);
		double y = doe.at<double>(i, 2);

		const auto correction_x = p0(0, 0) * x * x +
			p0(1, 0) * y * y +
			p0(2, 0) * x +
			p0(3, 0) * y +
			p0(4, 0) * x * y +
			p0(5, 0);

		const auto correction_y = p1(0, 0) * x * x +
			p1(1, 0) * y * y +
			p1(2, 0) * x +
			p1(3, 0) * y +
			p1(4, 0) * x * y +
			p1(5, 0);

		// Calc rms values
		doe_correction_rms_x_ += correction_x * correction_x;
		doe_correction_rms_y_ += correction_y * correction_y;

		doe.at<double>(i, 1) += correction_x;
		doe.at<double>(i, 2) += correction_y;
	}

	doe_correction_rms_x_ = sqrt(doe_correction_rms_x_ / doe.rows);
	doe_correction_rms_y_ = sqrt(doe_correction_rms_y_ / doe.rows);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : DOE paraboloid Correction RMS (pixel) x,y " << doe_correction_rms_x_ << " " <<
 doe_correction_rms_y_;
}

void Projector::GetUndistortDOE(cv::Mat aligned_doe,
                                std::vector<cv::Vec2f>& pts)
{
	// get bright dots only  (coordinates and indices)
	std::vector<cv::Vec2f> doe_bright;
	std::vector<int> doe_bright_indices;

	for(int i = 0; i < aligned_doe.rows; ++i)
	{
		if(aligned_doe.at<double>(i, 0) > 0.5)
		{
			doe_bright.push_back(cv::Vec2f((float)aligned_doe.at<double>(i, 1), (float)aligned_doe.at<double>(i, 2)));
			doe_bright_indices.push_back(i);
		}
	}

	// create kd tree for neighbor search in bright DOE dots
	// ------------------------------------------------
	cv::Mat samples_doe((int)doe_bright.size(), 2, CV_32F);
	for(int i = 0; i < (int)doe_bright.size(); ++i)
	{
		samples_doe.at<float>(i, 0) = doe_bright[i][0];
		samples_doe.at<float>(i, 1) = doe_bright[i][1];
	}

	// create kd tree for neighbor search in bright measured bright dots
	// -------------------------------------------------------------
	cv::Mat samples_mea((int)pts.size(), 2, CV_32F);
	for(int i = 0; i < (int)pts.size(); ++i)
	{
		samples_mea.at<float>(i, 0) = pts[i][0];
		samples_mea.at<float>(i, 1) = pts[i][1];
	}

	// Create the Index
	cv::flann::KDTreeIndexParams indexParams(1);
	cv::flann::Index kdtreeDOE(samples_doe, indexParams);
	cv::flann::Index kdtreeMEA(samples_mea, indexParams);
	cv::flann::SearchParams cv_radius_params(-1, 0.0f, false);

	// std::vector < cv::Point2f >  ptsDOE;
	// std::vector < cv::Point2f >  correction;
	std::vector<cv::Point2f> pts_mea;
	std::vector<int> doe_indices;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Find neighbors for " << pts.size() << " bright points";

	// Distance between two bright points in x/y direction is 30 px.
	// Should include dark points if they accidentally were detected.

	double radius = 10.0;

	for(size_t i = 0; i < pts.size(); ++i)
	{
		// find index of start points in origin
		std::vector<float> q(2);
		q[0] = pts[i][0];
		q[1] = pts[i][1];
		std::vector<int> idx_doe;     // (1000);
		std::vector<float> dists_doe; //(1000);
		auto num_neighbors = kdtreeDOE.radiusSearch(q, idx_doe, dists_doe, radius * radius, 100, cv_radius_params);

		// only take unambiguous correlations
		if(num_neighbors != 1) // No dark points should be near the bright points/should have been detected!
			continue;

		// check unambiguity also in the other direction
		std::vector<int> idx_mea;     // (1000);
		std::vector<float> dists_mea; //(1000);
		q[0] = doe_bright[idx_doe[0]][0];
		q[1] = doe_bright[idx_doe[0]][1];

		num_neighbors =
			kdtreeMEA.radiusSearch(q, idx_mea, dists_mea, radius * radius, 100, cv_radius_params);

		if(num_neighbors != 1)
		{
			continue;
		}

		doe_indices.push_back(doe_bright_indices[idx_doe[0]]);
		pts_mea.push_back(cv::Point2f(pts[i][0], pts[i][1]));
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << pts_mea.size() <<
 " bright points could correctly be matched to DOE points in current frame";

	ApplyParaboloidCompensation(aligned_doe, pts_mea, doe_indices);
}

void Projector::AddPointsToBeams(const std::vector<cv::Point3f>& points,
                                 const std::vector<DotInfoType>& infos,
                                 std::vector<LaserBeam>& beams,
                                 std::shared_ptr<CameraSensor> calibration /*= 0 */)
{
	if(!calibration)
	{
		calibration = calibration_;
	}

	if(!calibration)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calibration object is NULL";
		return;
	}

	if(points.empty())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Points vector is empty";
		return;
	}

	// project 3d points into projector image
	std::vector<cv::Point2f> projected_points;
	calibration->LaserProjectPoints(points, projected_points, true, true);

	auto good_points_added                                = 0;
	const auto max_beam_point_projection_distance_squared = max_beam_point_projection_distance_ * max_beam_point_projection_distance_;

	// O(n^2) 10.000 points => 100.000.000 operations
	for(size_t i_new = 0; i_new < projected_points.size(); ++i_new)
	{
		auto existing = 0;

		for(auto beam_idx = 0; beam_idx < beams.size(); ++beam_idx)
		{
			auto dist_vec = projected_points[i_new] - beams[beam_idx].mean_point;
			/* if(dist_vec.dot(dist_vec) >= 25 && dist_vec.dot(dist_vec) < 50)
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : dist_vec.dot(dist_vec)" << dist_vec.dot(dist_vec);
			}*/
			const auto distance = dist_vec.dot(dist_vec);

			if(distance < max_beam_point_projection_distance_squared)
			{
				good_points_added++;
				Add3dPoint(points[i_new], infos[i_new], projected_points[i_new], beams, beam_idx);
				existing++;
				break;
			}
		}

		if(!existing && !no_new_points_)
		{
			// create a new 2d cluster
			LaserBeam laser_beam;
			laser_beam.mean_point                = projected_points[i_new];
			laser_beam.number_of_points          = 1;
			laser_beam.nearest_3d_point_to_beam  = points[i_new].z;
			laser_beam.farthest_3d_point_to_beam = points[i_new].z;
			laser_beam.is_beam                   = false;
			laser_beam.beam_3d_points.push_back(points[i_new]);
			laser_beam.dot_infos.push_back(infos[i_new]);

			beams.push_back(laser_beam);
		}
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << good_points_added << " Good points have been added to beam ";
}

void Projector::UpdateProjectionsAndBeams(std::vector<LaserBeam>& beams)
{
	cv::Mat prev_translation_vector;
	cv::Mat rotation_matrix, translation_vector;

	for(auto i1 = 0; i1 < 2; ++i1)
	{
		// update the laser projector position by intersection of all stable beams
		cv::Point3f pos;
		CalculateProjectorPosition(pos, beams);
		calibration_->GetExtrinsicParameters(rotation_matrix, translation_vector);

		// Save previous T
		translation_vector.copyTo(prev_translation_vector);
		translation_vector.at<double>(0, 0) = pos.x;
		translation_vector.at<double>(1, 0) = pos.y;
		translation_vector.at<double>(2, 0) = pos.z;

		calibration_->SetExtrinsicParameters(rotation_matrix, translation_vector);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : translation vector from extrinsic parameters " << translation_vector;
		std::vector<cv::Point3f> pts_3d;
		std::vector<DotInfoType> dot_infos;

		for(auto& beam : beams)
		{
			// for each (auto point3D in beams[beamsIt].pts3d)
			for(const auto& beam_3d_point : beam.beam_3d_points)
			{
				pts_3d.push_back(beam_3d_point);
			}

			for(auto dot_info : beam.dot_infos)
			{
				dot_infos.push_back(dot_info);
			}
		}

		beams.clear();
		stable_beams_indexes_.clear();

		AddPointsToBeams(pts_3d, dot_infos, beams);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector position changed by " << cv::norm(translation_vector - prev_translation_vector) <<
 " m" << Constants::CHECK_ME;
	}
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Estimated projector position on bright dot beams" << translation_vector;
}

int Projector::TryFillMissingCentralBeam(std::vector<cv::Point2f>& candidate_sym_pts,
                                         bool try_mean) const
{
	auto min_x = FLT_MAX;
	auto max_x = -FLT_MAX;
	auto min_y = FLT_MAX;
	auto max_y = -FLT_MAX;
	cv::Point2f mean(0, 0);

	for(auto& candidate : candidate_sym_pts)
	{
		max_x = std::max(max_x, candidate.x);
		min_x = std::min(min_x, candidate.x);
		max_y = std::max(max_y, candidate.y);
		min_y = std::min(min_y, candidate.y);
		mean  = mean + candidate;
	}

	mean *= 1.0 / (float)candidate_sym_pts.size();
	auto width  = int(max_x - min_x) + 20;
	auto height = int(max_y - min_y) + 20;

	/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : width " << width;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : height " << height;*/

	cv::Point2f offset(width / 2 - mean.x, height / 2 - mean.y);
	cv::Mat image = cv::Mat::zeros(height, width, CV_8UC3);

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : offset " << offset;

	for(auto& candidate : candidate_sym_pts)
	{
		cv::circle(image, candidate + offset, 1, Constants::PURE_BLUE, 1);
	}

	// create kd tree for neighbor search
	cv::Mat samples((int)candidate_sym_pts.size(), 2, CV_32F);

	for(auto i = 0; i < (int)candidate_sym_pts.size(); ++i)
	{
		samples.at<float>(i, 0) = candidate_sym_pts[i].x;
		samples.at<float>(i, 1) = candidate_sym_pts[i].y;
	}

	// Create the Index
	cv::flann::KDTreeIndexParams index_params(1);
	cv::flann::Index kd_tree(samples, index_params);
	cv::flann::SearchParams cv_radius_params(-1, 0.0f, false);

	auto max_dist    = 40.0;
	auto min_dist    = 20.0;
	auto sq_max_dist = max_dist * max_dist;
	std::vector<std::vector<cv::Point2f>> new_centers;

	for(size_t i = 0; i < candidate_sym_pts.size(); ++i)
	{
		std::vector<float> pt(2);
		pt[0] = candidate_sym_pts[i].x;
		pt[1] = candidate_sym_pts[i].y;

		cv::circle(image, candidate_sym_pts[i] + offset, 5, Constants::GRAY, 1);

		std::vector<int> indices;
		std::vector<float> distances;

		int num_neighbors = kd_tree.radiusSearch(pt, indices, distances, sq_max_dist, 10, cv_radius_params);

		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : num_neighbors " << num_neighbors;

		for(size_t idx = 0; idx < distances.size(); ++idx)
		{
			// the search point itself or any invalid point
			if(distances[idx] < min_dist * min_dist)
			{
				continue;
			}

			cv::Vec2d difference(candidate_sym_pts[indices[idx]].x - candidate_sym_pts[i].x,
			                     candidate_sym_pts[indices[idx]].y - candidate_sym_pts[i].y);

			/*		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : difference " << difference;
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cv::norm(difference)  " << cv::norm(difference);
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : IsDoeOrthoDir(difference)  " << IsDoeOrthogonalDirection(difference);
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : IsDiagonal(difference)  " << IsDiagonal(difference);*/
			bool try_fill = false;

			// direct X or Y neighbor
			if(fabs(cv::norm(difference) - Constants::XY_NEIGHBOR_RADIUS_SEARCH) < 4 && IsDoeOrthogonalDirection(difference) > 0)
			{
				try_fill = true;
			}

			// direct diagonal neighbor
			if(fabs(cv::norm(difference) - Constants::DIAGONAL_NEIGHBOR_RADIUS_SEARCH) < 7 && IsDiagonal(difference) > 0)
			{
				try_fill = true;
			}

			if(!try_fill)
			{
				continue;
			}

			std::vector<float> try_point(2);
			try_point[0] = candidate_sym_pts[i].x - (float)difference[0];
			try_point[1] = candidate_sym_pts[i].y - (float)difference[1];

			// check that the point to try is not outside check area
			if(try_point[0] > max_x - 5 || try_point[0] < min_x + 5 || try_point[1] > max_y - 5 || try_point[1] < min_y + 5)
			{
				continue;
			}

			std::vector<int> try_indices;
			std::vector<float> try_distances;

			auto num_neighbors2 = kd_tree.radiusSearch(try_point, try_indices, try_distances, 100, 10, cv_radius_params);

			//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : num_neighbors2 " << num_neighbors2;

			// there is no measured point close to the symmetry point, probably missing bright point
			if(num_neighbors2 == 0)
			{
				cv::Point2f pt_missing(try_point[0], try_point[1]);

				// check if a very close point has already been added
				int isCloseTo = -1;

				for(size_t iNewPts = 0; iNewPts < new_centers.size(); ++iNewPts)
				{
					// check distance to mean kept in [0]
					if(cv::norm(new_centers[iNewPts][0] - pt_missing) < 7)
					{
						isCloseTo = (int)iNewPts;
						break;
					}
				}

				if(isCloseTo < 0)
				{
					// [0] always keeps mean value
					new_centers.push_back(std::vector<cv::Point2f>(2, pt_missing));
				}
				else
				{
					// add point and re-calculate mean
					new_centers[isCloseTo].push_back(pt_missing);

					new_centers[isCloseTo][0] = cv::Point2f(0, 0);
					for(size_t i = 1; i < new_centers[isCloseTo].size(); ++i)
						new_centers[isCloseTo][0] += new_centers[isCloseTo][i];

					new_centers[isCloseTo][0] *= 1.0f / (float)(new_centers[isCloseTo].size() - 1);
				}

				cv::circle(image, cv::Point2f(try_point[0], try_point[1]) + offset, 1, Constants::DARK_BLUE, 1);
			}
		} // iterate all neighbors
	}     // iterate all existing candidate points

	// exactly 1 point is missing and it is a candidate from all 8 neighbors ( element [0] mean)
	if(new_centers.size() == 1 /* && newCenters[0].size() == 9*/)
	{
		candidate_sym_pts.push_back(new_centers[0][0]);
		cv::circle(image, mean + offset, 3, Constants::BRIGHT_GREEN, 1);
		return 2;
	}

	// at least one gap was found
	if(!new_centers.empty())
	{
		for(auto& p : new_centers)
		{
			candidate_sym_pts.push_back(p[0]);
		}
		// added points not not clearly identified central beam
		return 1;
	}

	// go through all candidate points and check if 8 neighbors are available

	if(try_mean)
	{
		std::vector<cv::Point2f> meanCorrected = candidate_sym_pts;

		cv::Mat samples2((int)candidate_sym_pts.size(), 2, CV_32F);
		for(int i = 0; i < (int)candidate_sym_pts.size(); ++i)
		{
			samples2.at<float>(i, 0) = candidate_sym_pts[i].x;
			samples2.at<float>(i, 1) = candidate_sym_pts[i].y;
		}
		// Create the Index
		cv::flann::Index kdtree2(samples2, index_params);

		for(size_t i = 0; i < candidate_sym_pts.size(); ++i)
		{
			std::vector<float> pt(2);
			pt[0] = candidate_sym_pts[i].x;
			pt[1] = candidate_sym_pts[i].y;

			std::vector<int> idx;
			std::vector<float> dists;

			std::vector<cv::Point2f> directNeighbors, diagNeighbors;
			int numNeighbors = kdtree2.radiusSearch(pt, idx, dists, sq_max_dist, 10, cv_radius_params);

			double diffLengthSum = 0.0;
			for(int nn = 0; nn < numNeighbors; ++nn)
			{
				// the search point itself or any invalid point
				if(dists[nn] < min_dist * min_dist)
					continue;

				cv::Vec2d diff(
					candidate_sym_pts[idx[nn]].x - candidate_sym_pts[i].x,
					candidate_sym_pts[idx[nn]].y - candidate_sym_pts[i].y);

				// direct X or Y neighbor
				if((fabs(Helper::VectorLength(diff) - 25) < 4) && (IsDoeOrthogonalDirection(diff) > 0))
				{
					directNeighbors.push_back(candidate_sym_pts[idx[nn]]);
					diffLengthSum += Helper::VectorLength(diff);
				}

				// direct diagonal neighbor
				if((fabs(Helper::VectorLength(diff) - 35) < 7) && IsDiagonal(diff) > 0)
				{
					diagNeighbors.push_back(candidate_sym_pts[idx[nn]]);
					diffLengthSum += Helper::VectorLength(diff);
				}
			}

			cv::Point2f sum(0, 0);
			float nPts = 0.0f;
			if(directNeighbors.size() == 4)
			{
				nPts += 4.0f;
				for(auto& p : directNeighbors)
					sum += p;
			}

			if(diagNeighbors.size() == 4)
			{
				nPts += 4.0f;
				for(auto& p : diagNeighbors)
					sum += p;
			}

			if(nPts > 7.5)
			{
				meanCorrected[i] = sum * (1.0f / nPts);
				cv::circle(image, meanCorrected[i] + offset, 6, Constants::BRIGHT_BLUE, 1);
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : mean now : x, y " << meanCorrected[i].x << " " << meanCorrected[i].y <<
 " diff length " << diffLengthSum;
			}
		} // iterate all existing candidate points

		candidate_sym_pts = meanCorrected;
	}

	// nothing added
	return 0;
}

bool Projector::RotateProjectorToBeamAndProjectDMVS()
{
	if(GetUsedBeams().size() < min_num_beams_)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : GetUsedBeams().size() < min_num_beams_";
		return false;
	}

	auto projector_calibration_temp = calibration_/*->DuplicateLaser()*/;
	auto& beams                     = GetUsedBeams();
	std::vector<cv::Point2f> bright_dots;
	bright_dots.reserve(beams.size());

	for(const auto& beam : beams)
	{
		if(beam.number_of_points > 20)
		{
			bright_dots.emplace_back(beam.mean_point);
		}
	}

	cv::Point2f estimated_center(search_window_x_, search_window_y_);
	cv::Point2f detected_center(-1000.0, -1000.0);
	const auto result_central_beam_finding = GetCentralBeam(bright_dots, bright_dots, estimated_center, detected_center);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Estimated Center Beam " << estimated_center;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Detected Center Beam " << detected_center;

	if(result_central_beam_finding < 1)
	{
		std::vector<cv::Point2f> candidate_symmetric_points;

		for(auto beam : beams)
		{
			if(!beam.is_beam || beam.mean_point.x < estimated_center.x - search_window_width_ / 2.0f ||
				beam.mean_point.x > estimated_center.x + search_window_width_ / 2.0f ||
				beam.mean_point.y < estimated_center.y - search_window_height_ / 2.0f ||
				beam.mean_point.y > estimated_center.y + search_window_height_ / 2.0f)

			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Skip point " << beam.mean_point <<
														   " Search window size " << search_window_width_ << " " << search_window_height_ <<
														   " Position " << search_window_x_ << " " << search_window_y_;

				continue;
			}

			candidate_symmetric_points.push_back(beam.mean_point);
		}

		const int add_candidate_result = TryFillMissingCentralBeam(candidate_symmetric_points, false);
		//int nAdded                   = (int)candidate_symmetric_points.size() - (int)candidate_symmetric_points.size();

		// 2: A beam has been added which was already identified as center beam
		if(add_candidate_result == 2)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector calibration use unique fill point";
			BOOST_LOG_TRIVIAL(info) << "() : step2_center_point_added_as_bright_ before" << step2_center_point_added_as_bright_;
			step2_center_point_added_as_bright_ = true;
			BOOST_LOG_TRIVIAL(info) << "() : step2_center_point_added_as_bright_ after" << step2_center_point_added_as_bright_;
			detected_center = candidate_symmetric_points.at(candidate_symmetric_points.size() - 1);
		}
		else
		{
			return RotateProjectorToBeamAndProject();
		}
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Projector calibration rotated to " << detected_center;

	RotateProjectorToCenter(detected_center);

	UpdateProjections(beams);

	return true;
}

int Projector::GetCentralBeam(const std::vector<cv::Point2f>& beams,
                              const std::vector<cv::Point2f>& bright_beams,
                              cv::Point2f& estimated_center,
                              cv::Point2f& detected_center,
                              double curvature_offset) const
{
	std::string log_path = R"(D:\repos\BitBucket\farolabs\calibration-tools\CalibrationTools\DMVS011900031\20210426-101158\Logs)";

	std::ofstream beam_points_stream;
	auto beam_points_stream_file_path = log_path + R"(\brightbeams.txt)";
	beam_points_stream.open(beam_points_stream_file_path);

	if(beams.size() <= min_num_beams_)
		return -1;

	const int SEARCH_RADIUS       = 130, LOWER_DIST_THRESH = 5, UPPER_DIST_THRESH = 23;
	const double OFFSET_CURVATURE = 0.000005;

	std::vector<size_t> allBeamCandidates;
	std::vector<size_t> brightBeamCandidates;
	std::vector<cv::Vec2f> points2d(beams.size());
	std::vector<cv::Vec2f> brightPoints2d(bright_beams.size());

	if(beam_points_stream.is_open())
	{
		for(const auto& bright_beam : bright_beams)
		{
			beam_points_stream << bright_beam.x << " "
				<< bright_beam.y << '\n';
		}
	}

	// get beams
	for(size_t i = 0; i < beams.size(); ++i)
	{
		points2d[i] = beams[i];
	}

	// get bright beams
	for(size_t i = 0; i < bright_beams.size(); ++i)
	{
		brightPoints2d[i] = bright_beams[i];
	}

	// not sure if this is intended in the matlab script
	const int radius = std::min(SEARCH_RADIUS, 100);

	GetCandidates(beams, estimated_center, radius, allBeamCandidates);
	GetCandidates(bright_beams, estimated_center, SEARCH_RADIUS * 3, brightBeamCandidates);

	std::vector<cv::Point2f> beam_candidates;
	beam_candidates.reserve(allBeamCandidates.size());
	for(const auto& dot_index : allBeamCandidates)
	{
		beam_candidates.emplace_back(beams[dot_index]);
	}

	size_t beamCandidatesSizeBefore = beam_candidates.size();
	auto result                     = TryFillMissingCentralBeam(beam_candidates, true);
	size_t beamCandidatesSizeAfter  = beam_candidates.size();

	/*
	for (size_t i = std::max(beamCandidatesSizeBefore - 1, size_t(0)); i < beamCandidatesSizeAfter; ++i)
	{
		LaserBeam beam;
		beam.isBeam = true;
		beam.meanPt = beamCandidates[i];
		m_ptsStep0.push_back(beam);
	} */

	if(allBeamCandidates.empty() || brightBeamCandidates.empty())
	{
		return -1;
	}
	// remove points which are not bright points and (!) close to bright points by comparing
	// distances This way, also spots which could be bright spots are taken into account
	/*	float currentDist, minDist;
		for (int i = (int)allBeamCandidates.size() - 1; i > -1; i--)
		{
			minDist = INT_MAX;
			for (size_t j = 0; j < brightBeamCandidates.size(); ++j)
			{
				currentDist = pointDistance(meanBeams[allBeamCandidates[(size_t)i]],
	   meanBrightBeams[brightBeamCandidates[j]]); if (minDist > currentDist)
				{
					minDist = currentDist;
				}
			}
			if (LOWER_DIST_THRESH < minDist && minDist < UPPER_DIST_THRESH)
			{
				allBeamCandidates.erase(allBeamCandidates.begin() + i);
			}
		}*/

	std::vector<Cross> crossfits;
	crossfits.reserve(allBeamCandidates.size());
	std::vector<ErrorCrossFit> errorFits;
	errorFits.reserve(allBeamCandidates.size());
	std::vector<cv::Vec3d> pointsToFitPlanesHorizontal, pointsToFitPlanesVertical;
	pointsToFitPlanesHorizontal.reserve(allBeamCandidates.size());
	pointsToFitPlanesVertical.reserve(allBeamCandidates.size());

	constexpr double quadraticScaling = 1000000.0;
	for(const auto& point : beam_candidates)
	{
		auto cross = FindCrossAndGetLineRMS(bright_beams, point);
		crossfits.emplace_back(cross);
		errorFits.emplace_back(cross.errors);
		auto& error_fit         = cross.errors;
		auto& point_coordinates = point;

		if(error_fit.sum_square_line_fit_horizontal < 0.0 && error_fit.sum_square_line_fit_vertical < 0.0)
		{
			continue;
		}

		// Scale Data of quadratic fit to be usable
		pointsToFitPlanesHorizontal.emplace_back(cv::Vec3d(point_coordinates.x,
		                                                   point_coordinates.y,
		                                                   error_fit.paraboloid_fit_horizontal * quadraticScaling));
		pointsToFitPlanesVertical.emplace_back(cv::Vec3d(point_coordinates.x,
		                                                 point_coordinates.y,
		                                                 (error_fit.paraboloid_fit_vertical + curvature_offset) * quadraticScaling));
		/*std::vector<Cross> crossfittemp;
		crossfittemp.push_back(cross);*/
	}

	if(errorFits.size() != beam_candidates.size())
		return -1;

	auto plane_horizontal = FitPlaneProjectorImage(pointsToFitPlanesHorizontal);
	auto plane_vertical   = FitPlaneProjectorImage(pointsToFitPlanesVertical);

	cv::Vec3d line_origin;
	cv::Vec3d line_direction;
	if(!IntersectPlanes(plane_horizontal, plane_vertical, line_origin, line_direction))
		return -1;

	// calculate intersection of line and xy-plane (intersection of the 3 planes)
	// check if there is a posible intersection
	if(std::abs(line_direction[2]) < 0.000001)
		return -1;

	// calculation of intersectionPoint
	double t     = -line_origin[2] / line_direction[2];
	double temp1 = line_origin[0] + (t * line_direction[0]);
	double temp2 = line_origin[1] + (t * line_direction[1]);
	cv::Point2f intersectionPoint((float)temp1, (float)temp2);

	estimated_center = intersectionPoint;
	auto image2      = DrawDebugImage(bright_beams, intersectionPoint, crossfits);
	if(calibration_output_path_.empty())
	{
		cv::imwrite("./ProjectorCalDebugImage.png", image2);
	}
	else
	{
		std::string outputFile = std::string(calibration_output_path_) + "/ProjectorCalDebugImage.png";
		cv::imwrite(outputFile, image2);
	}

	// measure distance between allBeamCandidates and intersectionPoint and store of minDistance Point
	bool foundBeam             = false;
	size_t minIntersectionDist = 0;
	float MINDISTANCE          = 10.0;
	float currentDist          = 0.0f;

	if(options_->GetForceBeam())
	{
		intersectionPoint = cv::Point2f(options_->GetForceBeamX(), options_->GetForceBeamY());
	}
	for(size_t i = 0; i < brightBeamCandidates.size(); ++i)
	{
		currentDist = PointDistance(intersectionPoint, bright_beams[brightBeamCandidates[i]]);
		if(MINDISTANCE > currentDist)
		{
			MINDISTANCE         = currentDist;
			minIntersectionDist = brightBeamCandidates[i];
			foundBeam           = true;
		}
	}

	if(foundBeam)
	{
		detected_center = bright_beams[minIntersectionDist];
		return 1;
	}

	return 0;
}

void Projector::GetCandidates(const std::vector<cv::Point2f>& beams,
                              const cv::Point2f& center,
                              const int radius,
                              std::vector<size_t>& candidates) const
{
	// candidates = std::vector<int>(beams.size());
	for(int i = 0; i < beams.size(); ++i)
	{
		if(PointDistance(beams[i], center) <= radius)
		{
			candidates.push_back(i);
		}
	}
}

static cv::Point2f FitLine(std::vector<cv::Point2f> points)
{
	Eigen::MatrixX2d M(points.size(), 2);
	cv::Point2d centroid(0.0, 0.0);
	for(size_t i = 0; i < points.size(); ++i)
	{
		centroid += cv::Point2d(points[i].x, points[i].y);
		M(i, 0) = points[i].x;
		M(i, 1) = points[i].y;
	}
	centroid /= static_cast<double>(points.size());

	for(size_t i = 0; i < points.size(); ++i)
	{
		M(i, 0) -= centroid.x;
		M(i, 1) -= centroid.y;
	}
	auto svd                  = M.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
	Eigen::Vector2d direction = svd.matrixV().col(0);
	return cv::Point2f(direction.x(), direction.y());
}

Projector::Cross Projector::FitCross(const std::vector<cv::Point2f>& bright_beams,
                                     const cv::Point2f& center_point,
                                     const cv::Point2f& tolerances,
                                     const int radius) const
{
	Cross cross;
	cross.center                  = center_point;
	cross.vertical_direction      = cv::Point2f(0, 1);
	cross.horizontal_direction    = cv::Point2f(1, 0);
	cross.vertical_cross_points   = std::vector<cv::Point2f>();
	cross.horizontal_cross_points = std::vector<cv::Point2f>();

	std::vector<float> distances;
	for(cv::Point2f beam : bright_beams)
	{
		distances.push_back(PointDistance(beam, center_point));
	}

	// std::cout << "distances" << std::endl;
	// std::cout << distances << std::endl << std::endl;

	float tolerance = tolerances.x;

	std::vector<size_t> verticalCrossCandidates, horizontalCrossCandidates;
	std::map<size_t, size_t> verticalCrossCandidatesUsage;
	std::map<size_t, size_t> horizontalCrossCandidatesUsage;
	for(int i = radius; i < 721; i += (int)(radius * 1.5))
	{
		std::vector<size_t> closePoints;
		std::vector<float> verticalLineDistance(closePoints.size());
		std::vector<float> horizontalLineDistance(closePoints.size());

		for(size_t j = 0; j < distances.size(); ++j)
		{
			if(distances[j] < i)
			{
				closePoints.push_back(j);
			}
		}

		// std::cout << "closePoints" << std::endl;
		// std::cout << closePoints << std::endl << std::endl;
		std::vector<cv::Point2f> tempLineHorizontal;
		std::vector<cv::Point2f> tempLineVertical;
		for(size_t j : closePoints)
		{
			if(PointLineDist(center_point, cross.vertical_direction, bright_beams[j]) <= tolerance)
			{
				if(verticalCrossCandidatesUsage.find(j) == verticalCrossCandidatesUsage.end())
				{
					verticalCrossCandidates.push_back(j);
					verticalCrossCandidatesUsage[j] = j;
				}
				tempLineVertical.push_back(bright_beams[j] - center_point);
			}

			if(PointLineDist(center_point, cross.horizontal_direction, bright_beams[j]) <= tolerance)
			{
				if(horizontalCrossCandidatesUsage.find(j) == horizontalCrossCandidatesUsage.end())
				{
					horizontalCrossCandidates.push_back(j);
					horizontalCrossCandidatesUsage[j] = j;
				}
				tempLineHorizontal.push_back(bright_beams[j] - center_point); // cv::Point2f(temp.y, -temp.x));
			}
		}

		if(tempLineHorizontal.empty() || tempLineVertical.empty())
		{
			return cross;
		}

		Eigen::MatrixX2d M(tempLineHorizontal.size() + tempLineVertical.size(), 2);

		for(size_t j = 0; j < tempLineVertical.size(); ++j)
		{
			M(j, 0) = tempLineVertical[j].x;
			M(j, 1) = tempLineVertical[j].y;
		}
		// std::cout << "M" << std::endl;
		// std::cout << M << std::endl;
		for(size_t j = 0; j < tempLineHorizontal.size(); ++j)
		{
			M(j + tempLineVertical.size(), 0) = tempLineHorizontal[j].y;
			M(j + tempLineVertical.size(), 1) = -tempLineHorizontal[j].x;
		}
		M((tempLineHorizontal.size() + tempLineVertical.size() - 1), 0) = 0.0;
		M((tempLineHorizontal.size() + tempLineVertical.size() - 1), 1) = 0.0;

		auto svd                          = M.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
		Eigen::Vector2d directionVertical = svd.matrixV().col(0);
		// Just to make sure it is normalized
		directionVertical.normalize();
		Eigen::Matrix2d rotate90 = Eigen::Matrix2d::Identity();
		rotate90 << 0.0, -1.0, 1, 0;
		Eigen::Vector2d directionHorizontal = rotate90.transpose() * directionVertical;
		directionHorizontal.normalize();

		cross.vertical_direction.x   = directionVertical.x();
		cross.vertical_direction.y   = directionVertical.y();
		cross.horizontal_direction.x = directionHorizontal.x();
		cross.horizontal_direction.y = directionHorizontal.y();

		if(tolerance == tolerances.x)
		{
			tolerance = tolerances.y;
		}
	}

	for(size_t i : verticalCrossCandidates)
	{
		cross.vertical_cross_points.push_back(bright_beams[i]);
	}
	for(size_t i : horizontalCrossCandidates)
	{
		cross.horizontal_cross_points.push_back(bright_beams[i]);
	}

	// Refit directions for each vector of candidante points
	cross.vertical_direction   = FitLine(cross.vertical_cross_points);
	cross.horizontal_direction = FitLine(cross.horizontal_cross_points);

	return cross;
}

bool Projector::IntersectPlanes(LSFitPlane::PlaneParam& plane1,
                                LSFitPlane::PlaneParam& plane2,
                                cv::Vec3d& position,
                                cv::Vec3d& direction) const
{
	// Check if planes are parallel ore close to parallel
	if(fabs(Helper::VectorLength(plane1.normal - plane2.normal)) < 0.00001)
	{
		return false;
	}
	//TODO CHECK THIS
	auto plane1_normalized = cv::normalize(plane1.normal);
	auto plane2_normalized = cv::normalize(plane2.normal);

	plane1.normal = plane1_normalized;
	plane2.normal = plane2_normalized;

	auto dir  = cv::normalize(plane1.normal.cross(plane2.normal));
	direction = cv::Vec3d(dir); // [0] , dir.y(), dir.z());

	// plane origin distance
	double d1 = plane1.normal.dot(plane1.position);
	double d2 = plane2.normal.dot(plane2.position);

	// calculate plane intersection point
	Eigen::Matrix<double, 2, 1> b;
	Eigen::Matrix<double, 2, 2> A;
	if(dir[0] >= dir[1] && dir[0] >= dir[2])
	{
		b(0, 0) = (d1 - plane1.normal[0]);
		b(1, 0) = (d2 - plane2.normal[0]);

		A(0, 0) = plane1.normal[1];
		A(0, 1) = plane1.normal[2];
		A(1, 0) = plane2.normal[1];
		A(1, 1) = plane2.normal[2];

		Eigen::MatrixXd x = A.colPivHouseholderQr().solve(b);
		position          = cv::Vec3d(1.0, x(0, 0), x(1, 0));
	}
	else if(dir[1] >= dir[2])
	{
		b(0, 0) = (d1 - plane1.normal[1]);
		b(1, 0) = (d2 - plane2.normal[1]);

		A(0, 0) = plane1.normal[0];
		A(0, 1) = plane1.normal[2];
		A(1, 0) = plane2.normal[0];
		A(1, 1) = plane2.normal[2];

		Eigen::MatrixXd x = A.colPivHouseholderQr().solve(b);
		position          = cv::Vec3d(x(0, 0), 1.0, x(1, 0));
	}
	else
	{
		b(0, 0) = (d1 - plane1.normal[2]);
		b(1, 0) = (d2 - plane2.normal[2]);

		A(0, 0) = plane1.normal[0];
		A(0, 1) = plane1.normal[1];
		A(1, 0) = plane2.normal[0];
		A(1, 1) = plane2.normal[1];

		Eigen::MatrixXd x = A.colPivHouseholderQr().solve(b);
		position          = cv::Vec3d(x(0, 0), x(1, 0), 1.0);
	}

	return true;
}

cv::Mat Projector::DrawDebugImage(const std::vector<cv::Point2f>& bright_beams,
                                  const cv::Point2f& estimated_center,
                                  std::vector<Cross>& cross_fits,
                                  const cv::Point2f& direction_vertical,
                                  const cv::Point2f& direction_horizontal) const
{
	float maxX = 0.0;
	float maxY = 0.0;
	float minX = 0.0;
	float minY = 0.0;

	for(const auto& point : bright_beams)
	{
		if(point.x > maxX)
		{
			maxX = point.x;
		}
		if(point.y > maxY)
		{
			maxY = point.y;
		}
		if(point.x < minX)
		{
			minX = point.x;
		}
		if(point.y < minY)
		{
			minY = point.y;
		}
	}

	// Debug image
	cv::Point2f minPoint(fabs(minX), fabs(minY));

	cv::Mat image =
		cv::Mat::zeros(static_cast<int>(maxX - minX), static_cast<int>(maxY - minY), CV_8UC3);
	for(const auto& point : bright_beams)
	{
		cv::circle(image, point + minPoint, 2, Constants::PURE_BLUE, 2);
	}
	for(const auto& cross : cross_fits)
	{
		cv::line(
			image,
			cross.center + minPoint,
			cross.center + cross.vertical_direction * 1000.0f + minPoint,
			cv::Scalar(0, 0, 10 * cross.errors.sum_square_line_fit_vertical),
			1);
		cv::line(
			image,
			cross.center + minPoint,
			cross.center + cross.horizontal_direction * 1000.0f + minPoint,
			cv::Scalar(0, 0, 10 * cross.errors.sum_square_line_fit_horizontal),
			1);
		cv::line(
			image,
			cross.center + minPoint,
			cross.center + cross.vertical_direction * -1000.0f + minPoint,
			cv::Scalar(0, 0, 10 * cross.errors.sum_square_line_fit_vertical),
			1);
		cv::line(
			image,
			cross.center + minPoint,
			cross.center + cross.horizontal_direction * -1000.0f + minPoint,
			cv::Scalar(0, 0, 10 * cross.errors.sum_square_line_fit_horizontal),
			1);
		for(const auto& point : cross.vertical_cross_points)
		{
			cv::circle(image, point + minPoint, 7, Constants::DARK_BLUE, 1);
		}
		for(const auto& point : cross.horizontal_cross_points)
		{
			cv::circle(image, point + minPoint, 7, Constants::DARK_BLUE, 1);
		}
	}
	cv::circle(image, estimated_center, 2, Constants::BRIGHT_GREEN, 2);
	cv::line(
		image,
		estimated_center + minPoint,
		estimated_center + minPoint + direction_vertical * 100.0f,
		Constants::BRIGHT_GREEN,
		2);
	cv::line(
		image,
		estimated_center + minPoint,
		estimated_center + minPoint + direction_horizontal * 100.0f,
		Constants::BRIGHT_GREEN,
		2);
	return image;
}

static double Distance(const cv::Vec3d& pt,
                       const LSFitPlane::PlaneParam& plane)
{
	auto diff = (pt - plane.position);
	return diff.dot(plane.normal);
}

LSFitPlane::PlaneParam Projector::FitPlane2(const std::vector<cv::Vec3d>& plane_pts3d,
                                            std::vector<int>& used_plane_pts,
                                            const std::vector<int>& orig_plane_pts_indices)
{
	// Fit plane to the center pixels
	LSFitPlane planeFit;
	LSFitPlane::PlaneParam planeParams = planeFit.fit(plane_pts3d);

	// remove points outside 2 sigma for robust plane estimation
	// Take only points that fit best to the center plane
	double rmsAll = planeParams.ptDist;
	std::vector<cv::Vec3d> planePts3dRobust;
	for(size_t i = 0; i < plane_pts3d.size(); ++i)
	{
		const cv::Vec3d& pt = plane_pts3d[i];
		const double d      = Distance(pt, planeParams);
		if(fabs(d) < 2.0 * planeParams.ptDist)
			planePts3dRobust.push_back(pt);
	}

	// Fit plane through the best center plane
	planeParams = planeFit.fit(planePts3dRobust);

	std::vector<cv::Vec3d> planePts3dInlier;

	// remove points outside 10 Sigma (far outliers, stray points)
	// for valid statistics
	for(size_t i = 0; i < plane_pts3d.size(); ++i)
	{
		const cv::Vec3d& pt = plane_pts3d[i];
		const double d      = Distance(pt, planeParams);
		if(fabs(d) < 10.0 * planeParams.ptDist)
		{
			planePts3dInlier.push_back(pt);
			used_plane_pts.push_back(orig_plane_pts_indices[i]);
		}
	}

	planeParams = planeFit.fit(planePts3dInlier);

	return planeParams;
}

LSFitPlane::PlaneParam Projector::FitPlaneProjectorImage(const std::vector<cv::Vec3d> points_3d)
{
	std::vector<int> pointIndices(points_3d.size());
	for(int i = 0; i < points_3d.size(); ++i)
	{
		pointIndices[i] = i;
	}
	std::vector<int> usedIndices;
	auto result = FitPlane2(points_3d, usedIndices, pointIndices);
	return result;
}

Projector::Cross Projector::FindCrossAndGetLineRMS(const std::vector<cv::Point2f>& bright_beams,
                                                   const cv::Point2f& center_point,
                                                   const cv::Point2f& tolerances,
                                                   const int radius) const
{
	ErrorCrossFit errorFit;
	auto cross = FitCross(bright_beams, center_point, tolerances, radius);
	std::vector<float> errorsVertical;
	errorsVertical.reserve(cross.vertical_cross_points.size());
	for(auto& point : cross.vertical_cross_points)
	{
		auto distance = PointLineDist(cross.center, cross.vertical_direction, point);
		errorsVertical.emplace_back(distance * distance);
	}
	std::vector<float> errorsHorizontal;
	errorsHorizontal.reserve(cross.horizontal_cross_points.size());
	for(auto& point : cross.horizontal_cross_points)
	{
		auto distance = PointLineDist(cross.center, cross.horizontal_direction, point);
		errorsHorizontal.emplace_back(distance * distance);
	}
	auto sumSquaredErrorVertical = std::sqrt(std::accumulate(
		errorsVertical.begin(),
		errorsVertical.end(),
		0.0,
		[](auto& sum,
		   auto& squareError)
		{
			return sum + squareError;
		}));
	auto sumSquaredErrorHorizontal = std::sqrt(std::accumulate(
		errorsHorizontal.begin(),
		errorsHorizontal.end(),
		0.0,
		[](auto& sum,
		   auto& squareError)
		{
			return sum + squareError;
		}));

	errorFit.sum_square_line_fit_horizontal = sumSquaredErrorHorizontal;
	errorFit.sum_square_line_fit_vertical   = sumSquaredErrorVertical;
	errorFit.paraboloid_fit_horizontal      = cross.horizontal_cross_points.size() < 1
		                                          ? -1
		                                          : FitParabolaAndReturnQuadraticPart(cross.horizontal_cross_points, true);
	errorFit.paraboloid_fit_vertical = cross.vertical_cross_points.size() < 1
		                                   ? -1
		                                   : FitParabolaAndReturnQuadraticPart(cross.vertical_cross_points, false);

	cross.errors = errorFit;
	return cross;
}

float Projector::PointDistance(const cv::Point2f& p1,
                               const cv::Point2f& p2) const
{
	cv::Point2f diff = p1 - p2;

	float distance = std::sqrt(diff.x * diff.x + diff.y * diff.y);
	return distance;
}

float Projector::PointLineDist(const cv::Point2f& origin,
                               const cv::Point2f& direction,
                               const cv::Point2f& point) const
{
	cv::Vec3f Origin(origin.x, origin.y, 0);
	cv::Vec3f dir(direction.x, direction.y, 0);

	cv::Vec3f point3d(point.x, point.y, 0);
	auto vector       = point3d - Origin;
	auto resultVector = vector.cross(dir);

	return sqrt(resultVector.dot(resultVector)) / sqrt(dir.dot(dir));
}

double Projector::FitParabolaAndReturnQuadraticPart(std::vector<cv::Point2f>& points2d,
                                                    const bool use_y)
{
	cv::Mat parabelPoints((int)points2d.size(), 3, CV_64F);
	cv::Mat actualPoints((int)points2d.size(), 1, CV_64F);
	if(use_y)
	{
		for(int i = 0; i < points2d.size(); ++i)
		{
			actualPoints.at<double>(i, 0) = (double)points2d[i].y;
		}

		for(int i = 0; i < points2d.size(); ++i)
		{
			parabelPoints.at<double>(i, 0) = (double)(points2d[i].x * points2d[i].x);
			parabelPoints.at<double>(i, 1) = (double)points2d[i].x;
			parabelPoints.at<double>(i, 2) = 1.0;
		}
	}
	else
	{
		for(int i = 0; i < points2d.size(); ++i)
		{
			actualPoints.at<double>(i, 0) = (double)points2d[i].x;
		}

		for(int i = 0; i < points2d.size(); ++i)
		{
			parabelPoints.at<double>(i, 0) = (double)(points2d[i].y * points2d[i].y);
			parabelPoints.at<double>(i, 1) = (double)points2d[i].y;
			parabelPoints.at<double>(i, 2) = 1.0;
		}
	}
	// std::cout << "parabelPoins" << std::endl << parabelPoints << std::endl << std::endl;
	// std::cout << "actualPoints" << std::endl << actualPoints << std::endl << std::endl;

	cv::Mat inversion;

	cv::invert(parabelPoints, inversion, cv::DecompTypes::DECOMP_SVD);
	// std::cout << "inversion" << std::endl << inversion << std::endl << std::endl;

	cv::Mat result;
	result = inversion * actualPoints;

	// std::cout << "result" << std::endl << result << std::endl << std::endl;

	return result.at<double>(0, 0);
}

void Projector::ErrorCrossFit::SetSumSquareLineFitLength()
{
	sum_square_line_fit_horizontal = std::sqrt(
		sum_square_line_fit_horizontal * sum_square_line_fit_horizontal
		+ sum_square_line_fit_vertical * sum_square_line_fit_vertical);
}

void Projector::ErrorCrossFit::SetParaboloidFitLength()
{
	paraboloid_fit_length = std::sqrt(
		paraboloid_fit_horizontal * paraboloid_fit_horizontal
		+ paraboloid_fit_vertical * paraboloid_fit_vertical);
}

int Projector::IsDoeOrthogonalDirection(const cv::Vec2d direction)
{
	const auto doe_skew_deg = 6;
	const auto tolerance    = 7;

	const auto y = -tan(doe_skew_deg / 180.0 * 3.14159);
	const auto x = -y;

	const cv::Vec2d skew_x(1.0, y);
	const cv::Vec2d skew_y(x, 1.0);

	const auto delta_x = Helper::AngleTo(direction, skew_x);
	const auto ang_x   = delta_x / 3.14159 * 180.0;

	const auto delta_y = Helper::AngleTo(direction, skew_y);
	const auto ang_y   = delta_y / 3.14159 * 180.0;

	if(ang_x < tolerance || ang_x > 180 - tolerance)
		return 1;

	if(ang_y < tolerance || ang_y > 180 - tolerance)
		return 2;

	return 0;
}

int Projector::IsDiagonal(const cv::Vec2d direction)
{
	const auto doe_skew_deg = 6;
	const auto tolerance    = 7;

	const auto a = -tan((doe_skew_deg + 45) / 180.0 * 3.14159);
	const auto b = -tan((doe_skew_deg - 45) / 180.0 * 3.14159);

	const cv::Vec2d skew_x(1.0, a);
	const cv::Vec2d skew_y(1.0, b);

	const auto delta_x = Helper::AngleTo(direction, skew_x);
	const auto ang_x   = delta_x / 3.14159 * 180.0;

	const auto delta_y = Helper::AngleTo(direction, skew_y);
	const auto ang_y   = delta_y / 3.14159 * 180.0;

	if(ang_x < tolerance || ang_x > 180 - tolerance)
		return 1;

	if(ang_y < tolerance || ang_y > 180 - tolerance)
		return 2;

	return 0;
}
