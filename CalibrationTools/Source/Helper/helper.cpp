#include "helper.h"
#include "dmvs_exception.h"
#include "constants.h"
#include "marker_quality_estimator.h"
#include "camera_calibration_fit.h"
#include "enum_image_channel_type.h"
#include "io_messages.h"
#include "read_context_base.h"
#include "read_from_scope.h"
#include <boost/algorithm/string.hpp>

namespace DMVS
{
namespace CameraCalibration
{
bool Helper::IsDeviceSafe(DMVSDevice* device)
{
	return device && device->IsConnected();
}

void Helper::Sleeping(const size_t sleep_time_seconds,
                      const std::string& message)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Sleeping for " << sleep_time_seconds << " seconds " << message << Constants::LOG_START;

	for(auto i = 0; i < Constants::SLEEP_LOOP_CONSTANT; ++i)
		Sleep(DWORD(sleep_time_seconds));

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Sleeping for " << sleep_time_seconds << " seconds " << message << Constants::LOG_END;
}

unsigned int Helper::CharacterCount(const std::wstring& data)
{
	return unsigned int(data.empty() ? 0 : data.length());
}

wchar_t Helper::At(std::wstring& data,
                   const unsigned int position)
{
	assert(position >= 0 && position <= CharacterCount(data));
	return data.at(position);
}

int Helper::FindNonWS(std::wstring data,
                      const unsigned int position)
{
	const auto num_chars = CharacterCount(data);

	if(position < 0 || position >= num_chars)
		return -1;

	for(auto i = position; i < num_chars; ++i)
	{
		if(iswgraph(At(data, i)))
			return i;
	}

	return -1;
}

int Helper::FindWS(std::wstring data,
                   const unsigned int position)
{
	const auto num_chars = CharacterCount(data);

	if(position < 0 || position >= num_chars)
		return -1;

	for(auto i = position; i < num_chars; ++i)
	{
		if(!iswgraph(At(data, i)))
			return i;
	}

	return -1;
}

const wchar_t* Helper::StringNeverNull(const std::wstring& data)
{
	assert(!data.empty());

	return data.c_str();
}

std::wstring Helper::Substr(std::wstring data,
                            int position,
                            int last)
{
	//Make sure range is valid
	const auto len = int(CharacterCount(data));
	if(position < 0)
		position = 0;
	else if(position > len)
		position = len;

	if(last < 0 || last > len)
		last = len;

	//Change to length
	last = last - position;

	if(last <= 0)
		return L"";

	if(last >= len)
	{
		// result is whole string
		assert(position == 0u);
		assert(last == len);
		return data;
	}

	return std::wstring(StringNeverNull(data) + position, last);
}

std::wstring Helper::Trim(std::wstring data,
                          const bool front,
                          const bool back)
{
	if(data.empty())
		return data;

	// index of first non-whitespace character
	unsigned int front_idx = 0;

	// index of last non-whitespace character
	auto back_idx = CharacterCount(data) - 1;

	if(front)
	{
		while(front_idx <= back_idx)
		{
			if(iswspace(At(data, front_idx)))
			{
				++front_idx;
			}
			else
			{
				break;
			}
		}
	}

	if(back)
	{
		while(back_idx >= front_idx)
		{
			if(::iswspace(At(data, back_idx)))
			{
				--back_idx;
			}
			else
			{
				break;
			}
		}
	}

	return Substr(data, front_idx, back_idx + 1);
}

int Helper::ReplaceFileExtension(const wchar_t* to_replace,
                                 const wchar_t* replacement)
{
	if(!to_replace)
		return 0;

	assert(wcslen(to_replace) < INT_MAX);
	const auto to_replace_len = int(wcslen(to_replace));

	if(to_replace_len <= 0)
		return 0;

	std::wstring result;
	int found;
	auto index      = 0;
	size_type count = 0;

	while((found = FindNonWS(to_replace, index)) >= 0)
	{
		const auto n = found - index;

		if(n > 0)
			result += Substr(to_replace, index, found);
		index += n;

		if(replacement)
			result += replacement;

		index += to_replace_len;
		++count;
	}
	//result += Substr(index);

	//swap(result);

	return count;
}

std::string Helper::GetErrorString(error::DMVSErrorCode error)
{
	std::string error_string;

	switch(error)
	{
	case error::OK:
		error_string = "OK";
		break;
	case error::GENERICERROR:
		error_string = "Generic Error";
		break;
	case error::NOTCONNECTED:
		error_string = "Not Connected";
		break;
	case error::PARAMETEROUTOFRANGE:
		error_string = "Parameter Out of Range";
		break;
	case error::SEQUENCERMODEACTIVE:
		error_string = "Sequencer Mode Active";
		break;
	case error::DEVICENOTFOUND:
		error_string = "Device Not Found";
		break;
	case error::CALLBACKUNDEFINED:
		error_string = "Callback Undefined";
		break;
	case error::UNSUPPORTEDMODE:
		error_string = "Unsupported Mode";
		break;
	case error::WRONGMODE:
		error_string = "Wrong Mode";
		break;
	case error::ACQUISITIONRUNNING:
		error_string = "Acquisition Running";
		break;
	case error::ACQUISITIONNOTRUNNING:
		error_string = "Acquisition Not Running";
		break;
	case error::UNKOWNPARAMETER:
		error_string = "Unknown Parameter";
		break;
	case error::IMAGETRANSMISSINERROR:
		error_string = "Image Transmission Error";
		break;
	case error::CALLBACKALREADYDEFINED:
		error_string = "Call Back Already Defined";
		break;
	case error::TRANSFORMATIONSETTINGERROR:
		error_string = "Transformation Setting Error";

		break;
	case error::CAMERAGENERICERROR:
		error_string = "Camera Generic Error";
		break;
	case error::CAMERANOTCONNECTED:
		error_string = "Camera Not Connected";
		break;
	case error::CAMERAPARAMETEROUTOFRANGE:
		error_string = "Camera Parameter Out of Range";
		break;
	case error::CAMERATRIGGERMODEON:
		error_string = "Camera Trigger Mode ON";
		break;
	case error::CAMERAFEATURENOTAVAILABLEDURINGAUTOEXPOSURE:
		error_string = "Camera Feature Not Available During Auto Exposure";
		break;
	case error::CAMERAFILENOTFOUND:
		error_string = "Camera File Not Found";
		break;
	case error::CAMERAGRABSTARTERROR:
		error_string = "Camera Grab Start Error";
		break;
	case error::CAMERAPARAMETERINACTIVE:
		error_string = "Camera Parameter Inactive";

		break;
	case error::TRIGGERGENERICERROR:
		error_string = "Trigger Generic Error";
		break;
	case error::TRIGGERNOTCONNECTED:
		error_string = "Trigger Not Connected";
		break;
	case error::TRIGGERPARAMETEROUTOFRANGE:
		error_string = "Trigger Parameter Out of Range";
		break;
	case error::TRIGGERFEATURENOTAVAILABLE:
		error_string = "Trigger Feature Not Available";
		break;
	case error::TRIGGERTRIGGERMODEON:
		error_string = "Trigger Mode ON";

		break;
	case error::ERRORUPLOADINGCALIBRATIONTODEVICE:
		error_string = "Error Uploading Calibration to Device";
		break;
	case error::INFIELDCALIBRAIONDOESNOTEXIST:
		error_string = "Infield Calibration File does not exist";
		break;
	case error::ERRORCHANGINGCALIBRATION:
		error_string = "Error Changing Calibration";
		break;
	case error::ERRORCALIBRATIONFILEINVALID:
		error_string = "Error Calibration Field Invalid";

		break;
	case error::NOTIMPLEMENTED:
		error_string = "Not Implemented";
		break;
	case error::SDKERROR:
		error_string = "SDK Error";
		break;
	case error::NULLOBJECTPARAMETER:
		error_string = "Null Object Parameter";
		break;
	case error::NULLOBJECT:
		error_string = "Null Object";

		break;
	case error::BOARDDETECTORNOTINTIALIZED:
		error_string = "Board Detector Not Initialized";
		break;
	case error::BOARDDETECTORBOARDNOTFOUND:
		error_string = "Board Detector Board Not Found";
		break;
	case error::BOARDDETECTORMODEACTIVE:
		error_string = "Board Detector Mode Active";
		break;
	case error::BOARDDETECTORMODENOTACTIVE:
		error_string = "Board Detector Mode Not Active";
		break;
	case error::BOARDDETECTORDEVICENOTREADY:
		error_string = "Board Detector Device Not Ready";
		break;
	case error::BOARDDETECTORFILEINVALID:
		error_string = "Board Detector File Invalid";
		break;
	case error::BOARDDETECTORFILENOTFOUND:
		error_string = "Board Detector File Not Found";

		break;
	case error::HANDEYECALIBRATIONINSUFFICIENTPOSES:
		error_string = "Hand Eye Calibration Insufficient Poses";
		break;
	case error::HANDEYECALIBRATIONUNKOWNALGORITHM:
		error_string = "Hand Eye Calibration Unknown Algorithm";

		break;
	case error::LEDSNOTCONNECTED:
		error_string = "LED's Not Connected";
		break;
	case error::LEDSSETTINGERROR:
		error_string = "LED's Setting Error";

		break;
	case error::CALIBRATIONFILEINVALID:
		error_string = "Calibration File Invalid";
		break;
	case error::CALIBRATIONFILENOTFOUND:
		error_string = "Calibration File Not Found";

		break;
	case error::SENSORHEADOUTDATED:
		error_string = "Sensor Head Outdated";
		break;
	case error::INVALIDDMVSOBJECT:
		error_string = "Invalid DMVS Object";
		break;
	case error::FIRMWAREOUTDATED:
		error_string = "Firmware Outdated";
		break;
	case error::TIMEOUT:
		error_string = "Time Out";
		break;
	case error::READONLYDEVICE:
		error_string = "Read Only Device";

		break;
	case error::CANNOTWRITEFILE:
		error_string = "Cannot Write File";
		break;
	case error::FILENOTFOUND:
		error_string = "File Not Found";
		break;
	case error::DESERIALIZATIONERROR:
		error_string = "Deserialization Error";

		break;
	case error::ARRAYINCOMPLETE:
		error_string = "Array Incomplete";
		break;
	case error::ARRAYINVALIDGUID:
		error_string = "Array Invalid GUID";
		break;
	case error::ARRAYINVALIDNAME:
		error_string = "Array Invalid Name";
		break;
	case error::ARRAYUNKONWDEVICE:
		error_string = "Array Unknown Device";
		break;
	case error::ARRAYDEVICEALREADYINARRAY:
		error_string = "Array Device Already in Array";
		break;
	case error::ARRAYSINGLEDEVICENOTAVAILABLEFUNCTION:
		error_string = "Array Single Device Not Available Function";
	default:
		break;
	}

	return error_string;
}

void Helper::ThrowExceptionOnError(const error::DMVSErrorCode error_code)
{
	if(error_code != error::OK)
	{
		const auto error_string = GetErrorString(error_code);
		throw DMVSException(error_string.c_str(), __FILE__, __LINE__, __FUNCTION__);
	}
}

int Helper::RoundToInt(const double value)
{
	return static_cast<int>(round(value));
}

size_t Helper::LoadDetectorBoards(const std::string file_name,
                                  std::vector<std::shared_ptr<BoardDetector>>& boards)
{
	//std::vector<std::shared_ptr<BoardDetector>> boards_in;
	size_t num_boards_with_circle_grid = 0;

	for(auto i = 1; i < Constants::MAX_COUNT_DETECTOR_BOARDS; ++i)
	{
		std::ofstream board_file_stream;
		board_file_stream.open(file_name);

		if(!board_file_stream || !board_file_stream.is_open())
			continue;

		std::shared_ptr<BoardDetector> board;
		boards.push_back(board);
		boards.back()->LoadFromFile(file_name);

		if(boards.back()->HasCircleGridMarker())
			num_boards_with_circle_grid++;
	}

	//Actually throw an exception here
	if(num_boards_with_circle_grid)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Multiple boards with circle grid pattern - not allowed" << std::endl;
	}

	return boards.size();
}

size_t Helper::AddCodedMarkers(const std::vector<std::shared_ptr<BoardDetector>>& boards,
                               std::map<ImageChannel, CircularMarkerVector>& markers_map,
                               const size_t frame_id,
                               std::vector<std::shared_ptr<Observation>>& observations,
                               ChannelImageMap& channel)
{
	/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_START;*/

	for(auto& board : boards)
	{
		for(auto& marker_pair : markers_map)
		{
			//const auto current_board = board;
			const auto board_id   = board->Detect(marker_pair.second);
			auto detected_markers = board->GetDetectedMarkers();
			CircularMarkerVector found_coded_markers;

			for(const auto& detected_marker_pair : detected_markers)
			{
				found_coded_markers.push_back(detected_marker_pair.second);

				CameraCalibrationFit::AddObservation(observations,
				                                     detected_marker_pair.second,
				                                     marker_pair.first,
				                                     board->GetBoardId(),
				                                     detected_marker_pair.first,
				                                     frame_id);
			}

			DrawMarker(channel.channel_image_map[marker_pair.first], found_coded_markers);
		}
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::LOG_END;

	return observations.size();
}

void Helper::DrawMarker(cv::Mat& image,
                        const CircularMarkerVector& markers,
                        const bool if_draw_id,
                        const cv::Scalar& text_color)
{
	for(const auto& marker : markers)
	{
		cv::ellipse(image, marker.CorrectedEllipse(), text_color, 2, cv::LINE_AA);

		if(if_draw_id && marker.id > 0)
		{
			cv::putText(image,
			            std::to_string(marker.id),
			            cv::Point2f(marker.CorrectedCenter().x + 15, marker.CorrectedCenter().y),
			            cv::FONT_HERSHEY_SIMPLEX,
			            1.0,
			            text_color,
			            1.3,
			            cv::LINE_AA);
		}
	}
}

void Helper::DetectMarkers(ChannelImageMap& channel_images,
                           BlobDetector& blob_detector,
                           ImageChannelCircularMarkerVectorMap& markers)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Detecting Markers " << Constants::LOG_START;

	std::vector<ImageChannel> channels;
	channels.reserve(channel_images.channel_image_map.size());

	for(auto& channel_image_pair : channel_images.channel_image_map)
	{
		channels.push_back(channel_image_pair.first);

		//Create this beforehand
		markers[channel_image_pair.first] = CircularMarkerVector();
	}

	tbb::parallel_for(static_cast<std::size_t>(0),
	                  channels.size(),
	                  [&blob_detector, &markers, &channel_images, &channels](const size_t index)
	                  {
		                  //from now on use the BGR0 channel for the markers because this channel is used for the calibration
		                  const auto destination_channel = channels[0] == CHANNEL_BAYBG0
			                                                   ? CHANNEL_BGR0
			                                                   : channels[index];

		                  cv::Mat search_image;

		                  if(channels[index] == CHANNEL_BAYBG0)
		                  {
			                  cv::cvtColor(channel_images.channel_image_map[channels[index]],
			                               search_image,
			                               cv::COLOR_BayerRG2BGR_EA);
		                  }
		                  else
		                  {
			                  search_image = channel_images.channel_image_map[channels[index]];
		                  }

		                  blob_detector.DetectBlob(search_image, markers[destination_channel]);
	                  });

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Detecting markers " << Constants::LOG_END;
}

cv::Scalar Helper::PredefinedBoardColor(const size_t board_id)
{
	const auto color_idx = board_id % 10;

	switch(color_idx)
	{
	case 0:
		return Constants::WHITE;

	case 1:
		return Constants::RED;

	case 2:
		return Constants::GREEN;

	case 3:
		return Constants::BLUE;

	case 4:
		return Constants::YELLOW;

	case 5:
		return Constants::MAGENTA;

	case 6:
		return Constants::CYAN;

	case 7:
		return Constants::GRAY;

	case 8:
		return Constants::LIGHT_BLUE;

	case 9:
		return Constants::ORANGE;

	default:
		return Constants::WHITE;
	}
}

void Helper::RemoveLowQualityMarker(ImageChannelCircularMarkerVectorMap& markers,
                                    ChannelImageMap& channel_type)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calculating Marker Surrounding " << Constants::LOG_START;

	for(auto it = channel_type.channel_image_map.begin(); it != channel_type.channel_image_map.end(); ++it)
	{
		if(it->first == CHANNEL_GREY0 || it->first == CHANNEL_GREY1)
		{
			cv::Mat_<unsigned char> temp = it->second;
			MarkerQualityEstimator<unsigned char> estimator(temp);
			std::vector<size_t> markers_to_erase;

			for(auto i = 0; i < markers[it->first].size(); ++i)
			{
				if(!estimator.EstimateMarkerQuality(markers[it->first][i]))
				{
					markers_to_erase.push_back(i);
				}
			}

			//Erase all bad markers
			for(auto rev_it = markers_to_erase.rbegin(); rev_it != markers_to_erase.rend(); ++rev_it)
			{
				markers[it->first].erase(markers[it->first].begin() + *rev_it);
			}
		}
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Calculating Marker Surrounding " << Constants::LOG_END;
}

void Helper::AddUncodedMarkers(std::map<ImageChannel, CircularMarkerVector>& circular_markers,
                               std::vector<std::shared_ptr<Observation>>& observations,
                               const size_t frame_count)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Adding Uncoded Marker " << Constants::LOG_START;

	//Add all ID = 1 Marker (not identified marker) for later identification if initial
	//valid camera calibrations are available. Iterate over all channels (m.first)
	for(const auto& circular_marker_pair : circular_markers)
	{
		for(const auto& circular_marker : circular_marker_pair.second)
		{
			if(circular_marker.id == 1001000)
			{
				CameraCalibrationFit::AddObservation(observations,
				                                     circular_marker,
				                                     circular_marker_pair.first,
				                                     Constants::DEFAULT_SIZE_T_VALUE,
				                                     Constants::DEFAULT_SIZE_T_VALUE,
				                                     frame_count);
			}
		}
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Adding Uncoded Marker " << Constants::LOG_END;
}

void Helper::CopyImages(ChannelImageMap& source_images,
                        Frame& frame,
                        const size_t index)
{
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Channel Image Copying " << Constants::LOG_START;

	//Copy all channel images to image collection
	for(auto& channel_image_pair : source_images.channel_image_map)
	{
		auto source  = channel_image_pair.second;
		auto channel = channel_image_pair.first;
		cv::Mat destination;

		if(source.channels() == Constants::NUMBER_OF_CHANNELS)
		{
			std::vector<cv::Mat> channels(3);
			cv::split(source, channels);
			destination = channels[1];
		}
		else
		{
			source.copyTo(destination);
		}

		frame.frame_images[index].channel_image_map[channel] = destination.clone();
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Channel Image Copying " << Constants::LOG_END;
}

void Helper::CreateCollage(cv::Mat& collage,
                           const cv::Mat& pattern_image_left,
                           const cv::Mat& pattern_image_right,
                           const cv::Mat& projector_image)
{
	if(!(pattern_image_left.data && pattern_image_right.data && projector_image.data))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No image data available ";
	}

	const auto width  = 3200; // 400 * 5 * 2 = 200 * 2 = 4000
	const auto height = 1800; // 300 * 3 * 2 = 900 * 2 = 1800

	// create the output image containing all images
	collage.create(height, width, CV_8UC3);

	const cv::Size small_image(width / 2, height / 2);
	const cv::Size large_image(width / 2, height);

	// Positions of 3 sub images
	const cv::Rect top_left(0, 0, small_image.width, small_image.height);
	const cv::Rect bottom_left(0, small_image.height, small_image.width, small_image.height);
	const cv::Rect right_large(small_image.width, 0, large_image.width, large_image.height);

	// resize patter0 image and convert if necessary
	if(pattern_image_left.channels() == 3)
	{
		cv::resize(pattern_image_left, collage(top_left), cv::Size(), 0.5, 0.5);
	}
	else
	{
		cv::Mat color;
		cv::resize(pattern_image_left, color, cv::Size(), 0.5, 0.5);
		cvtColor(color, collage(top_left), cv::COLOR_GRAY2BGR);
	}

	// resize patter1 image and convert if necessary
	if(pattern_image_right.channels() == 3)
	{
		cv::resize(pattern_image_right, collage(bottom_left), cv::Size(), 0.5, 0.5);
	}
	else
	{
		cv::Mat color;
		cv::resize(pattern_image_right, color, cv::Size(), 0.5, 0.5);
		cvtColor(color, collage(bottom_left), cv::COLOR_GRAY2BGR);
	}

	// Display image info
	cv::putText(collage,
	            "IR1/Left in FS view direction",
	            cv::Point(top_left.x + 15, top_left.y + 50),
	            cv::FONT_HERSHEY_DUPLEX,
	            2.0,
	            Constants::WHITE,
	            1,
	            2);
	cv::putText(collage,
	            "IR0/Right in FS view direction",
	            cv::Point(bottom_left.x + 15, bottom_left.y + 50),
	            cv::FONT_HERSHEY_DUPLEX,
	            2.0,
	            Constants::WHITE,
	            1,
	            2);

	// resize extra image and convert if necessary
	if(projector_image.channels() == 3)
	{
		cv::resize(projector_image, collage(right_large), large_image);
	}
	else
	{
		cv::Mat color;
		cv::resize(projector_image, color, large_image);
		cvtColor(color, collage(right_large), cv::COLOR_GRAY2BGR);
	}
}

void Helper::CreateCollageImage3And1(cv::Mat& collage,
                                     const cv::Mat& color_image,
                                     const cv::Mat& pattern_image_left,
                                     const cv::Mat& pattern_image_right,
                                     const cv::Mat& extra_image)
{
	const auto width  = 400 * 4 * 2; // 400 * 5 * 2 = 200 * 2 = 4000
	const auto height = 300 * 3 * 2; // 300 * 3 * 2 = 900 * 2 = 1800

	// create the output image containing all images
	collage.create(height, width, CV_8UC3);

	cv::Size small_image(width / 4, height / 3);
	cv::Size large_image(3 * small_image.width, small_image.height * 3);

	// positions of 3 sub images
	cv::Rect top_left(0, 0, small_image.width, small_image.height);                         // (0,0,800,600)
	cv::Rect middle_left(0, small_image.height, small_image.width, small_image.height);     // (0, 600,800,600)
	cv::Rect bottom_left(0, 2 * small_image.height, small_image.width, small_image.height); // (0,1200,800,600)
	cv::Rect right_large(small_image.width, 0, large_image.width, large_image.height);      // (800,0,3200,1800)

	//resize color image and convert if necessary
	if(color_image.channels() == 3)
	{
		cv::resize(color_image, collage(top_left), small_image);
	}
	else
	{
		cv::Mat color;
		cv::resize(color_image, color, small_image);
		cvtColor(color, collage(top_left), cv::COLOR_GRAY2BGR);
	}

	// resize patter0 image and convert if necessary
	if(pattern_image_left.channels() == 3)
	{
		cv::resize(pattern_image_left, collage(top_left), small_image);
	}
	else
	{
		cv::Mat color;
		cv::resize(pattern_image_left, color, small_image);
		cvtColor(color, collage(top_left), cv::COLOR_GRAY2BGR);
	}

	// resize patter1 image and convert if necessary
	if(pattern_image_right.channels() == 3)
	{
		cv::resize(pattern_image_right, collage(middle_left), small_image);
	}
	else
	{
		cv::Mat color;
		cv::resize(pattern_image_right, color, small_image);
		cvtColor(color, collage(middle_left), cv::COLOR_GRAY2BGR);
	}

	// Display image info
	cv::putText(collage,
	            "IR1/Left in FS view direction",
	            cv::Point(top_left.x + 15, top_left.y + 50),
	            cv::FONT_HERSHEY_DUPLEX,
	            1.0,
	            Constants::WHITE,
	            1,
	            2);
	cv::putText(collage,
	            "IR0/Right in FS view direction",
	            cv::Point(middle_left.x + 15, middle_left.y + 50),
	            cv::FONT_HERSHEY_DUPLEX,
	            1.0,
	            Constants::WHITE,
	            1,
	            2);

	// resize extra image and convert if necessary
	if(extra_image.data)
	{
		if(extra_image.channels() == 3)
		{
			cv::resize(extra_image, collage(right_large), large_image);
		}
		else
		{
			cv::Mat color;
			cv::resize(extra_image, color, large_image);
			cvtColor(color, collage(right_large), cv::COLOR_GRAY2BGR);
		}
	}
}

double Helper::PositionFactor(const size_t frame_count,
                              const size_t positions_count)
{
	return double(frame_count) / double(positions_count - 1);
}

void Helper::CreateCollage(const cv::Mat& pattern_img0,
                           const cv::Mat& pattern_img1,
                           cv::Mat& collage)
{
	const auto width  = pattern_img0.cols + pattern_img1.cols;
	const auto height = pattern_img0.rows;

	//Create the output image containing all images
	cv::Mat temp_collage;
	temp_collage.create(height, width, CV_8UC3);
	collage.create(height, width, CV_8UC3);

	cv::Mat images[] = {
		cv::Mat(pattern_img0.rows, pattern_img0.cols, CV_8UC3),
		cv::Mat(pattern_img1.rows, pattern_img1.cols, CV_8UC3)
	};

	if(pattern_img0.channels() != Constants::NUMBER_OF_CHANNELS)
		cvtColor(pattern_img0, images[0], cv::COLOR_GRAY2BGR);
	else
		pattern_img0.copyTo(images[0]);

	if(pattern_img1.channels() != Constants::NUMBER_OF_CHANNELS)
		cvtColor(pattern_img1, images[1], cv::COLOR_GRAY2BGR);
	else
		pattern_img1.copyTo(images[1]);

	cv::hconcat(images, 2, temp_collage);

	//Positions of 3 sub images
	const cv::Rect top_left(0, 0, width / 2, height);
	const cv::Rect top_right(width / 2, 0, width, height);

	//Display image info
	cv::putText(temp_collage,
	            "IR1/Left in FS view direction",
	            cv::Point(top_right.x + 15, top_right.y + 50),
	            cv::FONT_HERSHEY_DUPLEX,
	            1.5,
	            Constants::WHITE,
	            2,
	            cv::LINE_AA);

	cv::putText(temp_collage,
	            "IR0/Right in FS view direction",
	            cv::Point(top_left.x + 15, top_left.y + 50),
	            cv::FONT_HERSHEY_DUPLEX,
	            1.5,
	            Constants::WHITE,
	            2,
	            cv::LINE_AA);

	collage = temp_collage.clone();
}

unsigned char Helper::CalculateLedPower(const size_t frame_count)
{
	const auto position_factor = PositionFactor(frame_count, Constants::NUMBER_CALIBRATION_POSITIONS);
	return Constants::LED_BASE_POWER + unsigned char(Constants::LED_VARIABLE_POWER * position_factor);
}

double Helper::GetLinearStagePosition(IKinematics* linear_stage)
{
	auto position                        = linear_stage->GetPosition();
	const auto current_position_in_meter = position->at<double>(0, 3);
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Current linear stage position: " << current_position_in_meter;
	delete position;
	return current_position_in_meter;
}

void Helper::JogForward(IKinematics* linear_stage,
                        const double step_size)
{
	if(linear_stage)
	{
		const auto current_position_in_meter = GetLinearStagePosition(linear_stage);
		if(current_position_in_meter + step_size < Constants::LINEAR_STAGE_LENGTH)
		{
			linear_stage->MoveXAxis(std::min(current_position_in_meter + step_size, Constants::LINEAR_STAGE_LENGTH));
			linear_stage->WaitForMovementToFinish();
		}
	}
}

void Helper::JogForward(int type,
                        void* data)
{
	const auto linear_stage = reinterpret_cast<IKinematics*>(data);
	JogForward(linear_stage, Constants::LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION);
}

void Helper::JogBackward(IKinematics* linear_stage,
                         const double step_size)
{
	if(linear_stage)
	{
		const auto current_position_in_meter = GetLinearStagePosition(linear_stage);

		if(current_position_in_meter >= step_size)
		{
			linear_stage->MoveXAxis(current_position_in_meter - step_size);
			linear_stage->WaitForMovementToFinish();
		}
	}
}

void Helper::JogBackward(int type,
                         void* data)
{
	const auto linear_stage = reinterpret_cast<IKinematics*>(data);
	JogBackward(linear_stage, Constants::LINEAR_STAGE_STEPPING_PROJECTOR_CALIBRATION);
}

std::string Helper::GetCaptureDeviceIdent()
{
	// FS2 device have no serial sometimes. Just use the deviceID
	if(GetSerialNumber().empty())
		return GetDeviceType();

	return GetDeviceType() + "_" + GetSerialNumber();
}

std::string Helper::GetSerialNumber()
{
	return std::string("DMVS_DEVICE");
}

std::string Helper::GetDeviceType()
{
	return std::string("DMVS_DEVICE");
}

std::string Helper::GetCaptureDeviceFile()
{
	return GetCaptureDeviceIdent() + ".xml";
}

cv::Mat Helper::AverageImages(const ImageVector& images)
{
	// define 32 bit type with appropriate number of channels for image accumulation
	const auto image_type_int = CV_MAKETYPE(CV_32S, images[0].channels());

	// create img for accumulation
	cv::Mat image_sum = cv::Mat::zeros(images[0].rows, images[0].cols, image_type_int);

	for(auto i = 0; i < images.size(); ++i)
	{
		cv::Mat normalized_image_int;
		images[i].convertTo(normalized_image_int, image_type_int);
		cv::add(image_sum, normalized_image_int, image_sum);
	}

	// spread image to max 255
	image_sum /= images.size();

	// convert to 8 bit char image
	cv::Mat spread_8bit;
	image_sum.convertTo(spread_8bit, images[0].type());
	return spread_8bit.clone();
}

void Helper::CopyReportData(const std::string& dest_path)
{
	//auto exePath = Directory::getdire(getApplicationFilePath());
	//auto src = exePath + L"FreestyleProduction\\FactoryReport\\*";
	//quoteString(src);
	//std::string dst = destPath;
	//quoteString(dst);
	//std::string options(" /I /Y /D");
	//std::string cmdString("xcopy ");
	//cmdString << src << " ";
	//cmdString << dst << " ";
	//cmdString << options;

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << Constants::FUNCTION_NOT_IMPLEMENTED;
	//system(cmdString.c_str());
}

//void Helper::ReportErrorMessage(const std::string& error_msg,
//                                const Messages::RESULT ls_result)
//{
//	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Internal Exception! Code:" << ls_result << "; Message: " << error_msg;
//}

int Helper::ComparePathComponents(const std::string& a,
                                  const std::string& b)
{
#ifdef _WINDOWS

	return boost::iequals(a, b);

#elif defined (_LINUX)
		return a.compare(b);

#else

#error Unknown platform!

#endif
}

fs::path Helper::GetDataFolder(const DefaultDirectory directory)
{
	TCHAR program_data_folder[1024];
	program_data_folder[0] = '\0';

#ifdef _WINDOWS
	if(FAILED(SHGetFolderPath(nullptr, CSIDL_COMMON_APPDATA, nullptr, 0, program_data_folder)))
	{
		return "";
	}
#else
		program_data_folder[0] = '.';
		program_data_folder[1] = '\0';

#endif

	fs::path data_folder(program_data_folder);

	switch(directory)
	{
	case SCENE_DIRECTORY:
		return data_folder.append("FARO");

	case FREESTYLE_DIRECTORY:
		return data_folder.append("FARO").append("Freestyle");

	case CAMERAS_DIRECTORY:
		return data_folder.append("FARO").append("Freestyle").append("Cameras");

	case CUSTOMIZE_DIRECTORY:
		return data_folder.append("FARO").append("Freestyle").append("Customize");

	case CALIBRATIONS_DIRECTORY:
		return data_folder.append("FARO").append("Freestyle").append("Calibrations");

	case CALIBRATIONS_IMAGE:
		return data_folder.append("FARO").append("Freestyle").append("Calibrations").append("Images");

	case CALIBRATIONS_IMAGE_AVERAGE:
		return data_folder.append("FARO").append("Freestyle").append("Calibrations").append("Images").append("Average");

	case CALIBRATIONS_IMAGE_COLLAGE:
		return data_folder.append("FARO").append("Freestyle").append("Calibrations").append("Images").append("Collage");

	case CALIBRATIONS_IMAGE_ALL:
		return data_folder.append("FARO").append("Freestyle").append("Calibrations").append("Images").append("All");

	case PROGRAM_DATA_DIRECTORY:
		return data_folder;

	default:
		throw std::exception("Directory folder not available");
	}
}

fs::path Helper::GetCalibrationsDirectory()
{
	return GetDataFolder(CALIBRATIONS_DIRECTORY);
}

fs::path Helper::GetCalibrationImagesDirectory()
{
	return GetDataFolder(CALIBRATIONS_IMAGE);
}

fs::path Helper::GetCalibrationImagesAverageDirectory()
{
	return GetDataFolder(CALIBRATIONS_IMAGE_AVERAGE);
}

fs::path Helper::GetCalibrationImagesCollageDirectory()
{
	return GetDataFolder(CALIBRATIONS_IMAGE_COLLAGE);
}

fs::path Helper::GetCalibrationImagesAllDirectory()
{
	return GetDataFolder(CALIBRATIONS_IMAGE_ALL);
}

fs::path Helper::GetCamerasDirectory()
{
	return GetDataFolder(CAMERAS_DIRECTORY);
}

fs::path Helper::GetSceneDirectory()
{
	return GetDataFolder(SCENE_DIRECTORY);
}

fs::path Helper::GetFreeStyleDirectory()
{
	return GetDataFolder(FREESTYLE_DIRECTORY);
}

fs::path Helper::GetCustomizedDirectory()
{
	return GetDataFolder(CUSTOMIZE_DIRECTORY);
}

fs::path Helper::GetProgramDataDirectory()
{
	return GetDataFolder(PROGRAM_DATA_DIRECTORY);
}

std::string Helper::GetWindowsError(const DWORD last_error)
{
	if(last_error == 0)
		return "";

	//wchar_t* buffer = nullptr;
	//::FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, 0, lastError, 0, (LPWSTR)&buffer, 0, nullptr);

	///*if(!buffer)
	//	return "[0x%08x]", lastError);*/

	//wchar_t* s(buffer);

	////// Remove line break(s) from the end of the returned message string
	////{
	////	const int length = std::wcslen(s);
	////	auto pos         = length - 1;

	////	while((pos >= 0) && ((s[pos] == L'\r') || (s[pos] == L'\n')))
	////		pos--;

	////	/*if(pos < length - 1)
	////	{
	////		wchar_t* pwc = std::wcsstr(s, "");
	////		s = s.substr(0, pos + 1);
	////	}*/
	////}

	//if(buffer)
	//	::LocalFree(buffer);

	//return Helper::Narrow(s);
	return "";
}

std::string Helper::GetWindowsError()
{
	return GetWindowsError(GetLastError());
}

bool Helper::IsValidPathComponent(const std::string& s)
{
#ifdef _WINDOWS
	static const std::string invalidEndChars(" .");
	static const std::string invalidChars("*?<>:\"\\/|");
#elif defined _LINUX
		static const std::string invalidEndChars("");
		static const std::string invalidChars("/");
#endif

	// Directory controls are valid
	if(s == "." || s == "..")
		return true;

	// Empty components are not valid
	auto len = s.length();

	if(len == 0)
		return false;

	// Test each char; characters 0-31 and some other chars are
	// not valid.
	for(auto i = 0; i < len; ++i)
	{
		wchar_t c = s.at(i);
		if((c < 32) || (invalidChars.find(c) > -1))
			return false;
	}

	// Test the last char against additional forbidden characters
	if(invalidEndChars.find(s.at(len - 1)) > -1)
		return false;

	return true;
}

bool Helper::IsValidSearchString(const std::string& s)
{
#ifdef _WINDOWS
	static const std::string invalidEndChars(" .");
	static const std::string invalidChars("<>:\"\\/|");
#elif defined _LINUX
		static const std::string invalidEndChars("");
		static const std::string invalidChars("/");
#endif
	if(s.empty() || s == "." || s == "..")
		return false;

	// Test each char; characters 0-31 and some other chars are
	// not valid.
	auto len = s.length();
	for(auto i = 0; i < len; ++i)
	{
		wchar_t c = s.at(i);
		if((c < 32) || (invalidChars.find(c) > -1))
			return false;
	}

	// Test the last char against additional forbidden characters
	if(invalidEndChars.find(s.at(len - 1)) > -1)
		return false;

	return true;
}

IOMessages::IOResult Helper::Read(const fs::path& file,
                                  std::shared_ptr<ConfigurationContainer> container)
{
	if(file.empty())
		return IOMessages::IO_FALSE;

	if(!exists(file))
		return IOMessages::IO_FALSE;

	std::ifstream file_stream(file.c_str(), std::ios::in);

	if(!file_stream.is_open())
		return IOMessages::IO_OPEN_READ;

	const auto context = std::make_shared<ReadContextBase>();
	ReadFromScope scope(&file_stream, context.get());
	auto& model_in_format = scope.GetModelInFormat();
	scope.GetReadContext()->SetFormat(model_in_format.DetermineFormat());

	std::string head;
	model_in_format.ReadHeader(head);

	if(head.empty())
		return IOMessages::IO_UNKNOWN_FORMAT;

	Version ver;
	model_in_format.ReadVersion(ver);

	/*imf.readFaroStart();
	int res = container->readFrom(in, context);
	imf.readFaroEnd();*/

	file_stream.close();
	return IOMessages::IO_OK;
}

std::string Helper::GetNowTime()
{
	// Get current time
	char str[26];
	auto t = std::time(nullptr);
	ctime_s(str, sizeof str, &t);
	std::string current_time(str);
	boost::trim_right_if(current_time,
	                     [](const char c)
	                     {
		                     return c == '\n';
	                     });

	return std::string(str);
}

void Helper::ToVec2D(const std::vector<double> vec,
                     cv::Vec2d& vec2d)
{
	if(vec.size() == 2)
	{
		vec2d[0] = vec[0];
		vec2d[1] = vec[1];
	}
}

void Helper::ToVec3D(std::vector<double> vec,
                     cv::Vec3d& vec3d)
{
	if(vec.size() == 3)
	{
		vec3d[0] = vec[0];
		vec3d[1] = vec[1];
		vec3d[2] = vec[2];
	}
}

void Helper::ToMatrix3x3(const std::vector<double> vec,
                         cv::Mat& mat3x3)
{
	if(vec.size() == 9)
	{
		mat3x3 = cv::Mat(3, 3, CV_64F);

		for(auto i = 0; i < 3; i++)
		{
			for(auto j = 0; j < 3; j++)
			{
				mat3x3.at<double>(i, j) = vec[(i * 3) + j];
			}
		}
	}
}

void Helper::ToDistortionMatrix(const std::vector<double> vec,
                                cv::Mat& mat5x1)
{
	if(vec.size() == 5)
	{
		mat5x1 = cv::Mat(5, 1, CV_64F);

		for(auto i = 0; i < 5; i++)
		{
			mat5x1.at<double>(i, 0) = vec[i];
		}
	}
}

void Helper::ToMatrix4x4(const std::vector<double> vec,
                         cv::Mat& mat4x4)
{
	if(vec.size() == 16)
	{
		mat4x4 = cv::Mat(4, 4, CV_64F);

		for(auto i = 0; i < 4; i++)
		{
			for(auto j = 0; j < 4; j++)
			{
				mat4x4.at<double>(i, j) = vec[(i * 4) + j];
			}
		}
	}
}

void Helper::ToExtrinsicsParameters(const std::vector<double>& vec,
                                    cv::Mat& rotation_matrix,
                                    cv::Mat& translation_vector)
{
	if(vec.size() == 16)
	{
		rotation_matrix = cv::Mat(3, 3, CV_64F);

		for(auto i = 0; i < 3; i++)
		{
			for(auto j = 0; j < 3; j++)
			{
				rotation_matrix.at<double>(i, j) = vec[(i * 4) + j];
			}
		}

		translation_vector                  = cv::Mat(3, 1, CV_64F);
		translation_vector.at<double>(0, 0) = vec[3];
		translation_vector.at<double>(1, 0) = vec[7];
		translation_vector.at<double>(2, 0) = vec[11];
	}
}

void Helper::ToLaserBeamCustomMatrix(const std::vector<double>& laser_beams,
                                     cv::Mat& laser_beams_matrix)
{
	if(laser_beams.size() == 46660)
	{
		laser_beams_matrix = cv::Mat(11665, 4, CV_64F);

		for(auto i = 0; i < 11665; i++)
		{
			for(auto j = 0; j < 4; j++)
			{
				laser_beams_matrix.at<double>(i, j) = laser_beams[i * 4 + j];
			}
		}
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Laser beams size Actual [" << laser_beams.size() << "] Expected  [ 46660 ]";
	}
}

void Helper::ToVector(const cv::Vec2d& vec2d,
                      std::vector<double>& vec)
{
	vec.push_back(vec2d[0]);
	vec.push_back(vec2d[1]);
}

void Helper::ToVector(const cv::Vec3d& vec3d,
                      std::vector<double>& vec)
{
	vec.push_back(vec3d[0]);
	vec.push_back(vec3d[1]);
	vec.push_back(vec3d[2]);
}

void Helper::ToVector(const cv::Mat& mat,
                      std::vector<double>& vec)
{
	const auto rows = mat.rows;
	const auto cols = mat.cols;

	for(auto row = 0; row < rows; row++)
	{
		for(auto col = 0; col < cols; col++)
		{
			vec.push_back(mat.at<double>(row, col));
		}
	}
}

//bool Helper::SplitPathString(const std::string& path,
//                             const int start_pos,
//                             std::vector<std::string>& components)
//{
//	components.clear();
//
//	auto current_pos  = start_pos;
//	const auto length = int(path.length());
//
//	while(current_pos < length)
//	{
//		std::string component;
//
//#ifdef _WINDOWS
//		const auto back_slash_position = path.find('\\', current_pos);
//
//#elif defined _LINUX
//
//		const auto back_slash_position = path.find('/', currentPos);
//#endif
//
//		if(back_slash_position >= 0 && back_slash_position < length)
//		{
//			component   = path.substr(current_pos, back_slash_position - current_pos);
//			current_pos = int(back_slash_position + 1);
//		}
//		else
//		{
//			component   = path.substr(current_pos);
//			current_pos = int(path.length());
//		}
//
//		// Skip empty segments
//		if(component.empty())
//			continue;
//
//		if(!IsValidPathComponent(component))
//		{
//			const auto error_message = "The path or file name" + path + " contains invalid characters";
//			ReportErrorMessage(error_message, Messages::UNKNOWN);
//			return false;
//		}
//
//		// Add the component to the path
//		components.push_back(component);
//	}
//
//	return true;
//}

double Helper::AngleTo(const cv::Vec3d& vec1,
                       const cv::Vec3d& vec2)
{
	const auto abs_v1 = cv::norm(vec1);
	const auto abs_v2 = cv::norm(vec2);

	if(abs_v1 <= 1e-5 || abs_v2 <= 1e-5)
	{
		return 0.0;
	}

	const auto length = vec1.dot(vec2);
	auto cos_phi      = length / abs_v1 / abs_v2;

	// Unfortunate numerical effects could cause cos_phi not to be in [-1, 1].
	cos_phi = std::max(-1.0, std::min(1.0, cos_phi));

	return acos(cos_phi);
}

double Helper::AngleTo(const cv::Vec2d& vec1,
                       const cv::Vec2d& vec2)
{
	const auto abs_v1 = cv::norm(vec1);
	const auto abs_v2 = cv::norm(vec2);

	if(abs_v1 <= 1e-5 || abs_v2 <= 1e-5)
	{
		return 0.0;
	}

	const auto length = vec1.dot(vec2);
	auto cos_phi      = length / abs_v1 / abs_v2;

	// Unfortunate numerical effects could cause cos_phi not to be in [-1, 1].
	cos_phi = std::max(-1.0, std::min(1.0, cos_phi));

	return acos(cos_phi);
}

double Helper::VectorLength(const cv::Vec3d vec)
{
	return cv::norm(vec);
}

double Helper::VectorLength(const cv::Vec3d vec1,
                            const cv::Vec3d vec2)
{
	return vec1.dot(vec2);
}

double Helper::VectorLength(cv::Vec2d vec)
{
	return cv::norm(vec);
}

const wchar_t* Helper::Widen(const char* source)
{
	const auto size      = std::strlen(source) + 1;
	const auto wide_char = new wchar_t[size];

	size_t converted_chars = 0;
	mbstowcs_s(&converted_chars, wide_char, size, source, _TRUNCATE);

	return wide_char;
}

const char* Helper::Narrow(const wchar_t* source)
{
	// Convert the wchar_t string to a char* string. Record
	// the length of the original string and add 1 to it to
	// account for the terminating null character.
	const auto original_size = wcslen(source) + 1;
	size_t converted_chars   = 0;

	// Use a multi-byte string to append the type of string
	// to the new string before displaying the result.
	char str_concat[]          = " (char *)";
	const auto str_concat_size = (strlen(str_concat) + 1) * 2;

	// Allocate two bytes in the multi-byte output string for every wide
	// character in the input string (including a wide character
	// null). Because a multi-byte character can be one or two bytes,
	// you should allot two bytes for each character. Having extra
	// space for the new string is not an error, but having
	// insufficient space is a potential security problem.
	const auto new_size = original_size * 2;

	// The new string will contain a converted copy of the original
	// string plus the type of string appended to it.
	const auto new_string = new char[new_size + str_concat_size];

	// Put a copy of the converted string into new_string
	wcstombs_s(&converted_chars, new_string, new_size, source, _TRUNCATE);

	// append the type of string to the new string.
	//_mbscat_s(reinterpret_cast<unsigned char*>(new_string), new_size + str_concat_size, reinterpret_cast<unsigned char*>(str_concat));

	return new_string;
}
}
}
