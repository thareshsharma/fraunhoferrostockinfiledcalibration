#include "device_description.h"

DeviceDescription::DeviceDescription() :
	type(DETECT_CAPTURE),
	state(UNKNOWN),
	width(0),
	height(0),
	minimum_z_value(0.1),
	maximum_z_value(50.0),
	default_z_value(5.0),
	serial(""),
	serial_number("")
{
}

DeviceDescription::DeviceDescription(const enum DeviceType device_type,
                                     const enum DeviceState device_state,
                                     const std::string& serial_number) :
	type(device_type),
	state(device_state),
	width(0),
	height(0),
	minimum_z_value(0),
	maximum_z_value(0),
	default_z_value(0),
	serial(serial_number),
	serial_number(serial_number)
{
}

DeviceDescription::~DeviceDescription()
= default;

bool DeviceDescription::Equals(const DeviceDescription& other) const
{
	return type == other.type && state == other.state;
}

bool DeviceDescription::HasComponent(const Component component) const
{
	return std::find(components.begin(), components.end(), component) != components.end();
}

bool DeviceDescription::HasCapability(const CapabilityType capability_type) const
{
	return std::find(capabilities.begin(), capabilities.end(), capability_type) != capabilities.end();
}
