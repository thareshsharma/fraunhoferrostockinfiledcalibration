#include "reconstructor_laser_dots_3D_back_projection.h"

#include "helper.h"

using namespace Reconstruct;

ReconstructorLaserDots3DBackProjection::ReconstructorLaserDots3DBackProjection() :
	ReconstructorLaserDots3D(),
	projector_back_projection_uncertainty_(0.4)
{
}

ReconstructorLaserDots3DBackProjection::~ReconstructorLaserDots3DBackProjection()
= default;

bool ReconstructorLaserDots3DBackProjection::Preparation()
{
	const auto success = ReconstructorLaserDots3D::Preparation();

	if(use_projector)
	{
		camera_sensor0.PreCalculateRelativeTransformation(camera_sensor1, projector);
	}

	return success;
}

void ReconstructorLaserDots3DBackProjection::SetUncertaintiesBackProjection(const double ir0,
                                                                            const double ir1,
                                                                            const double proj,
                                                                            const double back_proj)
{
	projector_back_projection_uncertainty_ = back_proj;
	SetUncertainties(ir0, ir1, proj);
}

void ReconstructorLaserDots3DBackProjection::FindTripleCorrespondences()
{
	triples_.clear();

	camera_sensor0.ResetDotUseCounter();
	camera_sensor1.ResetDotUseCounter();

	if(use_projector)
	{
		projector.ResetDotUseCounter();
	}

	const auto cam0_dots = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index];
	const auto cam0_pts  = int(cam0_dots.size());

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cam0_pts " << cam0_pts;

	// preallocate empty vector for every cam0 dot
	std::vector<std::vector<TripleCorrespondence>> local_triples(cam0_pts);

	// Loop all cam0 dots and find potential correspondences
	//tbb::parallel_for(size_t(0),
	//                  camera_sensor0.rectified_coordinates[camera_sensor1.camera_index].size(),
	//                  [&](const size_t idx0)


	/*std::ofstream output;
	auto output_path = R"(D:\TestData\output\camera_sensor0.txt)" + std::to_string(camera_sensor1.camera_index) + "_" + Helper::GetTimeStamp() + ".txt";
	output.open(output_path);
	if(output.is_open())
	{
		const auto rectified_coordinate_empty_1 = !camera_sensor0.rectified_coordinates[0].empty();
		const auto rectified_coordinate_empty_2 = !camera_sensor0.rectified_coordinates[1].empty();
		const auto rectified_coordinate_empty_3 = !camera_sensor0.rectified_coordinates[2].empty();

		for(size_t i = 0; i < camera_sensor0.rectified_coordinates[camera_sensor1.camera_index].size(); i++)
		{
			output << camera_sensor0.original_coordinates.at(i).x << " "
				<< camera_sensor0.original_coordinates.at(i).y << " "
				<< " 1 ";

			if(rectified_coordinate_empty_1)
			{
				output
					<< camera_sensor0.rectified_coordinates[0].at(i).x << " "
					<< camera_sensor0.rectified_coordinates[0].at(i).y << " "
					<< " 1 ";
			}

			if(rectified_coordinate_empty_2)
			{
				output << camera_sensor0.rectified_coordinates[1].at(i).x << " "
					<< camera_sensor0.rectified_coordinates[1].at(i).y << " "
					<< " 1 ";
			}
			if(rectified_coordinate_empty_3)
			{
				output << camera_sensor0.rectified_coordinates[2].at(i).x << " "
					<< camera_sensor0.rectified_coordinates[2].at(i).y;
			}
			output << "\n";
		}

		output.close();
	}*/


	for(int idx0 = 0; idx0 < camera_sensor0.rectified_coordinates[camera_sensor1.camera_index].size(); ++idx0)
	{
		// calculate indices of lookup tables
		const auto idx1 = camera_sensor1.GetEpipolarLineIndex(camera_sensor0,
		                                                      camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][idx0
		                                                      ].y);
		const auto idx2 = use_projector
			                  ? projector.GetEpipolarLineIndex(camera_sensor0,
			                                                   camera_sensor0.rectified_coordinates[projector.camera_index][idx0].y)
			                  : 1;

		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : idx1 " << idx1 << " idx2 " << idx2 << " idx0"  << idx0;
		// store triples to pre-allocated vector
		local_triples[idx0] = FindIntersectionByBackProjection(idx0, idx1, idx2);
	}
	//});

	// synchronous result assembling:  final triple vector and usage update
	for(auto idx0 = 0; idx0 < cam0_pts; ++idx0)
	{
		const auto& dot_triples = local_triples[idx0];

		// insert dot triples to all triples
		triples_.insert(triples_.end(), dot_triples.begin(), dot_triples.end());

		// increment all usage counters
		for(auto i_tr = 0; i_tr < int(local_triples[idx0].size()); ++i_tr)
		{
			// increase usage counter of cams and projector
			camera_sensor0.counter_used[dot_triples[i_tr].camera_index[0]]++;
			camera_sensor1.counter_used[dot_triples[i_tr].camera_index[1]]++;

			if(use_projector)
			{
				projector.counter_used[dot_triples[i_tr].camera_index[2]]++;
			}
		}
	}
}

std::vector<ReconstructorLaserDots3D::TripleCorrespondence> ReconstructorLaserDots3DBackProjection::FindIntersectionByBackProjection(
	const int cam0_index,
	const int corr_idx1,
	const int corr_idx2) const
{
	std::vector<TripleCorrespondence> result;

	if(corr_idx1 < 0 ||
		corr_idx1 >= int(camera_sensor1.lookup_correspondences.size()) ||
		corr_idx2 < 0 ||
		corr_idx2 >= int(projector.lookup_correspondences.size()))
	{
		return result;
	}

	const auto& cam1_candidates = camera_sensor1.lookup_correspondences[corr_idx1];
	const auto& pro0_candidates = projector.lookup_correspondences[corr_idx2];

	if(cam1_candidates.empty() || pro0_candidates.empty())
	{
		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cam1_candidates.empty() || pro0_candidates.empty() ";
		return result;
	}

	constexpr auto max_num_correspondences = 9;
	result.reserve(max_num_correspondences);

	const auto inv_dir_cam0_cam1 = camera_sensor0.IsCameraRotated(camera_sensor1.camera_index)
		                               ? true
		                               : camera_sensor0.GetQ(camera_sensor1.camera_index).at<double>(3, 2) < 0;

	const auto inv_dir_cam0_proj = (camera_sensor0.GetQ(projector.camera_index).at<double>(3, 2) < 0);

	// cam0 measurement, rectified to cam1 component parallel to epipolar line
	const double cam0_cam1_par_coord = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][cam0_index].x;

	// cam0 measurement, rectified to proj0 component parallel to epipolar line
	const double cam0_proj_par_coord = camera_sensor0.rectified_coordinates[projector.camera_index][cam0_index].x;

	auto pro0_iteration_low  = projector.lookup_correspondences[corr_idx2].begin();
	const auto& pro0_stop    = projector.lookup_correspondences[corr_idx2].end();
	auto pro0_iteration_high = pro0_iteration_low;

	for(auto cam1_iterator = cam1_candidates.begin(); cam1_iterator != cam1_candidates.end(); ++cam1_iterator)
	{
		const auto cam1_candidates_idx = *cam1_iterator;
		const auto cam1_cam0_par_coord = camera_sensor1.rectified_coordinates[camera_sensor0.camera_index][cam1_candidates_idx].x;

		if(!inv_dir_cam0_cam1 && cam1_cam0_par_coord > cam0_cam1_par_coord ||
			inv_dir_cam0_cam1 && cam1_cam0_par_coord < cam0_cam1_par_coord)
		{
			// actually a break should do the same more efficiently, but it does not
			// It should because candidate vector is sorted by coordinate in direction epipolar line
			continue;
		}

		// e3-e4: exactly check Cam0-Cam1 relation because being member of candidate lists
		// is only a required but not a sufficient criterion.
		// The selection by candidate lists according to bins was too coarse
		const double e3 = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][cam0_index].y;
		const double e4 = camera_sensor1.rectified_coordinates[camera_sensor0.camera_index][cam1_candidates_idx].y;

		//VGDebug(L" CORRESPONDENCE STEP 2 %f ", fabs(e3-e4) );
		// e5-e6: Cam0-Pro0-relation
		if(fabs(e3 - e4) <= camera_sensor0.uncertainty + camera_sensor1.uncertainty)
		{
			// get projector rectified coordinate of cam1 candidate
			const double e1 = camera_sensor1.rectified_coordinates[projector.camera_index][cam1_candidates_idx].y;

			// with every increase of the Cam1-iterator, the iterators Pro0iterlow and Pro0iterhigh are adjusted such that they
			// limit a range of possible correspondences in the Pro0 view
			auto pro0_low_index = *pro0_iteration_low;
			double e2_low       = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_low_index].y;

			while((e2_low < e1 - (camera_sensor1.uncertainty + projector.uncertainty)) && (pro0_iteration_low != pro0_stop))
			{
				pro0_low_index = *pro0_iteration_low;
				++pro0_iteration_low;

				if(pro0_iteration_low == pro0_stop)
				{
					--pro0_iteration_low;
					break;
				}

				pro0_low_index = *pro0_iteration_low;
				e2_low         = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_low_index].y;
			}

			auto pro0_high_index = *pro0_iteration_high;
			double e2_high       = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_high_index].y;

			while(e2_high < e1 + (camera_sensor1.uncertainty + projector.uncertainty) && pro0_iteration_high != pro0_stop)
			{
				pro0_high_index = *pro0_iteration_high;
				++pro0_iteration_high;

				if(pro0_iteration_high == pro0_stop)
				{
					--pro0_iteration_high;
					break;
				}

				pro0_high_index = *pro0_iteration_high;
				e2_high         = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_high_index].y;
			}

			for(auto pro0_iter = pro0_iteration_low; pro0_iter <= pro0_iteration_high; ++pro0_iter)
			{
				const auto pro0_index = *pro0_iter;
				auto& projector_point = projector.rectified_coordinates[camera_sensor0.camera_index][pro0_index];

				//Check for negative disparity of cam0 and projector
				if(!inv_dir_cam0_proj && projector_point.x > cam0_proj_par_coord || inv_dir_cam0_proj && projector_point.x < cam0_proj_par_coord)
				{
					continue;
				}

				// get cam1 rectified coordinate of projector candidate
				const double e2 = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_index].y;

				//VGDebug(L"Cam0_index,Cam1_index,Pro0_index, %d, %d,%d,%f,%f", Cam0_index,Cam1_index,Pro0_index,e1, e2);
				//VGDebug(L" CORRESPONDENCE STEP 1 %f ", fabs(e1-e2) );

				if(fabs(e1 - e2) <= (camera_sensor1.uncertainty + projector.uncertainty))
				{
					//Check if we are on epipolar line
					// e5-e6: exactly check Cam0-Proj relation because being member of candidate lists
					// is only a required but not a sufficient criterion. (due to discretization)
					// The selection by candidate lists according to bins was too coarse
					const double e5 = camera_sensor0.rectified_coordinates[projector.camera_index][cam0_index].y;
					const double e6 = projector_point.y;

					if(fabs(e5 - e6) <= (camera_sensor0.uncertainty + projector.uncertainty))
					{
						cv::Point2f projected_point_projector;
						const auto disparity = cam0_cam1_par_coord - cam1_cam0_par_coord;

						//Calculate point in Projector 2D Space
						camera_sensor0.ProjectPointToThirdCamera(cam0_cam1_par_coord,
						                                         (e3 + e4) * 0.5,
						                                         disparity,
						                                         camera_sensor1.camera_index,
						                                         projector.camera_index,
						                                         projected_point_projector);

						const double parallel_diff      = fabs(projector_point.x - projected_point_projector.x);
						const double perpendicular_diff = fabs(projector_point.y - projected_point_projector.y);

						if(parallel_diff < projector_back_projection_uncertainty_ && perpendicular_diff < projector_back_projection_uncertainty_)
						{
							result.emplace_back(TripleCorrespondence(cam0_index, cam1_candidates_idx, pro0_index, 0));
						}
					}
				}
			}
		}
	}

	return result;
}
