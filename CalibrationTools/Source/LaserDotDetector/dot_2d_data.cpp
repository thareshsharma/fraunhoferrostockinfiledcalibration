#include "dot_2d_data.h"
#include "helper.h"
#include "camera_sensor.h"

using namespace Reconstruct;
using namespace DMVS::CameraCalibration;

Dot2DData::Dot2DData() :
	camera_index(0),
	uncertainty(0.0),
	calibration(nullptr),
	camera_rotated{false, false, false},
	perpendicular_coordinates{-1, -1, -1},
	min_epipolar_line{0.0, 0.0, 0.0},
	max_epipolar_line{0.0, 0.0, 0.0},
	bin_center_dist{0.0, 0.0, 0.0}
{
	//InitRotatedCams();
}

Dot2DData::Dot2DData(const int index) :
	camera_index(index),
	uncertainty(0),
	calibration(nullptr),
	matrix_q{cv::Mat(), cv::Mat(), cv::Mat()},
	camera_rotated{false, false, false},
	perpendicular_coordinates{-1, -1, -1},
	min_epipolar_line{0.0, 0.0, 0.0},
	max_epipolar_line{0.0, 0.0, 0.0},
	bin_center_dist{0.0, 0.0, 0.0}
{
}

Dot2DData::~Dot2DData()
{
	calibration.reset();
	calibration = nullptr;
}

void Dot2DData::ResetDotUseCounter()
{
	counter_used.assign(original_coordinates.size(), 0);
}

void Dot2DData::ProjectPointToThirdCamera(const double x0,
                                          const double y_avg,
                                          const double disparity_cam_b,
                                          const int cam_b_index,
                                          const int cam_c_index,
                                          cv::Point2f& point2d_cam_c) const
{
	const Eigen::Vector4d ehom_point1(x0, y_avg, disparity_cam_b, 1);
	Eigen::Vector4d projected_point = projection_matrices_[cam_b_index][cam_c_index] * ehom_point1;
	point2d_cam_c.x                 = float(projected_point(0) / projected_point(2));
	point2d_cam_c.y                 = float(projected_point(1) / projected_point(2));
}

void Dot2DData::PreCalculateRelativeTransformation(Dot2DData& camera_b,
                                                   Dot2DData& camera_c)

{
	//First rotate back from this cams rectification with camB
	cv::Mat back_rotation    = cv::Mat::zeros(4, 4, CV_64F);
	const cv::Mat r2_inverse = rectification_2D_rotation_matrix_[camera_b.camera_index].inv();
	r2_inverse.copyTo(back_rotation(cv::Rect(0, 0, 3, 3)));
	back_rotation.at<double>(3, 3) = 1.0;

	//Forward Rotation to rectification with CamC
	cv::Mat forward_rotation = cv::Mat::zeros(4, 4, CV_64F);
	camera_c.GetR2(camera_index).copyTo(forward_rotation(cv::Rect(0, 0, 3, 3)));
	forward_rotation.at<double>(3, 3) = 1.0;

	//Concatenate all trafos including projection to CamC
	cv::Mat projection_matrix = projection_2D_rectification_matrix_[camera_c.camera_index] * forward_rotation * relative_transformation_[camera_c.camera_index] * back_rotation;

	projection_matrices_[camera_b.camera_index][camera_c.camera_index] = Eigen::Matrix4d::Zero();
	for(auto i = 0; i < 3; ++i)
	{
		for(auto j = 0; j < 4; ++j)
		{
			projection_matrices_[camera_b.camera_index][camera_c.camera_index](i, j) = projection_matrix.at<double>(i, j);
		}
	}

	eigen_q_[camera_b.camera_index] = Eigen::Matrix4d::Zero();

	for(auto i = 0; i < 4; ++i)
	{
		for(auto j = 0; j < 4; ++j)
		{
			eigen_q_[camera_b.camera_index](i, j) = matrix_q[camera_b.camera_index].at<double>(i, j);
		}
	}

	projection_matrices_[camera_b.camera_index][camera_c.camera_index] = projection_matrices_[camera_b.camera_index][camera_c.camera_index] * eigen_q_
		[camera_b.camera_index];
}

cv::Point2f Dot2DData::GetDot(const size_t idx) const
{
	return original_coordinates[idx];
}

size_t Dot2DData::DotsCount() const
{
	return original_coordinates.size();
}

bool Dot2DData::IfDotsEmpty() const
{
	return original_coordinates.empty();
}

Dots& Dot2DData::GetDots()
{
	return original_coordinates;
}

cv::Point2f Dot2DData::GetUndistortedDot(const size_t idx)
{
	return undistorted_coordinates[idx];
}

std::shared_ptr<const CameraSensor> Dot2DData::GetCalibration() const
{
	return calibration;
}

const cv::Mat& Dot2DData::GetR2(const int sensor_index) const
{
	return rectification_2D_rotation_matrix_[sensor_index];
}

const cv::Mat& Dot2DData::GetQ(const int sensor_index) const
{
	return matrix_q[sensor_index];
}

bool Dot2DData::IsCameraRotated(const int sensor_index) const
{
	return camera_rotated[sensor_index];
}

bool Dot2DData::SetCalibration(const std::shared_ptr<const CameraSensor> sensor_calibration)
{
	calibration = sensor_calibration;

	return calibration != nullptr;
}

void Dot2DData::SetDotCoords(const Dots& laser_dots)
{
	original_coordinates = laser_dots;
	if(camera_index == 2)
	{
		calibration->UndistortPointsLaser(laser_dots, undistorted_coordinates);
	}
	else
	{
		calibration->UndistortPoints2(laser_dots, undistorted_coordinates);
	}
	/*std::ofstream output;
	auto output_path = R"(D:\TestData\output\undistorted_coordinates_)" + std::to_string(camera_index) + "_" + Helper::GetTimeStamp() + ".txt";
	output.open(output_path);
	if(output.is_open())
	{
		for(size_t i = 0; i < undistorted_coordinates.size(); i++)
		{
			output << undistorted_coordinates.at(i).x << " "
				<< undistorted_coordinates.at(i).y << " "
				<< " 1 \n";
		}

		output.close();
	}*/

	counter_used.assign(original_coordinates.size(), 0);
}

void Dot2DData::StereoRectifyCams(Dot2DData& camera_2)
{
	//auto& cam1 = *this;
	cv::Mat r1;
	cv::Mat r2;
	cv::Mat p1;
	cv::Mat p2;
	cv::Mat q;
	cv::Mat rel_rotation;
	cv::Mat rel_location;
	p1.create(3, 4, CV_64FC1);
	p2.create(3, 4, CV_64FC1);
	cv::Rect valid_roi[2];

	auto image_size = cv::Size(1280, 1024);
	cv::Mat rot1, t1, rot2, t2;
	calibration->GetExtrinsicParameters(rot1, t1);
	camera_2.calibration->GetExtrinsicParameters(rot2, t2);
	rel_rotation = rot2 * rot1.inv();           //rotation of relative transformation between pair
	rel_location = t2 - rot2 * rot1.inv() * t1; //translation of relative transformation between pair

	cv::Mat cam_mat1;
	cv::Mat dist1;
	cv::Mat cam_mat2;
	cv::Mat dist2;
	calibration->GetBestFitCVModel(cam_mat1, dist1);

	if(camera_2.calibration->GetImageChannel() == CHANNEL_PROJECTOR0)
	{
		camera_2.calibration->GetBestFitCVModelLaser(cam_mat2, dist2);
	}
	else
	{
		camera_2.calibration->GetBestFitCVModel(cam_mat2, dist2);
	}

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cam_mat1 " << cam_mat1;
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : dist1 " << dist1;

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cam_mat2 " << cam_mat2;
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : dist2 " << dist2;

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : rel_rotation " << rel_rotation;
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : rel_location " << rel_location;

	// check whether stereo pair is horizontal or vertical
	auto coordinate_perpendicular_to_epipolar_lines = -1;

	//If one camera is rotated by 180 degree invert idx
	auto z_angle             = std::atan2(rel_rotation.at<double>(1, 0), rel_rotation.at<double>(0, 0));
	auto parallel_coordinate = 0;

	if(fabs(z_angle) > 2.0)
	{
		parallel_coordinate = StereoRectify2(cam_mat1,
		                                     dist1,
		                                     cam_mat2,
		                                     dist2,
		                                     image_size,
		                                     rel_rotation,
		                                     rel_location,
		                                     r1,
		                                     r2,
		                                     p1,
		                                     p2,
		                                     q,
		                                     cv::CALIB_ZERO_DISPARITY,
		                                     -1,
		                                     cv::Size(0, 0),
		                                     valid_roi[0],
		                                     valid_roi[1]);
		coordinate_perpendicular_to_epipolar_lines = (parallel_coordinate + 1) % 2;
		camera_rotated[camera_2.camera_index]      = true;
	}
	else
	{
		//Normal case
		cv::stereoRectify(cam_mat1,
		                  dist1,
		                  cam_mat2,
		                  dist2,
		                  image_size,
		                  rel_rotation,
		                  rel_location,
		                  r1,
		                  r2,
		                  p1,
		                  p2,
		                  q,
		                  cv::CALIB_ZERO_DISPARITY,
		                  -1,
		                  cv::Size(0, 0),
		                  &valid_roi[0],
		                  &valid_roi[1]);
		if(((int)p2.at<double>(1, 3)) != 0)
		{
			coordinate_perpendicular_to_epipolar_lines = 0;
		}
		else
		{
			coordinate_perpendicular_to_epipolar_lines = 1;
		}
	}

	relative_transformation_[camera_2.camera_index]                  = cv::Mat::zeros(4, 4, CV_64F);
	relative_transformation_[camera_2.camera_index].at<double>(3, 3) = 1.0;
	rel_rotation.copyTo(relative_transformation_[camera_2.camera_index](cv::Rect(0, 0, 3, 3)));
	relative_transformation_[camera_2.camera_index].at<double>(0, 3) = rel_location.at<double>(0, 0);
	relative_transformation_[camera_2.camera_index].at<double>(1, 3) = rel_location.at<double>(1, 0);
	relative_transformation_[camera_2.camera_index].at<double>(2, 3) = rel_location.at<double>(2, 0);

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : r1 " << r1;
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : p1 " << p1;

	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : r2 " << r2;
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : p2 " << p2;

	perpendicular_coordinates[camera_2.camera_index] = coordinate_perpendicular_to_epipolar_lines;
	camera_2.perpendicular_coordinates[camera_index] = coordinate_perpendicular_to_epipolar_lines;

	rectification_2D_rotation_matrix_[camera_2.camera_index]      = r1;
	projection_2D_rectification_matrix_[camera_2.camera_index]      = p1;
	matrix_q[camera_2.camera_index] = q;

	camera_2.rectification_2D_rotation_matrix_[camera_index]      = r2;
	camera_2.projection_2D_rectification_matrix_[camera_index]      = p2;
	camera_2.matrix_q[camera_index] = q;
}

void Dot2DData::RectifyPoints(Dot2DData& camera_b)
{
	/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : r2_ " << rectification_2D_rotation_matrix_[camera_b.camera_index];
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : p2_ " << projection_2D_rectification_matrix_[camera_b.camera_index];*/

	// Use as Eigen R matrix as Eigen
	const Eigen::Map<Eigen::Matrix3d, 0, Eigen::Stride<1, 3>> r((double*)rectification_2D_rotation_matrix_[camera_b.camera_index].ptr());

	// Use upper left 3x3 part part P matrix
	Eigen::Matrix3d p;
	if(projection_2D_rectification_matrix_[camera_b.camera_index].cols == 4)
	{
		p = Eigen::Map<Eigen::Matrix3d, 0, Eigen::Stride<1, 4>>((double*)projection_2D_rectification_matrix_[camera_b.camera_index].ptr());
	}
	else
	{
		p = Eigen::Map<Eigen::Matrix3d, 0, Eigen::Stride<1, 3>>((double*)projection_2D_rectification_matrix_[camera_b.camera_index].ptr());
	}

	Eigen::Matrix3d pr = p * r;
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : pr \n" << pr;
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : p  \n" << p;
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : r  \n" << r;

	// Reserve memory for CamB rectified coordinates
	rectified_coordinates[camera_b.camera_index].resize(undistorted_coordinates.size());
	//Helper::Sleeping(1, "Sleeping...");
	//std::ofstream output;
	//auto output_path = R"(D:\TestData\output\RectifyPoints_undistorted_coordinates_)" +std::to_string(camera_index) + "_" + Helper::GetTimeStamp() + ".txt";
	//output.open(output_path);
	//if(output.is_open())
	//{
	//	for(size_t i = 0; i < undistorted_coordinates.size(); i++)
	//	{
	//		output << undistorted_coordinates.at(i).x << " "
	//			<< undistorted_coordinates.at(i).y << " "
	//			<< " 1 \n";
	//	}

	//	output.close();
	//}

	for(size_t i = 0; i < undistorted_coordinates.size(); ++i)
	{
		// get undistorted but not yet rectified coordinates of this cam measurements
		const double x = undistorted_coordinates[i].x;
		const double y = undistorted_coordinates[i].y;

		const auto xx = (pr(0, 0) * x) + (pr(0, 1) * y ) + pr(0, 2);
		const auto yy = (pr(1, 0) * x) + (pr(1, 1) * y ) + pr(1, 2);
		const auto ww = 1. / ((pr(2, 0) * x )+ (pr(2, 1) * y ) + pr(2, 2));

		//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : xx " << xx << " yy " << yy << " ww " << ww;

		//Parallelcoordinate is 0 Perpendicular coordinate is 1
		rectified_coordinates[camera_b.camera_index][i].x = float(xx * ww);
		rectified_coordinates[camera_b.camera_index][i].y = float(yy * ww);
	}
}

int Dot2DData::GetEpipolarLineIndex(Dot2DData& camera_b,
                                    const double camera_a_coordinate)
{
	// return number of index in the lookup table corresponding
	//auto& cam_a = *this;

	const auto numerator   = camera_a_coordinate - min_epipolar_line[camera_b.camera_index];
	const auto denominator = bin_center_dist[camera_b.camera_index];

	/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : camera_a_coordinate " << camera_a_coordinate;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : camera_b " << camera_b.camera_index;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : numerator " << numerator;
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : denominator " << denominator;*/
	return int(ceil(numerator / denominator - 0.5));
}

void Dot2DData::GenerateLookupCorrespondences(Dot2DData& camera_b,
                                              Dot2DData& camera_c,
                                              const bool use_projector)
{
	//auto& cam_a = *this;

	//attention: the best size remains to be optimized but (CamA.uncertainty+CamB.uncertainty) might be close to optimum
	// every entry of the look-up table covers a range dependent on the required tolerances: the smaller the uncertainty the smaller the bins
	bin_center_dist[camera_b.camera_index] = (uncertainty + camera_b.uncertainty);

	/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : bin_center_dist[camera_b.camera_index] " << bin_center_dist[camera_b.camera_index];
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : uncertainty " << uncertainty << " camera_b.uncertainty" << camera_b.uncertainty;*/
	const auto bin_width = uncertainty + camera_b.uncertainty;

	// determine how many neighbor bins have to be considered in order not to miss a candidate
	// if bin_width is sum of uncertainties this 1 because it is always sufficient to check both neighbor bins
	const auto bin_widths_ratio = (int)ceil(bin_width / bin_center_dist[camera_b.camera_index] / 2);

	// VGDebug(L"CamA.bin_center_dist[CamB.Cam_idx], bin_width, bin_widths_ratio : %f, %f, %d",CamA.bin_center_dist[CamB.Cam_idx], bin_width, bin_widths_ratio);
	// get min and max epipolar lines:
	// ATTENTION, this can be simplified using fix numbers for max and min which  should come out of calibration

	min_epipolar_line[camera_b.camera_index] = -4000;
	max_epipolar_line[camera_b.camera_index] = 4000;

	//CamA.min_epipolar_line[CamB.Cam_idx]=CamA.Rect_coords[CamB.Cam_idx][0][CamA.perp_coord[CamB.Cam_idx]];
	//CamA.max_epipolar_line[CamB.Cam_idx]=CamA.Rect_coords[CamB.Cam_idx][0][CamA.perp_coord[CamB.Cam_idx]];
	//for (int i=1; i< CamA.Rect_coords[CamB.Cam_idx].size(); ++i)
	//{
	//	if (CamB.Rect_coords[CamA.Cam_idx][i][CamB.perp_coord[CamA.Cam_idx]] < CamA.min_epipolar_line[CamB.Cam_idx] )
	//		CamA.min_epipolar_line[CamB.Cam_idx]=CamB.Rect_coords[CamA.Cam_idx][i][CamB.perp_coord[CamA.Cam_idx]];
	//	if (CamB.Rect_coords[CamA.Cam_idx][i][CamB.perp_coord[CamA.Cam_idx]] > CamA.max_epipolar_line[CamB.Cam_idx] )
	//		CamA.max_epipolar_line[CamB.Cam_idx]=CamB.Rect_coords[CamA.Cam_idx][i][CamB.perp_coord[CamA.Cam_idx]];
	//}

	const auto epipolar_lines_count = (int)ceil(
		(max_epipolar_line[camera_b.camera_index] - min_epipolar_line[camera_b.camera_index]) / bin_center_dist[camera_b.camera_index]) + 1;
	//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Pushing to lookup_correspondences " << epipolar_lines_count << Constants::CHECK_ME;
	lookup_correspondences.resize(epipolar_lines_count);

	//CamA.Lookup_correspondences.clear();
	for(auto& lookup_correspondence : lookup_correspondences)
	{
		lookup_correspondence.clear();
	}

	// CamA.Lookup_correspondences.clear();
	// arrange all data points in bins
	// one dot is sorted into several bins according to the uncertainties.
	for(unsigned int i = 0; i < rectified_coordinates[camera_b.camera_index].size(); ++i)
	{
		//int idx = ceil((CamA.Rect_coords[CamB.Cam_idx][i][CamA.perp_coord[CamB.Cam_idx]]-CamA.min_epipolar_line[CamB.Cam_idx])/CamA.bin_center_dist[CamB.Cam_idx]);

		// get the index of what is perpendicular to the epipolar line
		// 0: x is perpendicular to epipolar lines, vertical camera setup
		// 1: y is perpendicular to epipolar lines, horizontal camera setup
		const auto perpendicular_direction = perpendicular_coordinates[camera_b.camera_index];

		// get the index of the look-up table entry to access all possible correspondences in camB of the current cam A coordinate
		// We need the component of the Cam A coordinate (rectified with respect to camB) which is perpendicular to the epipolar line
		const auto idx       = GetEpipolarLineIndex(camera_b, rectified_coordinates[camera_b.camera_index][i].y);
		const auto idx_start = idx - bin_widths_ratio > 0 ? idx - bin_widths_ratio : 0;
		const auto idx_stop  = idx + bin_widths_ratio < epipolar_lines_count - 1 ? idx + bin_widths_ratio : epipolar_lines_count - 1;

		for(auto j = idx_start; j <= idx_stop; ++j)
		{
			// attention: this line might be limiting runtime (dynamic resizing of array)
			//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Pushing to lookup_correspondences";
			//BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : j " << j << " idx_start " << idx_start << " idx_stop " << idx_stop;

			lookup_correspondences[j].push_back(i);
		}

		//std::cout << ceil((CamA.	Rect1_coords[i][CamA.perp_coord1]-CamA.min1)/uncertainty) << " bin size " << CamA.Lookup_correspondences[ceil((CamA.Rect1_coords[i][CamA.perp_coord1]-CamA.min1)/uncertainty)].size() << std::endl;
	}

	// std::cout << "num epipolar lines " << numepipolarlines << std::endl;
	// attention, this sorting might be too time consuming and be replaced by clever extration of found dots in the original images.
	for(auto i = 0; i < epipolar_lines_count; ++i)
	{
		//std::cout << "sorted correspondences" << CamA.Lookup_correspondences[i].size() << std::endl;
		if(lookup_correspondences[i].size() > 1)
		{
			// todo add valid flag for camC
			if(use_projector)
			{
				/*BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : idx " << i;*/
				std::sort(lookup_correspondences[i].begin(), lookup_correspondences[i].end(), Comparator(this, camera_c.camera_index));
			}
		}
	}
}

int Dot2DData::StereoRectify2(cv::InputArray camera_matrix1,
                              cv::InputArray dist_coeffs1,
                              cv::InputArray camera_matrix2,
                              cv::InputArray dist_coeffs2,
                              cv::Size image_size,
                              cv::InputArray rotation_matrix,
                              cv::InputArray translation_vector,
                              cv::OutputArray rot_mat1,
                              cv::OutputArray rot_mat2,
                              cv::OutputArray p_mat1,
                              cv::OutputArray p_mat2,
                              cv::OutputArray q_mat,
                              int flags,
                              double alpha,
                              cv::Size new_image_size,
                              cv::Rect& valid_pix_roi1,
                              cv::Rect& valid_pix_roi2) const
{
	const auto camera_matrix_1     = camera_matrix1.getMat();
	const auto camera_matrix_2     = camera_matrix2.getMat();
	const auto coeffs1             = dist_coeffs1.getMat();
	const auto coeffs2             = dist_coeffs2.getMat();
	const auto rot_mat             = rotation_matrix.getMat();
	const auto trans_mat           = translation_vector.getMat();
	const cv::Mat c_camera_matrix1 = camera_matrix_1;
	const cv::Mat c_camera_matrix2 = camera_matrix_2;
	const cv::Mat c_dist_coeffs1   = coeffs1;
	const cv::Mat c_dist_coeffs2   = coeffs2;
	const cv::Mat c_rot_mat        = rot_mat;
	const cv::Mat c_trans_mat      = trans_mat;

	rot_mat1.create(3, 3, CV_64F);
	rot_mat2.create(3, 3, CV_64F);
	p_mat1.create(3, 4, CV_64F);
	p_mat2.create(3, 4, CV_64F);
	p_mat2.create(3, 4, CV_64F);

	auto c_rot_mat_1 = rot_mat1.getMat();
	auto c_rot_mat_2 = rot_mat2.getMat();
	auto c_p1        = p_mat1.getMat();
	auto c_p2        = p_mat2.getMat();
	cv::Mat c_q;
	cv::Mat* p_q = nullptr;

	if(q_mat.needed())
	{
		q_mat.create(4, 4, CV_64F);
		p_q = &(c_q = q_mat.getMat());
	}

	auto p_dist_coeffs1 = coeffs1.empty() ? cv::Mat() : c_dist_coeffs1;
	auto p_dist_coeffs2 = coeffs2.empty() ? cv::Mat() : c_dist_coeffs2;

	return StereoRectifyInternal(c_camera_matrix1,
	                             c_camera_matrix2,
	                             p_dist_coeffs1,
	                             p_dist_coeffs2,
	                             image_size,
	                             c_rot_mat,
	                             c_trans_mat,
	                             c_rot_mat_1,
	                             c_rot_mat_2,
	                             c_p1,
	                             c_p2,
	                             *p_q,
	                             flags,
	                             alpha,
	                             new_image_size,
	                             valid_pix_roi1,
	                             valid_pix_roi2);
}

int Dot2DData::StereoRectifyInternal(const cv::Mat& camera_matrix1,
                                     const cv::Mat& camera_matrix2,
                                     const cv::Mat& dist_coeffs1,
                                     const cv::Mat& dist_coeffs2,
                                     cv::Size image_size,
                                     const cv::Mat& rot_mat,
                                     const cv::Mat& translate_mat,
                                     cv::Mat& r1,
                                     cv::Mat& r2,
                                     cv::Mat& p1,
                                     cv::Mat& p2,
                                     cv::Mat& q_mat,
                                     int flags,
                                     double alpha,
                                     cv::Size new_img_size,
                                     cv::Rect& roi1,
                                     cv::Rect& roi2) const
{
	double _om[3];
	double _t[3];
	double _uu[3] = {0, 0, 0};
	double _r_r[3][3];
	double _pp[3][4];
	double _ww[3];
	double _wr[3][3];
	double _z[3] = {0, 0, 0};
	double _ri[3][3];
	cv::Rect_<float> inner1, inner2, outer1, outer2;

	auto om   = cv::Mat(3, 1, CV_64F, _om);
	auto t    = cv::Mat(3, 1, CV_64F, _t);
	auto uu   = cv::Mat(3, 1, CV_64F, _uu);
	auto r_r  = cv::Mat(3, 3, CV_64F, _r_r);
	auto pp   = cv::Mat(3, 4, CV_64F, _pp);
	auto ww   = cv::Mat(3, 1, CV_64F, _ww); // temps
	auto w_r  = cv::Mat(3, 3, CV_64F, _wr);
	auto z    = cv::Mat(3, 1, CV_64F, _z);
	auto ri   = cv::Mat(3, 3, CV_64F, _ri);
	double nx = image_size.width, ny = image_size.height;
	int i, k;

	//TODO CHECK THIS
	// get vector rotation
	if(rot_mat.rows == 3 && rot_mat.cols == 3)
	{
		cv::Rodrigues(rot_mat, om);
	}
	else
	{
		// it's already a rotation vector
		cvConvert(&rot_mat, &om);
	}

	cvConvertScale(&om, &om, -0.5); // get average rotation

	//TODO CHECK THIS
	// rotate cameras to same orientation by averaging
	cv::Rodrigues(om, r_r);
	cvMatMul(&r_r, &translate_mat, &t);

	//If one camera is rotated by 180 degree invert idx
	auto z_angle = std::atan2(rot_mat.at<double>(1, 0), rot_mat.at<double>(0, 0));
	auto idx     = fabs(_t[0]) > fabs(_t[1]) ? 0 : 1;

	if(fabs(z_angle) > 1.570)
	{
		idx = (idx + 1) % 2;
	}

	auto c   = _t[idx], nt = cvNorm(&t, nullptr, CV_L2);
	_uu[idx] = c > 0 ? 1 : -1;

	// calculate global Z rotation
	cvCrossProduct(&t, &uu, &ww);
	auto nw = cvNorm(&ww, nullptr, CV_L2);

	if(nw > 0.0)
	{
		cvConvertScale(&ww, &ww, acos(fabs(c) / nt) / nw);
	}

	//TODO CHECK THIS
	cv::Rodrigues(ww, w_r);

	// apply to both views
	cvGEMM(&w_r, &r_r, 1, nullptr, 0, &ri, CV_GEMM_B_T);
	cvConvert(&ri, &r1);
	cvGEMM(&w_r, &r_r, 1, nullptr, 0, &ri, 0);
	cvConvert(&ri, &r2);
	cvMatMul(&ri, &translate_mat, &t);

	// calculate projection/camera matrices
	// these contain the relevant rectified image internal params (fx, fy=fx, cx, cy)
	auto fc_new            = DBL_MAX;
	CvPoint2D64f cc_new[2] = {{0, 0}, {0, 0}};

	for(k = 0; k < 2; ++k)
	{
		const auto A  = k == 0 ? camera_matrix1 : camera_matrix2;
		const auto Dk = k == 0 ? dist_coeffs1 : dist_coeffs2;

		//TODO CHECK THIS
		double dk1 = Dk.data ? Dk.at<double>(0, 0) : 0;

		//TODO CHECK THIS
		double fc = A.at<double>(idx ^ 1, idx ^ 1);

		if(dk1 < 0)
		{
			fc *= 1 + dk1 * (nx * nx + ny * ny) / (4 * fc * fc);
		}

		fc_new = MIN(fc_new, fc);
	}

	for(k = 0; k < 2; ++k)
	{
		const auto a  = k == 0 ? camera_matrix1 : camera_matrix2;
		const auto dk = k == 0 ? dist_coeffs1 : dist_coeffs2;

		CvPoint2D32f points[4];
		CvPoint3D32f points_3[4];
		auto pts   = cv::Mat(1, 4, CV_32FC2, points);
		auto pts_3 = cv::Mat(1, 4, CV_32FC3, points_3);

		for(i = 0; i < 4; ++i)
		{
			auto j      = i < 2 ? 0 : 1;
			points[i].x = (float)(i % 2 * nx);
			points[i].y = (float)(j * ny);
		}
		//TODO CHECK THIS
		cv::undistortPoints(pts, pts, a, dk, 0, 0);
		cv::convertPointsHomogeneous(pts, pts_3);

		//Change camera matrix to have cc=[0,0] and fc = fc_new
		double a_tmp[3][3];
		auto tmp    = cv::Mat(3, 3, CV_64F, a_tmp);
		a_tmp[0][0] = fc_new;
		a_tmp[1][1] = fc_new;
		a_tmp[0][2] = 0.0;
		a_tmp[1][2] = 0.0;

		//TODO CHECK THIS
		cv::projectPoints(pts_3, k == 0 ? r1 : r2, z, tmp, 0, pts);
		auto avg    = cvAvg(&pts);
		cc_new[k].x = (nx) / 2 - avg.val[0];
		cc_new[k].y = (ny) / 2 - avg.val[1];
	}

	// vertical focal length must be the same for both images to keep the epipolar constraint
	// (for horizontal epipolar lines -- TBD: check for vertical epipolar lines)
	// use fy for fx also, for simplicity

	// For simplicity, set the principal points for both cameras to be the average
	// of the two principal points (either one of or both x- and y- coordinates)
	if(flags & CV_CALIB_ZERO_DISPARITY)
	{
		cc_new[0].x = cc_new[1].x = (cc_new[0].x + cc_new[1].x) * 0.5;
		cc_new[0].y = cc_new[1].y = (cc_new[0].y + cc_new[1].y) * 0.5;
	}
	else if(idx == 0) // horizontal stereo
	{
		cc_new[0].y = cc_new[1].y = (cc_new[0].y + cc_new[1].y) * 0.5;
	}
	else // vertical stereo
	{
		cc_new[0].x = cc_new[1].x = (cc_new[0].x + cc_new[1].x) * 0.5;
	}

	cvZero(&pp);
	_pp[0][0] = _pp[1][1] = fc_new;
	_pp[0][2] = cc_new[0].x;
	_pp[1][2] = cc_new[0].y;
	_pp[2][2] = 1;
	cvConvert(&pp, &p1);

	_pp[0][2]   = cc_new[1].x;
	_pp[1][2]   = cc_new[1].y;
	_pp[idx][3] = _t[idx] * fc_new; // baseline * focal length
	cvConvert(&pp, &p2);

	alpha = MIN(alpha, 1.);
	CvSize image_size_in;
	image_size_in.height = image_size.height;
	image_size_in.width  = image_size.width;
	IcvGetRectangles(camera_matrix1, dist_coeffs1, r1, p1, image_size_in, inner1, outer1);
	IcvGetRectangles(camera_matrix2, dist_coeffs2, r2, p2, image_size_in, inner2, outer2);

	{
		new_img_size = new_img_size.width * new_img_size.height != 0 ? new_img_size : image_size;
		auto cx1_0   = cc_new[0].x;
		auto cy1_0   = cc_new[0].y;
		auto cx2_0   = cc_new[1].x;
		auto cy2_0   = cc_new[1].y;
		auto cx1     = new_img_size.width * cx1_0 / image_size.width;
		auto cy1     = new_img_size.height * cy1_0 / image_size.height;
		auto cx2     = new_img_size.width * cx2_0 / image_size.width;
		auto cy2     = new_img_size.height * cy2_0 / image_size.height;
		auto s       = 1.;

		if(alpha >= 0)
		{
			double s0 = std::max(std::max(std::max((double)cx1 / (cx1_0 - inner1.x), (double)cy1 / (cy1_0 - inner1.y)),
			                              (double)(new_img_size.width - cx1) / (inner1.x + inner1.width - cx1_0)),
			                     (double)(new_img_size.height - cy1) / (inner1.y + inner1.height - cy1_0));

			s0 = std::max(std::max(std::max(std::max((double)cx2 / (cx2_0 - inner2.x), (double)cy2 / (cy2_0 - inner2.y)),
			                                (double)(new_img_size.width - cx2) / (inner2.x + inner2.width - cx2_0)),
			                       (double)(new_img_size.height - cy2) / (inner2.y + inner2.height - cy2_0)),
			              s0);

			double s1 = std::min(std::min(std::min((double)cx1 / (cx1_0 - outer1.x), (double)cy1 / (cy1_0 - outer1.y)),
			                              (double)(new_img_size.width - cx1) / (outer1.x + outer1.width - cx1_0)),
			                     (double)(new_img_size.height - cy1) / (outer1.y + outer1.height - cy1_0));

			s1 = std::min(std::min(std::min(std::min((double)cx2 / (cx2_0 - outer2.x), (double)cy2 / (cy2_0 - outer2.y)),
			                                (double)(new_img_size.width - cx2) / (outer2.x + outer2.width - cx2_0)),
			                       (double)(new_img_size.height - cy2) / (outer2.y + outer2.height - cy2_0)),
			              s1);

			s = s0 * (1 - alpha) + s1 * alpha;
		}

		fc_new *= s;
		cc_new[0]           = cvPoint2D64f(cx1, cy1);
		cc_new[1]           = cvPoint2D64f(cx2, cy2);
		p1.at<double>(0, 0) = fc_new;
		p1.at<double>(1, 1) = fc_new;
		p1.at<double>(0, 2) = cx1;
		p1.at<double>(1, 2) = cy1;

		p2.at<double>(0, 0)   = fc_new;
		p2.at<double>(1, 1)   = fc_new;
		p2.at<double>(0, 2)   = cx2;
		p2.at<double>(1, 2)   = cy2;
		p2.at<double>(idx, 3) = s * p2.at<double>(idx, 3);

		//TODO CHECK THIS
		{
			roi1 = cv::Rect(cvCeil((inner1.x - cx1_0) * s + cx1),
			                cvCeil((inner1.y - cy1_0) * s + cy1),
			                cvFloor(inner1.width * s),
			                cvFloor(inner1.height * s))
				& cv::Rect(0, 0, new_img_size.width, new_img_size.height);
		}

		{
			roi2 = cv::Rect(cvCeil((inner2.x - cx2_0) * s + cx2),
			                cvCeil((inner2.y - cy2_0) * s + cy2),
			                cvFloor(inner2.width * s),
			                cvFloor(inner2.height * s))
				& cv::Rect(0, 0, new_img_size.width, new_img_size.height);
		}
	}

	{
		double q[] =
		{
			1, 0, 0, -cc_new[0].x,
			0, 1, 0, -cc_new[0].y,
			0, 0, 0, fc_new,
			0, 0, -1. / _t[idx],
			(idx == 0 ? cc_new[0].x - cc_new[1].x : cc_new[0].y - cc_new[1].y) / _t[idx]
		};

		auto Q = cvMat(4, 4, CV_64F, q);
		cvConvert(&Q, &q_mat);
	}

	return idx;
}

void Dot2DData::IcvGetRectangles(const cv::Mat& camera_matrix,
                                 const cv::Mat& distance_coefficients,
                                 const cv::Mat& r,
                                 const cv::Mat& new_camera_matrix,
                                 const CvSize image_size,
                                 cv::Rect_<float>& inner,
                                 cv::Rect_<float>& outer) const
{
	const auto count = 9;
	int x, y, k;
	auto points       = cv::Mat(1, count * count, CV_32FC2);
	CvPoint2D32f* pts = (CvPoint2D32f*)(points.data);

	for(y = k = 0; y < count; ++y)
	{
		for(x = 0; x < count; ++x)
		{
			pts[k++] = cvPoint2D32f((float)x * image_size.width / (count - 1),
			                        (float)y * image_size.height / (count - 1));
		}
	}

	//TODO CHECK THIS
	cv::undistortPoints(points, points, camera_matrix, distance_coefficients, r, new_camera_matrix);

	auto iX0 = -FLT_MAX;
	auto iX1 = FLT_MAX;
	auto iY0 = -FLT_MAX;
	auto iY1 = FLT_MAX;
	auto oX0 = FLT_MAX;
	auto oX1 = -FLT_MAX;
	auto oY0 = FLT_MAX;
	auto oY1 = -FLT_MAX;

	// find the inscribed rectangle.
	// the code will likely not work with extreme rotation matrices (R) (>45%)
	for(y = k = 0; y < count; ++y)
	{
		for(x = 0; x < count; ++x)
		{
			auto p = pts[k++];
			oX0    = MIN(oX0, p.x);
			oX1    = MAX(oX1, p.x);
			oY0    = MIN(oY0, p.y);
			oY1    = MAX(oY1, p.y);

			if(x == 0)
			{
				iX0 = MAX(iX0, p.x);
			}

			if(x == count - 1)
			{
				iX1 = MIN(iX1, p.x);
			}
			if(y == 0)
			{
				iY0 = MAX(iY0, p.y);
			}

			if(y == count - 1)
			{
				iY1 = MIN(iY1, p.y);
			}
		}
	}

	inner = cv::Rect_<float>(iX0, iY0, iX1 - iX0, iY1 - iY0);
	outer = cv::Rect_<float>(oX0, oY0, oX1 - oX0, oY1 - oY0);
}
