#include "sub_pixel.h"

namespace Reconstruct
{
/**
 * \brief Create a mask for calculation of the
 * background intensity around an image patch.
 * Used in the sub-pixel interpolation method below.
 * \param size
 * \return cv::Mat
*/
cv::Mat GetBackgroundMask(const int size)
{
	cv::Mat back_ground_mask(size, size, CV_8UC1, cv::Scalar(0));

	for(auto i = 1; i < size - 1; ++i)
	{
		back_ground_mask.at<uchar>(i, 0)        = 0x1;
		back_ground_mask.at<uchar>(i, size - 1) = 0x1;
		back_ground_mask.at<uchar>(0, i)        = 0x1;
		back_ground_mask.at<uchar>(size - 1, i) = 0x1;
	}

	return back_ground_mask;
}

/**
 * \brief Create matrix with pixel distances for the sub-pixel
 * interpolation. Used by the method getSubpixelMatrix below.
 * May be used if we use a weighted interpolation.
 * \param refinement
 * \return
*/
Eigen::MatrixXf GetDistanceMatrix(const int refinement)
{
	// f(x,y) = a + b*x + c*y + d*x^2 + e*y^2 + f*xy
	// For each pixel (x,y) in our sub-pixel refinement patch we get one row with 6 coeffs for a-f.
	// coeff for a is always 1
	// for x=-1 y=-1 the coeffs are b,(x): -1 c,(y): -1 d,(x*x): 1 e,(y*y): 1 f(x*y): 1
	// 	here for example the entire matrix for a 3x3 refinement patch, 9 pixel entries 6 parameter
	// 	1 -1 -1  1  1  1
	// 	1 -1  0  1  0  0
	// 	1 -1  1  1  1 -1
	// 	1  0 -1  0  1  0
	// 	1  0  0  0  0  0
	// 	1  0  1  0  1  0
	// 	1  1 -1  1  1 -1
	// 	1  1  0  1  0  0
	// 	1  1  1  1  1  1

	// Create the matrix for the requested patch size
	Eigen::MatrixXf A = Eigen::MatrixXf::Zero(refinement * refinement, 6);
	const auto start  = -(refinement - refinement / 2) + 1;
	const auto end    = refinement - refinement / 2;

	// OpenCV matrices are stored row by row. So create the distance matrix row by row here too.
	// Subpixel-interpolation will pass the memory of an OpenCV matrix to Eigen without any rearrangement.

	auto index = 0;

	for(auto y = start; y < end; ++y)
	{
		for(auto x = start; x < end; ++x)
		{
			A(index, 0) = 1.0f;
			A(index, 1) = float(x);
			A(index, 2) = float(y);
			A(index, 3) = float(x * x);
			A(index, 4) = float(y * y);
			A(index, 5) = float(y * x);

			index++;
		}
	}

	return A;
}

/**
 * \brief Precompute the pseudo-inverse for the paraboloid
 * fit used for the sub-pixel interpolation.
 * \param refinement
 * \return
*/
Eigen::MatrixXf GetSubPixelMatrix(const int refinement)
{
	const Eigen::MatrixXf distance_matrix = GetDistanceMatrix(refinement);

	// Calculate pseudo inverse for least square fit
	const Eigen::JacobiSVD<Eigen::MatrixXf> jacobi_svd_distance_matrix(distance_matrix, Eigen::ComputeFullU | Eigen::ComputeFullV);
	const Eigen::MatrixXf matrix_u_transpose = jacobi_svd_distance_matrix.matrixU().transpose();
	const Eigen::MatrixXf matrix_v           = jacobi_svd_distance_matrix.matrixV();

	Eigen::MatrixXf singular_value_matrix = Eigen::MatrixXf::Zero(6, refinement * refinement);
	Eigen::VectorXf singular_values       = jacobi_svd_distance_matrix.singularValues();

	for(auto i = 0; i < singular_values.size(); ++i)
	{
		if(singular_values(i) > 1e-6)
			singular_value_matrix(i, i) = 1.0f / singular_values(i);
	}

	Eigen::MatrixXf pseudo_inv = matrix_v * singular_value_matrix * matrix_u_transpose;
	return pseudo_inv;
}

// Precomputed pseudo inverses and masks for the sub-pixel interpolation
static const Eigen::MatrixXf matrix3 = GetSubPixelMatrix(3);
static const Eigen::MatrixXf matrix5 = GetSubPixelMatrix(5);
static const Eigen::MatrixXf matrix7 = GetSubPixelMatrix(7);
static const Eigen::MatrixXf matrix9 = GetSubPixelMatrix(9);

static const cv::Mat backgroundMask5  = GetBackgroundMask(5);
static const cv::Mat backgroundMask7  = GetBackgroundMask(7);
static const cv::Mat backgroundMask9  = GetBackgroundMask(9);
static const cv::Mat backgroundMask11 = GetBackgroundMask(11);

cv::Vec<float, 7> SubPixel::subPixel(const cv::Mat& image,
                                     const cv::Point& pixel,
                                     const int patch_size)
{
	// Sanity checks
	cv::Vec<float, 7> res(float(pixel.x), float(pixel.y), 0, 0, 0, FLT_MAX, 0);

	if(patch_size < 3)
		return res;

	if(patch_size > 9)
		return res;

	if((patch_size % 2) == 0)
		return res;

	if(image.cols < patch_size)
		return res;

	if(image.rows < patch_size)
		return res;

	const cv::Rect area(pixel.x - patch_size / 2, pixel.y - patch_size / 2, patch_size, patch_size);

	if((cv::Rect(0, 0, image.cols, image.rows) & area) != area)
		return res;

	// Get patch
	cv::Mat patch;

	switch(image.type())
	{
	case CV_32FC1:
		patch = cv::Mat(image, area).clone();
		break;
	case CV_8UC1:
		image(area).convertTo(patch, CV_32FC1, 1.0, 0.0);
		break;
	default:
		return res;
	}

	// Approximate paraboloid
	const Eigen::Map<Eigen::VectorXf> right(patch.ptr<float>(0), patch_size * patch_size);
	Eigen::Matrix<float, 6, 1> result = patch_size == 3
		                                    ? matrix3 * right
		                                    : patch_size == 5
		                                    ? matrix5 * right
		                                    : patch_size == 7
		                                    ? matrix7 * right
		                                    : matrix9 * right;

	// We cannot interpolate a flat area
	float* a        = result.data();
	const float div = a[5] * a[5] - 4.0f * a[3] * a[4];

	if(fabs(div) < FLT_EPSILON)
		return res;

	// Peak position
	const auto x = -(a[2] * a[5] - 2.0f * a[1] * a[4]) / div;
	const auto y = -(a[1] * a[5] - 2.0f * a[2] * a[3]) / div;

	// The interpolated position should be inside of our
	// center pixel. Use the direction and limit the radius
	// to sqrt(2) to guarantee this.
	const auto rad = sqrt(x * x + y * y);

	if(fabs(x) > 2.5f || fabs(y) > 2.5f)
		return res;

	// Check for reasonable data
	double min_val, max_val;
	cv::minMaxIdx(cv::Mat(image, area), &min_val, &max_val);

	if(min_val > 250.0)
		return res;

	if(max_val < 5.0)
		return res;

	if(max_val - min_val < 2.0)
		return res;

	res[0] = pixel.x + x;
	res[1] = pixel.y + y;
	res[2] = a[0] + a[1] * x + a[2] * y + a[3] * x * x + a[4] * y * y + a[5] * x * y; //this is the maximum intensity
	res[3] = a[3];
	res[4] = a[4];
	res[5] = rad;
	res[6] = float(min_val);

	//background signal. Assuming that a [1-5] are negative. Only then the peak is exactly at x,y
	const cv::Rect area1(pixel.x - patch_size / 2 - 1, pixel.y - patch_size / 2 - 1, patch_size + 2, patch_size + 2);

	if((cv::Rect(0, 0, image.cols, image.rows) & area1) == area1)
	{
		res[6] = float(cv::mean(cv::Mat(image, area1),
		                        patch_size == 3
			                        ? backgroundMask5
			                        : patch_size == 5
			                        ? backgroundMask7
			                        : patch_size == 7
			                        ? backgroundMask9
			                        : backgroundMask11)[0]);
	}

	return res;
}

void SubPixel::subPixel(const std::vector<cv::Mat>& pyramids,
                        const int pyr_level,
                        std::vector<cv::Point2f>& corners)
{
	// Sanity checks
	if(pyramids.empty())
		return;

	if(int(pyramids.size()) < pyr_level)
		return;

	if(corners.empty())
		return;

	// Open CV sub-pixel adjustment
	for(auto l = pyr_level; l >= 0; l -= 2)
	{
		// do sub-pixel refinement of detected features
		// use a small search window for the higher pyramid levels.
		cv::cornerSubPix(pyramids[l],
		                 corners,
		                 l > 0 ? cv::Size(1, 1) : cv::Size(3, 3),
		                 cv::Size(-1, -1),
		                 cv::TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 10, 1e-2));

		if(l > 0)
		{
			for(auto& corner : corners)
			{
				corner *= 2.0;
			}
		}
	}
}

void SubPixel::subPixel(const std::vector<cv::Mat>& pyramids,
                        const int pyr_level,
                        std::vector<cv::KeyPoint>& keys)
{
	std::vector<cv::Point2f> corners;
	cv::KeyPoint::convert(keys, corners);
	subPixel(pyramids, pyr_level, corners);

	for(size_t i = 0; i < corners.size(); ++i)
	{
		keys[i].pt = corners[i];
	}
}
}
