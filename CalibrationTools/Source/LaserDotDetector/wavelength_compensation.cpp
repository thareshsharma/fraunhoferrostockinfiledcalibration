#include "wavelength_compensation.h"

using namespace Reconstruct;

// FunctorForWavelengthCompensation
// Base class for WavelengthCompensationFit that implements interface as expected by Eigen.
//see://http://eigen.tuxfamily.org/dox-devel/unsupported/group__NonLinearOptimization__Module.html
//and the examples in NonLinearOpimization.cpp
template <typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
class FunctorForWavelengthCompensation
{
protected:

	typedef _Scalar Scalar; ///< which data format to use, e.g. double, float...
	enum
	{
		InputsAtCompileTime = NX,
		ValuesAtCompileTime = NY
	};

	const int inputs_, values_; ///< the number of parameters (inputs) and function values (values)

	/// constructor with dimensions of parameters and values known at compile time (faster)
	/// @param [in] m_inputs number of parameters
	/// @param [in] m_values number of function values
	FunctorForWavelengthCompensation() :
		inputs_(4),
		values_(ValuesAtCompileTime)
	{
	}

	/// constructor with dimensions of parameters and values not known at compile time (slower)
	/// @param [in] inputs number of parameters
	/// @param [in] values number of function values
	FunctorForWavelengthCompensation(const int inputs,
	                                 const int values) :
		inputs_(inputs),
		values_(values)
	{
	}

public:
	typedef Eigen::Matrix<Scalar, InputsAtCompileTime, 1> InputType;
	typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
	typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

	/// access to the number of parameters
	/// @return the number of parameters
	int inputs() const
	{
		return inputs_;
	}

	/// access to the number of function values
	/// @return the number of function values
	int values() const
	{
		return values_;
	}
};

WavelengthCompensationFit::WavelengthCompensationFit(const std::vector<Eigen::Vector2d>& orig_coords_in,
                                                     const std::vector<Eigen::Vector2d>& current_coords_in,
                                                     const Eigen::Vector2d& central_beam_in) :
	FunctorForWavelengthCompensation<double>(4, (int)current_coords_in.size() * 2),
	orig_coords_(orig_coords_in),
	current_coords_(current_coords_in),
	central_beam_(central_beam_in)
{
	for(unsigned int i = 0; i < orig_coords_.size(); ++i)
	{
		//for higher speed, cache sin and tan values
		const auto delta = orig_coords_[i] - central_beam_;
		orig_sin_angles_.push_back(sin(atan(GetTangentsOfBeam(delta))));
		orig_tan_angles_.push_back(GetTangentsOfBeam(delta));
	}
}

WavelengthCompensationFit::WavelengthCompInfo WavelengthCompensationFit::ToCompInfo(const Eigen::VectorXd& x)
{
	WavelengthCompInfo out;
	out.translation[0]  = x[0];
	out.translation[1]  = x[1];
	out.rotation        = x[2];
	out.rotation_matrix = Eigen::Rotation2D<double>(out.rotation);
	out.scale           = x[3];
	return out;
}

Eigen::VectorXd WavelengthCompensationFit::ToEigenVector(const WavelengthCompInfo& in)
{
	Eigen::Vector4d out;
	out[0] = in.translation[0];
	out[1] = in.translation[1];
	out[2] = in.rotation;
	out[3] = in.scale;

	return out;
}

int WavelengthCompensationFit::operator()(const Eigen::VectorXd& x,
                                          Eigen::VectorXd& fvec,
                                          JacobianType* jacobi_type) const
{
	const auto info    = ToCompInfo(x);
	const auto applied = ApplyToPoints<Eigen::Vector2d, double>(info, orig_coords_);

	for(unsigned int i = 0; i < orig_coords_.size(); ++i)
	{
		fvec[i * 2 + 0] = applied[i][0] - current_coords_[i][0];
		fvec[i * 2 + 1] = applied[i][1] - current_coords_[i][1];
	}

	return 0;
}

double WavelengthCompensationFit::GetTangentsOfBeam(const Eigen::Vector2d& beam)
{
	return beam.norm() / 1000.;
}

WavelengthCompensationFitScaleOnly::WavelengthCompensationFitScaleOnly(const std::vector<Eigen::Vector2d>& orig_coords_in,
                                                                       const std::vector<Eigen::Vector2d>& current_coords_in,
                                                                       Eigen::Vector2d central_beam_in) :
	FunctorForWavelengthCompensation<double>(2, (int)current_coords_in.size() * 2),
	orig_coords_(orig_coords_in),
	current_coords_(current_coords_in),
	central_beam_(std::move(central_beam_in))
{
	for(auto orig_coord : orig_coords_)
	{
		//for higher speed, cache sin and tan values
		const auto delta = orig_coord - central_beam_;
		orig_sin_angles_.push_back(sin(atan(GetTangentsOfBeam(delta))));
		orig_tan_angles_.push_back(GetTangentsOfBeam(delta));
	}
}

WavelengthCompensationFitScaleOnly::~WavelengthCompensationFitScaleOnly() = default;

WavelengthCompensationFit::WavelengthCompInfo WavelengthCompensationFitScaleOnly::ToCompInfo(const Eigen::VectorXd& x)
{
	WavelengthCompensationFit::WavelengthCompInfo out;
	out.scale = x[0];
	return out;
}

Eigen::VectorXd WavelengthCompensationFitScaleOnly::ToEigenVector(const WavelengthCompensationFit::WavelengthCompInfo& in)
{
	Eigen::Vector2d out;
	out[0] = in.scale;
	out[1] = 0;
	return out;
}

int WavelengthCompensationFitScaleOnly::operator()(const Eigen::VectorXd& x,
                                                   Eigen::VectorXd& fvec,
                                                   JacobianType* jacobi) const
{
	const auto info    = ToCompInfo(x);
	const auto applied = ApplyToPoints<Eigen::Vector2d, double>(info, orig_coords_);

	for(unsigned int i = 0; i < orig_coords_.size(); ++i)
	{
		fvec[i * 2 + 0] = applied[i][0] - current_coords_[i][0];
		fvec[i * 2 + 1] = applied[i][1] - current_coords_[i][1];
	}

	return 0;
}

double WavelengthCompensationFitScaleOnly::GetTangentsOfBeam(const Eigen::Vector2d& beam)
{
	return beam.norm() / 1000.;
}

WavelengthCompensationFitScaleOnlyWeighted::WavelengthCompensationFitScaleOnlyWeighted(const std::vector<Eigen::Vector2d>& orig_coords_in,
                                                                                       const std::vector<Eigen::Vector2d>& current_coords_in,
                                                                                       const Eigen::Vector2d& central_beam_in,
                                                                                       const std::vector<double>& weights_in) :
	WavelengthCompensationFitScaleOnly(orig_coords_in, current_coords_in, central_beam_in), weights_(weights_in)
{
}

int WavelengthCompensationFitScaleOnlyWeighted::operator()(const Eigen::VectorXd& x,
                                                           Eigen::VectorXd& fvec,
                                                           JacobianType* jacobi_type) const
{
	const auto info    = ToCompInfo(x);
	const auto applied = ApplyToPoints<Eigen::Vector2d, double>(info, orig_coords_);

	for(unsigned int i = 0; i < orig_coords_.size(); ++i)
	{
		fvec[i * 2 + 0] = (applied[i][0] - current_coords_[i][0]) * weights_[i];
		fvec[i * 2 + 1] = (applied[i][1] - current_coords_[i][1]) * weights_[i];
	}

	return 0;
}
