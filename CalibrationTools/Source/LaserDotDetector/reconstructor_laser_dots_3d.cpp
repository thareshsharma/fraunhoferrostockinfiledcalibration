#include "reconstructor_laser_dots_3d.h"
#include "union_find.h"
#include "sub_pixel.h"
#include "configuration_container.h"

using namespace Reconstruct;

std::vector<ReconstructorLaserDots3D::TripleCorrespondence> ReconstructorLaserDots3D::FindIntersection(const int cam0_index,
	const int corr_idx1,
	const int corr_idx2)
{
	// loops through two sorted indices to find approximate intersection
	//	PROFILE_FUNC();
	std::vector<TripleCorrespondence> result;

	if(corr_idx1 < 0)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : corr_idx1 < 0";
	}

	if(corr_idx2 < 0)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : corr_idx2 < 0";
	}

	if(corr_idx1 >= int(camera_sensor1.lookup_correspondences.size()))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : corr_idx1 " << corr_idx1 << " > lookup_correspondences " << camera_sensor1.
lookup_correspondences.size();
	}

	if(corr_idx2 >= projector.lookup_correspondences.size())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : corr_idx2 " << corr_idx2 << " > lookup_correspondences " << projector.lookup_correspondences.
size();
	}

	if(corr_idx1 < 0 || corr_idx2 < 0 || (corr_idx1 >= int(camera_sensor1.lookup_correspondences.size()) || corr_idx2 >= int(
		projector.lookup_correspondences.size())))
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : FindIntersection can fail : corr1 " << corr_idx1 << "( " << camera_sensor1.
lookup_correspondences.size() << " )  corr2 " << corr_idx2 << " ( " << projector.lookup_correspondences.size() << " )";

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cam0_index " << cam0_index << " Image Position " << camera_sensor0.GetDot(cam0_index).x <<
 " " << camera_sensor0.GetDot(cam0_index).y << "Returning but have done nothing";

		return result;
	}


	// attention: hard-coded break-off of correspondence search after 10 found per point.
	const auto max_num_correspondences = 10;

	TripleCorrespondence found_correspondence;
	result.reserve(max_num_correspondences);

	// Only two cameras without projector
	if(!use_projector)
	{
		auto& cam1_candidates = camera_sensor1.lookup_correspondences[corr_idx1];

		for(auto cam1_candidates_idx : cam1_candidates)
		{
			const auto e3 = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][cam0_index].y;
			const auto e4 = camera_sensor1.rectified_coordinates[camera_sensor0.camera_index][cam1_candidates_idx].y;

			// Membership of candidate list is only a required but not a sufficient criterion.
			// The selection by candidate lists according to bins was too coarse
			if(fabs(e3 - e4) <= camera_sensor0.uncertainty + camera_sensor1.uncertainty)
			{
				found_correspondence.camera_index[0] = cam0_index;
				found_correspondence.camera_index[1] = camera_sensor1.lookup_correspondences[corr_idx1][0];
				found_correspondence.camera_index[2] = 0;

				result.push_back(found_correspondence);
			}
		}

		// only return ambiguous solution
		if(result.size() > 1)
		{
			result.clear();
		}

		return result;
	}

	// Both candidate lists need at least one entry
	const auto camera_sensor1_candidates = camera_sensor1.lookup_correspondences[corr_idx1];
	const auto projector_candidates      = projector.lookup_correspondences[corr_idx2];

	if(projector_candidates.empty())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : projector_candidates is empty";
		result.clear();
		return result;
	}

	if(camera_sensor1_candidates.empty())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : camera_sensor1_candidates is empty";
		result.clear();
		return result;
	}

	// Triple correspondence including projector
	auto projector_iterator_begin     = projector.lookup_correspondences[corr_idx2].begin();
	const auto projector_iterator_end = projector.lookup_correspondences[corr_idx2].end();
	auto projector_iterator_high      = projector_iterator_begin;

	// cam0 measurement, rectified to cam1 component parallel to epipolar line
	const double cam0_cam1_par_coordinate = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][cam0_index].x;

	// cam0 measurement, rectified to proj0 component parallel to epipolar line
	const double cam0_proj_par_coord = camera_sensor0.rectified_coordinates[projector.camera_index][cam0_index].x;

	for(auto candidates_idx : camera_sensor1_candidates)
	{
		// this loops through all corresponding dots in the Cam1-view
		const double cam1_cam0_x_coordinate = camera_sensor1.rectified_coordinates[camera_sensor0.camera_index][candidates_idx].x;

		if(cam1_cam0_x_coordinate < cam0_cam1_par_coordinate)
		{
			// actually a break should do the same more efficiently, but it does not
			// It should because candidate vector is sorted by coordinate in direction epipol line
			continue;
		}

		// get projector rectified coordinate of cam1 candidate
		const double e1 = camera_sensor1.rectified_coordinates[projector.camera_index][candidates_idx].y;

		// with every increase of the Cam1-iterator, the iterators Pro0iterlow and Pro0iterhigh are adjusted such that they
		// limit a range of possible correspondences in the Pro0 view
		auto pro0_low_index = *projector_iterator_begin;
		double e2_low       = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_low_index].y;

		while(e2_low < e1 - (camera_sensor1.uncertainty + projector.uncertainty) && projector_iterator_begin != projector_iterator_end)
		{
			pro0_low_index = *projector_iterator_begin;
			++projector_iterator_begin;

			//TODO Will it ever come here
			if(projector_iterator_begin == projector_iterator_end)
			{
				--projector_iterator_begin;
				break;
			}

			pro0_low_index = *projector_iterator_begin;
			e2_low         = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_low_index].y;
		}

		auto pro0_high_index = *projector_iterator_high;
		double e2_high       = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_high_index].y;

		while(e2_high < e1 + (camera_sensor1.uncertainty + projector.uncertainty) && projector_iterator_high != projector_iterator_end)
		{
			pro0_high_index = *projector_iterator_high;
			++projector_iterator_high;

			//TODO Will it ever come here
			if(projector_iterator_high == projector_iterator_end)
			{
				--projector_iterator_high;
				break;
			}

			pro0_high_index = *projector_iterator_high;
			e2_high         = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_high_index].y;
		}

		std::vector<int>::iterator projector_iterator;

		for(projector_iterator = projector_iterator_begin; projector_iterator <= projector_iterator_high; ++projector_iterator)
		{
			const auto pro0_index = *projector_iterator;

			// check correspondence in remaining relation Cam1-Pro0
			// cam0 measurement, rectified to proj0 component parallel to epipolar line

			// pro0 coordinate measurement, rectified to cam0 component parallel to epipolar line
			const double proj_cam0_par_coordinate = projector.rectified_coordinates[camera_sensor0.camera_index][pro0_index].x;

			// This checks for intersection of cam0 and projector in front of camera/projector
			const auto inv_dir1 = camera_sensor0.GetQ(projector.camera_index).at<double>(3, 2) < 0;

			// actually a break should do the same more efficiently, but it does not
			// It should because candidate vector is sorted by coordinate in direction epipolar line
			if(!inv_dir1 && proj_cam0_par_coordinate > cam0_proj_par_coord || inv_dir1 && proj_cam0_par_coordinate < cam0_proj_par_coord)
			{
				continue;
			}

			// Projector Cam1 check
			const double cam1_proj_par_coordinate = camera_sensor1.rectified_coordinates[projector.camera_index][candidates_idx].x;
			const double proj_cam1_par_coordinate = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_index].x;

			// This checks for intersection of cam1 and projector in front of camera/projector
			const auto inv_dir2 = camera_sensor1.GetQ(projector.camera_index).at<double>(3, 2) < 0;

			// actually a break should do the same more efficiently, but it does not
			// It should because candidate vector is sorted by coordinate in direction epipol line
			if(!inv_dir2 && proj_cam1_par_coordinate > cam1_proj_par_coordinate || inv_dir2 && proj_cam1_par_coordinate < cam1_proj_par_coordinate)
			{
				continue;
			}

			// cam1 measurement, rectified to proj0 component parallel to epipolar line
			// get cam1 rectified coordinate of projector candidate
			const double e2 = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_index].y;

			if(fabs(e1 - e2) <= camera_sensor1.uncertainty + projector.uncertainty)
			{
				// e3-e4: exactly check Cam0-Cam1 relation because being member of candidate lists
				// is only a required but not a sufficient criterion.
				// The selection by candidate lists according to bins was too coarse
				const double e3 = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][cam0_index].y;
				const double e4 = camera_sensor1.rectified_coordinates[camera_sensor0.camera_index][candidates_idx].y;

				// e5-e6: exactly check Cam0-Proj relation because being member of candidate lists
				// is only a required but not a sufficient criterion. (due to discretization)
				// The selection by candidate lists according to bins was too coarse
				if(fabs(e3 - e4) <= camera_sensor0.uncertainty + camera_sensor1.uncertainty)
				{
					const double e5 = camera_sensor0.rectified_coordinates[projector.camera_index][cam0_index].y;
					const double e6 = projector.rectified_coordinates[camera_sensor0.camera_index][pro0_index].y;

					if(fabs(e5 - e6) <= camera_sensor0.uncertainty + projector.uncertainty)
					{
						found_correspondence.camera_index[0] = cam0_index;
						found_correspondence.camera_index[1] = candidates_idx;
						found_correspondence.camera_index[2] = pro0_index;
						found_correspondence.e[0]            = e1 - e2;
						found_correspondence.e[1]            = e3 - e4;
						found_correspondence.e[2]            = e5 - e6;

						result.push_back(found_correspondence);
					}
				}
			}
		}
	}

	return result;
}

void ReconstructorLaserDots3D::MarkPointIndices(std::vector<int>& point_flags,
                                                const size_t number_of_triangulated_points)
{
	point_flags.resize(number_of_triangulated_points, 0);

	// write projector beam index to flags
	for(size_t i = 0; i < number_of_triangulated_points; ++i)
	{
		const auto doe_idx = GetUsedCorrespondence((int)i).camera_index[2];
		point_flags[i] |= doe_idx & BEAM_INDEX_16BIT;
	}

	// mark all bright points
	auto bright_pts = GetBrightPoints();
	for(auto bright_pt : bright_pts)
	{
		point_flags[bright_pt] |= IS_BRIGHT;
	}
}

void ReconstructorLaserDots3D::FindTripleCorrespondences()
{
	triples_.clear();
	camera_sensor0.ResetDotUseCounter();
	camera_sensor1.ResetDotUseCounter();

	if(use_projector)
	{
		projector.ResetDotUseCounter();
	}

	// number of all dots in 1st camera to be looped through
	const auto& cam0_dots = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index];
	const auto cam0_pts   = int(cam0_dots.size());

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cam0_pts " << cam0_pts;

	// preallocate empty vector for every cam0 dot
	std::vector<std::vector<TripleCorrespondence>> local_triples(cam0_pts);

	// Loop all cam0 dots and find potential correspondences
	/*tbb::parallel_for(size_t(0),
	                  camera_sensor0.rectified_coordinates[camera_sensor1.camera_index].size(),
	                  [&](const size_t idx0)*/

	for(auto idx0 = 0; idx0 < cam0_pts; ++idx0)
	{
		// calculate indices of lookup tables
		const auto idx1 = camera_sensor1.GetEpipolarLineIndex(camera_sensor0, cam0_dots[idx0].y);
		const auto idx2 = use_projector ? projector.GetEpipolarLineIndex(camera_sensor0, cam0_dots[idx0].y) : 1;

		// store triples to pre-allocated vector
		local_triples[idx0] = FindIntersection((int)idx0, idx1, idx2);

		//});
	}

	// synchronous result assembling:  final triple vector and usage update
	for(auto idx0 = 0; idx0 < (int)cam0_pts; ++idx0)
	{
		const auto& dot_triples = local_triples[idx0];

		// insert dot triples to all triples
		triples_.insert(triples_.end(), dot_triples.begin(), dot_triples.end());

		// increment all usage counters
		for(auto idx_local_triple = 0; idx_local_triple < int(local_triples[idx0].size()); ++idx_local_triple)
		{
			// increase usage counter of cams and projector
			camera_sensor0.counter_used[dot_triples[idx_local_triple].camera_index[0]]++;
			camera_sensor1.counter_used[dot_triples[idx_local_triple].camera_index[1]]++;

			if(use_projector)
				projector.counter_used[dot_triples[idx_local_triple].camera_index[2]]++;
		}
	}
}

void ReconstructorLaserDots3D::FindTripleCorrespondencesOnLine()
{
	triples_.clear();
	camera_sensor0.ResetDotUseCounter();
	camera_sensor1.ResetDotUseCounter();

	if(use_projector)
	{
		projector.ResetDotUseCounter();
	}

	// number of all dots in 1st camera to be looped through
	const auto cam0_points_count = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index].size();

	// preallocate empty vector for every cam0 dot
	std::vector<std::vector<TripleCorrespondence>> local_triples(cam0_points_count);

	// Loop all cam0 dots and find potential correspondences
	tbb::parallel_for(size_t(0),
	                  camera_sensor0.rectified_coordinates[camera_sensor1.camera_index].size(),
	                  [&](const size_t idx0)
	                  //for (int idx0 = 0; idx0 < m_cam0.Rect_coords[m_cam1.Cam_idx].size(); ++idx0)
	                  {
		                  // calculate indices of lookup tables
		                  const auto idx1 = camera_sensor1.GetEpipolarLineIndex(camera_sensor0,
		                                                                        camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][idx0
		                                                                        ].y);
		                  const auto idx2 = use_projector
			                                    ? projector.GetEpipolarLineIndex(camera_sensor0,
			                                                                     camera_sensor0.rectified_coordinates[projector.camera_index][idx0].y)
			                                    : 1;

		                  // store triples to preallocated vector
		                  local_triples[idx0] = FindPossiblePoints((int)idx0, idx1, idx2);
	                  });
	//}
	// synchronous result assembling:  final triple vector and usage update

	/*
	if (true)
	{
		std::vector<int> projectorIDToTriples(m_pro0.Orig_coords.size(), -1);
		for (int idx0 = 0; idx0 < (int)nPtsCam0; ++idx0)
		{

			const std::vector<triple_correspondence_type> &dotTriples = localTriples[idx0];

			// increment all usage counters
			for (int iTr = 0; iTr < int(localTriples[idx0].size()); ++iTr)
			{

				int projIndex = dotTriples[iTr].camIndex[2];
				if (projectorIDToTriples[projIndex] < 0)
				{
					projectorIDToTriples[projIndex] = ((int)m_triples.size()) + iTr - 1;
					m_triples.push_back(dotTriples[iTr]);
				}
				else
				{
					//We have a collision so only save the triple if we the backprojection error is smaller
					triple_correspondence_type& triple = m_triples[projectorIDToTriples[projIndex]];
					if ((triple.backProjUncertaintyParallel + triple.backProjUncertaintyPerpendicular) > (dotTriples[iTr].backProjUncertaintyParallel + dotTriples[iTr].backProjUncertaintyPerpendicular))
					{
						projectorIDToTriples[projIndex] = ((int)m_triples.size()) + iTr - 1;
						triple.credibility = -1;
						m_triples.push_back(dotTriples[iTr]);
					}
				}

				// increase usage counter of cams and projector
				m_cam0.counter_used[dotTriples[iTr].camIndex[0]]++;
				m_cam1.counter_used[dotTriples[iTr].camIndex[1]]++;
				if (m_use_projector)
				{
					m_pro0.counter_used[dotTriples[iTr].camIndex[2]]++;
				}

			}
		}
	}
	else
	{
		*/
	// synchronous result assembling:  final triple vector and usage update
	for(auto idx0 = 0; idx0 < (int)cam0_points_count; ++idx0)
	{
		const auto& dot_triples = local_triples[idx0];

		// insert dot triples to all triples
		triples_.insert(triples_.end(), dot_triples.begin(), dot_triples.end());

		// increment all usage counters
		for(auto idx = 0; idx < int(local_triples[idx0].size()); ++idx)
		{
			// increase usage counter of cams and projector
			camera_sensor0.counter_used[dot_triples[idx].camera_index[0]]++;
			camera_sensor1.counter_used[dot_triples[idx].camera_index[1]]++;

			if(use_projector)
			{
				projector.counter_used[dot_triples[idx].camera_index[2]]++;
			}
		}
	}
}

void ReconstructorLaserDots3D::AnalyzeAmbiguities()
{
	std::vector<std::vector<int>> dots[3];
	dots[0].resize(camera_sensor0.DotsCount());
	dots[1].resize(camera_sensor1.DotsCount());
	dots[2].resize(projector.DotsCount());

	for(size_t iTriple = 0; iTriple < triples_.size(); ++iTriple)
	{
		for(int iCam = 0; iCam < 3; ++iCam)
		{
			dots[iCam][triples_[iTriple].camera_index[iCam]].push_back((int)iTriple);
		}
	}

	SFMAlgorithms::UnionFind uf((int)triples_.size());

	for(int iCam = 0; iCam < 3; ++iCam)
	{
		for(size_t iDot = 0; iDot < dots[iCam].size(); ++iDot)
		{
			if(dots[iCam][iDot].size() < 2)
				continue;
			for(size_t j = 0; j < dots[iCam][iDot].size(); ++j)
			{
				uf.unionPartitions(dots[iCam][iDot][0], dots[iCam][iDot][j]);
			}
		}
	}

	const auto partition_count = uf.getNumPartitions();
	std::vector<int> partition_sizes;
	partition_sizes.resize(20, 0);

	for(auto i_part = 0; i_part < partition_count; ++i_part)
	{
		const auto partition_card = uf.getCardinalityIndex(i_part);
		if(partition_card < 20)
			partition_sizes[partition_card]++;
		else
			partition_sizes[19]++;
	}

	for(size_t i = 0; i < partition_sizes.size(); ++i)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : TRIPLE_CONNECTION " << i << " " << partition_sizes[i] << " numPart " << partition_count;
	}
}

// the found triple correspondences contain ambiguities (dots that are used several times)
// this method shall resolve some of them
// in the current version, it only filters unambiguous correspondences

void ReconstructorLaserDots3D::ResolveAmbiguities()
{
	if(!resolve_ambiguities_)
	{
		for(auto& triple : triples_)
		{
			const auto nc0 = camera_sensor0.counter_used[triple.camera_index[0]];
			const auto nc1 = camera_sensor1.counter_used[triple.camera_index[1]];
			const auto np  = projector.counter_used[triple.camera_index[2]];

			triple.credibility = nc0 == 1 && nc1 == 1 && np == 1 ? 1 : 0;
		}

		return;
	}

	//analyzeAmbiguities();

	if(!use_projector)
	{
		return;
	}

	auto count222   = 0;
	auto count_skip = 0;
	auto count3     = 0;
	auto count221   = 0;
	auto count211   = 0;


	std::vector<int> ir0ReducedBy222;
	std::vector<int> ir1ReducedBy222;
	std::vector<int> projReducedBy222;

	ir0ReducedBy222.resize(camera_sensor0.DotsCount(), 0);
	ir1ReducedBy222.resize(camera_sensor1.DotsCount(), 0);
	projReducedBy222.resize(projector.DotsCount(), 0);

	// mark abc triples with a,b,c > 1
	for(auto& triple : triples_)
	{
		const auto nc0 = camera_sensor0.counter_used[triple.camera_index[0]];
		const auto nc1 = camera_sensor1.counter_used[triple.camera_index[1]];
		const auto np  = projector.counter_used[triple.camera_index[2]];

		if(nc0 > 1 && nc1 > 1 && np > 1)
		{
			triple.credibility = -2;
		}
		else
		{
			triple.credibility = 0;
		}

		if(nc0 == 2 && nc1 == 2 && np == 2)
		{
			triple.credibility = -1;
		}
	}

	// delete marked triples and reduce dot counters
	for(size_t i = 0; i < triples_.size(); ++i)
	{
		if(triples_[i].credibility < 0)
		{
			camera_sensor0.counter_used[triples_[i].camera_index[0]]--;
			camera_sensor1.counter_used[triples_[i].camera_index[1]]--;
			projector.counter_used[triples_[i].camera_index[2]]--;

			// mark the decrease of dot usage
			// the 222 filter can cause wrong correspondences, 3d checks later
			ir0ReducedBy222[triples_[i].camera_index[0]]++;
			ir1ReducedBy222[triples_[i].camera_index[1]]++;
			projReducedBy222[triples_[i].camera_index[2]]++;
		}
	}

	auto good = 0;

	for(size_t i = 0; i < triples_.size(); ++i)
	{
		if(triples_[i].credibility < 0)
		{
			triples_[i].credibility = 0;
			continue;
		}

		int nc0 = camera_sensor0.counter_used[triples_[i].camera_index[0]];
		int nc1 = camera_sensor1.counter_used[triples_[i].camera_index[1]];
		int np  = projector.counter_used[triples_[i].camera_index[2]];

		if(nc0 == 2 && nc1 == 2 && np == 2)
			count222++;

		if(nc0 > 2 || nc1 > 2 || np > 2)
		{
			count3++;
		}
		else
		{
			if(nc0 + nc1 + np == 5)
				count221++;

			if(nc0 + nc1 + np == 4)
				count211++;
		}

		if(nc0 == 1 && nc1 == 1 && np == 1) // && ( proj0.counter_used[ in_corresp[i].Pro0_index] == 1) )
		{
			int risky = 0;
			risky += ir0ReducedBy222[triples_[i].camera_index[0]];
			risky += ir1ReducedBy222[triples_[i].camera_index[1]];
			risky += projReducedBy222[triples_[i].camera_index[2]];
			triples_[i].credibility = risky ? -1 : 1;
			good++;
		}
		else
		{
			count_skip++;
			triples_[i].credibility = 0;
		}
	}

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << triples_.size() << " Triples found - " << count_skip << " Skipped - " << count222 <<
 " with 2 2 2 counters)";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << count3 << " Triples with at least one point > 2 usage ";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << count221 << " Triples with 221 - " << count211 << " triples with 211 ";
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Good :" << good;
}

std::vector<int>& ReconstructorLaserDots3D::GetBrightPoints()
{
	return bright_points_;
}

void ReconstructorLaserDots3D::TriangulateTripleCorrespondences(std::vector<cv::Point3f>& points)
{
	cv::Mat R0, T0, R1, T1, R2, T2;

	camera_sensor0.GetCalibration()->GetExtrinsicParameters(R0, T0);
	camera_sensor1.GetCalibration()->GetExtrinsicParameters(R1, T1);
	projector.GetCalibration()->GetExtrinsicParameters(R2, T2);

	Eigen::Map<Eigen::Vector3d> p0((double*)(T0.data), 3, 1);
	Eigen::Map<Eigen::Matrix3d> r0((double*)(R0.data), 3, 3);
	Eigen::Map<Eigen::Vector3d> p1((double*)(T1.data), 3, 1);
	Eigen::Map<Eigen::Matrix3d> r1((double*)(R1.data), 3, 3);

	Eigen::Map<Eigen::Vector3d> p2((double*)(T2.data), 3, 1);
	Eigen::Map<Eigen::Matrix3d> r2((double*)(R2.data), 3, 3);

	point2_triple_.clear();
	bright_points_.clear();

	// reset the mapping from beam index to a point, -1 if laser beam did not result in 3d point
	beam_idx2_point_idx_.clear();
	beam_idx2_point_idx_.resize(beam_neighbours_.size(), -1);

	// We only need to initialize 3d values if depth correction is active
	// and at least one camera sensor cares about it at undistortion
	// initializes pt3d in m_triples
	bool applyDepthCorrection = (camera_sensor0.GetCalibration()->HandlesDepthValues() || camera_sensor1.GetCalibration()->HandlesDepthValues());

	// quickly precalculate depth based on disparity values ( uses TBB )
	if(applyDepthCorrection)
	{
		UpdateTripleCorrespondencesDepth();
	}

	// undistorted coordinates of used points
	Dots linCoords0, linCoords1, origCoords0, origCoords1, linCoordsProj;
	std::vector<cv::Point3d> pts3d;

	if(applyDepthCorrection)
	{
		origCoords0.reserve(triples_.size());
		origCoords1.reserve(triples_.size());
		linCoordsProj.reserve(triples_.size());
		pts3d.reserve(triples_.size());
	}
	else
	{
		linCoords0.reserve(triples_.size());
		linCoords1.reserve(triples_.size());
		linCoordsProj.reserve(triples_.size());
	}

	bright_points_.reserve(triples_.size());

	for(size_t i = 0; i < triples_.size(); ++i)
	{
		if(triples_[i].credibility <= 0)
			continue;

		// triple not skipped, it will result in a 3d point
		// get access to the point via its  beam index
		beam_idx2_point_idx_[triples_[i].camera_index[2]] = (int)point2_triple_.size();

		// keep the triple to point index translation
		point2_triple_.push_back((int)i);

		// need projector index to check whether it is a bright beam
		int ptIdxPro0 = triples_[i].camera_index[2];

		if(applyDepthCorrection)
		{
			// get the original cam0 and cam1 coordinate from triple for depth dependent undistortion
			origCoords0.emplace_back(camera_sensor0.GetDot(triples_[i].camera_index[0]));
			origCoords1.emplace_back(camera_sensor1.GetDot(triples_[i].camera_index[1]));
			linCoordsProj.emplace_back(projector.GetUndistortedDot(triples_[i].camera_index[2]));
			pts3d.emplace_back(triples_[i].pt3d[0], triples_[i].pt3d[1], triples_[i].pt3d[2]);
		}
		else
		{
			// get the undistorted cam0 and cam1 coordinate from triple
			linCoords0.emplace_back(camera_sensor0.GetUndistortedDot(triples_[i].camera_index[0]));
			linCoords1.emplace_back(camera_sensor1.GetUndistortedDot(triples_[i].camera_index[1]));
			linCoordsProj.emplace_back(projector.GetUndistortedDot(triples_[i].camera_index[2]));
		}


		// keep bright beam indices
		if((!bright_only_ && laser_beam_matrix_.at<double>(ptIdxPro0, 0) > 0.5) || bright_only_)
		{
			bright_points_.emplace_back((int)linCoordsProj.size() - 1);
		}
	}

	// calculate distortion correction with depth dependent correction
	if(!origCoords0.empty())
	{
		camera_sensor0.GetCalibration()->UndistortPoints2(origCoords0, linCoords0, pts3d);
		camera_sensor1.GetCalibration()->UndistortPoints2(origCoords1, linCoords1, pts3d);
	}

	// resize result vector
	points.resize(linCoords0.size());

	// Calculate all points for previously collected valid triples of usedCam0, usedCam1 and usedProj
	double maxReproErr = max_reprojection_error_;

	tbb::parallel_for(size_t(0),
	                  linCoords0.size(),
	                  [&](size_t iPt)
	                  {
		                  Eigen::Matrix3d directions(Eigen::Matrix3d::Zero());
		                  Eigen::Vector3d positions(Eigen::Vector3d::Zero());

		                  // Get ray direction cam 0
		                  Eigen::Vector3d v0(linCoords0[iPt].x, linCoords0[iPt].y, 1.0);
		                  Eigen::Vector3d w0 = (r0 * v0).normalized();
		                  Eigen::Matrix3d V0 = Eigen::Matrix3d::Identity() - (w0 * w0.transpose());
		                  directions += V0;
		                  positions -= V0 * r0 * p0;

		                  // Get ray direction cam 1
		                  Eigen::Vector3d v1(linCoords1[iPt].x, linCoords1[iPt].y, 1.0);
		                  Eigen::Vector3d w1 = (r1 * v1).normalized();
		                  Eigen::Matrix3d V1 = Eigen::Matrix3d::Identity() - (w1 * w1.transpose());
		                  directions += V1;
		                  positions -= V1 * r1 * p1;

		                  if(triangulate_with_projector)
		                  {
			                  Eigen::Vector3d v2(linCoordsProj[iPt].x, linCoordsProj[iPt].y, 1.0);
			                  Eigen::Vector3d w2 = (r2 * v2).normalized();
			                  Eigen::Matrix3d V2 = Eigen::Matrix3d::Identity() - (w2 * w2.transpose());
			                  directions += V2;
			                  positions -= V2 * r2 * p2;
		                  }

		                  // Using Eigen::inverve to solve the equtions for t is
		                  //	not reproducible. The matrix "direction" may be
		                  //	ill-conditioned. Using the SVD for solving improves
		                  //	repro of replay-processes.
		                  Eigen::Vector3d t = directions.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV).solve(positions);

		                  points[iPt] = cv::Point3f((float)t[0], (float)t[1], (float)t[2]);

		                  // Calculate re-projection error
		                  //	Mark points with re-projection error greater
		                  //	than a given limit (m_maxReprojectionError)

		                  /*
		                  double reproErr = 0.0;

		                  cv::Vec2f pp0 = m_cam0.calib->projectPoint(points[iPt], true);
		                  cv::Vec2f pp1 = m_cam1.calib->projectPoint(points[iPt], true);
		                  cv::Point2f pp2 = m_pro0.calib->projectPoint(points[iPt], true);

		                  reproErr += cv::norm(usedCam0[iPt] - pp0, cv::NORM_L2SQR);
		                  reproErr += cv::norm(usedCam1[iPt] - pp1, cv::NORM_L2SQR);
		                  reproErr += cv::norm(usedProj[iPt] - pp2, cv::NORM_L2SQR);
		                  reproErr = sqrt(reproErr / 3.0);

		                  if (reproErr > maxReproErr)
			                  m_triples[m_point2triple[iPt]].credibility = 0;

		                  */
	                  }
	);
}

void ReconstructorLaserDots3D::SetTripleCorrespondences(const std::vector<TripleCorrespondence>& triples)
{
	triples_ = triples;
}

void ReconstructorLaserDots3D::UpdateTripleCorrespondencesDepth()
{
	Eigen::Matrix3d R, Rinverse;
	for(int i = 0; i < 3; ++i)
		for(int j   = 0; j < 3; ++j)
			R(i, j) = camera_sensor0.GetR2(camera_sensor1.camera_index).at<double>(i, j);

	Rinverse = R.inverse();
	Eigen::Map<Eigen::Matrix4d, 0, Eigen::Stride<1, 4>> Q((double*)camera_sensor0.GetQ(camera_sensor1.camera_index).ptr());

	cv::Mat Rcv, Tcv;
	camera_sensor0.GetCalibration()->GetExtrinsicParameters(Rcv, Tcv);
	Eigen::Map<Eigen::Vector3d> t((double*)Tcv.ptr());
	Eigen::Map<Eigen::Matrix3d, 0, Eigen::Stride<3, 1>> Rinv((double*)Rcv.ptr());
	Eigen::Vector3d tInv = -Rinv * t;

	tbb::parallel_for(size_t(0),
	                  triples_.size(),
	                  [&](size_t i)
	                  {
		                  double x0, y0, x1, y1, z;
		                  x0 = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][triples_[i].camera_index[0]].x;
		                  //?? Attention: fix coordinate problematic when changing rotation between cameras
		                  y0 = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][triples_[i].camera_index[0]].y;
		                  x1 = camera_sensor1.rectified_coordinates[camera_sensor0.camera_index][triples_[i].camera_index[1]].x;
		                  y1 = camera_sensor1.rectified_coordinates[camera_sensor0.camera_index][triples_[i].camera_index[1]].y;

		                  //disparity:
		                  z = x0 - x1;

		                  Eigen::Vector4d ehomPoint1(x0, (y0 + y1) * 0.5, z, 1);
		                  // Q * [x y disparity 1]^T
		                  Eigen::Vector4d ehomPoint2 = Q * ehomPoint1;
		                  // transform back from homogeneous coordinates
		                  Eigen::Vector3d ehomPoint3(ehomPoint2(0) / ehomPoint2(3), ehomPoint2(1) / ehomPoint2(3), ehomPoint2(2) / ehomPoint2(3));
		                  // Rotate back from rectified camera view
		                  ehomPoint3 = Rinverse * ehomPoint3;

		                  Eigen::Vector3d pt  = Rinv * ehomPoint3 + tInv;
		                  triples_[i].pt3d[0] = pt[0];
		                  triples_[i].pt3d[1] = pt[1];
		                  triples_[i].pt3d[2] = pt[2];
	                  });
}

std::vector<ReconstructorLaserDots3D::TripleCorrespondence> ReconstructorLaserDots3D::FindPossiblePoints(int cam0_index,
	int corr_idx1,
	int corr_idx2)
{
	std::vector<TripleCorrespondence> result;

	if(corr_idx1 < 0 || corr_idx1 >= (int)camera_sensor1.lookup_correspondences.size() || corr_idx2 < 0 || corr_idx2 >= (
		int)projector.lookup_correspondences.size())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : FindIntersection can fail : corr1 " << corr_idx1 << "( " << camera_sensor1
		                                                                                                                    .lookup_correspondences.
		                                                                                                                    size() << " )";

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : FindIntersection cam0 idx " << cam0_index << " img pos " << camera_sensor0.
original_coordinates[cam0_index].x << " " << camera_sensor0.original_coordinates[cam0_index].y;

		//VGDebug(" findIntersection cam0  cam1 epi idx %d ", m_cam0.
		return result;
	}

	const auto max_num_correspondences = 9; // attention: hard-coded break-off of correspondence search after 10 found per point.

	TripleCorrespondence help_correspondence;
	result.reserve(max_num_correspondences);

	std::vector<int>& cam1Candidates = camera_sensor1.lookup_correspondences[corr_idx1];
	std::vector<int>& pro0Candidates = projector.lookup_correspondences[corr_idx2];
	bool invDirCam0Cam1              = camera_sensor0.camera_rotated[camera_sensor1.camera_index];
	bool invDirCam0Proj              = camera_sensor0.matrix_q[projector.camera_index].at<double>(3, 2) < 0;

	if(cam1Candidates.size() == 0 || pro0Candidates.size() == 0)
	{
		result.clear();
		return result;
	}

	// cam0 measurement, rectified to cam1 component parallel to epipolar line
	double Cam0Cam1ParCoord = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][cam0_index].x;

	// cam0 measurement, rectified to proj0 component parallel to epipolar line
	double Cam0ProjParCoord                = camera_sensor0.rectified_coordinates[projector.camera_index][cam0_index].x;
	std::vector<int>::iterator Pro0iterlow = projector.lookup_correspondences[corr_idx2].begin();
	std::vector<int>::iterator Pro0stop    = projector.lookup_correspondences[corr_idx2].end();
	std::vector<int>::iterator Pro0iterhig = Pro0iterlow;

	for(std::vector<int>::iterator cam1iter = cam1Candidates.begin(); cam1iter != cam1Candidates.end(); ++cam1iter)
	{
		int cam1CandidatesIdx   = *cam1iter;
		double Cam1Cam0ParCoord = camera_sensor1.rectified_coordinates[camera_sensor0.camera_index][cam1CandidatesIdx].x;

		if((!invDirCam0Cam1 && Cam1Cam0ParCoord < Cam0Cam1ParCoord) ||
			(invDirCam0Cam1 && Cam1Cam0ParCoord > Cam0Cam1ParCoord))
		{
			// actually a break should do the same more efficiently, but it does not
			// It should because candidate vector is sorted by coordinate in direction epipol line
			continue;
		}

		// e3-e4: exactly check Cam0-Cam1 relation because being member of candidate lists
		// is only a required but not a sufficient criterion.
		// The selection by candidate lists according to bins was too coarse
		double e3 = camera_sensor0.rectified_coordinates[camera_sensor1.camera_index][cam0_index].y;
		double e4 = camera_sensor1.rectified_coordinates[camera_sensor0.camera_index][cam1CandidatesIdx].y;

		//VGDebug(" CORRESPONDENCE STEP 2 %f ", fabs(e3-e4) );
		// e5-e6: Cam0-Pro0-relation
		if(fabs(e3 - e4) <= (camera_sensor0.uncertainty + camera_sensor1.uncertainty))
		{
			// get projector rectified coordinate of cam1 candidate
			double e1 = camera_sensor1.rectified_coordinates[projector.camera_index][cam1CandidatesIdx].y;
			// with every increase of the Cam1-iterator, the iterators Pro0iterlow and Pro0iterhigh are adjusted such that they
			// limit a range of possible correspondences in the Pro0 view

			int Pro0low_index = *Pro0iterlow;
			double e2low      = projector.rectified_coordinates[camera_sensor1.camera_index][Pro0low_index].y;
			while((e2low < e1 - (camera_sensor1.uncertainty + projector.uncertainty)) && (Pro0iterlow != Pro0stop))
			{
				Pro0low_index = *Pro0iterlow;
				Pro0iterlow++;
				if(Pro0iterlow == Pro0stop)
				{
					Pro0iterlow--;
					break;
				}
				else
				{
					Pro0low_index = *Pro0iterlow;
					e2low         = projector.rectified_coordinates[camera_sensor1.camera_index][Pro0low_index].y;
				}
			}

			int Pro0hig_index = *Pro0iterhig;
			double e2hig      = projector.rectified_coordinates[camera_sensor1.camera_index][Pro0hig_index].y;
			while((e2hig < e1 + (camera_sensor1.uncertainty + projector.uncertainty)) && (Pro0iterhig != Pro0stop))
			{
				Pro0hig_index = *Pro0iterhig;
				Pro0iterhig++;
				if(Pro0iterhig == Pro0stop)
				{
					Pro0iterhig--;
					break;
				}
				else
				{
					Pro0hig_index = *Pro0iterhig;
					e2hig         = projector.rectified_coordinates[camera_sensor1.camera_index][Pro0hig_index].y;
				}
			}

			std::vector<int>::iterator pro0_iter;
			for(pro0_iter = Pro0iterlow; pro0_iter <= Pro0iterhig; ++pro0_iter)
			{
				auto pro0_index = *pro0_iter;

				const auto projector_point = projector.rectified_coordinates[camera_sensor0.camera_index][pro0_index];

				//Check for negative disparity of cam0 and projector
				if(!invDirCam0Proj && projector_point.x > Cam0ProjParCoord ||
					invDirCam0Proj && projector_point.x < Cam0ProjParCoord)
				{
					continue;
				}


				// get cam1 rectified coordinate of projector candidate
				const double e2 = projector.rectified_coordinates[camera_sensor1.camera_index][pro0_index].y;

				//VGDebug("Cam0_index,Cam1_index,Pro0_index, %d, %d,%d,%f,%f", Cam0_index,Cam1_index,Pro0_index,e1, e2);
				//VGDebug(" CORRESPONDENCE STEP 1 %f ", fabs(e1-e2) );
				if(fabs(e1 - e2) <= (camera_sensor1.uncertainty + projector.uncertainty))
				{
					//Check if we are on epipolar line
					// e5-e6: exactly check Cam0-Proj relation because being member of candidate lists
					// is only a required but not a sufficient criterion. (due to discretization)
					// The selection by candidate lists according to bins was too coarse
					double e5 = camera_sensor0.rectified_coordinates[projector.camera_index][cam0_index].y;
					double e6 = projector_point.y;

					if(fabs(e5 - e6) <= (camera_sensor0.uncertainty + projector.uncertainty))
					{
						cv::Point2f projected_point_projector;
						auto disparity = Cam0Cam1ParCoord - Cam1Cam0ParCoord;

						//Calculate point in Projector 2D Space
						camera_sensor0.ProjectPointToThirdCamera(Cam0Cam1ParCoord,
						                                         (e3 + e4) * 0.5,
						                                         disparity,
						                                         camera_sensor1.camera_index,
						                                         projector.camera_index,
						                                         projected_point_projector);

						const double parallel_diff      = fabs(projector_point.x - projected_point_projector.x);
						const double perpendicular_diff = fabs(projector_point.y - projected_point_projector.y);

						if(parallel_diff < projector_back_projection_uncertainty_ && perpendicular_diff < projector_back_projection_uncertainty_)
						{
							help_correspondence.camera_index[0]                           = cam0_index;
							help_correspondence.camera_index[1]                           = cam1CandidatesIdx;
							help_correspondence.camera_index[2]                           = pro0_index;
							help_correspondence.back_projection_uncertainty_parallel      = parallel_diff;
							help_correspondence.back_projection_uncertainty_perpendicular = perpendicular_diff;
							help_correspondence.credibility                               = 0;
							result.push_back(help_correspondence);
						}
					}
				}
			}
		}
	}

	return result;
}

void ReconstructorLaserDots3D::UpdateTripleCorrespondencesDepthTriangulation()
{
	cv::Mat R0, T0, R1, T1, R2, T2;

	camera_sensor0.calibration->GetExtrinsicParameters(R0, T0);
	camera_sensor1.calibration->GetExtrinsicParameters(R1, T1);
	projector.calibration->GetExtrinsicParameters(R2, T2);

	Eigen::Map<Eigen::Vector3d> p0((double*)(T0.data), 3, 1);
	Eigen::Map<Eigen::Matrix3d> r0((double*)(R0.data), 3, 3);
	Eigen::Map<Eigen::Vector3d> p1((double*)(T1.data), 3, 1);
	Eigen::Map<Eigen::Matrix3d> r1((double*)(R1.data), 3, 3);

	Eigen::Map<Eigen::Vector3d> p2((double*)(T2.data), 3, 1);
	Eigen::Map<Eigen::Matrix3d> r2((double*)(R2.data), 3, 3);

	//for (size_t iPt = 0; iPt<  usedCam0.size() ;iPt++ )
	for(size_t iPt = 0; iPt < triples_.size(); ++iPt)
	{
		Eigen::Matrix3d directions(Eigen::Matrix3d::Zero());
		Eigen::Vector3d positions(Eigen::Vector3d::Zero());

		// Get ray direction cam 0
		Eigen::Vector3d v0(camera_sensor0.undistorted_coordinates[triples_[iPt].camera_index[0]].x,
		                   camera_sensor0.undistorted_coordinates[triples_[iPt].camera_index[0]].y,
		                   1.0);
		Eigen::Vector3d w0 = (r0 * v0).normalized();
		Eigen::Matrix3d V0 = Eigen::Matrix3d::Identity() - (w0 * w0.transpose());
		directions += V0;
		positions -= V0 * r0 * p0;

		// Get ray direction cam 1
		Eigen::Vector3d v1(camera_sensor1.undistorted_coordinates[triples_[iPt].camera_index[1]].x,
		                   camera_sensor1.undistorted_coordinates[triples_[iPt].camera_index[1]].y,
		                   1.0);

		Eigen::Vector3d w1 = (r1 * v1).normalized();
		Eigen::Matrix3d V1 = Eigen::Matrix3d::Identity() - (w1 * w1.transpose());
		directions += V1;
		positions -= V1 * r1 * p1;

		if(triangulate_with_projector)
		{
			Eigen::Vector3d v2(projector.undistorted_coordinates[triples_[iPt].camera_index[2]].x,
			                   projector.undistorted_coordinates[triples_[iPt].camera_index[2]].y,
			                   1.0);

			Eigen::Vector3d w2 = (r2 * v2).normalized();
			Eigen::Matrix3d V2 = Eigen::Matrix3d::Identity() - (w2 * w2.transpose());
			directions += V2;
			positions -= V2 * r2 * p2;
		}

		Eigen::Vector3d t = directions.inverse() * positions;

		triples_[iPt].pt3d[0] = (float)t[0];
		triples_[iPt].pt3d[1] = (float)t[1];
		triples_[iPt].pt3d[2] = (float)t[2];
	}
}

ReconstructorLaserDots3D::ReconstructorLaserDots3D(std::shared_ptr<ConfigurationContainer> options) :
	use_stored_wavelength_comp_for_next_triangulation_(false)
{
	camera_sensor0 = Dot2DData(0);
	camera_sensor1 = Dot2DData(1);
	projector      = Dot2DData(2);

	// reserve space for triples
	triples_.reserve(20000);

	// Ignore return value. Parameter options may be nullptr.
	Initialize(options);
}

bool ReconstructorLaserDots3D::Initialize(std::shared_ptr<ConfigurationContainer> options)
{
	general_uncertainty_ = 0.35;
	uncertainty_proj_fac_              = 1.0;
	auto_compensation_iterations_      = 2;
	auto_compensation_calls_           = 0;
	compensation_min_pts_              = 300;
	auto_compensation_limit_           = 0.01;
	max_reprojection_error_            = 0.4;
	resolve_ambiguities_               = true;
	triangulate_with_projector         = false;
	force_deep_projector_compensation_ = false;
	wavelength_cnum_diff_delta_        = 1e-5;
	wavelength_clevmar_                = true;
	wavelength_cmax_fev_               = 500;
	wavelength_cx_tolerance_           = 1e-5;
	projector_radius_factor_           = 0.9;
	max_err_proj_                      = 0.3;
	max_err_cams_                      = 0.2;
	max_pt_dist_                       = 0.1f;
	search_bended_projector_           = true;
	search_wave_length_                = true;
	wavelength_compensation_on_        = true;

	// set uncertainties
	SetUncertainties(general_uncertainty_, general_uncertainty_, general_uncertainty_ * uncertainty_proj_fac_);
	bright_only_ = false;

	return options != nullptr;
}

void ReconstructorLaserDots3D::SetUncertainties(const double ir0,
                                                const double ir1,
                                                const double proj)
{
	camera_sensor0.uncertainty = ir0;
	camera_sensor1.uncertainty = ir1;
	projector.uncertainty      = proj;
}

ReconstructorLaserDots3D::~ReconstructorLaserDots3D()
{
}

void ReconstructorLaserDots3D::SetTriangulateWithProjector(const bool val)
{
	triangulate_with_projector = val;
}

void ReconstructorLaserDots3D::SetCoordinates(const Dots& dot_coords0,
                                              const Dots& dot_coords1)
{
	/*tbb::parallel_invoke(
		[&]()*/
		{
			// Copy dots and undistort
			camera_sensor0.SetDotCoords(dot_coords0);

			// rectify undistorted points for both other
			camera_sensor0.RectifyPoints(camera_sensor1);
			camera_sensor0.RectifyPoints(projector);
		}/*,
		[&]()*/
		{
			// Copy dots and undistort
			camera_sensor1.SetDotCoords(dot_coords1);

			// rectify undistorted points for both other
			camera_sensor1.RectifyPoints(camera_sensor0);
			camera_sensor1.RectifyPoints(projector);

			// Update the look up table to access all cam1 points that potentially correspond
			// to a cam0 epipolar line, candidate vectors sorted by distance to projector
			camera_sensor1.GenerateLookupCorrespondences(camera_sensor0, projector, use_projector);
		}
	//);
}

cv::Point2f ReconstructorLaserDots3D::GetProjectorCoordinate(const int idx) const
{
	return idx < projector.DotsCount() ? projector.GetDot(idx) : cv::Point2f(FLT_MAX, FLT_MAX);
}

cv::Point2f ReconstructorLaserDots3D::GetCam0Coordinate(const int idx) const
{
	return idx < camera_sensor0.DotsCount() ? camera_sensor0.GetDot(idx) : cv::Point2f(FLT_MAX, FLT_MAX);
}

cv::Point2f ReconstructorLaserDots3D::GetCam1Coordinate(const int idx) const
{
	return idx < camera_sensor1.DotsCount() ? camera_sensor1.GetDot(idx) : cv::Point2f(FLT_MAX, FLT_MAX);
}

void ReconstructorLaserDots3D::GetCoordinatesPro0(Dots& dot_coords)
{
	dot_coords.assign(projector.GetDots().begin(), projector.GetDots().end());
}

void ReconstructorLaserDots3D::SetCoordinatesPro0(const Dots& dot_coords)
{

	/*std::ofstream output;
	const auto output_path = R"(D:\TestData\output\SetCoordinatesPro0_undistorted_coordinates_)" + Helper::GetTimeStamp() + ".txt";
	output.open(output_path);
	if(output.is_open())
	{
		for(size_t i = 0; i < dot_coords.size(); i++)
		{
			output << dot_coords.at(i).x << " "
				<< dot_coords.at(i).y << " "
				<< " 1 \n";
		}

		output.close();
	}*/

	projector.SetDotCoords(dot_coords);

	projector.RectifyPoints(camera_sensor0);
	projector.RectifyPoints(camera_sensor1);

	// Update the look up table to access all projector points that potentially correspond
	// to a cam0 epipolar line, candidate vectors sorted by distance to cam1
	projector.GenerateLookupCorrespondences(camera_sensor0, camera_sensor1, true);
}


bool ReconstructorLaserDots3D::SetCalibrations(const std::shared_ptr<const CameraSensor> camera_calibration0,
                                               const std::shared_ptr<const CameraSensor> camera_calibration1,
                                               const std::shared_ptr<const CameraSensor> projector_calibration)
{
	return camera_sensor0.SetCalibration(camera_calibration0) &&
		camera_sensor1.SetCalibration(camera_calibration1) &&
		projector.SetCalibration(projector_calibration);
}

bool ReconstructorLaserDots3D::SetCalibrationAndPrepare(const CaptureCalibration& calibration)
{
	const auto camera_calibration0   = calibration.GetCalibration(CHANNEL_GREY0);
	const auto camera_calibration1   = calibration.GetCalibration(CHANNEL_GREY1);
	const auto projector_calibration = calibration.GetCalibration(CHANNEL_PROJECTOR0);

	if(!camera_calibration0)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot access channel gray0 channel";
		return false;
	}

	if(!camera_calibration1)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot access channel gray1 channel";
		return false;
	}

	if(!projector_calibration)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Cannot access channel projector0 channel";
		return false;
	}

	return SetCalibrations(camera_calibration0, camera_calibration1, projector_calibration) && Preparation();
}

void ReconstructorLaserDots3D::GetUpdatedCalibration(CaptureCalibration& calibration) const
{
	calibration.SetCalibration(CHANNEL_GREY0, camera_sensor0.GetCalibration());
	calibration.SetCalibration(CHANNEL_GREY1, camera_sensor1.GetCalibration());
	calibration.SetCalibration(CHANNEL_PROJECTOR0, projector.GetCalibration());

	cv::Mat laser_dots;
	GetLaserDotMatrix(laser_dots);
	calibration.SetCustomMatrix("laser_beams", laser_dots);
}

void ReconstructorLaserDots3D::GetLaserDotMatrix(cv::Mat& laser_dots) const
{
	laser_beam_matrix_.copyTo(laser_dots);

	for(auto i = 0; i < projector.DotsCount(); ++i)
	{
		laser_dots.at<double>(i, 1) = projector.GetDot(i).x;
		laser_dots.at<double>(i, 2) = projector.GetDot(i).y;
	}
}

int ReconstructorLaserDots3D::TriangulatePoints(const Dots& dots0,
                                                const Dots& dots1,
                                                std::vector<cv::Point3f>& triangulated_points,
                                                std::vector<int>& point_flags,
                                                const bool search_if_not_sufficient_pts,
                                                const unsigned int search_every_n_frame,
                                                const unsigned int necessary_amount_of_points_divider)
{
	if(auto_compensation_iterations_ > 0)
	{
		const auto correspondence_ok            = triangulated_points_.size() > dots0.size() / necessary_amount_of_points_divider;
		const auto search_for_wavelength_params = search_if_not_sufficient_pts || (!correspondence_ok && auto_compensation_calls_ %
			search_every_n_frame == search_every_n_frame - 1 || force_deep_projector_compensation_);

		TriangulateWithAutoWaveLengthCorrection(dots0, dots1, triangulated_points, search_for_wavelength_params);
		++auto_compensation_calls_;
		// If the calculation was not successful, try it again
		//	for the next frame. Otherwise reduce the number of
		//	updates
		//if (!hasGoodAccumulatedScaleCorrection())
		//	m_autoCompensationCalls = 0;
		//else
		//	++m_autoCompensationCalls;
	}
	else
	{
		SetCoordinates(dots0, dots1);
		triangulated_points = Calculate3DData();
	}

	MarkPointIndices(point_flags, triangulated_points.size());

	return int(triangulated_points.size());
}

int ReconstructorLaserDots3D::TriangulatePoints(const Dots& dots0,
                                                const Dots& dots1,
                                                std::vector<cv::Point3f>& triangulated_points,
                                                std::vector<int>& point_flags,
                                                const bool search_if_not_sufficient_pts)
{
	// Controls the frequency of the wave length compensation updates
	static int calls = 0;

	//Not enough points in at least one camera
	if(dots0.empty() || dots1.empty())
	{
		return 0;
	}

	if(auto_compensation_iterations_ > 0)
	{
		const auto correspondence_ok            = triangulated_points_.size() > dots0.size() / 4;
		const auto search_for_wavelength_params = search_if_not_sufficient_pts || (!correspondence_ok && calls % 10 == 9 ||
			force_deep_projector_compensation_);
		TriangulateWithAutoWaveLengthCorrection(dots0, dots1, triangulated_points, search_for_wavelength_params);
		++calls;
		// If the calculation was not successful, try it again
		//	for the next frame. Otherwise reduce the number of
		//	updates
		//if (!hasGoodAccumulatedScaleCorrection())
		//	calls = 0;
		//else
		//	++calls;
	}
	else
	{
		SetCoordinates(dots0, dots1);
		triangulated_points = Calculate3DData();
	}

	point_flags.resize(triangulated_points.size(), 0);

	// write projector beam index to flags
	for(size_t i = 0; i < triangulated_points.size(); ++i)
	{
		int doeIdx = GetUsedCorrespondence((int)i).camera_index[2];
		point_flags[i] |= (doeIdx & BEAM_INDEX_16BIT);
	}

	// mark all bright points
	std::vector<int> bright_pts = GetBrightPoints();
	for(size_t i = 0; i < bright_pts.size(); ++i)
	{
		point_flags[bright_pts[i]] |= IS_BRIGHT;
	}

	return (int)triangulated_points.size();
}

int ReconstructorLaserDots3D::TriangulatePointsAlgoChoice(const Dots& dots0,
                                                          const Dots& dots1,
                                                          std::vector<cv::Point3f>& triangulated_points,
                                                          std::vector<int>& point_flags,
                                                          const bool use_back_projection_correspondence_search,
                                                          const bool search_if_not_sufficient_pts,
                                                          const int search_every_n_frame,
                                                          const unsigned int necessary_amount_of_points_divider)
{
	use_online_correspondence_search = use_back_projection_correspondence_search;
	// Controls the frequency of the wave length compensation updates
	static int calls = 0;

	//Not enough points in at least one camera
	if(dots0.size() < 200 || dots1.size() < 200)
	{
		return 0;
	}

	if(auto_compensation_iterations_ > 0)
	{
		bool correspondenceOK         = triangulated_points_.size() > dots0.size() / necessary_amount_of_points_divider;
		bool seachForWavelengthParams = (search_if_not_sufficient_pts && (!correspondenceOK && (calls % search_every_n_frame == search_every_n_frame -
				1)) ||
			force_deep_projector_compensation_);
		TriangulateWithAutoWaveLengthCorrection(dots0, dots1, triangulated_points, seachForWavelengthParams);
		++calls;
		// If the calculation was not successful, try it again
		//	for the next frame. Otherwise reduce the number of
		//	updates
		//if (!hasGoodAccumulatedScaleCorrection())
		//	calls = 0;
		//else
		//	++calls;
	}
	else
	{
		SetCoordinates(dots0, dots1);
		triangulated_points = Calculate3DData();
	}

	point_flags.resize(triangulated_points.size(), 0);

	// write projector beam index to flags
	for(size_t i = 0; i < triangulated_points.size(); ++i)
	{
		int doeIdx = GetUsedCorrespondence((int)i).camera_index[2];
		point_flags[i] |= (doeIdx & BEAM_INDEX_16BIT);
	}

	// mark all bright points
	std::vector<int> brightPts = GetBrightPoints();
	for(size_t i = 0; i < brightPts.size(); ++i)
	{
		point_flags[brightPts[i]] |= IS_BRIGHT;
	}

	return int(triangulated_points.size());
}

void ReconstructorLaserDots3D::SetAutoCompensation(const int iterations)
{
	auto_compensation_iterations_ = iterations;
}

int ReconstructorLaserDots3D::GetAutoCompensation() const
{
	return auto_compensation_iterations_;
}


void ReconstructorLaserDots3D::SetProjectorAutoCompensationLimit(const double limit)
{
	auto_compensation_limit_ = limit;
}

double ReconstructorLaserDots3D::GetProjectorAutoCompensationLimit() const
{
	return auto_compensation_limit_;
}

int ReconstructorLaserDots3D::GetWaveLengthCompensationMinPts() const
{
	return compensation_min_pts_;
}

void ReconstructorLaserDots3D::SetLaserBeamsFromCalibration(const cv::Mat& mat)
{
	// column 0 contains brightness column 1+2 the coordinates
	// copy matrix containing brightness and direction of beams
	mat.copyTo(laser_beam_matrix_);
	bright_beams_from_calibration_.clear();
	all_beams_from_calibration_.clear();

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Run Freestyle with " << mat.rows << " dots pattern ";

	for(auto i = 0; i < mat.rows; ++i)
	{
		const auto brightness = mat.at<double>(i, 0);
		cv::Vec2f coordinate((float)mat.at<double>(i, 1), (float)mat.at<double>(i, 2));

		all_beams_from_calibration_.push_back(coordinate);

		if(brightness > 0.5)
		{
			bright_beams_from_calibration_.push_back(coordinate);
		}
	}

	UpdateProjectorPtsToPatternCompensationState(AccumulatedWavelengthCompInfo());
	InitBeamNeighbors(mat);
}

void ReconstructorLaserDots3D::UseBrightOnly(bool bright_only)
{
	const auto before = AccumulatedWavelengthCompInfo();
	bright_only_      = bright_only;
	ResetPattern();
	UpdateProjectorPtsToPatternCompensationState(before);
}

bool ReconstructorLaserDots3D::Preparation()
{
	if(!camera_sensor0.GetCalibration())
		return false;

	if(!camera_sensor1.GetCalibration())
		return false;

	camera_sensor0.StereoRectifyCams(camera_sensor1);

	if(use_projector)
	{
		if(!projector.GetCalibration())
			return false;

		camera_sensor0.StereoRectifyCams(projector);
		camera_sensor1.StereoRectifyCams(projector);
	}

	return true;
}

std::vector<cv::Point3f> ReconstructorLaserDots3D::Calculate3DData()
{
	FindTripleCorrespondences();
	ResolveAmbiguities();
	TriangulateTripleCorrespondences(triangulated_points_);

	return triangulated_points_;
}

std::vector<cv::Point3f> ReconstructorLaserDots3D::Calculate3DDataCorrespondencesGiven()
{
	triples_.clear();

	for(size_t i = 0; i < camera_sensor0.DotsCount(); ++i)
	{
		TripleCorrespondence triple;
		triple.camera_index[0] = int(i);
		triple.camera_index[1] = int(i);
		triple.camera_index[2] = int(i);
		triple.credibility     = 1;
		triples_.push_back(triple);
	}

	TriangulateTripleCorrespondences(triangulated_points_);

	return triangulated_points_;
}

ReconstructorLaserDots3D::TripleCorrespondence ReconstructorLaserDots3D::GetUsedCorrespondence(const int point_idx)
{
	return triples_[point2_triple_[point_idx]];
}

double ReconstructorLaserDots3D::GetProjectionError(std::vector<cv::Vec4f>& errors,
                                                    const int channel)
{
	errors.clear();

	if(triangulated_points_.empty())
		return 0.0;

	// get the selected camera/projector
	Dot2DData* cam = nullptr;
	switch(channel)
	{
	case 0: cam = &camera_sensor0;
		break;
	case 1: cam = &camera_sensor1;
		break;
	case 2: cam = &projector;
		break;
	default: cam = &camera_sensor0;
	}

	// Get camera/projector calibration and project detected 3d points into image
	std::vector<cv::Point2f> proj;
	cam->GetCalibration()->ProjectPoints(triangulated_points_, proj);

	// fill measurement/projection vector
	std::vector<cv::Vec4f> ir0errors;
	double sqNormSum = 0.0;

	for(auto i = 0; i < triangulated_points_.size(); ++i)
	{
		auto triple = GetUsedCorrespondence((int)i);

		cv::Point2f measurement(cam->GetDot(triple.camera_index[channel]));
		cv::Point2f diff = measurement - proj[i];
		sqNormSum += diff.ddot(diff);

		// no measurement but beam coordinate at projector
		errors.emplace_back(measurement.x, measurement.y, proj[i].x, proj[i].y);
	}

	sqNormSum /= double(triangulated_points_.size());

	return sqrt(sqNormSum);
}

void ReconstructorLaserDots3D::GetProjectionError(Dots& errors,
                                                  const int channel)
{
	errors.clear();

	if(triangulated_points_.empty())
	{
		return;
	}

	// get the selected camera/projector
	Dot2DData* cam = nullptr;
	switch(channel)
	{
	case 0: cam = &camera_sensor0;
		break;
	case 1: cam = &camera_sensor1;
		break;
	case 2: cam = &projector;
		break;
	default: cam = &camera_sensor0;
	}

	// Get camera/projector calibration and project detected 3d points into image
	std::vector<cv::Point2f> proj;
	cam->calibration->ProjectPoints(triangulated_points_, proj);

	Dots undistorted;
	cam->calibration->UndistortPoints2(proj, undistorted, triangulated_points_);

	// fill measurement/projection vector
	double sqNormSum = 0.0;

	for(size_t i = 0; i < triangulated_points_.size(); ++i)
	{
		auto triple = GetUsedCorrespondence(int(i));

		cv::Point2f measurement(cam->original_coordinates[triple.camera_index[channel]].x,
		                        cam->original_coordinates[triple.camera_index[channel]].y);

		const auto diff = measurement - proj[i];

		cv::Vec2f v;
		v[0] = diff.x; // no measurement but beam coordinate at projector
		v[1] = diff.y; // no measurement but beam coordinate at projector
		errors.push_back(v);
	}
}

bool ReconstructorLaserDots3D::CalculateDeviationAngleOfCamera(const int channel,
                                                               double& angular_x,
                                                               double& angular_y)
{
	DotInfos reprojectionErrors;
	GetProjectionError(reprojectionErrors, channel);
	//std::vector<cv::Vec4f> reprojectioinErrors2;
	//getProjectionError(reprojectioinErrors2, ch);

	Dot2DData* cam = nullptr;
	switch(channel)
	{
	case 0: cam = &camera_sensor0;
		break;
	case 1: cam = &camera_sensor1;
		break;
	case 2: cam = &projector;
		break;
	default: cam = &camera_sensor0;
	}

	cv::Mat intrinsics;
	cv::Mat distortion;
	cam->calibration->GetCameraIntrinsicParams(intrinsics, distortion);

	//float fx = (float)intrinsics(0, 0);
	//float fy = (float)intrinsics(1, 1);

	if(reprojectionErrors.size() == 0)
	{
		return false;
	}
	/*
	cv::Vec4f sum2 = cv::Vec4f(0,0,0,0);
	for (size_t i = 0; i < reprojectionErrors.size(); ++i)
	{
		//Calculate Angle
		sum2[0] += std::atan((reprojectionErrors[i][0] - 640.0) / fx) - std::atan((reprojectionErrors[i][2] - 640.0)/ fx);
		sum2[1] += std::atan((reprojectionErrors[i][1] - 512.0) / fy) - std::atan((reprojectionErrors[i][3] - 512.0)/ fy);
	}
	cv::Vec2f average2;
	angularx = sum2[0] / reprojectionErrors.size();
	angulary = sum2[1] / reprojectionErrors.size();
	*/

	//First we erease outliers so sort by length of vector
	//std::sort(reprojectionErrors.begin(), reprojectionErrors.end(), sortByVectorLength);
	//Upper 10 percent are outliers
	size_t endfiltered = size_t((double)reprojectionErrors.size());
	//size_t startFiltered = (size_t)(((double)reprojectionErrors.size()) *0.1);
	size_t startFiltered = 0;
	size_t filtered      = endfiltered - startFiltered;
	cv::Vec6f sum        = cv::Vec6f(0, 0, 0, 0, 0, 0);

	//Then average the remaining points
	for(size_t i = startFiltered; i < endfiltered; ++i)
	{
		sum += reprojectionErrors[i];
	}

	cv::Vec2f average;
	average[0] = sum[2] / filtered;
	average[1] = sum[3] / filtered;

	//Now calculate rotation angle
	angular_x = std::atan(average[0]);
	angular_y = std::atan(average[1]);

	/*double anglex = ((double)average[0] / fx) + (IMAGESIZEX / 2) / fx;
	angularx = std::asin(anglex) - std::asin((IMAGESIZEX / 2) / fx);

	double angley = ((double)average[1] /fy) + (IMAGESIZEY / 2) / fy;
	angulary = std::asin(angley) - std::asin((IMAGESIZEY/ 2) / fy);
	*/
	return true;
}

void ReconstructorLaserDots3D::GetProjectionError(DotInfos& errors,
                                                  const int channel)
{
	errors.clear();

	if(triangulated_points_.size() == 0)
		return;

	// get the selected camera/projector
	Dot2DData* cam = nullptr;
	switch(channel)
	{
	case 0: cam = &camera_sensor0;
		break;
	case 1: cam = &camera_sensor1;
		break;
	case 2: cam = &projector;
		break;
	default: cam = &camera_sensor0;
	}

	// Get camera/projector calibration and project detected 3d points into image
	std::vector<cv::Point2f> proj;
	cam->calibration->ProjectPoints(triangulated_points_, proj);

	std::vector<cv::Point2f> undistorted;
	cam->calibration->UndistortPoints2(proj, undistorted, triangulated_points_);

	// fill measurement/projection vector
	double sqNormSum = 0.0;

	for(size_t i = 0; i < triangulated_points_.size(); ++i)
	{
		TripleCorrespondence triple = GetUsedCorrespondence(int(i));

		cv::Point2f measurement(cam->original_coordinates[triple.camera_index[channel]].x,
		                        cam->original_coordinates[triple.camera_index[channel]].y);

		cv::Point2f measurementundistorted(cam->undistorted_coordinates[triple.camera_index[channel]].x,
		                                   cam->undistorted_coordinates[triple.camera_index[channel]].y);

		cv::Point2f diffUndistorted = measurementundistorted - undistorted[i];
		cv::Point2f diff            = measurement - proj[i];

		cv::Vec6f v;
		v[0] = diff.x; // no measurement but beam coordinate at projector
		v[1] = diff.y; // no measurement but beam coordinate at projector
		v[2] = diffUndistorted.x;
		v[3] = diffUndistorted.y;
		v[4] = 0;
		v[5] = 0;
		errors.push_back(v);
	}
}

int ReconstructorLaserDots3D::GetNumBeams() const
{
	return int(projector.DotsCount());
}

static cv::Vec2d radTanComp(cv::Vec2d posXY,
                            cv::Vec2d errXY)
{
	cv::Vec2d radTanComp;
	// TODO
	/*cv::Vec2d tanDir(posXY.y, -posXY.x);

	double ang1   = errXY.angleTo(posXY);
	radTanComp[0] = cos(ang1) * errXY.length();

	double ang2   = errXY.angleTo(tanDir);
	radTanComp[1] = cos(ang2) * errXY.length();*/
	return radTanComp;
}

void ReconstructorLaserDots3D::CalculateWeights(std::vector<cv::Point3f> points_3d,
                                                std::vector<double>& weights)
{
	std::vector<double> distortionCam0, distortionCam1;
	CalculateWeightsSingleCamera(points_3d, distortionCam0, camera_sensor0.calibration);
	CalculateWeightsSingleCamera(points_3d, distortionCam1, camera_sensor1.calibration);

	for(size_t i = 0; i < weights.size(); ++i)
	{
		weights[i] = 1 / sqrt((distortionCam0[i] * distortionCam0[i] + distortionCam1[i] * distortionCam1[i]) / 2) + 0.8;
	}
}

void ReconstructorLaserDots3D::CalculateWeightsSingleCamera(std::vector<cv::Point3f> points_3d,
                                                            std::vector<double>& distortion_vector,
                                                            std::shared_ptr<const CameraSensor> calibration) const
{
	double fx, fy, cx, cy;
	cv::Mat matA, distortion;
	calibration->GetBestFitCVModel(matA, distortion);
	fx = matA.at<double>(0, 0);
	fy = matA.at<double>(1, 1);
	cx = matA.at<double>(0, 2);
	cy = matA.at<double>(1, 2);

	cv::Mat r, T;
	calibration->GetExtrinsicParameters(r, T);

	Dots projectedUndistortedPoints, projectDistortedPoints;
	calibration->ProjectPoints(points_3d, projectedUndistortedPoints);
	ProjectWithoutDistortion(points_3d, projectDistortedPoints, fx, fy, cx, cy, r, T);

	distortion_vector.reserve(points_3d.size());
	for(size_t i = 0; i < points_3d.size(); ++i)
	{
		cv::Vec2f diff = projectDistortedPoints[i] - projectedUndistortedPoints[i];
		distortion_vector.push_back(sqrt(diff[0] * diff[0] + diff[1] * diff[1]));
	}
}

void ReconstructorLaserDots3D::ProjectWithoutDistortion(std::vector<cv::Point3f>& points_3d,
                                                        Dots& points_2d,
                                                        double fx,
                                                        double fy,
                                                        double cx,
                                                        double cy,
                                                        cv::Mat& R,
                                                        cv::Mat& t)
{
	Eigen::Matrix3d REigen;
	for(int row = 0; row < 3; ++row)
	{
		for(int col = 0; col < 3; ++col)
		{
			REigen(row, col) = R.at<double>(row, col);
		}
	}
	Eigen::Vector3d tEigen(t.at<double>(0, 0), t.at<double>(1, 0), t.at<double>(2, 0));

	points_2d.reserve(points_3d.size());
	for(size_t i = 0; i < points_3d.size(); ++i)
	{
		Eigen::Vector3d pSrc;
		pSrc[0] = points_3d[i].x;
		pSrc[1] = points_3d[i].y;
		pSrc[2] = points_3d[i].z;

		Eigen::Vector3d pCam = REigen * pSrc + tEigen;

		double x = (double)pCam.x() / pCam.z();
		double y = (double)pCam.y() / pCam.z();
		double u = x * fx + cx;
		double v = y * fy + cy;
		points_2d.push_back(cv::Vec2f((float)u, (float)v));
	}
}

double ReconstructorLaserDots3D::CompensatePatternErrors()
{
	return CompensatePatternErrorsImpl02();
}

double ReconstructorLaserDots3D::CompensatePatternErrorsOnlyScale()
{
	//std::string wsetup = "WaveLength Reprojection Error Calculation";
	//TimeRecorder::recordStart(wsetup, true);
	// get the 2d projections and projection errors of projector
	std::vector<cv::Vec4f> errors;
	GetProjectionError(errors, 2);

	// calculate the center of gravity of all detected points in 2d projector coordinates
	cv::Vec2f centerOfGravity(0, 0);
	for(unsigned int i = 0; i < errors.size(); ++i)
	{
		centerOfGravity[0] += errors[i][0];
		centerOfGravity[1] += errors[i][1];
	}

	// collect all indices with the distance of the projector coordinate to the center of gravity
	// The idea is to get rid of isolated points far away from the areas of correct point detection
	std::vector<std::pair<int, double>> idxSort;
	//double sqSumRad = 0.0;
	//double sqSumTan = 0.0;

	for(unsigned int i = 0; i < errors.size(); ++i)
	{
		cv::Vec2d err(errors[i][0] - errors[i][2], errors[i][1] - errors[i][3]);
		cv::Vec2d radTan = radTanComp(cv::Vec2d(errors[i][0], errors[i][1]), err);

		//sqSumRad += radTan[0] * radTan[0];
		//sqSumTan += radTan[1] * radTan[1];

		//cv::Vec2f vecToCog(errors[i][0] - centerOfGravity[0], errors[i][1] - centerOfGravity[1]);
		//double sqDistToCog = vecToCog[0] * vecToCog[0] + vecToCog[1] * vecToCog[1];

		idxSort.push_back(std::make_pair(i, radTan[1]));
	}

	//double rmsRad = sqrt(sqSumRad / (double)errors.size());
	//double rmsTan = sqrt(sqSumTan / (double)errors.size());

	// sort the point indices by length of radius
	std::sort(idxSort.begin(),
	          idxSort.end(),
	          [](const std::pair<int, double>& a,
	             const std::pair<int, double>& b)-> bool
	          {
		          return a.second < b.second;
	          });

	std::vector<int> used;
	float maxSqDist = 300.0f * 300.0f;

	for(size_t i = 0; i < 0.8 * idxSort.size(); ++i)
	{
		int ptIdx = idxSort[i].first;
		/*
		if (errors[ptIdx][0] * errors[ptIdx][0] + errors[ptIdx][1] * errors[ptIdx][1] > maxSqDist)
		continue;
		*/
		used.push_back(ptIdx);
		//cv::Vec2f err(errors[ptIdx][0] - errors[ptIdx][2], errors[ptIdx][1] - errors[ptIdx][3]);
		//sqSum += err[0] * err[0] + err[1] * err[1];
	}

	int minPts = 250;

	if(bright_only_)
		minPts /= 10;

	if((int)used.size() < minPts)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cannot compensate with " << used.size() << " points";
		return 0.0;
	}

	/*TimeRecorder::recordStop(wsetup, true);
	std::string resetPatternstring = "Reset Pattern";
	TimeRecorder::recordStart(resetPatternstring, true);*/
	/*
	// corrected 3d target positions for scale correction with Eigen umeyama
	Eigen::Matrix< double, 2, Eigen::Dynamic > projectedMeasurements;
	Eigen::Matrix< double, 2, Eigen::Dynamic > calibrationValues;

	projectedMeasurements.resize(2, used.size() );
	calibrationValues.resize(2, used.size() );

	//now revert back to zero pattern compensation
	const auto coordsBefore = m_pro0.getOrigCoords();

	resetToLastProjectorState();
	*/
	//now get corresponding points with reverted wavelength compensation
	//getProjectionError(errors, 2);
	//const auto coordsBefore = m_pro0.getOrigCoords();
	//	//now revert back to zero pattern compensation
	const Dots coordsBefore(projector.GetDots());
	std::vector<size_t> usedCorrespondences(used.size());

	std::vector<Eigen::Vector2d> origCoords(used.size());
	std::vector<Eigen::Vector2d> currCoords(used.size());

	for(unsigned int i = 0; i < used.size(); ++i)
	{
		/*
		calibrationValues( 0, i ) = errors[used[i]][0];
		calibrationValues( 1, i ) = errors[used[i]][1];

		projectedMeasurements( 0, i ) = errors[used[i]][2];
		projectedMeasurements( 1, i) = errors[used[i]][3];
		*/
		origCoords[i] = Eigen::Vector2d(errors[used[i]][0], errors[used[i]][1]);
		currCoords[i] = Eigen::Vector2d(errors[used[i]][2], errors[used[i]][3]);
		usedCorrespondences.push_back(GetUsedCorrespondence(used[i]).camera_index[2]);
	}
	//TimeRecorder::recordStop(resetPatternstring, true);
	// get optimal trafo between calibration model point clouds including scale correction (true flag)
	// this serves also as start values for nonlinear fit
	//std::string umeyama = "Umeyama ";
	//TimeRecorder::recordStart(umeyama, true);
	{
		int minPts = 300;
		if(bright_only_)
			minPts /= 10;

		if((int)errors.size() < minPts)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cannot compensate with" << errors.size() << " points";
			return 0.0;
		}
		/*
		//these are the known projector pts
		std::vector<Eigen::Vector2d> origCoords(used.size());
		std::transform(used.begin(), used.end(), origCoords.begin(), [errors] (const int& pt) -> Eigen::Vector2d
		{
		Eigen::Vector2d out;
		out[0] = errors[pt][0];
		out[1] = errors[pt][1];
		return out;
		});

		//these are the triangulated projector pts
		std::vector<Eigen::Vector2d> currCoords(used.size());
		std::transform(used.begin(), used.end(), currCoords.begin(), [errors] (const int& pt) -> Eigen::Vector2d
		{
		Eigen::Vector2d out;
		out[0] = errors[pt][2];
		out[1] = errors[pt][3];
		return out;
		});
		*/
		Eigen::Vector2d origCentralBeam;
		origCentralBeam[0] = projector.original_coordinates[0].x;
		origCentralBeam[1] = projector.original_coordinates[0].y;
		/*TimeRecorder::recordStop(umeyama, true);
		std::string fitstring = "Actual Wavelength fit";
		TimeRecorder::recordStart(fitstring, true);*/
		WavelengthCompensationFit fit(origCoords, currCoords, origCentralBeam);
		double numDiffDelta = 1e-5;
		Eigen::NumericalDiff<WavelengthCompensationFit, Eigen::Central> numDiff(fit, numDiffDelta);
		Eigen::LevenbergMarquardt<Eigen::NumericalDiff<WavelengthCompensationFit, Eigen::Central>, double> lm(numDiff);

		WavelengthCompensationFit::WavelengthCompInfo endInfo;
		if(!use_stored_wavelength_comp_for_next_triangulation_)
		{
			//use start values from umeyama
			WavelengthCompensationFit::WavelengthCompInfo startInfo = accumulated_wavelength_compensation_;
			/*startInfo.translation[0] = trafo(0,2);
			startInfo.translation[1] = trafo(1,2);
			startInfo.scale = trafo.col(0).norm();
			startInfo.rotation = acos(trafo(0,0)/startInfo.scale);*/

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : umeyama init  scale" << startInfo.scale << " rot " << startInfo.rotation <<
 " trans" << startInfo.translation[0] << startInfo.translation[1] << " with " << used.size() << " corr";

			if(wavelength_clevmar_)
			{
				Eigen::VectorXd start = WavelengthCompensationFit::ToEigenVector(startInfo);

				// number of function evaluations

				int maxFev           = 500;
				lm.parameters.maxfev = maxFev;

				// tolerance for the norm of the solution vector
				double xtolerance  = 1e-5;
				lm.parameters.xtol = xtolerance;

				Eigen::LevenbergMarquardtSpace::Status status = lm.minimize(start);

				endInfo = WavelengthCompensationFit::ToCompInfo(start);
			}
			else
			{
				endInfo = startInfo;
			}
		}
		else
		{
			endInfo = next_wavelength_compensation_;
		}

		//TimeRecorder::recordStop(fitstring, true);
		// updates the projector beams to the current state and stores state to member m_accumulatedWavelengthComp
		if(endInfo.scale > 1.05 || endInfo.scale < 0.95)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Suspicious projector points scale of  " << endInfo.scale << " not set.";
		}
		else
		{
			if(projector.original_coordinates.empty())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No projector points";
			}
			else
			{
				UpdateProjectorPtsToPatternCompensationStateWithoutReset(endInfo);
			}
		}
		//now calculate the remaining error after applying the new pattern trafo and before applying the new trafo
		//this indicates whether a second run may be necessary.
		double meanPtShift = 0.;
		for(const auto& corresp : usedCorrespondences)
		{
			meanPtShift += cv::norm(coordsBefore[corresp] - projector.original_coordinates[corresp]);
		}
		meanPtShift /= (double)usedCorrespondences.size();
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation: scale : " << endInfo.scale;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation auto calibration mean pt shift : " << meanPtShift << " , scale acc "
 <<
 accumulated_wavelength_compensation_.scale
			<< " now " << endInfo.scale;
		return meanPtShift;
	}
}

double ReconstructorLaserDots3D::CompensatePatternErrorsOnlyScaleWeighted()
{
	//std::string wsetup = "WaveLength Reprojection Error Calculation";
	//TimeRecorder::recordStart(wsetup, true);
	// get the 2d projections and projection errors of projector
	std::vector<cv::Vec4f> errors;
	GetProjectionError(errors, 2);

	// calculate the center of gravity of all detected points in 2d projector coordinates
	cv::Vec2f centerOfGravity(0, 0);
	for(unsigned int i = 0; i < errors.size(); ++i)
	{
		centerOfGravity[0] += errors[i][0];
		centerOfGravity[1] += errors[i][1];
	}


	// collect all indices with the distance of the projector coordinate to the center of gravity
	// The idea is to get rid of isolated points far away from the areas of correct point detection
	std::vector<std::pair<int, double>> idxSort;
	//double sqSumRad = 0.0;
	//double sqSumTan = 0.0;

	for(unsigned int i = 0; i < errors.size(); ++i)
	{
		cv::Vec2d err(errors[i][0] - errors[i][2], errors[i][1] - errors[i][3]);
		cv::Vec2d radTan = radTanComp(cv::Vec2d(errors[i][0], errors[i][1]), err);

		//sqSumRad += radTan[0] * radTan[0];
		//sqSumTan += radTan[1] * radTan[1];

		//cv::Vec2f vecToCog(errors[i][0] - centerOfGravity[0], errors[i][1] - centerOfGravity[1]);
		//double sqDistToCog = vecToCog[0] * vecToCog[0] + vecToCog[1] * vecToCog[1];

		idxSort.push_back(std::make_pair(i, radTan[1]));
	}

	//double rmsRad = sqrt(sqSumRad / (double)errors.size());
	//double rmsTan = sqrt(sqSumTan / (double)errors.size());

	// sort the point indices by length of radius
	std::sort(idxSort.begin(),
	          idxSort.end(),
	          [](const std::pair<int, double>& a,
	             const std::pair<int, double>& b)-> bool
	          {
		          return a.second < b.second;
	          });

	std::vector<int> used;


	/*
	for (size_t i = 0; i < errors.size(); ++i)
	{
	if (cv::norm( cv::Vec2f(errors[i][0], errors[i][1]) )  < 200)
	{
	used.push_back(i);
	}
	}
	*/


	//double sqSum = 0.0;


	float maxSqDist = 300.0f * 300.0f;
	for(size_t i = 0; i < 0.8 * idxSort.size(); ++i)
	{
		int ptIdx = idxSort[i].first;
		/*
		if (errors[ptIdx][0] * errors[ptIdx][0] + errors[ptIdx][1] * errors[ptIdx][1] > maxSqDist)
		continue;
		*/
		used.push_back(ptIdx);
		//cv::Vec2f err(errors[ptIdx][0] - errors[ptIdx][2], errors[ptIdx][1] - errors[ptIdx][3]);
		//sqSum += err[0] * err[0] + err[1] * err[1];
	}

	int minPts = 250;
	if(bright_only_)
		minPts /= 10;

	if((int)used.size() < minPts)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cannot compensate with" << errors.size() << " points";
		return 0.0;
	}

	/*TimeRecorder::recordStop(wsetup, true);
	std::string resetPatternstring = "Reset Pattern";
	TimeRecorder::recordStart(resetPatternstring, true);*/
	/*
	// corrected 3d target positions for scale correction with Eigen umeyama
	Eigen::Matrix< double, 2, Eigen::Dynamic > projectedMeasurements;
	Eigen::Matrix< double, 2, Eigen::Dynamic > calibrationValues;

	projectedMeasurements.resize(2, used.size() );
	calibrationValues.resize(2, used.size() );

	//now revert back to zero pattern compensation
	const auto coordsBefore = m_pro0.getOrigCoords();

	resetToLastProjectorState();
	*/
	//now get corresponding points with reverted wavelength compensation
	//getProjectionError(errors, 2);
	const Dots coordsBefore(projector.GetDots());
	std::vector<size_t> usedCorrespondences(used.size());

	std::vector<Eigen::Vector2d> origCoords(used.size());
	std::vector<Eigen::Vector2d> currCoords(used.size());

	for(unsigned int i = 0; i < used.size(); ++i)
	{
		/*
		calibrationValues( 0, i ) = errors[used[i]][0];
		calibrationValues( 1, i ) = errors[used[i]][1];

		projectedMeasurements( 0, i ) = errors[used[i]][2];
		projectedMeasurements( 1, i) = errors[used[i]][3];
		*/
		origCoords[i] = Eigen::Vector2d(errors[used[i]][0], errors[used[i]][1]);
		currCoords[i] = Eigen::Vector2d(errors[used[i]][2], errors[used[i]][3]);
		usedCorrespondences.push_back(GetUsedCorrespondence(used[i]).camera_index[2]);
	}
	//TimeRecorder::recordStop(resetPatternstring, true);
	// get optimal trafo between calibration model point clouds including scale correction (true flag)
	// this serves also as start values for nonlinear fit
	//std::string umeyama = "Umeyama ";
	//TimeRecorder::recordStart(umeyama, true);
	{
		int minPts = 300;
		if(bright_only_)
			minPts /= 10;

		if((int)errors.size() < minPts)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cannot compensate with" << errors.size() << " points";
			return 0.0;
		}
		/*
		//these are the known projector pts
		std::vector<Eigen::Vector2d> origCoords(used.size());
		std::transform(used.begin(), used.end(), origCoords.begin(), [errors] (const int& pt) -> Eigen::Vector2d
		{
		Eigen::Vector2d out;
		out[0] = errors[pt][0];
		out[1] = errors[pt][1];
		return out;
		});

		//these are the triangulated projector pts
		std::vector<Eigen::Vector2d> currCoords(used.size());
		std::transform(used.begin(), used.end(), currCoords.begin(), [errors] (const int& pt) -> Eigen::Vector2d
		{
		Eigen::Vector2d out;
		out[0] = errors[pt][2];
		out[1] = errors[pt][3];
		return out;
		});
		*/
		Eigen::Vector2d origCentralBeam;
		origCentralBeam[0] = projector.original_coordinates[0].x;
		origCentralBeam[1] = projector.original_coordinates[0].y;
		/*TimeRecorder::recordStop(umeyama, true);
		std::string fitstring = "Actual Wavelength fit";
		TimeRecorder::recordStart(fitstring, true);*/

		std::vector<double> weights(currCoords.size(), 1.0);
		//Weigh points in the center of the image more than on the outside since these might be innaccurate
		//Weight is defined by the camera model
		CalculateWeights(triangulated_points_, weights);

		//TODO
		WavelengthCompensationFitScaleOnlyWeighted fit(origCoords, currCoords, origCentralBeam, weights);
		double numDiffDelta = 1e-5;
		Eigen::NumericalDiff<WavelengthCompensationFitScaleOnlyWeighted, Eigen::Central> numDiff(fit, numDiffDelta);
		Eigen::LevenbergMarquardt<Eigen::NumericalDiff<WavelengthCompensationFitScaleOnlyWeighted, Eigen::Central>, double> lm(numDiff);

		WavelengthCompensationFit::WavelengthCompInfo endInfo;
		if(!use_stored_wavelength_comp_for_next_triangulation_)
		{
			//use start values from umeyama
			WavelengthCompensationFit::WavelengthCompInfo startInfo = accumulated_wavelength_compensation_;
			/*startInfo.translation[0] = trafo(0,2);
			startInfo.translation[1] = trafo(1,2);
			startInfo.scale = trafo.col(0).norm();
			startInfo.rotation = acos(trafo(0,0)/startInfo.scale);*/

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation umeyama init  scale" << startInfo.scale
			<< " rot " << startInfo.rotation << " trans" << startInfo.translation[0]
			<< startInfo.translation[1] << " with " << used.size() << " corr";

			if(wavelength_clevmar_)
			{
				Eigen::VectorXd start = WavelengthCompensationFitScaleOnly::ToEigenVector(startInfo);

				// number of function evaluations
				int maxFev           = 500;
				lm.parameters.maxfev = maxFev;

				// tolerance for the norm of the solution vector
				double xtolerance                             = 1e-5;
				lm.parameters.xtol                            = xtolerance;
				Eigen::LevenbergMarquardtSpace::Status status = lm.minimize(start);
				endInfo                                       = WavelengthCompensationFitScaleOnly::ToCompInfo(start);
			}
			else
			{
				endInfo = startInfo;
			}
		}
		else
		{
			endInfo = next_wavelength_compensation_;
		}

		//TimeRecorder::recordStop(fitstring, true);
		// updates the projector beams to the current state and stores state to member m_accumulatedWavelengthComp
		if(endInfo.scale > 1.05 || endInfo.scale < 0.95)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Suspicious projector points scale of" << endInfo.scale << " not set.";
		}
		else
		{
			if(projector.original_coordinates.empty())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No projector points";
			}
			else
			{
				UpdateProjectorPtsToPatternCompensationStateWithoutReset(endInfo);
			}
		}
		//now calculate the remaining error after applying the new pattern trafo and before applying the new trafo
		//this indicates whether a second run may be necessary.
		double meanPtShift = 0.;
		for(const auto& corresp : usedCorrespondences)
		{
			meanPtShift += cv::norm(coordsBefore[corresp] - projector.original_coordinates[corresp]);
		}
		meanPtShift /= (double)usedCorrespondences.size();
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation: scale : " << endInfo.scale;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation auto calibration mean pt shift : " << meanPtShift << " , scale acc "
 <<
 accumulated_wavelength_compensation_.scale
			<< " now " << endInfo.scale;
		return meanPtShift;
	}
}

double ReconstructorLaserDots3D::CompensatePatternErrorsImpl01()
{
	// get the 2d projections and projection errors of projector
	std::vector<cv::Vec4f> errors;
	GetProjectionError(errors, 2);

	// calculate the center of gravity of all detected points in 2d projector coordinates
	cv::Vec2f centerOfGravity(0, 0);
	for(unsigned int i = 0; i < errors.size(); ++i)
	{
		centerOfGravity[0] += errors[i][0];
		centerOfGravity[1] += errors[i][1];
	}

	// collect all indices with the distance of the projector coordinate to the center of gravity
	// The idea is to get rid of isolated points far away from the areas of correct point detection
	std::vector<std::pair<int, double>> idxSort;
	double sqSumRad = 0.0;
	double sqSumTan = 0.0;

	for(unsigned int i = 0; i < errors.size(); ++i)
	{
		cv::Vec2d err(errors[i][0] - errors[i][2], errors[i][1] - errors[i][3]);
		cv::Vec2d radTan = radTanComp(cv::Vec2d(errors[i][0], errors[i][1]), err);

		sqSumRad += radTan[0] * radTan[0];
		sqSumTan += radTan[1] * radTan[1];

		cv::Vec2f vecToCog(errors[i][0] - centerOfGravity[0], errors[i][1] - centerOfGravity[1]);
		double sqDistToCog = vecToCog[0] * vecToCog[0] + vecToCog[1] * vecToCog[1];

		idxSort.emplace_back(i, radTan[1]);
	}

	double rmsRad = sqrt(sqSumRad / (double)errors.size());
	double rmsTan = sqrt(sqSumTan / (double)errors.size());

	// sort the point indices by length of radius
	std::sort(idxSort.begin(),
	          idxSort.end(),
	          [](const std::pair<int, double>& a,
	             const std::pair<int, double>& b)-> bool
	          {
		          return a.second < b.second;
	          });

	std::vector<int> used;

	/*
	for (size_t i = 0; i < errors.size(); ++i)
	{
		if (cv::norm( cv::Vec2f(errors[i][0], errors[i][1]) )  < 200)
		{
			used.push_back(i);
		}
	}
	*/

	double sqSum = 0.0;

	float maxSqDist = 300.0f * 300.0f;
	for(size_t i = 0; i < 0.8 * idxSort.size(); ++i)
	{
		int ptIdx = idxSort[i].first;

		// 		if (errors[ptIdx][0] * errors[ptIdx][0] + errors[ptIdx][1] * errors[ptIdx][1] > maxSqDist )
		// 			continue;

		used.push_back(ptIdx);
		cv::Vec2f err(errors[ptIdx][0] - errors[ptIdx][2], errors[ptIdx][1] - errors[ptIdx][3]);
		sqSum += err[0] * err[0] + err[1] * err[1];
	}

	int minPts = compensation_min_pts_;
	if(bright_only_)
		minPts /= 10;

	if((int)used.size() < minPts)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cannot compensate with " << used.size() << " points";
		return 0.0;
	}

	// corrected 3d target positions for scale correction with Eigen umeyama
	Eigen::Matrix<double, 2, Eigen::Dynamic> projectedMeasurements;
	Eigen::Matrix<double, 2, Eigen::Dynamic> calibrationValues;

	projectedMeasurements.resize(2, used.size());
	calibrationValues.resize(2, used.size());

	//now revert back to zero pattern compensation
	const Dots coordsBefore(projector.GetDots());
	ResetPattern();

	//now get corresponding points with reverted wavelength compensation
	GetProjectionError(errors, 2);

	std::vector<size_t> usedCorrespondences;

	for(unsigned int i = 0; i < used.size(); ++i)
	{
		calibrationValues(0, i) = errors[used[i]][0];
		calibrationValues(1, i) = errors[used[i]][1];

		projectedMeasurements(0, i) = errors[used[i]][2];
		projectedMeasurements(1, i) = errors[used[i]][3];

		usedCorrespondences.push_back(GetUsedCorrespondence(used[i]).camera_index[2]);
	}

	// get optimal trafo between calibration model point clouds including scale correction (true flag)
	// this serves also as start values for nonlinear fit
	const Eigen::Matrix<double, -1, -1> trafo = Eigen::umeyama(calibrationValues, projectedMeasurements, true);
	{
		int minPts = compensation_min_pts_;
		if(bright_only_)
			minPts /= 10;

		if((int)errors.size() < minPts)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cannot compensate with" << errors.size() << " points";
			return 0.0;
		}

		//these are the known projector pts
		std::vector<Eigen::Vector2d> origCoords(used.size());
		std::transform(used.begin(),
		               used.end(),
		               origCoords.begin(),
		               [errors](const int& pt) -> Eigen::Vector2d
		               {
			               Eigen::Vector2d out;
			               out[0] = errors[pt][0];
			               out[1] = errors[pt][1];
			               return out;
		               });

		//these are the triangulated projector pts
		std::vector<Eigen::Vector2d> currCoords(used.size());
		std::transform(used.begin(),
		               used.end(),
		               currCoords.begin(),
		               [errors](const int& pt) -> Eigen::Vector2d
		               {
			               Eigen::Vector2d out;
			               out[0] = errors[pt][2];
			               out[1] = errors[pt][3];
			               return out;
		               });

		Eigen::Vector2d origCentralBeam;
		origCentralBeam[0] = projector.GetDot(0).x;
		origCentralBeam[1] = projector.GetDot(0).y;

		WavelengthCompensationFit fit(origCoords, currCoords, origCentralBeam);
		Eigen::NumericalDiff<WavelengthCompensationFit, Eigen::Central> numDiff(fit, wavelength_cnum_diff_delta_);
		Eigen::LevenbergMarquardt<Eigen::NumericalDiff<WavelengthCompensationFit, Eigen::Central>, double> lm(numDiff);

		WavelengthCompensationFit::WavelengthCompInfo endInfo;
		if(!use_stored_wavelength_comp_for_next_triangulation_)
		{
			//use start values from umeyama
			WavelengthCompensationFit::WavelengthCompInfo startInfo;
			startInfo.translation[0] = trafo(0, 2);
			startInfo.translation[1] = trafo(1, 2);
			startInfo.scale          = trafo.col(0).norm();
			startInfo.rotation       = acos(trafo(0, 0) / startInfo.scale);

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation umeyama init  scale" << startInfo.scale << " rot " << startInfo.
rotation <<
 " trans" << startInfo.
translation[0] << startInfo.translation[1] << " with " << used.size() << " corr";

			if(wavelength_clevmar_)
			{
				Eigen::VectorXd start = WavelengthCompensationFit::ToEigenVector(startInfo);

				// number of function evaluations

				lm.parameters.maxfev = wavelength_cmax_fev_;

				// tolerance for the norm of the solution vector
				lm.parameters.xtol = wavelength_cx_tolerance_;

				Eigen::LevenbergMarquardtSpace::Status status = lm.minimize(start);

				endInfo = WavelengthCompensationFit::ToCompInfo(start);
			}
			else
			{
				endInfo = startInfo;
			}
		}
		else
		{
			endInfo = next_wavelength_compensation_;
		}


		// updates the projector beams to the current state and stores state to member m_accumulatedWavelengthComp
		UpdateProjectorPtsToPatternCompensationState(endInfo);

		//now calculate the remaining error after applying the new pattern trafo and before applying the new trafo
		//this indicates whether a second run may be necessary.
		double meanPtShift = 0.;
		for(const auto& corresp : usedCorrespondences)
		{
			meanPtShift += cv::norm(coordsBefore[corresp] - projector.GetDot(corresp));
		}
		meanPtShift /= (double)usedCorrespondences.size();
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation: scale : " << endInfo.scale;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation auto calibration mean pt shift : " << meanPtShift << " , scale acc "
 <<
 accumulated_wavelength_compensation_.scale
 << " now " << endInfo.scale;
		return meanPtShift;
	}
}

double ReconstructorLaserDots3D::CompensatePatternErrorsImpl02()
{
	//std::string wsetup = "WaveLength Reprojection Error Calculation";
	//TimeRecorder::recordStart(wsetup, true);
	// get the 2d projections and projection errors of projector
	std::vector<cv::Vec4f> errors;
	GetProjectionError(errors, 2);

	// collection of all point indices that are used for compensation
	std::vector<int> used;


	float maxSqDist = 300.0f * 300.0f;
	for(size_t ptIdx = 0; ptIdx < errors.size(); ++ptIdx)
	{
		if(errors[ptIdx][0] * errors[ptIdx][0] + errors[ptIdx][1] * errors[ptIdx][1] > maxSqDist)
			continue;

		used.push_back((int)ptIdx);
	}

	int minPts = compensation_min_pts_;
	if(bright_only_)
		minPts /= 10;

	if((int)used.size() < minPts)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cannot compensate with " << used.size() << " points";
		return 0.0;
	}

	/*TimeRecorder::recordStop(wsetup, true);
	std::string resetPatternstring = "Reset Pattern";
	TimeRecorder::recordStart(resetPatternstring, true);*/
	/*
	// corrected 3d target positions for scale correction with Eigen umeyama
	Eigen::Matrix< double, 2, Eigen::Dynamic > projectedMeasurements;
	Eigen::Matrix< double, 2, Eigen::Dynamic > calibrationValues;

	projectedMeasurements.resize(2, used.size() );
	calibrationValues.resize(2, used.size() );

	//now revert back to zero pattern compensation
	const auto coordsBefore = m_pro0.getDots();

	resetToLastProjectorState();
	*/
	//now get corresponding points with reverted wavelength compensation
	//getProjectionError(errors, 2);
	const Dots coordsBefore(projector.GetDots());
	std::vector<size_t> usedCorrespondences(used.size());

	std::vector<Eigen::Vector2d> origCoords(used.size());
	std::vector<Eigen::Vector2d> currCoords(used.size());

	for(unsigned int i = 0; i < used.size(); ++i)
	{
		/*
		calibrationValues( 0, i ) = errors[used[i]][0];
		calibrationValues( 1, i ) = errors[used[i]][1];

		projectedMeasurements( 0, i ) = errors[used[i]][2];
		projectedMeasurements( 1, i) = errors[used[i]][3];
		*/
		origCoords[i] = Eigen::Vector2d(errors[used[i]][0], errors[used[i]][1]);
		currCoords[i] = Eigen::Vector2d(errors[used[i]][2], errors[used[i]][3]);
		usedCorrespondences.push_back(GetUsedCorrespondence(used[i]).camera_index[2]);
	}
	//TimeRecorder::recordStop(resetPatternstring, true);
	// get optimal trafo between calibration model point clouds including scale correction (true flag)
	// this serves also as start values for nonlinear fit
	//std::string umeyama = "Umeyama ";
	//TimeRecorder::recordStart(umeyama, true);
	{
		int minPts = compensation_min_pts_;
		if(bright_only_)
			minPts /= 10;

		if((int)errors.size() < minPts)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : cannot compensate with " << errors.size() << " points";
			return 0.0;
		}
		/*
		//these are the known projector pts
		std::vector<Eigen::Vector2d> origCoords(used.size());
		std::transform(used.begin(), used.end(), origCoords.begin(), [errors] (const int& pt) -> Eigen::Vector2d
		{
		Eigen::Vector2d out;
		out[0] = errors[pt][0];
		out[1] = errors[pt][1];
		return out;
		});

		//these are the triangulated projector pts
		std::vector<Eigen::Vector2d> currCoords(used.size());
		std::transform(used.begin(), used.end(), currCoords.begin(), [errors] (const int& pt) -> Eigen::Vector2d
		{
		Eigen::Vector2d out;
		out[0] = errors[pt][2];
		out[1] = errors[pt][3];
		return out;
		});
		*/
		Eigen::Vector2d origCentralBeam;
		origCentralBeam[0] = projector.GetDot(0).x;
		origCentralBeam[1] = projector.GetDot(0).y;
		/*TimeRecorder::recordStop(umeyama, true);
		std::string fitstring = "Actual Wavelength fit";
		TimeRecorder::recordStart(fitstring, true);*/
		WavelengthCompensationFit fit(origCoords, currCoords, origCentralBeam);
		Eigen::NumericalDiff<WavelengthCompensationFit, Eigen::Central> numDiff(fit, wavelength_cnum_diff_delta_);
		Eigen::LevenbergMarquardt<Eigen::NumericalDiff<WavelengthCompensationFit, Eigen::Central>, double> lm(numDiff);

		WavelengthCompensationFit::WavelengthCompInfo endInfo;
		if(!use_stored_wavelength_comp_for_next_triangulation_)
		{
			//use start values from umeyama
			WavelengthCompensationFit::WavelengthCompInfo startInfo = accumulated_wavelength_compensation_;
			/*startInfo.translation[0] = trafo(0,2);
			startInfo.translation[1] = trafo(1,2);
			startInfo.scale = trafo.col(0).norm();
			startInfo.rotation = acos(trafo(0,0)/startInfo.scale);*/

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation umeyama init  scale" << startInfo.scale << " rot " << startInfo.
rotation <<
 " trans" << startInfo.
				translation[0] << startInfo.translation[1] << " with " << used.size() << " corr";


			if(wavelength_clevmar_)
			{
				Eigen::VectorXd start = WavelengthCompensationFit::ToEigenVector(startInfo);

				// number of function evaluations

				lm.parameters.maxfev = wavelength_cmax_fev_;

				// tolerance for the norm of the solution vector
				lm.parameters.xtol = wavelength_cx_tolerance_;

				Eigen::LevenbergMarquardtSpace::Status status = lm.minimize(start);

				endInfo = WavelengthCompensationFit::ToCompInfo(start);
			}
			else
			{
				endInfo = startInfo;
			}
		}
		else
		{
			endInfo = next_wavelength_compensation_;
		}

		//TimeRecorder::recordStop(fitstring, true);
		// updates the projector beams to the current state and stores state to member m_accumulatedWavelengthComp
		if(endInfo.scale > 1.05 || endInfo.scale < 0.95)
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Suspicious projector points scale of " << endInfo.scale << " not set.";
		}
		else
		{
			if(projector.IfDotsEmpty())
			{
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No projector points";
			}
			else
			{
				UpdateProjectorPtsToPatternCompensationStateWithoutReset(endInfo);
			}
		}
		//now calculate the remaining error after applying the new pattern trafo and before applying the new trafo
		//this indicates whether a second run may be necessary.
		double meanPtShift = 0.;
		for(const auto& corresp : usedCorrespondences)
		{
			meanPtShift += cv::norm(coordsBefore[corresp] - projector.GetDot(corresp));
		}
		meanPtShift /= (double)usedCorrespondences.size();
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation: scale: " << endInfo.scale;
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation auto calibration mean pt shift: " << meanPtShift << ", scale acc " <<
 accumulated_wavelength_compensation_.scale <<
 " now " << endInfo.scale;
		return meanPtShift;
	}
}

void ReconstructorLaserDots3D::ResetToLastProjectorState()
{
	if(bright_only_)
	{
		SetUncertainties(1.0, 1.0, 2.0);
		SetCoordinatesPro0(bright_beams_from_calibration_);
		accumulated_wavelength_compensation_ = WavelengthCompensationFit::WavelengthCompInfo();
	}
	else
	{
		SetUncertainties(general_uncertainty_, general_uncertainty_, general_uncertainty_ * uncertainty_proj_fac_);
		if(all_beams_from_last_wavelength_compensation_.empty())
		{
			all_beams_from_last_wavelength_compensation_ = all_beams_from_calibration_;
		}
		SetCoordinatesPro0(all_beams_from_last_wavelength_compensation_);
	}
}

void ReconstructorLaserDots3D::UpdateProjectorPtsToPatternCompensationStateWithoutReset(
	const WavelengthCompensationFit::WavelengthCompInfo& info)
{
	//apply new state
	{
		//to eigen::vector2d...
		std::vector<Eigen::Vector2d> origCoords(projector.DotsCount());
		std::transform(projector.GetDots().begin(),
		               projector.GetDots().end(),
		               origCoords.begin(),
		               [](const cv::Vec2f& pt) -> Eigen::Vector2d
		               {
			               return Eigen::Vector2d(pt[0], pt[1]);
		               });


		// use wavelength compensation fit for calculating new projector points.
		const Eigen::Vector2d centralBeam(projector.GetDot(0).x, projector.GetDot(0).y);
		WavelengthCompensationFit fit(origCoords, origCoords, centralBeam);

		const auto toApply = fit.ToCompInfo(fit.ToEigenVector(info));

		all_beams_from_last_wavelength_compensation_ = fit.ApplyToPoints(toApply, projector.GetDots());
		SetCoordinatesPro0(all_beams_from_last_wavelength_compensation_);
		accumulated_wavelength_compensation_ = info;
	}
}

int ReconstructorLaserDots3D::MarkUnsafeByPeakValue(const DotInfos ir0_dot_params,
                                                    const DotInfos ir1_dot_params,
                                                    std::vector<int>& point_flags)
{
	int unsafe = 0;
	for(size_t i = 0; i < triangulated_points_.size(); ++i)
	{
		TripleCorrespondence corr;
		corr       = GetUsedCorrespondence((int)i);
		int ir0Idx = corr.camera_index[0];
		int ir1Idx = corr.camera_index[1];

		double q = ir0_dot_params[ir0Idx][0] / ir1_dot_params[ir1Idx][0];
		if(q < 0.5 || q > 2.0)
		{
			point_flags[i] |= IS_UNSAFE;
			unsafe++;
		}
	}
	return unsafe;
}

int ReconstructorLaserDots3D::MarkUnsafeByRadius(std::vector<int>& point_flags)
{
	// Sanity checks
	if(triangulated_points_.empty())
		return 0;
	if(triangulated_points_.size() != point_flags.size())
		return 0;

	// Projection of 3D points on DOE/laser
	std::vector<cv::Vec4f> errors;
	GetProjectionError(errors, 2);

	// Compute maximal radius
	double maxRad = 0.0;
	for(unsigned int i = 0; i < errors.size(); ++i)
	{
		double rad = sqrt(errors[i][0] * errors[i][0] + errors[i][1] * errors[i][1]);
		if(rad > maxRad)
			maxRad = rad;
	}

	// Eliminate points outside of the radius
	maxRad     = std::max(250.0, maxRad * projector_radius_factor_);
	int unsafe = 0;
	for(unsigned int i = 0; i < errors.size(); ++i)
	{
		double rad = sqrt(errors[i][0] * errors[i][0] + errors[i][1] * errors[i][1]);
		if(rad > maxRad)
		{
			point_flags[i] |= IS_UNSAFE;
			unsafe++;
		}
	}
	return unsafe;
}

int ReconstructorLaserDots3D::MarkUnsafeByAggressiveBrightOnly(std::vector<int>& point_flags)
{
	// Sanity checks
	if(triangulated_points_.empty())
		return 0;
	if(triangulated_points_.size() != point_flags.size())
		return 0;
	int unsafe = 0;
	for(size_t i = 0; i < triangulated_points_.size(); ++i)
	{
		int tripleIdx   = point2_triple_[i];
		int projBeamIdx = triples_[tripleIdx].camera_index[2];
		if(laser_beam_matrix_.at<double>(projBeamIdx, 0) < 0.5)
		{
			point_flags[i] |= IS_UNSAFE;
			unsafe++;
		}
	}
	return unsafe;
}

int ReconstructorLaserDots3D::MarkUnsafeByCredibility(std::vector<int>& point_flags)
{
	// Sanity checks
	if(triangulated_points_.empty())
		return 0;
	if(triangulated_points_.size() != point_flags.size())
		return 0;
	int unsafe = 0;
	for(size_t i = 0; i < triangulated_points_.size(); ++i)
	{
		if(triples_[point2_triple_[i]].credibility <= 0)
		{
			point_flags[i] |= IS_UNSAFE;
			unsafe++;
		}
	}
	return unsafe;
}

int ReconstructorLaserDots3D::MarkUnsafeByProjectionError(std::vector<int>& point_flags)
{
	// Sanity checks
	if(triangulated_points_.empty())
		return 0;
	if(triangulated_points_.size() != point_flags.size())
		return 0;

	// 3d point is calculated by cams only so the triple is not treated symmetrically

	float sqMaxErrProj = (float)(max_err_proj_ * max_err_proj_);
	float sqMaxErrCams = (float)(max_err_cams_ * max_err_cams_);

	int unsafe = 0;

	Dots proj2d, cam0_2d, cam1_2d;
	projector.GetCalibration()->ProjectPoints(triangulated_points_, proj2d);
	camera_sensor0.GetCalibration()->ProjectPoints(triangulated_points_, cam0_2d);
	camera_sensor1.GetCalibration()->ProjectPoints(triangulated_points_, cam1_2d);

	for(size_t i = 0; i < triangulated_points_.size(); ++i)
	{
		const TripleCorrespondence& tr = triples_[point2_triple_[i]];

		cv::Vec2f diff_proj = proj2d[i] - projector.GetDot(tr.camera_index[2]);
		cv::Vec2f diff_cam0 = cam0_2d[i] - camera_sensor0.GetDot(tr.camera_index[0]);
		cv::Vec2f diff_cam1 = cam1_2d[i] - camera_sensor1.GetDot(tr.camera_index[1]);


		if(diff_proj.dot(diff_proj) > sqMaxErrProj ||
			diff_cam0.dot(diff_cam0) > sqMaxErrCams ||
			diff_cam1.dot(diff_cam1) > sqMaxErrCams)
		{
			point_flags[i] |= IS_UNSAFE;
			unsafe++;
		}
	}
	return unsafe;
}

int ReconstructorLaserDots3D::MarkUnsafeByDepth(std::vector<int>& point_flags)
{
	int unsafe = 0;
	for(size_t i = 0; i < triangulated_points_.size(); ++i)
	{
		if(triangulated_points_[i].z < 0.3 || triangulated_points_[i].z > 1.2)
		{
			point_flags[i] |= IS_UNSAFE;
		}
		unsafe++;
	}
	return unsafe;
}

std::vector<ReconstructorLaserDots3D::TripleCorrespondence> ReconstructorLaserDots3D::GetTripleCorrespondences() const
{
	return triples_;
}

int ReconstructorLaserDots3D::MarkUnsafeByPatchComparison(const cv::Mat& ir0_img,
                                                          const cv::Mat& ir1_img,
                                                          std::vector<int>& point_flags)
{
	int unsafe = 0;
	for(size_t i = 0; i < triangulated_points_.size(); ++i)
	{
		const TripleCorrespondence& corr = GetUsedCorrespondence((int)i);

		cv::Vec2f coord0 = camera_sensor0.GetDot(corr.camera_index[0]);
		cv::Vec2f coord1 = camera_sensor1.GetDot(corr.camera_index[1]);

		cv::Mat patch0, patch1, patch2, patch3;


		// probably this is not good because new patch is created with bilinear interpolation
		// and we do a quadric fit later on the correlation image
		// Maybe it is better to match sub-matrices with POI off center and keep the offset vectors in mind
		// instead of creating an interpolated image with POI in center

		cv::getRectSubPix(ir0_img, cv::Size(15, 15), cv::Point2f(coord0), patch0);
		cv::getRectSubPix(ir0_img, cv::Size(9, 9), cv::Point2f(coord0), patch1);
		cv::getRectSubPix(ir1_img, cv::Size(15, 15), cv::Point2f(coord1), patch2);
		cv::getRectSubPix(ir1_img, cv::Size(9, 9), cv::Point2f(coord1), patch3);

		cv::Mat correlation0;
		cv::matchTemplate(patch1, patch3, correlation0, cv::TM_CCOEFF_NORMED);

		// 		if ( correlation0.at<float>(0,0) < 0.8 )
		// 		{
		// 			pointFlags[i] |= IS_UNSAFE ;
		// 			unsafe++;
		// 		}

		cv::Mat correlation1;
		cv::matchTemplate(patch0, patch3, correlation1, cv::TM_CCOEFF_NORMED);
		double minc, maxc;
		cv::Point maxp, minp;
		cv::minMaxLoc(correlation1, &minc, &maxc, &minp, &maxp);
		//TODO
		cv::Vec<float, 7> subpixelCoord = SubPixel::subPixel(correlation1, maxp, 5);
		cv::Vec2f corrected0(subpixelCoord[0], subpixelCoord[1]);
		cv::Vec2f ideal(3.0f, 3.0f);

		cv::Mat correlation2;
		cv::matchTemplate(patch2, patch1, correlation2, cv::TM_CCOEFF_NORMED);
		cv::minMaxLoc(correlation2, &minc, &maxc, &minp, &maxp);
		// TODO
		subpixelCoord = SubPixel::subPixel(correlation1, maxp, 5);
		cv::Vec2f corrected1(subpixelCoord[0], subpixelCoord[1]);

		double dist0 = cv::norm(corrected0 - ideal);
		double dist1 = cv::norm(corrected1 - ideal);

		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Patch Distance 0 [" << dist0 << "] Patch Distance 1 [" << dist1 << " difference [" <<
 dist0 - dist1 << "]";

		if(dist0 > 0.3 && correlation0.at<float>(0, 0) < 0.8)
		{
			point_flags[i] |= IS_UNSAFE;
			unsafe++;
		}
	}
	return unsafe;
}

void ReconstructorLaserDots3D::InitBeamNeighbors(const cv::Mat& beams)
{
	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : beam matrix rows " << beams.rows << " cols " << beams.cols;

	cv::Mat beamPositions;
	cv::Mat(beams, cv::Range(0, beams.rows), cv::Range(1, 3)).convertTo(beamPositions, CV_32F);

	using Distance = cvflann::L2<float>;

	// create a FLANN matrix interface to the position matrix data
	cvflann::Matrix<Distance::ElementType> flannBeamPositions((float*)beamPositions.data, beamPositions.rows, beamPositions.cols);

	cvflann::KDTreeSingleIndexParams idxParams;
	cvflann::KDTreeSingleIndex<Distance> kdTree(flannBeamPositions, idxParams);
	kdTree.buildIndex();

	//cvflann::Index<Distance> kdTree(flannBeamPositions, idxParams, Distance());

	// cv::flann::Index  kdtree( beamPositions,
	// Create the Index and search tree
	// one neighbor vector for each beam
	beam_neighbours_.resize(beams.rows);

	std::vector<int> brightBeams;

	// for each beam find the neighbors and add them to the neighborhood vector

	const int dim = 2;
	std::vector<float> q(dim);
	cvflann::Matrix<float> queryMat(&q[0], 1, dim);


	const int sz = 11;
	int idxData[sz];
	float distData[sz];

	cvflann::Matrix<int> idxMat(&idxData[0], 1, sz);
	cvflann::Matrix<float> distMat(&distData[0], 1, sz);
	cvflann::SearchParams searchParams(-1, 0.0f, true);


	for(int i = 0; i < beams.rows; ++i)
	{
		if(beams.at<double>(i, 0) > 0.5)
			brightBeams.push_back(i);

		const float radius = 25.0;

		queryMat[0][0] = flannBeamPositions[i][0];
		queryMat[0][1] = flannBeamPositions[i][1];

		kdTree.knnSearch(queryMat, idxMat, distMat, 10, searchParams);
		//int nncv = kdTree.radiusSearch(queryMat, idxMat, distMat, radius*radius, searchParams );

		for(int j = 1; j < 9; ++j)
		{
			// add to neighborhood vector
			beam_neighbours_[i].push_back(idxMat[0][j]);
		}
	}


	// all bright beams get the bright neighbor beams in addition


	// reserve memory...
	cv::Mat brightBeamPositions((int)brightBeams.size(), 2, CV_32F);

	// ... and create a FLANN matrix interface to the position matrix data
	cvflann::Matrix<Distance::ElementType> flannBrightBeamPositions((float*)brightBeamPositions.data,
	                                                                brightBeamPositions.rows,
	                                                                brightBeamPositions.cols);

	// fill FLANN matrix with bright beam positions
	for(int iBr = 0; iBr < (int)brightBeams.size(); ++iBr)
	{
		flannBrightBeamPositions[iBr][0] = beamPositions.at<float>(brightBeams[iBr], 0);
		flannBrightBeamPositions[iBr][1] = beamPositions.at<float>(brightBeams[iBr], 1);
	}

	// create a FLANN kd-tree for exact neighborhood search
	cvflann::KDTreeSingleIndex<Distance> kdTreeBright(flannBrightBeamPositions);
	kdTreeBright.buildIndex();

	// search bright beams neighbors
	for(int iBr = 0; iBr < (int)brightBeams.size(); ++iBr)
	{
		// query point of current bright beam
		queryMat[0][0] = flannBrightBeamPositions[iBr][0];
		queryMat[0][1] = flannBrightBeamPositions[iBr][1];

		// find neighbors
		kdTreeBright.knnSearch(queryMat, idxMat, distMat, 10, searchParams);

		// add 8 neighbors (0 index is the query point itself)
		for(int j = 1; j < 9; ++j)
		{
			// idx maps to the index in the bright beams vector which then maps to the overall beam index
			const int brightNeighborBeamIdx = brightBeams[idxMat[0][j]];

			// add the bright neighbors as additional neighbors
			beam_neighbours_[brightBeams[iBr]].push_back(brightNeighborBeamIdx);
		}
	}

	/*
		for (size_t i = 0; i < m_beamNeighbours.size(); ++i)
		{
			std::shared_ptrString str;
			str << L"beam neighbors of " << i << L"at (" << beamPositions.at<float>((int)i, 0) << L" " << beamPositions.at<float>((int)i, 1) << L") : " ;
			for (size_t j = 0; j < m_beamNeighbours[i].size(); ++j)
			{
				str << m_beamNeighbours[i][j] << L" ";
			}
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : " << str.stringValue();

		}
	*/
}

/*
 * check for all triangulated 3d points whether at least one point of its laser beam neighbors
 * results in a 3d point with a max. distance of "ProjectorNeighborDistance"
 */
int ReconstructorLaserDots3D::MarkPlanarByNeighborAnalysis(std::vector<int>& point_flags)
{
	float maxSqPtDist = max_pt_dist_ * max_pt_dist_;

	std::vector<tbb::atomic<char>> hasValidNeighbor(point_flags.size(), 0);

	tbb::parallel_for(size_t(0),
	                  point_flags.size(),
	                  [&](size_t iPt)
	                  // for ( size_t iPt=0; iPt< pointFlags.size(); iPt++ )
	                  {
		                  // check if point has already been validated as neighbor from earlier 3d point
		                  if(!hasValidNeighbor[iPt])
		                  {
			                  // get beam index and 3d coordinate of currently analyzed point
			                  int beamIndex         = triples_[point2_triple_[iPt]].camera_index[2];
			                  const cv::Point3f& pt = triangulated_points_[iPt];

			                  // all neighbors of the current beam as precalculated at init time.
			                  const std::vector<int>& neighbors = beam_neighbours_[beamIndex];

			                  for(const int neighborIdx : neighbors)
			                  {
				                  // access the detected 3d point via the beam index, -1 if beam did not result in 3d point
				                  const int neighborPtIdx = beam_idx2_point_idx_[neighborIdx];
				                  if(neighborPtIdx < 0)
				                  {
					                  continue;
				                  }

				                  // get the 3d point of the currently analyzed neighbor beam and check distance
				                  const cv::Point3f& ptNeighbor = triangulated_points_[neighborPtIdx];
				                  cv::Point3f diff              = pt - ptNeighbor;
				                  if(diff.dot(diff) > maxSqPtDist)
				                  {
					                  continue;
				                  }

				                  // both points have valid neighbors
				                  hasValidNeighbor[iPt]           = 1;
				                  hasValidNeighbor[neighborPtIdx] = 1;
			                  }
		                  }
	                  });

	int numMarked = 0;
	for(size_t i = 0; i < hasValidNeighbor.size(); ++i)
	{
		if(hasValidNeighbor[i] == 0)
		{
			point_flags[i] |= HAS_NO_PROJ_NEIGHBOR;
			numMarked++;
		}
	}

	return numMarked;
}

void ReconstructorLaserDots3D::UpdateProjectorPtsToPatternCompensationState(const WavelengthCompensationFit::WavelengthCompInfo& info)
{
	if(info.scale < 0.95 || info.scale > 1.05)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Suspicious projector points scale of " << info.scale << " not set.";
		return;
	}

	ResetPattern();

	if(projector.IfDotsEmpty())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No projector points";
		return;
	}

	//apply new state
	{
		//to eigen::vector2d...
		std::vector<Eigen::Vector2d> orig_coords(projector.DotsCount());
		std::transform(projector.GetDots().begin(),
		               projector.GetDots().end(),
		               orig_coords.begin(),
		               [](const cv::Point2f& pt) -> Eigen::Vector2d
		               {
			               return Eigen::Vector2d(pt.x, pt.y);
		               });

		// use wavelength compensation fit for calculating new projector points.
		const Eigen::Vector2d central_beam(projector.GetDot(0).x, projector.GetDot(0).y);

		//TODO Why origCoords are given arguments for  both currentCoordsIn and origCoordsIn
		WavelengthCompensationFit fit(orig_coords, orig_coords, central_beam);
		const auto to_apply = fit.ToCompInfo(fit.ToEigenVector(info));
		SetCoordinatesPro0(fit.ApplyToPoints(to_apply, projector.GetDots()));
		accumulated_wavelength_compensation_ = info;
	}
}

void ReconstructorLaserDots3D::SetNextWavelengthComp(const WavelengthCompensationFit::WavelengthCompInfo& in)
{
	next_wavelength_compensation_                      = in;
	use_stored_wavelength_comp_for_next_triangulation_ = true;
}

bool ReconstructorLaserDots3D::GetForceDeepProjectorCompensation() const
{
	return force_deep_projector_compensation_;
}

void ReconstructorLaserDots3D::SetForceDeepProjectorCompensation(bool in)
{
	force_deep_projector_compensation_ = in;
}

bool ReconstructorLaserDots3D::HasGoodAccumulatedScaleCorrection() const
{
	return (int)(triangulated_points_.size()) > GetNumBeams() / 3;
}

void ReconstructorLaserDots3D::ResetPattern()
{
	if(bright_only_)
	{
		SetUncertainties(1.0, 1.0, 2.0);
		SetCoordinatesPro0(bright_beams_from_calibration_);
	}
	else
	{
		SetUncertainties(general_uncertainty_, general_uncertainty_, general_uncertainty_ * uncertainty_proj_fac_);
		SetCoordinatesPro0(all_beams_from_calibration_);
	}
	accumulated_wavelength_compensation_ = WavelengthCompensationFit::WavelengthCompInfo();
}

void ReconstructorLaserDots3D::TriangulateWithAutoWaveLengthCorrection(const Dots& dots0,
                                                                       const Dots& dots1,
                                                                       std::vector<cv::Point3f>& triangulated_points,
                                                                       bool deep_search)
{
	if(auto_compensation_iterations_ <= 0)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : auto_compensation_iterations_ <= 0";
		return;
	}

	auto mean_pt_shift             = 0.0;
	const auto mean_pt_shift_limit = auto_compensation_limit_;
	auto num_runs                  = 0;

	SetCoordinates(dots0, dots1);

	do
	{
		if(!deep_search)
		{
			triangulated_points = Calculate3DData();
		}
		else
#pragma region DEEP_SEARCH
		{
			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : deep wavelength search: dots0 " << dots0.size() << " triangulated Points : " <<
 triangulated_points_.size() << " min: " << dots0.size() / 2;

			const WavelengthCompensationFit::WavelengthCompInfo start_info;

			//this treats the case where in replay the start projection pattern does not match the one in the video.
			//In this case, do a search to find a better starting point for the triangulation.
			WavelengthCompensationFit::WavelengthCompInfo test_info;
			WavelengthCompensationFit::WavelengthCompInfo used_best = start_info;

			UpdateProjectorPtsToPatternCompensationState(start_info);
			triangulated_points = Calculate3DData();

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation points count with start_info: " << triangulated_points.size();
			test_info = used_best;

			//Deep search not only take care of wavelength compensation but also corrects the projector calibration itself
			if(search_bended_projector_)
			{
				std::vector<std::pair<WavelengthCompensationFit::WavelengthCompInfo, size_t>> fitnesses;
				const auto num_step_y = 5;
				const auto step_y     = 0.1;

				for(auto i = -num_step_y; i < num_step_y; ++i)
				{
					test_info.translation = start_info.translation - Eigen::Vector2d(0., (double)i * step_y);
					UpdateProjectorPtsToPatternCompensationState(test_info);
					triangulated_points = Calculate3DData();
					fitnesses.emplace_back(test_info, triangulated_points.size());
				}

				const auto best = std::max_element(fitnesses.begin(),
				                                   fitnesses.end(),
				                                   [](const std::pair<WavelengthCompensationFit::WavelengthCompInfo, size_t>& first,
				                                      const std::pair<WavelengthCompensationFit::WavelengthCompInfo, size_t>& second) -> bool
				                                   {
					                                   return first.second < second.second;
				                                   });

				assert(best != fitnesses.end());

				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation search result " << best->second << " correspondences";

				//only set new values if they fulfill the required number of triangulated points
				const int wl_comp_min_pts = compensation_min_pts_;

				if(best->second > (wl_comp_min_pts / (bright_only_ ? 10 : 1)))
				{
					used_best = best->first;
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation numPts after translation: " << best->second;
				}
				else
				{
					//reset to startInfo if search was not successful
					BOOST_LOG_TRIVIAL(info) << __FUNCTION__ <<
 "() : wavelength compensation numPts after translation reset to start, no success in translation";
					used_best = start_info;
				}
			}

			test_info = used_best;
			if(search_wave_length_)
			{
				std::vector<std::pair<WavelengthCompensationFit::WavelengthCompInfo, size_t>> fitness_vector;
				const auto num_steps_scale = 20;
				const auto scale_step      = 0.001;

				for(auto i = -num_steps_scale; i < num_steps_scale; ++i)
				{
					test_info.scale = start_info.scale + (double)i * scale_step;
					UpdateProjectorPtsToPatternCompensationState(test_info);
					triangulated_points = Calculate3DData();
					fitness_vector.emplace_back(test_info, triangulated_points.size());
				}

				// Get the compensation element with maximum number of detected 3d points
				const auto best = std::max_element(fitness_vector.begin(),
				                                   fitness_vector.end(),
				                                   [](const std::pair<WavelengthCompensationFit::WavelengthCompInfo, size_t>& first,
				                                      const std::pair<WavelengthCompensationFit::WavelengthCompInfo, size_t>& second) -> bool
				                                   {
					                                   return first.second < second.second;
				                                   });

				//assert(best != fitnesses.end());

				used_best = best->first;
				BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Wavelength compensation numPts after scale : " << best->second;
				// calc projector errors
			}


			UpdateProjectorPtsToPatternCompensationState(used_best);
			triangulated_points = Calculate3DData();

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation start compensation : Scale [" << start_info.scale << "] Trans ["
 << start_info.translation << "] Rot [" << start_info.rotation << "]";

			BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : wavelength compensation used best compensation: Scale [" << used_best.scale <<
 "] Trans[" << used_best.translation << "] Rot [" << used_best.rotation << "]";

			//not point in repeating process
			deep_search = false;
		}
#pragma endregion // DEEP_SEARCH

		// I tried the number of detected 3D points as additional break criterion ( which appeared obvious to me) :
		// no further compensation if last iteration did not increase the number of Points
		// But the result was a significant increase of stray points [GH]

		mean_pt_shift = wavelength_compensation_on_ ? CompensatePatternErrors() : 0.0;
		num_runs++;
	}
	while(mean_pt_shift > mean_pt_shift_limit && num_runs < auto_compensation_iterations_ && !use_stored_wavelength_comp_for_next_triangulation_);

	// Ask Mark for details....
	use_stored_wavelength_comp_for_next_triangulation_ = false;
}

double ReconstructorLaserDots3D::AccumulatedScaleCorrection() const
{
	return accumulated_wavelength_compensation_.scale;
}

double ReconstructorLaserDots3D::AccumulatedRotationCorrection() const
{
	return accumulated_wavelength_compensation_.rotation;
}

Eigen::Vector2d ReconstructorLaserDots3D::AccumulatedTranslationCorrection() const
{
	return accumulated_wavelength_compensation_.translation;
}

WavelengthCompensationFit::WavelengthCompInfo ReconstructorLaserDots3D::AccumulatedWavelengthCompInfo() const
{
	return accumulated_wavelength_compensation_;
}

bool ReconstructorLaserDots3D::GetBrightOnly() const
{
	return bright_only_;
}

double ReconstructorLaserDots3D::GetLastWavelengthScale() const
{
	return accumulated_wavelength_compensation_.scale;
}

Eigen::Vector2d ReconstructorLaserDots3D::GetLastTranslation() const
{
	return accumulated_wavelength_compensation_.translation;
}

double ReconstructorLaserDots3D::GetLastRotation() const
{
	return accumulated_wavelength_compensation_.rotation;
}

//void ReconstructorLaserDots3D::setNextWavelengthComp(const WavelengthCompensationFit::WavelengthCompInfo& in)
//{
//	m_nextWavelengthComp                        = in;
//	useStoredWavelengthCompForNextTriangulation = true;
//}
//
//bool ReconstructorLaserDots3D::getForceDeepProjectorCompensation() const
//{
//	return m_forceDeepProjectorCompensation;
//}
//
//void ReconstructorLaserDots3D::setForceDeepProjectorCompensation(bool in)
//{
//	m_forceDeepProjectorCompensation = in;
//}
//
//bool ReconstructorLaserDots3D::hasGoodAccumulatedScaleCorrection() const
//{
//}
//
//void ReconstructorLaserDots3D::setTripleCorrespondences(const std::vector<triple_correspondence_type>& triples)
//{
//}

int ReconstructorLaserDots3D::MarkNoMotion(cv::Mat& left,
                                           cv::Mat& right)
{
	// Sanity checks
	if(right.empty())
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Right image is empty";
		return -1;
	}

	if(right.type() != CV_32F)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Right image is of incorrect type";
		return -1;
	}

	if(right.cols != 9)
	{
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Right image cols != 9";
		return -1;
	}

	// Sort by beam. Left matrix may be empty
	if(!left.empty())
	{
		qsort(left.data,
		      left.rows,
		      left.step,
		      [](const void* l,
		         const void* r)
		      {
			      const int* li = (int*)l;
			      const int* ri = (int*)r;

			      return
				      (li[8] & BEAM_INDEX_16BIT) < (ri[8] & BEAM_INDEX_16BIT) ? -1 : (li[8] & BEAM_INDEX_16BIT) > (ri[8] & BEAM_INDEX_16BIT) ? 1 : 0;
		      });
	}

	qsort(right.data,
	      right.rows,
	      right.step,
	      [](const void* l,
	         const void* r)
	      {
		      const int* li = (int*)l;
		      const int* ri = (int*)r;

		      return
			      (li[8] & BEAM_INDEX_16BIT) < (ri[8] & BEAM_INDEX_16BIT) ? -1 : (li[8] & BEAM_INDEX_16BIT) > (ri[8] & BEAM_INDEX_16BIT) ? 1 : 0;
	      });

	// Left matrix is empty, mark all points in right matrix as non moving
	if(left.empty() || left.type() != right.type())
	{
		left = right.clone();

		for(int r = 0; r < right.rows; ++r)
		{
			right.at<int>(r, 8) |= NOT_MOVED;
		}

		return right.rows;
	}

	// Mark non-moving points
	//	Merge right beams into left. Keep some history of motion
#	define BeamNo(MAT, I) (MAT.at<int>(I, 8) & BEAM_INDEX_16BIT)
	auto n_marked        = 0;
	const auto left_rows = left.rows;

	for(auto r = 0, l = 0; r < right.rows && l < left_rows; ++r)
	{
		// The beam is in left, but not in right
		while(BeamNo(left, l) < BeamNo(right, r) && l < left_rows)
		{
			++l;
		}

		// The beam is in right, but not in left.
		//	Add the beam to the left matrix
		if(l == left.rows || BeamNo(left, l) > BeamNo(right, r))
		{
			left.push_back(right.row(r));
			continue;
		}

		// Both matrices have this beam
		//	Check for no-motion and replace the beam data in left.
		const auto pix_diff = cv::norm(cv::Vec2f(left.ptr<float>(l, 0)) - cv::Vec2f(right.ptr<float>(r, 0)));
		const auto z_diff   = fabs((left.at<float>(l, 4) - right.at<float>(r, 4)));
		const auto z_dist   = std::max({fabs(left.at<float>(l, 4)), fabs(right.at<float>(r, 4)), 1.0f});

		if(pix_diff < 1.0 && z_diff / z_dist / z_dist < 0.01)
		{
			right.at<int>(r, 8) |= NOT_MOVED;
			++n_marked;
		}

		right.row(r).copyTo(left.row(l));
		++l;
	}

	return n_marked;
}

bool ReconstructorLaserDots3D::IsValid(const TemperatureOfScale& scale)
{
	// Something between 0 and 400 �Celsius
	return 0.0f < scale.first && scale.first < 400.0f;
}

ReconstructorLaserDots3D::TemperatureOfScale ReconstructorLaserDots3D::AdjustScale(const TemperatureOfScale& last_value,
                                                                                   const float current_temperature)
{
	if(!IsValid(last_value))
	{
		//apparently not yet set or some error, not projector points update
		BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : No last wavelength and temperature information stored on device or error reading them.";
		return last_value;
	}

	//we assume here that the last wavelength corresponded to 810nm. Note that this does not influence the scale (which we are interested in only) very much.
	const float delta_temp = current_temperature - last_value.first;
	const float ref_wavelength = 810.f;
	const float wavelength_vs_temperature_slope = 0.3f;
	const float scale = (ref_wavelength + delta_temp * wavelength_vs_temperature_slope) / ref_wavelength * last_value.second;

	WavelengthCompensationFit::WavelengthCompInfo info(AccumulatedWavelengthCompInfo());
	info.scale = scale;

	UpdateProjectorPtsToPatternCompensationState(info);

	BOOST_LOG_TRIVIAL(info) << __FUNCTION__ << "() : Device temperature " << current_temperature << "() : Adjusted wavelength scale to " << scale;

	return {current_temperature, scale};
}
