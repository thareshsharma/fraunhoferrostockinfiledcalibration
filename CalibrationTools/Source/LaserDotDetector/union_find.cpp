#include "union_find.h"

using namespace SFMAlgorithms;

// Erzeugen und initialisieren von n Partitionen
// jedes der n Elemente bildet seine eigene Partition und ist
// sein eigener Repraesentant
UnionFind::UnionFind(int n)
{
	if(n < 1)
	{
		m_rootList.pred = &m_rootList;
		m_rootList.succ = &m_rootList;
		m_numPartitions = 0;
		return;
	}

	// Erzeugen der Knoten
	m_nodes = new node[n];

	// jeder Knoten ist eigene Partition
	m_numPartitions = n;

	// Initialisieren der Knoten
	for(int i = 0; i < n; ++i)
	{
		m_nodes[i].id     = i;
		m_nodes[i].parent = &m_nodes[i];
		m_nodes[i].size   = 0;
		m_nodes[i].card   = 1;
		m_nodes[i].pred   = (i == 0) ? &m_rootList : &m_nodes[i - 1];
		m_nodes[i].succ   = (i == n - 1) ? &m_rootList : &m_nodes[i + 1];
	}

	m_rootList.pred = &m_nodes[n - 1];
	m_rootList.succ = &m_nodes[0];

	// Repraesentanten-Vektor leeren und als unbesetzt markieren
	m_roots.clear();
	m_rootsDirty = 1;
}

UnionFind::~UnionFind()
{
	if(m_numPartitions)
		delete[] m_nodes;
}

// Suche nach dem Repraesentanten der Partition, in der Element i liegt

int UnionFind::findPartition(int i)
{
	// Wenn Element i Wurzel (zeigt auf sich selber) des Elementbaumes ist,
	// ist es selber Repraesentant
	if(m_nodes[i].parent == &m_nodes[i])
	{
		return m_nodes[i].id;
	}

	// Ansonsten ist der Repraesentant des Vorgaengers der Repraesentant
	// Rekursiv anwenden, und Vorgaenger anpassen (Prinzip: Pfadkomprimierung bei Suche)
	m_nodes[i].parent = &m_nodes[findPartition((m_nodes[i].parent)->id)];

	return m_nodes[i].parent->id;
}

int UnionFind::getPartitionRepresentative(int partitionIdx)
{
	// ggf. m_roots neu aufbauen
	if(m_rootsDirty)
	{
		renameRoots();
		m_rootsDirty = 0;
	}
	return m_roots[partitionIdx]->id;
}

int UnionFind::findPartitionIndex(int i)
{
	// ggf. m_roots neu aufbauen
	if(m_rootsDirty)
	{
		renameRoots();
		m_rootsDirty = 0;
	}

	// Wenn Element i Wurzel des Elementbaumes ist, ist es selber Repraesentant
	if(m_nodes[i].parent == &m_nodes[i])
	{
		return m_nodes[i].partition_idx;
	}

	// Ansonsten ist der Repraesentant des Vorgaengers der Repraesentant
	// Rekursiv anwenden, und Vorgaenger anpassen (Prinzip: Pfadkomprimierung bei Suche)
	m_nodes[i].parent = &m_nodes[findPartition((m_nodes[i].parent)->id)];

	return m_nodes[i].parent->partition_idx;
}

void UnionFind::unionPartitions(int j,
                                int k)
{
	int root_j = findPartition(j);
	int root_k = findPartition(k);

	// wenn Knoten j und k bereits in der gleichen Partition sind ist nichts zu tun
	if(root_j == root_k)
	{
		return;
	}

	// Anzahl der Partitionen reduzieren
	--m_numPartitions;

	// Repraesentantenindex-Vektor als nicht mehr aktuell markieren
	m_rootsDirty = 1;

	// Bei ungleicher Tiefe wird die Wurzel des Teilbaumes mit groesserer Tiefe gemeinsamer Repraesentant
	// Sorgt fuer max. logarithmisch wachsende Tiefe der Baeume
	if(m_nodes[root_j].size >= m_nodes[root_k].size)
	{
		m_nodes[root_k].parent = &m_nodes[root_j];

		// Wenn beide Teilbaeume vorher gleich tief waren, ist der neue nun um eins tiefer
		if(m_nodes[root_j].size == m_nodes[root_k].size)
		{
			m_nodes[root_j].size++;
		}

		m_nodes[root_j].card += m_nodes[root_k].card;

		// Knoten K aus der Wurzelliste entfernen
		m_nodes[root_k].pred->succ = m_nodes[root_k].succ;
		m_nodes[root_k].succ->pred = m_nodes[root_k].pred;
	}
		// ansonsten wird representant von k Wurzel
	else
	{
		m_nodes[root_j].parent = &m_nodes[root_k];
		m_nodes[root_k].card += m_nodes[root_j].card;

		// Knoten j aus der Wurzelliste entfernen
		m_nodes[root_j].pred->succ = m_nodes[root_j].succ;
		m_nodes[root_j].succ->pred = m_nodes[root_j].pred;
	}
}

// Abfrage der aktuellen Anzahl von Partitionen
int UnionFind::getNumPartitions()
{
	return m_numPartitions;
}

// Abfrage der Kardinalitaet der Partition, zu der Element i gehoert
int UnionFind::getCardinality(int i)
{
	return m_nodes[findPartition(i)].card;
}

int UnionFind::getCardinalityIndex(int part_idx)
{
	// ggf. m_roots neu aufbauen
	if(m_rootsDirty)
	{
		renameRoots();
		m_rootsDirty = 0;
	}

	return m_roots[part_idx]->card;
}

void UnionFind::renameRoots()
{
	m_roots.clear();
	int i = 0;

	// Liste der Repraesentantenknoten durchlaufen und Index der Partition zaehlen und setzen
	node* next = m_rootList.succ;
	while(next != &m_rootList)
	{
		m_roots.push_back(next);
		next->partition_idx = i;
		next                = next->succ;
		i++;
	}
}
