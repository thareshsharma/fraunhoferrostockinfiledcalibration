#pragma once

//Defines
#define DMVS_CAMERA_CALIBRATION
#define _WINDOWS
#define F_OK 0x00
#define W_OK 0x02
#define R_OK 0x04

//std namespace
#include <iostream>
#include <sstream>
#include <fstream>
#include <istream>
#include <string>
#include <numeric>
#include <memory>
#include <atomic>
#include <chrono>
#include <memory>

#define WIN32_LEAN_AND_MEAN      //Exclude rarely-used stuff from Windows headers

#include <windows.h>
#include <synchapi.h>
#include <tchar.h>
#include <ShlObj.h>
#include <Shlwapi.h>
#include <shellapi.h>
#include <minwindef.h>
#include <shlobj_core.h>
#include <fcntl.h>
#include <corecrt_io.h>
#include <atltime.h>
#include <atlbase.h>
#include <Windows.h>
#include <cassert>
#include <time.h>

//Containers
#include <vector>
#include <deque>
#include <map>
#include <list>
#include <set>
#include <algorithm>
#include <utility>
#include <unordered_map>
#include "assert.h"

#include <stack>
#include <sstream>
#include <future>
#include <filesystem>

//Math
#define _USE_MATH_DEFINES

//Exceptions
#include <exception>

//Eigen
#include <Eigen/Dense>
#include <Eigen/Eigen>
#include <unsupported/Eigen/NumericalDiff>
#include <unsupported/Eigen/NonLinearOptimization>

//OpenCV
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/cvdef.h>
#include <opencv2/core/eigen.hpp>
#include <opencv2/core/affine.hpp>
#include <opencv2/core/types_c.h>
#include <opencv2/core/core_c.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/calib3d/calib3d_c.h>

//boost
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/moment.hpp>

#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/program_options.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>

//Intel Tbb
#include "tbb/parallel_for.h"
#include "tbb/parallel_for_each.h"
#include "tbb/parallel_invoke.h"

// JSON
#include "nlohmann/json.hpp"

// Pugi XML
#include <pugixml.hpp>

//Define
#define DMVS_DELETE(ptr) delete ptr; ptr = nullptr;
